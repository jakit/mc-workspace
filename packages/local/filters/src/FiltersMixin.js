Ext.define('Filters.FiltersMixin', {
    // filterButtons: [{
        // xtype: 'button',
        // iconCls: "filters-toolbar-apply-filter",
        // tooltip: "Apply Filters",
        // listeners: {
            // "click": {
                // fn: function(btn) {
                    // btn.up('grid').applyFilters();
                // }
            // }
        // }
    // }],    
    
    filterButtons: [{
        xtype: 'button',
        iconCls: "filters-toolbar-clear-filter",
        tooltip: "Clear Filters",
        listeners: {
            "click": {
                fn: function(btn) {
                    var grid = btn.up('grid');
                    btn.up('grid').clearFilters.apply(grid, [false]);
                }
            }
        }
    }],
    
    defaultField: {
        xtype: 'textfield',
        enableKeyEvents: true,
        margin: "0",
        listeners: {
            change: {
                //fn: Ext.Function.createBuffered(function(field) {field.up('grid').applyFilters();}, 250, this)
				fn: Ext.Function.createBuffered(function(field) {
						 var tmpval = Ext.String.trim(field.getValue());
						 if(tmpval!="^"){field.up('grid').applyFilters();}
					}, 250, this)
            }
        }
    },
    applyFilters: function() {
        var me = this;
        var fn = function() {
            var me = this;
            var fields = me.HFAPI().getAllFields();
            var x, l = fields.length, field;
            var val;
            var filters = [];
			var existFilters = me.getStore().getFilters().items;
			//console.log("Exist Filters Items");
			//console.log(me.getStore().getFilters());
			//console.log(existFilters.length);
            for(x = 0; x < l; x++) {
                field = fields[x];
                if(field) {
                    val = Ext.String.trim(field.getValue());
					//console.log(field.up().dataIndex+":"+val+" : Length : "+val.length);
                    if(val.length > 0) {
						var newObj = {
							//disableOnEmpty : true,
							id: field.up().dataIndex,
							property: field.up().dataIndex,
							value: val,
							exactMatch: false,
							caseSensitive: false
						};
						newObj.operator = "like";
						filters.push(newObj);
                    }
					else{
						if(existFilters.length>1){
							for(var r=0; r<existFilters.length;r++){
								var filterObj = existFilters[r];
								if(filterObj.config.id==field.up().dataIndex){
									//console.log("Remove Filter");
									me.getStore().removeFilter(field.up().dataIndex,true);
								}
							}
						}
					}
                }
            }
			console.log(filters);
            if(filters.length > 0) {
                me.getStore().addFilter(filters); 
            } else {
				console.log("Clear Filters Call");
                me.clearFilters(true);
            }
        };
        var fn2 = Ext.Function.bind(fn, me);
        window.setTimeout(fn2, 50);
    },
    clearFilters: function(doit) {
        var me = this;
        var fn = function() {
            var me = this;
            var fields = me.HFAPI().getAllFields();
            var x, l = fields.length, field;
            var val;
            var filters = [];
            var removeAtKey = [];
            var doClearFilter = doit;
            for(x = 0; x < l; x++) {
                field = fields[x];
                if(field) {
                    val = Ext.String.trim(field.getValue());
                    if(val.length > 0) {
                        doClearFilter = true;
                        removeAtKey.push(field.up().dataIndex);
                        field.suspendEvents();
                        field.setValue(null);
                        field.resumeEvents({
                            discardQueue: true
                        });
                    }
                }
            }
           if(doClearFilter) {
			   var existFiltersItems = me.getStore().getFilters().items;
			   if(existFiltersItems.length>0){
	                me.getStore().clearFilter();               
			   }
           }
        };
        var fn2 = Ext.Function.bind(fn, me);
        window.setTimeout(fn2, 50);
    }    
});
