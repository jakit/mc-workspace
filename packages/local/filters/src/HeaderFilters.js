Ext.define('Filters.HeaderFields', {
    extend: 'Ext.grid.feature.Feature',
    alias: 'feature.headerfields',
    requires: [
        'Ext.form.field.*'
    ],
    eventPreventHandler: function(e) {
        e.stopPropagation();  
    },
    constructor: function(cfg) {
       var me = this;
       Ext.apply(me, cfg);
       me.callParent(arguments);
    },
    cfg: null,  
    init: function(grid) {
        var me = this,
            callback = null;
        
        var hidden = me.cfg.hidden;
        var columns, x, l;
        if(hidden) {
            Ext.suspendLayouts();
            columns = grid.columns;
            l = columns.length;
            for(x = 0; x < l; x++) {
                columns[x].down('component').hide();
            }
            Ext.resumeLayouts();
        }
        
        if(!grid.listeners) {
            grid.listeners = {};
        } else {
            if(grid.listeners.columnresize) {
                if(grid.listeners.columnresize.fn) {
                    if(grid.listeners.columnresize.scope) {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid.listeners.columnresize.scope);
                    } else {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid);
                    }
                } else {
                    callback = Ext.Function.bind(grid.listeners.columnresize, grid);    
                }
            }
        }
        
        me.setAPI(grid);
        
        Ext.apply(grid.listeners, {
            'columnresize': {
                fn: Ext.Function.bind(me.onColumnResizeFunc, grid, [callback], true), 
                scope: me
          }
        });         
    
    },
    onColumnResizeFunc: function(headerCt, targetColumn, width, opts, callback) {
        var me = this,
            items = Ext.ComponentQuery.query("component", targetColumn),
            x, l = items.length;
        
        for(x = 0; x < l; x++) {
            items[x].setWidth(width);
        }
        
        if(callback) {
            callback();
        }   
    },
    setAPI: function(grid) {
        grid.HFAPI = grid.HeaderFieldsAPI = Ext.Function.bind(function() {

            var grid = this;
            var result = [];
            var columns = grid.getColumnManager().columns;
            var x, field, l;
			if(columns){
				l = columns.length;
				for(x = 0; x < l; x++) {
					result.push(columns[x].down('component'));
				};
			}
            return {
                getField: function(dataIndex) {
                    var column = grid.down('gridcolumn[dataIndex="'+ dataIndex +'"]');
                    var obj = column.down('component');
                    return obj;
                },
                getFields: function(dataIndexArr) {
                    var result = [];
                    var x, l, column;
                    if(Ext.isArray(dataIndexArr) && dataIndexArr.length > 0) {
                        l = dataIndexArr.length;
                        for(x = 0; x < l; x++) {
                            result.push(grid.HFAPI().getField(dataIndexArr[x]));
                        }
                    }
                    return result;
                },
                getAllFields: function() {
                    return result;
                },
                hideRow: function() {
                    var x, l = result.length;                    
                    for(x = 0; x < l; x++) {
                        if(result[x]) {
                            result[x].hide();    
                        }
                        
                    }
                },
                showRow: function() {
                    var x, l = result.length;
                    for(x = 0; x < l; x++) {
                        if(result[x]) {
                            result[x].show();    
                        }
                    }
                }
            };
        }, grid);
    } 
});