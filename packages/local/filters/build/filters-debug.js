Ext.define('Filters.FiltersMixin', {
    // filterButtons: [{
    // xtype: 'button',
    // iconCls: "filters-toolbar-apply-filter",
    // tooltip: "Apply Filters",
    // listeners: {
    // "click": {
    // fn: function(btn) {
    // btn.up('grid').applyFilters();
    // }
    // }
    // }
    // }],    
    filterButtons: [
        {
            xtype: 'button',
            iconCls: "filters-toolbar-clear-filter",
            tooltip: "Clear Filters",
            listeners: {
                "click": {
                    fn: function(btn) {
                        var grid = btn.up('grid');
                        btn.up('grid').clearFilters.apply(grid, [
                            false
                        ]);
                    }
                }
            }
        }
    ],
    defaultField: {
        xtype: 'textfield',
        enableKeyEvents: true,
        margin: "0",
        listeners: {
            change: {
                fn: Ext.Function.createBuffered(function(field) {
                    field.up('grid').applyFilters();
                }, 250, this)
            }
        }
    },
    applyFilters: function() {
        var me = this;
        var fn = function() {
                var me = this;
                var fields = me.HFAPI().getAllFields();
                var x,
                    l = fields.length,
                    field;
                var val;
                var filters = [];
                for (x = 0; x < l; x++) {
                    field = fields[x];
                    if (field) {
                        val = Ext.String.trim(field.getValue());
                        if (val.length > 0) {
                            var newObj = {
                                    id: field.up().dataIndex,
                                    property: field.up().dataIndex,
                                    value: val,
                                    exactMatch: false,
                                    caseSensitive: false
                                };
                            newObj.operator = "like";
                            filters.push(newObj);
                        }
                    }
                }
                if (filters.length > 0) {
                    me.getStore().addFilter(filters);
                } else {
                    me.clearFilters(true);
                }
            };
        var fn2 = Ext.Function.bind(fn, me);
        window.setTimeout(fn2, 50);
    },
    clearFilters: function(doit) {
        var me = this;
        var fn = function() {
                var me = this;
                var fields = me.HFAPI().getAllFields();
                var x,
                    l = fields.length,
                    field;
                var val;
                var filters = [];
                var removeAtKey = [];
                var doClearFilter = doit;
                for (x = 0; x < l; x++) {
                    field = fields[x];
                    if (field) {
                        val = Ext.String.trim(field.getValue());
                        if (val.length > 0) {
                            doClearFilter = true;
                            removeAtKey.push(field.up().dataIndex);
                            field.suspendEvents();
                            field.setValue(null);
                            field.resumeEvents({
                                discardQueue: true
                            });
                        }
                    }
                }
                if (doClearFilter) {
                    me.getStore().clearFilter();
                }
            };
        var fn2 = Ext.Function.bind(fn, me);
        window.setTimeout(fn2, 50);
    }
});

Ext.define('Filters.HeaderFields', {
    extend: 'Ext.grid.feature.Feature',
    alias: 'feature.headerfields',
    requires: [
        'Ext.form.field.*'
    ],
    eventPreventHandler: function(e) {
        e.stopPropagation();
    },
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    cfg: null,
    init: function(grid) {
        var me = this,
            callback = null;
        var hidden = me.cfg.hidden;
        var columns, x, l;
        if (hidden) {
            Ext.suspendLayouts();
            columns = grid.columns;
            l = columns.length;
            for (x = 0; x < l; x++) {
                columns[x].down('component').hide();
            }
            Ext.resumeLayouts();
        }
        if (!grid.listeners) {
            grid.listeners = {};
        } else {
            if (grid.listeners.columnresize) {
                if (grid.listeners.columnresize.fn) {
                    if (grid.listeners.columnresize.scope) {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid.listeners.columnresize.scope);
                    } else {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid);
                    }
                } else {
                    callback = Ext.Function.bind(grid.listeners.columnresize, grid);
                }
            }
        }
        me.setAPI(grid);
        Ext.apply(grid.listeners, {
            'columnresize': {
                fn: Ext.Function.bind(me.onColumnResizeFunc, grid, [
                    callback
                ], true),
                scope: me
            }
        });
    },
    onColumnResizeFunc: function(headerCt, targetColumn, width, opts, callback) {
        var me = this,
            items = Ext.ComponentQuery.query("component", targetColumn),
            x,
            l = items.length;
        for (x = 0; x < l; x++) {
            items[x].setWidth(width);
        }
        if (callback) {
            callback();
        }
    },
    setAPI: function(grid) {
        grid.HFAPI = grid.HeaderFieldsAPI = Ext.Function.bind(function() {
            var grid = this;
            var result = [];
            var columns = grid.getColumnManager().columns;
            var x,
                l = columns.length,
                field;
            for (x = 0; x < l; x++) {
                result.push(columns[x].down('component'));
            }
            
            return {
                getField: function(dataIndex) {
                    var column = grid.down('gridcolumn[dataIndex="' + dataIndex + '"]');
                    var obj = column.down('component');
                    return obj;
                },
                getFields: function(dataIndexArr) {
                    var result = [];
                    var x, l, column;
                    if (Ext.isArray(dataIndexArr) && dataIndexArr.length > 0) {
                        l = dataIndexArr.length;
                        for (x = 0; x < l; x++) {
                            result.push(grid.HFAPI().getField(dataIndexArr[x]));
                        }
                    }
                    return result;
                },
                getAllFields: function() {
                    return result;
                },
                hideRow: function() {
                    var x,
                        l = result.length;
                    for (x = 0; x < l; x++) {
                        if (result[x]) {
                            result[x].hide();
                        }
                    }
                },
                showRow: function() {
                    var x,
                        l = result.length;
                    for (x = 0; x < l; x++) {
                        if (result[x]) {
                            result[x].show();
                        }
                    }
                }
            };
        }, grid);
    }
});

Ext.define('Filters.InjectHeaderFields', {
    // hideable: true, // or false // if its not hideable, then the toolbar doesn't appear on the right side of the grid (defaults to false)
    // hidden: false // or true // this is ignored when hideable is set to false (defaults to false)
    // position: "inner" or "outer" // defaults to inner
    // dockPosition: "right, top"
    headerFieldsConfig: {
        useToolbar: true,
        hideable: false,
        hidden: false,
        position: "inner",
        dockPosition: 'right',
        backgroundColor: "#f5f5f5",
        borderColor: "#c1c1c1",
        buttons: null,
        extraButtons: null
    },
    getHeaderFieldsConfig: function() {
        return this.headerFieldsConfig;
    },
    injectHeaderFields: function(obj) {
        var me = this,
            hideable, hidden, position, backgroundColor, borderColor, useToolbar, dockPosition, extraButtons, columns, x, l, y, ll,
            itemsCount = 0,
            column, z, rbarItem, HFToolbar,
            toolbars = [],
            buttons, z, lll;
        if (obj && Ext.isObject(obj) && Ext.Object.getSize(obj) > 0) {
            Ext.apply(me.headerFieldsConfig, obj);
        }
        extraButtons = me.headerFieldsConfig.extraButtons;
        dockPosition = me.headerFieldsConfig.dockPosition;
        useToolbar = me.headerFieldsConfig.useToolbar;
        hideable = me.headerFieldsConfig.hideable;
        hidden = me.headerFieldsConfig.hidden;
        position = me.headerFieldsConfig.position;
        backgroundColor = me.headerFieldsConfig.backgroundColor;
        borderColor = me.headerFieldsConfig.borderColor;
        buttons = me.headerFieldsConfig.buttons;
        // this needs to happen
        if (Ext.isArray(me.columns)) {
            columns = me.columns;
            if (Ext.isObject(me.defaults)) {
                l = columns.length;
                for (x = 0; x < l; x++) {
                    Ext.apply(columns[x], me.defaults);
                }
            }
        } else {
            l = me.columns.items.length;
            columns = [];
            for (x = 0; x < l; x++) {
                Ext.apply(me.columns.items[x], me.columns.defaults);
                columns.push(me.columns.items[x]);
                if (columns[x].headerField) {
                    itemsCount = Math.max(itemsCount, columns[x].headerField.length);
                }
            }
        }
        for (x = 0; x < l; x++) {
            column = columns[x];
            ll = (column.headerField) ? column.headerField.length : 0;
            column.items = [];
            if (ll == 0) {
                for (y = 0; y < itemsCount; y++) {
                    column.items.push({
                        xtype: 'label'
                    });
                }
            } else {
                if (ll == itemsCount) {
                    for (y = 0; y < ll; y++) {
                        column.items.push(column.headerField[y]);
                    }
                } else {
                    for (y = 0; y < ll; y++) {
                        column.items.push(column.headerField[y]);
                    }
                    for (z = y; z < itemsCount; z++) {
                        column.items.push({
                            xtype: 'label'
                        });
                    }
                }
            }
        }
        me.columns = columns;
        // toolbar is optional        
        if (useToolbar) {
            if (hideable == false) {
                hidden = false;
            }
            var collapseExpandButton = {
                    xtype: 'button',
                    iconCls: (hidden) ? "filters-toolbar-expand" : "filters-toolbar-collapse",
                    tooltip: (hidden) ? "Expand Filter Bar" : "Collapse Filter Bar",
                    rowHiddenState: hidden,
                    listeners: {
                        click: {
                            fn: function(btn) {
                                var t, tp, tb, items, x, l;
                                btn.rowHiddenState = !btn.rowHiddenState;
                                t = (btn.rowHiddenState) ? "filters-toolbar-expand" : "filters-toolbar-collapse";
                                btn.setIconCls(t);
                                tp = (btn.rowHiddenState) ? "Expand Filter Bar" : "Collapse Filter Bar";
                                btn.setTooltip(tp);
                                tb = btn.up('toolbar');
                                items = tb.items.items;
                                l = items.length;
                                if (btn.dockPosition == "right") {
                                    if (btn.rowHiddenState) {
                                        me.HFAPI().hideRow();
                                        for (x = 1; x < l; x++) {
                                            items[x].hide();
                                        }
                                        me.clearFilters();
                                    } else {
                                        me.HFAPI().showRow();
                                        for (x = 1; x < l; x++) {
                                            items[x].show();
                                        }
                                    }
                                } else {
                                    if (btn.rowHiddenState) {
                                        me.HFAPI().hideRow();
                                        for (x = 1; x < l - 1; x++) {
                                            items[x].hide();
                                        }
                                        me.clearFilters();
                                    } else {
                                        me.HFAPI().showRow();
                                        for (x = 1; x < l - 1; x++) {
                                            items[x].show();
                                        }
                                    }
                                }
                            },
                            scope: me
                        }
                    }
                };
            var placeHolder = {
                    xtype: 'box',
                    width: 16,
                    height: 24,
                    html: '&nbsp;'
                };
            var mainButton;
            if (dockPosition == "right") {
                if (hideable) {
                    collapseExpandButton.dockPosition = dockPosition;
                    mainButton = collapseExpandButton;
                } else {
                    mainButton = placeHolder;
                }
                HFToolbar = {
                    xtype: 'toolbar',
                    dock: dockPosition,
                    padding: "2",
                    style: ("background-color:" + backgroundColor + ";border:1px solid " + borderColor + "!important;"),
                    items: [
                        mainButton,
                        {
                            xtype: "tbseparator",
                            hidden: hidden
                        }
                    ]
                };
                if (buttons) {
                    lll = buttons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(buttons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, buttons);
                    if (extraButtons) {
                        HFToolbar.items.push({
                            xtype: "tbseparator",
                            hidden: hidden
                        });
                    }
                }
                if (extraButtons) {
                    lll = extraButtons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(extraButtons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, extraButtons);
                }
                if (me.rbar) {
                    rbarItem = me.rbar;
                    me.rbar = null;
                }
                if (!me.hasDockedItems()) {
                    me.dockedItems = [];
                }
                if (rbarItem) {
                    me.dockedItems.push(Ext.apply(rbarItem, {
                        dock: 'right'
                    }));
                }
                if (position === "inner") {
                    me.dockedItems.push(HFToolbar);
                } else {
                    me.dockedItems.unshift(HFToolbar);
                }
            } else {
                HFToolbar = {
                    xtype: 'toolbar',
                    dock: dockPosition,
                    padding: "2",
                    style: ("background-color:" + backgroundColor + ";border:1px solid " + borderColor + "!important;"),
                    items: [
                        "->"
                    ]
                };
                if (extraButtons) {
                    lll = extraButtons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(extraButtons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, extraButtons);
                    if (buttons || hideable) {
                        HFToolbar.items.push({
                            xtype: 'tbseparator',
                            hidden: hidden
                        });
                    }
                }
                if (buttons) {
                    lll = buttons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(buttons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, buttons);
                    if (hideable) {
                        HFToolbar.items.push({
                            xtype: 'tbseparator',
                            hidden: hidden
                        });
                    }
                }
                if (hideable) {
                    collapseExpandButton.dockPosition = dockPosition;
                    HFToolbar.items.push(collapseExpandButton);
                }
                if (me.tbar) {
                    rbarItem = me.tbar;
                    me.tbar = null;
                }
                if (!me.hasDockedItems()) {
                    me.dockedItems = [];
                }
                if (rbarItem) {
                    me.dockedItems.push(Ext.apply(rbarItem, {
                        dock: 'right'
                    }));
                }
                if (position === "inner") {
                    me.dockedItems.push(HFToolbar);
                } else {
                    me.dockedItems.unshift(HFToolbar);
                }
            }
        }
        return {
            hidden: hidden
        };
    },
    hasDockedItems: function() {
        var me = this;
        if (me.dockedItems && me.dockedItems.length > 0) {
            return true;
        }
        return false;
    }
});

