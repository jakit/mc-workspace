# mctheme/sass/etc

This folder contains miscellaneous SASS files. Unlike `"mctheme/sass/etc"`, these files
need to be used explicitly.
