# metainfogrid/sass/etc

This folder contains miscellaneous SASS files. Unlike `"metainfogrid/sass/etc"`, these files
need to be used explicitly.
