# metainfogrid/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    metainfogrid/sass/etc
    metainfogrid/sass/src
    metainfogrid/sass/var
