Ext.define('Filters2.FiltersMixin', {
    filterButtons: [
        {
            xtype: 'button',
            iconCls: "filters-toolbar-apply-filter",
            tooltip: "Apply Filters",
            listeners: {
                "click": {
                    fn: function(btn) {
                        btn.up('grid').applyFilters();
                    }
                }
            }
        },
        {
            xtype: 'button',
            iconCls: "filters-toolbar-clear-filter",
            tooltip: "Clear Filters",
            listeners: {
                "click": {
                    fn: function(btn) {
                        var grid = btn.up('grid');
                        btn.up('grid').clearFilters.apply(grid, [
                            false
                        ]);
                    }
                }
            }
        }
    ],
    defaultField: {
        xtype: 'textfield',
        enableKeyEvents: true,
        cls: "fieldFilterCls",
        margin: "0"
    },
    // listeners: {
    // change: {
    // //                fn: Ext.Function.createBuffered(function(field) {field.up('grid').applyFilters();}, 250, this)
    // }
    // }
    setFilterField: function(type, cfg) {
        var fieldTypes = {
                "genericlistfield": [
                    {
                        xtype: 'filters-genericlistfield'
                    }
                ],
                "combocheckboxfield": [
                    {
                        xtype: 'filters-combocheckboxfield'
                    }
                ],
                "stringfield": [
                    {
                        xtype: 'filters-stringfield'
                    }
                ],
                "numberfield": [
                    {
                        xtype: 'filters-numberfield1'
                    },
                    {
                        xtype: 'filters-numberfield2'
                    }
                ],
                "datefield": [
                    {
                        xtype: 'filters-datefield1'
                    },
                    {
                        xtype: 'filters-datefield2'
                    }
                ]
            };
        var field = Ext.clone(fieldTypes[type]);
        switch (type) {
            case "genericlistfield":
            case "combocheckboxfield":
            case "stringfield":
                Ext.apply(field[0], cfg);
                break;
            case "numberfield":
            case "datefield":
                Ext.apply(field[0], cfg.field1);
                Ext.apply(field[1], cfg.field2);
                break;
        }
        return field;
    },
    applyFiltersOnEnter: function(field, evt) {
        var me = this;
        if (evt.keyCode == 13) {
            me.applyFilters();
        }
    },
    applyFilters: function() {
        function joinWithQuotes(val) {
            if (val) {
                var l = val.length,
                    x = 0,
                    w,
                    newArr = [];
                if (l) {
                    for (x = 0; x < l; x++) {
                        w = Ext.String.trim(val[x]);
                        w = "'" + w + "'";
                        newArr.push(w);
                    }
                    return newArr;
                }
            }
            return [];
        }
        var me = this,
            fields = me.HFAPI.getFieldsForRow(0),
            l = fields.length,
            x = 0,
            store, proxy, url, proxyCfg, fullUrl, field, filterMode, isHidden, val, otherVal, temp, dataIndex, QueryStr, filterDataType,
            fullQueryStr = "",
            msg = [],
            filterModeMatrix = {
                "string": {
                    "Starts With": "{0} LIKE'{1}%25'",
                    "Ends With": "{0} LIKE'%25{1}'",
                    "Contains": "{0} LIKE'%25{1}%25'",
                    "Equal": "{0} ='{1}'",
                    "IN": "{0} IN({1})"
                },
                "number": {
                    "Equal": "{0} ={1}",
                    "Greater Than": "{0} >={1}",
                    "Lesser Than": "{0} <={1}",
                    "Between": "{0}>={1}&filter={0}<={2}",
                    "IN": "{0} IN({1})"
                },
                "date": {
                    "Between": "{0}>='{1}'&filter={0}<='{2}'",
                    "On": "{0} ='{1}'",
                    "Before": "{0} <='{1}'",
                    "After": "{0} >='{1}'",
                    "IN": "{0} IN({1})"
                },
                "boolean": {
                    "Equal": "{0} ={1}"
                }
            };
        if (fields.length) {
            for (x = 0; x < l; x++) {
                field = fields[x];
                if (!field.isHidden()) {
                    filterMode = field.filterMode;
                    filterDataType = field.filterDataType;
                    val = field.getValue();
                    if (field.xtype.indexOf("datefield") != -1) {
                        val = Ext.Date.format(val, "m/d/Y");
                    }
                    dataIndex = field.up().dataIndex;
                    otherVal = "";
                    var branchMode = filterModeMatrix[filterDataType][filterMode];
                    var greaterBranchMode;
                    var lesserBranchMode;
                    if (filterDataType == "number") {
                        greaterBranchMode = filterModeMatrix[filterDataType]["Greater Than"];
                        lesserBranchMode = filterModeMatrix[filterDataType]["Lesser Than"];
                    }
                    if (filterDataType == "date") {
                        greaterBranchMode = filterModeMatrix[filterDataType]["After"];
                        lesserBranchMode = filterModeMatrix[filterDataType]["Before"];
                    }
                    if (filterMode == "Between") {
                        otherVal = field.next().getValue();
                        if (field.xtype.indexOf("datefield") != -1) {
                            if (val) {
                                val = Ext.Date.format(new Date(val), "m/d/Y");
                            }
                            if (otherVal) {
                                otherVal = Ext.Date.format(new Date(otherVal), "m/d/Y");
                            }
                        }
                        if (val && otherVal) {
                            queryStr = branchMode;
                            queryStr = Ext.String.format(queryStr, dataIndex, val, otherVal);
                        } else {
                            if (val) {
                                queryStr = greaterBranchMode;
                                queryStr = Ext.String.format(queryStr, dataIndex, val);
                            } else {
                                if (otherVal) {
                                    queryStr = lesserBranchMode;
                                    queryStr = Ext.String.format(queryStr, dataIndex, otherVal);
                                } else {}
                            }
                        }
                    } else {
                        if (filterMode == "IN") {
                            val = joinWithQuotes(val);
                            if (val.length >= 1) {
                                queryStr = branchMode;
                                queryStr = Ext.String.format(queryStr, dataIndex, val);
                            }
                        } else {
                            queryStr = branchMode;
                            queryStr = Ext.String.format(queryStr, dataIndex, val);
                        }
                    }
                    if ((val && filterMode != "Between") || ((val || otherVal) && filterMode == "Between")) {
                        if (filterMode != "IN") {
                            if (fullQueryStr === "") {
                                fullQueryStr += ("filter=" + queryStr);
                            } else {
                                fullQueryStr += "&filter=" + queryStr;
                            }
                        } else {
                            if (val.length > 0) {
                                if (fullQueryStr === "") {
                                    fullQueryStr += ("filter=" + queryStr);
                                } else {
                                    fullQueryStr += ("&filter=" + queryStr);
                                }
                            }
                        }
                    }
                }
            }
            store = me.getStore();
            store.removeAll();
            proxy = store.getProxy();
            proxyCfg = proxy.config;
            url = proxyCfg.originalUrl;
            fullUrl = url + "?" + fullQueryStr;
            proxyCfg.url = fullUrl;
            store.setProxy(proxyCfg);
            store.load();
        }
    },
    clearFilters: function(doit) {
        var me = this,
            fieldsArr = me.HFAPI.getAllFields(),
            x, y,
            l = fieldsArr.length,
            store, proxy, proxyCfg, url;
        var field, filtermode;
        for (x = 0; x < l; x++) {
            for (y = 0; y < fieldsArr[x].length; y++) {
                field = fieldsArr[x][y];
                filtermode = null;
                if (field.setValue) {
                    field.setValue(null);
                }
                if (field.resetFilterMode) {
                    field.resetFilterMode(field);
                }
            }
        }
        store = me.getStore();
        store.removeAll();
        proxy = store.getProxy();
        proxyCfg = proxy.config;
        url = proxyCfg.originalUrl;
        proxyCfg.url = url;
        store.setProxy(proxyCfg);
        store.load();
    }
});

Ext.define('Filters2.field.number.MenuViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.numbermenu',
    init: function(view) {
        this.control({
            'menuitem': {
                'click': {
                    fn: function(cmp) {
                        var me = this,
                            allTriggers = [
                                'equal-trigger',
                                'greater-equal-trigger',
                                'lesser-equal-trigger',
                                'between-trigger'
                            ],
                            tips = {
                                'greater-equal-trigger': "Greater Than",
                                'lesser-equal-trigger': "Lesser Than",
                                'between-trigger': "Between",
                                'equal-trigger': "Equal"
                            },
                            el = view.curTrigger.el,
                            x = 0,
                            l = allTriggers.length,
                            nextfield = me.thisField.next();
                        for (x = 0; x < l; x++) {
                            el.removeCls(allTriggers[x]);
                        }
                        el.addCls(cmp.iconCls);
                        me.thisField.filterMode = tips[cmp.iconCls];
                        view.curTrigger.el.dom.setAttribute('data-qtip', tips[cmp.iconCls]);
                        if (cmp.iconCls == "between-trigger") {
                            me.grid.HFAPI.registerField(me.dataIndex);
                            nextfield.setDisabled(false);
                            me.grid.HFAPI.showRow(1);
                        } else {
                            me.grid.HFAPI.unRegisterField(me.dataIndex);
                            nextfield.setValue(null);
                            nextfield.setDisabled(true);
                            if (me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                                me.grid.HFAPI.hideRow(1);
                            }
                        }
                        if (Ext.isNumber(me.thisField.getValue())) {
                            me.thisField.grid.applyFilters();
                        }
                    },
                    scope: view
                }
            }
        });
    }
});

Ext.define('Filters2.field.number.Menu', {
    extend: 'Ext.menu.Menu',
    xtype: 'filters-numberfield-menu',
    requires: [
        'Ext.menu.Item',
        'Filters2.field.number.MenuViewController'
    ],
    controller: "numbermenu",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    initComponent: function() {
        var me = this;
        me.width = 27;
        me.padding = 0;
        me.margin = 0;
        me.items = [
            {
                iconCls: 'equal-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Equal'
            },
            {
                iconCls: 'greater-equal-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: "Greater Than"
            },
            {
                iconCls: 'lesser-equal-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Lesser Than'
            },
            {
                iconCls: 'between-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Between'
            }
        ];
        me.callParent(arguments);
    }
});

Ext.define('Filters2.field.number.Field1', {
    extend: 'Ext.form.field.Text',
    xtype: 'filters-numberfield1',
    requires: [
        'Filters2.field.number.Menu'
    ],
    grid: null,
    gridxtype: null,
    filterMode: "Equal",
    defaultFilterMode: "Equal",
    filterDataType: "number",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            }
        },
        filtermode: {
            cls: 'equal-trigger',
            defaultCls: 'equal-trigger',
            handler: function(field, triggerEl, evt) {
                var dataIndex = field.up().dataIndex,
                    menu = field.grid[field.menuId];
                menu.dataIndex = dataIndex;
                menu.curTrigger = triggerEl;
                menu.thisField = field;
                menu.show();
                menu.alignTo(triggerEl);
            },
            tooltip: 'Equal'
        }
    },
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    initComponent: function() {
        var me = this,
            menuId;
        menuId = "menu_" + me.getId();
        me.gridxtype = me.grid.xtype;
        me.menuId = menuId;
        me.defaultFilterMode = me.filterMode;
        me.grid[menuId] = Ext.create('Filters2.field.number.Menu', {
            grid: me.grid,
            id: menuId,
            filterMode: me.filterMode
        });
        me.listeners = {
            blur: {
                fn: me.grid.applyFilters,
                scope: me.grid
            },
            keypress: {
                fn: me.grid.applyFiltersOnEnter,
                scope: me.grid
            },
            render: {
                fn: function(cmp) {
                    var me = this,
                        trig = cmp.getTrigger('filtermode'),
                        el = trig.el,
                        clearTrig, clearEl,
                        allTriggers = [
                            'equal-trigger',
                            'greater-equal-trigger',
                            'lesser-equal-trigger',
                            'between-trigger'
                        ],
                        cls = {
                            "Equal": "equal-trigger",
                            "Between": "between-trigger",
                            "Lesser Than": "lesser-equal-trigger",
                            "Greater Than": "greater-equal-trigger"
                        },
                        x = 0,
                        l = allTriggers.length,
                        iconCls = cls[me.filterMode],
                        nextfield = me.next(),
                        dataIndex = me.up().dataIndex;
                    for (x = 0; x < l; x++) {
                        el.removeCls(allTriggers[x]);
                    }
                    el.addCls(iconCls);
                    trig.tooltip = me.filterMode;
                    el.dom.setAttribute('data-qtip', trig.tooltip);
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                    if (iconCls == "between-trigger") {
                        me.grid.HFAPI.registerField(dataIndex);
                        nextfield.setDisabled(false);
                        me.grid.HFAPI.showRow(1);
                    } else {
                        me.grid.HFAPI.unRegisterField(dataIndex);
                        nextfield.setValue(null);
                        nextfield.setDisabled(true);
                        if (me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                            me.grid.HFAPI.hideRow(1);
                        }
                    }
                },
                scope: me
            }
        };
        me.callParent(arguments);
    },
    resetFilterMode: function(cmp) {
        var me = this,
            trig = cmp.getTrigger('filtermode'),
            el = trig.el,
            clearTrig, clearEl,
            allTriggers = [
                'equal-trigger',
                'greater-equal-trigger',
                'lesser-equal-trigger',
                'between-trigger'
            ],
            cls = {
                "Equal": "equal-trigger",
                "Between": "between-trigger",
                "Lesser Than": "lesser-equal-trigger",
                "Greater Than": "greater-equal-trigger"
            },
            x = 0,
            l = allTriggers.length,
            iconCls = cls[me.defaultFilterMode],
            nextfield = me.next(),
            dataIndex = me.up().dataIndex;
        for (x = 0; x < l; x++) {
            el.removeCls(allTriggers[x]);
        }
        el.addCls(iconCls);
        trig.tooltip = me.defaultFilterMode;
        el.dom.setAttribute('data-qtip', trig.tooltip);
        clearTrig = cmp.getTrigger('clear');
        clearEl = clearTrig.el;
        clearEl.dom.setAttribute('data-qtip', 'clear field');
        if (iconCls == "between-trigger") {
            me.grid.HFAPI.registerField(dataIndex);
            nextfield.setDisabled(false);
            me.grid.HFAPI.showRow(1);
        } else {
            me.grid.HFAPI.unRegisterField(dataIndex);
            nextfield.setValue(null);
            nextfield.setDisabled(true);
            if (me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                me.grid.HFAPI.hideRow(1);
            }
        }
    }
});

Ext.define('Filters2.field.number.Field2', {
    extend: 'Ext.form.field.Text',
    xtype: 'filters-numberfield2',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            }
        }
    },
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    grid: null,
    initComponent: function() {
        var me = this;
        me.listeners = {
            blur: {
                fn: me.grid.applyFilters,
                scope: me.grid
            },
            keypress: {
                fn: me.grid.applyFiltersOnEnter,
                scope: me.grid
            },
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            }
        };
        me.callParent(arguments);
    }
});

Ext.define('Filters2.field.date.MenuViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.datemenu',
    init: function(view) {
        this.control({
            'menuitem': {
                'click': {
                    fn: function(cmp) {
                        var me = this,
                            allTriggers = [
                                'ondate-trigger',
                                'after-trigger',
                                'before-trigger',
                                'between-trigger'
                            ],
                            tips = {
                                'after-trigger': "After",
                                'before-trigger': "Before",
                                'between-trigger': "Between",
                                'ondate-trigger': "On"
                            },
                            el = view.curTrigger.el,
                            x = 0,
                            l = allTriggers.length,
                            nextfield = me.thisField.next();
                        for (x = 0; x < l; x++) {
                            el.removeCls(allTriggers[x]);
                        }
                        el.addCls(cmp.iconCls);
                        me.thisField.filterMode = tips[cmp.iconCls];
                        view.curTrigger.el.dom.setAttribute('data-qtip', tips[cmp.iconCls]);
                        if (cmp.iconCls == "between-trigger") {
                            me.grid.HFAPI.registerField(me.dataIndex);
                            nextfield.setDisabled(false);
                            me.grid.HFAPI.showRow(1);
                        } else {
                            me.grid.HFAPI.unRegisterField(me.dataIndex);
                            nextfield.setValue(null);
                            nextfield.setDisabled(true);
                            if (me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                                me.grid.HFAPI.hideRow(1);
                            }
                        }
                        if (me.thisField.getValue()) {
                            me.thisField.grid.applyFilters();
                        }
                    },
                    scope: view
                }
            }
        });
    }
});

Ext.define('Filters2.field.date.Menu', {
    extend: 'Ext.menu.Menu',
    xtype: 'filters-datefield-menu',
    requires: [
        'Ext.menu.Item',
        'Filters2.field.date.MenuViewController'
    ],
    controller: "datemenu",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    initComponent: function() {
        var me = this;
        me.width = 27;
        me.padding = 0;
        me.margin = 0;
        me.items = [
            {
                iconCls: 'ondate-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'On'
            },
            {
                iconCls: 'after-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: "After"
            },
            {
                iconCls: 'before-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Before'
            },
            {
                iconCls: 'between-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Between'
            }
        ];
        me.callParent(arguments);
    }
});

Ext.define('Filters2.field.date.Field1', {
    extend: 'Ext.form.field.Date',
    xtype: 'filters-datefield1',
    requires: [
        'Filters2.field.date.Menu'
    ],
    grid: null,
    gridxtype: null,
    filterMode: "On",
    defaultFilterMode: "On",
    filterDataType: "date",
    editable: false,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            },
            weight: 0
        },
        filtermode: {
            cls: 'ondate-trigger',
            defaultCls: 'ondate-trigger',
            handler: function(field, triggerEl, evt) {
                var dataIndex = field.up().dataIndex,
                    menu = field.grid[field.menuId];
                menu.dataIndex = dataIndex;
                menu.curTrigger = triggerEl;
                menu.thisField = field;
                menu.show();
                menu.alignTo(triggerEl);
            },
            tooltip: 'On',
            weight: 1
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
        }
    },
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    initComponent: function() {
        var me = this,
            menuId;
        menuId = "menu_" + me.getId();
        me.gridxtype = me.grid.xtype;
        me.menuId = menuId;
        me.defaultFilterMode = me.filterMode;
        me.grid[menuId] = Ext.create('Filters2.field.date.Menu', {
            grid: me.grid,
            id: menuId,
            filterMode: me.filterMode
        });
        me.listeners = {
            expand: {
                fn: function(cmp) {
                    cmp.picker.setWidth(212);
                },
                scope: me
            },
            select: {
                fn: function() {
                    this.grid.applyFilters();
                },
                scope: me
            },
            render: {
                fn: function(cmp) {
                    var me = this,
                        trig = cmp.getTrigger('filtermode'),
                        el = trig.el,
                        clearTrig, clearEl,
                        allTriggers = [
                            'ondate-trigger',
                            'after-trigger',
                            'before-trigger',
                            'between-trigger'
                        ],
                        cls = {
                            "On": "ondate-trigger",
                            "Between": "between-trigger",
                            "Before": "before-trigger",
                            "After": "after-trigger"
                        },
                        x = 0,
                        l = allTriggers.length,
                        iconCls = cls[me.filterMode],
                        nextfield = me.next(),
                        dataIndex = me.up().dataIndex;
                    for (x = 0; x < l; x++) {
                        el.removeCls(allTriggers[x]);
                    }
                    el.addCls(iconCls);
                    trig.tooltip = me.filterMode;
                    el.dom.setAttribute('data-qtip', trig.tooltip);
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                    if (iconCls == "between-trigger") {
                        me.grid.HFAPI.registerField(dataIndex);
                        nextfield.setDisabled(false);
                        me.grid.HFAPI.showRow(1);
                    } else {
                        me.grid.HFAPI.unRegisterField(dataIndex);
                        nextfield.setValue(null);
                        nextfield.setDisabled(true);
                        if (me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                            me.grid.HFAPI.hideRow(1);
                        }
                    }
                },
                scope: me
            }
        };
        me.callParent(arguments);
    },
    resetFilterMode: function(cmp) {
        var me = this,
            trig = cmp.getTrigger('filtermode'),
            el = trig.el,
            clearTrig, clearEl,
            allTriggers = [
                'ondate-trigger',
                'after-trigger',
                'before-trigger',
                'between-trigger'
            ],
            cls = {
                "On": "ondate-trigger",
                "Between": "between-trigger",
                "Before": "before-trigger",
                "After": "after-trigger"
            },
            x = 0,
            l = allTriggers.length,
            iconCls = cls[me.defaultFilterMode],
            nextfield = me.next(),
            dataIndex = me.up().dataIndex;
        for (x = 0; x < l; x++) {
            el.removeCls(allTriggers[x]);
        }
        el.addCls(iconCls);
        trig.tooltip = me.defaultFilterMode;
        el.dom.setAttribute('data-qtip', trig.tooltip);
        clearTrig = cmp.getTrigger('clear');
        clearEl = clearTrig.el;
        clearEl.dom.setAttribute('data-qtip', 'clear field');
        if (iconCls == "between-trigger") {
            me.grid.HFAPI.registerField(dataIndex);
            nextfield.setDisabled(false);
            me.grid.HFAPI.showRow(1);
        } else {
            me.grid.HFAPI.unRegisterField(dataIndex);
            nextfield.setValue(null);
            nextfield.setDisabled(true);
            if (me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                me.grid.HFAPI.hideRow(1);
            }
        }
    }
});

Ext.define('Filters2.field.date.Field2', {
    extend: 'Ext.form.field.Date',
    xtype: 'filters-datefield2',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            },
            weight: 1
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
        }
    },
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    editable: false,
    initComponent: function() {
        var me = this;
        me.listeners = {
            expand: {
                fn: function(cmp) {
                    cmp.picker.setWidth(212);
                },
                scope: me
            },
            select: {
                fn: function() {
                    this.grid.applyFilters();
                },
                scope: me
            },
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            }
        };
        me.callParent(arguments);
    }
});

Ext.define('Filters2.field.string.MenuViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.stringmenu',
    init: function(view) {
        this.control({
            'menuitem': {
                'click': {
                    fn: function(cmp) {
                        var me = this,
                            allTriggers = [
                                'textequal-trigger',
                                'starts-with-trigger',
                                'ends-with-trigger',
                                'contains-trigger'
                            ],
                            tips = {
                                'starts-with-trigger': "Starts With",
                                'ends-with-trigger': "Ends With",
                                'contains-trigger': "Contains",
                                'textequal-trigger': "Equal"
                            },
                            el = view.curTrigger.el,
                            x = 0,
                            l = allTriggers.length;
                        for (x = 0; x < l; x++) {
                            el.removeCls(allTriggers[x]);
                        }
                        el.addCls(cmp.iconCls);
                        me.thisField.filterMode = tips[cmp.iconCls];
                        view.curTrigger.el.dom.setAttribute('data-qtip', tips[cmp.iconCls]);
                        if (me.thisField.getValue()) {
                            me.thisField.grid.applyFilters();
                        }
                    },
                    scope: view
                }
            }
        });
    }
});

Ext.define('Filters2.field.string.Menu', {
    extend: 'Ext.menu.Menu',
    xtype: 'filters-stringfield-menu',
    requires: [
        'Ext.menu.Item',
        'Filters2.field.string.MenuViewController'
    ],
    controller: "stringmenu",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    initComponent: function() {
        var me = this;
        me.width = 27;
        me.padding = 0;
        me.margin = 0;
        me.items = [
            {
                iconCls: 'textequal-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Equal'
            },
            {
                iconCls: 'starts-with-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: "Starts With"
            },
            {
                iconCls: 'ends-with-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Ends With'
            },
            {
                iconCls: 'contains-trigger',
                xtype: 'menuitem',
                width: 27,
                height: 25,
                text: 'Contains'
            }
        ];
        me.callParent(arguments);
    }
});

Ext.define('Filters2.field.string.Field', {
    extend: 'Ext.form.field.Text',
    xtype: 'filters-stringfield',
    requires: [
        'Filters2.field.string.Menu'
    ],
    grid: null,
    gridxtype: null,
    filterMode: "Equal",
    defaultFilterMode: "Equal",
    filterDataType: "string",
    typeAheadDelay: 500,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            }
        },
        filtermode: {
            cls: 'textequal-trigger',
            defaultCls: 'textequal-trigger',
            handler: function(field, triggerEl, evt) {
                var dataIndex = field.up().dataIndex,
                    menu = field.grid[field.menuId];
                menu.dataIndex = dataIndex;
                menu.curTrigger = triggerEl;
                menu.thisField = field;
                menu.show();
                menu.alignTo(triggerEl);
            },
            tooltip: 'Equal'
        }
    },
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    initComponent: function() {
        var me = this,
            menuId;
        menuId = "menu_" + me.getId();
        me.gridxtype = me.grid.xtype;
        me.menuId = menuId;
        me.defaultFilterMode = me.filterMode;
        me.grid[menuId] = Ext.create('Filters2.field.string.Menu', {
            grid: me.grid,
            id: menuId,
            filterMode: me.filterMode
        });
        me.listeners = {
            change: {
                fn: Ext.Function.createBuffered(function(field) {
                    field.up('grid').applyFilters();
                }, me.typeAheadDelay, this)
            },
            render: {
                fn: function(cmp) {
                    var me = this,
                        trig = cmp.getTrigger('filtermode'),
                        el = trig.el,
                        clearTrig, clearEl,
                        allTriggers = [
                            'textequal-trigger',
                            'starts-with-trigger',
                            'ends-with-trigger',
                            'contains-trigger'
                        ],
                        cls = {
                            "Equal": "textequal-trigger",
                            "Contains": "contains-trigger",
                            "Starts With": "starts-with-trigger",
                            "Ends With": "ends-with-trigger"
                        },
                        x = 0,
                        l = allTriggers.length,
                        iconCls = cls[me.filterMode],
                        dataIndex = me.up().dataIndex;
                    for (x = 0; x < l; x++) {
                        el.removeCls(allTriggers[x]);
                    }
                    el.addCls(iconCls);
                    trig.tooltip = me.filterMode;
                    el.dom.setAttribute('data-qtip', trig.tooltip);
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                },
                scope: me
            }
        };
        me.callParent(arguments);
    },
    resetFilterMode: function(cmp) {
        var me = this,
            trig = cmp.getTrigger('filtermode'),
            el = trig.el,
            clearTrig, clearEl,
            allTriggers = [
                'textequal-trigger',
                'starts-with-trigger',
                'ends-with-trigger',
                'contains-trigger'
            ],
            cls = {
                "Equal": "textequal-trigger",
                "Contains": "contains-trigger",
                "Starts With": "starts-with-trigger",
                "Ends With": "ends-with-trigger"
            },
            x = 0,
            l = allTriggers.length,
            iconCls = cls[me.defaultFilterMode],
            dataIndex = me.up().dataIndex;
        for (x = 0; x < l; x++) {
            el.removeCls(allTriggers[x]);
        }
        el.addCls(iconCls);
        trig.tooltip = me.defaultFilterMode;
        el.dom.setAttribute('data-qtip', trig.tooltip);
        clearTrig = cmp.getTrigger('clear');
        clearEl = clearTrig.el;
        clearEl.dom.setAttribute('data-qtip', 'clear field');
    }
});

Ext.define('Filters2.field.genericlist.Field', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
        'Ext.data.Store'
    ],
    xtype: 'filters-genericlistfield',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    triggers: {
        clear: {
            weight: 1,
            cls: 'x-form-clear-trigger',
            handler: function() {
                this.setValue(null);
                this.grid.applyFilters();
            },
            scope: 'this'
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
        }
    },
    inlineData: null,
    enableKeyEvents: true,
    cls: "fieldFilterCls",
    margin: "0",
    filterMode: "Equal",
    filterDataType: "string",
    initComponent: function() {
        var me = this;
        var inlineData = me.inlineData;
        if (Ext.isFunction(me.inlineData)) {
            inlineData = [];
        }
        me.store = Ext.create('Ext.data.Store', {
            fields: [
                {
                    name: 'display',
                    type: 'string'
                },
                {
                    name: 'val',
                    type: 'string'
                }
            ],
            data: inlineData
        });
        me.editable = false;
        me.valueField = "val";
        me.displayField = "display";
        me.queryMode = 'local';
        me.tpl = Ext.create('Ext.XTemplate', '<ul class="x-list-plain"><tpl for=".">', '<li role="option" class="x-boundlist-item">{display}</li>', '</tpl></ul>');
        me.displayTpl = Ext.create('Ext.XTemplate', '<tpl for=".">', '{display}', '</tpl>');
        me.listeners = {
            afterrender: {
                fn: function(cmp) {
                    var me = this;
                    if (Ext.isFunction(me.inlineData)) {
                        Ext.Function.bind(me.inlineData, me)();
                    }
                },
                scope: me
            },
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            },
            select: {
                fn: function(cmp) {
                    me.grid.applyFilters();
                },
                scope: me
            }
        };
        me.callParent(arguments);
    }
});

Ext.define('Filters2.field.combocheckbox.Field', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
        'Ext.data.Store'
    ],
    xtype: 'filters-combocheckboxfield',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    triggers: {
        clear: {
            weight: 1,
            cls: 'x-form-clear-trigger',
            handler: function(field) {
                field.setValue(null);
                field.grid.applyFilters();
            },
            scope: 'this'
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
        }
    },
    inlineData: null,
    enableKeyEvents: true,
    cls: "fieldFilterCls",
    margin: "0",
    filterMode: "IN",
    filterDataType: "string",
    onCollapse: function() {
        var me = this;
        me.grid.applyFilters();
        me.callSuper(arguments);
    },
    initComponent: function() {
        var me = this;
        var inlineData = me.inlineData;
        if (Ext.isFunction(me.inlineData)) {
            inlineData = [];
        }
        me.store = Ext.create('Ext.data.Store', {
            fields: [
                {
                    name: 'display',
                    type: 'string'
                },
                {
                    name: 'val',
                    type: 'string'
                }
            ],
            data: inlineData
        });
        me.editable = false;
        me.valueField = "val";
        me.displayField = "display";
        me.queryMode = 'local';
        me.multiSelect = true;
        me._checkField = 'checked';
        me.tpl = new Ext.XTemplate('<tpl for=".">', '<div class="x-boundlist-item">', '<tpl if="values.checked">', '<div class="nowrap">', '<input type="checkbox" checked="checked" />', // '<input type="checkbox" checked="checked" disabled="disabled" />',
        ' {', this.displayField, '}', '</div>', '</tpl>', '<tpl if="!values.checked">', '<div class="nowrap">', // '<input type="checkbox" disabled="disabled" />',
        '<input type="checkbox" />', ' {', this.displayField, '}', '</div>', '</tpl>', '</div>', '</tpl>');
        me.displayTpl = Ext.create('Ext.XTemplate', '{[this.setDisplayValues(values, xcount, xindex, xkey)]}', {
            setDisplayValues: function(values, xcount, xindex, xkey) {
                var x = 0;
                var l;
                var arr = [];
                if (values && values.length > 0) {
                    l = values.length;
                    for (x = 0; x < l; x++) {
                        if (values[x].checked === 1) {
                            arr.push(values[x].display);
                        }
                    }
                }
                return arr.join(", ");
            }
        });
        me.listeners = {
            afterrender: {
                fn: function(cmp) {
                    var me = this;
                    if (Ext.isFunction(me.inlineData)) {
                        Ext.Function.bind(me.inlineData, me)();
                    }
                },
                scope: me
            },
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            },
            change: function(combo, n) {
                combo.store.each(function(item) {
                    item.set(combo._checkField, 0);
                });
                if (n && n.forEach) {
                    n.forEach(function(item) {
                        combo.store.findRecord(combo.valueField, item, 0, false, true, true).set(combo._checkField, 1);
                    });
                }
            }
        };
        me.callParent(arguments);
    }
});

Ext.define('Filters2.HeaderFields', {
    extend: 'Ext.grid.feature.Feature',
    alias: 'feature.headerfields',
    requires: [
        'Ext.form.field.*',
        'Filters2.field.number.Field1',
        'Filters2.field.number.Field2',
        'Filters2.field.date.Field1',
        'Filters2.field.date.Field2',
        'Filters2.field.string.Field',
        'Filters2.field.genericlist.Field',
        'Filters2.field.combocheckbox.Field'
    ],
    eventPreventHandler: function(e) {
        e.stopPropagation();
    },
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    cfg: null,
    init: function(grid) {
        var me = this,
            callback = null;
        var hidden = me.cfg.hidden;
        var columns, x, l;
        if (hidden) {
            Ext.suspendLayouts();
            columns = grid.columns;
            l = columns.length;
            for (x = 0; x < l; x++) {
                columns[x].down('component').hide();
            }
            Ext.resumeLayouts();
        }
        if (!grid.listeners) {
            grid.listeners = {};
        } else {
            if (grid.listeners.columnresize) {
                if (grid.listeners.columnresize.fn) {
                    if (grid.listeners.columnresize.scope) {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid.listeners.columnresize.scope);
                    } else {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid);
                    }
                } else {
                    callback = Ext.Function.bind(grid.listeners.columnresize, grid);
                }
            }
        }
        me.setAPI(grid);
        Ext.apply(grid.listeners, {
            'columnresize': {
                fn: Ext.Function.bind(me.onColumnResizeFunc, grid, [
                    callback
                ], true),
                scope: me
            }
        });
    },
    onColumnResizeFunc: function(headerCt, targetColumn, width, opts, callback) {
        var me = this,
            items = Ext.ComponentQuery.query("component", targetColumn),
            x,
            l = items.length,
            el;
        for (x = 0; x < l; x++) {
            items[x].setWidth(width);
            // CG: 05/14/2016 - HACK!
            el = items[x].getEl().select('.x-form-trigger-wrap');
            console.log(el.elements.length);
            if (el.elements.length > 0) {
                Ext.get(el.elements[0]).setWidth(width);
            }
        }
        if (callback) {
            callback();
        }
    },
    setAPI: function(grid) {
        var init = Ext.Function.bind(function() {
                var grid = this;
                var result = [];
                var rows = [];
                var columns = grid.getColumns();
                var x,
                    l = columns.length,
                    column, obj;
                var multipleRows = false;
                var fieldRegistrationObj = {};
                for (x = 0; x < l; x++) {
                    column = columns[x];
                    obj = Ext.ComponentQuery.query('component', column);
                    result.push(obj);
                    for (y = 0; y < obj.length; y++) {
                        if (obj[y].triggers && obj[y].triggers.filtermode) {
                            if (obj[y].triggers.filtermode.cls == "between-trigger") {
                                multipleRows = true;
                            }
                        }
                        if (!Ext.isArray(rows[y])) {
                            rows[y] = [];
                        }
                        
                        if (obj[y].componentCls == "x-field") {
                            rows[y].push(obj[y]);
                        }
                    }
                }
                
                return {
                    getField: function(dataIndex, idx) {
                        var column = grid.down('gridcolumn[dataIndex="' + dataIndex + '"]');
                        var obj = Ext.ComponentQuery.query('field', column);
                        if (obj && obj.length > -1 && Ext.isNumber(idx) && (idx < obj.length)) {
                            return obj[idx];
                        }
                        return obj;
                    },
                    getFields: function(dataIndexArr) {
                        var result = [];
                        var x, l, column;
                        if (Ext.isArray(dataIndexArr) && dataIndexArr.length > 0) {
                            l = dataIndexArr.length;
                            for (x = 0; x < l; x++) {
                                result.push(grid.HFAPI.getField(dataIndexArr[x]));
                            }
                        }
                        return result;
                    },
                    getAllFields: function() {
                        return result;
                    },
                    getFieldsForRow: function(r) {
                        var x, l;
                        if (rows[r]) {
                            return rows[r];
                        }
                        return [];
                    },
                    hideRow: function(r) {
                        var x, l;
                        if (rows[r]) {
                            l = rows[r].length;
                            for (x = 0; x < l; x++) {
                                if (rows[r][x]) {
                                    rows[r][x].hide();
                                }
                            }
                        }
                    },
                    showRow: function(r) {
                        var x, l;
                        if (rows[r]) {
                            l = rows[r].length;
                            for (x = 0; x < l; x++) {
                                if (rows[r][x]) {
                                    rows[r][x].show();
                                }
                            }
                        }
                    },
                    getMultipleRows: function() {
                        return multipleRows;
                    },
                    getFieldRegistrationSize: function() {
                        return Ext.Object.getSize(fieldRegistrationObj);
                    },
                    registerField: function(field) {
                        fieldRegistrationObj[field] = true;
                    },
                    unRegisterField: function(field) {
                        delete fieldRegistrationObj[field];
                    },
                    getFieldRegistrationObj: function() {
                        return fieldRegistrationObj;
                    }
                };
            }, grid);
        grid.HFAPI = grid.HeaderFieldsAPI = init();
    }
});

Ext.define('Filters2.InjectHeaderFields', {
    // hideable: true, // or false // if its not hideable, then the toolbar doesn't appear on the right side of the grid (defaults to false)
    // hidden: false // or true // this is ignored when hideable is set to false (defaults to false)
    // position: "inner" or "outer" // defaults to inner
    // dockPosition: "right, top"
    headerFieldsConfig: {
        useToolbar: true,
        hideable: false,
        hidden: false,
        position: "inner",
        dockPosition: 'right',
        backgroundColor: "#f5f5f5",
        borderColor: "#c1c1c1",
        buttons: null,
        extraButtons: null
    },
    getHeaderFieldsConfig: function() {
        return this.headerFieldsConfig;
    },
    injectHeaderFields: function(obj) {
        var me = this,
            hideable, hidden, position, backgroundColor, borderColor, useToolbar, dockPosition, extraButtons, columns, x, l, y, ll,
            itemsCount = 0,
            column, z, rbarItem, HFToolbar,
            toolbars = [],
            buttons, z, lll;
        if (obj && Ext.isObject(obj) && Ext.Object.getSize(obj) > 0) {
            Ext.apply(me.headerFieldsConfig, obj);
        }
        extraButtons = me.headerFieldsConfig.extraButtons;
        dockPosition = me.headerFieldsConfig.dockPosition;
        useToolbar = me.headerFieldsConfig.useToolbar;
        hideable = me.headerFieldsConfig.hideable;
        hidden = me.headerFieldsConfig.hidden;
        position = me.headerFieldsConfig.position;
        backgroundColor = me.headerFieldsConfig.backgroundColor;
        borderColor = me.headerFieldsConfig.borderColor;
        buttons = me.headerFieldsConfig.buttons;
        // this needs to happen
        if (Ext.isArray(me.columns)) {
            columns = me.columns;
            if (Ext.isObject(me.defaults)) {
                l = columns.length;
                for (x = 0; x < l; x++) {
                    if (columns[x].headerField) {
                        Ext.applyIf(columns[x], me.defaults);
                    } else {
                        Ext.apply(columns[x], me.defaults);
                    }
                    if (columns[x].headerField) {
                        itemsCount = Math.max(itemsCount, columns[x].headerField.length);
                    }
                }
            }
        } else {
            l = me.columns.items.length;
            columns = [];
            for (x = 0; x < l; x++) {
                if (me.columns[x].headerField) {
                    Ext.applyIf(me.columns.items[x], me.columns.defaults);
                } else {
                    Ext.apply(me.columns.items[x], me.columns.defaults);
                }
                columns.push(me.columns.items[x]);
                if (columns[x].headerField) {
                    itemsCount = Math.max(itemsCount, columns[x].headerField.length);
                }
            }
        }
        for (x = 0; x < l; x++) {
            column = columns[x];
            ll = (column.headerField) ? column.headerField.length : 0;
            column.items = [];
            if (ll == 0) {
                for (y = 0; y < itemsCount; y++) {
                    column.items.push({
                        xtype: 'label'
                    });
                }
            } else {
                if (ll == itemsCount) {
                    for (y = 0; y < ll; y++) {
                        column.items.push(column.headerField[y]);
                    }
                } else {
                    for (y = 0; y < ll; y++) {
                        column.items.push(column.headerField[y]);
                    }
                    for (z = y; z < itemsCount; z++) {
                        column.items.push({
                            xtype: 'label'
                        });
                    }
                }
            }
        }
        me.columns = columns;
        // toolbar is optional        
        if (useToolbar) {
            if (hideable == false) {
                hidden = false;
            }
            var collapseExpandButton = {
                    xtype: 'button',
                    iconCls: (hidden) ? "filters-toolbar-expand" : "filters-toolbar-collapse",
                    tooltip: (hidden) ? "Expand Filter Bar" : "Collapse Filter Bar",
                    rowHiddenState: hidden,
                    listeners: {
                        click: {
                            fn: function(btn) {
                                var t, tp, tb, items, x, l, multi;
                                btn.rowHiddenState = !btn.rowHiddenState;
                                t = (btn.rowHiddenState) ? "filters-toolbar-expand" : "filters-toolbar-collapse";
                                btn.setIconCls(t);
                                tp = (btn.rowHiddenState) ? "Expand Filter Bar" : "Collapse Filter Bar";
                                btn.setTooltip(tp);
                                tb = btn.up('toolbar');
                                items = tb.items.items;
                                l = items.length;
                                me.HFAPI.rowHiddenState = btn.rowHiddenState;
                                multi = me.HFAPI.getFieldRegistrationSize();
                                me.clearFilters();
                                if (btn.rowHiddenState) {
                                    if (multi) {
                                        me.HFAPI.hideRow(1);
                                    }
                                    me.HFAPI.hideRow(0);
                                } else {
                                    me.HFAPI.showRow(0);
                                    if (multi) {
                                        me.HFAPI.showRow(1);
                                    }
                                }
                            },
                            scope: me
                        }
                    }
                };
            var placeHolder = {
                    xtype: 'box',
                    width: 16,
                    height: 24,
                    html: '&nbsp;'
                };
            var mainButton;
            if (dockPosition == "right") {
                if (hideable) {
                    collapseExpandButton.dockPosition = dockPosition;
                    mainButton = collapseExpandButton;
                } else {
                    mainButton = placeHolder;
                }
                HFToolbar = {
                    xtype: 'toolbar',
                    dock: dockPosition,
                    padding: "2",
                    style: ("background-color:" + backgroundColor + ";border:1px solid " + borderColor + "!important;"),
                    items: [
                        mainButton,
                        {
                            xtype: "tbseparator",
                            hidden: hidden
                        }
                    ]
                };
                if (buttons) {
                    lll = buttons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(buttons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, buttons);
                    if (extraButtons) {
                        HFToolbar.items.push({
                            xtype: "tbseparator",
                            hidden: hidden
                        });
                    }
                }
                if (extraButtons) {
                    lll = extraButtons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(extraButtons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, extraButtons);
                }
                if (me.rbar) {
                    rbarItem = me.rbar;
                    me.rbar = null;
                }
                if (!me.hasDockedItems()) {
                    me.dockedItems = [];
                }
                if (rbarItem) {
                    me.dockedItems.push(Ext.apply(rbarItem, {
                        dock: 'right'
                    }));
                }
                if (position === "inner") {
                    me.dockedItems.push(HFToolbar);
                } else {
                    me.dockedItems.unshift(HFToolbar);
                }
            } else {
                HFToolbar = {
                    xtype: 'toolbar',
                    dock: dockPosition,
                    padding: "2",
                    style: ("background-color:" + backgroundColor + ";border:1px solid " + borderColor + "!important;"),
                    items: [
                        "->"
                    ]
                };
                if (extraButtons) {
                    lll = extraButtons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(extraButtons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, extraButtons);
                    if (buttons || hideable) {
                        HFToolbar.items.push({
                            xtype: 'tbseparator',
                            hidden: hidden
                        });
                    }
                }
                if (buttons) {
                    lll = buttons.length;
                    for (z = 0; z < lll; z++) {
                        Ext.apply(buttons[z], {
                            hidden: hidden
                        });
                    }
                    Ext.Array.push(HFToolbar.items, buttons);
                    if (hideable) {
                        HFToolbar.items.push({
                            xtype: 'tbseparator',
                            hidden: hidden
                        });
                    }
                }
                if (hideable) {
                    collapseExpandButton.dockPosition = dockPosition;
                    HFToolbar.items.push(collapseExpandButton);
                }
                if (me.tbar) {
                    rbarItem = me.tbar;
                    me.tbar = null;
                }
                if (!me.hasDockedItems()) {
                    me.dockedItems = [];
                }
                if (rbarItem) {
                    me.dockedItems.push(Ext.apply(rbarItem, {
                        dock: 'right'
                    }));
                }
                if (position === "inner") {
                    me.dockedItems.push(HFToolbar);
                } else {
                    me.dockedItems.unshift(HFToolbar);
                }
            }
        }
        return {
            hidden: hidden
        };
    },
    hasDockedItems: function() {
        var me = this;
        if (me.dockedItems && me.dockedItems.length > 0) {
            return true;
        }
        return false;
    }
});

