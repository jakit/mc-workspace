Ext.define('Filters2.FiltersMixin', {
    filterButtons: [{
        xtype: 'button',
        iconCls: "filters-toolbar-apply-filter",
        tooltip: "Apply Filters",
        listeners: {
            "click": {
                fn: function(btn) {
                    btn.up('grid').applyFilters();
                }
            }
        }
    }, {
        xtype: 'button',
        iconCls: "filters-toolbar-clear-filter",
        tooltip: "Clear Filters",
        listeners: {
            "click": {
                fn: function(btn) {
                    var grid = btn.up('grid');
                    btn.up('grid').clearFilters.apply(grid, [false]);
                }
            }
        }
    }],
    
    defaultField: {
        xtype: 'textfield',
        enableKeyEvents: true,
        cls: "fieldFilterCls",
        margin: "0"
        // listeners: {
            // change: {
// //                fn: Ext.Function.createBuffered(function(field) {field.up('grid').applyFilters();}, 250, this)
            // }
        // }
    },
    
    setFilterField: function(type, cfg) {

        var fieldTypes = {
            "genericlistfield": [{
                xtype: 'filters-genericlistfield'
            }],
            "combocheckboxfield": [{
                xtype: 'filters-combocheckboxfield'
            }],
            "stringfield": [{
                xtype: 'filters-stringfield'
            }],
            "numberfield": [{
                xtype: 'filters-numberfield1'
            }, {
                xtype: 'filters-numberfield2'
            }],
            "datefield": [{
                xtype: 'filters-datefield1'
            }, {
                xtype: 'filters-datefield2'
            }]          
        };
                
        var field = Ext.clone(fieldTypes[type]);
        
        switch (type) {
            case "genericlistfield":
            case "combocheckboxfield":
            case "stringfield":
                Ext.apply(field[0], cfg);
            break;
            
            case "numberfield":
            case "datefield": 
                Ext.apply(field[0], cfg.field1);
                Ext.apply(field[1], cfg.field2);
            break;
        }

        return field;
        
    },
    applyFiltersOnEnter: function(field, evt) {
        var me = this;
        if(evt.keyCode == 13) {
            me.applyFilters();
        }        
    },
    applyFilters: function() {
		console.log("Apply Filters");
        function joinWithQuotes(val) {
            if(val) {
                var l = val.length,
                    x = 0, w, newArr = [];
                if(l) {
                    for(x = 0; x < l; x++) {
                       w = Ext.String.trim(val[x]);
                       w = "'" + w + "'";    
                       newArr.push(w);
                    }
                    return newArr;
                }
            }
            return [];
        } 

        var me = this,
            fields = me.HFAPI.getFieldsForRow(0),
            l = fields.length,
            x = 0,
            store, proxy, url, proxyCfg, fullUrl,
            field, filterMode, isHidden, val, otherVal, temp, dataIndex,  QueryStr, filterDataType, fullQueryStr = "",
            msg = [],
            filterModeMatrix = {
                "string": {
                    "Starts With": "{0} LIKE'{1}%25'", 
                    "Ends With": "{0} LIKE'%25{1}'", 
                    "Contains": "{0} LIKE'%25{1}%25'", 
                    "Equal": "{0} ='{1}'", 
                    "IN": "{0} IN({1})" 
                    
                },
                "number": {
                    "Equal": "{0} ={1}", 
                    "Greater Than": "{0} >={1}", 
                    "Lesser Than": "{0} <={1}", 
                    "Between": "{0}>={1}&filter={0}<={2}", 
                    "IN": "{0} IN({1})"
                },
                "date": {
                    "Between": "{0}>='{1}'&filter={0}<='{2}'", 
                    "On": "{0} ='{1}'",
                    "Before": "{0} <='{1}'",
                    "After": "{0} >='{1}'",
                    "IN": "{0} IN({1})"
                },
                "boolean": {
                    "Equal": "{0} ={1}"
                }
             };

        if(fields.length) {
            for(x = 0; x < l; x++) {
                field = fields[x];
                
                if(!field.isHidden()) {
                    filterMode = field.filterMode;
                    filterDataType = field.filterDataType;
                    val = field.getValue();
                    if(field.xtype.indexOf("datefield") != -1) {
                        val = Ext.Date.format(val, "m/d/Y");
                    }
                    dataIndex = field.up().dataIndex;
                    otherVal = "";
                    var branchMode = filterModeMatrix[filterDataType][filterMode];
                    
                    var greaterBranchMode;
                    var lesserBranchMode;
                    
                    if(filterDataType == "number") {
                        greaterBranchMode = filterModeMatrix[filterDataType]["Greater Than"];
                        lesserBranchMode = filterModeMatrix[filterDataType]["Lesser Than"];
                        
                    }
                    
                    if(filterDataType == "date") {
                        greaterBranchMode = filterModeMatrix[filterDataType]["After"];
                        lesserBranchMode = filterModeMatrix[filterDataType]["Before"];
                    }
                     
                    
                    if(filterMode == "Between") {
                        otherVal = field.next().getValue();
                        
                        if(field.xtype.indexOf("datefield") != -1) {
                            if(val) {
                                val = Ext.Date.format(new Date(val), "m/d/Y");
                            }
                            if(otherVal) {
                                otherVal = Ext.Date.format(new Date(otherVal), "m/d/Y");    
                            }
                        } 
                        if(val && otherVal) {
                            queryStr = branchMode;
                            queryStr = Ext.String.format(queryStr, dataIndex, val, otherVal);
                        } else {
                            if(val) {
                                queryStr = greaterBranchMode;
                                queryStr = Ext.String.format(queryStr, dataIndex, val);
                            } else {
                                if(otherVal) {
                                    queryStr = lesserBranchMode;
                                    queryStr = Ext.String.format(queryStr, dataIndex, otherVal);
                                } else {

                                    
                                }
                            }
                        }
                        
                    } else {
                        
                        if(filterMode == "IN") {
                            val = joinWithQuotes(val);
                            if(val.length >= 1) {
                                queryStr = branchMode;
                                queryStr = Ext.String.format(queryStr, dataIndex, val);    
                            }
                        } else {
                            queryStr = branchMode;
                            queryStr = Ext.String.format(queryStr, dataIndex, val);
                        }
                    }
                    
                   
                    
                    if((val && filterMode != "Between") || ((val || otherVal) && filterMode == "Between")) {
                        if(filterMode != "IN") {
                            if(fullQueryStr === "") {
                                fullQueryStr += ("filter=" + queryStr);
                            } else {
                                fullQueryStr += "&filter=" + queryStr;
                            }   
                        } else {
                            if(val.length > 0) {
                                if(fullQueryStr === "") {
                                    fullQueryStr += ("filter=" + queryStr);
                                } else {
                                    fullQueryStr += ("&filter=" + queryStr);
                                }   
                            }
                        }
                    }
                }
            }
            
            
            store = me.getStore();
            store.removeAll();
            proxy = store.getProxy();
            proxyCfg = proxy.config;
            url = proxyCfg.originalUrl;
            fullUrl = url + "?" + fullQueryStr;
            proxyCfg.url = fullUrl;
            store.setProxy(proxyCfg);
            store.load();            
        }
    },
    clearFilters: function(doit) {
        console.log("Clear Filters");
        var me = this,
            fieldsArr = me.HFAPI.getAllFields(),
            x, y, l = fieldsArr.length,
            store, proxy, proxyCfg, url;
            var field, filtermode;
            
        for(x = 0; x < l; x++) {
            for(y = 0; y < fieldsArr[x].length; y++) {
                field = fieldsArr[x][y];
                filtermode = null;
                if(field.setValue) {
                    field.setRawValue(null);
					//New For Clear Filter in Combo Check box Field
					if(field.xtype.indexOf("combocheckboxfield") != -1) {						
                        field.store.each(function (item) {
							item.set(field._checkField, 0);
						});
						if(field.picker){
							field.picker.getSelectionModel().deselectAll();
						}
                    }
					////
                }
				//console.log(field.resetFilterMode);
                if(field.resetFilterMode) {
                    field.resetFilterMode(field);
                }
            }
        }
    
        store = me.getStore();
        store.removeAll();
        proxy = store.getProxy();
        proxyCfg = proxy.config;
        url = proxyCfg.originalUrl;
        proxyCfg.url = url;
        store.setProxy(proxyCfg);
        store.load();            
        
    }    
});
