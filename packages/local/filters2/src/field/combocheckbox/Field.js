Ext.define('Filters2.field.combocheckbox.Field', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
        'Ext.data.Store'
    ],
    xtype: 'filters-combocheckboxfield',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);        
    },
    grid: null,
    triggers: {
        clear: {
            weight: 1,
            cls: 'x-form-clear-trigger',
            handler: function(field) {
				field.setRawValue(null);
				field.store.each(function (item) {
                    item.set(field._checkField, 0);
                });                
				field.picker.getSelectionModel().deselectAll();
                field.grid.applyFilters();				
            },
            scope: 'this'
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
            
        }
    },    
    inlineData: null,
    enableKeyEvents: true,
    cls: "fieldFilterCls",
    margin: "0",
    filterMode: "IN",
    filterDataType: "string",
    onCollapse: function() {
         var me = this;
         me.grid.applyFilters();
         me.callSuper(arguments);        
    },
    initComponent: function() {
        var me = this;
        var inlineData = me.inlineData;

        if(Ext.isFunction(me.inlineData)) {
            inlineData = [];
        }
        
        me.store = Ext.create('Ext.data.Store', {
            fields: [{
                name: 'display',
                type: 'string'
            }, {
                name: 'val',
                type: 'string'
            }],
            data: inlineData
        });

        me.editable = false;
        me.valueField = "val";
        me.displayField = "display";
        me.queryMode = 'local';
        me.multiSelect = true;
        me._checkField = 'checked';
        
        me.tpl = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="x-boundlist-item">',
                    '<tpl if="values.checked">',
                        '<div class="nowrap">',
                            '<input type="checkbox" checked="checked" />',
                            // '<input type="checkbox" checked="checked" disabled="disabled" />',
                                ' {',
                                    this.displayField,
                                '}',
                        '</div>',
                    '</tpl>',
                    '<tpl if="!values.checked">',
                        '<div class="nowrap">',
                            // '<input type="checkbox" disabled="disabled" />',
                            '<input type="checkbox" />',
                                ' {',
                                    this.displayField,
                                '}',
                        '</div>',
                    '</tpl>',
                '</div>',
            '</tpl>'
        );

        me.displayTpl = Ext.create('Ext.XTemplate',
            '{[this.setDisplayValues(values, xcount, xindex, xkey)]}',
            {
                setDisplayValues: function(values, xcount, xindex, xkey) {
                     var x = 0;
                     var l;
                     var arr = [];
                     if(values && values.length > 0) {
                         l = values.length;
                         for(x = 0; x < l; x++) {
                             if(values[x].checked === 1) {
                                 arr.push(values[x].display);
                             }
                         }
                     }
                     return arr.join(", ");                                         
                }
            }
        );

        me.listeners = {
            afterrender: {
                fn: function(cmp) {
                    var me = this;
                    if(Ext.isFunction(me.inlineData)) {
                        Ext.Function.bind(me.inlineData, me)();
                    }
                },
                scope: me
            },
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;                    
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            },
            change: function (combo, n) {
                combo.store.each(function (item) {
                    item.set(combo._checkField, 0);
                });
                if(n && n.forEach) {
                    n.forEach(function (item) {
                        combo.store.findRecord(combo.valueField, item, 0, false, true, true).set(combo._checkField, 1);
                    });
                }
            }
        };
        
        me.callParent(arguments);
        
    }
});
