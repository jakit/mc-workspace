Ext.define('Filters2.field.string.Field', {
    extend: 'Ext.form.field.Text',
    xtype: 'filters-stringfield',
    requires: [
        'Filters2.field.string.Menu'
    ],
    grid: null,
    gridxtype: null,
    filterMode: "Equal",
    defaultFilterMode: "Equal",
    filterDataType: "string",
    typeAheadDelay: 500,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setRawValue(null);
                field.grid.applyFilters();
            }
        },
        filtermode: {
            cls: 'textequal-trigger',
            defaultCls: 'textequal-trigger',
            handler: function(field, triggerEl, evt) {
                var dataIndex = field.up().dataIndex,
                    menu = field.grid[field.menuId];
                menu.dataIndex = dataIndex;
                menu.curTrigger = triggerEl;
                menu.thisField = field;
                menu.show();
                menu.alignTo(triggerEl.el);
            },
            tooltip: 'Equal'
        }
    },  
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    initComponent: function() {
        var me = this, menuId;
        menuId = "menu_" + me.getId();
        me.gridxtype = me.grid.xtype;
        me.menuId = menuId;
        me.defaultFilterMode = me.filterMode;
        me.grid[menuId] = Ext.create('Filters2.field.string.Menu', {
            grid: me.grid,
            id: menuId,
            filterMode: me.filterMode
        });
        me.listeners = {
            change: {
                fn: Ext.Function.createBuffered(function(field) {field.up('grid').applyFilters();}, me.typeAheadDelay, this)
            },
            render: {
                fn: function(cmp) {
                    var me = this, trig = cmp.getTrigger('filtermode'), el = trig.el, clearTrig, clearEl,
                        allTriggers = [
                            'textequal-trigger',
                            'starts-with-trigger',
                            'ends-with-trigger',
                            'contains-trigger'
                        ],
                        cls = {
                           "Equal": "textequal-trigger",
                           "Contains": "contains-trigger",
                           "Starts With": "starts-with-trigger",
                           "Ends With": "ends-with-trigger"      
                        },
                        x = 0,
                        l = allTriggers.length,
                        iconCls = cls[me.filterMode],
                        dataIndex = me.up().dataIndex;
                        
                    for(x = 0; x < l; x++) {
                        el.removeCls(allTriggers[x]);    
                    }
                    
                    el.addCls(iconCls);
                    
                    trig.tooltip = me.filterMode;
                    el.dom.setAttribute('data-qtip', trig.tooltip);
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                    
                },
                scope: me
            }
        };
        me.callParent(arguments);
    },
    resetFilterMode: function(cmp) {
        var me = this, trig = cmp.getTrigger('filtermode'), el = trig.el, clearTrig, clearEl,
            allTriggers = [
                'textequal-trigger',
                'starts-with-trigger',
                'ends-with-trigger',
                'contains-trigger'
            ],
            cls = {
               "Equal": "textequal-trigger",
               "Contains": "contains-trigger",
               "Starts With": "starts-with-trigger",
               "Ends With": "ends-with-trigger"      
            },
            x = 0,
            l = allTriggers.length,
            iconCls = cls[me.defaultFilterMode],
            dataIndex = me.up().dataIndex;
            
        for(x = 0; x < l; x++) {
            el.removeCls(allTriggers[x]);    
        }
        
        el.addCls(iconCls);
        
        trig.tooltip = me.defaultFilterMode;
        el.dom.setAttribute('data-qtip', trig.tooltip);
        clearTrig = cmp.getTrigger('clear');
        clearEl = clearTrig.el;
        clearEl.dom.setAttribute('data-qtip', 'clear field');
       
    }    
});