Ext.define('Filters2.field.number.Field2', {
    extend: 'Ext.form.field.Text',
    xtype: 'filters-numberfield2',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setRawValue(null);
                field.grid.applyFilters();
            }
        }
    },  
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    grid: null,
    initComponent: function() {
        var me = this;
        me.listeners = {
            blur: {
                fn: me.grid.applyFilters,
                scope: me.grid
            },
            keypress: {
                fn: me.grid.applyFiltersOnEnter,
                scope: me.grid
            },
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;                    
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            }
        };
        me.callParent(arguments);
    }    
});