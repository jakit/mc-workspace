Ext.define('Filters2.field.number.MenuViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.numbermenu',
    init: function(view) {
        this.control({
            'menuitem': {
                'click': {
                    fn: function(cmp) {
                        var me = this,
                            allTriggers = [
                                'equal-trigger',
                                'greater-equal-trigger',
                                'lesser-equal-trigger',
                                'between-trigger'
                            ],
                            tips = {
                                'greater-equal-trigger': "Greater Than",
                                'lesser-equal-trigger': "Lesser Than",
                                'between-trigger': "Between",
                                'equal-trigger': "Equal"
                            },
                            el = view.curTrigger.el,
                            x = 0,
                            l = allTriggers.length,
                            nextfield = me.thisField.next();
                        
                        for(x = 0; x < l; x++) {
                            el.removeCls(allTriggers[x]);    
                        }
                        el.addCls(cmp.iconCls);
                        me.thisField.filterMode = tips[cmp.iconCls];
                        view.curTrigger.el.dom.setAttribute('data-qtip', tips[cmp.iconCls]);
                        
                        if(cmp.iconCls == "between-trigger") {
                            me.grid.HFAPI.registerField(me.dataIndex);
                            nextfield.setDisabled(false);
                            me.grid.HFAPI.showRow(1);
                        } else {
                            me.grid.HFAPI.unRegisterField(me.dataIndex);
                            nextfield.setValue(null);
                            nextfield.setDisabled(true);
                            
                            if(me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                                me.grid.HFAPI.hideRow(1);
                            } 
                        }
                        if(Ext.isNumber(me.thisField.getValue())) {
                            me.thisField.grid.applyFilters();    
                        }
                        
                        
                    },
                    scope: view
                }
            }
        });
    }
});