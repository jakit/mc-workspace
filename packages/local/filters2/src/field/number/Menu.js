Ext.define('Filters2.field.number.Menu', {
    extend: 'Ext.menu.Menu',
    xtype: 'filters-numberfield-menu',
    requires: [
        'Ext.menu.Item',
        'Filters2.field.number.MenuViewController'
    ],
    controller: "numbermenu",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    initComponent: function() {
        var me = this;
        
        me.width = 27;
        me.padding = 0;
        me.margin = 0;
        
        me.items = [{
            iconCls: 'equal-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Equal'
        }, {
            iconCls: 'greater-equal-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: "Greater Than"
        }, {
            iconCls: 'lesser-equal-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Lesser Than'
        }, {
            iconCls: 'between-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Between'
        }];
        
        me.callParent(arguments);
    }
});
