Ext.define('TabBarStyle.view.main.TabPanel', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-tabpanel',
    requires: [
        'Ext.panel.Panel'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function(cfg) {
        var me = this;
        me.ui = "default";
        me.defaults = {
            xtype: 'panel',
            style: "border: 10px solid #3892D3",
            bodyPadding: 10
        };
        me.items = [{
            title: 'Tab 1',
            html: '<h2>Content appropriate for the current navigation.</h2>'
        }, {
            title: 'Tab 2',
            html: '<h2>Content appropriate for the current navigation.</h2>'
        }, {
            title: 'Tab 3',
            html: '<h2>Content appropriate for the current navigation.</h2>'
        }];
        
        me.callParent(arguments);
    }    

});
