Ext.define('UIFilter6.store.Main', {
    extend: 'Ext.data.Store',
    model: 'UIFilter6.model.Main',
    autoLoad: true,
    proxy: {
        url: "http://devnyeapi01:8080/rest/default/hsjag/v1/v_rdeals",
        originalUrl: "http://devnyeapi01:8080/rest/default/hsjag/v1/v_rdeals", // as a backup
        type: "rest",
        headers: {
            "Content-type": "application/json",
            "Authorization": "Espresso sDSGZwvHXDAChJVcy4RU:1"
        },
        reader: {
            type: "json",
            rootProperty: ""
        }
    }
});