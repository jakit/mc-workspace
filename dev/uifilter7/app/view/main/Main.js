/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */

Ext.define('UIFilter7.view.main.Main', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-main',
    requires: [
        'UIFilter7.view.HeaderFields'
    ],
    mixins: ['UIFilter7.view.InjectHeaderFields', 'UIFilter7.view.FiltersMixin'],
    scrollable: "y",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;

        me.store = "Main";
        
        me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js // required at all times
        };
        
        me.columns = [{
            dataIndex: "idDeal",
            header: "idDeal",
            flex: 1,
            headerField: me.setFilterField("stringfield", {
                grid: me
            })      
        }, {
            dataIndex: "dealName",
            header: "dealName",
            flex: 1,
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string",
                typeAheadDelay: 500
            })
        }, {
            dataIndex: "loanDate",
            header: "loanDate",
            flex: 1,
            headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        }, {
            dataIndex: "status",
            header: "status",
            flex: 1,
            headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: [{
                    'display': "AC",
                    'val': "AC"
                }, {
                    'display': "CL",
                    'val': "CL"
                }, {
                    'display': "CX",
                    'val': "CX"
                }, {
                    'display': "DX",
                    'val': "DX"
                }, {
                    'display': "EC",
                    'val': "EC"
                }, {
                    'display': "IP",
                    'val': "IP"
                }, {
                    'display': "LD",
                    'val': "LD"
                }, {
                    'display': "PR",
                    'val': "PR"
                }, {
                    'display': "UL",
                    'val': "UL"
                }],
                filterDataType: "string"
            }) 
        }, {
            dataIndex: "state",
            header: "state",
            flex: 1,
            headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "NJ",
                    'val': "NJ"
                }, {
                    'display': "NY",
                    'val': "NY"
                }, {
                    'display': "FL",
                    'val': "FL"
                }, {
                    'display': "CA",
                    'val': "CA"
                }]
            })            
        }, {
            dataIndex: "loanAmt",
            header: "loanAmt",
            flex: 1,
            headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })  
        }];
         
        cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'top',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });

        headerFieldsFeature = Ext.create('UIFilter7.view.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];  
        me.callParent(arguments);  
   }    
});