Ext.define('UIFilter7.view.field.number.Field1', {
    extend: 'Ext.form.field.Text',
    xtype: 'filters-numberfield1',
    requires: [
        'UIFilter7.view.field.number.Menu'
    ],
    grid: null,
    gridxtype: null,
    filterMode: "Equal",
    defaultFilterMode: "Equal",
    filterDataType: "number",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            }
        },
        filtermode: {
            cls: 'equal-trigger',
            defaultCls: 'equal-trigger',
            handler: function(field, triggerEl, evt) {
                var dataIndex = field.up().dataIndex,
                    menu = field.grid[field.menuId];
                menu.dataIndex = dataIndex;
                menu.curTrigger = triggerEl;
                menu.thisField = field;
                menu.show();
                menu.alignTo(triggerEl);
            },
            tooltip: 'Equal'
        }
    },  
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    initComponent: function() {
        var me = this, menuId;
        menuId = "menu_" + me.getId();
        me.gridxtype = me.grid.xtype;
        me.menuId = menuId;
        me.defaultFilterMode = me.filterMode;
        me.grid[menuId] = Ext.create('UIFilter7.view.field.number.Menu', {
            grid: me.grid,
            id: menuId,
            filterMode: me.filterMode
        });
        me.listeners = {
            blur: {
                fn: me.grid.applyFilters,
                scope: me.grid
            },
            keypress: {
                fn: me.grid.applyFiltersOnEnter,
                scope: me.grid
            },
            render: {
                fn: function(cmp) {
                    var me = this, trig = cmp.getTrigger('filtermode'), el = trig.el, clearTrig, clearEl,
                        allTriggers = [
                            'equal-trigger',
                            'greater-equal-trigger',
                            'lesser-equal-trigger',
                            'between-trigger'
                        ],
                        cls = {
                           "Equal": "equal-trigger",
                           "Between": "between-trigger",
                           "Lesser Than": "lesser-equal-trigger",
                           "Greater Than": "greater-equal-trigger"      
                        },
                        x = 0,
                        l = allTriggers.length,
                        iconCls = cls[me.filterMode],
                        nextfield = me.next(),
                        dataIndex = me.up().dataIndex;
                        
                    for(x = 0; x < l; x++) {
                        el.removeCls(allTriggers[x]);    
                    }
                    el.addCls(iconCls);
                    trig.tooltip = me.filterMode;                    
                    el.dom.setAttribute('data-qtip', trig.tooltip);
                    
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                    
                    if(iconCls == "between-trigger") {
                        me.grid.HFAPI.registerField(dataIndex);
                        nextfield.setDisabled(false);
                        me.grid.HFAPI.showRow(1);
                    } else {
                        me.grid.HFAPI.unRegisterField(dataIndex);
                        nextfield.setValue(null);
                        nextfield.setDisabled(true);
                        if(me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                            me.grid.HFAPI.hideRow(1);
                        } 
                    }  
                },
                scope: me
            }
        };
        me.callParent(arguments);
    },
    resetFilterMode: function(cmp) {
        var me = this, trig = cmp.getTrigger('filtermode'), el = trig.el, clearTrig, clearEl,
            allTriggers = [
                'equal-trigger',
                'greater-equal-trigger',
                'lesser-equal-trigger',
                'between-trigger'
            ],
            cls = {
               "Equal": "equal-trigger",
               "Between": "between-trigger",
               "Lesser Than": "lesser-equal-trigger",
               "Greater Than": "greater-equal-trigger"      
            },
            x = 0,
            l = allTriggers.length,
            iconCls = cls[me.defaultFilterMode],
            nextfield = me.next(),
            dataIndex = me.up().dataIndex;
            
        for(x = 0; x < l; x++) {
            el.removeCls(allTriggers[x]);    
        }
        el.addCls(iconCls);
        trig.tooltip = me.defaultFilterMode;                    
        el.dom.setAttribute('data-qtip', trig.tooltip);
        
        clearTrig = cmp.getTrigger('clear');
        clearEl = clearTrig.el;
        clearEl.dom.setAttribute('data-qtip', 'clear field');
        
        if(iconCls == "between-trigger") {
            me.grid.HFAPI.registerField(dataIndex);
            nextfield.setDisabled(false);
            me.grid.HFAPI.showRow(1);
        } else {
            me.grid.HFAPI.unRegisterField(dataIndex);
            nextfield.setValue(null);
            nextfield.setDisabled(true);
            if(me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                me.grid.HFAPI.hideRow(1);
            } 
        }
    }    
});