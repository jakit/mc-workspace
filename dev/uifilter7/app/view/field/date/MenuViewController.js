Ext.define('UIFilter7.view.field.date.MenuViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.datemenu',
    init: function(view) {
        this.control({
            'menuitem': {
                'click': {
                    fn: function(cmp) {
                        var me = this,
                            allTriggers = [
                                'ondate-trigger',
                                'after-trigger',
                                'before-trigger',
                                'between-trigger'
                            ],
                            tips = {
                                'after-trigger': "After",
                                'before-trigger': "Before",
                                'between-trigger': "Between",
                                'ondate-trigger': "On"
                            },
                            el = view.curTrigger.el,
                            x = 0,
                            l = allTriggers.length,
                            nextfield = me.thisField.next();
                        
                        for(x = 0; x < l; x++) {
                            el.removeCls(allTriggers[x]);    
                        }
                        el.addCls(cmp.iconCls);
                        me.thisField.filterMode = tips[cmp.iconCls];
                        view.curTrigger.el.dom.setAttribute('data-qtip', tips[cmp.iconCls]);
                        
                        if(cmp.iconCls == "between-trigger") {
                            me.grid.HFAPI.registerField(me.dataIndex);
                            nextfield.setDisabled(false);
                            me.grid.HFAPI.showRow(1);
                        } else {
                            me.grid.HFAPI.unRegisterField(me.dataIndex);
                            nextfield.setValue(null);
                            nextfield.setDisabled(true);
                        
                            if(me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                                me.grid.HFAPI.hideRow(1);
                            } 
                        }
                        if(me.thisField.getValue()) {
                            me.thisField.grid.applyFilters();    
                        }
                    },
                    scope: view
                }
            }
        });
    }
});