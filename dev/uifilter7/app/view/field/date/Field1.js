Ext.define('UIFilter7.view.field.date.Field1', {
    extend: 'Ext.form.field.Date',
    xtype: 'filters-datefield1',
    requires: [
        'UIFilter7.view.field.date.Menu'
    ],
    grid: null,
    gridxtype: null,
    filterMode: "On",
    defaultFilterMode: "On",
    filterDataType: "date",  
    editable: false,  
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            },
            weight: 0
        },
        filtermode: {
            cls: 'ondate-trigger',
            defaultCls: 'ondate-trigger',
            handler: function(field, triggerEl, evt) {
                var dataIndex = field.up().dataIndex,
                    menu = field.grid[field.menuId];
                menu.dataIndex = dataIndex;
                menu.curTrigger = triggerEl;
                menu.thisField = field;
                menu.show();
                menu.alignTo(triggerEl);
            },
            tooltip: 'On',
            weight: 1
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
        }
    },
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    initComponent: function() {
        var me = this, menuId;
        menuId = "menu_" + me.getId();
        me.gridxtype = me.grid.xtype;
        me.menuId = menuId;
        me.defaultFilterMode = me.filterMode;
        me.grid[menuId] = Ext.create('UIFilter7.view.field.date.Menu', {
            grid: me.grid,
            id: menuId,
            filterMode: me.filterMode
        });
        me.listeners = {
            
            expand: {
                fn: function(cmp) {
                    cmp.picker.setWidth(212);
                },
                scope: me                
            },
            
            select: {
                fn: function() {
                    this.grid.applyFilters();
                },
                scope: me
            },
            render: {
                fn: function(cmp) {
                    var me = this, trig = cmp.getTrigger('filtermode'), el = trig.el, clearTrig, clearEl,
                        allTriggers = [
                            'ondate-trigger',
                            'after-trigger',
                            'before-trigger',
                            'between-trigger'
                        ],
                        cls = {
                           "On": "ondate-trigger",
                           "Between": "between-trigger",
                           "Before": "before-trigger",
                           "After": "after-trigger"      
                        },
                        x = 0,
                        l = allTriggers.length,
                        iconCls = cls[me.filterMode],
                        nextfield = me.next(),
                        dataIndex = me.up().dataIndex;
                        
                    for(x = 0; x < l; x++) {
                        el.removeCls(allTriggers[x]);    
                    }

                    el.addCls(iconCls);
                    trig.tooltip = me.filterMode;                    
                    el.dom.setAttribute('data-qtip', trig.tooltip);
                    
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');                    
                    
                    if(iconCls == "between-trigger") {
                        me.grid.HFAPI.registerField(dataIndex);
                        nextfield.setDisabled(false);
                        me.grid.HFAPI.showRow(1);
                    } else {
                        me.grid.HFAPI.unRegisterField(dataIndex);
                        nextfield.setValue(null);
                        nextfield.setDisabled(true);
                        if(me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                            me.grid.HFAPI.hideRow(1);
                        } 
                    }  
                },
                scope: me
            }
        };
        me.callParent(arguments);
    },
    resetFilterMode: function(cmp) {
        var me = this, trig = cmp.getTrigger('filtermode'), el = trig.el, clearTrig, clearEl,
            allTriggers = [
                'ondate-trigger',
                'after-trigger',
                'before-trigger',
                'between-trigger'
            ],
            cls = {
               "On": "ondate-trigger",
               "Between": "between-trigger",
               "Before": "before-trigger",
               "After": "after-trigger"      
            },
            x = 0,
            l = allTriggers.length,
            iconCls = cls[me.defaultFilterMode],
            nextfield = me.next(),
            dataIndex = me.up().dataIndex;
            
        for(x = 0; x < l; x++) {
            el.removeCls(allTriggers[x]);    
        }

        el.addCls(iconCls);
        trig.tooltip = me.defaultFilterMode;                    
        el.dom.setAttribute('data-qtip', trig.tooltip);
        
        clearTrig = cmp.getTrigger('clear');
        clearEl = clearTrig.el;
        clearEl.dom.setAttribute('data-qtip', 'clear field');                    
        
        if(iconCls == "between-trigger") {
            me.grid.HFAPI.registerField(dataIndex);
            nextfield.setDisabled(false);
            me.grid.HFAPI.showRow(1);
        } else {
            me.grid.HFAPI.unRegisterField(dataIndex);
            nextfield.setValue(null);
            nextfield.setDisabled(true);
            if(me.grid.HFAPI.getFieldRegistrationSize() == 0) {
                me.grid.HFAPI.hideRow(1);
            } 
        }  
    
    }    
});