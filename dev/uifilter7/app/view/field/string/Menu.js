Ext.define('UIFilter7.view.field.string.Menu', {
    extend: 'Ext.menu.Menu',
    xtype: 'filters-stringfield-menu',
    requires: [
        'Ext.menu.Item',
        'UIFilter7.view.field.string.MenuViewController'
    ],
    controller: "stringmenu",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    initComponent: function() {
        var me = this;
        
        me.width = 27;
        me.padding = 0;
        me.margin = 0;
        
        me.items = [{
            iconCls: 'textequal-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Equal'
        }, {
            iconCls: 'starts-with-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: "Starts With"
        }, {
            iconCls: 'ends-with-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Ends With'
        }, {
            iconCls: 'contains-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Contains'
        }];
        
        me.callParent(arguments);
    }
});
