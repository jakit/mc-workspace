Ext.define('UIFilter7.model.Main', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'idDeal',          type: 'string' },
        { name: 'dealName',         type: 'string'    },
        { name: 'loanDate',         type: 'string' },
        { name: 'status',           type: 'string'    },
        { name: 'state',            type: 'string'    },
        { name: 'loanAmt',           type: 'number'  }
    ]    
});
