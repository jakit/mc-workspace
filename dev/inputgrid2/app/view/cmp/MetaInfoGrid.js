Ext.define('InputGrid2.view.cmp.MetaInfoGrid', {
    // extend: 'Ext.panel.Panel',
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.plugin.CellEditing',
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar',
        'Ext.menu.Menu',
        'Ext.window.MessageBox',
        'Ext.data.Store',
        'Ext.data.Model',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text'
            
    ],
    xtype: 'metainfogrid',
    settings: null,
    btnPrefixId: "btnHolder-",
    btnId: null,
    control: null,
    button: null,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.control = me.generateControl();
        me.button = me.generateAddButton(me.control);

        me.viewConfig = {
            forceFit: true,
            stripeRows: false,
            trackOver: false
        };

        me.cls = 'actionGrid'; // Is this even important now?

        me.plugins = {
            ptype: 'cellediting',
            clicksToEdit: 1,
            listeners: {
                edit: {
                    fn: me.cellEditFunction,
                    scope: me
                }
            }
        };

        me.preFields = [{ "name": "tag",        "type": "string"  }];
        me.postFields = [{ "name": "isPrimary",  "type": "boolean" }]; 

        var fields = me.settings.grid.fields;
        fields.unshift(me.preFields[0]);
        fields.push(me.postFields[0]);
        
        me.preColumns = [{
            dataIndex: 'tag',
            header: 'Tag',
            editor: me.control,
            width: 140,
            renderer: me.tagRenderer
        }];

        me.postColumns = [{
            dataIndex: "isPrimary",
            header: "isPrimary",
            align: 'center',
            width: 90,
            renderer: Ext.Function.bind(me.radioButtonRenderer, me, ["isPrimaryRadioGroup", "isPrimaryRadioButton"], true)
        }, {
            width: 40,
            header: "",
            renderer: me.actionRenderer
        }];
        
        var columns = me.settings.grid.columns;
        columns.unshift(me.preColumns[0]);
        columns.push(me.postColumns[0]);
        columns.push(me.postColumns[1]);
        
        
        var store = Ext.create('Ext.data.Store', {
            "fields": fields
        });
        
        me.store = store;
        me.columns = columns;
        
        me.listeners = {
            // cellclick: {
                // fn: me.cellClickDispatch
            // },
            titlechange: {
                fn: me.setSplitButton
            },
            afterrender: {
                fn: me.setSplitButtonHolder
            }
        };

        me.callParent(arguments);
    },
    generateControl: function() {
        var me = this;
        if(me.settings.editor.type == "combo") {
            return me.generateComboField();
        } else {
            return me.generateTextField();
        }
    },
    generateAddButton: function(control) {
        var me = this;
        if(me.settings.editor.type == "combo") {
            return me.generateSplitButton(control);
        } else {
            return me.generateButton();
        }
    },
    generateComboField: function() {
        var me = this,
            model, store, combo;
        
        store = Ext.create('Ext.data.Store', {
            "fields": [
                {"name": "tag", "type": "string"}, 
                {"name": "label", "type": "string"}
            ]
        });
        
        combo = Ext.create('Ext.form.field.ComboBox', {
            store: store,
            emptyText: 'Select...',
            valueField: 'tag',
            displayField: 'label',
            queryMode: 'local',
            value: null
        });

        store.loadData(me.settings.editor.data);
        
        return combo;
    },
    generateTextField: function() {
        var me = this;

        return Ext.create('Ext.form.field.Text', {
            value: null
        });
        
    },
    generateSplitButton: function(combo) {
        var me = this,
            comboStore = combo.getStore(),
            menuItems = me.generateMenu(comboStore),
            menu = new Ext.menu.Menu({
                items: menuItems
            });
        
        me.menu = menu;

        return Ext.create('Ext.button.Split', {
            text: 'Add',
            menu: me.menu
        });

    },
    generateButton: function() {
        var me = this;
        return Ext.create('Ext.button.Button', {
            text: 'Add'
        });       
    },
    
    // imported from mixins
    setSplitButtonHolder: function() {
        var me = this;
        var tpl = "<span style='margin-top:4px;display:inline-block;'>{1}</span><span style='padding-left: 10px;' id='{0}'></span>";
        me.btnId = me.btnPrefixId + me.id;
        me.setTitle(Ext.String.format(tpl, me.btnId, me.title));
    },

    setSplitButton: function(panel, newTitle, oldTitle) {
        var me = this;
        var btnHolder = Ext.get(me.btnId);
        me.button.render(btnHolder);        
    },

    getMenuItem: function(tag) {
        // CG: 12/29/2015 - Would probably be more efficient to just set the menuitem button to each
        //                  grid row and refer to it via rec.data.menuitem for example
        //                  will consider refactoring if it doesn't cause regression amd doesn't add code complexity        
        var me = this,
            menuItems = me.menu.items.items,
            menuItemsLength = menuItems.length,
            x;
            
        for(x = 0; x < menuItemsLength; x++) {
            if(menuItems[x].tag == tag) {
                return menuItems[x];
            }
        }
        return null;
    },

    swapMenuItems: function(cmb, newVal, oldVal) {
        var grid = this,
            newMenuItem = grid.getMenuItem(newVal),
            oldMenuItem = grid.getMenuItem(oldVal);
            
        newMenuItem.hide();
        oldMenuItem.show();
    },

    addRowToGrid: function(menuitem, event) {
        
        var view = this,             
            store = view.getStore(),
            count = store.count(),
            isPrimary = true,
            rec;
        
        if(count > 0) {
            isPrimary = false;
        }
        
        rec = Ext.create('InputGrid.model.Grid1', {
            tag: menuitem.tag,
            number: "",
            isPrimary: isPrimary
        });
        
        menuitem.hide();
        store.add(rec);
                                
    },
    
    cellEditFunction: function(editor, context) {
       context.record.commit();
    },

    triggerClickOverride: function(cmb, trigEl, event, menu) {

        var store = cmb.getStore(),
            menuItems = menu.items.items,
            menuItemsLength = menuItems.length,
            x, menuItem,
            records = [], rec;
        
        store.removeAll();

        // CG: 29/12/2015 - should create a getMenuItems function        
        for(x = 0; x < menuItemsLength; x++) {
            menuItem = menuItems[x];
            if(menuItem.isVisible()) {
                rec = Ext.create('InputGrid.model.PhoneCombo', {
                    tag: menuItem.tag,
                    label: menuItem.text,
                    inUse: false
                });
                records.push(rec);
            }
        }
        
        store.loadData(records);

        if(cmb.isExpanded) {
            cmb.collapse();
        } else {
            cmb.expand();
        }
    },


    generateMenu: function(store) {
        var me = this,
            menuArr = [],
            items = store.data.items,
            x, l = items.length,
            rec, menuItem;
            
        for(x = 0; x < l; x++) {
            rec = items[x];
            menuItem = {
                text: rec.data.label,
                tag: rec.data.tag,
                listeners: {
                    click: {
                        fn: me.addRowToGrid,
                        scope: me
                    }
                }
            };
            menuArr.push(menuItem);
        }
        
        return menuArr;
    },
    
    // CG: 12/29/2015 - Dormant function, need to use this so that we don't sync incomplete records 
    //                  once REST call is ready to use this function will be used
    // validateRow: function(rec) {
        // // CG: 12/28/2015 - JS empty string is false
        // if(Ext.String.trim(rec.data.number) && Ext.String.trim(rec.data.tag)) {
            // return true;
        // }
        // return false;
    // },
    
    radioButtonRenderer: function(val, meta, rec, rowIdx, colIdx, store, view, group, id) {
        var me = this;
        var compositeId = me.id + "-" + id;
        var htmlTpl = "<input style='cursor:pointer;' type='radio' name='{3}' id='{4}{0}' value='{1}' {2}>",
            isChecked = val ? "checked": "",
            html = Ext.String.format(htmlTpl, rowIdx, val, isChecked, group, compositeId);
            
        // CG: 12/28/2015 - A bug in ExtJS 5 which never happened in 4, when rec is set in the cellclick, meta is null
        //                - fortunately, this is a one time requirement, so no issues with the workaround
        if(meta) {
            meta.align = "center";
            meta.tdAttr = "celltype=" + id;
            meta.tdStyle = 'cursor:pointer;';
        } 
        return html;
    },

    tagRenderer: function(val, meta, rec, rowIdx, colIdx, store, view) {
        var me = this,
            menuItem;

        // CG: 12/30/2015 - this might be necessary anymore, will need to re-evaluate with testing        
        if(!val) {
            return "Select ..."
        }
        menuItem = me.getMenuItem(val);
        return menuItem.text;
    },


    // ORIGINAL
    actionRenderer: function(val, meta, rec, rowIdx, colIdx, store, view, group, id) {
        var count = store.count(),
            removeSpan = "<span class='actionFormat' title='Remove Row'>x</span>";

        // CG: 12/28/2015 - A bug in ExtJS 5 which never happened in 4, when rec is set in the cellclick, meta is null
        //                - fortunately, this is a one time requirement, so no issues with the workaround
        if(meta) {
            meta.align = "center";
            meta.tdAttr = "celltype=action";
        }

        return removeSpan;        
    },
    
    cellClickDispatch: function(gridview, td, cellIndex, record, tr, rowIndex, e) {

        var me = this,
            cellType = td.getAttribute('celltype'),
            store = gridview.getStore(),
            length,
            x, rec, removeRec, menuItem, phone;
        
        switch(cellType) {
            case "isPrimaryRadioButton": 
                if(e.target.type != "radio") {
                    Ext.get(e.target.parentElement).select('input[type=radio]').elements[0].checked = true;
                }
                record.set('isPrimary', true);
                record.commit();
                
                length = store.count();
                
                // CG: 12/28/2015 - the list of items is never more than 10 at best, so this is an efficient way to deal with uniqueness
                for(x = 0; x < length; x++) {
                    rec = store.getAt(x);
                    if(record.internalId != rec.internalId) {
                        rec.set('isPrimary', false);
                        rec.commit();
                    }            
                }        
                store.sync();
            break;
            
            case "action":
            
                phone = "";
                if(record.data.number) {
                    phone = "Phone: " + record.data.number;
                }

                menuItem = me.getMenuItem(record.data.tag);
            
                var msg = [
                    "Do you wish to delete this record?",
                    "Tag: " + menuItem.text,
                    phone
                ].join("<br/>");

                Ext.Msg.confirm("Deletion Warning", msg, function(answer) {
                    if(answer == "yes") {
                        var menuItem = this.menuItem;
                        var store = this.store;
                        var record = this.record;
                        menuItem.show(); 
                        store.remove(record);
                    }
                }, {
                    menuItem: menuItem,
                    store: store,
                    record: record
                });
                
            break;

        }
    }

});
