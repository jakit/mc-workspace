Ext.define('InputGrid2.view.cmp.TagComboBox', {
    extend: 'Ext.form.field.ComboBox',
    requires: [],
    xtype: 'tagcombobox',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
    }
});
