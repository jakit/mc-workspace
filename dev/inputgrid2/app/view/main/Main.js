Ext.define('InputGrid2.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.panel.Panel',
        'Ext.layout.container.Border',
        'Ext.layout.container.VBox',
        'InputGrid2.view.cmp.MetaInfoGrid'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
      
        me.layout = {
            type: 'border'
        };
        
        me.items = [{
            xtype: 'panel',
            region: 'center',
            title: 'Area',
            html: 'Area'
        }, {
            xtype: 'panel',
            region: 'east',
            width: 350,
            title: 'Contacts',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'metainfogrid',
                title: 'Phones',
                settings: {
                    "editor": {
                        "type": "combo",
                        "data": [
                            { "tag": "officeFax1", "label": "Fax Number 1"},
                            { "tag": "officeFax2", "label": "Fax Number 2"},
                            { "tag": "officeFax3", "label": "Fax Number 3"},
                            { "tag": "cellPhone1", "label": "Cell Number 1"},
                            { "tag": "cellPhone2", "label": "Cell Number 2"},
                            { "tag": "cellPhone3", "label": "Cell Number 3"}
                        ]
                    },
                    "grid": {
                        "fields": [
                            { "name": "number",     "type": "string"  }
                        ],
                        "columns": [{
                            dataIndex: "number",
                            header: "Number",
                            width: 150,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter a number'
                            }
                        }]  
                    }

                }
            }, {
                xtype: 'metainfogrid',
                title: 'E-Mails',
                settings: {
                    "editor": {
                        "type": "combo",
                        "data": [
                            { "tag": "email1", "label": "E-Mail 1"},
                            { "tag": "email2", "label": "E-Mail 2"},
                            { "tag": "email3", "label": "E-Mail 3"}
                        ]
                    },
                    "grid": {
                        "fields": [
                            { "name": "number",     "type": "string"  }
                        ],
                        "columns": [{
                            dataIndex: "number",
                            header: "Number",
                            width: 150,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter a number'
                            }
                        }]
                    }

                }
            }, {
                xtype: 'metainfogrid',
                title: 'Addresses',
                settings: {
                    "editor": {
                        "type": "combo",
                        "data": [
                            { "tag": "address1", "label": "Address 1"},
                            { "tag": "address2", "label": "Address 2"},
                            { "tag": "address3", "label": "Address 3"}
                        ]
                    },
                    "grid": {
                        "fields": [
                            { "name": "address",   "type": "string"  },
                            { "name": "city",      "type": "string"  },
                            { "name": "state",     "type": "string"  },
                            { "name": "zip",       "type": "string"  }
                        ],
                        "columns": [{
                            dataIndex: "address",
                            header: "Address",
                            width: 150,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter an address'
                            }
                        }, {
                            dataIndex: "city",
                            header: "City",
                            width: 150,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter a city'
                            }
                        }, {
                            dataIndex: "state",
                            header: "State",
                            width: 60,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'State'
                            }
                        }, {
                            dataIndex: "zip",
                            header: "Zip",
                            width: 60,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Zip Code'
                            }
                        }]
                    }
                }
            }, {
                xtype: 'metainfogrid',
                title: 'Miscellaneous',
                settings: {
                    "editor": {
                        "type": "text"
                    },
                    "grid": {
                        "fields": [
                            { "name": "value",     "type": "string"  }
                        ],
                        "columns": [{
                            dataIndex: "value",
                            header: "Value",
                            width: 150,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter a Value'
                            }
                        }]                        
                    }
                }
            }]            
       }];
                 
       me.callParent(arguments);
    }
});