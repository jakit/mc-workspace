Ext.application({
    name: 'InputGrid2',

    extend: 'InputGrid2.Application',
    
    autoCreateViewport: 'InputGrid2.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to InputGrid2.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
