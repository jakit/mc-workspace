/*
 * Mixin for MetaInfoGrid class
 */

Ext.define('InputGrid.mixins.view.MetaInfoGrid', {

    setViewConfig: function() {

        var me = this,
            viewConfig = me.settings.grid.viewConfig,
            baseViewConfig = {
                stripeRows: false,
                trackOver: false
            };
         
        if(Ext.isObject(viewConfig) && Ext.Object.getSize(viewConfig)) {
            Ext.apply(baseViewConfig, viewConfig);
        }  
         
        me.viewConfig = baseViewConfig;
        
    },

    setConfirmDeletion: function() {

        var me = this,
            confirmDeletion = me.settings.grid.confirmDeletion;
            
        if(confirmDeletion !== undefined) {
            me.confirmDeletion = confirmDeletion;
        }  
        
    },

    setAddButtonLabel: function() {

        var me = this,
            buttonLabel = me.settings.grid.buttonLabel;
            
        if(buttonLabel) {
            me.buttonLabel = Ext.String.trim(buttonLabel);
        }  
        
    },

    setTagTitle: function() {

        var me = this,
            tagTitle = me.settings.grid.tagTitle;
            
        if(tagTitle) {
            me.tagTitle = Ext.String.trim(tagTitle);
        }
          
    },
    
    generateComboEditorStore: function() {
        var me = this;
       
        if(me.editorType == "combo") {

            me.comboModel = Ext.define(me.id + "_ComboModel", {
                extend: 'Ext.data.Model',
                fields: [
                    {"name": "tag", "type": "string"}, 
                    {"name": "label", "type": "string"}
                ]            
            });
            me.comboStore = Ext.create('Ext.data.Store', {
                model: me.comboModel,
                data: me.settings.editor.data
            });
              
            me.generateMenuItems();    
        }
    },

    stopEmptyMenuFromShowing: function(p) {
        
        var me = this,
            x = 0,
            items = p.items.items,
            l = items.length;
            
        for(x = 0; x < l; x++) {
            if(items[x].hidden == false) {
                return true;
            }
        }
        
        return false;
          
    },

    generateMenuItems: function() {

        var me = this,
            menuItems;
            
        menuItems = me.generateMenu(me.comboStore);
        
        menu = new Ext.menu.Menu({
            items: menuItems,
            listeners: {
                "beforeshow": {
                    fn: me.stopEmptyMenuFromShowing,
                    scope: me
                }
            }
        });

        me.menu = menu;
        
    },

    generateGridStore: function() {
        
        var me = this,
            tagField, isPrimaryField,
            gridFields, x, l;

        tagField = { "name": "tag", "type": "string"  };
        isPrimaryField = { "name": "isPrimary", "type": "boolean" };
        
        me.gridModelFields = [];
        me.gridModelFields.push(tagField);
        
        gridFields = me.settings.grid.fields;
        l = gridFields.length;
        
        for(x = 0; x < l; x++) {
            me.gridModelFields.push(gridFields[x]);
        }

        if(me.useIsPrimary) {
            me.gridModelFields.push(isPrimaryField);
        }
        
        me.gridModel = Ext.define(me.id + '_GridModel', {
            extend: 'Ext.data.Model',
            fields: me.gridModelFields
        });
        
        me.gridStore = Ext.create('Ext.data.Store', {
            model: me.gridModel,
            data: {
                data: []
            },
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'data'
                }
            }            
        });

        me.store = me.gridStore;
    },

    getEditor: function() {
        
        var me = this,
            editor;
        
        if(me.editorType == "combo") {
            editor = {
                xtype: 'combo',
                store: me.comboStore,
                emptyText: 'Select...',
                valueField: 'tag',
                displayField: 'label',
                queryMode: 'local',
                value: null,
                editable: false,
                onTriggerClick: Ext.Function.bind(me.triggerClickOverride, me, [me.menu], true),
                listeners: {
                    change: {
                        fn: me.swapMenuItems,
                        scope: me
                    }
                }
            };    
        } else {
            editor = {
                xtype: 'textfield',
                emptyText: 'Type your tag'
            };
        }

        return editor;
        
    },
    
    setTagWidth: function(tagColumn) {

        var me = this;
        
        var tagFieldWidth = me.settings.grid.tagFieldWidth;
        var tagFieldFlex = me.settings.grid.tagFieldFlex;        
      
        if(tagFieldFlex) {
            tagColumn.width = null;
            tagColumn.flex = tagFieldFlex
        } else {
            if(tagFieldWidth) {
                tagColumn.width = tagFieldWidth;
                tagColumn.flex = null;
            }
        }
      
        return tagColumn;  
        
    },
    
    setGridColumns: function(editor) {

        var me = this,
            tagColumn, isPrimaryRadioGroup, isPrimaryRadioButton, isPrimaryColumn, actionColumn,
            columns, x, l;
        
        tagColumn = {
            dataIndex: 'tag',
            header: me.tagTitle,
            editor: editor,
            width: 140,
            flex: null,
            renderer: me.tagRenderer
        }; 
        
        tagColumn = me.setTagWidth(tagColumn);

        isPrimaryRadioGroup = me.id + "_isPrimaryRadioGroup";
        isPrimaryRadioButton = me.id + "_isPrimaryRadioButton";

        isPrimaryColumn = {
            dataIndex: "isPrimary",
            header: "isPrimary",
            align: 'center',
            width: 90,
            renderer: Ext.Function.bind(me.radioButtonRenderer, me, [isPrimaryRadioGroup, isPrimaryRadioButton], true)
        };
        
        actionColumn = {
            width: 40,
            header: "",
            renderer: me.actionRenderer
        };  
        
        columns = me.settings.grid.columns;
        l = columns.length;
        
        me.columns = [];
        me.columns.push(tagColumn);
        
        for(x = 0; x < l; x++) {
            me.columns.push(columns[x]);
        }
        
        if(me.useIsPrimary) {
            me.columns.push(isPrimaryColumn);
        }
        
        me.columns.push(actionColumn);
        
    },


    setSplitButtonHolder: function() {
        
        var me = this,
            btnHolderId = me.id + "_btnHolder",
            tpl = "<span style='margin-top:4px;display:inline-block;'>{1}</span><span style='padding-left: 10px;' id='{0}'></span>",
            title = Ext.String.format(tpl, btnHolderId, me.title);
        
        me.setTitle(title);
        
        // CG: 02/03/2016 - Should refactor the 'afterrender' listener to be more generic
        me.cellEditPlugin = me.getCellPluginEditor();
        
    },
    
    getCellPluginEditor: function() {
        var me = this,
            x, l = me.plugins.length;
            
        if(l) {
            for(x = 0; x < l; x++) {
                if(me.plugins[x].ptype == "cellediting") {
                    return me.plugins[x];
                }
            }
        }
        
        return null;
        
    },

    setSplitButton: function(panel, newTitle, oldTitle) {
        
        var me = this,
            btnHolderId = me.id + "_btnHolder",
            btnHolder = Ext.get(btnHolderId),
            btn;
               
        if(me.editorType == "combo") {
            
            btn = Ext.create('Ext.button.Split', {
                text: me.buttonLabel,
                menu: me.menu
            });
            
        } else {
            btn = Ext.create('Ext.button.Button', {
                text: me.buttonLabel,
                listeners: {
                    click: {
                        fn: Ext.Function.createThrottled(me.addEmptyRowToGrid, 2000, me),
                        scope: me
                    }
                }
            });
        }

        btn.render(btnHolder);
        
    },

    getMenuItem: function(tag) {

        // CG: 12/29/2015 - Would probably be more efficient to just set the menuitem button to each
        //                  grid row and refer to it via rec.data.menuitem for example
        //                  will consider refactoring if it doesn't cause regression amd doesn't add code complexity        

        var me = this,
            menuItems = me.menu.items.items,
            menuItemsLength = menuItems.length,
            x;
            
        for(x = 0; x < menuItemsLength; x++) {
            if(menuItems[x].tag == tag) {
                return menuItems[x];
            }
        }
        return null;
        
    },

    swapMenuItems: function(cmb, newVal, oldVal) {

        var grid = this,
            newMenuItem = grid.getMenuItem(newVal),
            oldMenuItem = grid.getMenuItem(oldVal);
            
        newMenuItem.hide();
        oldMenuItem.show();
        
    },

    addEmptyRowToGrid: function(btn) {
        
        var view = this,             
            store = view.getStore(),
            count = store.count(),
            isPrimary = true,
            rec;
        
        if(count > 0) {
            isPrimary = false;
        }

        rec = Ext.create(view.gridModel, {
            tag: "",
            number: "",
            isPrimary: isPrimary
        });

        store.add(rec);
        count = store.getCount();
        view.startEditingCell(count - 1, 0);
                                
    },


    addRowToGrid: function(menuitem, event) {
        
        var view = this,             
            store = view.getStore(),
            count = store.count(),
            isPrimary = true,
            rec;
        
        if(count > 0) {
            isPrimary = false;
        }

        rec = Ext.create(view.gridModel, {
            tag: menuitem.tag,
            number: "",
            isPrimary: isPrimary
        });
        
        menuitem.hide();
        store.add(rec);
        
        count = store.getCount();
        view.startEditingCell(count - 1, 0);
        
                                
    },
    
    startEditingCell: function(row, col) {
        var me = this;
        me.cellEditPlugin.startEditByPosition({
            row: row,
            column: col
        });
        
    },
    
    cellEditFunction: function(editor, context) {
        
        context.record.commit();
        
    },

    triggerClickOverride: function(cmb, trigEl, event, menu) {

        var store = cmb.getStore(),
            menuItems = menu.items.items,
            menuItemsLength = menuItems.length,
            x, menuItem,
            records = [], rec;
        
        store.removeAll();
        
        // CG: 29/12/2015 - should create a getMenuItems function        
        for(x = 0; x < menuItemsLength; x++) {
            menuItem = menuItems[x];
            if(menuItem.isVisible()) {
                rec = Ext.create(store.getModel(), {
                    tag: menuItem.tag,
                    label: menuItem.text,
                    inUse: false
                });                
                records.push(rec);
            }
        }
        
        store.loadData(records);

        if(cmb.isExpanded) {
            cmb.collapse();
        } else {
            cmb.expand();
        }
    },


    generateMenu: function(store) {
        var me = this,
            menuArr = [],
            items = store.data.items,
            x, l = items.length,
            rec, menuItem;
            
        for(x = 0; x < l; x++) {
            rec = items[x];
            menuItem = {
                text: rec.data.label,
                tag: rec.data.tag,
                listeners: {
                    click: {
                        fn: me.addRowToGrid,
                        scope: me
                    }
                }
            };
            menuArr.push(menuItem);
        }
        
        return menuArr;
    },
    
    // CG: 12/29/2015 - Dormant function, need to use this so that we don't sync incomplete records 
    //                  once REST call is ready to use this function will be used
    // validateRow: function(rec) {
        // // CG: 12/28/2015 - JS empty string is false
        // if(Ext.String.trim(rec.data.number) && Ext.String.trim(rec.data.tag)) {
            // return true;
        // }
        // return false;
    // },
    
    radioButtonRenderer: function(val, meta, rec, rowIdx, colIdx, store, view, group, id) {
        var htmlTpl = "<input style='cursor:pointer;' type='radio' name='{3}' id='{4}{0}' value='{1}' {2}>",
            isChecked = val ? "checked": "",
            html = Ext.String.format(htmlTpl, rowIdx, val, isChecked, group, id);
            
        // CG: 12/28/2015 - A bug in ExtJS 5 which never happened in 4, when rec is set in the cellclick, meta is null
        //                - fortunately, this is a one time requirement, so no issues with the workaround
        if(meta) {
            meta.align = "center";
            meta.tdAttr = "celltype=" + id;
            meta.tdStyle = 'cursor:pointer;';
        } 
        return html;
    },

    tagRenderer: function(val, meta, rec, rowIdx, colIdx, store, view) {
        var me = this,
            menuItem;

        // CG: 12/30/2015 - this might be necessary anymore, will need to re-evaluate with testing        
        if(!val && me.editorType == "combo") {
            return "Select ..."
        }
        
        if(me.editorType == "combo") {
            menuItem = me.getMenuItem(val);
            return menuItem.text;    
        }
        return val;
    },


    // ORIGINAL
    actionRenderer: function(val, meta, rec, rowIdx, colIdx, store, view, group, id) {
        var count = store.count(),
            removeSpan = "<span class='actionFormat' title='Remove Row'>x</span>";

        // CG: 12/28/2015 - A bug in ExtJS 5 which never happened in 4, when rec is set in the cellclick, meta is null
        //                - fortunately, this is a one time requirement, so no issues with the workaround
        if(meta) {
            meta.align = "center";
            meta.tdAttr = "celltype=action";
        }

        return removeSpan;        
    },
    
    cellClickDispatch: function(gridview, td, cellIndex, record, tr, rowIndex, e) {

        var me = this,
            cellType = td.getAttribute('celltype'),
            store = gridview.getStore(),
            length,
            x, rec, removeRec, menuItem, phone, msg;
        
        switch(cellType) {
            case "isPrimaryRadioButton": 
                if(e.target.type != "radio") {
                    Ext.get(e.target.parentElement).select('input[type=radio]').elements[0].checked = true;
                }
                record.set('isPrimary', true);
                record.commit();
                
                length = store.count();
                
                // CG: 12/28/2015 - the list of items is never more than 10 at best, so this is an efficient way to deal with uniqueness
                for(x = 0; x < length; x++) {
                    rec = store.getAt(x);
                    if(record.internalId != rec.internalId) {
                        rec.set('isPrimary', false);
                        rec.commit();
                    }            
                }        
                store.sync();
            break;
            
            case "action":
            
                if(me.editorType == "combo") {
                    menuItem = me.getMenuItem(record.data.tag);    
                }


                if(me.confirmDeletion) {
                    msg = [
                        "Do you wish to delete this record?",
                        me.tagTitle + ": " + record.data.tag
                    ].join("<br/>");
    
                    Ext.Msg.confirm("Deletion Warning", msg, function(answer) {
                        
                        var me = this.scope,
                            menuItem, store, record;
                        
                        if(answer == "yes") {
                            
                            menuItem = this.menuItem;
                            store = this.store;
                            record = this.record;

                            me.removeItem(menuItem, store, record);
                        }
                    }, {
                        menuItem: menuItem,
                        store: store,
                        record: record,
                        scope: me
                    });
                    
                } else {
                    me.removeItem(menuItem, store, record);
                }
                
            break;

        }
    },
    
    removeItem: function(menuItem, store, record) {
        var me = this;
    
        if(menuItem) {
            menuItem.show();    
        }
        
        store.remove(record);
        
    }
});