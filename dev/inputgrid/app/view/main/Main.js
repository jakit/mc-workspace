Ext.define('InputGrid.view.main.Main', {
    extend: 'Ext.panel.Panel',
    requires: [
        'InputGrid.view.main.MetaInfoGrid',
        'Ext.layout.container.Border',
        'Ext.layout.container.HBox',
        'Ext.layout.container.Anchor'
    ],
    xtype: 'app-main',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.layout = {
            type: 'border'
        };
        
        me.items = [{
            region: 'west',
            width: '41%',
            xtype: 'panel',
            layout: {
                type: 'hbox'
            },
            defaults: {
                flex: 1
            },
            items: [{
                xtype: 'metainfogrid',
                title: 'Phones',
                margin: "5",
                settings: {
                    editor: {
                        type: "combo",
                        data: [{
                            "tag": "officePhone1",
                            "label": "Office Phone 1"
                        }, {
                            "tag": "officePhone2",
                            "label": "Office Phone 2"
                        }, {
                            "tag": "officePhone3",
                            "label": "Office Phone 3"
                        }, {
                            "tag": "faxPhone1",
                            "label": "Fax Phone 1"
                        }, {
                            "tag": "faxPhone2",
                            "label": "Fax Phone 2"
                        }, {
                            "tag": "faxPhone3",
                            "label": "Fax Phone 3"
                        }, {
                            "tag": "cellPhone1",
                            "label": "Cell Phone 1"
                        }, {
                            "tag": "cellPhone2",
                            "label": "Cell Phone 2"
                        }, {
                            "tag": "cellPhone3",
                            "label": "Cell Phone 3"
                        }]
                    },
                    grid: {
                        fields: [{ "name": "number",     "type": "string"  }],
                        columns: [{
                            dataIndex: "number",
                            header: "Number",
                            width: 150,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter a number'
                            }
                        }],
                        useIsPrimary: true,
                        buttonLabel: "Add Row",
                        viewConfig: {
                            stripeRows: true
                        },
                        tagFieldWidth: 400,
                        tagFieldFlex: null
                    }
                    
                }
            }]
        }, {
            region: 'center',
            xtype: 'panel',
            layout: {
                type: 'anchor',
                anchor: '100%'
            },
            defaults: {
                anchor: '100%'
            },
            items: [{
                xtype: 'metainfogrid',
                title: 'Misc',
                margin: "5",
                settings: {
                    editor: {
                        type: "textfield"
                    },
                    grid: {
                        fields: [{ "name": "number",     "type": "string"  }],
                        columns: [{
                            dataIndex: "value",
                            header: "Value",
                            width: 150,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter a value'
                            }
                        }],
                        useIsPrimary: false,
                        tagTitle: "Key",
                        buttonLabel: "Add +",
                        tagFieldFlex: 1,
                        confirmDeletion: false
                    }
                    
                }
            }, {
                xtype: 'metainfogrid',
                title: 'Addresses',
                margin: "5",
                settings: {
                    editor: {
                        type: "combo",
                        data: [
                            { "tag": "address1", "label": "Address 1"},
                            { "tag": "address2", "label": "Address 2"},
                            { "tag": "address3", "label": "Address 3"}
                        ]
                    },
                    grid: {
                        fields: [
                            { "name": "address",   "type": "string"  },
                            { "name": "city",      "type": "string"  },
                            { "name": "state",     "type": "string"  },
                            { "name": "zip",       "type": "string"  }
                        ],
                        columns: [{
                            dataIndex: "address",
                            header: "Address",
                            width: 100,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter an address'
                            }
                        }, {
                            dataIndex: "city",
                            header: "City",
                            width: 100,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Enter a city'
                            }
                        }, {
                            dataIndex: "state",
                            header: "State",
                            width: 60,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'State'
                            }
                        }, {
                            dataIndex: "zip",
                            header: "Zip",
                            width: 60,
                            editor: {
                                xtype: 'textfield',
                                emptyText: 'Zip Code'
                            }
                        }],
                        useIsPrimary: true
                    }
                    
                }
            }]
        }];
        
        me.callParent(arguments);
    }

});