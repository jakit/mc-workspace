/* 
 
MetaInfoGrid
Author: Claude Gauthier
E-Mail: claude_r_gauthier@hotmail.com

xtype: 'metainfogrid'
extend: 'Ext.grid.Panel'

Goal

To provide a tool for managing meta information, either via a controlled list, or adhoc, via the tag column

Features

- can be added to any ExtJS layout
- Grid editor for Tag column dictates the 'Add' button behavior
  - as a combo, the add button is synchronize with a supplied list of restricted input
    - the tag combo editor for each row are synchronized with the add button list
  - as a textfield, the add button simply adds an empty row, nothing to synchronized, the metainfogrid has no restriction.
- Easy to delete a row, uses a confirmation box
- Adding a row in the grid automatically selected the tag field for editing
- can disable isPrimary field (defaults to enable)
- can change the tag title (defaults to "Tag")
- can change the add button text (defaults to "Add")
- can modify the grid's viewConfig
- can change the tag column's width either via tagFieldFlex or tagFieldWidth
- can disable confirmDeletion box (defaults to true to confirm a deletion of a row)


Configs definition for 'metainfogrid'

settings: {
    "editor": {
        "type": "combo" or "textfield" (MANDATORY)
        "data": [
            { "tag": "", "label": ""}  when using combo it is MANDATORY
        ]
    },
    "grid": {
        "fields": [] // fields which will be added after the 'tag' field, as per Ext.data.Model fields definition (MANDATORY)
        
        "columns": [] // columns which will be added after the 'tag' column as per columns definition of Ext.grid.Panel (MANDATORY)
        
        "useIsPrimary": true or false, defaults to true (OPTIONAL)
        
        "tagTitle": string, defaults to "Tag" (OPTIONAL)
        
        "buttonLabel": string, defaults to "Add" (OPTIONAL)
        
        "viewConfig: object, defaults to:   {
                                                stripeRows: false,
                                                trackOver: false
                                            } (OPTIONAL)
                                            
        "tagFieldWidth": number, defaults to 140 // tagFieldFlex takes precendence over tagFieldWidth if it's set as a truthy value (OPTIONAL)
        
        "tagFieldFlex": number, defaults to null // when setting tagFieldFlex with a truthy value, it will take precedence over tagFieldWidth (OPTIONAL)
        
        "confirmDeletion": boolean, defauls to true (OPTIONAL)
    }
    
}
 
*/

Ext.define('InputGrid.view.main.MetaInfoGrid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.plugin.CellEditing',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar',
        'Ext.menu.Menu',
        'Ext.window.MessageBox'
    ],

    mixins: [
        'InputGrid.mixins.view.MetaInfoGrid'
    ],

    xtype: 'metainfogrid',

    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },

    settings: null,
    comboModel: null,
    comboStore: null,
    gridModel: null,
    gridStore: null,
    gridModelFields: null,
    useIsPrimary: true,
    editorType: null,
    tagTitle: 'Tag', // default header value for grid's tag column
    buttonLabel: "Add", // default header value button
    confirmDeletion: true, // set confirmation deletion box, default to true 
    
    cellEditPlugin: null,
        
    initComponent: function() {
        var me = this,
            editor;
        
        me.useIsPrimary = me.settings.grid.useIsPrimary;
        me.editorType = me.settings.editor.type;

        me.plugins = {
            ptype: 'cellediting',
            clicksToEdit: 1,
            listeners: {
                edit: {
                    fn: me.cellEditFunction,
                    scope: me
                }
            }
        };
       
        me.setConfirmDeletion();
        me.setTagTitle();
        me.setAddButtonLabel();
        me.setViewConfig();
        me.generateComboEditorStore();
        me.generateGridStore();        
        editor = me.getEditor();                
        me.setGridColumns(editor);

        me.disableSelection = true;
        me.cls = 'actionGrid';
         
        me.listeners = {
            cellclick: {
                fn: me.cellClickDispatch
            },
            titlechange: {
                fn: me.setSplitButton
            },
            afterrender: {
                fn: me.setSplitButtonHolder
            }
        };

        me.callParent(arguments);
    }

});