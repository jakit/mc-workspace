/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('ListToDetail.view.main.Main', {
    extend : 'Ext.container.Container',
    requires : [
        'ListToDetail.view.main.MainController', 
        'ListToDetail.view.main.MainModel', 
        'ListToDetail.view.component.container.ListToDetail', 
        'ListToDetail.view.component.container.GridToForm',
        'Ext.form.CheckboxGroup',  // required to build RadioGroup correctly // an ExtJS 5.x bug
        'Ext.form.RadioGroup',
        'Ext.menu.Menu'
    ],
    xtype : 'app-main',
    controller : 'main',
    viewModel : {
        type : 'main'
    },
    constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;

        me.layout = {
            type : 'border'
        };

        var msg =  '<ul>';
        msg += '<li>List To Detail Demo - This is a demo using the ListToDetail base class allowing to switch between 2 layouts.</li>';
        msg += '<li>Grid to Form Demo - This is a demo using the GridToForm base class which is based on ListToDetail allowing the switching between a grid and a form with selection.</li>'
        msg += '</ul>';

        var gridMenu = new Ext.menu.Menu({
            items: [{
                text: 'Item 1',
                listeners: {
                    'click': {
                        fn: function() {
                            alert("item 1");
                        },
                        scope: me
                    }
                    
                }
            }, {
                text: 'Item 2',
                listeners: {
                    'click': {
                        fn: function() {
                            alert("item 2");
                        },
                        scope: me
                    }
                    
                }
            }, {
                text: 'Sub Menu',
                menu: {
                    items: [{
                        text: 'Sub Menu 1',
                        listeners: {
                            'click': {
                                fn: function() {
                                    alert("Sub Menu 1");
                                },
                                scope: me
                            }
                            
                        }
                        
                    }]
                }
            }]
        });

        me.items = [{
            xtype : 'panel',
            bind : {
                title : '{name}'
            },
            region : 'west',
            html : msg,
            width : 250,
            split : true
        }, {
            region : 'center',
            xtype : 'tabpanel',
            items : [{
                title : 'List To Detail Demo',
                xtype : 'cmp-container-listtodetail',
                layoutFocus : 'listLayout', // 0 or listLayout (default) // 1 or 'detailLayout',
                listLayout : {
                    xtype : 'panel',
                    title : 'List Layout',
                    items : [{
                        xtype : 'radiogroup',
                        fieldLabel : 'Two Columns',
                        // Arrange radio buttons into two columns, distributed vertically
                        columns : 2,
                        vertical : true,
                        defaults : {
                            margin : "2"
                        },
                        items : [{
                            boxLabel : 'Item 1',
                            name : 'rb1',
                            inputValue : '1'
                        }, {
                            boxLabel : 'Item 2',
                            name : 'rb1',
                            inputValue : '2',
                            checked : true
                        }, {
                            boxLabel : 'Item 3',
                            name : 'rb1',
                            inputValue : '3'
                        }, {
                            boxLabel : 'Item 4',
                            name : 'rb1',
                            inputValue : '4'
                        }, {
                            boxLabel : 'Item 5',
                            name : 'rb1',
                            inputValue : '5'
                        }, {
                            boxLabel : 'Item 6',
                            name : 'rb1',
                            inputValue : '6'
                        }],
                        listeners : {
                            "change" : {
                                fn : function(grp, newV, oldV) {
                                    var me = grp.up('cmp-container-listtodetail');
                                    me.setData(newV.rb1);
                                },
                                scope : me
                            },
                            "beforerender" : {
                                fn : function(grp) {
                                    var val = grp.getValue();
                                    me = grp.up('cmp-container-listtodetail');
                                    me.setData(val.rb1);
                                },
                                scope : me
                            }
                        }
                    }],
                    bbar : {
                        xtype : 'toolbar',
                        items : ["->", {
                            xtype : 'button',
                            text : 'Switch to Detail',
                            listeners : {
                                'click' : {
                                    fn : function(btn) {
                                        var me = btn.up('cmp-container-listtodetail');
                                        me.switchPanel(1);
                                        // 0 is listLayout and 1 is for detailLayout
                                        var data = me.getData();
                                        var detailLayoutPanel = me.down('panel[itemId="detailLayout"]');
                                        detailLayoutPanel.update(data);
                                    }
                                }
                            }
                        }]
                    }
                }
            }, {
                title : 'Grid To Form Demo',
                xtype : 'cmp-container-gridtoform',
                contextMenu: gridMenu,
                detailOnSingleRecord: true,
                listLayout : {
                    store: 'GridToForm',
                    columns : [{
                        dataIndex : 'id',
                        header : 'ID',
                        flex : 1
                    }, {
                        dataIndex : 'label',
                        header : 'LABEL',
                        flex : 1
                    }, {
                        dataIndex : 'value',
                        header : 'VALUE',
                        flex : 1
                    }]
                },
                detailLayout : {
                    items : [{
                        fieldLabel : 'ID',
                        name : 'id',
                        itemId : 'id'
                    }, {
                        fieldLabel : 'LABEL',
                        name : 'label',
                        itemId : 'label'
                    }, {
                        fieldLabel : 'VALUE',
                        name : 'value',
                        itemId : 'value'
                    }]
                }
            }]
        }];

        me.callParent(arguments);

    }
});
