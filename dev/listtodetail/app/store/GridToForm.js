Ext.define('ListToDetail.store.GridToForm', {
    extend: 'Ext.data.Store',
    model: 'ListToDetail.model.GridToForm',
    data: [{
        id: '1',
        label: 'CBS',
        value: 'Person Of Interest'
    }, {
        id: '2',
        label: 'NBC',
        value: 'The BlackList'
    }, {
        id: '3',
        label: 'ABC',
        value: 'Once Upon A Time'
    }]
});

/*
 * 
 * , {
        id: '2',
        label: 'NBC',
        value: 'The BlackList'
    }, {
        id: '3',
        label: 'ABC',
        value: 'Once Upon A Time'
    }
 */