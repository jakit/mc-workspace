Ext.define('ListToDetail.model.GridToForm', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id'
    }, {
        name: 'label'
    }, {
        name: 'value'
    }]  
});