/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('BuildInfo.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'BuildInfo'
    }

    //TODO - add data, formulas and/or methods to support your view
});