/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
// START - place this block of code before Ext.application in app.js
var appMode;
        
try{
    if(appTimestamp) {
        appMode = "production";
        console.log("appTimestamp: " + appTimestamp);
        console.log("majorVersion: " + majorVersion);
        console.log("minorVersion: " + minorVersion);                
    }
} catch(e) {
    appMode = "development";
}
finally {
    console.log("appMode: " + appMode);
}
// END

Ext.application({
    name: 'BuildInfo',

    extend: 'BuildInfo.Application',
    
    autoCreateViewport: 'BuildInfo.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to BuildInfo.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
