/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
Ext.application({
    name: 'UIFilter5',

    extend: 'UIFilter5.Application',
    
    autoCreateViewport: 'UIFilter5.view.main.Main'
	
    //-------------------------------------------------------------------------
    // Most customizations should be made to UIFilter5.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
