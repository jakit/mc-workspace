Ext.define('UIFilter5.view.filters.InjectHeaderFields', {
    
    // hideable: true, // or false // if its not hideable, then the toolbar doesn't appear on the right side of the grid (defaults to false)
    // hidden: false // or true // this is ignored when hideable is set to false (defaults to false)
    // position: "inner" or "outer" // defaults to inner
    // dockPosition: "right, top"
    headerFieldsConfig: {
        useToolbar: true,
        hideable: false,
        hidden: false,
        position: "inner",
        dockPosition: 'right', 
        backgroundColor: "#f5f5f5",
        borderColor: "#c1c1c1",
        buttons: null,
        extraButtons: null
    },
    getHeaderFieldsConfig: function() {
        return this.headerFieldsConfig;        
    },
    injectHeaderFields: function(obj) {
        var me = this,
            hideable, hidden, position, backgroundColor, borderColor, useToolbar, dockPosition, extraButtons,
            columns, x, l, y, ll, itemsCount = 0, column, z, rbarItem, HFToolbar, toolbars = [], buttons, z, lll;
            
        if(obj && Ext.isObject(obj) && Ext.Object.getSize(obj) > 0) {
            Ext.apply(me.headerFieldsConfig, obj);
        }
        
        extraButtons = me.headerFieldsConfig.extraButtons;
        dockPosition = me.headerFieldsConfig.dockPosition;
        useToolbar = me.headerFieldsConfig.useToolbar;
        hideable = me.headerFieldsConfig.hideable;
        hidden = me.headerFieldsConfig.hidden;
        position = me.headerFieldsConfig.position;        
        backgroundColor = me.headerFieldsConfig.backgroundColor;
        borderColor = me.headerFieldsConfig.borderColor;
        buttons = me.headerFieldsConfig.buttons;

        // this needs to happen
        if(Ext.isArray(me.columns)) {
            columns = me.columns;
            
            if(Ext.isObject(me.defaults)) {
                l = columns.length;
                for(x = 0; x < l; x++) {
                    if(columns[x].headerField) {
                        Ext.applyIf(columns[x], me.defaults);
                    } else {
                        Ext.apply(columns[x], me.defaults);    
                    }
                    if(columns[x].headerField) {
                        itemsCount = Math.max(itemsCount, columns[x].headerField.length);    
                    }
                }
            }
        } else {
            l = me.columns.items.length;
            columns = [];
            
            for(x = 0; x < l; x++) {
                if(me.columns[x].headerField) {
                    Ext.applyIf(me.columns.items[x], me.columns.defaults);
                } else {
                    Ext.apply(me.columns.items[x], me.columns.defaults);    
                }
                columns.push(me.columns.items[x]);
                if(columns[x].headerField) {
                    itemsCount = Math.max(itemsCount, columns[x].headerField.length);    
                }
            }
        }
       
        for(x = 0; x < l; x++) {
            column = columns[x];
            ll = (column.headerField) ? column.headerField.length : 0;
            column.items = [];
            if(ll == 0) {
               for(y = 0; y < itemsCount; y++) {
                   column.items.push({
                       xtype: 'label'
                   });
               } 
            } else {
                if(ll == itemsCount) {
                   for(y = 0; y < ll; y++) {
                       column.items.push(column.headerField[y]);
                   }                     
                } else {
                   for(y = 0; y < ll; y++) {
                       column.items.push(column.headerField[y]);
                   }                     
                   for(z = y; z < itemsCount; z++) {
                       column.items.push({
                           xtype: 'label'
                       });
                   }                    
                }
            }
        } 
        me.columns = columns;

        // toolbar is optional        
        if(useToolbar) {

            if(hideable == false) {
                hidden = false;
            }
    
            var collapseExpandButton = {
                xtype: 'button',
                iconCls: (hidden) ? "filters-toolbar-expand" : "filters-toolbar-collapse",
                tooltip: (hidden) ? "Expand Filter Bar" : "Collapse Filter Bar",
                rowHiddenState: hidden,
                listeners: {
                    click: {
                        fn: function(btn) {
                            var t, tp, tb, items, x, l, multi;
                            
                            btn.rowHiddenState = !btn.rowHiddenState;
                            t = (btn.rowHiddenState) ? "filters-toolbar-expand" : "filters-toolbar-collapse";
                            btn.setIconCls(t);
                            tp = (btn.rowHiddenState) ? "Expand Filter Bar" : "Collapse Filter Bar";
                            btn.setTooltip(tp);
                            tb = btn.up('toolbar');
                            items = tb.items.items;
                            l = items.length; 
                            
                            me.HFAPI.rowHiddenState = btn.rowHiddenState;
                            multi = me.HFAPI.getFieldRegistrationSize();
                            me.clearFilters();
                            if(btn.rowHiddenState) {
                                if(multi) {
                                    me.HFAPI.hideRow(1);
                                }
                                me.HFAPI.hideRow(0);
                            } else {
                                me.HFAPI.showRow(0);
                                if(multi) {
                                    me.HFAPI.showRow(1);
                                }
                            }
                        },
                        scope: me                        
                    }
                }
            };
    
            var placeHolder = {
                xtype: 'box',
                width: 16,
                height: 24,
                html: '&nbsp;'
            };
            
            var mainButton;
            
            if(dockPosition == "right") {
                
                if(hideable) {
                    collapseExpandButton.dockPosition = dockPosition;
                    mainButton = collapseExpandButton;
                } else {
                    mainButton = placeHolder;
                }
        
                HFToolbar = {
                    xtype: 'toolbar',
                    dock: dockPosition,
                    padding: "2",
                    style: ("background-color:" + backgroundColor + ";border:1px solid " + borderColor + "!important;"),
                    items: [mainButton, {
                        xtype: "tbseparator",
                        hidden: hidden
                    }] 
                };
                
                if(buttons) {
                    lll = buttons.length;
                    for(z = 0; z < lll; z++) {
                        Ext.apply(buttons[z], {hidden: hidden});
                    }
                    Ext.Array.push(HFToolbar.items, buttons);
                    
                    if(extraButtons) {
                        HFToolbar.items.push({
                            xtype: "tbseparator",
                            hidden: hidden
                        });
                    }
                }
                
                if(extraButtons) {
                    lll = extraButtons.length;
                    for(z = 0; z < lll; z++) {
                        Ext.apply(extraButtons[z], {hidden: hidden});
                    }
                    Ext.Array.push(HFToolbar.items, extraButtons);
                }
                
                if(me.rbar) {
                    rbarItem = me.rbar;
                    me.rbar = null;
                }
                
                if(!me.hasDockedItems()) {
                    me.dockedItems = [];
                } 
        
                if(rbarItem) {
                    me.dockedItems.push(Ext.apply(rbarItem, { dock: 'right'}));
                }
                
                if(position === "inner") {
                    me.dockedItems.push(HFToolbar);
                } else {
                    me.dockedItems.unshift(HFToolbar);
                }
                
            } else {

                HFToolbar = {
                    xtype: 'toolbar',
                    dock: dockPosition,
                    padding: "2",
                    style: ("background-color:" + backgroundColor + ";border:1px solid " + borderColor + "!important;"),
                    items: ["->"] 
                };

                if(extraButtons) {
                    lll = extraButtons.length;
                    for(z = 0; z < lll; z++) {
                        Ext.apply(extraButtons[z], {hidden: hidden});
                    }
                    Ext.Array.push(HFToolbar.items, extraButtons);
                    if(buttons || hideable) {
                        HFToolbar.items.push({
                            xtype: 'tbseparator',
                            hidden: hidden
                        });
                    }
                }

                if(buttons) {
                    lll = buttons.length;
                    for(z = 0; z < lll; z++) {
                        Ext.apply(buttons[z], {hidden: hidden});
                    }
                    Ext.Array.push(HFToolbar.items, buttons);
                    if(hideable) {
                        HFToolbar.items.push({
                            xtype: 'tbseparator',
                            hidden: hidden
                        });
                    }
                }

                if(hideable) {
                    collapseExpandButton.dockPosition = dockPosition;
                    HFToolbar.items.push(collapseExpandButton);    
                }   
                
                if(me.tbar) {
                    rbarItem = me.tbar;
                    me.tbar = null;
                }
                
                if(!me.hasDockedItems()) {
                    me.dockedItems = [];
                } 
        
                if(rbarItem) {
                    me.dockedItems.push(Ext.apply(rbarItem, { dock: 'right'}));
                }
                
                if(position === "inner") {
                    me.dockedItems.push(HFToolbar);
                } else {
                    me.dockedItems.unshift(HFToolbar);
                }
                             
            }
        }
            
        return {
            hidden: hidden
        };
        
   },
   hasDockedItems: function() {
        var me = this;
        if(me.dockedItems && me.dockedItems.length > 0) {
            return true;
        }
        return false;
               
   } 
  
   
});
