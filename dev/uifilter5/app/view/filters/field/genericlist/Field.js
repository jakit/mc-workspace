Ext.define('UIFilter5.view.filters.field.genericlist.Field', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
        'Ext.data.Store'
    ],
    xtype: 'filters-genericlistfield',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);        
    },
    grid: null,
    triggers: {
        clear: {
            weight: 1,
            cls: 'x-form-clear-trigger',
            handler: function() {
                this.setValue(null);
                this.grid.applyFilters();
            },
            scope: 'this'
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
        }
    },    
    inlineData: null,
    enableKeyEvents: true,
    cls: "fieldFilterCls",
    margin: "0",
    filterMode: "Equal",
    filterDataType: "string",
    initComponent: function() {
        var me = this;
        var inlineData = me.inlineData;

        if(Ext.isFunction(me.inlineData)) {
            inlineData = [];
        }
        
        me.store = Ext.create('Ext.data.Store', {
            fields: [{
                name: 'display',
                type: 'string'
            }, {
                name: 'val',
                type: 'string'
            }],
            data: inlineData
        });

        me.editable = false;
        me.valueField = "val";
        me.displayField = "display";
        me.queryMode = 'local';
        me.tpl = Ext.create('Ext.XTemplate',
            '<ul class="x-list-plain"><tpl for=".">',
                '<li role="option" class="x-boundlist-item">{display}</li>',
            '</tpl></ul>'
        );
        me.displayTpl = Ext.create('Ext.XTemplate',
            '<tpl for=".">',
                '{display}',
            '</tpl>'
        );

        me.listeners = {
            afterrender: {
                fn: function(cmp) {
                    var me = this;
                    if(Ext.isFunction(me.inlineData)) {
                        Ext.Function.bind(me.inlineData, me)();
                    }
                },
                scope: me
            },
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;                    
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            },
            select: {
                fn: function(cmp) {
                    me.grid.applyFilters();
                },
                scope: me
            }
        };
        
        me.callParent(arguments);
        
    }
});
