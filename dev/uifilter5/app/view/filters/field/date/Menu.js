Ext.define('UIFilter5.view.filters.field.date.Menu', {
    extend: 'Ext.menu.Menu',
    xtype: 'filters-datefield-menu',
    requires: [
        'Ext.menu.Item',
        'UIFilter5.view.filters.field.date.MenuViewController'
    ],
    controller: "datemenu",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    initComponent: function() {
        var me = this;
        
        me.width = 27;
        me.padding = 0;
        me.margin = 0;
        
        me.items = [{
            iconCls: 'ondate-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'On'
        }, {
            iconCls: 'after-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: "After"
        }, {
            iconCls: 'before-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Before'
        }, {
            iconCls: 'between-trigger',
            xtype: 'menuitem',
            width: 27,
            height: 25,
            text: 'Between'
        }];
        
        me.callParent(arguments);
    }
});
