Ext.define('UIFilter5.view.filters.field.date.Field2', {
    extend: 'Ext.form.field.Date',
    xtype: 'filters-datefield2',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    grid: null,
    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function(field, triggerEl, evt) {
                field.setValue(null);
                field.grid.applyFilters();
            },
            weight: 1
        },
        picker: {
            weight: 2,
            handler: 'onTriggerClick',
            scope: 'this'
        }
    },  
    enableKeyEvents: true,
    margin: "0",
    fieldLabel: null,
    cls: "fieldFilterCls",
    editable: false,
    initComponent: function() {
        var me = this;
        me.listeners = {
            select: {
                fn: function() {
                    this.grid.applyFilters();
                },
                scope: me
            },            
            render: {
                fn: function(cmp) {
                    var clearTrig, clearEl;                    
                    clearTrig = cmp.getTrigger('clear');
                    clearEl = clearTrig.el;
                    clearEl.dom.setAttribute('data-qtip', 'clear field');
                }
            }
        };
        me.callParent(arguments);
    }    
});