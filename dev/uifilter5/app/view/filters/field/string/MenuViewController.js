Ext.define('UIFilter5.view.filters.field.string.MenuViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.stringmenu',
    init: function(view) {
        this.control({
            'menuitem': {
                'click': {
                    fn: function(cmp) {
                        var me = this,
                            allTriggers = [
                                'textequal-trigger',
                                'starts-with-trigger',
                                'ends-with-trigger',
                                'contains-trigger'
                            ],
                            tips = {
                                'starts-with-trigger': "Starts With",
                                'ends-with-trigger': "Ends With",
                                'contains-trigger': "Contains",
                                'textequal-trigger': "Equal"
                            },
                            el = view.curTrigger.el,
                            x = 0,
                            l = allTriggers.length;
                        
                        for(x = 0; x < l; x++) {
                            el.removeCls(allTriggers[x]);    
                        }
                        el.addCls(cmp.iconCls);
                        me.thisField.filterMode = tips[cmp.iconCls];
                        view.curTrigger.el.dom.setAttribute('data-qtip', tips[cmp.iconCls]);
                        if(me.thisField.getValue()) {
                            me.thisField.grid.applyFilters();    
                        }
                        
                    },
                    scope: view
                }
            }
        });
    }
});