Ext.define('UIFilter5.view.filters.HeaderFields', {
    extend: 'Ext.grid.feature.Feature',
    alias: 'feature.headerfields',
    requires: [
        'Ext.form.field.*',
        'UIFilter5.view.filters.field.number.Field1',
        'UIFilter5.view.filters.field.number.Field2',
        'UIFilter5.view.filters.field.date.Field1',
        'UIFilter5.view.filters.field.date.Field2',
        'UIFilter5.view.filters.field.string.Field',
        'UIFilter5.view.filters.field.genericlist.Field',
        'UIFilter5.view.filters.field.combocheckbox.Field'         
    ],
    eventPreventHandler: function(e) {
        e.stopPropagation();  
    },
    constructor: function(cfg) {
       var me = this;
       Ext.apply(me, cfg);
       me.callParent(arguments);
    },
    cfg: null,  
    init: function(grid) {
        var me = this,
            callback = null;
        
        var hidden = me.cfg.hidden;
        var columns, x, l;
        if(hidden) {
            Ext.suspendLayouts();
            columns = grid.columns;
            l = columns.length;
            for(x = 0; x < l; x++) {
               columns[x].down('component').hide();
            }
            Ext.resumeLayouts();
        }
        
        if(!grid.listeners) {
            grid.listeners = {};
        } else {
            if(grid.listeners.columnresize) {
                if(grid.listeners.columnresize.fn) {
                    if(grid.listeners.columnresize.scope) {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid.listeners.columnresize.scope);
                    } else {
                        callback = Ext.Function.bind(grid.listeners.columnresize.fn, grid);
                    }
                } else {
                    callback = Ext.Function.bind(grid.listeners.columnresize, grid);    
                }
            }
        }
        
        me.setAPI(grid);
        
        Ext.apply(grid.listeners, {
            'columnresize': {
                fn: Ext.Function.bind(me.onColumnResizeFunc, grid, [callback], true), 
                scope: me
          }
        });         
    
    },
    onColumnResizeFunc: function(headerCt, targetColumn, width, opts, callback) {
        
        var me = this,
            items = Ext.ComponentQuery.query("component", targetColumn),
            x, l = items.length, el;
                    
            for(x = 0; x < l; x++) {
                items[x].setWidth(width);
                // CG: 05/14/2016 - HACK!
                el = items[x].getEl().select('.x-form-trigger-wrap');
                if(el.elements.length > 0) {
                    Ext.get(el.elements[0]).setWidth(width);
                }
            }
            if(callback) {
                callback();
            }   
       
    },
    setAPI: function(grid) {

        var init = Ext.Function.bind(function() {
            var grid = this;
            var result = [];
            var rows = [];
            var columns = grid.getColumns();
            var x, l = columns.length, column, obj;
            var multipleRows = false;
            var fieldRegistrationObj = {};
            for(x = 0; x < l; x++) {
                column = columns[x];
                obj = Ext.ComponentQuery.query('component', column);
                result.push(obj);
                for(y = 0; y < obj.length; y++) {
                    if(obj[y].triggers && obj[y].triggers.filtermode) {
                        if(obj[y].triggers.filtermode.cls == "between-trigger") {
                            multipleRows = true;
                        }
                    }
                    if(!Ext.isArray(rows[y])) {
                        rows[y] = [];
                    };

                    if(obj[y].componentCls == "x-field") {
                        rows[y].push(obj[y]);    
                    }
                }
            };
            
            return {
                getField: function(dataIndex, idx) {
                    var column = grid.down('gridcolumn[dataIndex="'+ dataIndex +'"]');
                    var obj = Ext.ComponentQuery.query('field', column);
                    if(obj && obj.length > -1 && Ext.isNumber(idx) && (idx < obj.length)) {
                        return obj[idx];
                    }
                    return obj;
                },
                getFields: function(dataIndexArr) {
                    var result = [];
                    var x, l, column;
                    if(Ext.isArray(dataIndexArr) && dataIndexArr.length > 0) {
                        l = dataIndexArr.length;
                        for(x = 0; x < l; x++) {
                            result.push(grid.HFAPI.getField(dataIndexArr[x]));
                        }
                    }
                    return result;
                },
                getAllFields: function() {
                    return result;
                },
                getFieldsForRow: function(r) {
                    var x, l;
                    if(rows[r]) {
                        return rows[r];                        
                    }
                    return [];
                },            
                hideRow: function(r) {
                    var x, l;
                    if(rows[r]) {
                        l = rows[r].length;
                        for(x = 0; x < l; x++) {
                            if(rows[r][x]) {
                                rows[r][x].hide();    
                            }
                        }                        
                    }
                },
                showRow: function(r) {
                    var x, l;
                    if(rows[r]) {
                        l = rows[r].length;
                        for(x = 0; x < l; x++) {
                            if(rows[r][x]) {
                                rows[r][x].show();    
                            }
                        }                        
                    }
                },
                getMultipleRows: function() {
                    return multipleRows;
                },
                getFieldRegistrationSize: function() {
                    return Ext.Object.getSize(fieldRegistrationObj);
                },
                registerField: function(field) {
                    fieldRegistrationObj[field] = true;
                },
                unRegisterField: function(field) {
                    delete fieldRegistrationObj[field]; 
                },
                getFieldRegistrationObj: function() {
                    return fieldRegistrationObj;
                }
            };
        }, grid);

        grid.HFAPI = grid.HeaderFieldsAPI = init();

    } 
});