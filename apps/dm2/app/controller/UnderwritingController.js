Ext.define('DM2.controller.UnderwritingController', {
    extend: 'Ext.app.Controller',

	requires:[],
	
    refs: {
        underwriting: {
            autoCreate: true,
            selector: 'underwriting',
            xtype: 'underwriting'
        }
    },
	
	init: function(application) {
        this.control({
		    "#propertycombo": {
				select: 'onPropertycomboSelect'
			},
			"#underwriting": {
				close: 'onUnderwritingClose',
				afterrender: 'loadUnderWritingDetails'
			},
			'underwriting button[action="saveActionsBtn"]':{
                click: this.saveActionsBtnClick
            },
			'underwriting button[action="doneActionBtn"]': {
                click: 'onUnderwritingDoneBtnClick'
            }
        });
    },

    UnderwritingClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this,underwriting;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('underwriting')){
			detailpanel.down('underwriting').destroy();
		}
		if(detailpanel.down('underwriting')){
			console.log("Use Old Panel");
			underwriting = detailpanel.down('underwriting');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	underwriting = Ext.create('DM2.view.Underwriting',{
								closable : true
						   });//me.getUnderwriting();
		}
        eastregion.getLayout().setActiveItem(underwriting);
		underwriting.setTitle("Activities > "+activitytext);
		underwriting.setMargin(3);
		underwriting.setCollapsible(false);
		//underwriting.setConfig('closable',true);
		underwriting.down('button[action="doneActionBtn"]').show();
        //underwriting.down('#activityid').setValue(activityid);
    },
	
	onUnderwritingClose: function(panel, eOpts) {		
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },
	
	loadUnderWritingDetails: function(panel){
		var me = this;
		
		var underwritingpanel = panel;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
	
        var record = context.record;
        var dealid = record.data.idDeal;
		
		me.loadDealProperty(dealid,context);
		
        //me.loadUnderwrittingActions(context);
	},
	
	loadDealProperty: function(dealid,context){
		var me = this;
		var propertycombo = context.down('underwriting').down('#propertycombo');  
        var dealpropertyst = propertycombo.getStore();
		
		//Load Properties
		//var dealProperties = context.down('dealdetailpropertyfieldset').down('grid').getStore().getData();        
		//dealpropertyst.loadData(dealProperties.items);
		
        dealpropertyst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealpropertyst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				var dealpropcnt = dealpropertyst.getCount();
				if(dealpropcnt>0){
					//Get the First Record
					var proprec = dealpropertyst.getAt(0);
					propertycombo.select(proprec);
					propertycombo.fireEvent('select',propertycombo,proprec);
				}
			}
		});
	},
	
	onPropertycomboSelect: function(combo, record, eOpts) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(combo);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);

        console.log("Fire select event");
        var propdtfrm = detailpanel.down('underwriting').down('#propertydetailfrm');
        //propdtfrm.getForm().loadRecord(record);
		propdtfrm.getForm().findField('street_no').setRawValue(record.get('street_no'));
		propdtfrm.getForm().findField('city').setRawValue(record.get('city'));
		propdtfrm.getForm().findField('propertytype').setRawValue(record.get('propertytype'));
		propdtfrm.getForm().findField('street').setRawValue(record.get('street'));
		propdtfrm.getForm().findField('state').setRawValue(record.get('state'));
		propdtfrm.getForm().findField('zip').setRawValue(record.get('zip'));
	    me.addUnderwrittingActions(context);
    },
	
    //loadUnderwrittingActions: function(context) {
	loadUnderwrittingActions: function() {		
        var me = this;
		//var underwriting = context.down('underwriting');
        //var actionpanel = context.down('underwriting').down('#actionspanel');

		//var underwrt_action_st = underwriting.underwrittingActionsStore; //Ext.getStore('UnderwrittingActions');	
		var underwrt_action_st = Ext.getStore('UnderwrittingActions');	
        underwrt_action_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        //actionpanel.setLoading(true);
        underwrt_action_st.load({
            scope: this,
            callback: function(records, operation, success) {
                /*actionpanel.setLoading(false);
                if(success)
                {
                    me.addUnderwrittingActions(context);
                }*/
            }
        });
    },

    addUnderwrittingActions: function(context) {
        var me = this,backview = "";
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var underwriting = context.down('underwriting');
		if(underwriting.up('dealdetailquotepanel')){
			backview = "loanmanager";
		} else if(underwriting.up('quotedetailspanel')){
			backview = "loaneditor";
		} else {
			backview = "underwritingactivity";
		}
		//var underwrt_action_st = underwriting.underwrittingActionsStore; //Ext.getStore('UnderwrittingActions');
		var underwrt_action_st = Ext.getStore('UnderwrittingActions');
        var undwrtactns = underwrt_action_st.getCount();
        if(undwrtactns > 0){
            var actionpanel = detailpanel.down('underwriting').down('#actionspanel');
            actionpanel.removeAll(true);
            underwrt_action_st.each(function(record) {
                var actnameval = record.get('actionName');
                var actshrtnameval = record.get('actionShrtName');
                var idUnderwritingActions = record.get('idUnderwritingActions');

                var actionfrm = Ext.create('DM2.view.UnderwrittingActionForm',{
                    itemId:actshrtnameval,
                    actionid:idUnderwritingActions
                });
                actionpanel.add(actionfrm);
                console.log(actionfrm.getItemId());
                actionfrm.down('fieldset').setTitle(actnameval);
				
				var addexecbtn = actionfrm.down('button[action="addexecnamebtn"]');
				addexecbtn.on('click', function() {
					console.log("Add Contact Button Clicked");
					
					var eastregion = detailpanel.down('[region=east]');
					var underwriting;
					
					if(detailpanel.down('adddealcontactform')){
						adddealcontactform = detailpanel.down('adddealcontactform');
					} else {
						//create new Activity Detail Panel
						adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
					}
					eastregion.getLayout().setActiveItem(adddealcontactform);					
					adddealcontactform.config.utactionfrm = actionfrm;
					adddealcontactform.config.backview = backview;
					
					///Load Contacts
					var filterstarr = [];
					if(actnameval!==''){
						var filterst = "contactType = '"+actnameval.toUpperCase()+"'";
						filterstarr.push(filterst);
						actnameval = actnameval.toUpperCase();
					}
					me.loadUwSelContacts(actnameval,adddealcontactform,'contactType');
				});
                actionfrm.getForm().findField('idUnderwritingActions').setRawValue(idUnderwritingActions);
            }, this);
        }
        //
        var propcombo = underwriting.down('#propertycombo');
        var propid = propcombo.getValue();
		console.log("propid"+propid);
        me.loadUnderWritting(propid,context);
    },

	loadUwSelContacts: function(cnttypeval,selcntpanel,fieldName) {
        var me = this;
        var uwcontactallselbuff_st = selcntpanel.down('grid').getStore();
		var uwcontactallselbuff_stproxy = uwcontactallselbuff_st.getProxy();
        /*uwcontactallselbuff_stproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var contactType = selcntpanel.down('grid').down('[dataIndex=contactType]');
		contactType.show();
		var uwcontactallselbuff_stproxyCfg = uwcontactallselbuff_stproxy.config;
        uwcontactallselbuff_stproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		uwcontactallselbuff_st.setProxy(uwcontactallselbuff_stproxyCfg);	
		
        uwcontactallselbuff_st.load({
            callback: function(records, operation, success){
				var submcntselgrid  = selcntpanel.down('grid');
				var field;
				if(fieldName=="contactType"){
					field = submcntselgrid.HFAPI.getField('contactType');
				}else if(fieldName=="companyName"){
					field = submcntselgrid.HFAPI.getField('companyName');
				}
				//console.log(field);
				if(fieldName!="nofilter"){
					field[0].setValue(cnttypeval);				
				}
				//cmpname.setValue(cnttypeval);
			}
        });
    },
	
    loadUnderWritting: function(propertyid,context) {
        console.log("Load Under Writting with propid & dealid.");
        var me = this;
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var actionpanel = detailpanel.down('underwriting').down('#actionspanel');
		
        var filterstarr = [];
    
		if(propertyid!==''){
			var filterst = "idDealxProp = '"+propertyid+"'";
            filterstarr.push(filterst);
        }

        actionpanel.setLoading(true);
		
		var underwriting = context.down('underwriting');
		var underwrt_st = underwriting.underwritingStore; //Ext.getStore('Underwriting');
        underwrt_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        underwrt_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                actionpanel.setLoading(false);
                if(success)
                {
                    var undwrtcnt = underwrt_st.getCount();
                    if(undwrtcnt > 0){
                        underwrt_st.each(function(record) {
                            var idUnderwritingActionsval = record.get('idUnderwritingActions');
                            var actionfrms = actionpanel.query('form');
                            for(var i=0;i<actionfrms.length;i++){
                                var actfrm = actionfrms[i];
                                var underwritingActionsid = actfrm.getForm().findField('idUnderwritingActions').getValue();
                                if(idUnderwritingActionsval===underwritingActionsid){
                                    console.log("Action Saved & Matched,So Load the form");
                                    //actfrm.getForm().loadRecord(record);
									var firstName = record.get('firstName');
									var lastName  = record.get('lastName');
									var fullName = firstName+' '+lastName;
									actfrm.getForm().findField('activityExecutorLastName').setRawValue(fullName);
									
									if(record.get('scheduledActivityDate')!=null){
										var sadt = Ext.Date.format(record.get('scheduledActivityDate'), "m-d-Y");
										actfrm.getForm().findField('scheduledActivityDate').setRawValue(sadt);
									}
									if(record.get('activityCompleteDate')!=null){
										var acdt = Ext.Date.format(record.get('activityCompleteDate'), "m-d-Y");
										actfrm.getForm().findField('activityCompleteDate').setRawValue(acdt);
									}
									
									actfrm.getForm().findField('activityComplete').setRawValue(record.get('activityComplete'));
									actfrm.getForm().findField('idContact').setRawValue(record.get('idContact'));
									actfrm.getForm().findField('idUnderwriting').setRawValue(record.get('idUnderwriting'));
									actfrm.getForm().findField('idUnderwritingActions').setRawValue(record.get('idUnderwritingActions'));
                                }
                            }
                        }, this);
                    }
                }
            }
        });
    },

    saveActionsBtnClick: function(btn) {
        console.log("Save Underwriting Actions");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var row = context.record;
       	var dealid = row.data.idDeal;
		
		var underwriting = detailpanel.down('underwriting');
        var actionpanel = underwriting.down('#actionspanel');
        var actionfrms = actionpanel.query('form');

        //Get Property Id
        var propcombo = underwriting.down('#propertycombo');
        var propid = propcombo.getValue();

        var actionarr = [];
        for(var i=0;i<actionfrms.length;i++){
            var actfrm = actionfrms[i];
            console.log(actfrm);
            var underwritingidval = actfrm.getForm().findField('idUnderwriting').getValue();

            var contactidval = actfrm.getForm().findField('idContact').getValue();
            var scheduledActivityDateval = actfrm.getForm().findField('scheduledActivityDate').getValue();
            var activityCompleteDateval = actfrm.getForm().findField('activityCompleteDate').getValue();
            var activityCompleteval = actfrm.getForm().findField('activityComplete').getValue();
            var idUnderwritingActionsval = actfrm.getForm().findField('idUnderwritingActions').getValue();

            if(underwritingidval===null || underwritingidval===0){
                console.log("Add Action");
                actionarr.push({
                    "@metadata": {
                        "entity": "Underwriting",
                        "action": "INSERT"
                    },
					'idDealxProp':propid,
                    'idUnderwritingActions':idUnderwritingActionsval,
                    'scheduledActivityDate':scheduledActivityDateval,
                    'activityCompleteDate':activityCompleteDateval,
                    'activityComplete':activityCompleteval,
                    'idContact':contactidval
                });
            }else{
                console.log("Edit Action");
                actionarr.push({
                    "@metadata": {
                        "href": DM2.view.AppConstants.apiurl+'Underwriting/'+underwritingidval,
                        "checksum": "override",
                        "action": "UPDATE"
                    },
					'idDealxProp':propid,
                    'scheduledActivityDate':scheduledActivityDateval,
                    'activityCompleteDate':activityCompleteDateval,
                    'activityComplete':activityCompleteval,
                    'idContact':contactidval
                });
            }
        }

        ///Do Save
        var methodname = "PUT";

        actionpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Underwriting',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(actionarr),
            success: function(response, opts) {
                actionpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                    console.log("Action Value Save.Now Refresh.");
                    me.loadUnderWritting(propid,context);
                } else {
                    console.log("Generate File Failed.");
                    Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                actionpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Generate File', obj.errorMessage);
            }
        });
    },
	saveContactToExecBtnClick: function(btn) {
		console.log("Save Contact To Executor Name.");
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var adddealcontactform = detailpanel.down('adddealcontactform');
		var selcntgrid = adddealcontactform.down('grid');
        if (selcntgrid.getSelectionModel().hasSelection()) {
            var row = selcntgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('contactid'));
            var contactid = row.get('contactid');
            var firstName = row.get('firstName');
			var lastName  = row.get('lastName');
			//var fullName = firstName+' '+lastName;
			var fullName  = row.get('name');
			if(adddealcontactform.config.backview=="quotedbyloanmanager" || adddealcontactform.config.backview=="quotedbyloaneditor"){
				var quotedetailspanel = detailpanel.down('quotedetailspanel');
				quotedetailspanel.getForm().findField('quoteIssuerName').setValue(fullName);
				quotedetailspanel.getForm().findField('quoteIssuer').setValue(contactid);
			} else if(adddealcontactform.config.backview=="letterAddrloanmanager" || adddealcontactform.config.backview=="letterAddrloaneditor"){
				var quotedetailspanel = detailpanel.down('quotedetailspanel');
				quotedetailspanel.getForm().findField('letterAddresseeName').setValue(fullName);
				quotedetailspanel.getForm().findField('letterAddressee').setValue(contactid);
			} else {
				var actionfrm = adddealcontactform.config.utactionfrm;
				actionfrm.getForm().findField('activityExecutorLastName').setRawValue(fullName);
				actionfrm.getForm().findField('idContact').setRawValue(contactid);
			}
			me.getController('DealDetailController').addDealContactCloseBtnClick(adddealcontactform);
        } else {
            Ext.Msg.alert('Failed', "Please select contact.");
            return false;
        }
	},
	
	onUnderwritingDoneBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	}
});