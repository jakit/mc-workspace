Ext.define('DM2.controller.DashboardController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.DealHistoryTab','DM2.view.AboutWindow'],
    refs: {
        dashboardcontainer: {
            autoCreate: true,
            selector: 'dashboardcontainer',
            xtype: 'dashboardcontainer'
        },
        usernametext  : '#usernametext',
		msgtext       : '#msgtext',
        dashboardview : 'dashboardcontainer #dashboardview',
        dealgrid      : 'tab-deal-grid',
		propertygrid  : 'tab-property-grid',
		bankgrid      : 'tab-bank-grid',
		contactgrid   : 'tab-contact-grid',
		aboutwindow   : 'aboutwindow',
		salesgrid   : 'tab-sales-grid',
		gotosalesdealfield: 'dashboardcontainer splitbutton textfield[itemId=gotosalefield]',
		maintabpanel : 'tab-panel'
    },

    init: function(application) {
         this.control({
			 'dashboardcontainer':{
				afterrender: this.afterDashboardContainerRender
			 },
			 'maintabpanel':{
				afterrender: this.afterMainTabPanelRender
			 },
             'dashboardcontainer #tasksbtn': {
                 click: this.showTasks
             },
             'dashboardcontainer #dealsbtn':{
                 click:this.dealBtnClick
             },
             'dashboardcontainer #dealmenu':{
                 click:this.dealMenuClick
             },
             'dashboardcontainer #logoutbtn':{
                 click:this.logOutBtnClick
             },
             'dashboardcontainer #propertiesbtn':{
                 click:this.showProperties
             },
             'dashboardcontainer #contactsbtn':{
                 click:this.showContacts
             },
             'dashboardcontainer #banksbtn':{
                 click:this.showBanks
             },
             'dashboardcontainer #reportbtn':{
                 click:this.showReports
             },
			 'dashboardcontainer #aboutbtn':{
                 click:this.aboutBtnClick
             },
             'dashboardcontainer #contactmenu':{
                 click:this.dealContactMenuClick
             },
             'dashboardcontainer #propmasterbtn':{
                 click:this.showPropertiesMaster
             },
			 'dashboardcontainer #salesbtn':{
                 click:this.showSales
             },
			 'dashboardcontainer #salemenu':{
                 click:this.saleMenuClick
             },
			 'gotosalesdealfield': {
                'keyup': this.goToSalesDealField
             }
         });
    },
	
	afterDashboardContainerRender: function(dctoolbar){
		var me = this,toolbaritem;
		
		toolbaritem = Ext.create('Ext.Img',{
			autoEl : {
				tag : 'a',
				href : '/DealMaker/',
				target : '_self'
			},
			height : 40,
			width : 230,
			src : 'resources/images/logo.png'
		});
		var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
		if(menuFunc==1){			
		} else {
			var menuFunc = me.getController('MainController').getMenuStatus("salemenu");
			if(menuFunc==1){
				toolbaritem = Ext.create('Ext.Img',{
					autoEl : {
						tag : 'a',
						href : '/DealMaker/',
						target : '_self'
					},
					height : 40,
					width : 230,
					src : 'resources/images/SaleLogoSmall.png'
				});
			}
		}
		dctoolbar.add(toolbaritem);
		
		toolbaritem = Ext.create('Ext.button.Button',{
			hidden : true,
			itemId : 'tasksbtn',
			ui : 'menubtnui-toolbar-medium',
			scale : 'medium',
			text : 'Tasks'
		});
		dctoolbar.add(toolbaritem);
		
		var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
		if(menuFunc==1){
			var toolbaritem = Ext.create('Ext.button.Split',{
				itemId : 'dealsbtn',
				ui : 'menubtnui-toolbar-medium',
				iconCls : 'dealbtn',
				scale : 'medium',
				text : 'Latest Deals',
				menu : {
					xtype : 'menu',
					itemId : 'dealmenu',
					items : [{
						itemId: 'allDeals',
						xtype : 'menuitem',
						text : 'History Deals',
						focusable : true
					},{
						itemId: 'latestDeals',
						xtype : 'menuitem',
						text : 'Latest Deals',
						focusable : true
					}, {
						itemId: 'myDeals',
						xtype : 'menuitem',
						text : 'My Deals',
						focusable : true
					}, {
						itemId: 'recentDeals',
						xtype : 'menuitem',
						text : 'All Deals',
						focusable : true
					}, {
						xtype : 'textfield',
						text: 'Go To Deal',
						itemId : 'gotodealfield',
						margin : '5 5 5 26',
						enableKeyEvents: true,
						ui : 'dealinfofield',
						width : 180,
						fieldLabel : 'Go To Deal',
						labelWidth : 75
					}]
				}
			});
			dctoolbar.add(toolbaritem);
		}
		
		var menuFunc = me.getController('MainController').getMenuStatus("salemenu");
		if(menuFunc==1){
			var toolbaritem = Ext.create('Ext.button.Split',{
				itemId : 'salesbtn',
				ui : 'menubtnui-toolbar-medium',
				iconCls : 'dealbtn',
				scale : 'medium',
				text : 'Sales',
				menu : {
					xtype : 'menu',
					itemId : 'salemenu',
					items : [{
						itemId: 'sales',
						xtype : 'menuitem',
						text : 'Sales',
						focusable : true
					}, {
						itemId: 'mySales',
						xtype : 'menuitem',
						text : 'My Sales',
						focusable : true
					}, {
						itemId: 'recentSales',
						xtype : 'menuitem',
						text : 'Recent Sales',
						focusable : true
					}, {
						xtype : 'textfield',
						hidden: true,
						text: 'Go To Sale',
						itemId : 'gotosalefield',
						margin : '5 5 5 26',
						enableKeyEvents: true,
						ui : 'dealinfofield',
						width : 180,
						fieldLabel : 'Go To Sale',
						labelWidth : 75
					}]
				}
			});
			dctoolbar.add(toolbaritem);
		}
		var toolbaritem = Ext.create('Ext.button.Button',{
			hidden : true,
            itemId : 'propertiesbtn',
            ui : 'menubtnui-toolbar-medium',
            iconCls : 'propertybtn',
            scale : 'medium',
            text : 'Properties'
		});
		dctoolbar.add(toolbaritem);
		var toolbaritem = Ext.create('Ext.button.Button',{
			itemId : 'propmasterbtn',
            ui : 'menubtnui-toolbar-medium',
            iconCls : 'propertybtn',
            scale : 'medium',
            text : 'Properties'
		});
		dctoolbar.add(toolbaritem);
		var toolbaritem = Ext.create('Ext.button.Split',{
			itemId : 'contactsbtn',
            ui : 'menubtnui-toolbar-medium',
            iconCls : 'contactbtn',
            scale : 'medium',
            text : 'Contacts',
			menuaction:'allContacts',
            menu : {
                xtype : 'menu',
                itemId : 'contactmenu',
                width : 140,
                items : [{
                    xtype : 'menuitem',
					itemId: 'allContacts',
                    text : 'All Contacts',
                    focusable : true
                }, {
                    xtype : 'menuitem',
					itemId: 'myContacts',
                    text : 'My Contacts',
                    focusable : true
                }/*, {
                    xtype : 'menuitem',
					itemId: 'recentContacts',
                    text : 'Recent Contacts',
                    focusable : true
                }*/]
            }
		});
		dctoolbar.add(toolbaritem);
		
		var menuFunc = me.getController('MainController').getMenuStatus("bankmenu");
		if(menuFunc==1){
			var toolbaritem = Ext.create('Ext.button.Button',{
				itemId : 'banksbtn',
				ui : 'menubtnui-toolbar-medium',
				iconCls : 'bankbtn',
				scale : 'medium',
				text : 'Banks'
			});
			dctoolbar.add(toolbaritem);
		}
		
		var toolbaritem = Ext.create('Ext.button.Button',{
			itemId : 'reportbtn',
			hidden:true,
            ui : 'menubtnui-toolbar-medium',
            scale : 'medium',
            text : 'Reports'
		});
		dctoolbar.add(toolbaritem);
		var toolbaritem = Ext.create('Ext.button.Button',{
			 itemId : 'aboutbtn',
            ui : 'menubtnui-toolbar-medium',
            scale : 'medium',
            text : 'About'
		});
		dctoolbar.add(toolbaritem);
		
		///Show Environment Button
		if(DM2.view.AppConstants.environment!=""){
			var styleval = "";
			if(DM2.view.AppConstants.environment=="DEV"){
				styleval = "background:#FFDF00;border-color:#FFDF00;";
			} else if(DM2.view.AppConstants.environment=="UAT"){
				styleval = "background:#FFFF00;border-color:#FFFF00;";
			} else if(DM2.view.AppConstants.environment=="PROD"){
				styleval = "background:#ADFF2F;border-color:#ADFF2F;";
			} else if(DM2.view.AppConstants.environment=="QA"){
				styleval = "background:#ADFF2F;border-color:#ADFF2F;";
			}
			var toolbaritem = Ext.create('Ext.button.Button',{
				itemId : 'envbtn',
				ui : 'menubtnui-toolbar-medium',
				scale : 'medium',
				style:styleval,
				text : DM2.view.AppConstants.environment
			});
			dctoolbar.add(toolbaritem);
		}
		///
		var toolbaritem = Ext.create('Ext.toolbar.TextItem',{
			itemId : 'msgtext'
		});
		dctoolbar.add(toolbaritem);
		var toolbaritem = Ext.create('Ext.toolbar.Fill');
		dctoolbar.add(toolbaritem);
		var toolbaritem = Ext.create('Ext.toolbar.TextItem',{
			itemId : 'usernametext',
            style : 'color:#fff;font-weight:bold;'
		});
		dctoolbar.add(toolbaritem);
		var toolbaritem = Ext.create('Ext.button.Button',{
			itemId : 'logoutbtn',
            ui : 'menubtnui-toolbar-medium',
            scale : 'medium',
            text : 'Log Out'
		});
		dctoolbar.add(toolbaritem);
		
		var toolbaritem = Ext.create('Ext.Img',{
			autoEl : {
				tag : 'a',
				href : '/DealMaker/',
				target : '_self'
			},
			height : 40,
			width : 146,
			src : 'resources/images/DealLogoSmall.png'
		});
		dctoolbar.add(toolbaritem);
	},
	
	afterMainTabPanelRender: function(tabpanel){
		var me = this,dealMenuFlag,saleMenuFlag;
		console.log("After Main tab Panel Render");
		dealMenuFlag = me.getController('MainController').getMenuStatus("dealmenu");
		if(dealMenuFlag==1){
			var tabitem = Ext.create('DM2.view.component.tab.Deals');
			tabpanel.add(tabitem);
		}
		
		saleMenuFlag = me.getController('MainController').getMenuStatus("salemenu");
		if(saleMenuFlag==1){
			var tabitem = Ext.create('DM2.view.component.tab.Sales');
			tabpanel.add(tabitem);
		}
		
		if(dealMenuFlag ==0 && saleMenuFlag == 0){
			var tabitem = Ext.create('DM2.view.component.tab.Properties');
			tabpanel.add(tabitem);
		}
		var tabitem = Ext.create('DM2.view.component.tab.Contacts');
		tabpanel.add(tabitem);
		
		bankmenuFlag = me.getController('MainController').getMenuStatus("bankmenu");
		if(bankmenuFlag==1){
			var tabitem = Ext.create('DM2.view.component.tab.Banks');
			tabpanel.add(tabitem);
		}
		
		var tabitem = Ext.create('DM2.view.component.tab.Reports');
		tabpanel.add(tabitem);
	},

    setUserNameText: function(text) {
        this.getUsernametext().setText(text);
    },

    // value for index
    // 0 - Deals, 1 - Properties, 2 - Contacts, 3 - Banks, 4 - Reports
    setTabPanel: function(btn, index) {
        var me = this;
        var viewport = btn.up('viewport');
        var tabPanel = viewport.down('tab-panel');
        tabPanel.getLayout().setActiveItem(index);
        me.setActiveBtn(btn);        
    },

    showDashboardContainer: function() {
        var me = this;

        var mainviewport = me.getController('MainController').getMainviewport();
		mainviewport.removeAll();
		mainviewport.add([{
            xtype: 'dashboardcontainer',
            height: 60,
            region: 'north'
        }, {
            xtype: 'tab-panel',
            region: 'center'
        }]);
        // mainviewport.getLayout().setActiveItem(me.getDashboardcontainer());
        // me.showDeals(me.getDashboardcontainer().down('#dealsbtn'));
    },

    showTasks: function(btn) {
        var taskpanel = this.getController('TaskController').getTaskgridpanel();
        this.getDashboardview().getLayout().setActiveItem(taskpanel);
        this.setActiveBtn(btn);
    },

    dealMenuClick: function(menu, item, e, eOpts) {
        var me = this;
        var btn = menu.up();
        btn.setText(item.text);
        me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-deals'));
        var grid = me.getDealgrid();
		if(item.itemId != "gotodealfield"){
			grid.clearFilters();
			Ext.getStore('Deals').clearFilter();
		}
        grid.up('tabpanel').setActiveTab(0);
        switch(item.itemId) {
            case "allDeals":
				//Ext.getStore('Deals').getProxy().setUrl(DM2.view.AppConstants.apiurl+'v_rdealsAll');
				console.log("Load All Deals");
				var dealsproxy = Ext.getStore('Deals').getProxy();
				var dealsproxyCfg = dealsproxy.config;
				dealsproxyCfg.url = DM2.view.AppConstants.apiurl+'v_rdealsAll';
				dealsproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'v_rdealsAll';
				Ext.getStore('Deals').setProxy(dealsproxyCfg);
				
                me.getController('DealController').loadAllDeals();
            break;
            
			case "latestDeals":
				//Ext.getStore('Deals').getProxy().setUrl(DM2.view.AppConstants.apiurl+'v_rdeals');
				console.log("Load Latest Deals");
				var dealsproxy = Ext.getStore('Deals').getProxy();
				var dealsproxyCfg = dealsproxy.config;
				dealsproxyCfg.url = DM2.view.AppConstants.apiurl+'v_rdeals';
				dealsproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'v_rdeals';
				Ext.getStore('Deals').setProxy(dealsproxyCfg);				
                me.getController('DealController').loadAllDeals();
            break;
			
            case "myDeals":
				var dealsproxy = Ext.getStore('Deals').getProxy();
				var dealsproxyCfg = dealsproxy.config;
				dealsproxyCfg.url = DM2.view.AppConstants.apiurl+'v_rdeals';
				dealsproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'v_rdeals';
				Ext.getStore('Deals').setProxy(dealsproxyCfg);				
                me.showMyDeals();
            break;
            
            case "recentDeals":
				var dealsproxy = Ext.getStore('Deals').getProxy();
				var dealsproxyCfg = dealsproxy.config;
				dealsproxyCfg.url = DM2.view.AppConstants.apiurl+'v_recentDeals';
				dealsproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'v_recentDeals';
				Ext.getStore('Deals').setProxy(dealsproxyCfg);
                me.showRecentDeals();
            break;
        }        
    },

    logOutBtnClick: function(btn) {
        var me = this;
        Ext.util.Cookies.clear("apikey-"+DM2.view.AppConstants.apiurl);
        Ext.util.Cookies.clear("username-"+DM2.view.AppConstants.apiurl);
        Ext.util.Cookies.clear("userid-"+DM2.view.AppConstants.apiurl);
		Ext.util.Cookies.clear("keyexpiretime-"+DM2.view.AppConstants.apiurl);
		Ext.util.Cookies.clear("apiserver-"+DM2.view.AppConstants.apiurl);
		Ext.util.Cookies.clear("logintime-"+DM2.view.AppConstants.apiurl);
		
        me.getController('MainController').showLoginView();
    },

    showProperties: function(btn) {
        var me = this;
        me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-properties'));        
		//me.getController('PropertyController').loadAllPropMaster();
    },

    dealBtnClick: function(btn) {
        this.showDeals(btn);
    },

    showDeals: function(btn) {
		var me = this;
        me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-deals'));		
		
        // var alldeals = this.getController('DealController').getDealmenupanel();
        // this.getDashboardview().getLayout().setActiveItem(alldeals);
        // console.log("Show Deals");
        // Ext.getStore('Deals').getProxy().setUrl(DM2.view.AppConstants.apiurl+'v_rdeals');
// 
        // Ext.getStore('Deals').clearFilter();
        // Ext.getStore('Deals').load();
        // this.setActiveBtn(btn);
    },

    showMyDeals: function(btn) {
        // var alldeals = this.getController('DealController').getDealmenupanel();
        // this.getDashboardview().getLayout().setActiveItem(alldeals);
        console.log("Show My Deals");
		var me = this;
		Ext.getStore('Deals').sorters.clear();
        var filterarr = Ext.getStore('Deals').filters;
        // console.log(filterarr);
        var useridval = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
        filterarr.add(new Ext.util.Filter({
            //property : 'broker',
			property : 'userList',
            value: useridval,
            operator: 'like'
        }));
		/*if(Ext.getStore('Deals').sorters && Ext.getStore('Deals').sorters.getCount()){
            var sorter = Ext.getStore('Deals').sorters.getAt(0);
            sorter._property = 'statusdate';
			sorter._direction = 'DESC';
        }*/
		Ext.getStore('Deals').setSorters(new Ext.util.Sorter({property : 'statusdate',direction : 'DESC'}));
		//Ext.getStore('Deals').sorters.add(new Ext.util.Sorter({property : 'statusdate',direction : 'DESC'}));
        Ext.getStore('Deals').getProxy().setUrl(DM2.view.AppConstants.apiurl+'v_rdeals');
		//Ext.getStore('Deals').sort('statusdate', 'DESC');
        Ext.getStore('Deals').load({
			callback: function(records, operation, success) {
				if(records.length > 0){
					me.getController('DealController').getDealgrid().getSelectionModel().select(0);
					//me.getDealgrid().getSelectionModel().select(0);
				} else if(records.length==0){
					me.getController('DealController').getDealgrid().getSelectionModel().deselectAll();
					var activetab = me.getController('DealController').getDealmenutabpanel().getActiveTab();
					var dealnotegrid = me.getController('DealController').getDealmenutabpanel().down('dealnotesgridpanel');
					var dealcontgrid = me.getController('DealController').getDealmenutabpanel().down('dealcontactsgridpanel');
					var dealusergrid = me.getController('DealController').getDealmenutabpanel().down('dealdetailusergridpanel');
					var dealdocsgrid = me.getController('DealController').getDealmenutabpanel().down('dealdocsgridpanel');
					var dealhisgrid = me.getController('DealController').getDealmenutabpanel().down('dealhistorygrid');
					activetab.getStore().removeAll();
					dealcontgrid.getStore().removeAll();
					dealusergrid.getStore().removeAll();
					dealnotegrid.getStore().removeAll();
					dealdocsgrid.getStore().removeAll();
					dealhisgrid.getStore().removeAll();
				}
            }
		});
        // this.setActiveBtn(btn);
    },

    showRecentDeals: function(btn) {
        var me = this;
        // var alldeals = this.getController('DealController').getDealmenupanel();
        // this.getDashboardview().getLayout().setActiveItem(alldeals);
        // console.log("Show Recent Deals");
        // this.setActiveBtn(btn);

        var dealsproxy = Ext.getStore('Deals').getProxy();
        dealsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealsproxy.setUrl(DM2.view.AppConstants.apiurl+'v_recentDeals');
        Ext.getStore('Deals').load({
            scope: this,
            params:{
                filter :"username = '"+Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl)+"'"
            },
            callback: function(records, operation, success) {
                if(success){                
                    if(records.length > 0){
                        // me.getController('DealController').getDealgrid().getSelectionModel().select(0);
                        me.getDealgrid().getSelectionModel().select(0);
                    }
                }
            }
        });
    },

    showContacts: function(btn) {
        
        var me = this;
        me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-contacts'));
		if(me.getContactgrid().firstLoad){
        	var filterstarr = [];
        	me.getController('ContactController').loadAllContacts(filterstarr);
			me.getContactgrid().firstLoad = false;
		}
    },

    showBanks: function(btn) {        
        var me = this;
        me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-banks'));
		if(me.getBankgrid().firstLoad){
	        var filterstarr = [];
    	    me.getController('BankController').loadAllBanks(filterstarr);
			me.getBankgrid().firstLoad = false;
		}
    },

    setActiveBtn: function(btn) {
        //console.log("Set the Active Btn");
        var me = this;
        if(me.getDashboardcontainer().currActiveBtn)
        {
            me.getDashboardcontainer().currActiveBtn.setUI('menubtnui-toolbar-medium');
        }
        //Set the Curr Active Btn as new one
        me.getDashboardcontainer().currActiveBtn = btn;

        ///Add the Pressed active cls of that Button
        me.getDashboardcontainer().currActiveBtn.setUI('activemenubtnui-toolbar-medium');
    },

    showReports: function(btn) {
        
        var me = this;
        me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-reports'));        
        
        // this.setActiveBtn(btn);
        // var reportpanel = this.getController('ReportController').getReportgridpanel();
        // this.getDashboardview().getLayout().setActiveItem(reportpanel);
        // this.setActiveBtn(btn);
    },

    dealContactMenuClick: function(menu, item, e, eOpts) {
        var me = this;
		var btn = menu.up();
        btn.setText(item.text);
		btn.menuaction = item.itemId;
		me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-contacts'));
		//alert("Hi");
		var grid = me.getContactgrid();
		grid.getStore().getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        //grid.clearFilters();		
		//alert("clearFilters");
		var cntgridst = Ext.getStore('Contactallbuff');
		cntgridst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        cntgridst.clearFilter();
        grid.up('tabpanel').setActiveTab(0);        
        switch(item.itemId) {
            case "allContacts":
				var filterstarr = [];
				var cntgridstproxy = cntgridst.getProxy();
				var cntgridstproxyCfg = cntgridstproxy.config;
				cntgridstproxyCfg.url = DM2.view.AppConstants.apiurl+'v_contacts';
				cntgridstproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'v_contacts';
				cntgridstproxyCfg.headers = {
					Accept: 'application/json',
					Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
				};
				cntgridst.setProxy(cntgridstproxyCfg);
        		me.getController('ContactController').loadAllContacts(filterstarr);
            break;
            
            case "myContacts":
                me.showMyContacts();
            break;
            
            case "recentContacts":
                me.showRecentContacts();
            break;
        }
    },
	showRecentContacts: function(){
		var me = this;
		var cntgridst = Ext.getStore('Contactallbuff');
        cntgridst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        cntgridst.getProxy().setUrl(DM2.view.AppConstants.apiurl+'mssql:v_recentContacts');
        cntgridst.load({
            scope: this,
            params:{
                filter :"username = '"+Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl)+"'"
            },
            callback: function(records, operation, success) {
                if(!success)
                {
                    // CG wonder why...
                }
                else{
                    if(records.length > 0){
                        me.getContactgrid().getSelectionModel().select(0);
                    }
                }
            }
        });
	},
	
	showMyContacts: function(btn) {
        // var alldeals = this.getController('DealController').getDealmenupanel();
        // this.getDashboardview().getLayout().setActiveItem(alldeals);
        console.log("Show My Contact");
		//alert("Show My Contact");
		var me = this;
        /*var filterarr = Ext.getStore('Contactallbuff').filters;
         console.log(filterarr);
        var useridval = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
        filterarr.add(new Ext.util.Filter({
            property : 'broker',
            value: useridval,
            operator: 'like'
        }));*/
		var cntgridst = Ext.getStore('Contactallbuff');		
		var cntgridstproxy = cntgridst.getProxy();
		var cntgridstproxyCfg = cntgridstproxy.config;
		cntgridstproxyCfg.url = DM2.view.AppConstants.apiurl+'v_myContacts';
		cntgridstproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'v_myContacts';
		cntgridstproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		cntgridst.setProxy(cntgridstproxyCfg);
		
        cntgridst.load({
			callback: function(records, operation, success) {
				if(records.length > 0){
					me.getController('DashboardController').getContactgrid().getSelectionModel().select(0);
					//me.getDealgrid().getSelectionModel().select(0);
				}else if(records.length==0){
					/*me.getController('DealController').getDealgrid().getSelectionModel().deselectAll();
					var activetab = me.getController('DealController').getDealmenutabpanel().getActiveTab();
					var dealnotegrid = me.getController('DealController').getDealmenutabpanel().down('dealnotesgridpanel');
					var dealcontgrid = me.getController('DealController').getDealmenutabpanel().down('dealcontactsgridpanel');
					var dealusergrid = me.getController('DealController').getDealmenutabpanel().down('dealdetailusergridpanel');
					var dealdocsgrid = me.getController('DealController').getDealmenutabpanel().down('dealdocsgridpanel');
					var dealhisgrid = me.getController('DealController').getDealmenutabpanel().down('dealhistorygrid');
					activetab.getStore().removeAll();
					dealcontgrid.getStore().removeAll();
					dealusergrid.getStore().removeAll();
					dealnotegrid.getStore().removeAll();
					dealdocsgrid.getStore().removeAll();
					dealhisgrid.getStore().removeAll();*/
				}
            }
		});
    },

    showPropertiesMaster: function(btn) {        
        var me = this;
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-panel');
		if(tabpanel.down('tab-properties')){} else {
			var tabitem = Ext.create('DM2.view.component.tab.Properties');
			me.getController('MainController').getMainviewport().down('tab-panel').add(tabitem);
		}
        me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-properties'));
		/*if(me.getPropertygrid().firstLoad){
	        me.getController('PropertyController').loadAllPropMaster();
			me.getPropertygrid().firstLoad = false;
		}*/
    },
	
	aboutBtnClick: function(){
		var me = this;
		//console.log(Ext.manifest);
		var aboutwindow = me.getAboutwindow();
		if(aboutwindow){
			aboutwindow.close();
		}
		var aboutwindow = Ext.create('DM2.view.AboutWindow',{
			closeAction : 'destroy'
		});
		aboutwindow.show();
		var infopanel = aboutwindow.down('#infopanel');
		var apiserver = DM2.view.AppConstants.apiurl;
		var logintime = Ext.util.Cookies.get("logintime-"+DM2.view.AppConstants.apiurl);
		var keyexpiretimeval = Ext.util.Cookies.get("keyexpiretime-"+DM2.view.AppConstants.apiurl);
		
		var expdate  = new Date(keyexpiretimeval);
		var currdate = new Date();
		var rmTime = expdate - currdate;
		var timeval = me.parseMillisecondsIntoReadableTime(rmTime);
		
		console.log(apiserver);
		console.log("Ext Js Version : "+Ext.getVersion().version);
		var st = '<br /><div style="font-weight:bold;">Apiserver : '+apiserver+'</div><br /><div style="font-weight:bold;">Sencha Ext Js Version : '+Ext.getVersion().version+'</div><br /><div style="font-weight:bold;">Dealmaker Version : 1.0.1</div><br /><div style="font-weight:bold;">Login Time : '+logintime+'</div><br /><div style="font-weight:bold;">KeyExpire Time : '+keyexpiretimeval+'</div><br /><div style="font-weight:bold;">Remaining Time : '+timeval+' H:mm:ss</div>';

		if(Ext.manifest.appBuildMode==="prod"){
			if(typeof appTimestamp !== 'undefined'){
				// the variable is defined
				console.log("appTimestamp"+appTimestamp);
				aboutwindow.setHeight(440);
				st = st+'<br /><div style="font-weight:bold;">Build Time : '+appTimestamp+'</div>';
			}
		}
		
		infopanel.setHtml(st);
	},
	parseMillisecondsIntoReadableTime: function(milliseconds){
	  //Get hours from milliseconds
	  var hours = milliseconds / (1000*60*60);
	  var absoluteHours = Math.floor(hours);
	  var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
	
	  //Get remainder from hours and convert to minutes
	  var minutes = (hours - absoluteHours) * 60;
	  var absoluteMinutes = Math.floor(minutes);
	  var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;
	
	  //Get remainder from minutes and convert to seconds
	  var seconds = (minutes - absoluteMinutes) * 60;
	  var absoluteSeconds = Math.floor(seconds);
	  var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
	
	  return h + ':' + m + ':' + s;
	},
	
	showSales: function(btn) {        
        var me = this;
        me.setTabPanel(btn, 'tab-sales');
		if(me.getSalesgrid().firstLoad){
        	var filterstarr = [];
        	me.getController('SalesController').loadAllSales(filterstarr);
			me.getSalesgrid().firstLoad = false;
		}
    },
	
	saleMenuClick: function(menu, item, e, eOpts) {
        var me = this;
        var btn = menu.up();
        btn.setText(item.text);
        me.setTabPanel(btn, 'tab-sales');
        var grid = me.getSalesgrid();
		if(item.itemId != "gotosalefield"){
			grid.clearFilters();
			grid.getStore().clearFilter();
		}
        grid.up('tabpanel').setActiveTab(0);
        switch(item.itemId) {
            case "sales":
				console.log("Load All sales");
				var salesdealsproxy = grid.getStore().getProxy();
				var salesdealsproxyCfg = salesdealsproxy.config;
				salesdealsproxyCfg.url = DM2.view.AppConstants.apiurl+'v_rsaledeals';
				grid.getStore().setProxy(salesdealsproxyCfg);
				var filterstarr = [];
                me.getController('SalesController').loadAllSales(filterstarr);
            break;
			
            case "mySales":
                me.showMySalesDeals();
            break;
            
            case "recentSales":
                me.showRecentSalesDeals();
            break;
        }        
    },
	
	showMySalesDeals: function(btn) {
        // var alldeals = this.getController('DealController').getDealmenupanel();
        // this.getDashboardview().getLayout().setActiveItem(alldeals);
        console.log("Show My Deals");
		var me = this;
		var grid = me.getSalesgrid();
        var filterarr = grid.getStore().filters;
        // console.log(filterarr);
        var useridval = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
        filterarr.add(new Ext.util.Filter({
            //property : 'broker',
			property : 'userList',
            value: useridval,
            operator: 'like'
        }));
        grid.getStore().getProxy().setUrl(DM2.view.AppConstants.apiurl+'v_rsaledeals');
        grid.getStore().load({
			callback: function(records, operation, success) {
				if(records.length > 0){
					me.getSalesgrid().getSelectionModel().select(0);
					//me.getDealgrid().getSelectionModel().select(0);
				} else if(records.length==0){
					me.getSalesgrid().getSelectionModel().deselectAll();
					var salestabpanel = me.getController('SalesController').getSalestabpanel();
					var activetab = salestabpanel.getActiveTab();
					var dealnotegrid = salestabpanel.down('dealnotesgridpanel');
					var dealcontgrid = salestabpanel.down('dealcontactsgridpanel');
					var dealusergrid = salestabpanel.down('dealdetailusergridpanel');
					var dealdocsgrid = salestabpanel.down('dealdocsgridpanel');
					activetab.getStore().removeAll();
					dealcontgrid.getStore().removeAll();
					dealusergrid.getStore().removeAll();
					dealnotegrid.getStore().removeAll();
					dealdocsgrid.getStore().removeAll();
				}
            }
		});
    },
	
	showRecentSalesDeals: function(btn) {
        var me = this;        
        console.log("Show Recent Sales Deals");
		var grid = me.getSalesgrid();

        var dealsproxy = grid.getStore().getProxy();
        dealsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealsproxy.setUrl(DM2.view.AppConstants.apiurl+'v_recentSaleDeals');
        grid.getStore().load({
            scope: this,
            params:{
                filter :"username = '"+Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl)+"'"
            },
            callback: function(records, operation, success) {
                if(success){
                    if(records.length > 0){
                        grid.getSelectionModel().select(0);
                    }
                }
            }
        });
    },
	
	goToSalesDealField: function(field, e) {
		var me = this;
		console.log("Go To Sales Deals No.");
		var splitbutton = field.up('splitbutton');
		var val = Ext.String.trim(field.getValue());      
		if(e.getKey() == e.ENTER && val) {
			Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
				},
				url:DM2.view.AppConstants.apiurl+'v_rsaledeals',
				params:{
					filter : "idDeal = '" + val + "'"
				},
				scope: {
					me: me,
					splitbutton: splitbutton,
					field: field,
					val: val
				},
				method:'GET',
				success: function(response, opts) {
					var me = this.me;
					var splitbutton = this.splitbutton;
					var field = this.field;
					var val = this.val;
					var rec;
					var grid = field.up('mainviewport').down('tab-sales-layout').down('dataview');
					splitbutton.setText('Sales');
					field.setValue(null);
					splitbutton.menu.hide();
					var obj = Ext.decode(response.responseText);
					if(obj.length > 0) {
						rec = Ext.create('DM2.model.Deal', obj[0]);
						me.getController('SalesController').showDealDetail(rec, grid);    
					} else {
						Ext.Msg.alert('Failed', "Please enter valid dealno.");
					}
				},
				failure: function(response, opts) {
					var me = this.me;
					var splitbutton = this.splitbutton;
					var field = this.field;
					var val = this.val;
					splitbutton.setText('Sales');
					field.setValue(null);
					splitbutton.menu.hide();
					Ext.Msg.alert('Failed', "Please enter valid dealno.");
				}
			});                            
		}
	}
});