Ext.define('DM2.controller.DealUserController', {
    extend: 'Ext.app.Controller',

    refs: {
        dealdetailusergridpanel: 'dealdetailusergridpanel',
        dealuseroptionmenu: {
            autoCreate: true,
            selector: 'dealuseroptionmenu',
            xtype: 'dealuseroptionmenu'
        },
        adddealuserpanel: {
            autoCreate: true,
            selector: 'adddealuserpanel',
            xtype: 'adddealuserpanel'
        }
    },

    init: function(application) {
        this.control({
            'dealdetailusergridpanel':{
                rowcontextmenu: this.dealusergridrightclick,
				afterrender: 'loadDealDetailUsers'
            },
			'dealdetailusergridpanel dataview': {
                itemlongpress: this.onUserGridItemLongPress
            },
            'dealdetailusergridpanel #add':{
                click:this.onAddDealUserBtnClick
            },
            'dealuseroptionmenu':{
                click: this.dealuseroptionmenuclick
            },
            'adddealuserpanel #adddealuseresetbtn':{
                click:this.addDealUserFormReset
            },
            'adddealuserpanel #adddealusersubmitbtn':{
                click:this.addDealUserFormSubmit
            },
            'adddealuserpanel':{
                close: this.addDealUserPanelClose
            },
			'adddealuserpanel grid':{
                select: this.addDealUserPanelGridItemClick
            },
			'adddealuserpanel numberfield[name="commissionPercent"]': {
				keyup : this.validateCommissionPercent
			},
			'adddealuserpanel field': {
				change : this.onUserFormFieldChange
			}
        });
    },
	
	loadDealDetailUsers: function(panel){
		console.log("Users Load");
        var me = this;
		var context = panel.up('tab-deal-detail');
		if(context) {
        	var record = context.record;
       		//var dealid = record.data.dealid;
			var dealid = record.data.idDeal;
			me.loadDealUserDetailsWithParams('active',dealid,panel);
		}
	},
	
    showAddDealUserPanel: function(cmp,frmmode) {
        var me = this;
        var usrfrm,tmprec,title,tabpanel,usergridpanel,sbmbtn,trgtcmp,eastregion,adddealuserpanel;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
		    // when cmp is a button.
			trgtcmp = cmp;
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		}
		context = tabpanel.getActiveTab();
		switch(frmmode) {
            case "add":
                title = "Add User";    
            break;
            
            case "edit":
                title = "Edit User";
				if(!grid.getSelectionModel().hasSelection()) {
					Ext.Msg.alert('Failed', "Please select user to edit.");
					return;
				} 
            break;
        }
      	if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
			if(context.xtype == "tab-deal-layout"){
				eastregion = context.down('#deal-east-panel');
				usergridpanel = context.down('dealmenutabpanel').down('dealdetailusergridpanel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion = context.down('#sales-east-panel');
				usergridpanel = context.down('salestabpanel').down('dealdetailusergridpanel');
			}
			eastregion.removeAll();
			if(context.down('adddealuserpanel')){
				console.log("Use Old Panel");
				adddealuserpanel = context.down('adddealuserpanel');
			} else {
				console.log("Create New Panel");
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');//me.getAdddealuserpanel();
				eastregion.add(adddealuserpanel);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(adddealuserpanel);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();			
			usrfrm = adddealuserpanel;			
			if(context.xtype == "tab-deal-layout"){
				adddealuserpanel.getForm().findField('commissionPercent').hide();
			} else if(context.xtype == "tab-sales-layout"){
				adddealuserpanel.getForm().findField('commissionPercent').show();
				me.setCommissionPercentField(adddealuserpanel.getForm().findField('commissionPercent'));
			}
        } else if(context.xtype == "tab-deal-detail"){
			eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			//eastregion.removeAll();
			if(context.down('dealdetailsformpanel').down('adddealuserpanel')){
				console.log("Use Old Panel");
				adddealuserpanel = context.down('dealdetailsformpanel').down('adddealuserpanel');
			} else {
				console.log("Create New Panel");
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');
			}
			eastregion.getLayout().setActiveItem(adddealuserpanel);
			adddealuserpanel.getHeader().show();			
			usrfrm = adddealuserpanel;
			usergridpanel = context.down('dealdetailsformpanel').down('dealdetailusergridpanel');
			adddealuserpanel.getForm().findField('commissionPercent').hide();
        } else if(context.xtype == "tab-sales-detail"){
			eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			//eastregion.removeAll();
			if(context.down('salesdealdetailsformpanel').down('adddealuserpanel')){
				console.log("Use Old Panel");
				adddealuserpanel = context.down('salesdealdetailsformpanel').down('adddealuserpanel');
			} else {
				console.log("Create New Panel");
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');
			}
			eastregion.getLayout().setActiveItem(adddealuserpanel);
			adddealuserpanel.getHeader().show();			
			usrfrm = adddealuserpanel;
			usergridpanel = context.down('salesdealdetailsformpanel').down('dealdetailusergridpanel');
			adddealuserpanel.getForm().findField('commissionPercent').show();
			me.setCommissionPercentField(adddealuserpanel.getForm().findField('commissionPercent'));
        }
		adddealuserpanel.setTitle(title);
		sbmbtn = usrfrm.down('#adddealusersubmitbtn');

        usrfrm.getForm().reset();
		
		var rolestatic = Ext.getStore("RolesStatic"); //Get Roles Store Data
		var rolest = usrfrm.getForm().findField('role').getStore();
        if(rolestatic.isLoaded()){
			rolest.loadData(rolestatic.data.items);
        }

        if(frmmode==="edit"){			
            if (usergridpanel.getSelectionModel().hasSelection()) {
                tmprec = usergridpanel.getSelectionModel().getSelection()[0];
            }
            //usrfrm.getForm().loadRecord(tmprec); 
			usrfrm.getForm().findField('userName').setValue(tmprec.get('user'));
			//usrfrm.getForm().findField('idCSR').setValue(tmprec.get('csrid'));
			usrfrm.getForm().findField('active').setValue(tmprec.get('active'));
			usrfrm.getForm().findField('idDealxUser').setValue(tmprec.get('idDealxUser'));
			usrfrm.getForm().findField('commissionPercent').setValue(tmprec.get('commissionPercent'));
			usrfrm.getForm().findField('commissionPercentOriginal').setValue(tmprec.get('commissionPercent'));
			
			usrfrm.getForm().findField('firstName').hide();
			usrfrm.getForm().findField('address').hide();
			usrfrm.getForm().findField('state').hide();
			usrfrm.getForm().findField('lastName').hide();
			usrfrm.getForm().findField('city').hide();
			usrfrm.getForm().findField('zip').hide();
			usrfrm.getForm().findField('position').hide();
			
			usrfrm.down('panel[region="north"]').setHeight("100%");
			usrfrm.down('grid').hide();
			usrfrm.getForm().findField('role').select(tmprec.get('Role_idRole'));
			sbmbtn.setDisabled(true);
			//sbmbtn.enable();
        } else {
			usrfrm.getForm().findField('firstName').show();
			usrfrm.getForm().findField('address').show();
			usrfrm.getForm().findField('state').show();
			usrfrm.getForm().findField('lastName').show();
			usrfrm.getForm().findField('city').show();
			usrfrm.getForm().findField('zip').show();
			usrfrm.getForm().findField('position').show();
			usrfrm.down('grid').show();
			usrfrm.down('panel[region="north"]').setHeight("auto");
			
			usrfrm.getForm().findField('role').select(rolest.getAt(0));
			usrfrm.down('#adddealusersubmitbtn').setDisabled(false);
		}
		        
		if(frmmode==="add"){
			var csrst = usrfrm.down('grid').getStore(); //Ext.getStore('CSR');
			if(csrst.isLoaded()) {
				if(usrfrm.isVisible()){
	                usrfrm.down('grid').getSelectionModel().select(0);
				}
            } else {
				/*var filterstarr = [];
				var filterst = "active = 1";
				filterstarr.push(filterst);
				*/
				var csrstproxy = csrst.getProxy();
				var csrstproxyCfg = csrstproxy.config;
				csrstproxyCfg.headers = {
					Accept: 'application/json',
					Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
				};
				csrst.setProxy(csrstproxyCfg);
				csrst.load({
					/*params:{
						filter :filterstarr
					},*/
					callback: function(records, operation, success){
						console.log(usrfrm.isVisible());
						if(usrfrm.isVisible()){
						   usrfrm.down('grid').getSelectionModel().select(0);
						}
					}
				});
	        }
		}
		adddealuserpanel.config.backview = "dealusergridpanel";
    },
	
	setCommissionPercentField: function(field){
		var me = this;
		var userEditorItem = me.getController('MainController').getFunPermData('userEditor');
		if(userEditorItem){
			if(userEditorItem.allowed==1 && userEditorItem.functionActivity=="commisionPerUser"){
				field.setReadOnly(false);
			}
		}
	},
	
	bindDealUsers: function(tabItem){
		console.log("Bind Deal Users");
		var me = this;
		var panel = tabItem.down('dealdetailusergridpanel');
		var dealdocsst = panel.getStore();
		dealdocsst.loadData(tabItem.dealDetailStore.data.items[0].data.UsersPerDeal);
		me.afterDealUsersDataLoad(panel);		
	},
	
	clearDealUsers: function(tabItem){
		console.log("Clear Deal Users");
		var me = this;
		var panel = tabItem.down('dealdetailusergridpanel');
		var dealdocsst = panel.getStore();
		dealdocsst.removeAll();
		me.afterDealUsersDataLoad(panel);		
	},
	
	afterDealUsersDataLoad: function(panel){
		var me = this,permission,tabItem;
		var dealdocsst = panel.getStore();
		var count = dealdocsst.getCount();		
		var usertoolbar = panel.down('toolbar');
		if(count>0){
			if(usertoolbar){
				usertoolbar.hide();
			}
		}else{
			if(usertoolbar){
				usertoolbar.show();
				if(panel.up('tab-deal-detail')){
					tabItem = panel.up('tab-deal-detail');
				}else if(panel.up('tab-sales-detail')){
					tabItem = panel.up('tab-sales-detail');
				} else if(panel.up('tab-deal-layout')){
					tabItem = panel.up('tab-deal-layout');
				} else if(panel.up('tab-sales-layout')){
					tabItem = panel.up('tab-sales-layout');
				}
				if(tabItem.dealDetailStore){
					permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
					if(parseInt(permission)===0){						
						usertoolbar.down('button').disable();
					}else{
						usertoolbar.down('button').enable();
					}
				}
			}
		}
	},

    dealusergridrightclick: function(view, record, tr, rowIndex, e, eOpts) {
		var me = this;
		me.showUserGridMenu(view,record,e);		
    },
	
	showUserGridMenu: function(view,record,e){
		var me = this,tabItem;
        console.log("Show Menu for Deal User Grid");
		e.stopEvent();
        if(this.getDealuseroptionmenu()){
			if(view.ownerCt.up('tab-deal-detail')){
				tabItem = view.ownerCt.up('tab-deal-detail');			    
			} else if(view.ownerCt.up('tab-deal-layout')){
				tabItem = view.ownerCt.up('tab-deal-layout');			    
			} else if(view.ownerCt.up('tab-sales-layout')){
				tabItem = view.ownerCt.up('tab-sales-layout');			    
			} else if(view.ownerCt.up('tab-sales-detail')){
				tabItem = view.ownerCt.up('tab-sales-detail');			    
			}
			DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,me.getDealuseroptionmenu());
			
			this.getDealuseroptionmenu().spawnFrom = view;
            this.getDealuseroptionmenu().showAt(e.getXY());
			var activeval = record.get('active');
			if(activeval==0){
				 me.getDealuseroptionmenu().down('#removeuser').hide();
			}else if(activeval==1){
				me.getDealuseroptionmenu().down('#removeuser').show();
			}
        }
	},
	
	onUserGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showUserGridMenu(view,record,e);
	},

    dealuseroptionmenuclick: function(menu, item, e, eOpts) {
        console.log("Deal Detail User Menu Item Clicked.");
        if(item.text==="Add User"){
            this.showAddDealUserPanel(item,'add');
        }else if(item.text==="Edit User"){
            this.showAddDealUserPanel(item,'edit');
        }else if(item.text==="Remove User"){
            this.removeDealUser(item);
        }
    },

    addDealUserFormReset: function() {
        this.getAdddealuserwindow().down('form').getForm().reset();
        this.getAdddealuserwindow().down('form').getForm().findField('role').select(Ext.getStore('Roles').getAt(0));
    },

    addDealUserFormSubmit: function(btn) {
        console.log("Add User to Deal");
        var me = this;
		var dealid, context,usergridpanel;
		
		var activemenutab = btn.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		var dealuserfrm = btn.up('adddealuserpanel');
		if(dealuserfrm.config.backview.xtype == "createnewdealpanel" || dealuserfrm.config.backview.xtype == "createnewdealfrompropertypanel" || dealuserfrm.config.backview.xtype == "createnewsalesdealfrompropertypanel") {
			me.getController('CreateNewDealController').addUserToNewDeal(dealuserfrm,dealuserfrm.config.backview);
		} else {
			if(context.xtype == 'tab-deal-detail'){
				var record = context.record;
				dealid = record.data.idDeal;
				usergridpanel = context.down('dealdetailsformpanel').down('dealdetailusergridpanel');
			} else if(context.xtype == 'tab-sales-detail'){
				var record = context.record;
				dealid = record.data.idDeal;
				usergridpanel = context.down('salesdealdetailsformpanel').down('dealdetailusergridpanel');
			} else if(context.xtype == 'tab-deal-layout') {
				var tabdealgrid = context.down('tab-deal-grid');
				if(!tabdealgrid.getSelectionModel().hasSelection()) {
					Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
					return;
				}
				var row = tabdealgrid.getSelectionModel().getSelection()[0];
				dealid = row.data.idDeal;
				usergridpanel = context.down('dealmenutabpanel').down('dealdetailusergridpanel');
			} else if(context.xtype == 'tab-sales-layout') {
				var tabdealgrid = context.down('tab-sales-grid');
				if(!tabdealgrid.getSelectionModel().hasSelection()) {
					Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
					return;
				}
				var row = tabdealgrid.getSelectionModel().getSelection()[0];
				dealid = row.data.idDeal;
				usergridpanel = context.down('salestabpanel').down('dealdetailusergridpanel');
			}
			//console.log("dealdetailusergridpanel");
			//console.log(dealdetailusergridpanel);
		    var username = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
			var dealuserid = dealuserfrm.getForm().findField('idDealxUser').getValue();
			if(dealuserid!==0 && dealuserid!==null){
				var data = [{
					'idDealxUser':dealuserid,
					//'CSR_idCSR' : dealuserfrm.getForm().findField('idCSR').getValue(),
					'userName' : dealuserfrm.getForm().findField('userName').getValue(),
					'assignUserName':username,
					'modifyDateTime': new Date(),
					'idDeal' : dealid,
					//'active' : dealuserfrm.getForm().findField('active').getValue(),
					'idRole' : dealuserfrm.getForm().findField('role').getValue(),
					'commissionPercent' : dealuserfrm.getForm().findField('commissionPercent').getValue(),
					"@metadata": {
					  "checksum": "override"
					}
				}];
				var methodname = "PUT";
				var urlval = DM2.view.AppConstants.apiurl+'mssql:DealxUser/'+dealuserid;
			} else {
				var data = [{
					//'CSR_idCSR' : dealuserfrm.getForm().findField('idCSR').getValue(),
					'userName' : dealuserfrm.getForm().findField('userName').getValue(),
					'assignUserName':username,
					'assignDateTime': new Date(),
					'createDateTime': new Date(),
					'modifyDateTime': new Date(),					
					'idDeal' : dealid,
					//'active' : dealuserfrm.getForm().findField('active').getValue(),
					'idRole' : dealuserfrm.getForm().findField('role').getValue(),
					'commissionPercent' : dealuserfrm.getForm().findField('commissionPercent').getValue()
				}];
				var methodname = "POST";
				var urlval = DM2.view.AppConstants.apiurl+'mssql:DealxUser';
			}
			dealuserfrm.setLoading(true);
			Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
				},
				url : urlval,
				method : methodname,
				params : Ext.util.JSON.encode(data),
				scope:this,
				success: function(response, opts) {
					console.log("User is added to deal.");
					dealuserfrm.setLoading(false);
					var obj = Ext.decode(response.responseText);
					if(obj.statusCode == 201 || obj.statusCode == 200){
						dealuserfrm.getForm().reset();
						dealuserfrm.close();
						//me.addDealUserPanelClose(dealuserfrm);						
						if(context.xtype == 'tab-deal-detail'){							
							me.getController('DealDetailController').loadDealDetails(context.down('dealdetailuppersection'));
						} else if(context.xtype == 'tab-sales-detail'){
							me.getController('SalesDealDetailController').refreshSaleDealDetailData(context);
						} else if(context.xtype == 'tab-sales-layout'){
							//me.getController('SalesDealDetailController').refreshSaleDealDetailData(context);
							me.loadDealUserDetailsWithParams(usergridpanel.config.alloractiveusermode,dealid,usergridpanel);
						} else if(context.xtype == 'tab-deal-layout'){
							me.loadDealUserDetailsWithParams(usergridpanel.config.alloractiveusermode,dealid,usergridpanel);
						}					
					} else {
						console.log("User is added to deal Failure.");
						Ext.Msg.alert('Add user failed', obj.errorMessage);
					}
				},
				failure: function(response, opts) {
					dealuserfrm.setLoading(false);
					console.log("User is added to deal Failure.");
					var obj = Ext.decode(response.responseText);
					var errText = "";
					var errMsg = obj.errorMessage;
					var n = errMsg.indexOf("User Already exist");
					if(n==-1){
						errText = obj.errorMessage;
						//Ext.Msg.alert('Add User Failed', obj.errorMessage);
					} else {
						errText = "User Already exist. Please try to add another user.";
						//Ext.Msg.alert('Add User Failed', "User Already exist.");
					}
					var title = "Add User Failed";
					me.getController('DealDetailController').showToast(errText,title,'error',null,'tr',true);
				}
			});
		}
    },

    addDealUserPanelClose: function(panel) {
		var me = this,tabpanel;
		if(panel.up('tab-deals') || panel.up('tab-sales')){
			if(panel.up('tab-deals')){
				tabpanel = panel.up('tab-deals');
			} else if(panel.up('tab-sales')){
				tabpanel = panel.up('tab-sales');
			}
			var context = tabpanel.getActiveTab();
			if(context.xtype == "tab-deal-layout"){
				var eastregion = context.down('#deal-east-panel');
				eastregion.removeAll();
				//eastregion.setCollapsed(false);
				eastregion.setVisible(false);
			} else if(context.xtype == "tab-sales-layout"){
				var eastregion = context.down('#sales-east-panel');
				eastregion.removeAll();
				//eastregion.setCollapsed(false);
				eastregion.setVisible(false);
			} else if(context.xtype == "tab-deal-detail"){
				var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
				if(panel.config.backview.xtype == "createnewdealpanel"){
					eastregion.getLayout().setActiveItem(panel.config.backview);
				} else {
					eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
				}
			} else if(context.xtype == "tab-sales-detail"){
				var eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');				
				if(panel.config.backview == "createnewdealusergridpanel"){
					createnewdealpanel = context.down('salesdealdetailsformpanel').down('createnewdealpanel');
					eastregion.getLayout().setActiveItem(createnewdealpanel);
				} else if(panel.config.backview.xtype == "createnewsalesdealfrompropertypanel"){
					eastregion.getLayout().setActiveItem(panel.config.backview);
				} else {
					//eastregion.remove(panel);
					eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
				}
			}		
		} else if(panel.up('tab-properties')){
			var tabpropertypanel = panel.up('tab-properties');
			var context = tabpropertypanel.getActiveTab();
			 if(context.xtype == "tab-property-layout"){
				var eastregion = context.down('#property-east-panel');
				if(panel.config.backview.xtype == "createnewdealfrompropertypanel"){
					//createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
					eastregion.getLayout().setActiveItem(panel.config.backview);
				} else if(panel.config.backview.xtype == "createnewsalesdealfrompropertypanel"){
					//createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
					eastregion.getLayout().setActiveItem(panel.config.backview);
				}
			}
		} else if(panel.up('tab-contacts')){
			var tabcontactpanel = panel.up('tab-contacts');
			var context = tabcontactpanel.getActiveTab();
			 if(context.xtype == "tab-contact-layout"){
				var eastregion = context.down('#contact-east-panel');
				if(panel.config.backview.xtype == "createnewdealfrompropertypanel"){
					//createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
					eastregion.getLayout().setActiveItem(panel.config.backview);
				} else if(panel.config.backview.xtype == "createnewsalesdealfrompropertypanel"){
					//createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
					eastregion.getLayout().setActiveItem(panel.config.backview);
				}
			}
		}
    },

    onAddDealUserBtnClick: function(btn) {
        this.showAddDealUserPanel(btn,'add');
    },
	
	addDealUserPanelGridItemClick: function(selModel, record, index, eOpts){
		var me = this;
		var adddealuserpanel = selModel.view.ownerCt.up('adddealuserpanel');		
        adddealuserpanel.getForm().loadRecord(record);
	},
	loadDealUserDetailsWithParams: function(alloracitve, dealid, grid) {
        
        console.log("Get the Deal Specific User details using the dealid Param");
        var me = this,usergrid;
        if(grid.xtype == "tab-deal-grid") {
            usergrid = grid.up().up().down('dealdetailusergridpanel');
        } else {
			usergrid = grid;
        }
		
		var activemenutab = grid.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();		
		
        var store = usergrid.getStore();
        var dealuserproxy = store.getProxy();

        dealuserproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+ me.getController('MainController').getMainviewport().config.apikey+':1'
        });
		
		var filterstarr = [];
        if(alloracitve=='active'){
            var filterst = "active = 1";
            filterstarr.push(filterst);
        }
        var filterst = "dealid = '"+dealid+"'";
        filterstarr.push(filterst);
		
        store.load({
            params:{
                filter :filterstarr
            },
            scope: {
                me: me,
                grid: usergrid,
                store: store
            },
            callback: function(records, operation, success){
                var me = this.me;
                var grid = this.grid;
                var store = this.store;
                var toolbar = grid.down('toolbar');                          
                
                if(store.getCount() > 0){
                    if(toolbar){
                        toolbar.hide();
                    }
                } else {
                    if(toolbar){
                        toolbar.show();
                    }
                }
				//context.dealUsersData = store.data.items;
				//console.log(records);
				context.dealUsersData = records;
				//console.log(context.dealUsersData);
            }
        });
    },
	
	removeDealUser: function(cmp){
        var me = this;
        var usrfrm,tmprec,title,tabpanel,usergridpanel,sbmbtn,trgtcmp;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
		    // when cmp is a button.
			trgtcmp = cmp;
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		}
		context = tabpanel.getActiveTab();
		
		usergridpanel = context.down('dealdetailusergridpanel');
	
		if (usergridpanel.getSelectionModel().hasSelection()) {
			tmprec = usergridpanel.getSelectionModel().getSelection()[0];
		}
		var idDealxUser = tmprec.get('idDealxUser');
		//var idDealxUser = tmprec.get('idDealxUser');
		var username = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);		
		var data = [{
            '@metadata': { 'checksum': 'override' },
            'idDealxUser' :idDealxUser,
            'unassignDateTime':new Date(),
			'unassignUserName':username
        }];
		var dealid = tmprec.get('dealid');
		
		usergridpanel.setLoading(true);
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'mssql:DealxUser',
			params: Ext.util.JSON.encode(data),
			scope:this,
			method:'PUT',
			success: function(response, opts) {
				usergridpanel.setLoading(false);
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 200 || obj.statusCode == 201){					
					me.loadDealUserDetailsWithParams(usergridpanel.config.alloractiveusermode,dealid,usergridpanel);
					if(context.xtype == 'tab-deal-detail'){
						me.getController('DealDetailController').loadDealDetails(context.down('dealdetailuppersection'));
					}	
				} else {
					console.log("Remove user failure.");
					Ext.Msg.alert('Remove user failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				usergridpanel.setLoading(false);
				var obj = Ext.decode(response.responseText);
				Ext.Msg.alert('Remove User', obj.errorMessage);
			}
		});
	},	
	validateCommissionPercent: function( field , e , eOpts ) {
		var me = this,totalCommPer = 0,context,tmprec;
		var commPerVal = field.getValue();
		if(field.up('tab-sales-layout')){
			context = field.up('tab-sales-layout');
		} else if(field.up('tab-sales-detail')){
			context = field.up('tab-sales-detail');
		}
		var dealUsersData = context.dealUsersData;
		console.log(dealUsersData);
		var dealuserfrm = field.up('adddealuserpanel');
		var dealuserid = dealuserfrm.getForm().findField('idDealxUser').getValue();
			
		for(var i=0;i<dealUsersData.length;i++){
			if(dealUsersData[i].data){
				tmprec = dealUsersData[i].data;
			} else {
				tmprec = dealUsersData[i];
			}
			if(dealuserid!==0 && dealuserid!==null && dealuserid==tmprec.idDealxUser){
			} else {
				totalCommPer = totalCommPer + tmprec.commissionPercent;
			}
		}
		totalCommPer = totalCommPer + commPerVal;
		console.log("totalCommPer"+totalCommPer);
		if(totalCommPer>100){
			var errText = "All users total Commission is more than 100 Percent.";
			var title = "Error Commission Percent";
			me.getController('DealDetailController').showToast(errText,title,'error',null,'tr',true);
			field.setValue(dealuserfrm.getForm().findField('commissionPercentOriginal').getValue());
			return false;
		}
	},
	
	onUserFormFieldChange: function(field , newValue , oldValue , eOpts){
		var me = this;
		var adddealuserpanel = field.up('adddealuserpanel');
		adddealuserpanel.down('#adddealusersubmitbtn').setDisabled(false);
	},
	
	loadRoles: function(){
		var me = this;
		var rolestatic_st = Ext.getStore("RolesStatic");
		var rolestaticproxy = rolestatic_st.getProxy();
		var rolestaticproxyCfg = rolestaticproxy.config;
        rolestaticproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		rolestatic_st.setProxy(rolestaticproxyCfg);
		rolestatic_st.load({
			callback: function(records, operation, success) {
			}
		});
	}
});