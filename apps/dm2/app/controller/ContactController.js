Ext.define('DM2.controller.ContactController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.ContactNoteOptionMenu',
			  'DM2.view.DealHistoryContactTabMenu',
			  'DM2.view.MyContactSelectionGrid',
			  'DM2.view.CompanyPickerPanel',
			  'DM2.view.AddEditCompanyView',
			  'DM2.view.UploadContactView',
			  'DM2.view.MultiPickListGrid'],
    refs: {
        contactphonesgridpanel: 'contactphonesgridpanel',
		contactfaxgridpanel: 'contactfaxgridpanel',
        contactemailsgridpanel: 'contactemailsgridpanel',
        contactmiscgridpanel: 'contactmiscgridpanel',
        contactaddressgridpanel: 'contactaddressgridpanel',
        contactmenu: {
            autoCreate: true,
            selector: 'contactmenu',
            xtype: 'contactmenu'
        },
        contactdetailsform: {
            autoCreate: true,
            selector: 'contactdetailsform',
            xtype: 'contactdetailsform'
        },
        contactmenutab: {
            autoCreate: true,
            selector: 'contactmenutab',
            xtype: 'contactmenutab'
        },
        // CG: 10/11/2015
        dealdetailcontactfieldset: '#dealdetailcontactfieldset',
        dealdetailcontact: 'dealdetailcontactfieldset',
        dealcontactsgridpanel: 'dealcontactsgridpanel',
        contactdocoptionmenu: {
            autoCreate: true,
            selector: 'contactdocoptionmenu',
            xtype: 'contactdocoptionmenu'
        },
		contactnoteoptionmenu: {
            autoCreate: true,
            selector: 'contactnoteoptionmenu',
            xtype: 'contactnoteoptionmenu'
        },
		contactgrid: 'tab-contact-grid',
		tabcontactlayout : 'tab-contact-layout',
		contacttabpanel : 'contacttabpanel',
		contactdealhistorygrid : '#contactdealhistorygrid',
		contactsalehistorygrid : '#contactsalehistorygrid',
		mycontactselectiongrid : 'mycontactselectiongrid',
		addeditcompanyview : 'addeditcompanyview'
    },

    init: function(application) {
        this.control({
			'tabcontactlayout':{
				afterrender: this.afterTabContactLayoutRender
			},
            'contactgrid': {
                select: this.loadContactDetailForm,
				afterrender : this.afterRenderContactGrid,
                rowcontextmenu: this.onContactsGridPanelRightClick
            },
			'tab-contact-grid dataview': {
				itemlongpress: this.onContactsGridItemLongPress
            },
            'contactphonesgridpanel':{
                selectionchange:this.onSelectChangePhone
            },
            'contactphonesgridpanel #delete':{
                click:this.onDeletePhoneClick
            },
            'contactphonesgridpanel #add':{
                click:this.onAddPhoneClick
            },
            'contactemailsgridpanel':{
                selectionchange:this.onSelectChangeEmail
            },
            'contactemailsgridpanel #delete':{
                click:this.onDeleteEmailClick
            },
            'contactemailsgridpanel #add':{
                click:this.onAddEmailClick
            },
            'contactmiscgridpanel':{
                selectionchange:this.onSelectChangeMisc
            },
            'contactmiscgridpanel #delete':{
                click:this.onDeleteMiscClick
            },
            'contactmiscgridpanel #add':{
                click:this.onAddMiscClick
            },
            'contactaddressgridpanel':{
                selectionchange:this.onSelectChangeAddress
            },
            'contactaddressgridpanel #delete':{
                click:this.onDeleteAddressClick
            },
            'contactaddressgridpanel #add':{
                click:this.onAddAddressClick
            },
            'contactmenu':{
                click:this.contactMenuClick
            },
			'contactdetailsform field': {
                change:this.onCntDtFormFieldsChange
            },
            'contactdetailsform #contactdetailformaddbtn': {
                click:this.contactDetailFormAddBtnClick
            },
			'contactdetailsform #contactdetailformresetbtn': {
                click:this.contactDetailFormResetBtnClick
            },
			/*'contactdetailsform textfield[name="companyName"]':{
				ontriggerclick: this.showCompanyPicker
			},*/
			'contactdetailsform button[action="companypickbtn"]':{
				click: this.showCompanyPicker
			},
            // CG
            'dealdetailcontactfieldset': {
                contextmenu: this.onDealContactsGridPanelRightClick
            },            
            'dealcontactsgridpanel': {
                rowcontextmenu: this.onDealContactsTabGridPanelRightClick
            },
			'dealcontactsgridpanel dataview': {
                 itemlongpress: this.onDealContactsGridItemLongPress
             },
			'contactdetailsform panel[action="toppanel"]': {
                beforeclose:this.contactDetailFormCloseBtnClick
            },      
            'contactdocsgridpanel': {
                load: this.loadContactDocsDrop,
                rowcontextmenu: this.onCntDocsGridPanelRightClick,
				select : this.onCntDocGridItemSelect,
				beforeitemmousedown: this.onCntDocGridItemMouseDown,
				itemclick : this.onCntDocGridItemClick
            },
			'contactdocsgridpanel dataview': {
                 itemlongpress: this.onCntDocsGridItemLongPress
            },
            'contactdocsgridpanel button[action="adddocbtn"]':{
                click: this.showRecvDealDocs
            },
			'contactdocoptionmenu menuitem':{
                click: this.contactDocOptionItemClick
            },
			'contacttabpanel':{
				afterrender: this.contactTabPanelAfterRender,
				tabchange: this.contactTabChange
			},
			'contacttabpanel #dealhistorycontacttab':{
				click: this.dealHistoryContactTabClick
			},
			'contacttabpanel #salehistorycontacttab':{
				click: this.saleHistoryContactTabClick
			},
			'contacttabpanel #dealhistorycontacttab menu':{
				click: this.dealHistoryContactTabMenuClick
			},
			'#salehistorycontacttab menu':{
				click: this.saleHistoryContactTabMenuClick
			},
			'contactdealhistorygrid':{
				itemdblclick: this.onContactDealHistoryGridItemDblClick
			},
			'contactsalehistorygrid':{
				itemdblclick: this.onContactSaleHistoryGridItemDblClick
			},
			'contactnotesgridpanel button[action="addnotebtn"]':{
                click: this.addContactNoteBtnClick
            },
			'contactnotesgridpanel':{
                rowcontextmenu: this.onCntNotesGridPanelRightClick
            },
			'contactnotesgridpanel dataview':{
				itemlongpress: this.onCntNotesGridItemLongPress
            },
			'contactnoteoptionmenu menuitem':{
                click: this.contactNoteOptionItemClick
            },
			'mycontactselectiongrid':{
				close: this.myContactSelectionGridClose,
				selectionchange: this.onMyContactSelectionChange
			},
			'mycontactselectiongrid button[action="savemycontactbtn"]':{
				click: this.saveToMyContacts
			},
			'companypickerpanel': {
				close: 'onCompanyPickerPanelClose'
			},
			'companypickerpanel button[action="selectCompanyBtn"]': {
				click: 'selectCompanyBtnClick'
			},
			'companypickerpanel grid': {
				itemclick: 'onCompanyPickerGridItemSelect'
			},
			'companypickerpanel button[action="addCompanyBtn"]':{
				click: 'addCompanyBtnClick'
			},
			'addeditcompanyview button[action="saveCompanyBtn"]':{
				click: 'saveCompanyBtnClick'
			},
			'addeditcompanyview': {
				close: 'onAddEditCompanyViewClose'
			},
			'contactmenutab menuitem':{
                click: this.dealContactGridOptionItemClick
            },
			'dealcontactsgridpanel button[action="addcontactbtn"]': {
                 click: {
                    fn: function(btn) {                        
						this.showAddDealContactForm(btn,"dealcontactgrid");
                    }
                }
            },
			'uploadcontactview button[action="savebtn"]': {
				click: this.saveUploadContact	 
			},
			'uploadcontactview button[action="resetbtn"]': {
				click: this.resetUploadContact	 
			},
			/*'uploadcontactview filefield[name="contactfile"]':{
                change: this.addContactFileFieldChange
            },*/
			'uploadcontactview': {
				close: this.uploadContactViewClose
			},
			'uploadcontactview button[action="memberspickbtn"]': {
				click: this.openMultiPickList
			},
			'uploadcontactview button[action="groupspickbtn"]': {
				click: this.openMultiPickList
			},
			'multipicklistgrid': {
				close: this.multiPickListGridClose
			},
			'multipicklistgrid button[action="savebtn"]': {
				click: this.saveMultiPickList
			}
        });
    },
	
	dealContactGridOptionItemClick: function(item, e, eOpts) {
        console.log("Deal Grid Contact Menu Item Clicked.");
		var me = this;
        if(item.action==="addcontact"){
            console.log("Contact Add Menu Item Clicked");
			me.showAddDealContactForm(item,"dealcontactgrid");
        }else if(item.action==="removecontact"){
            console.log("Remove Contact Menu Item Clicked");
            this.removeDealContact(item);
        }/*else if(item.text==="Change Role"){
            console.log("Change Role Menu Item Clicked");
            this.openChangeRole();
        }*/else if(item.action==="editcontact"){
            console.log("Edit Contact Menu Item Clicked");
            me.editDealContact(item);
        }
    },
	
	editDealContact: function(cmp) {
        console.log("Edit Deal Contact");
        var me = this,contactdetailsform,grid;
        if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
		}		
		if(grid.up('tab-deals')){
			tabpanel = grid.up('tab-deals');
		} else if(grid.up('tab-sales')){
			tabpanel = grid.up('tab-sales');
		}
		context = tabpanel.getActiveTab();
		
        console.log("Remove Contact from the Deal");
        var rec = grid.getSelectionModel().getSelection()[0];
		
		////
		if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
			if(context.xtype == "tab-deal-layout"){
				eastregion = context.down('#deal-east-panel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion = context.down('#sales-east-panel');
			}
			eastregion.removeAll();
			if(context.down('contactdetailsform')){
				console.log("Use Old Panel");
				contactdetailsform = context.down('contactdetailsform');
			} else {
				console.log("Create New Panel");
				contactdetailsform = Ext.create('DM2.view.ContactDetailsForm');
				eastregion.add(contactdetailsform);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(contactdetailsform);			
			eastregion.setTitle("Edit Contact");
			eastregion.getHeader().hide();
        }
		//contactdetailsform.getHeader().show();
		//contactdetailsform.config.backview = backfrm;
		contactdetailsform.config.backfrm = "dealcontactgrid";
		contactdetailsform.config.backview = "dealcontactgrid";
		////
		        
        var dealid = rec.get('dealid');
        var contactid = rec.get('contactid');
        var idDeal_has_Contact = rec.get('iddeal_has_contact');
		        		
        me.getController('ContactController').loadContactDetails(contactid,context);
		contactdetailsform.down('tool[type="close"]').show();		
    },	
	
	removeDealContact: function(cmp) {
        var me = this,grid;
        if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
		}		
		if(grid.up('tab-deals')){
			tabpanel = grid.up('tab-deals');
		} else if(grid.up('tab-sales')){
			tabpanel = grid.up('tab-sales');
		}
		context = tabpanel.getActiveTab();
		
        console.log("Remove Contact from the Deal");
        var row = grid.getSelectionModel().getSelection()[0];        
        //var dealid = row.get('dealid');
        //var contactid = row.get('contactid');
        var idDealxCont = row.get('idDealxCont');
        
        var data = {
            'checksum': "override"
        };
        
        Ext.Msg.show({
            title:'Delete Contact?',
            message: 'Are you sure you want to delete the contact from deal?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:DealxCont/'+idDealxCont+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            console.log("Add contact to deal is submitted.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200) {
								me.getController('DealDetailController').loadDealContactDetails(grid.up('dealcontactsgridpanel'));   
                            } else {
                                console.log("Delete Contact Failure.");
                                Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Delete Contact Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },
	
	showAddDealContactForm: function(cmp,backfrm){
	    
		var me = this,
            title = "", tabpanel, context, spawnFrom, grid, activeTab, baseCardPanel, borderPanel, noteContainerPanel, notepanel, row, trgtcmp,eastregion;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
		    // when cmp is a button.
			trgtcmp = cmp;
		}
		
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		}
		context = tabpanel.getActiveTab();
        		
        if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
			if(context.xtype == "tab-deal-layout"){
				eastregion = context.down('#deal-east-panel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion = context.down('#sales-east-panel');
			}
			eastregion.removeAll();
			if(context.down('adddealcontactform')){
				console.log("Use Old Panel");
				adddealcontactform = context.down('adddealcontactform');
			} else {
				console.log("Create New Panel");
				adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
				eastregion.add(adddealcontactform);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(adddealcontactform);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();
        } else if(context.xtype == "tab-deal-detail"){
			eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			if(context.down('dealdetailsformpanel').down('adddealcontactform')){
				console.log("Use Old Panel");
				adddealcontactform = context.down('dealdetailsformpanel').down('adddealcontactform');
			} else {
				console.log("Create New Panel");
				adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
			}
			eastregion.getLayout().setActiveItem(adddealcontactform);
        } else if(context.xtype == "tab-sales-detail"){
			eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			if(context.down('salesdealdetailsformpanel').down('notepanel')){
				console.log("Use Old Panel");
				adddealcontactform = context.down('salesdealdetailsformpanel').down('adddealcontactform');
			} else {
				console.log("Create New Panel");
				adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
			}
			eastregion.getLayout().setActiveItem(adddealcontactform);
        }
		adddealcontactform.getHeader().show();			
        //adddealcontactform.setTitle(title);
		adddealcontactform.getForm().reset();
		var filterstarr = [];
		me.getController('DealDetailController').loadContactAllSelBuff(filterstarr,context);
		adddealcontactform.config.backview = backfrm;
		adddealcontactform.config.backfrm = backfrm;
	},
	
	onContactDealHistoryGridItemDblClick: function(dataview, record, item, index, e, eOpts){
		var me = this;
		var dealsbtn = dataview.up('viewport').down('#dealsbtn');
		me.getController('DashboardController').setTabPanel(dealsbtn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-deals'));
		
		var dealid = record.get('DM-dealNo');
		var magicDealID = record.get('LT-dealNo');
		record.set('dealid',dealid);
		record.set('idDeal',dealid);
		record.set('magicDealID',magicDealID);
		
		me.getController('DealController').showDealDetail(record, dataview.up('viewport').down('tab-deal-grid'));
	},
	
	onContactSaleHistoryGridItemDblClick: function(dataview, record, item, index, e, eOpts){
		var me = this;
		var salesbtn = dataview.up('viewport').down('#salesbtn');
		me.getController('DashboardController').setTabPanel(salesbtn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-sales'));
		
		var dealid = record.get('DM-dealNo');
		var magicDealID = record.get('LT-dealNo');
		record.set('dealid',dealid);
		record.set('idDeal',dealid);
		record.set('magicDealID',magicDealID);
		
		me.getController('SalesController').showDealDetail(record, dataview.up('viewport').down('tab-sales-grid'));
	},
	
	dealHistoryContactTabClick: function(tabItem){
		console.log("Set Deal History Tab");
		var me = this;
		tabItem.up('tabpanel').setActiveTab(tabItem.up('tabpanel').down('#contactdealhistorygrid'));
		me.onCategoryChange("Contacts", tabItem.down('menu'),tabItem.up('tabpanel').down('#contactdealhistorygrid'),"deal");
	},
	
	saleHistoryContactTabClick: function(tabItem){
		console.log("Set Sale History Tab");
		var me = this;
		tabItem.up('tabpanel').setActiveTab(tabItem.up('tabpanel').down('#contactsalehistorygrid'));
		me.onCategoryChange("Contacts", tabItem.down('menu'),tabItem.up('tabpanel').down('#contactsalehistorygrid'),"sale");
	},
	
	dealHistoryContactTabMenuClick: function(menu, item, e, eOpts){
		console.log("Deal History Menu Clicked");
        var me = this;
        var text = item.text;
		// me.getDealdetailsformpanel().down('#dealdetailtabpanel').setActiveTab(6);
		menu.up('tabpanel').setActiveTab(menu.up('tabpanel').down('#contactdealhistorygrid'));
		me.onCategoryChange(text, menu, menu.up('tabpanel').down('#contactdealhistorygrid'), "deal");
	},
	
	saleHistoryContactTabMenuClick: function(menu, item, e, eOpts){
		console.log("Deal History Menu Clicked");
        var me = this;
        var text = item.text;
		// me.getDealdetailsformpanel().down('#dealdetailtabpanel').setActiveTab(6);
		menu.up('tabpanel').setActiveTab(menu.up('tabpanel').down('#contactsalehistorygrid'));
		me.onCategoryChange(text, menu, menu.up('tabpanel').down('#contactsalehistorygrid'), "sale");
	},
	
	onCategoryChange: function(newValue, menu, historyGrid,historyType) {
        var me = this,contactid,row,maingrid,dealid;
		dealhistory_st = historyGrid.getStore();
        var p = menu.up('tab-contact-detail');
        var anchor = "detail";
        if(!p) {
            anchor = "grid";
            p = menu.up('tab-contact-layout');            
        }
        if(anchor == "grid") {
            maingrid = p.down('tab-contact-grid');
			if(maingrid.getSelectionModel().hasSelection())	{
            	row = maingrid.getSelectionModel().getSelection()[0];
            	contactid = row.data.contactid;
			}else{
				return false;
			}
        } else {
            contactid = p.contactid;
        }
        
		var viewname;
		console.log(newValue);
		var filterstarr = [];
		if(newValue=="Addressess"){
			viewname = "address";
		}else if(newValue=="Streets"){
			viewname = "street";
		}else if(newValue=="Contacts"){
			viewname = "contact";
		}else if(newValue=="Company"){
			viewname = "company";
		}
		console.log("viewname"+viewname);
		var filterst = "reqtype="+viewname;
        filterstarr.push(filterst);
		var filterst1 = "contactid="+contactid;
        filterstarr.push(filterst1);
		
        var mainViewport = Ext.ComponentQuery.query('mainviewport')[0];
        
		var url;
		if(historyType=="deal"){
			url = DM2.view.AppConstants.apiurl+'DealHistoryByContactID';
		} else if(historyType=="sale") {
			url = DM2.view.AppConstants.apiurl+'SaleHistoryByContactID';
		}
		
		/////
		var dealhistorystproxy = dealhistory_st.getProxy();
		var dealhistorystproxyCfg = dealhistorystproxy.config;
        dealhistorystproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+ mainViewport.config.apikey + ':1'
        };
		dealhistorystproxyCfg.originalUrl = url;
		dealhistorystproxyCfg.url = url;
		dealhistorystproxyCfg.extraParams = {
            filter :filterstarr
        };
		dealhistory_st.setProxy(dealhistorystproxyCfg);
		/////
		
        dealhistory_st.load();
    },
	
	contactTabPanelAfterRender: function(cmp, eOpts){
		var me = this,starttab=2,tabItem;		
		var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
		if(menuFunc==1){
			var dealhistorycontacttab = Ext.create('DM2.view.DealHistoryContactTabMenu',{
				itemId:'dealhistorycontacttab',
				style:'margin-right:5px',
				ui:'tabsplitbutton'
			});
			cmp.tabBar.insert(starttab, dealhistorycontacttab);
			starttab++;
			//Insert Deal History Grid & Tab		
			tabItem = Ext.create('DM2.view.contact.ContactDealHistoryGrid',{
				iconCls: 'loanbtn',
				itemId : 'contactdealhistorygrid',
				title: 'Deal History',
				tabConfig: {
					hidden: true
				}
			});
			cmp.add(tabItem);
			starttab++;
		}
		
		var menuFunc = me.getController('MainController').getMenuStatus("salemenu");
		if(menuFunc==1){
			//Add Sale History
			var salehistorycontacttab = Ext.create('DM2.view.DealHistoryContactTabMenu',{
				itemId:'salehistorycontacttab',
				ui:'tabsplitbutton'
			});
			salehistorycontacttab.setText("Sale History");
			cmp.tabBar.insert(starttab, salehistorycontacttab);
			//Insert Sale History Grid & Tab		
			tabItem = Ext.create('DM2.view.contact.ContactDealHistoryGrid',{
				iconCls: 'loanbtn',
				itemId : 'contactsalehistorygrid',
				title: 'Sale History',
				tabConfig: {
					hidden: true
				}
			});
			cmp.add(tabItem);
		}
		
	},
	
	contactTabChange: function(tabPanel, newCard, oldCard, eOpts){
		var me = this;
		var dealhistorycontacttab = tabPanel.down('#dealhistorycontacttab');
		var salehistorycontacttab = tabPanel.down('#salehistorycontacttab');
		if(newCard.xtype=="contactdealhistorygrid" && newCard.getItemId()=="contactdealhistorygrid"){
			if(dealhistorycontacttab){dealhistorycontacttab.addCls("docactivebtncls");}
			if(salehistorycontacttab){salehistorycontacttab.removeCls("docactivebtncls");}
		} else if(newCard.xtype=="contactdealhistorygrid" && newCard.getItemId()=="contactsalehistorygrid"){
			if(dealhistorycontacttab){dealhistorycontacttab.removeCls("docactivebtncls");}
			if(salehistorycontacttab){salehistorycontacttab.addCls("docactivebtncls");}
		} else /*if(newCard.xtype=="propertycontactsgridpanel")*/{
			if(dealhistorycontacttab){dealhistorycontacttab.removeCls("docactivebtncls");}
			if(salehistorycontacttab){salehistorycontacttab.removeCls("docactivebtncls");}
		}
	},
	
	afterTabContactLayoutRender: function(panel){
		var me = this;
		panel.down('#contact-east-panel').getHeader().hide();
	},

	showRecvDealDocs: function(cmp){
		var me = this,
            tabcntpanel, context, spawnFrom, grid;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			tabcntpanel = grid.up('tab-contacts');
			context = tabcntpanel.getActiveTab();
		} else {
		    // when cmp is a button.
			tabcntpanel = cmp.up('tab-contacts');
			context = tabcntpanel.getActiveTab();	    
		}
		if(context.xtype == "tab-contact-layout"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDocs',context);	
        }
        else if(context.xtype == "tab-contact-detail"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
		}
	},
	
	closeRecvDealDetailDocs: function(context) {
		var me = this;
		if(context.xtype == "tab-contact-layout"){
			var eastregion = context.down('#contact-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			//eastregion.setVisible(false);
			
			var contactdetailsform;
			if(context.down('contactdetailsform')){
				console.log("Use Old Panel");
				contactdetailsform = context.down('contactdetailsform');
			} else {
				console.log("Create New east add deal doc Panel");
				contactdetailsform = Ext.create('DM2.view.ContactDetailsForm');//me.getContactdetailsform();
				eastregion.add(contactdetailsform);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(contactdetailsform);			
			eastregion.setTitle("Contact Details");
			eastregion.getHeader().hide();
			
			var contactgrid = context.down('tab-contact-grid');
			if(contactgrid.getSelectionModel().hasSelection()){
				row = contactgrid.getSelectionModel().getSelection()[0];
				contactgrid.fireEvent('select',contactgrid.getSelectionModel(),row);
				//propmasterdetailsform.getForm().loadRecord(row);
    	   		//propmasterdetailsform.setTitle("Property Details");
			} else {
				return false;
			}
        }
        else if(context.xtype == "tab-contact-detail"){
	        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
    	    eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
	},
	
	bindDealContactGrid: function(tabItem){
		console.log("Bind Contact Details");
		var me = this;
		var panel = tabItem.down('dealcontactsgridpanel');
		var dealcntst = panel.getStore();
		dealcntst.loadData(tabItem.dealDetailStore.data.items[0].data.ContactPerDeal);
		me.afterContactDataLoad(panel);		
	},
	clearDealContactGrid: function(tabItem){
		console.log("Bind Contact Details");
		var me = this;
		var panel = tabItem.down('dealcontactsgridpanel');
		var dealcntst = panel.getStore();
		dealcntst.removeAll();
		me.afterContactDataLoad(panel);		
	},
	afterContactDataLoad: function(panel){
		var dealnotesst = panel.getStore();
		var count = dealnotesst.getCount();		
		var contacttoolbar = panel.down('toolbar');
		if(count>0){
			if(contacttoolbar){
				contacttoolbar.hide();
			}
		} else {
			if(contacttoolbar){
				contacttoolbar.show();
				if(panel.up('tab-deal-detail')){
					tabItem = panel.up('tab-deal-detail');
				} else if(panel.up('tab-deal-layout')){
					tabItem = panel.up('tab-deal-layout');
				} else if(panel.up('tab-sales-layout')){
					tabItem = panel.up('tab-sales-layout');
				} else if(panel.up('tab-sales-detail')){
					tabItem = panel.up('tab-sales-detail');
				}
				if(tabItem.dealDetailStore){
					permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
					console.log("Permission : "+permission);
					if(parseInt(permission)===0){						
						contacttoolbar.down('button').disable();
					} else {
						contacttoolbar.down('button').enable();
					}
				}
			}
		}
	},
	
    loadContactsStore: function(dealid) {
        var me = this;
        var contactsproxy = Ext.getStore('Contacts').getProxy();
        contactsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('Contacts').load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
                var dealdetailcontacttoolbar = me.getController('DealDetailController').getDealdetailcontacttoolbar();
                var contactscnt = Ext.getStore('Contacts').getCount();
                console.log("Contacts Cnt"+contactscnt);
                if(contactscnt > 0){
                    if(dealdetailcontacttoolbar){
                        dealdetailcontacttoolbar.hide();
                        me.getController('DealDetailController').getDealdetailsformpanel().down('#dealdetailcontactgrid').setHeight(142);
                    }
                }else{
                    if(dealdetailcontacttoolbar){
                        dealdetailcontacttoolbar.show();
                        me.getController('DealDetailController').getDealdetailsformpanel().down('#dealdetailcontactgrid').setHeight(115);
                    }
                }
            }
        });
    },

    loadDealContactDetails: function(dealid) {
        var me = this;
        var dealdetailcontactsproxy = Ext.getStore('DealDetailContacts').getProxy();
        dealdetailcontactsproxy.setHeaders({
            // Accept: 'application/json',
            // Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            // 'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: ('Espresso ' + me.getController('MainController').getMainviewport().config.apikey + ':1')            
        });
        Ext.getStore('DealDetailContacts').load({
           // url: DM2.view.AppConstants.apiurl+'mssql:Contact/'+record.get('contactid'),
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
                var count = Ext.getStore('DealDetailContacts').getCount();
                var panel = me.getDealdetailcontactfieldset();
                var singleDetail = panel.detailOnSingleRecord; 
                if(count === 1 && singleDetail == true) {
                    panel.setData(records[0]);
                    panel.loadForm(panel);
                }                 

                // var contactscnt = Ext.getStore('DealDetailContacts').getCount();
// 
                // var cntfldset = me.getController('DealDetailController').getDealdetailcontactfieldset();
                // cntfldset.down('#contactdetaildealclosebtn').hide();
// 
                // var grid = cntfldset.down('#dealdetailcontactgrid');
                // var cntform = cntfldset.down('#dealdetailcontactform');
// 
                // if(contactscnt === 0){
                    // CG: 10/11/2015
                    // If this is understood, we are hiding the entire contacts panel, makes no sense, how do you add a contact?

                    // cntfldset.getLayout().setActiveItem(grid);
                    // //cntfldset.down('#dealdetailcontactform').hide();
                    // //cntfldset.down('gridpanel').hide();
                // }else if(contactscnt === 1){
                    // cntfldset.getLayout().setActiveItem(cntform);
                    // //Load the one contact Details into form
                    // var rec = Ext.getStore('DealDetailContacts').getAt(0);
                    // me.getController('DealDetailController').displayContactDetailForm(rec);
                // }else if(contactscnt > 1){
                    // cntfldset.getLayout().setActiveItem(grid);
                    // //cntfldset.down('#dealdetailcontactform').hide();
                    // //cntfldset.down('gridpanel').show();
                // }
            }
        });
    },

    loadAllContacts: function(filterstarr) {
        var me = this;
		console.log("Load All Contacts in buffered grid.");
		var contactallbuff_st = Ext.getStore('Contactallbuff');
        var contactsproxy = contactallbuff_st.getProxy();
        /*contactsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var contactsproxyCfg = contactsproxy.config;
        contactsproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		contactallbuff_st.setProxy(contactsproxyCfg);
        contactallbuff_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                console.log(success);
                if(success) {
                    if(records.length > 0){
						if(me.getContactgrid()){
	                        me.getContactgrid().getSelectionModel().select(0);
						}
                    }
                }
            }
        });
    },

    loadContactDetailForm: function(selModel, record, index, eOpts) {
        var me = this;
		var contactid = record.get('contactid');
		var context = selModel.view.ownerCt.up('tab-contact-layout');
		var contactdetailsform = context.down('contactdetailsform');
		
		me.loadContactDetails(contactid,context);
		
		contactdetailsform.down('tool[type="close"]').hide();
		
		if(me.getContacttabpanel().down('#dealhistorycontacttab')){
			if(me.getContacttabpanel().getActiveTab().xtype==="contactdealhistorygrid" && me.getContacttabpanel().getActiveTab().getItemId()==="contactdealhistorygrid"){
				me.onCategoryChange("Contacts", me.getContacttabpanel().down('#dealhistorycontacttab').down('menu'),me.getContacttabpanel().getActiveTab(),"deal");
			}
		}        
    },
	
	loadContactDetails: function(contactid,context){
		var me = this,filterstarr = [];
		var filterst = "idContact = '"+contactid+"'";
        filterstarr.push(filterst);
		
		var contactdetailsform = context.down('contactdetailsform');
		contactdetailsform.setLoading(true);
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'RcontactDetails',
			params: {
				filter:filterstarr
			},
			method:'GET',
			success: function(response, opts) {
				contactdetailsform.setLoading(false);
				var obj = Ext.decode(response.responseText);
				if(response.status == 200) {
					if(obj.length>0){
						var cntObj = obj[0];				
						
						var cntrec = Ext.create('DM2.model.Dealhascontact',obj[0]);
						contactdetailsform.config.cntdetailrec = cntrec;
						
						contactdetailsform.getForm().findField('idContact').setRawValue(obj[0].idContact);
						contactdetailsform.getForm().findField('idCompany').setRawValue(obj[0].idCompany);
						contactdetailsform.getForm().findField('title').setRawValue(obj[0].title);
						contactdetailsform.getForm().findField('isMyContact').setRawValue(obj[0].isMyContact.ismycontact);
						contactdetailsform.getForm().findField('isMyContactOriginal').setRawValue(obj[0].isMyContact.ismycontact);
						contactdetailsform.getForm().findField('idUserxCont').setRawValue(obj[0].isMyContact.idUserxcont);
						contactdetailsform.getForm().findField('firstName').setRawValue(obj[0].firstName);
						contactdetailsform.getForm().findField('lastName').setRawValue(obj[0].lastName);
						contactdetailsform.getForm().findField('companyName').setRawValue(obj[0].companyName);
						contactdetailsform.getForm().findField('position').setRawValue(obj[0].position);
						contactdetailsform.getForm().findField('showCompany').setRawValue(obj[0].showCompany);
						///Load Contact Type Values
						var cntTypecombo = contactdetailsform.getForm().findField('contactType');
						me.getController('ContactController').loadContactType(cntTypecombo,obj[0].contactType);
						
						var cntphgrid    = contactdetailsform.down('contactphonesgridpanel');
						var cntemailgrid = contactdetailsform.down('contactemailsgridpanel');
						var cntmiscgrid  = contactdetailsform.down('contactmiscgridpanel');
						var cntaddressgrid = contactdetailsform.down('contactaddressgridpanel');
						var cntfaxgrid = contactdetailsform.down('contactfaxgridpanel');

						cntphgrid.show();
						cntemailgrid.show();
						cntmiscgrid.show();
						cntaddressgrid.show();
						cntfaxgrid.show();
						
						/*
						me.bindContactPhones(cntObj.phone,context); // Bind Contact Phones
						me.bindContactEmails(cntObj.email,context); // Bind Contact Emails
						me.bindContactFax(cntObj.fax,context); // Bind Contact Fax
						me.bindContactAddress(cntObj.address,context); // Bind Contact Address
						if(cntObj.misc){
							me.bindContactMisc(cntObj.misc,context); // Bind Contact Misc
						}*/
						me.bindContactMetaGrid(cntObj.phone,cntphgrid); // Bind Contact Phones
						me.bindContactMetaGrid(cntObj.email,cntemailgrid); // Bind Contact Emails
						me.bindContactMetaGrid(cntObj.fax,cntfaxgrid); // Bind Contact Fax
						me.bindContactMetaGrid(cntObj.address,cntaddressgrid); // Bind Contact Address
						if(cntObj.misc){
							me.bindContactMetaGrid(cntObj.misc,cntmiscgrid); // Bind Contact Misc
						}
						
						me.bindContactDocs(cntObj.docsPerContact,context);
						me.bindContactNotes(cntObj.notesPerContact,context);
						
						contactdetailsform.down('#contactdetailformresetbtn').show();
						contactdetailsform.down('#contactdetailformaddbtn').setDisabled(true);
						contactdetailsform.down('#contactdetailformresetbtn').setDisabled(true);
					}
				}
			},
			failure: function(response, opts) {
				console.log("Login Failure.");
				contactdetailsform.setLoading(false);
				Ext.Msg.alert('Failed', "Please try again.");
			}
		});
	},
	
	// Bind Contact Meta Grid Data
	bindContactMetaGrid: function(recs,grid){
		var me = this;
		grid.getStore().getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
		grid.getStore().loadData(recs);
		me.afterContactMetaDataLoad(grid);
	},
	
	// Bind Contact Phones
	bindContactPhones: function(phonerecs,context){
		var me = this;
		var cntphgrid = context.down('contactphonesgridpanel');
		cntphgrid.getStore().loadData(phonerecs);
		me.afterContactMetaDataLoad(cntphgrid);
	},
	
	// Bind Contact Emails
	bindContactEmails: function(emailrecs,context){
		var me = this;		
		var cntEmailsGrid = context.down('contactemailsgridpanel');
		cntEmailsGrid.getStore().loadData(emailrecs);
		me.afterContactMetaDataLoad(cntEmailsGrid);
	},
	
	// Bind Contact Fax
	bindContactFax: function(faxrecs,context){
		var me = this;
		var cntfaxgrid = context.down('contactfaxgridpanel');
		cntfaxgrid.getStore().loadData(faxrecs);
		me.afterContactMetaDataLoad(cntfaxgrid);
	},
	
	// Bind Contact Address
	bindContactAddress: function(addrrecs,context){
		var me = this;
		var cntaddrgrid = context.down('contactaddressgridpanel');
		cntaddrgrid.getStore().loadData(addrrecs);
		me.afterContactMetaDataLoad(cntaddrgrid);
	},
	
	// Bind Contact Misc
	bindContactMisc: function(miscrecs,context){
		var me = this;
		var cntmiscgrid = context.down('contactmiscgridpanel');
		cntmiscgrid.getStore().loadData(miscrecs);
		//me.afterContactMetaDataLoad(cntmiscgrid);
	},
	
	afterContactMetaDataLoad: function(cntmetagrid){
		var me = this;
		if(cntmetagrid.menu){
			var menuItems = cntmetagrid.menu.items.items;
			for(var i=0;i<menuItems.length;i++){
				menuItems[i].show();						
			}
			var metagridst = cntmetagrid.getStore();
			for(var j=0; j < metagridst.getCount(); j++){
				var item = metagridst.getAt(j);						
				var tagRec = item.get('tag');			
				for(var i=0;i<menuItems.length;i++){
					var tagLabel = menuItems[i].tag; //menuItems[i].getText();
					if(tagLabel.toUpperCase()==tagRec.toUpperCase()){
						menuItems[i].hide();
					}
				}
			}
		}
	},
		
	clearContactDocs: function(context){
		var me = this;
		if(context.down('contactdocsgridpanel')){
			var cntdocgrid = context.down('contactdocsgridpanel');
			var cntdocst = cntdocgrid.getStore();
			cntdocst.removeAll();
			me.afterContactDocsLoad(cntdocgrid);
		}
	},
	
	bindContactDocs: function(docsPerContact,context){
		var me = this;
		if(context.down('contactdocsgridpanel')){
			var cntdocgrid = context.down('contactdocsgridpanel');
			var cntdocst = cntdocgrid.getStore();
			cntdocst.loadData(docsPerContact);
			me.afterContactDocsLoad(cntdocgrid);
		}
	},
	
	afterContactDocsLoad: function(cntdocgrid){
		var me = this;		
		var cntdoctbar = cntdocgrid.down('toolbar');
		if(cntdoctbar){
			var cntdocst = cntdocgrid.getStore();
			if(cntdocst.getCount()>0){
				cntdoctbar.hide();
			} else {
				cntdoctbar.show();
			}
		}
	},
	
	clearContactNotes: function(context){
		var me = this;
		if(context.down('contactnotesgridpanel')){
			var cntnotegrid = context.down('contactnotesgridpanel');
			var cntnotest = cntnotegrid.getStore();
			cntnotest.removeAll();
			me.afterContactNotesLoad(cntnotegrid);
		}
	},
	
	bindContactNotes: function(notesPerContact,context){
		var me = this;
		if(context.down('contactnotesgridpanel')){
			var cntnotegrid = context.down('contactnotesgridpanel');
			var cntnotest = cntnotegrid.getStore();
			cntnotest.loadData(notesPerContact);
			me.afterContactNotesLoad(cntnotegrid);
		}
	},
	
	afterContactNotesLoad: function(cntnotegrid){
		var me = this;		
		var cntnotestbar = cntnotegrid.down('toolbar');
		if(cntnotestbar){
			var cntnotesst = cntnotegrid.getStore();
			if(cntnotesst.getCount()>0){
				cntnotestbar.hide();
			} else {
				cntnotestbar.show();
			}
		}
	},
	
	loadContactType: function(cntTypecombo,cntTypeVal){
		var me = this;
		var ctypesst = Ext.getStore('ContactTypes');
        if(ctypesst.isLoaded()){
            cntTypecombo.select(cntTypeVal);
        }
	},
	
	loadContactTypeStore: function(){
		var me = this;
		var ctypesst = Ext.getStore('ContactTypes');
        if(!ctypesst.isLoaded()){
            var contacttypeproxy = ctypesst.getProxy();
            contacttypeproxy.setHeaders({
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            });
            ctypesst.load();
        }
	},
	
	loadFaxRecords: function(idContact) {
        var me = this;
		console.log("Load Fax Records : "+idContact);
        var cntfaxproxy = Ext.getStore('ContactFaxes').getProxy();
        cntfaxproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
		
		var cntfaxSt;
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-panel');
		var menutabpanel = tabpanel.getLayout().getActiveItem();
		var menuactivetab = menutabpanel.getActiveTab();
		
		if(menuactivetab.down('contactfaxgridpanel')){
			cntfaxSt = me.getContactfaxgridpanel().getStore();
			cntfaxSt.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			});
		}
		
        Ext.getStore('ContactFaxes').load({
            params:{
				filter :'idContact = '+idContact
            },
			callback: function(records, operation, success){
				if(menuactivetab.down('contactfaxgridpanel')){
					var cntfaxgrid = menuactivetab.down('contactfaxgridpanel');
					cntfaxSt.loadData(records);
					me.afterContactMetaDataLoad(cntfaxgrid);
				}
			}
        });
    },
	
    loadPhoneRecords: function(idContact) {
        var me = this;
		console.log("Load Phone Records : "+idContact);
        var cntphproxy = Ext.getStore('ContactPhones').getProxy();
        cntphproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
		
		var cntphSt;
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-panel');
		var menutabpanel = tabpanel.getLayout().getActiveItem();
		var menuactivetab = menutabpanel.getActiveTab();
		
		if(menuactivetab.down('contactphonesgridpanel')){
			cntphSt = me.getContactphonesgridpanel().getStore();
			cntphSt.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			});
		}
		
        Ext.getStore('ContactPhones').load({
            params:{
				filter :'idContact = '+idContact
            },
			callback: function(records, operation, success){
				if(menuactivetab.down('contactphonesgridpanel')){
					var cntphgrid = menuactivetab.down('contactphonesgridpanel');
					cntphSt.loadData(records);					
					me.afterContactMetaDataLoad(cntphgrid);
				}
			}
        });
    },

    onSelectChangePhone: function(selModel, selected, eOpts) {
        this.getContactphonesgridpanel().down('#delete').setDisabled(selected.length === 0);
    },

    onDeletePhoneClick: function() {
        console.log("Contact Phone Delete");
        var me = this;
        var row = this.getContactphonesgridpanel().getSelectionModel().getSelection()[0];
        console.log(row.get('idPhone'));
        var idPhone = row.get('idPhone');
        var idContact = row.get('idContact');

        Ext.Msg.show({
            title:'Delete Phone?',
            message: 'Are you sure you want to delete the phone from this contact?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    me.getContactphonesgridpanel().setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Phone/'+idPhone+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            me.getContactphonesgridpanel().setLoading(false);
                            console.log("Phone is removed from Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                me.loadPhoneRecords(idContact);
                            } else {
                                console.log("Delete Phone Failure.");
                                Ext.Msg.alert('Delete Phone failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            me.getContactphonesgridpanel().setLoading(false);
                            console.log("Delete Phone Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Phone failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    onAddPhoneClick: function() {
        console.log("Contact Phone Add");
        var me = this;
        var rec = Ext.create('DM2.model.ContactPhone',{
            idContact:me.getContactdetailsform().getForm().findField('idContact').getValue(),
            tag: '',
            phoneNumber: ''
        });

        //var celledit = me.getContactphonesgridpanel().getPlugin('contactphoneeditingplugin');

        //celledit.cancelEdit();
        me.getContactphonesgridpanel().store.insert(0, rec);
        /*celledit.startEditByPosition({
            row: 0,
            column: 0
        });*/
    },

    onSelectChangeEmail: function(selModel, selected, eOpts) {
        this.getContactemailsgridpanel().down('#delete').setDisabled(selected.length === 0);
    },

    onDeleteEmailClick: function() {
        console.log("Contact Email Delete");
        var me = this;
        var row = this.getContactemailsgridpanel().getSelectionModel().getSelection()[0];
        console.log(row.get('idEmail'));
        var idEmail = row.get('idEmail');
        var idContact = row.get('idContact');

        Ext.Msg.show({
            title:'Delete Email?',
            message: 'Are you sure you want to delete the Email from this contact?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    me.getContactemailsgridpanel().setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Email/'+idEmail+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            me.getContactemailsgridpanel().setLoading(false);
                            console.log("Email is removed from Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                me.loadEmailRecords(idContact);
                            } else {
                                console.log("Delete Email Failure.");
                                Ext.Msg.alert('Delete Email failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            me.getContactemailsgridpanel().setLoading(false);
                            console.log("Delete Email Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Email failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    onAddEmailClick: function() {
        console.log("Contact Email Add");
        var me = this;
        var rec = Ext.create('DM2.model.ContactEmail',{
            idContact:me.getContactdetailsform().getForm().findField('idContact').getValue(),
            tag: '',
            email: ''
        });

        //var edit = me.getContactemailsgridpanel().editing;

        //edit.cancelEdit();
        me.getContactemailsgridpanel().store.insert(0, rec);
        /*edit.startEditByPosition({
                    row: 0,
                    column: 0
                });*/
    },

    loadEmailRecords: function(idContact) {
        var me = this;
        var cntemailproxy = Ext.getStore('ContactEmails').getProxy();
        cntemailproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
		
		var cntEmailSt;
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-panel');
		var menutabpanel = tabpanel.getLayout().getActiveItem();
		var menuactivetab = menutabpanel.getActiveTab();
		
		if(menuactivetab.down('contactemailsgridpanel')){
			cntEmailSt = menuactivetab.down('contactemailsgridpanel').getStore();
			cntEmailSt.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			});
		}
		
        Ext.getStore('ContactEmails').load({
            params:{
				filter :'idContact = '+idContact
            },
			callback: function(records, operation, success){
				
				if(menuactivetab.down('contactemailsgridpanel')){
					var cntEmailsGrid = menuactivetab.down('contactemailsgridpanel');
					cntEmailSt.loadData(records);
					me.afterContactMetaDataLoad(cntEmailsGrid);
				}								
			}
        });
    },

    onSelectChangeMisc: function(selModel, selected, eOpts) {
        this.getContactmiscgridpanel().down('#delete').setDisabled(selected.length === 0);
    },

    onDeleteMiscClick: function() {
        console.log("Contact Misc Delete");
        var me = this;
        var row = this.getContactmiscgridpanel().getSelectionModel().getSelection()[0];
        console.log(row.get('idMisc'));
        var idMisc = row.get('idMisc');
        var idContact = row.get('idContact');

        Ext.Msg.show({
            title:'Delete Misc?',
            message: 'Are you sure you want to delete the Misc from this contact?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    me.getContactmiscgridpanel().setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Misc/'+idMisc+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            me.getContactmiscgridpanel().setLoading(false);
                            console.log("Misc is removed from Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                me.loadMiscRecords(idContact);
                            } else {
                                console.log("Delete Misc Failure.");
                                Ext.Msg.alert('Delete Misc failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            me.getContactmiscgridpanel().setLoading(false);
                            console.log("Delete Misc Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Misc failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    onAddMiscClick: function() {
        console.log("Contact Misc Add");
        var me = this;
        var rec = Ext.create('DM2.model.ContactMisc',{
            idContact:me.getContactdetailsform().getForm().findField('idContact').getValue(),
            tag: '',
            miscValue: ''
        });

        //var edit = me.getContactmiscgridpanel().editing;

        //edit.cancelEdit();
        me.getContactmiscgridpanel().store.insert(0, rec);
        /*edit.startEditByPosition({
                            row: 0,
                            column: 0
                        });*/
    },

    loadMiscRecords: function(idContact) {
        var me = this;
        var cntmiscproxy = Ext.getStore('ContactMiscs').getProxy();
        cntmiscproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
		
		var cntMiscSt;
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-panel');
		var menutabpanel = tabpanel.getLayout().getActiveItem();
		var menuactivetab = menutabpanel.getActiveTab();
		
		if(menuactivetab.down('contactmiscgridpanel')){
			cntMiscSt = menuactivetab.down('contactmiscgridpanel').getStore();
			cntMiscSt.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			});
		}
		
        Ext.getStore('ContactMiscs').load({
            params:{
				filter :'idContact = '+idContact
            },
			callback: function(records, operation, success){
				if(success){
					if(menuactivetab.down('contactmiscgridpanel')){
						var cntmiscgrid = menuactivetab.down('contactmiscgridpanel');
						cntMiscSt.loadData(records);
					}
				}
			}
        });
    },

    onSelectChangeAddress: function(selModel, selected, eOpts) {
        this.getContactaddressgridpanel().down('#delete').setDisabled(selected.length === 0);
    },

    onDeleteAddressClick: function() {
        console.log("Contact Misc Delete");
        var me = this;
        var row = this.getContactaddressgridpanel().getSelectionModel().getSelection()[0];
        console.log(row.get('idAddress'));
        var idAddress = row.get('idAddress');
        var idContact = row.get('idContact');

        Ext.Msg.show({
            title:'Delete Misc?',
            message: 'Are you sure you want to delete the Address from this contact?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    me.getContactaddressgridpanel().setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Address/'+idAddress+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            me.getContactaddressgridpanel().setLoading(false);
                            console.log("Address is removed from Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                me.loadAddressRecords(idContact);
                            } else {
                                console.log("Delete Address Failure.");
                                Ext.Msg.alert('Delete Address failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            me.getContactaddressgridpanel().setLoading(false);
                            console.log("Delete Address Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Address failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    onAddAddressClick: function() {
         console.log("Contact Address Add");
        var me = this;
        var rec = Ext.create('DM2.model.ContactAddress',{
            idContact:me.getContactdetailsform().getForm().findField('idContact').getValue(),
            tag: '',
            address: '',
            city: '',
            state: '',
            zip: ''
        });

        //var edit = me.getContactaddressgridpanel().editing;

        //edit.cancelEdit();
        me.getContactaddressgridpanel().store.insert(0, rec);
        /*edit.startEditByPosition({
                                    row: 0,
                                    column: 0
                                });*/
    },

    loadAddressRecords: function(idContact) {
         var me = this;
        var cntaddrproxy = Ext.getStore('ContactAddress').getProxy();
        cntaddrproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
		var cntAddressSt;
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-panel');
		var menutabpanel = tabpanel.getLayout().getActiveItem();
		var menuactivetab = menutabpanel.getActiveTab();
		
		if(menuactivetab.down('contactaddressgridpanel')){
			cntAddrSt = menuactivetab.down('contactaddressgridpanel').getStore();
			cntAddrSt.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			});
		}
        Ext.getStore('ContactAddress').load({
            params:{
				filter :'idContact = '+idContact				
            },
			callback: function(records, operation, success){
				if(menuactivetab.down('contactaddressgridpanel')){
					var cntaddrgrid = menuactivetab.down('contactaddressgridpanel');
					cntAddrSt.loadData(records);
					me.afterContactMetaDataLoad(cntaddrgrid);
				}
			}
        });
    },

    loadContactDocsStore: function(idContact,cntdocsgrid) {
        var me = this;
        var cntdocst = cntdocsgrid.getStore();
        cntdocst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        cntdocst.load({
            params:{
                filter :'idContact = '+idContact
            },
            callback: function(records, operation, success){
				me.afterContactDocsLoad(cntdocsgrid);
            }
        });
    },

    loadContactNotesStore: function(idContact,context) {
        console.log("Notes Load");
        var me = this;
		var cntnotegrid = context.down('contactnotesgridpanel');
		var cntNoteSt = cntnotegrid.getStore();
        cntNoteSt.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        cntNoteSt.load({
            params:{
                filter :'idContact = '+idContact
            },
            callback: function(records, operation, success){
                me.afterContactNotesLoad(cntnotegrid);
            }
        });
    },
	
	onContactsGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		console.log("onContactsGridPanelRightClick : Show Menu for Contact Add/Delete");
		var grid = view.up('tab-contact-grid');
		grid.getSelectionModel().select(record);
		me.showContactGridMenu(grid,record,e);
	},
	
	showContactGridMenu: function(grid,record,e){
		console.log("onContactsGridPanelRightClick : Show Menu for Contact Add/Delete");
		var me = this;
		e.stopEvent();
		var contactmenu = grid.contextMenu;		
        if(contactmenu){
			contactmenu.spawnFrom = grid;
            contactmenu.showAt(e.getXY());
			var cntmenu = me.getController('DashboardController').getDashboardcontainer().down('#contactsbtn');
			if(cntmenu.getText()=="Contacts" || cntmenu.getText()=="All Contacts"){
				console.log("Set Menu For All Contacts");
				var removefrommycontactmenu = contactmenu.down('menuitem[action="removefrommycontact"]');
				removefrommycontactmenu.hide();
				var removecontactmenu = contactmenu.down('menuitem[action="removecontact"]');
				removecontactmenu.hide();
			} else if(cntmenu.getText()=="My Contacts"){
				console.log("Set Menu For My Contacts");
				var removefrommycontactmenu = contactmenu.down('menuitem[action="removefrommycontact"]');
				removefrommycontactmenu.show();
				var removecontactmenu = contactmenu.down('menuitem[action="removecontact"]');
				removecontactmenu.hide();
			}
			var idContact = record.get('contactid');
			me.buildContactSubMenu(idContact,contactmenu);
        }
	},
	
    onContactsGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
		var me = this;
		var grid = view.up('tab-contact-grid');
		me.showContactGridMenu(grid,record,e);
    },

    contactMenuClick: function(menu, item, e, eOpts) {
        console.log("Contact Grid Sub Menu Clicked");
		var me = this;
        if(item.action === "addnewcontact"){
            console.log("Add New Contact");
            me.showAddContact();
        }else if(item.action === "removecontact"){
            console.log("Remove Contact");
            me.removeContact();
        }else if(item.action === "editcontact"){
            console.log("Edit Contact");
        }else if(item.action === "uploadcontact"){
            console.log("Upload Contact");
			me.showUploadContactView(item);
        }else if(item.action === "addtomycontact"){
            console.log("Show Add To My Contact");
			var cntmenu = me.getController('DashboardController').getDashboardcontainer().down('#contactsbtn');
			if(cntmenu.getText()=="Contacts" || cntmenu.getText()=="All Contacts"){
				console.log("Add selected cnt to My Contacts");
				me.addSelCntToMyContact();
			} else if(cntmenu.getText()=="My Contacts"){
				me.showAddToMyContact(item);
			}
        } else if(item.action === "removefrommycontact"){
            console.log("Remove From My Contact");
            me.removeFromMyContact();
        } else if(item.action === "createnewsaledealfromcnt"){
            console.log("Create New Sale Deal From Contact Click");
            me.getController('CreateNewDealFromPropertyController').CreateNewSalesDealClick("cntgrid");
        }
    },

    contactDetailFormCloseBtnClick: function(panel) {
		console.log("contact Detail Form Close Btn Click"+panel.config.backview);
		var me = this;
		panel.show();
		if(panel.up('tab-contact-layout')){
			panel.up('tab-contact-layout').down('tab-contact-grid').getSelectionModel().select(0);
		} else {
			if(panel.up('contactdetailsform').config.backview == "adddealxcontactview"){
				var eastregion = panel.up('contactdetailsform').up('[region=east]');	
				eastregion.getLayout().setActiveItem(eastregion.down('adddealcontactform'));
			} else if(panel.up('contactdetailsform').config.backview == "dealcontactgrid"){
				var context = me.getController('DealDetailController').getTabContext(panel);
				if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
					if(context.xtype == "tab-deal-layout"){
						eastregion = context.down('#deal-east-panel');
					} else if(context.xtype == "tab-sales-layout"){
						eastregion = context.down('#sales-east-panel');
					}				
					eastregion.removeAll();
					//eastregion.setCollapsed(false);
					eastregion.setVisible(false);
				}
			} else {
				var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
				var context = activemenutab.getActiveTab();
				var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
				var eastregion = detailpanel.down('[region=east]');
				eastregion.getLayout().setActiveItem(this.getController('DealDetailController').getDefaultworkareapanel());
			}
		}
		return false;
    },

    showAddContact: function() {
        var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
        var cntgrid = context.down('tab-contact-grid');
		cntgrid.getSelectionModel().deselectAll();
		
		var contactdetailsform = context.down('contactdetailsform');
		me.getController('ContactController').setContactDetailsFormToAdd(contactdetailsform);		
    },
	
	setContactDetailsFormToAdd: function(contactdetailsform){
		var me = this;
        contactdetailsform.getForm().reset();
		
        contactdetailsform.down('#contactdetailformaddbtn').show();
        contactdetailsform.down('#contactdetailformresetbtn').show();
		
		contactdetailsform.down('#contactdetailformaddbtn').setDisabled(true);
		contactdetailsform.down('#contactdetailformresetbtn').setDisabled(true);

		contactdetailsform.down('tool[type="close"]').show();
		
		var contactphonesgridpanel = contactdetailsform.down('contactphonesgridpanel');
        contactphonesgridpanel.hide();
        contactphonesgridpanel.getStore().setAutoSync(false);
        contactphonesgridpanel.getStore().removeAll();
        contactphonesgridpanel.getStore().setAutoSync(true);
		
		var contactemailsgridpanel = contactdetailsform.down('contactemailsgridpanel');
        contactemailsgridpanel.hide();
        contactemailsgridpanel.getStore().setAutoSync(false);
        contactemailsgridpanel.getStore().removeAll();
        contactemailsgridpanel.getStore().setAutoSync(true);
		
		var contactmiscgridpanel = contactdetailsform.down('contactmiscgridpanel');
        contactmiscgridpanel.hide();
        contactmiscgridpanel.getStore().setAutoSync(false);
        contactmiscgridpanel.getStore().removeAll();
        contactmiscgridpanel.getStore().setAutoSync(true);
		
		var contactaddressgridpanel = contactdetailsform.down('contactaddressgridpanel');
        contactaddressgridpanel.hide();
        contactaddressgridpanel.getStore().setAutoSync(false);
        contactaddressgridpanel.getStore().removeAll();
        contactaddressgridpanel.getStore().setAutoSync(true);
		
		var contactfaxgridpanel = contactdetailsform.down('contactfaxgridpanel');
		contactfaxgridpanel.hide();
        contactfaxgridpanel.getStore().setAutoSync(false);
        contactfaxgridpanel.getStore().removeAll();
        contactfaxgridpanel.getStore().setAutoSync(true);
	},

    contactDetailFormAddBtnClick: function(btn) {
        
        var me = this;		
		var contactFormRef = btn.up('contactdetailsform');
		//var contactFormRef = me.getContactdetailsform();
        var contactForm = contactFormRef.getForm();
        var idContact = contactForm.findField('idContact').getValue();
        console.log("idContact"+ idContact);
		
        var contactType = contactForm.findField('contactType').getValue();
        var firstName = contactForm.findField('firstName').getValue();
        var lastName = contactForm.findField('lastName').getValue();
        var companyName = contactForm.findField('companyName').getValue();
		var idCompany = contactForm.findField('idCompany').getValue();
        var position = contactForm.findField('position').getValue();
        var title = contactForm.findField('title').getValue();
		var showCompany = contactForm.findField('showCompany').getValue();
        
        contactType = (contactType) ? contactType.toUpperCase().trim() : "";
        firstName = (firstName) ? firstName.toUpperCase().trim() : "";
        lastName = (lastName) ? lastName.toUpperCase().trim() : "";
        position = (position) ? position.toUpperCase().trim() : "";
        companyName = (companyName) ? companyName.toUpperCase().trim() : "";
        title = (title) ? title.toUpperCase().trim() : "";

		var isMyContactval = false;
		isMyContactval = contactForm.findField('isMyContact').getValue();
		console.log("isMyContactval : "+isMyContactval);
		var userxContList = [];
        var data = {
            'contactType': contactType,
            'firstName': firstName,
            'lastName': lastName,
            //'companyName': companyName,
			'idCompany': idCompany,
            'position': position,
            'title': title,
			'showCompany' : showCompany,
			'UserxContList':userxContList
        };
        
        if(idContact===0 || idContact===null){
            console.log("Add New Contact");
            var methodname = "POST";
			if(isMyContactval==true){
				data['UserxContList'].push({
					//"contactType":contactType,
					"userName": Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl)
				});
			}
        } else {
            console.log("Save Contact");
            data['idContact'] = contactForm.findField('idContact').getValue();
			var isMyContactOriginal = contactForm.findField('isMyContactOriginal').getValue();
			if(isMyContactval!=isMyContactOriginal){
				if(isMyContactval==true){
					//Insert Rec
					data['UserxContList'].push({
						//"contactType":contactType,
						"userName": Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl),
						"@metadata": {
                          "entity": "UserxCont",
                          "action": "INSERT"
                        },
                        "idContact": contactForm.findField('idContact').getValue()
					});
				} else if(isMyContactval==false){
					// Delete Rec
					data['UserxContList'].push({
						"@metadata": {
                            "href": DM2.view.AppConstants.apiurl+"mssql:UserxCont/"+contactForm.findField('idUserxCont').getValue(),
                            "checksum": "override",
                            "action": "DELETE"
                        }
					});
				}
				//data['UserxContList'][0]['idContact'] = contactForm.findField('idContact').getValue();
			}
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";
        }

        contactFormRef.setLoading(true);

        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'Rcontact',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                contactFormRef.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
                    var filterstarr = [],context;
                    //me.loadAllContacts(filterstarr);
					var dashboardcontainer = me.getController('MainController').getMainviewport().down('dashboardcontainer');
					var contactsbtn = dashboardcontainer.down('#contactsbtn');
					var btnText = contactsbtn.menuaction;
					switch(btnText) {
						case "allContacts":
							var cntgridst = Ext.getStore('Contactallbuff');
							cntgridst.getProxy().setUrl(DM2.view.AppConstants.apiurl+'v_contacts');
							me.getController('ContactController').loadAllContacts(filterstarr);
						break;						
						case "myContacts":
							me.getController('DashboardController').showMyContacts();
						break;						
						case "recentContacts":
							me.getController('DashboardController').showRecentContacts();
						break;
					}
					if(btn.up('tab-contact-layout')){
					} else {
						var tmpcontactid,tmpcontactname="";
						for(var k=0;k<obj.txsummary.length;k++){
							//console.log(obj.txsummary[k]['@metadata'].resource+" "+k);
							if(obj.txsummary[k]['@metadata'].resource=="Rcontact"){								
								tmpcontactid = obj.txsummary[k].idContact;
								tmpcontactname = obj.txsummary[k].firstName+" "+obj.txsummary[k].lastName;	
								tmpcontactType = obj.txsummary[k].contactType;								
							}
						}
						context = me.getController('DealDetailController').getTabContext(btn);						
						var dealdetailcontactfieldset = context.down('dealdetailcontactfieldset');
						if(contactFormRef.config.backview == "adddealxcontactview" && contactFormRef.config.originbackview=="dealdetailcontacts"){							
							me.getController('DealDetailController').saveContactToDeal(tmpcontactid,contactFormRef,context);
							contactFormRef.getForm().reset();
						} else if(contactFormRef.config.backview == "adddealxcontactview" && contactFormRef.config.originbackview=="dealcontactgrid"){							
							me.getController('DealDetailController').saveContactToDeal(tmpcontactid,contactFormRef,context);
							contactFormRef.getForm().reset();
						} else if(contactFormRef.config.backview == "dealcontactgrid"){
							var dealcontactsgridpanel = context.down('dealcontactsgridpanel');
							me.getController('DealDetailController').loadDealContactDetails(dealcontactsgridpanel);
							contactFormRef.down('panel[action="toppanel"]').close();
						} else if(contactFormRef.config.backview == "adddealxcontactview" && contactFormRef.config.originbackview=="addsalebid"){
							///On Save Contact, Set Contact Id & Name to Add Sale Bid View & Close Contact Detail Form
							var addsalebid = contactFormRef.config.originbackfrm;
							addsalebid.getForm().findField('fullName').setRawValue(tmpcontactname);
							addsalebid.getForm().findField('idBuyerName').setRawValue(tmpcontactid);
							addsalebid.getForm().findField('contactType').setRawValue(tmpcontactType);
							me.getController("DealDetailController").addDealContactCloseBtnClick(contactFormRef.config.backfrm);
							contactFormRef.getForm().reset();
						} else {
							me.getController('DealDetailController').loadDealContactDetails(dealdetailcontactfieldset);
		                    me.contactDetailFormCloseBtnClick(contactFormRef.down('panel[action="toppanel"]'));
							contactFormRef.getForm().reset();
						}
					}					
                } else {
                    console.log("Save Contact Failure.");
                    Ext.Msg.alert('Save Contact failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                contactFormRef.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Contact', obj.errorMessage);
            }
        });
    },

    removeContact: function() {
        console.log("Remove Contact");
        var me = this;
        var cntgrid = me.getContactgrid();
        var row = cntgrid.getSelectionModel().getSelection()[0];
        console.log(row.get('idContact'));
        var idContact = row.get('idContact');

        Ext.Msg.show({
            title:'Delete Contact?',
            message: 'Are you sure you want to delete the Contact?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    cntgrid.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Contact/'+idContact+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            cntgrid.setLoading(false);
                            console.log("Removed Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                var filterstarr = [];
                                me.loadAllContacts(filterstarr);
                            } else {
                                console.log("Delete Contact Failure.");
                                Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            cntgrid.setLoading(false);
                            console.log("Delete Contact Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    onDealContactsGridPanelRightClick: function(me, record, tr, rowIndex, e, eOpts) {
        var me = this;
  //      debugger;
        e.stopEvent();
        console.log("Show Menu for Contact Add/Dealte");
        if(this.getContactmenutab()){
            this.getContactmenutab().showAt(e.getXY());
        }
        me.loadPhoneRecords(record.get('contactid'));
    },
	
	onDealContactsGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showDealContactGridMenu(view,record,e);
	},
	
	showDealContactGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
        console.log("onDealContactsTabGridPanelRightClick : Show Menu for Contact Add/Delete");
        if(this.getContactmenutab()){
			var menu = this.getContactmenutab();
            menu.showAt(e.getXY());
			var contactid = record.get('contactid');
			
			var menuitems = menu.items.items;
			var readable = record.get('readable');
			console.log("readable : "+readable);
			if(readable===0){						
				for(var i=0;i<menuitems.length;i++){
					if(menuitems[i].readMenu === "yes"){
						menuitems[i].disable();
					}
				}
			} else {
				for(var i=0;i<menuitems.length;i++){
					if(menuitems[i].readMenu === "yes"){
						menuitems[i].enable();
					}
				}
			}
			var tabItem;
			if(view.ownerCt.up('tab-deal-detail')){
				tabItem = view.ownerCt.up('tab-deal-detail');
			} else if(view.ownerCt.up('tab-deal-layout')){
				tabItem = view.ownerCt.up('tab-deal-layout');
			} else if(view.ownerCt.up('tab-sales-layout')){
				tabItem = view.ownerCt.up('tab-sales-layout');
			} else if(view.ownerCt.up('tab-sales-detail')){
				tabItem = view.ownerCt.up('tab-sales-detail');
			}
			if(tabItem){
			    me.getController('DealDetailController').disableModifyMenu(tabItem,menu);
			}
			menu.spawnFrom = view;
			me.getController('ContactController').buildContactSubMenu(contactid,this.getContactmenutab());
        }
	},
	
	onDealContactsTabGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
        var me = this;
        me.showDealContactGridMenu(view,record,e);
    },

    loadContactDocsDrop: function(cmp, e, file) {
        console.log("Done Reading");
        var me = this;

		var activemenutab = cmp.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		if(context.xtype == "tab-contact-layout"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDocs',context);
		}else if(context.xtype == "tab-contact-detail"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
		}		
		me.getController('GeneralDocSelController').onGeneralAddDealDocPanelDrop(cmp, e, file);
		/*
        var eastregion = me.getContactmenucontainer().down('#contacteastpanel');
        eastregion.getLayout().setActiveItem(me.getAddcontactdocs());

        var filesize = (file.size/1024).toFixed(2);

        ///// Add the file item to Store
        var docrec = Ext.create('DM2.model.Document',{
            'name' : file.name,
            'size' : filesize,
            'clsfctn' :'',
            'desc' :'',
            'fileobj':file
        });
        var docstmpst = Ext.getStore('DocsTemp');
        docstmpst.add(docrec);
        /////

        me.getAddcontactdocs().down('grid').getSelectionModel().select(0);
		*/
    },

    contactDocOptionItemClick: function(item, e, eOpts){
		console.log("Doc Menu Item Clicked.");
        var me = this;
        if(item.getItemId()==="cntdocedit"){
            console.log("Doc Edit Menu Item Clicked");
			me.getController('DocController').showGeneralEditDocPanel(item);
        } else if(item.getItemId()==="dochistory"){
            console.log("Doc History Menu Item Clicked");
        } else if(item.getItemId()==="cntdocadd"){
			me.showRecvDealDocs(item);
        } else if(item.getItemId()==="cntdocdelete"){
			me.getController('DocController').deleteDocument(item);
        } else if(item.getItemId()==="emaildocument" || item.getItemId()==="faxdocument"){
			me.getController('DocController').showEmailDocs(item);
        } else if(item.getItemId()==="printdocument"){
			me.getController('DocController').printDocs(item);
        } else if(item.getItemId()==="downloaddocument"){
			me.getController('DocController').downloadDocs(item);
        }
	},

    onCntDocsGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
        var me = this;
		me.showCntDocsGridMenu(view,record,e);
    },
	
	showCntDocsGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
        console.log("Show Menu for Contact Doc Add/Dealte");
        if(this.getContactdocoptionmenu()){
			this.getContactdocoptionmenu().spawnFrom = view;
            this.getContactdocoptionmenu().showAt(e.getXY());
        }
	},
	
	onCntDocsGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showCntDocsGridMenu(view,record,e);
	},
	
	addContactNoteBtnClick: function(btn){
		var me = this;
		me.showAddNoteView(btn,"add");
	},
	
	showAddNoteView: function(cmp,mode){
		console.log("Show Add Note View");
		var me = this,
            title = "", tabdealpanel, context, spawnFrom, grid, activeTab, baseCardPanel, borderPanel, noteContainerPanel, notepanel, row;
			
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			tabcntpanel = grid.up('tab-contacts');
			context = tabcntpanel.getActiveTab();
		} else {
		    // when cmp is a button.
			tabcntpanel = cmp.up('tab-contacts');
			context = tabcntpanel.getActiveTab();	    
		}
		//console.log(tabdealdetail.xtype);
		var eastregion  = context.down('#contact-east-panel');
		
		switch(mode) {
            case "add":
                title = "Add Note";    
            break;
            
            case "edit":
                title = "Edit Note";
				if(!grid.getSelectionModel().hasSelection()) {
					Ext.Msg.alert('Failed', "Please select note to edit.");
					return;
				} 
            break;
        }
		
		var generaladddealdocpanel;
		if(context.down('notepanel')){
			console.log("Use Old Panel");
			notepanel = context.down('notepanel');
		}
		else {
			console.log("Create New east add deal doc Panel");
			notepanel = Ext.create('DM2.view.NotePanel');//me.getGeneraladddealdocpanel();
			eastregion.add(notepanel);
		}
		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(notepanel);			
		eastregion.setTitle("Add Note");
		eastregion.getHeader().hide();			
		eastregion.config.backview = "addcontactnotes";
		notepanel.config.backview = "addcontactnotes";
		
		notepanel.setTitle(title);
		notepanel.getForm().findField('content').focus();
		switch(mode) {
            case "add":
                notepanel.getForm().reset();    
            break;
            
            case "edit":
                var row = grid.getSelectionModel().getSelection()[0];
                notepanel.getForm().loadRecord(row);
				notepanel.getForm().findField('noteid').setValue(row.get('idNote'));
            break;
        }
	},
	
	onCntNotesGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {        
        var me = this;
		me.showCntNotesGridMenu(view,record,e);
    },
	
	showCntNotesGridMenu: function(view, record, e){
		var me = this;
		e.stopEvent();
		console.log("Show Menu for Contact Notes Add/Delete");
        if(this.getContactnoteoptionmenu()){
			this.getContactnoteoptionmenu().spawnFrom = view;
            this.getContactnoteoptionmenu().showAt(e.getXY());
        }
	},
	
	onCntNotesGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showCntNotesGridMenu(view,record,e);
	},
	
	contactNoteOptionItemClick: function(item, e, eOpts){
		console.log("Note Menu Item Clicked.");
        var me = this;
        if(item.getItemId()==="noteedit"){
            console.log("Note Edit Menu Item Clicked");
			me.showAddNoteView(item,"edit");
        }else if(item.getItemId()==="notehistory"){
            console.log("Note History Menu Item Clicked");
        }else if(item.getItemId()==="noteadd"){
			me.showAddNoteView(item,"add");
        }
	},
	
	buildContactSubMenu: function(contactid,menu){
		var me = this;
		var filterstarr = [];
		var filterst = "idContact = '"+contactid+"'";
        filterstarr.push(filterst);
		
		var cntemailmenu = menu.down('#emailcontact').down('menu');
		cntemailmenu.removeAll();
		
		var cntphonemenu = menu.down('#callcontact').down('menu');
		cntphonemenu.removeAll();
		
		var cntfaxmenu = menu.down('#faxcontact').down('menu');
		cntfaxmenu.removeAll();
		
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'RcontactDetails',
			params: {
				filter:filterstarr
			},
			method:'GET',
			success: function(response, opts) {
				var obj = Ext.decode(response.responseText);
				if(response.status == 200) {
					if(obj.length>0){
						var cntObj = obj[0];
						if(cntObj.email.length>0){
							//Build Email Menu						
							me.buildEmailMenu(cntObj.email,cntemailmenu);
						}
						if(cntObj.phone.length>0){
							//Build Phone Menu
							me.buildPhoneMenu(cntObj.phone,cntphonemenu);
						}
						if(cntObj.fax){
							if(cntObj.fax.length>0){
								//Build Fax Menu
								me.buildFaxMenu(cntObj.fax,cntfaxmenu);
							}
						}
					}
				}
			},
			failure: function(response, opts) {
				console.log("Login Failure.");
				Ext.Msg.alert('Failed', "Please try again.");
			}
		});
	},
	
	buildFaxMenu: function(records,menu){
		var me = this;
		for(var j=0;j<records.length;j++){
			var item = records[j];			
			var faxNo = item.faxNumber;					
			var menuitem = Ext.create('Ext.menu.Item', {
				fax_no    : faxNo,
				tooltip : item.tag,
				href    : 'mailto:'+faxNo+'@fax.local',
				hrefTarget : '_self',
				text    : item.tag+' ( '+faxNo+' )'
			});
			menu.add(menuitem);
		}
	},
	
	buildEmailMenu: function(records,menu){
		var me = this;
		for(var j=0;j<records.length;j++){
			var item = records[j];
			var menuitem = Ext.create('Ext.menu.Item', {
				email    : item.email,
				tooltip : item.tag,
				href    : 'mailto:'+item.email,
				text    : item.tag+' ( '+item.email+' )'
			});		
			menu.add(menuitem);
		}
	},
	
	buildPhoneMenu: function(records,menu){
		var me = this;
		for(var j=0;j<records.length;j++){
			var item = records[j];
			
			var phNo = item.phoneNumber;
			var firstNo = phNo.charAt(0);
			if(firstNo!="1"){
				phNo = "1"+phNo;
			}
			var menuitem = Ext.create('Ext.menu.Item', {
				phone_no    : phNo,
				tooltip : item.tag,
				href    : 'ciscotel:+'+phNo,
				hrefTarget : '_self',
				text    : item.tag+' ( '+phNo+' )'
			});		
			menu.add(menuitem);
		}
	},
	
	onCntDocGridItemSelect: function(selModel, record, index, eOpts){
		var me = this;
		console.log("Contact Docs Grid Item Select event fired & delayed");
		
		var docgrid;
		var tabItem = selModel.view.ownerCt.up('tab-contact-detail');
		if(tabItem){
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		}
		var tabcontactayout = selModel.view.ownerCt.up('tab-contact-layout');
		if(tabcontactayout){
			docgrid = tabcontactayout.down('contactdocsgridpanel');
		}		
		docgrid.config.allowSelection = true;
		docgrid.config.openWindow = true;
		setTimeout(function() {
			console.log("Contact Docs Grid Item Select");
			console.log(docgrid.config.allowSelection);
			if(docgrid.config.allowSelection){
				console.log(docgrid.config.openWindow);
				if(docgrid.config.openWindow){
					var docurl = record.get('url');
					var extension = record.get('ext');
					var docsupportformat = "no";
					if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xlsx" || extension==="pdf"){
						docsupportformat = "yes";
					}
					me.getController('DocController').openWopiDocWindow(docurl,docsupportformat);
				} else {
					docgrid.config.openWindow = true;
				}
			} else {
				docgrid.config.allowSelection = true;
			}
		}, 300);
	},
	
	onCntDocGridItemMouseDown: function(view, record, item, index, e, eOpts){
		console.log("Contact Doc Grid Item Mouse Down");
		var me = this;
		
		var docgrid;
		var tabItem = view.up('tab-contact-detail');
		//var tabItem = tabdealpanel.getActiveTab();
		if(tabItem){
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		}
		var tabcontactlayout = view.up('tab-contact-layout');
		if(tabcontactlayout){
			docgrid = tabcontactlayout.down('contactdocsgridpanel');
		}
		if (e.button==0) {
			var doclink = e.getTarget('.doclink');
			if(doclink) {
				docgrid.config.openWindow = false;
			} else {
				docgrid.config.openWindow = true;
			}
			docgrid.config.allowSelection=true;
		} else { 
			docgrid.config.allowSelection=false;
		}
	},
	
	onCntDocGridItemClick: function(grid, record, item, index, e, eOpts ){
		console.log("Contact Docs Grid Item Click");
		var me = this;
		console.log(e.button);
		var docgrid;
		var tabItem = grid.up('tab-contact-detail');
		if(tabItem){
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		}
		var tabcontactlayout = grid.up('tab-contact-layout');
		if(tabcontactlayout){
			docgrid = tabcontactlayout.down('contactdocsgridpanel');
		}
		if(e.button == 2) {      
			/*console.log("cancel rightclick");*/
			  docgrid.config.allowSelection=false;
		}else{
			var doclink = e.getTarget('.doclink');
			if(doclink) {
				docgrid.config.openWindow = false;
			} else {
				docgrid.config.openWindow = true;
				var docurl = record.get('url');
				var extension = record.get('ext');
				//if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xls" || extension==="xlsx" || extension==="pdf"){
				var docsupportformat = "no";
				if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xlsx" || extension==="pdf"){
					docsupportformat = "yes";
				}
				me.getController('DocController').openWopiDocWindow(docurl,docsupportformat);
			}
		}
	},
	
	showAddToMyContact: function(cmp){
		var me = this;
		console.log("Show Add To My Contact View");
		var me = this,
            title = "", tabdealpanel, context, spawnFrom, grid, activeTab, baseCardPanel, borderPanel, noteContainerPanel, notepanel, row;
			
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			tabcntpanel = grid.up('tab-contacts');
			context = tabcntpanel.getActiveTab();
		} else {
		    // when cmp is a button.
			tabcntpanel = cmp.up('tab-contacts');
			context = tabcntpanel.getActiveTab();	    
		}
		//console.log(tabdealdetail.xtype);
		var eastregion  = context.down('#contact-east-panel');
				
		var generaladddealdocpanel;
		if(context.down('mycontactselectiongrid')){
			console.log("Use Old Panel");
			mycontactpanel = context.down('mycontactselectiongrid');
		} else {
			console.log("Create New east add to my contact Panel");
			mycontactpanel = Ext.create('DM2.view.MyContactSelectionGrid');//me.getGeneraladddealdocpanel();
			eastregion.add(mycontactpanel);
		}
		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(mycontactpanel);			
		eastregion.setTitle("Add To My Contact");
		eastregion.getHeader().hide();			
		eastregion.config.backview = "mycontactselectiongrid";
		mycontactpanel.config.backview = "mycontactselectiongrid";
		
		mycontactpanel.setTitle("Add To My Contact");
		
		console.log("Load All Contacts in add to my contact grid.");
		var contactallbuff_st = mycontactpanel.getStore();
        var contactsproxy = contactallbuff_st.getProxy();
		var contactsproxyCfg = contactsproxy.config;
        contactsproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		contactallbuff_st.setProxy(contactsproxyCfg);
		var filterstarr = [];
        contactallbuff_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                console.log(success);
                if(success) {
                }
            }
        });
	},
	
	myContactSelectionGridClose: function(panel) {
		var me = this;
		console.log("Add To My Contact Close");
		if(panel.config.backview && panel.config.backview == "mycontactselectiongrid"){
			var tabcntpanel = panel.up('tab-contacts');
			var context = tabcntpanel.getActiveTab();			
			var eastregion = context.down('#contact-east-panel');
			me.getController('ContactController').closeRecvDealDetailDocs(context);
		}
    },
	
	saveToMyContacts: function(btn){
		console.log("Save To My Contacts Btn CLick");
		var me = this;
		var mycontactselectiongrid = btn.up('mycontactselectiongrid');
		var mycntselarr = mycontactselectiongrid.getSelectionModel().getSelection();
		var userxContList = [];
		for(var j=0;j<mycntselarr.length;j++){
			var item = mycntselarr[j];
			var User_x_Contact_item = {
				'idContact' : item.get('contactid'),
				'userName'  : Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl),
				'contactType' : item.get('contactType')
			};
			userxContList.push(User_x_Contact_item);
		}		
        me.addCntArrToMyContact(mycontactselectiongrid,userxContList,"mycontactselectiongrid");		
	},
	
	onMyContactSelectionChange: function(selModel, selected, eOpts){
		console.log("On My Contact Selection Change.");
		var me = this;
		var tabcntpanel = selModel.view.ownerCt.up('tab-contacts');
		var context = tabcntpanel.getActiveTab();
		var mycontactselectiongrid = context.down('mycontactselectiongrid');
		var savemycontactbtn = mycontactselectiongrid.down('button[action="savemycontactbtn"]');
		if(selModel.hasSelection())	{
			savemycontactbtn.enable();
		}else{
			savemycontactbtn.disable();
		}
	},
	
	addSelCntToMyContact: function(){
		var me = this;
		console.log();
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		var tabcontactgrid = context.down('tab-contact-grid');
		
		var mycntselarr = tabcontactgrid.getSelectionModel().getSelection();
		var userxContList = [];
		for(var j=0;j<mycntselarr.length;j++){
			var item = mycntselarr[j];
			var User_x_Contact_item = {
				'idContact' : item.get('contactid'),
				'userName'  : Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl),
				'contactType' : item.get('contactType')
			};
			userxContList.push(User_x_Contact_item);
		}		
        me.addCntArrToMyContact(tabcontactgrid,userxContList,"tabcontactgrid");
	},
	
	addCntArrToMyContact: function(grid,userxContList,from){
		var me = this;
        grid.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:UserxCont',
            params: Ext.util.JSON.encode(userxContList),
            scope:this,
            method:'POST',
            success: function(response, opts) {
                grid.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201) {
					var title = "Add to my contact success";
					var errText = "Contact is added to my contacts List.";
					me.getController('DealDetailController').showToast(errText,title,'success',null,'tr',true);
					if(from=="mycontactselectiongrid"){
						me.myContactSelectionGridClose(grid);
						me.loadMyContacts();						
					}
                } else {
                    console.log("Save To My Contact Failure.");
                    Ext.Msg.alert('Save To My Contact failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                grid.setLoading(false);
                var obj = Ext.decode(response.responseText);
				var errMsg = obj.errorMessage;
				var errText = "";
				var n = errMsg.indexOf("Violation of UNIQUE KEY constraint");
				if(n==-1){
					errText = obj.errorMessage;
				} else {
					errText = "Contact already exist in my contacts list. Please try to add another contact.";
				}
				var title = "Add to my contact failed";
				me.getController('DealDetailController').showToast(errText,title,'error',null,'tr',true);
            }
        });
	},
	
	loadMyContacts: function() {
		var me = this;
		var cntgridst = Ext.getStore('Contactallbuff');
		cntgridst.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		cntgridst.getProxy().setUrl(DM2.view.AppConstants.apiurl+'v_myContacts');
		cntgridst.load({
			callback: function(records, operation, success) {
				if(records.length > 0){
					//me.getController('DashboardController').getContactgrid().getSelectionModel().select(0);
				}else if(records.length==0){
				}
			}
		});
	},
	
	removeFromMyContact: function() {
        console.log("Remove From My Contact");
        var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		var tabcontactgrid = context.down('tab-contact-grid');
		
		var row = tabcontactgrid.getSelectionModel().getSelection()[0];
        //console.log(row.get('contactid'));
        var idUserxCont = row.get('idUserxCont');
        Ext.Msg.show({
            title:'Delete From My Contact?',
            message: 'Are you sure you want to delete the contact from my contact?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    tabcontactgrid.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:UserxCont/'+idUserxCont+'?checksum=override',
						scope:this,
                        success: function(response, opts) {
                            tabcontactgrid.setLoading(false);
                            console.log("Removed From My Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200) {
                        		var title = "Removed from my contact success";
								var errText = "Contact is removed from my contacts List.";
								me.getController('DealDetailController').showToast(errText,title,'success',null,'tr',true);
								me.loadMyContacts();
                            } else {
                                console.log("Delete From My Contact Failure.");
                                Ext.Msg.alert('Delete From My Contact failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            tabcontactgrid.setLoading(false);
                            console.log("Delete My Contact Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },
	//Show Company Picker
	showCompanyPicker: function(btn,callfrom){
		var me = this,companypickerpanel;
		console.log("Show Company Selction Panel : "+callfrom);
		//var context = me.getController('DealDetailController').getTabContext(btn);
		//var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
		var contactdetailform = btn.up('contactdetailsform');
		
		var eastregion = contactdetailform.up('[region=east]');
		if(eastregion.down('companypickerpanel')){
			console.log("Use Old Panel");
			companypickerpanel = eastregion.down('companypickerpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
			companypickerpanel = Ext.create('DM2.view.CompanyPickerPanel');
		}
		eastregion.getLayout().setActiveItem(companypickerpanel);
		companypickerpanel.config.backview = contactdetailform;
		me.loadCompany(companypickerpanel.down('grid'));
	},
	
	loadCompany: function(companygrid){
		var me = this;
		var companyst = companygrid.getStore();
		var companystproxy = companyst.getProxy();
		var companystproxyCfg = companystproxy.config;
        companystproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		companyst.setProxy(companystproxyCfg);
        companyst.load({
            callback: function(records, operation, success){
				if(records.length > 0){
					companygrid.getSelectionModel().select(0);
				}
			}
        });
	},
	
	onCompanyPickerGridItemSelect: function(grid, record, item, index, e, eOpts) {
		console.log("Select Company Grid Item Select");
		var me = this;
		var companypickerpanel = grid.up('companypickerpanel');
		companypickerpanel.getForm().loadRecord(record);
	},
	
	onCompanyPickerPanelClose: function(panel){
		var me = this;
		console.log("Company Picker Panel Close");
		var contactdetailform = panel.config.backview;
		
		var eastregion = panel.up('[region=east]');
		/*if(eastregion.down('contactdetailsform')){
			console.log("Use Old Panel");
			contactdetailform = eastregion.down('contactdetailsform');
		} else {
			//companypickerpanel = Ext.create('DM2.view.CompanyPickerPanel');
		}*/
		eastregion.getLayout().setActiveItem(contactdetailform);
	},
	
	selectCompanyBtnClick: function(btn) {
		console.log("Save Company To Company Name.");
		var me = this;
		var companypickerpanel = btn.up('companypickerpanel');
		var cmpgrid = companypickerpanel.down('grid');
        if (cmpgrid.getSelectionModel().hasSelection()) {
            var row = cmpgrid.getSelectionModel().getSelection()[0];
            var idCompany = row.get('idCompany');
            var companyName = row.get('companyName');
			var contactdetailform = companypickerpanel.config.backview;			
			contactdetailform.getForm().findField('companyName').setValue(companyName);
			contactdetailform.getForm().findField('idCompany').setValue(idCompany);
			
			me.onCompanyPickerPanelClose(companypickerpanel);
        } else {
            Ext.Msg.alert('Failed', "Please select company.");
            return false;
        }
	},
	
	addCompanyBtnClick: function(btn){
		var me = this,companypickerpanel;
		console.log("Show Company Add/Edit Panel : Add");
		var companypickerpanel = btn.up('companypickerpanel');
		me.showAddEditCompanyView("add",companypickerpanel,'');
	},
	
	showAddEditCompanyView: function(mode,companypickerpanel,rec){
		var me = this,addeditcompanyview;
		console.log("Show Company Add/Edit Panel : "+mode);		
		var eastregion = companypickerpanel.up('[region=east]');
		if(eastregion.down('addeditcompanyview')){
			console.log("Use Old Panel");
			addeditcompanyview = eastregion.down('addeditcompanyview');
		} else {
			console.log("Create New Panel");
			addeditcompanyview = Ext.create('DM2.view.AddEditCompanyView');
		}
		eastregion.getLayout().setActiveItem(addeditcompanyview);
		addeditcompanyview.config.backview = companypickerpanel;
		addeditcompanyview.config.originalbackview = companypickerpanel.config.backview;
		if(mode=="edit"){
			addeditcompanyview.getForm().loadRecord(rec);
		} else {
			addeditcompanyview.getForm().reset();
		}
	},
	
	saveCompanyBtnClick: function(btn){
		var me = this,salerec,idSale,methodname;
		console.log("Save Company Button Click");       
       
        var addeditcompanyview = btn.up('addeditcompanyview');
		var addeditcompanyviewForm = addeditcompanyview.getForm();
		var idCompany = addeditcompanyviewForm.findField('idCompany').getValue();
		var data = {
            'companyName' : addeditcompanyviewForm.findField('companyName').getValue(),
            'state'       : addeditcompanyviewForm.findField('state').getValue(),
			'phone1'  : addeditcompanyviewForm.findField('phone1').getValue(),
			'phone2'  : addeditcompanyviewForm.findField('phone2').getValue(),
            'address' : addeditcompanyviewForm.findField('address').getValue(),
			'city'    : addeditcompanyviewForm.findField('city').getValue(),
			'email1'  : addeditcompanyviewForm.findField('email1').getValue(),
			'email2'  : addeditcompanyviewForm.findField('email2').getValue()
		};
		
		if(idCompany===0 || idCompany===null || idCompany==="") {
            console.log("Add New Company");
            methodname = "POST";			
        } else {
            console.log("Save Edit Company");
            data['idCompany'] = idCompany;
            data['@metadata'] = {'checksum' : 'override'};
            methodname = "PUT";			
        }

        addeditcompanyview.setLoading(true);
        Ext.Ajax.request({
            method: methodname,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Company',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Save Company is submitted.");
                addeditcompanyview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					addeditcompanyview.getForm().reset();
					var companypickerpanel = addeditcompanyview.config.backview;
					me.loadCompany(companypickerpanel.down('grid'));
					
					//me.onAddEditCompanyViewClose(addeditcompanyview);
					var idCompany = obj.txsummary[0].idCompany;
					var companyName = obj.txsummary[0].companyName;
					var contactdetailform = addeditcompanyview.config.originalbackview;			
					contactdetailform.getForm().findField('companyName').setRawValue(companyName);
					contactdetailform.getForm().findField('idCompany').setRawValue(idCompany);
		
					var eastregion = addeditcompanyview.up('[region=east]');
					eastregion.getLayout().setActiveItem(contactdetailform);
                } else {
                    console.log("Save Company Failure.");
                    Ext.Msg.alert('Save Company failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Save Company Failure.");
                addeditcompanyview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Company failed', obj.errorMessage);
            }
        });
	},
	
	onAddEditCompanyViewClose: function(panel){
		var me = this;
		console.log("on AddEditCompanyView Close");
		var companypickerpanel = panel.config.backview;
		
		var eastregion = panel.up('[region=east]');
		/*if(eastregion.down('companypickerpanel')){
			console.log("Use Old Panel");
			companypickerpanel = eastregion.down('companypickerpanel');
		} else {
			//companypickerpanel = Ext.create('DM2.view.CompanyPickerPanel');
		}*/
		eastregion.getLayout().setActiveItem(companypickerpanel);
	},
	
	onCntDtFormFieldsChange: function(field, newValue, oldValue, eOpts){
		var me = this;
		console.log("cnt dt fieldss are change"+field+"-"+newValue);
        var contactdetailsform = field.up('contactdetailsform');
		contactdetailsform.down('#contactdetailformaddbtn').enable();
		contactdetailsform.down('#contactdetailformresetbtn').enable();
	},
	
	contactDetailFormResetBtnClick: function(btn){
		var me = this;		
        var contactdetailsform = btn.up('contactdetailsform');
		var cntdetailrec = contactdetailsform.config.cntdetailrec;
		contactdetailsform.getForm().loadRecord(cntdetailrec);		
		contactdetailsform.down('#contactdetailformaddbtn').disable();
		contactdetailsform.down('#contactdetailformresetbtn').disable();
	},
	
	afterRenderContactGrid: function(grid) {
        var me = this;
		console.log("After Contact Grid Render");
		grid.getView().getHeaderAtIndex(0).hide();
		grid.store.on( 'load', function( store, records, options ) {
			if(store.getCount()==0){
				// Clear Deal Tabs Store Data
				grid.getSelectionModel().deselectAll();
				var context = grid.up('tab-contact-layout');
				contactdetailsform = context.down('contactdetailsform');
				if(contactdetailsform){
					contactdetailsform.getForm().reset();
					//var cntdetailrec = contactdetailsform.config.cntdetailrec;
					contactdetailsform.down('#contactdetailformaddbtn').disable();
					contactdetailsform.down('#contactdetailformresetbtn').disable();
					//contactdetailsform.down('contactphonesgridpanel').getStore().removeAll();
					//contactdetailsform.down('contactemailsgridpanel').getStore().removeAll();
					//contactdetailsform.down('contactmiscgridpanel').getStore().removeAll();
					//contactdetailsform.down('contactaddressgridpanel').getStore().removeAll();
					//contactdetailsform.down('contactfaxgridpanel').getStore().removeAll();
				}
				me.clearContactDocs(context);
				me.clearContactNotes(context);
				me.clearContactDealHistory(context);
			} else {
				// Select first item if wants
			}
		});
	},
	
	clearContactDealHistory: function(tabItem){
		console.log("Clear Contact Deal History");
		var me = this;
		if(tabItem.down('#contactdealhistorygrid')){
			var panel = tabItem.down('#contactdealhistorygrid');
			var dealhistst = panel.getStore();
			dealhistst.removeAll();
		}
	},
	
	showUploadContactView: function(cmp){
		console.log("Show Upload Contact View");
		var me = this,tabdealpanel, context, spawnFrom, grid, uploadcontactview;
			
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			tabcntpanel = grid.up('tab-contacts');
			context = tabcntpanel.getActiveTab();
		} else {
		    // when cmp is a button.
			tabcntpanel = cmp.up('tab-contacts');
			context = tabcntpanel.getActiveTab();	    
		}
		var eastregion  = context.down('#contact-east-panel');
	
		if(context.down('uploadcontactview')){
			console.log("Use Old Panel");
			uploadcontactview = context.down('uploadcontactview');
		} else {
			console.log("Create New east add deal doc Panel");
			uploadcontactview = Ext.create('DM2.view.UploadContactView');
			eastregion.add(uploadcontactview);
		}
		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(uploadcontactview);			
		eastregion.setTitle("Upload Contacts");
		eastregion.getHeader().hide();			
		eastregion.config.backview = "uploadcontactview";
		uploadcontactview.config.backview = "uploadcontactview";
		//uploadcontactview.getForm().findField('content').focus();
	},
	
	addContactFileFieldChange: function(field, value, eOpts) {
        console.log("File Selected through Add Doc Button");
        var me = this;
		//var context = field.up('tab-deal-detail');
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		//var activemenutab = field.up('tab-panel').getLayout().getActiveItem();
		//var context = activemenutab.getActiveTab();
		
        if(field.fileInputEl.dom.files.length > 0){
            //var generaladddealdocpanel = context.down('generaladddealdocpanel');//me.getGeneraladddealdocpanel();

            var file = field.fileInputEl.dom.files[0];

            var filesize = (file.size/1024).toFixed(2);

            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : file.name,
                'size' : filesize,
                'fileobj':file
            });
        }
    },
	
	resetUploadContact: function(btn){
		var me = this;
		var uploadcontactview = btn.up('uploadcontactview');
		uploadcontactview.getForm().reset();
	},
	
	saveUploadContact: function(btn){
		console.log("Doc Submit Button Clicked.");
        var me = this,associateid,parmname,generaladddealdocpanel,grpcmbval,memebercmbval;
		var context = btn.up('tab-contact-layout');	
		var uploadcontactview = btn.up('uploadcontactview');
		var field = uploadcontactview.down('filefield[name="contactfile"]');
		var grpcmb = uploadcontactview.down('tagfield[name="grpcmb"]').getValue();
		var memebercmb = uploadcontactview.down('tagfield[name="memebercmb"]').getValue();
		if(grpcmb.length>0){
			grpcmbval = grpcmb.join(",");
		}
		if(memebercmb.length>0){
			memebercmbval = memebercmb.join(",");
		}
		///
        if(field.fileInputEl.dom.files.length > 0){
            //var generaladddealdocpanel = context.down('generaladddealdocpanel');//me.getGeneraladddealdocpanel();

            var file = field.fileInputEl.dom.files[0];

            var filesize = (file.size/1024).toFixed(2);

            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : file.name,
                'size' : filesize,
                'fileobj':file
            });
            /////
			///
			var fileObj = file;
			var filename = file.name;
	
			var ext = /^.+\.([^.]+)$/.exec(filename);
			if(ext === null){
				ext = "";
			} else {
				ext = ext[1];
			}
			
			var uploadmode =  "fileupload";
		
		//var uploadmode =  record.get('uploadmode');
        /*if(uploadmode==="outlook"){

                var data = [{
                    'name'           : filename,
                    'description'    : record.get('desc'),
                    'classification' : tempclsf,
                    'size'           : record.get('filesize'),
                    'ext'            : ext,
                    'content'        : fileObj//,
                    //'idDeal': associateid
					//parmname : associateid
                }];
				if(parmname=="idProperty"){
					data[0].idProperty = associateid;
				}else if(parmname=="idContact"){
					data[0].idContact = associateid;
				}else{
					data[0].idDeal = associateid;
				}
				me.uploadDealDoc(generaladddealdocpanel,data,context,associateid,record);
            } else {*/
                var reader = new FileReader();

                reader.readAsDataURL(fileObj);
				//reader.readAsText(fileObj);

                reader.onload = function (e) {
					//console.log("Base64 Data :: ");
					//console.log(e.target.result);
					
					////Get the data from Csv File & Convert & Return Jason Array
					//var texto = e.target.result;
					//var jsonArray = content = me.csvJSON(texto);
										
                    var conentarr = e.target.result.split("base64,");
                    var content = '';
                    if(conentarr.length > 1){
                        content = conentarr[1];
                    }
					
					var prepend = 'b64:';
					content = prepend + content;
										
                    var data = [{
						'UserPermissionList': memebercmbval,
						'GroupPermissionList': grpcmbval,
                        'data'        : content
                        //'idDeal': associateid
                    }];
					me.uploadDealDoc(uploadcontactview,data,context);  
                };
                reader.onerror = function (e) {
                    console.log(e.target.error);
                };
            //}
		}
	},
	
	//var csv is the CSV file with headers
	csvJSON: function(csv){
	
	  var lines=csv.split("\n");
	
	  var result = [];
	
	  var headers=lines[0].split(",");
	
	  for(var i=1;i<lines.length;i++){
	
		  var obj = {};
		  var currentline=lines[i].split(",");
	
		  for(var j=0;j<headers.length;j++){
			  obj[headers[j]] = currentline[j];
		  }
	
		  result.push(obj);
	
	  }
	
	  return result; //JavaScript object
	  //return JSON.stringify(result); //JSON
	},
	
	uploadDealDoc: function(uploadcontactview,data,context){
		var me = this;
		uploadcontactview.setLoading(true);
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'ContactUpload',
			params: Ext.util.JSON.encode(data),
			scope:this,
			success: function(response, opts) {
				uploadcontactview.setLoading(false);
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 201){
					/*if(obj.txsummary[0].url){
						docurl = obj.txsummary[0].url;
					} else if(obj.txsummary[1]){
						if(obj.txsummary[1].url){
							docurl = obj.txsummary[1].url;
						}
					}*/
					me.loadAllContacts([]);
					me.uploadContactViewClose(uploadcontactview);
				} else {
					Ext.Msg.alert('Failed', "Please try again.");
				}
			},
			failure: function(response, opts) {
				uploadcontactview.setLoading(false);
				Ext.Msg.alert('Failed', "Please try again.");
			}
		});
	},
	
	uploadContactViewClose: function(panel){
		var me = this;
		var tabcntpanel = panel.up('tab-contacts');
		context = tabcntpanel.getActiveTab();			
		eastregion = context.down('#contact-east-panel');
		me.getController('ContactController').closeRecvDealDetailDocs(context);
	},
	
	openMultiPickList: function(btn){
		var me = this,tabdealpanel, context, spawnFrom, grid, uploadcontactview,title;
			
		tabcntpanel = btn.up('tab-contacts');
		context = tabcntpanel.getActiveTab();	    

		var eastregion  = context.down('#contact-east-panel');
	
		if(context.down('multipicklistgrid')){
			console.log("Use Old Panel");
			multipicklistgrid = context.down('multipicklistgrid');
		} else {
			console.log("Create New east add deal doc Panel");
			multipicklistgrid = Ext.create('DM2.view.MultiPickListGrid');
			eastregion.add(multipicklistgrid);
		}
		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(multipicklistgrid);			
		eastregion.getHeader().hide();			
		eastregion.config.backview = "multipicklistgrid";
		multipicklistgrid.config.backview = "multipicklistgrid";
		
		var st = multipicklistgrid.getStore();
        var stproxy = st.getProxy();
		var stproxyCfg = stproxy.config;

		if(btn.action=="memberspickbtn"){
			title = "Select Users";
			stproxyCfg.url = DM2.view.AppConstants.apiurl+'mssql:Members';
			stproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'mssql:Members';
			multipicklistgrid.config.listview = "memberslist";
		} else if(btn.action=="groupspickbtn"){
			title = "Select Groups";
			stproxyCfg.url = DM2.view.AppConstants.apiurl+'mssql:Groups';
			stproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'mssql:Groups';
			multipicklistgrid.config.listview = "groupslist";
		}
		eastregion.setTitle(title);
		multipicklistgrid.setTitle(title);
        stproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		st.setProxy(stproxyCfg);
        st.load({
            scope: this,
            callback: function(records, operation, success) {
				multipicklistgrid.getSelectionModel().deselectAll();
            }
        });
	},
	
	multiPickListGridClose: function(panel){
		var me = this;
		var tabcntpanel = panel.up('tab-contacts');
		context = tabcntpanel.getActiveTab();			
		eastregion = context.down('#contact-east-panel');
		eastregion.getLayout().setActiveItem(context.down('uploadcontactview'));
	},
	
	saveMultiPickList: function(btn){
		var me = this,cmb;
		var tabcntpanel = btn.up('tab-contacts');
		context = tabcntpanel.getActiveTab();			
		eastregion = context.down('#contact-east-panel');
		var uploadcontactview = context.down('uploadcontactview');
		eastregion.getLayout().setActiveItem(uploadcontactview);
		
		var multipicklistgrid = btn.up('multipicklistgrid');
		var selRecs = multipicklistgrid.getSelectionModel().getSelection();
		if(selRecs.length>0){
			if(multipicklistgrid.config.listview == "groupslist"){
				cmb = uploadcontactview.down('tagfield[name="grpcmb"]');
			} else if(multipicklistgrid.config.listview == "memberslist"){
				cmb = uploadcontactview.down('tagfield[name="memebercmb"]');
			}
			console.log(selRecs);
			cmb.getStore().loadData(selRecs);
			cmb.select(cmb.getStore().collect(cmb.valueField));
		}
	}
});