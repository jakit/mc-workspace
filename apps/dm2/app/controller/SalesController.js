Ext.define('DM2.controller.SalesController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.DealHistoryGrid',
			  'DM2.view.DealHistoryTab'],
    refs: {
		salesgrid: 'tab-sales-grid',
		tabsaleslayout : 'tab-sales-layout',
		salestabpanel : 'salestabpanel'
    },

    init: function(application) {
        this.control({
			'tabsaleslayout':{
				afterrender: this.afterTabSalesLayoutRender
			},
			'salestabpanel':{
				afterrender: this.setUpTabPanel,
				beforetabchange: this.saleDealMenuTabChange
			},
			'salesgrid': {
                select : this.onSaleDealMenuGridpanelItemSelect,
                itemdblclick: this.onDealMenuGridpanelItemDblClick,
                afterrender : this.afterRenderSalesDealMenuGrid,
                rowcontextmenu: this.onSalesMenuGridPanelRightClick/*,
                containercontextmenu: this.onDealMenuGridContainerRightClick,
                headercontextmenu: this.onDealMenuGridHeaderContainerRightClick/*,
				headerclick: this.onDealMenuGridHeaderClick*/
            },
			'tab-sales-grid #saledealmenugridview': {
				itemlongpress: this.onSalesMenuGridItemLongPress
            },
			'salestabpanel #salesdealdocstab':{
				click: this.dealDocsTabClick
			},
			'salestabpanel #salesdealdocstabmenu':{
				click: this.dealDocsTabMenuClick
			},
			'salestabpanel #salesdealusertab':{
				click: this.dealUsersTabClick
			},
			'#salesdealusertab menu':{
				click: this.dealUsersTabMenuClick
			},
			'salesgridoptionsmenu':{
                click: this.salesGridItemMenuClick
            },
			'salestabpanel #salesdealhistorytab':{
				click: this.salesdealHistoryTabClick
			},
			'#salesdealhistorytab menu':{
				click: this.salesdealHistoryTabMenuClick
			}
        });
    },

	setUpTabPanel: function(tabpanel) {
        var me = this,tabItem;
		console.log("After Sales Tab Panel Render");
		salestabpanel = tabpanel;
		tabItem = Ext.create('DM2.view.DealNotesGridPanel',{
					iconCls: 'notebtn'
				  });
		salestabpanel.add(tabItem);
		
		tabItem = Ext.create('DM2.view.DealDocsGridPanel',{
					iconCls: 'docbtn'/*,
					tabConfig: {
						hidden: true
					}*/
				  });
		salestabpanel.add(tabItem);
		/*salestabpanel.tabBar.insert(1, {
				xtype:'splitbutton',
				iconCls: 'docbtn',
				text:'Documents',
				cls:'docbtncls',
				ui:'tabsplitbutton',
				itemId:'salesdealdocstab',
				menu:{
					itemId: 'salesdealdocstabmenu',
					items:[{
						group: 'saledoctab',
						checked : true,
						text:'Deal Only'
					},{
						group: 'saledoctab',
						checked: false,
						text:'All'
					}]
			}
		});*/
		tabItem = Ext.create('DM2.view.DealContactsGridPanel',{
					iconCls: 'contactbtn'
				  });
		salestabpanel.add(tabItem);
		
		tabItem = Ext.create('DM2.view.DealDetailUserGridPanel',{
					iconCls: 'userbtn',
					title: 'Users',
					tabConfig: {
						hidden: true
					}
				  });
		salestabpanel.add(tabItem);
		///Add CommissionPercent Column for Sales Deal into User Grid
		var column = Ext.create('Ext.grid.column.Column', {
			text: 'Commision(%)',
			flex: 1,
			dataIndex: 'commissionPercent'
		});
	
		tabItem.headerCt.insert(
		  tabItem.columns.length - 1, // that's index column
		  column);
	
		tabItem.getView().refresh();
		
		//Insert Users Tab
		var dealuserstab = Ext.create('DM2.view.DealUserTab',{
                                itemId:'salesdealusertab',
								ui:'tabsplitbutton'
                            });
        salestabpanel.tabBar.insert(4, dealuserstab);
		salestabpanel.setActiveTab(0);
		
		//Insert Deal History Grid & Tab		
		tabItem = Ext.create('DM2.view.DealHistoryGrid',{
			iconCls: 'loanbtn',
			itemId : 'salesdealhistorygrid',
			title: 'Sale History',
			tabConfig: {
				hidden: true
			}
	    });
		salestabpanel.add(tabItem);
		
        var dealhistorytab = Ext.create('DM2.view.DealHistoryTab',{
			itemId:'salesdealhistorytab',
			ui:'tabsplitbutton',
			style:'margin-right:5px'
		});
		dealhistorytab.setText("Sale History");
        salestabpanel.tabBar.insert(5, dealhistorytab);  
    },
	
	afterTabSalesLayoutRender: function(panel){
		var me = this;
		//panel.down('#contact-east-panel').getHeader().hide();
	},

    loadAllSales: function(filterstarr) {
        var me = this;
		console.log("Load All Sales in buffered grid.");
		var salesallbuff_st = Ext.getStore('Salesallbuff');
        var salesproxy = salesallbuff_st.getProxy();
        /*salesproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var salesproxyCfg = salesproxy.config;
        salesproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		salesallbuff_st.setProxy(salesproxyCfg);
        salesallbuff_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                console.log(success);
                if(success) {
                    if(records.length > 0){
						if(me.getSalesgrid()){
	                        me.getSalesgrid().getSelectionModel().select(0);
						}
                    }
                }
            }
        });
    },
	
	afterRenderSalesDealMenuGrid: function(salesdealgrid) {
        var me = this;
		console.log("After Sales Deal Grid Render & Activate");
		me.getController('MainController').getGridKeyData(salesdealgrid,'sale');	
		if(salesdealgrid.firstLoad){
        	var filterstarr = [];
        	me.getController('SalesController').loadAllSales(filterstarr);
			salesdealgrid.firstLoad = false;
		}
		salesdealgrid.store.on( 'load', function( store, records, options ) {
			if(store.getCount()==0){
				// Clear Deal Tabs Store Data
				salesdealgrid.getSelectionModel().deselectAll();
				var context = salesdealgrid.up('tab-sales-layout')
				me.getController('NoteController').clearDealNotes(context);
				me.getController('ContactController').clearDealContactGrid(context);
				me.getController('DocController').clearDealDocs(context);
				me.getController('DealUserController').clearDealUsers(context);
				//me.getController('DealHistoryController').clearDealHistory(context);
			} else {
				// Select first item if wants
			}
		});
	},
	
	onSaleDealMenuGridpanelItemSelect: function(grid, record, index, eOpts) {
       
        console.log("Grid Item is selected.So Load the Contacts,Docs & Notes.");
		var me = this,g;
		var context = grid.view.ownerCt.up('tab-sales-layout');	
		context.record = record;
		//context.dealid = record.get('dealid');
		context.dealid = record.get('idDeal');
        try{
            g = grid.view.up();
        }
        catch(e) {
            g = grid.up('tab-sales-layout').down('tab-sales-grid');
        }
        finally {
			var readable = record.get('readable');
			if(readable==0){
				me.getController('DealController').dealPermissionNotAllowed();
			} else {
				me.getSalestabpanel().enable();
				var warwindow = me.getController('DealController').getWarningwindow(); //me.getDealmenutabpanel().getEl().down('warningwindow'); 
				console.log("warwindow");
				console.log(warwindow);
				if(warwindow){
					warwindow.close();
				}
				
				me.getController('DashboardController').getMsgtext().setText("");
				me.getController('DashboardController').getMsgtext().setStyle({border:'0px'});
			
				me.loadRDealDetails(context);
			}
        }
    },
	
	loadRDealDetails: function(tabItem){
		var me = this;
		console.log("Load RdealDetails Once");
		var dealDetailStore = Ext.create('DM2.store.SaleDealDetails',{
			storeId: 'SaleDealDetails-'+tabItem.dealid
		});
		tabItem.dealDetailStore = dealDetailStore;
		
		dealDetailStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		dealDetailStore.load({
			scope: this,
			params: {
				'filter' : 'idDeal = '+tabItem.dealid
			},
			callback: function(records, operation, success) {
				//tabItem.setLoading(false);
				//dealdetailform.setLoading(false);
				console.log("Load RDealDetails Callback.");
				if(success){
					me.afterRDealDetailsLoad(tabItem);
				}
			}
		});
	},
	
	afterRDealDetailsLoad: function(tabItem){
		var me = this;	
		tabItem.isRDealDetailsLoaded = 'yes';
		tabItem.dealPropertyData = tabItem.dealDetailStore.data.items[0].data.PropertiesPerDeal;
		tabItem.dealContactData = tabItem.dealDetailStore.data.items[0].data.ContactPerDeal;
		tabItem.dealDocsData = tabItem.dealDetailStore.data.items[0].data.DocsPerDeal;
		tabItem.dealUsersData = tabItem.dealDetailStore.data.items[0].data.UsersPerDeal;
		
		if(tabItem.xtype=="tab-sales-layout") {
			me.getController('NoteController').bindDealNotes(tabItem);
			me.getController('ContactController').bindDealContactGrid(tabItem);
			me.getController('DocController').bindDealDocs(tabItem);
			me.getController('DealUserController').bindDealUsers(tabItem);
		} else {
			me.getController('DealDetailController').bindDealPrimaryDetails(tabItem);
			me.getController('DealDetailController').bindPropertyDetails(tabItem);
			//me.getController('DealDetailController').bindExistingLoanDetails(tabItem);
			me.getController('DealDetailController').bindMasterLoanDetails(tabItem);
			me.getController('DealDetailController').bindContactDetails(tabItem);	
			me.getController('DealDetailController').bindActivityDetails(tabItem);
			me.getController('NoteController').bindDealNotes(tabItem);
			me.getController('SalesDealDetailController').bindDealSales(tabItem);
			me.getController('DealUserController').bindDealUsers(tabItem);
			//me.getController('SalesDealDetailController').bindNewLoanDetails(tabItem);
			me.getController('DocController').bindDealDocs(tabItem);
		}
		me.getController('CreateNewDealFromPropertyController').bindCreateNewSalesDealViewInfo(tabItem);
	},
	
	dealDocsTabClick: function(tab) {
		console.log("Set Sales Document Tab");
		var me = this;
		me.getSalestabpanel().setActiveTab(1);
		var context = tab.up('tab-sales-layout');
		var dealdocsgridpanel = context.down('dealdocsgridpanel');
		me.getController('DocController').loadDocsPanelStore(dealdocsgridpanel);
	},
	
	dealDocsTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Sales Deal Docs Menu Clicked");
        var me = this;
        var text = item.text;
		me.getSalestabpanel().setActiveTab(1);
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabpanel.getActiveTab();
		var dealdocsgridpanel = context.down('dealdocsgridpanel');
        if(text === "All"){
			console.log("Show All Docs");
			me.getController('DocController').loadAllDocsPerDealPanelStore(dealdocsgridpanel);
        } else if(text === "Deal Only"){
			console.log("Show Deal Only Docs");
			me.getController('DocController').loadDocsPanelStore(dealdocsgridpanel);
        }
	},
	
	saleDealMenuTabChange: function(tabPanel, newCard, oldCard, eOpts){
		console.log("Sales Deal Menu Tab Change");
		var me = this;

		//var dealdocstab = tabPanel.down('#salesdealdocstab');
		var dealusertab = tabPanel.down('#salesdealusertab');
		var salesdealhistorytab = tabPanel.down('#salesdealhistorytab');
		
		/*if(newCard.xtype==="dealdocsgridpanel"){
			dealdocstab.addCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
			if(salesdealhistorytab){salesdealhistorytab.removeCls("docactivebtncls");}
		} else*/ if(newCard.xtype==="dealhistorygrid"){
			if(salesdealhistorytab){salesdealhistorytab.addCls("docactivebtncls");}
			//dealdocstab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
		} else if(newCard.xtype==="dealdetailusergridpanel"){
			dealusertab.addCls("docactivebtncls");
			//dealdocstab.removeCls("docactivebtncls");
			if(salesdealhistorytab){salesdealhistorytab.removeCls("docactivebtncls");}
		} else {
			//dealdocstab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
			if(salesdealhistorytab){salesdealhistorytab.removeCls("docactivebtncls");}
		}
	},
	
	dealUsersTabClick: function(tab){
		console.log("Deal Users Tab Click");
		var me = this;
		me.getSalestabpanel().setActiveTab(3);
		var context = tab.up('tab-sales-layout');
		var usergrid = context.down('dealdetailusergridpanel');
		var dealgrid = context.down('tab-sales-grid');
	
		if (dealgrid.getSelectionModel().hasSelection()) {
			var record = dealgrid.getSelectionModel().getSelection()[0];			
			me.getController('DealUserController').loadDealUserDetailsWithParams('active',record.get('idDeal'),usergrid);
			usergrid.config.alloractiveusermode = "active";
		}		
	},
	
	dealUsersTabMenuClick: function(menu, item, e, eOpts){
		console.log("Deal Users Tab Menu Click");
		var me = this;
		var text = item.text;
		me.getSalestabpanel().setActiveTab(3);
		
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabpanel.getActiveTab();
		var usergrid = context.down('dealdetailusergridpanel');
		var dealgrid = context.down('tab-sales-grid');
		
        if(text === "All"){
			console.log("Show All Docs");
            if (dealgrid.getSelectionModel().hasSelection()) {
                var record = dealgrid.getSelectionModel().getSelection()[0];
                me.getController('DealUserController').loadDealUserDetailsWithParams('all',record.get('idDeal'),usergrid);
				usergrid.config.alloractiveusermode = "all";
            }else{
				return false;
			}			
        }else if(text === "Active Only"){
			console.log("Show Active Users Only");
            if (dealgrid.getSelectionModel().hasSelection()) {
				var record = dealgrid.getSelectionModel().getSelection()[0];
				me.getController('DealUserController').loadDealUserDetailsWithParams('active',record.get('idDeal'),usergrid);
				usergrid.config.alloractiveusermode = "active";
			}else{
				return false;
			}			
        }
	},
	
	onDealMenuGridpanelItemDblClick: function(dataview, record, item, index, e, eOpts) {
		var me = this;
		var readable = record.get('readable');
		if(readable==0){
			me.getController('DealController').dealPermissionNotAllowed();
			me.getController('DashboardController').getMsgtext().setStyle({
				fontWeight: 'bold',
				borderWidth: '1px',
				borderColor: '#ff0000',
				color : '#ff0000',
				borderStyle : 'dotted'
			});
			me.getController('DashboardController').getMsgtext().setText("No Permission is allowed for this deal.");
		} else {
			me.getSalestabpanel().enable();
			me.getController('DashboardController').getMsgtext().setText("");
			me.getController('DashboardController').getMsgtext().setStyle({border:'0px'});
	        me.showDealDetail(record, dataview);
		}
    },
    detailClass: 'DM2.view.component.tab.sales.Detail',
	showDealDetail: function(record, dataview) {
        
        var me = this;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
        var baseTabPanel = dataview.up('tab-basecardpanel');
        var tabLength = baseTabPanel.items.length;
        var tabItems = baseTabPanel.items.items;
        var tabItem;
        var createPanel = false;
        var x;
        //var name = record.data.name;
		var name = record.data.dealName;
        
        
        if(tabLength < 2) {
            createPanel = true;
        } else {
            for(x = 1; x < tabLength; x++) {
                tabItem = tabItems[x];
                if(tabItem.dealid == dealid) {
                    createPanel = false;
                    break;
                } else {
                    createPanel = true;
                }
            }
        }
        
        if(createPanel == true) {
			var cleanName = '';
			if(name){
				cleanName = name.replace(/\s\s+/g, ' ');
			}
			var title;
			if(cleanName.length < 1) {
			    title = dealid  
			} else {
			    title = cleanName.substring(0,10);
			}
			tabItem = Ext.create(me.detailClass, {
				title: title,
				html: title,
				dealid: dealid,
				record: record,
				iconCls: 'readonlybtn',
				iconAlign:'right'
			});
			baseTabPanel.add(tabItem);
			me.loadRDealDetailsNewTab(tabItem,baseTabPanel);			                
        }
		baseTabPanel.setActiveItem(tabItem);
		////////
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var tabsalesgrid = tabdealpanel.down('tab-sales-grid');
		console.log("rightMenuClick");
		console.log(tabsalesgrid.rightMenuClick);
		if(tabsalesgrid.rightMenuClick=="createnewsalefrom"){
			tabsalesgrid.rightMenuClick = null;
			//var acttabdealdetail = tabdealpanel.getActiveTab();
			//console.log("acttabdealdetail");
			//console.log(tabItem);
			me.getController('CreateNewDealFromPropertyController').CreateNewSalesDealClick("salesgrid");
		}
		////////
    },
	
	loadRDealDetailsNewTab: function(tabItem,baseTabPanel){
		var me = this;
		console.log("Load RdealDetails Once From New Tab");
		console.log(tabItem.dealid);
		//var dealdetailform = tabItem.down('dealdetailsformpanel');
        //dealdetailform.setLoading(true);
		//tabItem.setLoading(true);
		var dealDetailStore = Ext.create('DM2.store.SaleDealDetails',{
			storeId: 'SaleDealDetails-'+tabItem.dealid
		});
		tabItem.dealDetailStore = dealDetailStore;
		
		dealDetailStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		dealDetailStore.load({
			scope: this,
			params: {
				'filter' : 'idDeal = '+tabItem.dealid
			},
			callback: function(records, operation, success) {
				//tabItem.setLoading(false);
				//dealdetailform.setLoading(false);
				console.log("Load RDealDetails Callback.");
				if(success){					
					console.log("Permission");
					console.log(records[0].data.permission.modifiable);
					var dealTabBar = baseTabPanel.getTabBar();
					console.log(dealTabBar.items.items);
					var dealTabs = dealTabBar.items.items;
					var activeDealTab = dealTabs[dealTabs.length-1];
					
					if(records[0].data.permission.modifiable!=0){
						console.log("Added button : removeReadOnlyBtn");
						activeDealTab.addCls('removeReadOnlyBtn');
					} else {
						activeDealTab.removeCls('removeReadOnlyBtn');
					}
					me.afterRDealDetailsLoad(tabItem);
				}
			}
		});
	},
	
	onSalesMenuGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
        console.log("Sales Grid Right Click");
		var me = this;
		me.showSalesGridMenu(view.up('tab-sales-grid'), record,e);
        //e.stopEvent();
    },
	
	salesGridItemMenuClick: function(menu, item, e, eOpts) {
        console.log("Sales Grid Menu Clicked");
        var me = this;
		
		var tabsalespanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var tabsalesgrid = tabsalespanel.down('tab-sales-grid');
		
        var action = item.action;
        if(action === "opensale"){
            if (tabsalesgrid.getSelectionModel().hasSelection()) {
                var record = tabsalesgrid.getSelectionModel().getSelection()[0];

                me.showDealDetail(record,tabsalesgrid);
            }
        } else if(action === "printsale"){
			if (tabsalesgrid.getSelectionModel().hasSelection()) {
                var record = tabsalesgrid.getSelectionModel().getSelection()[0];
				var dealid = record.get('idDeal');
            }
			var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
			var apikey = Ext.util.Cookies.get("apikey-"+DM2.view.AppConstants.apiurl);
			/////
			window.open("./resources/printSnapshot.php?apikey="+apikey+"&username="+userid+"&dealid="+dealid+"&apiurl="+DM2.view.AppConstants.apiurl,"_blank");
        } else if(action === "createnewsale"){
            if (tabsalesgrid.getSelectionModel().hasSelection()) {
                var record = tabsalesgrid.getSelectionModel().getSelection()[0];
				
				tabsalesgrid.rightMenuClick = "createnewsalefrom";
				
                me.showDealDetail(record,tabsalesgrid);
            }
        } else if(action === "refreshsales"){
			me.getController("SalesController").loadAllSales(); //Load All Sales
		}
    },
	
	salesdealHistoryTabClick: function(tabItem) {
		console.log("Set Deal History Tab");
		var me = this;
		tabItem.up('tabpanel').setActiveTab(tabItem.up('tabpanel').down('dealhistorygrid'));
		me.getController('DealHistoryController').onCategoryChange("Addressess", tabItem.down('menu'),"sale");
	},
	
	salesdealHistoryTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Sales Deal History Menu Clicked");
        var me = this;
        var text = item.text;
		menu.up('tabpanel').setActiveTab(menu.up('tabpanel').down('dealhistorygrid'));
		me.getController('DealHistoryController').onCategoryChange(text, menu,"sale");			
	},
	
	onSalesMenuGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		console.log("Sales Grid Item Long Presss");
		console.log(record);
		var grid = view.up('tab-sales-grid');
		grid.getSelectionModel().select(record);
		me.showSalesGridMenu(grid,record,e);
	},
	
	showSalesGridMenu: function(grid,record,e){
		var me = this;
		e.stopEvent();
        var menu = grid.contextMenu;
        menu.showAt(e.getXY());
		/*
		var menuitems = menu.items.items;
		console.log(menuitems);
		var readable = record.get('readable');
		console.log("readable : "+readable);
		if(readable===0){						
			for(var i=0;i<menuitems.length;i++){
				if(menuitems[i].modifyMenu === "yes"){
					menuitems[i].disable();
				}
			}
		} else {
			for(var i=0;i<menuitems.length;i++){
				if(menuitems[i].modifyMenu === "yes"){
					menuitems[i].enable();
				}
			}
		}*/
		
		if(record.get('status')!="CL"){
			menu.down('menuitem[action="createnewsale"]').disable();
		} else {
			menu.down('menuitem[action="createnewsale"]').enable();
			/*if(record.get('covered') && record.get('covered')==1){
				menu.down('#createnewdeal').disable();
			}*/
		}
	}
});