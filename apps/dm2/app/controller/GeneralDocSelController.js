Ext.define('DM2.controller.GeneralDocSelController', {
    extend: 'Ext.app.Controller',

    refs: {
        generaladddealdocpanel: {
            autoCreate: true,
            selector: 'generaladddealdocpanel',
            xtype: 'generaladddealdocpanel'
        },
        generaldocmenu: {
            autoCreate: true,
            selector: 'generaldocmenu',
            xtype: 'generaldocmenu'
        },
        generaltemplateselgridpanel: {
            autoCreate: true,
            selector: 'generaltemplateselgridpanel',
            xtype: 'generaltemplateselgridpanel'
        },
        generaldocselrepositorypanel: {
            autoCreate: true,
            selector: 'generaldocselrepositorypanel',
            xtype: 'generaldocselrepositorypanel'
        }
    },

    init: function(application) {
        this.control({
            '#selectfiletoolbar menu':{
                click:this.onGeneralDocMenuClick
            },
            'generaldocmenu':{
                click:this.onGeneralDocMenuClick
            },
            'generaltemplateselgridpanel':{
                'close': this.onGeneralTemplateSelGridPanelClose,
                select: this.generalTemplateSelGridItemSelect
            },
            'generaltemplateselgridpanel #createtmpbtn':{
                click: this.createGeneralTmpBtnClick
            },
            'generaldocmenu #addgeneraldocfile':{
                change: this.addDealDocGeneralFileFieldChange
            },
            '#selectfiletoolbar #selectdocfiletoolbar':{
                change: this.addDealDocGeneralFileFieldChange
            },
            'generaladddealdocpanel':{
                close:this.generalAddDealDocPanelCloseBtnClick,
                load: this.onGeneralAddDealDocPanelDrop
            },
            'generaladddealdocpanel grid':{
                select: this.addGeneralDealDocItemSelect,
                containercontextmenu: this.onGeneralAddDealDocPanelRightClick,
                rowcontextmenu: this.onGeneralAddDealDocItemRightClick
            },
            'generaladddealdocpanel textareafield[name="desc"]':{
                keyup: this.onChangeClsfDesc
            },
            'generaladddealdocpanel combobox[name="clsfctn"]':{
                keyup: this.onChangeClsfDesc,
				change: this.onChangeClsfValue
            },
			'generaladddealdocpanel textfield[name="name"]':{
                keyup: this.onChangeClsfDesc
            },
            'generaladddealdocpanel #dealdocuploadallbtn':{
                click: this.dealDocUploadAllBtnClick
            },
            'generaladddealdocpanel #dealdocclearallbtn':{
                click: this.dealDocClearAllBtnClick
            },
            'generaldocselrepositorypanel':{
                'close': this.onGeneralDocSelRepositoryPanelClose
            },
            'generaldocselrepositorypanel #generaldocsselrepositorygridpanel':{
                select:this.generalDocsSelRepoGridItemSelect
            },
            'generaldocselrepositorypanel #savedocbtn':{
                click: this.saveDocRepositoryBtnClick
            }
        });
    },

    showGeneralDocSelPanel: function(st,tabdealdetail) {
        var me = this,eastregion;
		
		if(st==="RecvDealDocs"){
			console.log(tabdealdetail.xtype);
			if(tabdealdetail.xtype == "tab-property-layout"){
				eastregion = tabdealdetail.down('#property-east-panel');
			} else if(tabdealdetail.xtype == "tab-contact-layout"){
				eastregion  = tabdealdetail.down('#contact-east-panel');
			} else if(tabdealdetail.xtype == "tab-sales-layout"){
				eastregion  = tabdealdetail.down('#sales-east-panel');
			} else {
				eastregion  = tabdealdetail.down('#deal-east-panel');
			}
			/*else if(tabdealdetail.xtype == "tab-property-detail"){
				me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
			}*/
			//eastregion.removeAll();
			var generaladddealdocpanel;
			if(tabdealdetail.down('generaladddealdocpanel')){
				console.log("Use Old Panel");
				generaladddealdocpanel = tabdealdetail.down('generaladddealdocpanel');
			} else {
				console.log("Create New east add deal doc Panel");
				generaladddealdocpanel = Ext.create('DM2.view.GeneralAddDealDocPanel');//me.getGeneraladddealdocpanel();
				eastregion.add(generaladddealdocpanel);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(generaladddealdocpanel);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();			
			eastregion.config.backview = st;
        } else {
			
			//var eastregion = tabdealdetail.down('dealdetailsformpanel').down('[region=east]');
			var eastregion = tabdealdetail.down('[region=east]');
			var generaladddealdocpanel;
			if(tabdealdetail.down('generaladddealdocpanel')){
				console.log("Use Old Panel");
				generaladddealdocpanel = tabdealdetail.down('generaladddealdocpanel');
			} else {
				//create new GeneralAddDealDoc Panel
				console.log("Create New Panel");
				generaladddealdocpanel = Ext.create('DM2.view.GeneralAddDealDocPanel');//me.getActivitydetailpanel();
			}
			if(generaladddealdocpanel.getHeader())
			{
				generaladddealdocpanel.getHeader().show();
			}
			if(tabdealdetail.config.activityid){
				generaladddealdocpanel.config.activityid = tabdealdetail.config.activityid;
			}
			eastregion.getLayout().setActiveItem(generaladddealdocpanel);		
			eastregion.config.backview = st;
		}

        var title = "";
        if(st==="approval"){
            var clsf = me.getController('ApprovalController').getApprovalpanel().config.clsf;
            if(clsf==="r.c"){
                title = "Activities > Approval > Receive Commitment";
            }else {
				var newclsf = clsf.substring(0,clsf.length - 1);
				if(newclsf==="r.m"){
                	title = "Activities > Approval > Receive Modificaton Reply";
				}
            }
        } else if(st==="RecvSignLOI"){
           title = "Activities > Received Signed LOI";
        } else if(st==="RecvDealDetailDocs" || st==="RecvDealDocs"){
           title = "Add Documents";
        } else if(st==="RecvGoodFaithCheck"){
            title = "Activities > Received Good Faith Check";
        } else if(st==="RentRoll"){
            title = "Activities > Receive Rent Roll";
        } else if(st==="Expenses"){
            title = "Activities > Receive Expenses Document";
        } else if(st==="RxSiBrkCnt"){
            title = "Activities > Receive Signed Brokerage Contract";
        } else if(st==="RxFinCntr"){
            title = "Activities > Receive Finalized Contract";
        } else if(st==="RxSiBrCtTS"){
            title = "Activities > Receive Signed Brokerage Contract To Seller";
        } else if(st==="RxSiBrCtTB"){
            title = "Activities > Receive Signed Brokerage Contract To Buyer";
        } else if(st==="receivebids"){
            title = "Activities > Receive Bids";
        } else if(st==="sendloi"){
            title = "Activities > Send an LOI";
        } else if(st==="application"){
            title = "Activities > Application";
        } else if(st==="reiterate"){
            title = "Activities > Reiterate";
        } else if(st==="sendmodification"){
            title = "Activities > Approval > Send Mod";
        } else if(st==="createofferingmemorandum"){
            title = "Activities > Create Offering Memorandum";
        } else if(st==="SnCntToB"){
            title = "Activities > Send Contract To Buyer";
        } else if(st==="SnCntToS"){
            title = "Activities > Send Contract To Seller";
        } else if(st==="SnBrCntToS"){
            title = "Activities > Send Brokerage Contract To Seller";
        } else if(st==="SnBrCntToB"){
            title = "Activities > Send Brokerage Contract To Buyer";
         }else if(st==="ModiFN"){
            title = "Activities > Modification";
        }
        generaladddealdocpanel.setTitle(title);

        var docstempst = generaladddealdocpanel.down('grid').getStore();//Ext.getStore('DocsTemp');
        docstempst.removeAll();
        generaladddealdocpanel.getForm().reset();
		
		var cls_st = generaladddealdocpanel.down('combobox[name="clsfctn"]').getStore();
		me.loadClassification(cls_st);
    },
	
	loadClassification: function(cls_st){
		var me = this;
		var selectionList_st = Ext.getStore('SelectionList');
		var clList = Ext.Array.filter(selectionList_st.data.items, function(item) {
			return item.data.selectionType=="CL";
		});
		console.log(clList);
		cls_st.loadData(clList);
	},

    onGeneralDocMenuClick: function(menu, item, e, eOpts) {
        console.log("GeneralDoc Menu Clicked");
        var me = this;
        var eventname = item.itemevent;
        if(eventname === "addfile"){
            console.log("Doc Add Menu Item Clicked");
            //var docstempst = Ext.getStore('DocsTemp');
            //docstempst.removeAll();
            //me.getGeneraladddealdocpanel().getForm().reset();
        } else if(eventname === "filetemplate"){
            me.showGeneralTemplateSelGridPanel("gendoc");
        } else if(eventname === "filerepository"){
            me.showGeneralDocSelRepositoryPanel("gendoc");
        } else if(eventname === "removedoc"){
            me.removeDocFromAddDealDocPanel();
        }
    },

    onGeneralAddDealDocPanelRightClick: function(cmp, e, eOpts) {
        e.stopEvent();
        this.showGeneralDocMenu(e);
    },

    onGeneralAddDealDocItemRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        e.stopEvent();
        this.showGeneralDocMenu(e);
    },

    showGeneralDocMenu: function(e) {
        console.log("Show Menu for General Docs");
        if(this.getGeneraldocmenu()){
            this.getGeneraldocmenu().showAt(e.getXY());
        }
    },

    showGeneralTemplateSelGridPanel: function(closeview) {
        var me = this;
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();	
		
		var gentemplateselgridpanel,eastregion;
		
        if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			eastregion = detailpanel.down('[region=east]');
			if(detailpanel.down('generaltemplateselgridpanel')){
				console.log("Use Old Panel");
				gentemplateselgridpanel = detailpanel.down('generaltemplateselgridpanel');
			} else {
				//create new Activity Detail Panel
				console.log("Create New Panel");
				gentemplateselgridpanel = Ext.create('DM2.view.GeneralTemplateSelGridPanel');//me.getGeneraltemplateselgridpanel();
			}
			eastregion.getLayout().setActiveItem(gentemplateselgridpanel);
			
		} else if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout" || context.xtype == "tab-property-layout" || context.xtype == "tab-contact-layout"){
			
			if(context.xtype == "tab-property-layout"){
				eastregion = context.down('#property-east-panel');
			} else if(context.xtype == "tab-contact-layout"){
				eastregion = context.down('#contact-east-panel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion = context.down('#sales-east-panel');
			} else {
				eastregion  = context.down('#deal-east-panel');
			}
			//eastregion.removeAll();			
			if(context.down('generaltemplateselgridpanel')){
				console.log("Use Old Panel");
				gentemplateselgridpanel = context.down('generaltemplateselgridpanel');
			} else {
				console.log("Create New Panel");
				gentemplateselgridpanel = Ext.create('DM2.view.GeneralTemplateSelGridPanel');//me.getGeneraltemplateselgridpanel();
				eastregion.add(gentemplateselgridpanel);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(gentemplateselgridpanel);			
			//eastregion.setTitle(title);
			eastregion.getHeader().hide();
        }
		
        //Load Selected Bank's Contacts
        me.loadTemplates(gentemplateselgridpanel);
        gentemplateselgridpanel.down('form').getForm().reset();
        gentemplateselgridpanel.config.closeview = closeview;
		
		var cls_st = gentemplateselgridpanel.down('combobox[name="clsfctn"]').getStore();
		me.loadClassification(cls_st);
    },

	loadTemplates: function(gentmpgrid) {
        //Load Templates & add to submenu
        var me = this;
        var templatesst = gentmpgrid.getStore();		
		var templatesstproxy = templatesst.getProxy();
        /*templatesstproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var templatesstproxyCfg = templatesstproxy.config;
        templatesstproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		templatesst.setProxy(templatesstproxyCfg);
        templatesst.load({
            scope: this,
            callback: function(records, operation, success) {
            }
        });
    },
    showGeneralDocSelRepositoryPanel: function(closeview) {
        var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();	
		
		var generaldocselrepositorypanel,eastregion;
		if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout" || context.xtype == "tab-property-layout" || context.xtype == "tab-contact-layout"){
			if(context.xtype == "tab-property-layout"){
				eastregion = context.down('#property-east-panel');
			} else if(context.xtype == "tab-contact-layout"){
				eastregion = context.down('#contact-east-panel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion = context.down('#sales-east-panel');
			} else {
				eastregion  = context.down('#deal-east-panel');
			}
			//eastregion.removeAll();			
			if(context.down('generaldocselrepositorypanel')){
				console.log("Use Old Panel");
				generaldocselrepositorypanel = context.down('generaldocselrepositorypanel');
			} else {
				console.log("Create New Panel");
				generaldocselrepositorypanel = Ext.create('DM2.view.GeneralDocSelRepositoryPanel');//me.getGeneraldocselrepositorypanel();
				eastregion.add(generaldocselrepositorypanel);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(generaldocselrepositorypanel);			
			//eastregion.setTitle(title);
			eastregion.getHeader().hide();
        } else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			eastregion = detailpanel.down('[region=east]');
			if(detailpanel.down('generaldocselrepositorypanel')){
				console.log("Use Old Panel");
				generaldocselrepositorypanel = detailpanel.down('generaldocselrepositorypanel');
			} else {
				//create new Activity Detail Panel
				console.log("Create New Panel");
				generaldocselrepositorypanel = Ext.create('DM2.view.GeneralDocSelRepositoryPanel');//me.getGeneraldocselrepositorypanel();
			}
			eastregion.getLayout().setActiveItem(generaldocselrepositorypanel);
		}		
        //Load Documents
        var filterstarr = [];
        me.loadAllDocs(filterstarr,generaldocselrepositorypanel);
        generaldocselrepositorypanel.config.closeview = closeview;
		
		var cls_st = generaldocselrepositorypanel.down('combobox[name="classification"]').getStore();
		me.loadClassification(cls_st);		
    },
	
	loadAllDocs: function(filterstarr,gendocselrepopanel) {
        var me = this;
        var docsallst = gendocselrepopanel.down('generaldocsselrepositorygridpanel').getStore('Docsall');
		
		var docsallstproxy = docsallst.getProxy();
        /*docsallstproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var docsallstproxyCfg = docsallstproxy.config;
        docsallstproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		docsallst.setProxy(docsallstproxyCfg);
        docsallst.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){

            }
        });
    },
	
    filterDocBtnClick: function() {
        var me = this;
        console.log("Filter Doc");
        var docselfrm = me.getGeneraldocselrepositorypanel().down('form');
        var nameval = docselfrm.getForm().findField('docnameser').getValue();
        var descval = docselfrm.getForm().findField('descser').getValue();
        var filterstarr = [];
        if(nameval!==''){
            var filterst = "name like '%"+nameval+"%'";
            filterstarr.push(filterst);
        }
        if(descval!==''){
            var filterst = "description like '%"+descval+"%'";
            filterstarr.push(filterst);
        }
        ///Load All the Banks Store///
        me.getController('DealActivityController').loadAllDocs(filterstarr);
    },

    onGeneralTemplateSelGridPanelClose: function(panel) {
        var me = this;
		var activemenutab = panel.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();	

		if(context.xtype == "tab-deal-layout"){
			var eastregion = context.down('#deal-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-sales-layout"){
			var eastregion = context.down('#sales-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-property-layout"){
			var eastregion = context.down('#property-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-contact-layout"){
			var eastregion = context.down('#contact-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			var backview = eastregion.config.backview;
			var clsview = detailpanel.down('generaltemplateselgridpanel').config.closeview;
			if(clsview==="gendoc"){
				me.showGeneralDocSelPanel(backview,context);
			} else if(clsview==="senddoc"){
				me.getController('SendDocumentController').goToSendDocument();
			}
		}
    },

    generalTemplateSelGridItemSelect: function(selModel, record, index, eOpts) {
        var me = this;
        console.log("Template Selected");
		var activemenutab = selModel.view.ownerCt.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
        var templatepanel = context.down('generaltemplateselgridpanel');
		templatepanel.down('form').getForm().loadRecord(record);
		
        me.setClassification(context);
        var clsfcn = me.getClassification(context);
        templatepanel.down('form').getForm().findField('clsfctn').setValue(clsfcn);
    },

    getClassification: function(context) {
        var me = this, clsfcn = "", eastregion, backview="";
		
		if(context.xtype == "tab-deal-detail"){
			eastregion = context.down('dealdetailsformpanel').down('[region=east]');
	        backview = eastregion.config.backview;
		} else if(context.xtype == "tab-sales-detail"){
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			eastregion = detailpanel.down('[region=east]');
	        backview = eastregion.config.backview;
		}
				
        if(backview==="approval"){
            var tmprec = context.down('dealdetailsformpanel').down('approvalpanel').config.approvalRec;
            var clsf = context.down('dealdetailsformpanel').down('approvalpanel').config.clsf;
            //clsfcn = clsf+"."+tmprec.get('bank');
			clsfcn = tmprec.get('bank')+"."+clsf;
        } else if(backview==="RecvSignLOI"){
            clsfcn = "RxSignLOI";
        } else if(backview==="RecvGoodFaithCheck"){
            clsfcn = "RxGdFhCk";
        } else if(backview==="RentRoll"){
            clsfcn = "RentRoll";
        } else if(backview==="Expenses"){
            clsfcn = "Expenses";
        } else if(backview==="RxFinCntr" || backview==="RxSiBrkCnt" || backview==="RxSiBrCtTS" || backview==="RxSiBrCtTB" || backview === "SnBrCntToS" || backview === "SnCntToB" || backview === "SnCntToS" || backview === "SnBrCntToB" || backview === "ModiFN"){
            clsfcn = backview;
        } else if(backview==="createofferingmemorandum"){
            clsfcn = "CrOffMemo";
        }  else if(backview==="receivebids"){
            clsfcn = "RxBids";
        } else if(backview === "sendloi"){
            clsfcn = 'LOI';
        } else if(backview === "application"){
            clsfcn = 'Application';
        } else if(backview==="reiterate"){
            clsfcn = 'Reiterate';
        } else if(backview==="sendmodification"){
			var approvalpanel = context.down('dealdetailsformpanel').down('approvalpanel');
			
			var moddocgrid = approvalpanel.down('#approvalgridpanel');
			var moddoccnt = moddocgrid.getStore().getCount()+1;
	        approvalpanel.config.clsf = "s.m"+moddoccnt;
			
            var tmprec = approvalpanel.config.approvalRec;
            var clsf = approvalpanel.config.clsf;
            //clsfcn = clsf+"."+tmprec.get('bank');			
			clsfcn = tmprec.get('bank')+"."+clsf;
        }

        return clsfcn;
    },

    createGeneralTmpBtnClick: function(btn) {
        console.log("Create Doc File From Template");
        var me = this,associateid;
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		var templategridpanel;
		if(context.getXType()=="tab-deal-detail") {
			templategridpanel = context.down('dealdetailsformpanel').down('generaltemplateselgridpanel');
			var row = context.record;
        	var dealid = row.data.idDeal;
			associateid = dealid;
		} else if(context.getXType()=="tab-sales-detail") {
			templategridpanel = context.down('salesdealdetailsformpanel').down('generaltemplateselgridpanel');
			var row = context.record;
        	var dealid = row.data.idDeal;
			associateid = dealid;
		} else if(context.getXType()=="tab-deal-layout"){
			templategridpanel = context.down('generaltemplateselgridpanel');
			var dealgrid = context.down('tab-deal-grid');
			if (dealgrid.getSelectionModel().hasSelection()) {
				var row = dealgrid.getSelectionModel().getSelection()[0];
				var dealid = row.get('idDeal');
				associateid = dealid;
			}
		} else if(context.getXType()=="tab-sales-layout"){
			templategridpanel = context.down('generaltemplateselgridpanel');
			var dealgrid = context.down('tab-sales-grid');
			if (dealgrid.getSelectionModel().hasSelection()) {
				var row = dealgrid.getSelectionModel().getSelection()[0];
				var dealid = row.get('idDeal');
				associateid = dealid;
			}
		} else if(context.getXType()=="tab-property-layout"){
			templategridpanel = context.down('generaltemplateselgridpanel');
			var propertygrid = context.down('tab-property-grid');
			if (propertygrid.getSelectionModel().hasSelection()) {
				var row = propertygrid.getSelectionModel().getSelection()[0];
				var propertyid = row.get('idPropertyMaster');
				//associateid = propertyid;
				
				if(context.config.propertyid==0){
					Ext.Msg.alert('Failed', "Property is not exist in property table for selected master property.");
					return false;
				}else{
					associateid = context.config.propertyid;
				}
				//console.log("PropertyId:"+context.config.propertyid);
			}
		} else if(context.getXType()=="tab-contact-layout"){
			templategridpanel = context.down('generaltemplateselgridpanel');
			var contactgrid = context.down('tab-contact-grid');
			if (contactgrid.getSelectionModel().hasSelection()) {
				var row = contactgrid.getSelectionModel().getSelection()[0];
				//var contactid = row.get('idContact');
				var contactid = row.get('contactid');
				associateid = contactid;
			}
		}    

        if (templategridpanel.getSelectionModel().hasSelection()) {
            var record = templategridpanel.getSelectionModel().getSelection()[0];

            var descval = templategridpanel.down('form').getForm().findField('description').getValue();
			var nameval = templategridpanel.down('form').getForm().findField('name').getValue();
            var clsfctnval = templategridpanel.down('form').getForm().findField('clsfctn').getValue();
			clsfctnval = me.getShortClassification(clsfctnval);
            var templateid = record.get('idTemplate');

            var methodname = "POST";
            var data = {
                'requestType' : "template",
                'paramater1'  :  associateid.toString(),
                'paramater2'  :  templateid.toString(),
                'paramater3'  :  nameval,
                'paramater4'  :  clsfctnval,
                'paramater5'  :  descval
            };

            templategridpanel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
                scope:this,
                method:methodname,
                params: Ext.util.JSON.encode(data),
                success: function(response, opts) {
                    templategridpanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201)
                    {
                        //Load the Docs Per Deal
						if(context.getXType()=="tab-deal-detail" || context.getXType()=="tab-sales-detail"){
							var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
							var dealdocsgridpanel = context.down('dealdocsgridpanel');							
							me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
							//me.getController("DealDetailController").loadDealDetailDocs(context,"deal");
							var eastregion = detailpanel.down('[region=east]');
							var backview = eastregion.config.backview;
							if(backview==="sendloi" || backview==="application" || backview==="reiterate" || backview==="sendmodification" || backview==="SnCntToB"  || backview==="SnCntToS" || backview==="SnBrCntToS" || backview==="SnBrCntToB" || backview==="ModiFN"){
								me.getController('SendDocumentController').loadSendDocuments(associateid,context);
								/*	
								var docurl = obj.txsummary[0].fileURL;
								var docname = obj.txsummary[0].fileName;
								docname = docname.replace("/", ".");
	
								///// Add the file item to Store
								var tmpdocrec = Ext.create('DM2.model.Document',{
									'name' : docname,
									'clsfctn' :clsfctnval,
									'desc' :descval,
									'url' : docurl
								});
								
								var docgrid = context.down('dealdetailsformpanel').down('senddocumentpanel').down('#dealdocssendloigridpanel');
								var senddocsperdeal_st = docgrid.getStore();
								senddocsperdeal_st.add(tmpdocrec);
								
								docgrid.getView().refresh();
								*/
								//docgrid.getSelectionModel().select((senddocsperdeal_st.getCount()-1));
							}
						} else if(context.getXType()=="tab-deal-layout" || context.getXType()=="tab-sales-layout"){
							//me.getController("DocController").loadDocsStore(dealid);
							var dealdocsgridpanel = context.down('dealdocsgridpanel');
							me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
						} else if(context.getXType()=="tab-property-layout"){
							var propertygrid = context.down('tab-property-grid');
							if (propertygrid.getSelectionModel().hasSelection()) {
								var row = propertygrid.getSelectionModel().getSelection()[0];
								var propertyid = row.get('idPropertyMaster');
								me.getController("PropertyController").loadDocsPerProperty(propertyid,context.down('propertydocsgridpanel'));
							}
						} else if(context.getXType()=="tab-contact-layout"){
							//me.getController("ContactController").loadContactDocsStore(associateid,context.down('contactdocsgridpanel'));
						}
                        me.afterDocUpload(context);
                    } else {
                        console.log("Generate File Failed.");
                        Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    templategridpanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Generate File', obj.errorMessage);
                }
            });
        }
        else{
            Ext.Msg.alert('Failed', "Please select template.");
        }
    },

    afterDocUpload: function(context) {
        var me = this;
		//var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		//var context = tabdealpanel.getActiveTab();
		var generaladddealdocpanel,detailpanel;
		if(context.getXType()=="tab-deal-detail" || context.getXType()=="tab-sales-detail"){			
			if(context.getXType()=="tab-deal-detail"){
				detailpanel = context.down('dealdetailsformpanel');
			}else if(context.getXType()=="tab-sales-detail"){
				detailpanel = context.down('salesdealdetailsformpanel');
			}
			var eastregion = detailpanel.down('[region=east]');
			var backview = eastregion.config.backview;
			if(backview==="approval"){
				apprcntrl = me.getController('ApprovalController');
				var tmprec = detailpanel.down('approvalpanel').config.approvalRec;
				apprcntrl.loadApproval(tmprec,context);
	
				apprcntrl.showApprovalView(context);
				
				var record = context.record;
		        var dealid = record.data.idDeal;
				apprcntrl.checkRecvCommit(tmprec,dealid,context);
			} else if(backview==="RentRoll" || backview==="Expenses" || backview==="RxSiBrkCnt" || backview==="RxFinCntr" || backview==="RxSiBrCtTS" || backview==="RxSiBrCtTB" || backview==="receivebids" || backview==="createofferingmemorandum"){
				me.getController('AddRentRollExpenseController').afterRentRollExpenseDocUpload(context);
			} else if(backview==="RecvSignLOI"){
				me.getController('RecvSignedLOIController').afterRecvSignedLOIDocUpload(context);
			} else if(backview==="RecvGoodFaithCheck"){
				me.getController('RecvGoodFaithCheckController').afterRecvGoodFaithCheckDocUpload(context);
			} else if(backview==="sendloi" || backview==="SnCntToB" || backview==="SnCntToS" || backview==="SnBrCntToS" || backview==="SnBrCntToB" || backview==="ModiFN"){
				me.getController('SendDocumentController').goToSendDocument(context);
				me.getController('SendDocumentController').selectSendDocumentAfterLoad(context);
			} else if(backview==="application"){
				me.getController('SendDocumentController').goToSendDocument(context);
				me.getController('SendDocumentController').selectSendDocumentAfterLoad(context);
			} else if(backview==="reiterate"){
				me.getController('SendDocumentController').goToSendDocument(context);
				me.getController('SendDocumentController').selectSendDocumentAfterLoad(context);
			} else if(backview==="sendmodification"){
				me.getController('SendDocumentController').goToSendDocument(context);
				me.getController('SendDocumentController').selectSendDocumentAfterLoad(context);
	
				apprcntrl = me.getController('ApprovalController');
				var tmprec = context.down('dealdetailsformpanel').down('approvalpanel').config.approvalRec;
				apprcntrl.loadApproval(tmprec,context);
			} else if(backview==="RecvDealDetailDocs"){
				me.getController('DocController').closeRecvDealDetailDocs(context);
			}
		} else if(context.getXType()=="tab-deal-layout"){
			var eastregion = context.down('#deal-east-panel');
			var backview = eastregion.config.backview;
			if(backview==="RecvDealDocs"){
				me.getController('DocController').closeRecvDealDetailDocs(context);
			}
		} else if(context.getXType()=="tab-sales-layout"){
			var eastregion = context.down('#sales-east-panel');
			var backview = eastregion.config.backview;
			if(backview==="RecvDealDocs"){
				me.getController('DocController').closeRecvDealDetailDocs(context);
			}
		} else if(context.getXType()=="tab-property-layout"){
			var eastregion = context.down('#property-east-panel');
			var backview = eastregion.config.backview;
			if(backview==="RecvDealDocs"){
				me.getController('PropertyController').closeRecvDealDetailDocs(context);
			}
		} else if(context.getXType()=="tab-contact-layout"){
			var eastregion = context.down('#contact-east-panel');
			var backview = eastregion.config.backview;
			if(backview==="RecvDealDocs"){
				me.getController('ContactController').closeRecvDealDetailDocs(context);
			}
		}  
    },

    onGeneralDocSelRepositoryPanelClose: function(panel) {
        var me = this;
		var activemenutab = panel.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();	

		if(context.xtype == "tab-deal-layout"){
			var eastregion = context.down('#deal-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-sales-layout"){
			var eastregion = context.down('#sales-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-property-layout"){
			var eastregion = context.down('#property-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-contact-layout"){
			var eastregion = context.down('#contact-east-panel');
			var backview = eastregion.config.backview;
			me.showGeneralDocSelPanel(backview,context);
        } else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			var backview = eastregion.config.backview;
			var clsview = context.down('generaldocselrepositorypanel').config.closeview;
			if(clsview==="gendoc") {
				me.showGeneralDocSelPanel(backview,context);
			} else if(clsview==="senddoc"){
				me.getController('SendDocumentController').goToSendDocument();
			}
		}
    },

    generalDocsSelRepoGridItemSelect: function(selModel, record, item, index, e, eOpts) {
        var me = this;
		var activemenutab = selModel.view.ownerCt.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();	
		
        var repopanel = context.down('generaldocselrepositorypanel');
        repopanel.down('form').getForm().loadRecord(record);

        me.setClassification(context);
        var clsfcn = me.getClassification(context);
        repopanel.down('form').getForm().findField('classification').setValue(clsfcn);
    },

    saveDocRepositoryBtnClick: function(btn) {
        console.log("Create Doc File From Repo");
        var me = this;
		
		var tabdealpanel = btn.up('tab-panel').getLayout().getActiveItem();
		var context = tabdealpanel.getActiveTab();	
		var docselrepopanel,associateid;
		if(context.getXType()=="tab-deal-detail"){
			docselrepopanel = context.down('dealdetailsformpanel').down('generaldocselrepositorypanel');
			var row = context.record;
        	var dealid = row.data.idDeal;
			associateid = dealid;
		} else if(context.getXType()=="tab-sales-detail"){
			docselrepopanel = context.down('salesdealdetailsformpanel').down('generaldocselrepositorypanel');
			var row = context.record;
        	var dealid = row.data.idDeal;
			associateid = dealid;
		} else if(context.getXType()=="tab-sales-layout"){
			docselrepopanel = context.down('generaldocselrepositorypanel');
			var dealgrid = context.down('tab-sales-grid');
			if (dealgrid.getSelectionModel().hasSelection()) {
				var row = dealgrid.getSelectionModel().getSelection()[0];
				var dealid = row.get('idDeal');
				associateid = dealid;
			}
		} else if(context.getXType()=="tab-deal-layout"){
			docselrepopanel = context.down('generaldocselrepositorypanel');
			var dealgrid = context.down('tab-deal-grid');
			if (dealgrid.getSelectionModel().hasSelection()) {
				var row = dealgrid.getSelectionModel().getSelection()[0];
				var dealid = row.get('idDeal');
				associateid = dealid;
			}
		} else if(context.getXType()=="tab-property-layout"){
			docselrepopanel = context.down('generaldocselrepositorypanel');
			var propertygrid = context.down('tab-property-grid');
			if (propertygrid.getSelectionModel().hasSelection()) {
				var row = propertygrid.getSelectionModel().getSelection()[0];
				var propertyid = row.get('idPropertyMaster');
				//associateid = propertyid;
				if(context.config.propertyid==0){
					Ext.Msg.alert('Failed', "Property is not exist in property table for selected master property.");
					return false;
				} else {
					associateid = context.config.propertyid;
				}
				//console.log("PropertyId:"+context.config.propertyid);
			}
		}else if(context.getXType()=="tab-contact-layout"){
			docselrepopanel = context.down('generaldocselrepositorypanel');
			var contactgrid = context.down('tab-contact-grid');
			if (contactgrid.getSelectionModel().hasSelection()) {
				var row = contactgrid.getSelectionModel().getSelection()[0];
				//var contactid = row.get('idContact');
				var contactid = row.get('contactid');
				associateid = contactid;
			}
		}
		
        var docselgrid = docselrepopanel.down('grid');
        if (docselgrid.getSelectionModel().hasSelection()) {
            var record = docselgrid.getSelectionModel().getSelection()[0];

            var docselfrm = docselrepopanel.down('form');
            var idDocument = docselfrm.getForm().findField('idDocument').getValue();
            var clsfctnval = docselfrm.getForm().findField('classification').getValue();
			clsfctnval = me.getShortClassification(clsfctnval);
            var nameval = docselfrm.getForm().findField('name').getValue();
            var descval = docselfrm.getForm().findField('description').getValue();

            var methodname = "POST";
            var data = {
                'requestType' : "RepositoryCopyGeneral",
                'paramater1'  :  associateid.toString(),
                'paramater2'  :  idDocument.toString(),
                'paramater3'  :  nameval,
                'paramater4'  :  clsfctnval,
                'paramater5'  :  descval
            };

            docselrepopanel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
                scope:this,
                method:methodname,
                params: Ext.util.JSON.encode(data),
                success: function(response, opts) {
                    docselrepopanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201)
                    {
						//Load the Docs Per Deal
						if(context.getXType()=="tab-deal-detail"){
							var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
							var dealdocsgridpanel = context.down('dealdocsgridpanel');;
							me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
							//me.getController("DealDetailController").loadDealDetailDocs(context,"deal");
							var eastregion = detailpanel.down('[region=east]');
							var backview = eastregion.config.backview;
							if(backview==="sendloi" || backview==="application" || backview==="reiterate" || backview==="sendmodification" || backview==="SnCntToB" || backview==="SnCntToS" || backview==="SnBrCntToS" || backview==="SnBrCntToB" || backview==="ModiFN"){
								me.getController('SendDocumentController').loadSendDocuments(associateid,context);
								/*var docurl = obj.txsummary[0].fileURL;
								var docname = obj.txsummary[0].fileName;
								docname = docname.replace("/", ".");
	
								///// Add the file item to Store
								var tmpdocrec = Ext.create('DM2.model.Document',{
									'name' : docname,
									'clsfctn' :clsfctnval,
									'desc' :descval,
									'url' : docurl
								});
								var docgrid = context.down('dealdetailsformpanel').down('senddocumentpanel').down('#dealdocssendloigridpanel');
								var senddocsperdeal_st = docgrid.getStore();
								senddocsperdeal_st.add(tmpdocrec);
								
								docgrid.getView().refresh();
								*/
								//docgrid.getSelectionModel().select((senddocsperdeal_st.getCount()-1));
							}
						} else if(context.getXType()=="tab-deal-layout" || context.getXType()=="tab-sales-layout"){
							var dealdocsgridpanel = context.down('dealdocsgridpanel');
							me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
							//me.getController("DocController").loadDocsStore(dealid);
						} else if(context.getXType()=="tab-property-layout"){
							var propertygrid = context.down('tab-property-grid');
							if (propertygrid.getSelectionModel().hasSelection()) {
								var row = propertygrid.getSelectionModel().getSelection()[0];
								var propertyid = row.get('idPropertyMaster');
								me.getController("PropertyController").loadDocsPerProperty(propertyid,context.down('propertydocsgridpanel'));
							}
						} else if(context.getXType()=="tab-contact-layout"){
							//me.getController("ContactController").loadContactDocsStore(associateid,context.down('contactdocsgridpanel'));
						}
						me.afterDocUpload(context);
                    } else {
                        console.log("Generate File Failed.");
                        Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    docselrepopanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Generate File', obj.errorMessage);
                }
            });
        } else {
            Ext.Msg.alert('Failed', "Please select document.");
        }
    },

    generalAddDealDocPanelCloseBtnClick: function(panel) {
        var me = this;
		var activemenutab = panel.up('tab-panel').getLayout().getActiveItem();
		var tabdealdetail = activemenutab.getActiveTab();
		
		if(tabdealdetail.xtype == "tab-deal-layout"){
			var eastregion = tabdealdetail.down('#deal-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        } else if(tabdealdetail.xtype == "tab-sales-layout"){
			var eastregion = tabdealdetail.down('#sales-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        } else if(tabdealdetail.xtype == "tab-property-layout"){
			me.getController('PropertyController').closeRecvDealDetailDocs(tabdealdetail);
        } else if(tabdealdetail.xtype == "tab-contact-layout"){
			me.getController('ContactController').closeRecvDealDetailDocs(tabdealdetail);
        } else if(tabdealdetail.xtype == "tab-deal-detail" || tabdealdetail.xtype == "tab-sales-detail"){
			//var tabdealdetail = panel.up('tab-deal-detail');
			var eastregion;
			if(tabdealdetail.xtype == "tab-deal-detail"){
				eastregion = tabdealdetail.down('dealdetailsformpanel').down('[region=east]');
			} else if(tabdealdetail.xtype == "tab-sales-detail"){
				eastregion = tabdealdetail.down('salesdealdetailsformpanel').down('[region=east]');
			}
			//var eastregion = tabdealdetail.down('[region=east]');
			var backview = eastregion.config.backview;
			if(backview==="approval"){
				me.getController('ApprovalController').showApprovalView(tabdealdetail);
			} else if(backview==="RecvSignLOI"){
				me.getController('RecvSignedLOIController').closeRecvSignedLOI(tabdealdetail);
			} else if(backview==="RecvGoodFaithCheck"){
				me.getController('RecvGoodFaithCheckController').closeRecvGoodFaithCheck(tabdealdetail);
			} else if(backview==="RentRoll"){
				me.getController('AddRentRollExpenseController').closeRentRollExpense(tabdealdetail);
			} else if(backview==="Expenses" || backview==="RxSiBrkCnt" || backview==="RxFinCntr" || backview==="receivebids" || backview==="RxSiBrCtTS"|| backview==="RxSiBrCtTB" || backview==="createofferingmemorandum"){
				me.getController('AddRentRollExpenseController').closeRentRollExpense(tabdealdetail);
			} else if(backview==="sendloi" || backview==="SnCntToB" || backview==="SnCntToS" || backview==="SnBrCntToS" || backview==="SnBrCntToB" || backview==="ModiFN"){
				me.getController('SendDocumentController').goToSendDocument(tabdealdetail);
			} else if(backview==="application"){
				me.getController('SendDocumentController').goToSendDocument(tabdealdetail);
			} else if(backview==="reiterate"){
				me.getController('SendDocumentController').goToSendDocument(tabdealdetail);
			} else if(backview==="sendmodification"){
				me.getController('SendDocumentController').goToSendDocument(tabdealdetail);
			} else if(backview==="RecvDealDetailDocs"){
				me.getController('DocController').closeRecvDealDetailDocs(tabdealdetail);
			}   
		}
    },

    addDealDocGeneralFileFieldChange: function(field, value, eOpts) {
        console.log("File Selected through Add Doc Button");
        var me = this;
		//var context = field.up('tab-deal-detail');
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		//var activemenutab = field.up('tab-panel').getLayout().getActiveItem();
		//var context = activemenutab.getActiveTab();
		
        if(field.fileInputEl.dom.files.length > 0){
            var generaladddealdocpanel = context.down('generaladddealdocpanel');//me.getGeneraladddealdocpanel();

            var file = field.fileInputEl.dom.files[0];

            var filesize = (file.size/1024).toFixed(2);

            me.setClassification(context);
            var clsfcn = me.getClassification(context);

            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : file.name,
                'size' : filesize,
                'clsfctn' :clsfcn,
                'desc' :'',
                'fileobj':file
            });
            var docstempst = generaladddealdocpanel.down('grid').getStore('DocsTemp');//Ext.getStore('DocsTemp');
            docstempst.add(docrec);
            /////

            generaladddealdocpanel.down('grid').getSelectionModel().select(0);
            me.getGeneraldocmenu().hide();
			me.checkDocButtons();
        }
    },
	
	checkDocButtons: function(){
		var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
        var generaladddealdocpanel = context.down('generaladddealdocpanel');
		var docstempst = generaladddealdocpanel.down('grid').getStore('DocsTemp');//Ext.getStore('DocsTemp');
		if(docstempst.getCount()>0){
			generaladddealdocpanel.down('#dealdocclearallbtn').enable(true);
			generaladddealdocpanel.down('#dealdocuploadallbtn').enable(true);
		}
	},

    addGeneralDealDocItemSelect: function(selModel, record, index, eOpts) {
		var me = this;
		//var context = selModel.view.ownerCt.up('tab-deal-detail');	
		var activemenutab = selModel.view.ownerCt.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
        var adddocform = context.down('generaladddealdocpanel').getForm();
        adddocform.findField('name').focus();
		console.log(record);
        adddocform.loadRecord(record);
        adddocform.findField('docfilesize').setValue(record.get('size'));
		//console.log(record);
		var extension = me.getFilePathExtension(record.get('name'));
		var filenameonly = me.getFilePathName(record.get('name'));
		adddocform.findField('name').setValue(filenameonly);
		adddocform.findField('ext').setValue(extension);
    },
	
	getFilePathExtension: function (path) {
		var filename = path.split('\\').pop().split('/').pop();
		var lastIndex = filename.lastIndexOf(".");
		if (lastIndex < 1) return "";
		return filename.substr(lastIndex + 1);
	},
	
	getFilePathName: function (path) {
		var filename = path.split('\\').pop().split('/').pop();
		var lastIndex = filename.lastIndexOf(".");
		if (lastIndex < 1) return filename;
		return filename.substr(0,lastIndex);
	},

    dealDocUploadAllBtnClick: function(btn) {
        console.log("Doc Submit Button Clicked.");
        var me = this,associateid,parmname,generaladddealdocpanel;
		
		//var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		//var context = tabdealpanel.getActiveTab();
		
		var activemenutab = btn.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();	
		
		if(context.getXType()=="tab-deal-detail") {
			generaladddealdocpanel = context.down('dealdetailsformpanel').down('generaladddealdocpanel');
			var row = context.record;
        	associateid = row.data.idDeal;
			parmname = 'idDeal';
		} else if(context.getXType()=="tab-sales-detail") {
			generaladddealdocpanel = context.down('salesdealdetailsformpanel').down('generaladddealdocpanel');
			var row = context.record;
        	associateid = row.data.idDeal;
			parmname = 'idDeal';
		} else if(context.getXType()=="tab-deal-layout"){
			generaladddealdocpanel = context.down('generaladddealdocpanel');
			var dealgrid = context.down('tab-deal-grid');
			if (dealgrid.getSelectionModel().hasSelection()) {
				var row = dealgrid.getSelectionModel().getSelection()[0];
				associateid = row.get('idDeal');
				parmname = 'idDeal';
			}
		} else if(context.getXType()=="tab-sales-layout"){
			generaladddealdocpanel = context.down('generaladddealdocpanel');
			var dealgrid = context.down('tab-sales-grid');
			if (dealgrid.getSelectionModel().hasSelection()) {
				var row = dealgrid.getSelectionModel().getSelection()[0];
				associateid = row.get('idDeal');
				parmname = 'idDeal';
			}
		} else if(context.getXType()=="tab-property-layout"){
			generaladddealdocpanel = context.down('generaladddealdocpanel');
			var propertygrid = context.down('tab-property-grid');
			if (propertygrid.getSelectionModel().hasSelection()) {
				var row = propertygrid.getSelectionModel().getSelection()[0];
				//associateid = row.get('idPropertyMaster');
				parmname = 'idProperty';
				
				if(context.config.propertyid==0){
					Ext.Msg.alert('Failed', "Property is not exist in property table for selected master property.");
					return false;
				}else{
					associateid = context.config.propertyid;
				}
				console.log("PropertyId:"+context.config.propertyid);
			}
		} else if(context.getXType()=="tab-contact-layout"){
			generaladddealdocpanel = context.down('generaladddealdocpanel');
			var contactgrid = context.down('tab-contact-grid');
			if (contactgrid.getSelectionModel().hasSelection()) {
				var row = contactgrid.getSelectionModel().getSelection()[0];
				//associateid = row.get('idContact');
				associateid = row.get('contactid');
				parmname = 'idContact';
			}
		}          

        var docstempst = generaladddealdocpanel.down('grid').getStore();
        docstempst.each(function(record) {

            var fileObj = record.get('fileobj');
            var filename = record.get('name');

            var ext = /^.+\.([^.]+)$/.exec(filename);
            if(ext === null){
                ext = "";
            }else {
                ext = ext[1];
            }

            var uploadmode =  record.get('uploadmode');
            var tempclsf = record.get('clsfctn');
            if(tempclsf==="" || tempclsf===null){
                tempclsf = me.getClassification(context);
            }
			tempclsf = me.getShortClassification(tempclsf);
			console.log("tempclsf : "+tempclsf);
            if(uploadmode==="outlook"){

                var data = [{
                    'name'           : filename,
                    'description'    : record.get('desc'),
                    'classification' : tempclsf,
                    'size'           : record.get('filesize'),
                    'ext'            : ext,
                    'content'        : fileObj//,
                    //'idDeal': associateid
					//parmname : associateid
                }];
				if(parmname=="idProperty"){
					data[0].idProperty = associateid;
				}else if(parmname=="idContact"){
					data[0].idContact = associateid;
				}else{
					data[0].idDeal = associateid;
				}
				me.uploadDealDoc(generaladddealdocpanel,data,context,associateid,record);
            } else {
                var reader = new FileReader();

                reader.readAsDataURL(fileObj);

                reader.onload = function (e) {
					//console.log("Base64 Data :: ");
					//console.log(e.target.result);
                    var conentarr = e.target.result.split("base64,");
                    var content = '';
                    if(conentarr.length > 1){
                        content = conentarr[1];
                    }
					
					var prepend = 'b64:';
					content = prepend + content;
					
					//console.log("Modi Base64 Data :: ");
					//console.log(content);
					
                    var data = [{
                        'name'           : filename,
                        'description'    : record.get('desc'),
                        'classification' : tempclsf,
                        'size'           : fileObj.size,
                        'ext'            : ext,
                        'content'        : content//,
						//parmname : associateid
                        //'idDeal': associateid
                    }];
					if(parmname=="idProperty"){
						data[0].idProperty = associateid;
					}else if(parmname=="idContact"){
						data[0].idContact = associateid;
					}else{
						data[0].idDeal = associateid;
					}
					me.uploadDealDoc(generaladddealdocpanel,data,context,associateid,record);                    
                };
                reader.onerror = function (e) {
                    console.log(e.target.error);
                };
            }
        });
    },
	
	uploadDealDoc: function(generaladddealdocpanel,data,context,dealid,record){
		var me = this;
		var docstempst = generaladddealdocpanel.down('grid').getStore();
		generaladddealdocpanel.setLoading(true);
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'Rdocs',
			params: Ext.util.JSON.encode(data),
			scope:this,
			success: function(response, opts) {
				generaladddealdocpanel.setLoading(false);
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 201){
					if(context.getXType()=="tab-deal-detail" || context.getXType()=="tab-sales-detail"){						
						var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
						var dealdocsgridpanel = context.down('dealdocsgridpanel');
						me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
						//me.getController("DealDetailController").loadDealDetailDocs(context,"deal");
						var eastregion = detailpanel.down('[region=east]');						
						var backview = eastregion.config.backview;
						if(backview==="sendloi" || backview==="application" || backview==="reiterate" || backview==="sendmodification" || backview==="SnCntToB" || backview==="SnCntToS" || backview==="SnBrCntToS" || backview==="SnBrCntToB" || backview==="ModiFN"){
							var docurl = "";
	
							if(obj.txsummary[0].url){
								docurl = obj.txsummary[0].url;
							}else if(obj.txsummary[1]){
								if(obj.txsummary[1].url){
									docurl = obj.txsummary[1].url;
								}
							}
							
							me.getController('SendDocumentController').loadSendDocuments(dealid,context);
							/*
							///// Add the file item to Store
							var tmpdocrec = Ext.create('DM2.model.Document',{
								'name'    : data[0].name,
								'size'    : record.get('filesize'),
								'clsfctn' : data[0].classification,
								'desc'    : data[0].description,
								'url'     : docurl
							});
	
							var docgrid = context.down('dealdetailsformpanel').down('senddocumentpanel').down('#dealdocssendloigridpanel');
							var senddocsperdeal_st = docgrid.getStore();
							senddocsperdeal_st.add(tmpdocrec);
							
							docgrid.getView().refresh();*/
						}
					} else if(context.getXType()=="tab-deal-layout" || context.getXType()=="tab-sales-layout"){
						//me.getController("DocController").loadDocsStore(dealid);
						var dealdocsgridpanel = context.down('dealdocsgridpanel');
						me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
					} else if(context.getXType()=="tab-property-layout"){
						var propertygrid = context.down('tab-property-grid');
						if (propertygrid.getSelectionModel().hasSelection()) {
							var row = propertygrid.getSelectionModel().getSelection()[0];
							var propertyid = row.get('idPropertyMaster');
							me.getController("PropertyController").loadDocsPerProperty(propertyid,context.down('propertydocsgridpanel'));
						}
					} else if(context.getXType()=="tab-contact-layout"){
						//me.getController("ContactController").loadContactDocsStore(dealid,context.down('contactdocsgridpanel'));
					}
					generaladddealdocpanel.getForm().reset();

					var recindex = docstempst.indexOf(record);
					if(recindex === (docstempst.getCount()-1)){
						me.afterDocUpload(context);
					}
				} else {
					Ext.Msg.alert('Failed', "Please try again.");
				}
			},
			failure: function(response, opts) {
				generaladddealdocpanel.setLoading(false);
				Ext.Msg.alert('Failed', "Please try again.");
			}
		});
	},

    dealDocClearAllBtnClick: function(btn) {
        var me = this;
		//var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		//var context = tabdealpanel.getActiveTab();
		//console.log(context.getXType());
		var generaladddealdocgrid = btn.up('generaladddealdocpanel').down('grid');		
		var docstempst = generaladddealdocgrid.getStore();
		docstempst.removeAll();
		btn.up('generaladddealdocpanel').getForm().reset();
		/*if(context.getXType()=="tab-deal-detail"){
			var generaladddealdocgrid = context.down('dealdetailsformpanel').down('generaladddealdocpanel').down('grid');
			
	        var docstempst = generaladddealdocgrid.getStore();
    	    docstempst.removeAll();
        	context.down('dealdetailsformpanel').down('generaladddealdocpanel').getForm().reset();
		}*/
    },

    onGeneralAddDealDocPanelDrop: function(cmp, e, file) {
        console.log("Done Reading");
        var me = this,generaladddealdocpanel;
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var acttabdealdetail = activemenutab.getActiveTab();
		
		generaladddealdocpanel = acttabdealdetail.down('generaladddealdocpanel');

        var filesize = (file.size/1024).toFixed(2);

        me.setClassification(acttabdealdetail);
        var clsfcn = me.getClassification(acttabdealdetail);

        ///// Add the file item to Store
        var docrec = Ext.create('DM2.model.Document',{
            'name' : file.name,
            'size' : filesize,
            'clsfctn' :clsfcn,
            'desc' :'',
            'fileobj':file
        });
        var docstempst = generaladddealdocpanel.down('grid').getStore();
        docstempst.add(docrec);
        /////

        generaladddealdocpanel.down('grid').getSelectionModel().select(0);

        generaladddealdocpanel.getForm().findField('name').focus();
		me.checkDocButtons();
    },

    getGeneralDocFromOutlook: function(fna, fca) {
        var me = this,generaladddealdocpanel;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		if(context.getXType()=="tab-deal-detail"){
			generaladddealdocpanel = context.down('dealdetailsformpanel').down('generaladddealdocpanel');
		} else if(context.getXType()=="tab-deal-layout" || context.getXType()=="tab-sales-layout"){
			generaladddealdocpanel = context.down('generaladddealdocpanel');
		}        
		
        for (var i=0; i<fna.length; i++) {
            //console.log(fna[i]);
            //console.log(fca[i]);
            var filename = fna[i];
            var filecnt = fca[i];

            var filesize = filecnt.length;
            var filesizekb = (filesize/1024).toFixed(2);

            me.setClassification(context);
            var clsfcn = me.getClassification(context);

            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : filename,
                'size' : filesizekb,
                'clsfctn' :clsfcn,
                'desc' :'',
                'fileobj':filecnt,
                'uploadmode':'outlook',
                'filesize':filesize
            });
            var docstempst = generaladddealdocpanel.down('grid').getStore();
            docstempst.add(docrec);
            /////
			me.checkDocButtons();
        }

        generaladddealdocpanel.down('grid').getSelectionModel().select(0);
        generaladddealdocpanel.getForm().findField('name').focus();
    },

    removeDocFromAddDealDocPanel: function() {
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var acttabdealdetail = tabdealpanel.getActiveTab();
		console.log(acttabdealdetail.getXType());
		if(acttabdealdetail.getXType()=="tab-deal-detail"){
			var generaladddealdocgrid = context.down('dealdetailsformpanel').down('generaladddealdocpanel').down('grid');
			if (generaladddealdocgrid.getSelectionModel().hasSelection()) {
				var tmprec = generaladddealdocgrid.getSelectionModel().getSelection()[0];
	
				var docstempst = generaladddealdocgrid.getStore();
				docstempst.remove(tmprec);
			}
		}
    },
	
	onChangeClsfValue: function(fld , newValue , oldValue , eOpts){
		var me = this;
		me.onChangeClsfDesc(fld, "", eOpts);
	},
	
    onChangeClsfDesc: function(fld, e, eOpts) {
        console.log("Change Clsf Doc Item Save");
        var me = this;
		var activemenutab = fld.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();		
		//var context = fld.up('tab-deal-detail');

        var adddocform = context.down('generaladddealdocpanel').getForm();
        
		var nameval = adddocform.findField('name').getValue();
		var extval = adddocform.findField('ext').getValue();
		var fullname = nameval+"."+extval;
		
        var descval = adddocform.findField('desc').getValue();
        var clsfctnval = adddocform.findField('clsfctn').getValue();

        var docgrid = context.down('generaladddealdocpanel').down('grid');
        if(docgrid.getSelectionModel().hasSelection()){
            var row = docgrid.getSelectionModel().getSelection()[0];
            row.set('name',fullname,true);
            row.set('desc',descval,true);
            row.set('clsfctn',clsfctnval,true);
        }
    },

    setClassification: function(context) {
        var me = this;
        var viewtype = "", eastregion, backview="";
		if(context.xtype == "tab-deal-detail"){
			eastregion = context.down('dealdetailsformpanel').down('[region=east]');
	        backview = eastregion.config.backview;
		}

        //var clas_st = context.down('generaladddealdocpanel').down('combobox[name="clsfctn"]').getStore();
        //clas_st.removeAll();

        if(backview==="sendloi"){
            var tmpclsrec = Ext.create('DM2.model.Classification',{
                'selectionDesc' : 'LOI'
            });

            clas_st.add(tmpclsrec);
        } else if(backview==="SnCntToB" || backview==="SnCntToS" || backview==="SnBrCntToS" || backview==="SnBrCntToB" || backview==="ModiFN"){
            var tmpclsrec = Ext.create('DM2.model.Classification',{
                'selectionDesc' : backview
            });
            clas_st.add(tmpclsrec);
        } else if(backview==="application"){
            var tmpclsrec1 = Ext.create('DM2.model.Classification',{
                'selectionDesc' : 'Application'
            });
            var tmpclsrec2 = Ext.create('DM2.model.Classification',{
                'selectionDesc' : 'Signed Application'
            });
            clas_st.add(tmpclsrec1);
            clas_st.add(tmpclsrec2);
        } else if(backview==="reiterate"){
            var tmpclsrec1 = Ext.create('DM2.model.Classification',{
                'selectionDesc' : 'Reiterate'
            });
            var tmpclsrec2 = Ext.create('DM2.model.Classification',{
                'selectionDesc' : 'Signed Reiterate'
            });
            clas_st.add(tmpclsrec1);
            clas_st.add(tmpclsrec2);
        } else if(backview==="sendmodification"){
            var tmprec = context.down('dealdetailsformpanel').down('approvalpanel').config.approvalRec;
            var clsf   = context.down('dealdetailsformpanel').down('approvalpanel').config.clsf;
            cls = clsf+"."+tmprec.get('bank');

            var tmpclsrec1 = Ext.create('DM2.model.Classification',{
                'selectionDesc' : cls
            });
            clas_st.add(tmpclsrec1);
        }
    },
	
	getShortClassification: function(clsfctn){
		var me = this;
		if(clsfctn.length>10){
			clsfctn = clsfctn.substring(0, 10);
		}
		return clsfctn
	}
});