Ext.define('DM2.controller.DealController', {
    extend: 'Ext.app.Controller',
	
	requires:['DM2.view.DealMenuPropertyLoanPanel', 
			  'DM2.view.DealHistoryTab',
			  'DM2.view.WarningWindow',
			  'DM2.view.DealUserTab'],
    uses: ['DM2.view.component.tab.deal.Detail'],	
    refs: {
        dealmenugridpanel: {
            selector: 'dealmenugridpanel',
            xtype: 'dealmenugridpanel'
        },
        dealgridoptionmenu: {
            autoCreate: true,
            selector: 'dealgridoptionmenu',
            xtype: 'dealgridoptionmenu'
        },
        dealmenupanel: {
            autoCreate: true,
            selector: 'dealmenupanel',
            xtype: 'dealmenupanel'
        },
        dealitemmenu: {
            autoCreate: true,
            selector: 'dealitemmenu',
            xtype: 'dealitemmenu'
        },
        dealgrid: 'tab-deal-grid',
        dealgridview: 'tab-deal-grid #dealmenugridview',
        dealmenutabpanel: 'dealmenutabpanel',
        dealhistorygrid: 'dealhistorygrid',
		warningwindow : 'warningwindow'
    },

    init: function(application) {
        this.control({
            
            dealhistorygrid: {
                itemdblclick: {
                    fn: this.onDealHistoryGridItemDblClick
                }
            },
            'dealgrid': {
                select : this.onDealMenuGridpanelItemClick,
                itemdblclick: this.onDealMenuGridpanelItemDblClick,
                rowcontextmenu: this.onDealMenuGridPanelRightClick,
                afterrender : this.afterRenderDealMenuGrid,
                containercontextmenu: this.onDealMenuGridContainerRightClick,
                headercontextmenu: this.onDealMenuGridHeaderContainerRightClick/*,
				headerclick: this.onDealMenuGridHeaderClick*/
            },
            'dealgridview': {
                expandbody: this.onDealMenuGridViewExpandbody,
				itemlongpress: this.onDealMenuGridItemLongPress
            },
            'dealgridoptionmenu menuitem':{
                click: this.dealgridoptionitemclick
            },
            'dealgrid #dealmenufilterbtn': {
            // 'dealmenugridpanel #dealmenufilterbtn': {
                click: this.onDealMenuFilterBtnClick
            },
            'dealgrid #dealgridactionclmn':{
            // 'dealmenugridpanel #dealgridactionclmn':{
                headerclick: this.dealGridActionHeaderClick
            },
            'dealitemmenu':{
                click: this.dealGridItemMenuClick
            },
			'dealmenutabpanel #dealdocstab':{
				click: this.dealDocsTabClick
			},
			'dealmenutabpanel #dealdocstabmenu':{
				click: this.dealDocsTabMenuClick
			},
			'dealmenutabpanel':{
				afterrender: this.setUpTabPanel,
				beforetabchange: this.dealMenuTabChange
			},
			'dealmenupanel':{
				deactivate: this.dealMenuPanelDeActivate
			},
			'dealmenutabpanel #dealhistorytab':{
				click: this.dealHistoryTabClick/*,
				afterrender: this.afterDealHistoryTabRender*/
			},
			'#dealhistorytab menu':{
				click: this.dealHistoryTabMenuClick
			},
			'dealmenutabpanel #dealusertab':{
				click: this.dealUsersTabClick
			},
			'#dealusertab menu':{
				click: this.dealUsersTabMenuClick
			}
        });
    },
	
	setUpTabPanel: function() {
        var me = this,
		dealmenutabpanel = me.getDealmenutabpanel();
		/*dealmenutabpanel.tabBar.insert(1, {
				xtype:'splitbutton',
				iconCls: 'docbtn',
				text:'Documents',
				cls:'docbtncls',
				ui:'tabsplitbutton',
				itemId:'dealdocstab',
				menu:{
					itemId: 'dealdocstabmenu',
					items:[{
						//xtype: 'menucheckitem',
						group: 'opts',
						checked : true,
						text:'Deal Only'
					},{
						//xtype: 'menucheckitem',
						group: 'opts',
						checked: false,
						text:'All'
					}]
			}
		});*/
				
		//Insert Users Tab
		var dealuserstab = Ext.create('DM2.view.DealUserTab',{
                                itemId:'dealusertab',
								ui:'tabsplitbutton'
                            });
        dealmenutabpanel.tabBar.insert(4, dealuserstab);
		
		//Insert Deal History Tab
        var dealhistorytab = Ext.create('DM2.view.DealHistoryTab',{
                                itemId:'dealhistorytab',
								ui:'tabsplitbutton',
								style:'margin-left:5px'
                            });
        dealmenutabpanel.tabBar.insert(6, dealhistorytab);           
    },
	
	/*onDealMenuGridHeaderClick: function( ct, column, e, t, eOpts ){
		console.log(e.button);
		if(e.button == 2) {      
			console.log("cancel rightclick");						
			  e.preventDefault();            
			  e.stopPropagation();            
			  e.stopEvent();            
			  console.log("Fire rightclick");
			  //ct.fireEventArgs('headercontextmenu',arguments);
			  //e.stopEvent();
				//var menu = this.getDealmenugridpanel().headerCt.getMenu();
				//menu.showAt(e.getXY());
			  return false;
		}else{
			                        
		}
	},*/
	
    onDealMenuGridpanelItemClick: function(grid, record, index, eOpts) {
       
        console.log("Grid Item is clicked.So Load the Contacts,Docs & Notes.");
		var me = this,g;
		var context = grid.view.ownerCt.up('tab-deal-layout');	
		context.record = record;
		//context.dealid = record.get('dealid');
		context.dealid = record.get('idDeal');
        //this.getController('DocController').loadDocsStore(record.get('dealid'));
        try{
            g = grid.view.up();
        }
        catch(e) {
            g = grid.up('tab-deal-layout').down('tab-deal-grid');
        }
        finally {
			var readable = record.get('readable');
			if(readable==0){
				me.getController('DealController').dealPermissionNotAllowed();
			}else{
				me.getDealmenutabpanel().enable();
				var warwindow = me.getWarningwindow(); //me.getDealmenutabpanel().getEl().down('warningwindow'); 
				console.log("warwindow");
				console.log(warwindow);
				if(warwindow){
					warwindow.close();
				}
				
				me.getController('DashboardController').getMsgtext().setText("");
				me.getController('DashboardController').getMsgtext().setStyle({border:'0px'});
			
				me.loadRDealDetails(context);
				if(me.getDealmenutabpanel().down('#dealhistorytab')){
					if(me.getDealmenutabpanel().getActiveTab().xtype==="dealhistorygrid"){
						me.getController('DealHistoryController').onCategoryChange("Addressess", me.getDealmenutabpanel().down('#dealhistorytab').down('menu'),"deal");
					}
				}
			}
        }
    },

    onDealMenuGridViewExpandbody: function(rowNode, record, expandRow, eOpts) {
        console.log("Row Expanded.");
        var me = this;
		
		var readable = record.get('readable');
		var modifiable = record.get('modifiable');
		var dealid = record.get('idDeal');
		if(readable==0){
			return false;
			//me.getController('DealController').dealPermissionNotAllowed();
		}else{
			var targetId = 'propertygridrow-' + dealid;
			if(Ext.getCmp(targetId + '_grid')){
				Ext.getCmp(targetId + '_grid').destroy();
			}
			var nestedGrid = Ext.getCmp(targetId + '_grid');
			if (!nestedGrid) {
				nestedGrid = Ext.create('DM2.view.DealMenuPropertyLoanPanel', {
					renderTo: targetId,
					id: targetId + '_grid',
					cls:'dealmenupropertyloancls'
				});
				rowNode.grid = nestedGrid;
				// prevent bubbling of the events
				nestedGrid.getEl().swallowEvent([
					'mousedown', 'mouseup', 'click',
					'contextmenu', 'mouseover', 'mouseout',
					'dblclick', 'mousemove'
				]);
			}
			grids = nestedGrid.query('grid');
			console.log(grids);
			var grid1 = grids[0];
			var grid2 = grids[1];
			/*if(record.get('dealid')==162769){
				modifiable = 0;
			}else{
				modifiable = 1;
			}*/
			console.log("modifiable val"+modifiable);
			if(modifiable==0){
				grid2.hide();
			}else{
				grid2.show();
			}
			var properties_st = Ext.getStore('Properties');
			properties_st.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			properties_st.load({
				params:{
					filter :'dealid = '+dealid
				},
				scope:this,
				callback: function(records, operation, success) {
					//console.log(operation._response.responseText);
					var obj = Ext.decode(operation._response.responseText);
					//console.log(obj);				
					grid1.getStore().loadRawData(obj);
					grid2.getStore().loadRawData(obj);
				}
			});
		}
		
		/*var propertiesproxy = grid1.getStore().getProxy();
        propertiesproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        grid1.getStore().load({params:{filter :'dealid = '+record.get('dealid')}});
		
		var loanproxy = grid2.getStore().getProxy();
        loanproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        grid2.getStore().load({params:{filter :'dealid = '+record.get('dealid')}});*/
    },
	
	onDealHistoryGridItemDblClick: function(dataview, record, item, index, e, eOpts){
		var me = this;
		me.getDealmenutabpanel().enable();
		me.getController('DashboardController').getMsgtext().setText("");
		me.getController('DashboardController').getMsgtext().setStyle({border:'0px'});
		
		/*var tmprec = Ext.create("DM2.model.Deal",{
						idDeal : record.get('dealid'),
						dealName : record.get('name'),
						startdate : record.get('startdate'),
						closedate : record.get('closedate'),
						status    : record.get('status'),
						streetNumber : record.get('street_no'),
						streetName : record.get('street'),
						city : record.get('city'),
						state : record.get('state'),
						allPropertyNames : record.get('allPropertyNames'),
						userList : record.get('broker'),
						statusUserName : record.get('statussetby'),
						allContacts : record.get('allcontacts'),
						mainContact : record.get('mainContact'),
						loanAmt : record.get('loanamt'),
						loanDate : record.get('loandate'),
						quoteType : record.get('quotetype'),
						rateType : record.get('ratetype'),
						bankName : record.get('bank')
					 });*/
		var dealid = record.get('DM-dealNo');
		var magicDealID = record.get('LT-dealNo');
		record.set('dealid',dealid);
		record.set('idDeal',dealid);
		record.set('magicDealID',magicDealID);
		if(dataview.up('tab-deals')){
			me.showDealDetail(record, dataview.up('viewport').down('tab-deal-grid'));
		} else if(dataview.up('tab-sales')){
			me.getController('SalesController').showDealDetail(record, dataview.up('viewport').down('tab-sales-grid'));
		}
		//me.showDealDetail(tmprec, dataview);
	},
	
    onDealMenuGridpanelItemDblClick: function(dataview, record, item, index, e, eOpts) {
		var me = this;
		var readable = record.get('readable');
		if(readable==0){
			me.getController('DealController').dealPermissionNotAllowed();
			me.getController('DashboardController').getMsgtext().setStyle({
				fontWeight: 'bold',
				borderWidth: '1px',
				borderColor: '#ff0000',
				color : '#ff0000',
				borderStyle : 'dotted'
			});
			me.getController('DashboardController').getMsgtext().setText("No Permission is allowed for this deal.");
		} else {
			me.getDealmenutabpanel().enable();
			me.getController('DashboardController').getMsgtext().setText("");
			me.getController('DashboardController').getMsgtext().setStyle({border:'0px'});
	        this.showDealDetail(record, dataview);
		}
    },
	
	onDealMenuGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		console.log("Deal Grid Item Long Presss");
		console.log(record);
		var grid = view.up('tab-deal-grid');
		grid.getSelectionModel().select(record);
		me.showDealGridMenu(grid,record,e);
	},
	
	showDealGridMenu: function(grid,record,e){
		var me = this;
		e.stopEvent();
		console.log("Filtered"+grid.getStore().isFiltered());
        if(grid.getStore().isFiltered()){
            console.log("Show Menu for Remove All Filters");
            if(this.getDealgridoptionmenu()){
                this.getDealgridoptionmenu().showAt(e.getXY());
            }
        }
        var menu = this.getDealitemmenu();
        menu.showAt(e.getXY());
		var menuitems = menu.items.items;
		console.log(menuitems);
		var readable = record.get('readable');
		console.log("readable : "+readable);
		if(readable===0){						
			for(var i=0;i<menuitems.length;i++){
				if(menuitems[i].modifyMenu === "yes"){
					menuitems[i].disable();
				}
			}
		} else {
			for(var i=0;i<menuitems.length;i++){
				if(menuitems[i].modifyMenu === "yes"){
					menuitems[i].enable();
				}
			}
		}
		if(record.get('status')!="CL"){
			menu.down('#createnewdeal').disable();
		} else {
			if(record.get('covered') && record.get('covered')==1){
				menu.down('#createnewdeal').disable();
			}
		}
	},
	
    onDealMenuGridPanelRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        console.log("Deal Grid Right Click");
		var me = this;
        //e.stopEvent();
		me.showDealGridMenu(grid, record,e);		
    },

    dealgridoptionitemclick: function(item, e, eOpts) {
        console.log("Remove All the FIlters");
        var me = this;
        //console.log(me.getDealmenugridpanel().filters);
        me.getDealmenugridpanel().filters.clearFilters();
        //me.getDealmenugridpanel().getStore().clearFilter();
    },
	
    afterRenderDealMenuGrid: function(dealgrid) {
        var me = this;
		me.getController('MainController').getGridKeyData(dealgrid,'deal');
		me.getController('DealController').loadAllDeals();
        // var menu = me.getDealmenugridpanel().headerCt.getMenu();
        var menu = dealgrid.headerCt.getMenu();
        //console.log("After dealmanu grid render");
        //menu.child('#ascItem').hide();
        //menu.child('#descItem').hide();
        //menu.child('#columnItemSeparator').hide();
        //var cl5 = me.getDealgrid().columns[5];
        //cl5.menuDisabled = false;
        //var cl0 = me.getDealgrid().columns[0];
        //cl0.menuDisabled = false;
        /*menu.add({
            text: 'Remove All Filters',
            handler: function() {
                console.log("Remove All FIlter From the Grid");
                //me.getDealmenugridpanel().getStore().filters = [];
                me.getDealmenugridpanel().filters.clearFilters();
            }
        },{
            text: 'Add Search Line',
            itemId:'addline',
            handler: function(item) {
                console.log("Add Search Line into the Grid");
                var searchlinearr = me.getDealmenugridpanel().query('textfield[action="searchline"]');

                for(var j=0;j<searchlinearr.length;j++){
                    searchlinearr[j].show();
                }
                item.hide();
                menu.down("#removeline").show();
            }
        },{
            text: 'Remove Search Line',
            hidden:true,
            itemId:'removeline',
            handler: function(item) {
                console.log("Remove Search Line From the Grid");
                //me.getDealmenugridpanel().getStore().filters = [];
                var searchlinearr = me.getDealmenugridpanel().query('textfield[action="searchline"]');
                console.log(searchlinearr);
                for(var j=0;j<searchlinearr.length;j++){
                    if(searchlinearr[j].getValue()!==''){
                        searchlinearr[j].setValue('');
                        searchlinearr[j].setFilter(searchlinearr[j].up().dataIndex, '');
                    }
                    searchlinearr[j].hide();
                }
                me.getDealmenugridpanel().filters.clearFilters();
                item.hide();
                menu.down("#addline").show();
            }
        });*/
        // menu.activeHeader = me.getDealmenugridpanel().headerCt.getHeaderAtIndex(1);
        menu.activeHeader = dealgrid.headerCt.getHeaderAtIndex(1);
		dealgrid.store.on( 'load', function( store, records, options ) {
			console.log("Store Cnt : "+store.getCount());
			console.log("Store Total Cnt : "+store.totalCount);
			if(store.getCount()==0){
				// Clear Deal Tabs Store Data
				dealgrid.getSelectionModel().deselectAll();
				var context = dealgrid.up('tab-deal-layout')
				me.getController('NoteController').clearDealNotes(context);
				me.getController('ContactController').clearDealContactGrid(context);
				me.getController('DocController').clearDealDocs(context);
				me.getController('DealUserController').clearDealUsers(context);
				me.getController('DealHistoryController').clearDealHistory(context);				
			} else {
				// Select first item if wants
			}
		});
    },

    loadAllDeals: function() {
        var me = this;		
		var deal_st = Ext.getStore('Deals');
        var dealsproxy = deal_st.getProxy();
        /*dealsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var dealsproxyCfg = dealsproxy.config;
        dealsproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		deal_st.setProxy(dealsproxyCfg);
        deal_st.load({
            scope: this,
            callback: function(records, operation, success) {
                // the operation object
                // contains all of the details of the load operation
                if(!success){
                    me.getController('MainController').getMainviewport().getLayout().setActiveItem(me.getController('MainController').getLogincontainer());
                    Ext.util.Cookies.clear("apikey-"+DM2.view.AppConstants.apiurl);
                } else {
                    if(records.length > 0){
                        // CG Why this?
                        me.getDealgrid().getSelectionModel().select(0);
                    }
                }
            }
        });
    },
    detailClass: 'DM2.view.component.tab.deal.Detail',
    counter: 1,
    showDealDetail: function(record, dataview) {
        
        var me = this;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
        var baseTabPanel = dataview.up('tab-basecardpanel');
        var tabLength = baseTabPanel.items.length;
        var tabItems = baseTabPanel.items.items;
        var tabItem;
        var createPanel = false;
        var x;
        //var name = record.data.name;
		var name = record.data.dealName;
        
        
        if(tabLength < 2) {
            createPanel = true;
        } else {
            for(x = 1; x < tabLength; x++) {
                tabItem = tabItems[x];
                if(tabItem.dealid == dealid) {
                    createPanel = false;
                    break;
                } else {
                    createPanel = true;
                }
            }
        }
        
        if(createPanel == true) {
			var cleanName = '';
			if(name){
				cleanName = name.replace(/\s\s+/g, ' ');
			}
			var title;
			if(cleanName.length < 1) {
			    title = dealid  
			} else {
			    title = cleanName.substring(0,10);
			}
			tabItem = Ext.create(me.detailClass, {
				title: title,
				html: title,
				dealid: dealid,
				record: record,
				iconCls: 'readonlybtn',
				iconAlign:'right'
			});
			baseTabPanel.add(tabItem);
			me.loadRDealDetailsNewTab(tabItem,baseTabPanel);			                
        }
		baseTabPanel.setActiveItem(tabItem);
		////////
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var tabdealgrid = tabdealpanel.down('tab-deal-grid');
		console.log("rightMenuClick");
		console.log(tabdealgrid.rightMenuClick);
		if(tabdealgrid.rightMenuClick=="createnewdealfrom"){
			tabdealgrid.rightMenuClick = null;
			//var acttabdealdetail = tabdealpanel.getActiveTab();
			//console.log("acttabdealdetail");
			//console.log(tabItem);
			me.getController('CreateNewDealController').CreateNewDealClick(25,'Create New Deal','CreateNewDealClick',tabItem);
		}
		////////
    },
	
	loadRDealDetailsNewTab: function(tabItem,baseTabPanel){
		var me = this;
		console.log("Load RdealDetails Once From New Tab");
		console.log(tabItem.dealid);
		//var dealdetailform = tabItem.down('dealdetailsformpanel');
        //dealdetailform.setLoading(true);
		//tabItem.setLoading(true);
		var dealDetailStore = Ext.create('DM2.store.DealDetails',{
			storeId: 'DealDetails-'+tabItem.dealid
		});
		tabItem.dealDetailStore = dealDetailStore;
		
		dealDetailStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		dealDetailStore.load({
			scope: this,
			params: {
				'filter' : 'idDeal = '+tabItem.dealid
			},
			callback: function(records, operation, success) {
				//tabItem.setLoading(false);
				//dealdetailform.setLoading(false);
				console.log("Load RDealDetails Callback.");
				if(success){					
					console.log("Permission");
					console.log(records[0].data.permission.modifiable);
					var dealTabBar = baseTabPanel.getTabBar();
					console.log(dealTabBar.items.items);
					var dealTabs = dealTabBar.items.items;
					var activeDealTab = dealTabs[dealTabs.length-1];
					
					if(records[0].data.permission.modifiable!=0){
						console.log("Added button : removeReadOnlyBtn");
						activeDealTab.addCls('removeReadOnlyBtn');
					}else{
						activeDealTab.removeCls('removeReadOnlyBtn');
					}
					me.afterRDealDetailsLoad(tabItem);
				}
			}
		});
	},
	
	afterRDealDetailsLoad: function(tabItem){
		var me = this;	
		tabItem.isRDealDetailsLoaded = 'yes';
		tabItem.dealPropertyData = tabItem.dealDetailStore.data.items[0].data.PropertiesPerDeal;
		tabItem.dealContactData = tabItem.dealDetailStore.data.items[0].data.ContactPerDeal;
		tabItem.dealDocsData = tabItem.dealDetailStore.data.items[0].data.DocsPerDeal;
		tabItem.dealUsersData = tabItem.dealDetailStore.data.items[0].data.UsersPerDeal;
		if(tabItem.xtype=="tab-deal-layout") {
			me.getController('NoteController').bindDealNotes(tabItem);
			me.getController('ContactController').bindDealContactGrid(tabItem);
			me.getController('DocController').bindDealDocs(tabItem);
			me.getController('DealUserController').bindDealUsers(tabItem);
		} else {
			me.getController('DealDetailController').bindDealPrimaryDetails(tabItem);
			me.getController('DealDetailController').bindPropertyDetails(tabItem);
			me.getController('DealDetailController').bindNewLoanDetails(tabItem);
			me.getController('DealDetailController').bindExistingLoanDetails(tabItem);
			me.getController('DealDetailController').bindMasterLoanDetails(tabItem);
			me.getController('DealDetailController').bindContactDetails(tabItem);	
			me.getController('DealDetailController').bindActivityDetails(tabItem);
			me.getController('NoteController').bindDealNotes(tabItem);
			me.getController('DocController').bindDealDocs(tabItem);
		}
		me.getController('CreateNewDealController').bindCreateNewDealViewInfo(tabItem);
	},
	
	loadRDealDetails: function(tabItem){
		var me = this;
		console.log("Load RdealDetails Once");
		//var dealdetailform = tabItem.down('dealdetailsformpanel');
        //dealdetailform.setLoading(true);
		//tabItem.setLoading(true);
		var dealDetailStore = Ext.create('DM2.store.DealDetails',{
			storeId: 'DealDetails-'+tabItem.dealid
		});
		tabItem.dealDetailStore = dealDetailStore;
		
		dealDetailStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		dealDetailStore.load({
			scope: this,
			params: {
				'filter' : 'idDeal = '+tabItem.dealid
			},
			callback: function(records, operation, success) {
				//tabItem.setLoading(false);
				//dealdetailform.setLoading(false);
				console.log("Load RDealDetails Callback.");
				if(success){
					me.afterRDealDetailsLoad(tabItem);
				}
			}
		});
	},
	
	/**
	 * On Permission Not allowed
	 */
	dealPermissionNotAllowed: function(){
		console.log("Permission is not allowed for this deal.");
		var me = this;
		var warwindow = me.getWarningwindow();
		if(warwindow){
			warwindow.close();
		}
		var warwindow = Ext.create('DM2.view.WarningWindow',{
			renderTo : me.getDealmenutabpanel().getEl(),
			closable : false,
			closeAction : 'destroy'
		});
		warwindow.show();
		me.getDealmenutabpanel().disable();
	},

    onDealMenuGridContainerRightClick: function(view, e, eOpts) {
        console.log("Deal Grid container Right Click");
        e.stopEvent();
        var menu = this.getDealmenugridpanel().headerCt.getMenu();
        menu.showAt(e.getXY());
    },

    onDealMenuGridHeaderContainerRightClick: function(ct, column, e, t, eOpts) {
        console.log("Deal Grid header container Right Click");
        e.stopEvent();
        var menu = this.getDealgrid().headerCt.getMenu();
        menu.showAt(e.getXY());
    },

    onDealMenuFilterBtnClick: function(btn, e, eOpts) {
        var me = this;
        var searchlinearr = me.getDealmenugridpanel().query('textfield[action="searchline"]');
        if(btn.getText()==="Show"){
            btn.setText("Hide");
            for(var j=0;j<searchlinearr.length;j++){
                searchlinearr[j].show();
            }
        }
        else if(btn.getText()==="Hide"){
            btn.setText("Show");
            for(var j=0;j<searchlinearr.length;j++){
                if(searchlinearr[j].getValue()!==''){
                    searchlinearr[j].setValue('');
                    searchlinearr[j].setFilter(searchlinearr[j].up().dataIndex, '');
                }
                searchlinearr[j].hide();
            }
            me.getDealmenugridpanel().filters.clearFilters();
        }
    },

    dealGridActionHeaderClick: function(ct, column, e, t, eOpts) {
        console.log("Deal Grid Action Header CLick");
        var me = this;
        var searchlinearr = me.getDealmenugridpanel().query('textfield[action="searchline"]');
        console.log(column.config.action);
        var actionval = column.config.action;
        if(actionval==="Show"){
            column.setText('<span data-qtip="hide"><img src="resources/images/hide.png" style="margin-top: 23px;"/></span');
            column.config.action = "Hide";
            for(var j=0;j<searchlinearr.length;j++){
                searchlinearr[j].show();
            }
        }
        else if(actionval==="Hide"){
            column.setText('<span data-qtip="show"><img src="resources/images/show.png"  /></span>');
            column.config.action = "Show";
            for(var j=0;j<searchlinearr.length;j++){
                if(searchlinearr[j].getValue()!==''){
                    searchlinearr[j].setValue('');
                    searchlinearr[j].setFilter(searchlinearr[j].up().dataIndex, '');
                }
                searchlinearr[j].hide();
            }
            me.getDealmenugridpanel().filters.clearFilters();
        }
    },

    dealGridItemMenuClick: function(menu, item, e, eOpts) {
        console.log("GeneralDoc Menu Clicked");
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var tabdealgrid = tabdealpanel.down('tab-deal-grid');
		
        var text = item.getItemId();
        if(text === "opendeal"){
            if (tabdealgrid.getSelectionModel().hasSelection()) {
                var record = tabdealgrid.getSelectionModel().getSelection()[0];

                me.showDealDetail(record,tabdealgrid);
            }
        } else if(text === "printdeal"){
            //me.showGeneralTemplateSelGridPanel();
			if (tabdealgrid.getSelectionModel().hasSelection()) {
                var record = tabdealgrid.getSelectionModel().getSelection()[0];
				//var dealid = record.get('dealid');
				var dealid = record.get('idDeal');
            }
			var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
			var apikey = Ext.util.Cookies.get("apikey-"+DM2.view.AppConstants.apiurl);
			window.open("./resources/printSnapshot.php?apikey="+apikey+"&username="+userid+"&dealid="+dealid+"&apiurl="+DM2.view.AppConstants.apiurl,"_blank");
        } else if(text === "createnewdeal"){
            if (tabdealgrid.getSelectionModel().hasSelection()) {
                var record = tabdealgrid.getSelectionModel().getSelection()[0];
				
				tabdealgrid.rightMenuClick = "createnewdealfrom";
				
                me.showDealDetail(record,tabdealgrid);
            }
        } else if(text === "refreshdeal"){
			me.getController("DealController").loadAllDeals(); //Load All Deals
		}
    },
	
	dealDocsTabClick: function() {
		console.log("Set Document Tab");
		var me = this;
		me.getDealmenutabpanel().setActiveTab(1);
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabpanel.getActiveTab();
		var dealdocsgridpanel = context.down('dealdocsgridpanel');
		me.getController('DocController').loadDocsPanelStore(dealdocsgridpanel);
		
		/*var dealmenugridpanel = me.getDealgrid();
		if (dealmenugridpanel.getSelectionModel().hasSelection()) {
			var record = dealmenugridpanel.getSelectionModel().getSelection()[0];			
			me.getController('DocController').loadDocsStore(record.get('idDeal'));
		}*/		
	},
	
	dealDocsTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Deal Docs Menu Clicked");
        var me = this;
        var text = item.text;
		me.getDealmenutabpanel().setActiveTab(1);
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabpanel.getActiveTab();
		var dealdocsgridpanel = context.down('dealdocsgridpanel');
        if(text === "All"){
			console.log("Show All Docs");
            me.getController('DocController').loadAllDocsPerDealPanelStore(dealdocsgridpanel);		
        } else if(text === "Deal Only"){
			console.log("Show Deal Only Docs");
            me.getController('DocController').loadDocsPanelStore(dealdocsgridpanel);
        }
	},
	
	dealMenuTabChange: function(tabPanel, newCard, oldCard, eOpts){
		console.log("Deal Menu Tab Change");
		var me = this;
		//var dealdocstab = me.getDealmenutabpanel().down('#dealdocstab');
		var dealhistorytab = me.getDealmenutabpanel().down('#dealhistorytab');
		var dealusertab = me.getDealmenutabpanel().down('#dealusertab');
		
		/*if(newCard.getItemId()==="dealdocsgridpanel"){
			dealdocstab.addCls("docactivebtncls");
			dealhistorytab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
		}else*/ if(newCard.getItemId()==="dealhistorygrid"){
			dealhistorytab.addCls("docactivebtncls");
			//dealdocstab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
		} else if(newCard.xtype==="dealdetailusergridpanel"){
			dealusertab.addCls("docactivebtncls");
			dealhistorytab.removeCls("docactivebtncls");
			//dealdocstab.removeCls("docactivebtncls");
		} else {
			dealhistorytab.removeCls("docactivebtncls");
			//dealdocstab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
		}
	},
	
	dealMenuPanelDeActivate: function(){
		var me = this;
	},
	
	dealHistoryTabClick: function(btn) {
		console.log("Set Deal History Tab");
		var me = this;
		me.getDealmenutabpanel().setActiveTab(6);	
		me.getController('DealHistoryController').onCategoryChange("Addressess", btn.down('menu'),"deal");
	},
	
	dealHistoryTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Deal History Menu Clicked");
        var me = this;
        var text = item.text;
		me.getDealmenutabpanel().setActiveTab(6);
		me.getController('DealHistoryController').onCategoryChange(text, menu, "deal");			
	},
	dealUsersTabClick: function(){
		console.log("Deal Users Tab Click");
		var me = this;
		me.getDealmenutabpanel().setActiveTab(4);
		// var dealmenugridpanel = me.getDealmenugridpanel();
		var dealmenugridpanel = me.getDealgrid();
		if (dealmenugridpanel.getSelectionModel().hasSelection()) {
			var record = dealmenugridpanel.getSelectionModel().getSelection()[0];
			
			var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
			var context = tabdealpanel.getActiveTab();
			var usergrid = context.down('dealdetailusergridpanel');
			me.getController('DealUserController').loadDealUserDetailsWithParams('active',record.get('idDeal'),usergrid);
			usergrid.config.alloractiveusermode = "active";
		}		
	},
	
	dealUsersTabMenuClick: function(menu, item, e, eOpts){
		console.log("Deal Users Tab Menu Click");
		var me = this;
		var text = item.text;
		me.getDealmenutabpanel().setActiveTab(4);
		var dealgrid = me.getDealgrid();
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var usergrid = context.down('dealdetailusergridpanel');
		
        if(text === "All"){
			console.log("Show All Docs");
            if (dealgrid.getSelectionModel().hasSelection()) {
                var record = dealgrid.getSelectionModel().getSelection()[0];
                me.getController('DealUserController').loadDealUserDetailsWithParams('all',record.get('idDeal'),usergrid);
				usergrid.config.alloractiveusermode = "all";
            }else{
				return false;
			}			
        }else if(text === "Active Only"){
			console.log("Show Active Users Only");
            if (dealgrid.getSelectionModel().hasSelection()) {
				var record = dealgrid.getSelectionModel().getSelection()[0];
				me.getController('DealUserController').loadDealUserDetailsWithParams('active',record.get('idDeal'),usergrid);
				usergrid.config.alloractiveusermode = "active";
			}else{
				return false;
			}			
        }
	}
});