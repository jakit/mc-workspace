Ext.define('DM2.controller.BillingController', {
    extend: 'Ext.app.Controller',
	
	requires:[
		'DM2.view.BillingPanel'
	],
	
    refs: {
        billingpanel: {
            autoCreate: true,
            selector: 'billingpanel',
            xtype: 'billingpanel'
        }
    },

    init: function(application) {
        this.control({
            'billingpanel':{
                close: this.billingPanelCloseClick,
				afterrender: 'loadBillingPanelDetails'
            }
        });
    },
	
	BillingClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this,billingpanel;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = tabdealdetail.down('[region=east]');
		if(detailpanel.down('billingpanel')){
			console.log("Use Old Panel");
			billingpanel = detailpanel.down('billingpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	billingpanel = Ext.create('DM2.view.BillingPanel');//me.getClosingview();
		}
        eastregion.getLayout().setActiveItem(billingpanel);
		billingpanel.setTitle("Activities > "+activitytext);
        //closingview.down('#activityid').setValue(activityid);
    },
	
	loadBillingPanelDetails: function(panel){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var record = context.record;
        var dealid = record.data.idDeal;
		/*
		var loancombo = panel.down('#loancombo');
		var existingLoans_st = loancombo.getStore();//('ExistingLoans');
		if(existingLoans_st.isLoaded()){
			var existingLoanscnt = existingLoans_st.getCount();
			if(existingLoanscnt>0){
				//Get the First Record
				var loanrec = existingLoans_st.getAt(0);           
				loancombo.select(loanrec);
				loancombo.fireEvent('select',loancombo,loanrec);
			}
		}else {	
				existingLoans_st.getProxy().setHeaders({
					Accept: 'application/json',
					Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
				});
				existingLoans_st.load({
					params:{
						filter :'dealid = '+dealid
					},
					callback: function(records, operation, success){        
						var existingLoanscnt = existingLoans_st.getCount();
						if(existingLoanscnt>0){
							//Get the First Record
							var loanrec = existingLoans_st.getAt(0);           
							loancombo.select(loanrec);
							loancombo.fireEvent('select',loancombo,loanrec);
						}
					}
				});
		}
		
		var cvpropertycombo = panel.down('#propertyfrm').down('#cvpropertycombo');
		var dealprptyst = cvpropertycombo.getStore();        
        dealprptyst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealprptyst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				var prpcnt = dealprptyst.getCount();
				if(prpcnt>0){
					//Get the First Record
					var prprec = dealprptyst.getAt(0);           
					cvpropertycombo.select(prprec);
					cvpropertycombo.fireEvent('select',cvpropertycombo,prprec);
				}
            }
        });*/
        //me.loadClosingActions();
	},
	
    billingPanelCloseClick: function(panel) {
		var me = this;
		console.log("Billing Panel Close");
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    onChangeLoans: function(combo, rec, eOpts) {
        var me = this;
        console.log("Fire select event");
		var context = me.getController('DealDetailController').getTabContext(combo);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var loandtfrm = detailpanel.down('closingview').down('#closingloanfrm');
        loandtfrm.getForm().loadRecord(rec);
    },

    onPropertyChange: function(combo, rec, eOpts) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(combo);
        console.log("Fire select event");
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var propertyfrm = detailpanel.down('closingview').down('#propertyfrm');
        propertyfrm.getForm().loadRecord(rec);
    }
});