Ext.define('DM2.controller.PropertyController', {
    extend: 'Ext.app.Controller',
	
	requires: [
		'DM2.view.PropertyDocOptionMenu',
		'DM2.view.PropertyContactOptionMenu',
		'DM2.view.PropertyMasterGridMenu',
		'DM2.view.CreateNewDealFromPropertyPanel',
		'DM2.view.component.tab.property.Layout',
		'DM2.view.AddNewMasterPropertyView'
	],
    refs: {
        propertygridpanel: {
            autoCreate: true,
            selector: 'propertygridpanel',
            xtype: 'propertygridpanel'
        },
        propertygmapwindow: {
            autoCreate: true,
            selector: 'propertygmapwindow',
            xtype: 'propertygmapwindow'
        },
        propertyzillowwindow: {
            autoCreate: true,
            selector: 'propertyzillowwindow',
            xtype: 'propertyzillowwindow'
        },
		propertygrid: 'tab-property-grid',
		tabpropertylayout : 'tab-property-layout',
		propertymenutabpanel : 'propertymenutabpanel',
		propertydocoptionmenu: {
            autoCreate: true,
            selector: 'propertydocoptionmenu',
            xtype: 'propertydocoptionmenu'
        },
		propertydealhistorygrid:'#propertydealhistorygrid',
		propertysalehistorygrid:'#propertysalehistorygrid',
		propertycontactoptionmenu: {
			autoCreate: true,
            selector: 'propertycontactoptionmenu',
            xtype: 'propertycontactoptionmenu'
		},
		propertymastergridmenu: {
			autoCreate: true,
            selector: 'propertymastergridmenu',
            xtype: 'propertymastergridmenu'
		},
		addnewmasterpropertyview :'addnewmasterpropertyview'
    },
	init: function(application) {
        this.control({
			'propertygrid': {
				afterrender : this.afterRenderPropertyMenuGrid,
                select: this.loadPropMasterDetailForm,
				celldblclick: this.onPropertyGridItemDblClick,
                rowcontextmenu: this.onPropMasterGridPanelRightClick,
				headercontextmenu: this.onPropertyGridHeaderContainerRightClick
            },
			'tab-property-grid dataview': {
				itemlongpress: this.onPropMasterGridItemLongPress
            },
            'propertygmapwindow #gmapContiner':{
                afterrender: 'onGmapContainerAfterRender'
            },
            'propertyzillowwindow #zillowContiner':{
                afterrender: 'onZillowContainerAfterRender'
            },
			'propertymenutabpanel':{
				afterrender: this.propertyTabPanelAfterRender,
				beforetabchange: this.propertyTabChange
			},
			'propertymenutabpanel #dealhistorypropertytab':{
				click: this.dealHistoryPropertyTabClick
			},
			'#dealhistorypropertytab menu':{
				click: this.dealHistoryPropertyTabMenuClick
			},
			'propertymenutabpanel #salehistorypropertytab':{
				click: this.saleHistoryPropertyTabClick
			},
			'#salehistorypropertytab menu':{
				click: this.saleHistoryPropertyTabMenuClick
			},			
			'propertydocsgridpanel':{
				rowcontextmenu: this.onPropDocsGridPanelRightClick,
				select : this.onPropertyDocGridItemSelect,
				beforeitemmousedown: this.onPropertyDocGridItemMouseDown,
				load: this.onPropDocsGridLoad,
				itemclick : this.onPropertyDocGridItemClick
				//itemdblclick : this.onPropertyDocGridItemDbClick				
			},
			'propertydocsgridpanel dataview': {
                itemlongpress: this.onPropDocsGridItemLongPress
            },
			'propertydocsgridpanel button[action="adddocbtn"]':{
				click: this.showRecvDealDocs				
			},
			'propertydocoptionmenu menuitem':{
                click: this.propertyDocOptionItemClick
            },
			'propertydealhistorygrid': {
                itemdblclick: this.onPropertyDealHistoryGridItemDblClick
            },
			'propertysalehistorygrid': {
                itemdblclick: this.onPropertySaleHistoryGridItemDblClick
            },			
			'propertycontactsgridpanel button[action="addpropertycontactbtn"]':{
				click: this.showAddContactView
			},
			'propertycontactsgridpanel':{
				rowcontextmenu: this.onPropContactsGridPanelRightClick
			},
			'propertycontactsgridpanel dataview': {
                itemlongpress: this.onPropContactsGridItemLongPress
            },
			'propertycontactoptionmenu menuitem':{
                click: this.propertyContactOptionItemClick
            },
			'propertymastergridmenu #researchmenu':{
                click: this.propertyMasterGridReaearchMenuClick
            },
			'propertymastergridmenu menuitem':{
                click: this.propertiesMasterGridOptionItemClick
            },
			'addnewmasterpropertyview':{
                close:this.addNewMasterPropertyViewClose
            },
			'addnewmasterpropertyview button[action="createnewpropertybtn"]':{
                click:this.createNewPropertyBtnClick
            }
        });
    },
	
	afterRenderPropertyMenuGrid: function(propgrid) {
        var me = this;
		console.log("After Property Grid Render & Activate");
		if(propgrid.up('tab-property-layout').filterTab && propgrid.up('tab-property-layout').filterTab==true){
			console.log("Dont load");
		} else {
			//me.setTabPanel(btn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-properties'));
			if(propgrid.firstLoad){
				me.getController('PropertyController').loadAllPropMaster(propgrid);
				propgrid.firstLoad = false;
			}
		}
		propgrid.store.on( 'load', function( store, records, options ) {
			if(store.getCount()==0){
				// Clear Deal Tabs Store Data
				propgrid.getSelectionModel().deselectAll();
				var context = propgrid.up('tab-property-layout');
				propmasterdetailsform = context.down('propmasterdetailsform');
				if(propmasterdetailsform){
					propmasterdetailsform.getForm().reset();
				}
				me.getController('PropertyController').clearPropertyContactGrid(context);
				me.getController('PropertyController').clearPropertyDocsGrid(context);
				me.getController('PropertyController').clearPropertyDealHistory(context);
			} else {
				// Select first item if wants
			}
		});
	},
	
	onPropMasterGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		console.log("Property Grid Item Long Presss");
		var grid = view.up('tab-property-grid');
		grid.getSelectionModel().select(record);
		me.showPropertyGridMenu(grid,record,e);
	},
	
	showPropertyGridMenu: function(grid,record,e){
		var me = this;
		e.stopEvent();
		console.log("Show Menu for Property Master Grid");
		if(this.getPropertymastergridmenu()){
			this.getPropertymastergridmenu().showAt(e.getXY());
			var prpmenu = this.getPropertymastergridmenu();
			var menuFunc = me.getController('MainController').getMenuStatus("salemenu");
			if(menuFunc==1){
				prpmenu.down('menuitem[action="createnewsaledeal"]').show();
			} else {
				prpmenu.down('menuitem[action="createnewsaledeal"]').hide();
			}
			var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
			if(menuFunc==1){
				prpmenu.down('menuitem[action="createnewdeal"]').show();
			} else {
				prpmenu.down('menuitem[action="createnewdeal"]').hide();
			}
			if((record.get('lastDealID')==null) || (record.get('lastDealID')!=null && record.get('lastDealStatus')=='CL')){
				prpmenu.down('menuitem[action="createnewdeal"]').enable();
			} else if(record.get('lastDealID')!=null && record.get('lastDealStatus')!='CL'){
				prpmenu.down('menuitem[action="createnewdeal"]').disable();
			}
		}
	},
	
	onPropMasterGridPanelRightClick: function(grid, record, tr, rowIndex, e, eOpts){
		var me = this;
		me.showPropertyGridMenu(grid,record,e);
	},
	
	propertyMasterGridReaearchMenuClick: function(menu, item, e, eOpts){
		var me = this,rec,blockval,lotval,boroval,stateVal,streetVal,cityVal,streetnoVal,zipVal;
		console.log("Research Menu Clicked");
		var tabpropertypanel = me.getController('MainController').getMainviewport().down('tab-properties');
		var context = tabpropertypanel.getActiveTab();
		
		var propertygrid = context.down('tab-property-grid');
		if(propertygrid.getSelectionModel().hasSelection()){
			rec = propertygrid.getSelectionModel().getSelection()[0];
			blockval  = rec.get('Block');
			lotval    = rec.get('Lot');
			boroval   = rec.get('Boro');
			stateVal    = rec.get('State');
			streetVal   = rec.get('StreetName');
			cityVal     = rec.get('Township');
			streetnoVal = rec.get('StreetNumber');
			zipVal     = rec.get('ZipCode');
		}
		
        if(item.text==="Gmap") {
            //this.getController('PropertyController').showOnGmap();
			var qst = streetnoVal+"+"+streetVal+"+"+cityVal+"+"+stateVal;
			window.open("http://maps.google.com?q="+qst);
        } else if(item.text==="Zillow") {
            //this.getController('PropertyController').showOnZillow();
			me.openZillowLink(streetnoVal,streetVal,cityVal,stateVal,zipVal);
        } else if(item.text==="Reonomy") {
            console.log("Open the reonomy link on new page");
            me.getController('DealDetailController').openReonomyLink(boroval,blockval,lotval);
        } else if(item.text==="Oasis") {
            console.log("Open the Oasis link on new page");
			me.getController('PropertyController').displayOasisMap(boroval,blockval,lotval);
        }
	},
	
	displayOasisMap: function(borough, block, lot) {
		switch(borough){
		  case "B": borough = 20; break;
		  //case "B": borough = 30; break;
		  case "Q": borough = 40; break;
		  case "S": borough = 50; break;
		  case "M":
		  default: borough = 10; break;
		}
		block = ('0000'+block).slice(-4);
		lot = ('0000'+lot).slice(-4);
		//alert("Borough: "+borough+" Block: "+block+" Lot: "+lot);
		var url = "http://www.oasisnyc.net/map.aspx?zoomto=lot:"+borough+block+lot;
		window.open(url, "_blank");
	},
	
	propertiesMasterGridOptionItemClick: function(item, e, eOpts){
		var me = this;
		console.log("Property Grid Menu Item Clicked.");
		if(item.action==="listacrisdocuments"){
            console.log("List ACRIS Documents Menu Item Clicked");
            me.listACRISDocumentsItemClick();
        } else if(item.action==="createnewdeal"){
			me.getController('CreateNewDealFromPropertyController').CreateNewDealClick();			
		} else if(item.action==="createnewsaledeal"){
			me.getController('CreateNewDealFromPropertyController').CreateNewSalesDealClick("propertygrid");			
		} else if(item.action==="addnewproperty"){
			me.showAddNewPropertyView();
		} else if(item.action==="editproperty"){
			me.showEditPropertyView();
		}
	},
		
	listACRISDocumentsItemClick: function(){
		var me = this,blockval,lotval,boroval,rec;
		var tabpropertypanel = me.getController('MainController').getMainviewport().down('tab-properties');
		var context = tabpropertypanel.getActiveTab();
		
		var eastregion = context.down('#property-east-panel');
		
		var listacrisdocuments;
		if(context.down('#property-east-panel').down('listacrisdocuments')){
			console.log("Use Old Panel");
			listacrisdocuments = context.down('#property-east-panel').down('listacrisdocuments');
		}
		else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	listacrisdocuments = Ext.create('DM2.view.ListACRISDocuments');
		}
   		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(listacrisdocuments);	
		eastregion.setTitle("List ACRIS Documents");
		eastregion.getHeader().hide();
		
		eastregion.config.backview = "propertydetails";
		listacrisdocuments.config.backview = "propertydetails";
				
		var propertygrid = context.down('tab-property-grid');
		if(propertygrid.getSelectionModel().hasSelection()){
			rec = propertygrid.getSelectionModel().getSelection()[0];
			blockval  = rec.get('Block');
			lotval    = rec.get('Lot');
			boroval   = rec.get('Boro');
		}

		var acrispropertydocsgrid = listacrisdocuments.down('acrispropertydocsgrid');
		me.getController('DealDetailController').loadAcrisDocumentsbyAPN(acrispropertydocsgrid,boroval,blockval,lotval);
	},
	
	onPropertyDealHistoryGridItemDblClick: function(dataview, record, item, index, e, eOpts){
		var me = this;
		var dealsbtn = dataview.up('viewport').down('#dealsbtn');
		me.getController('DashboardController').setTabPanel(dealsbtn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-deals'));
		
		var dealid = record.get('DM-dealNo');
		var magicDealID = record.get('LT-dealNo');
		record.set('dealid',dealid);
		record.set('idDeal',dealid);
		record.set('magicDealID',magicDealID);
		
		me.getController('DealController').showDealDetail(record, dataview.up('viewport').down('tab-deal-grid'));
	},
	
	onPropertySaleHistoryGridItemDblClick: function(dataview, record, item, index, e, eOpts){
		var me = this;
		console.log("Property Sale History Grid Item Db Click");
		var salesbtn = dataview.up('viewport').down('#salesbtn');
		me.getController('DashboardController').setTabPanel(salesbtn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-sales'));
		
		var dealid = record.get('DM-dealNo');
		var magicDealID = record.get('LT-dealNo');
		record.set('dealid',dealid);
		record.set('idDeal',dealid);
		record.set('magicDealID',magicDealID);
		
		me.getController('SalesController').showDealDetail(record, dataview.up('viewport').down('tab-sales-grid'));
	},
	
	addPropertyDocBtnClick: function(btn){
		var me = this;
		console.log("Add Property Button Click");		
	},
	
	onPropDocsGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts){		
		var me = this;
		me.showPropDocsGridMenu(view,record,e);
	},
	
	showPropDocsGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
		console.log("Show Menu for Doc Add/Histroy");
        if(me.getPropertydocoptionmenu()){
			me.getPropertydocoptionmenu().spawnFrom = view;
            me.getPropertydocoptionmenu().showAt(e.getXY());
        }
	},
	
	onPropDocsGridItemLongPress: function(view, record , item , index , e , eOpts) {
		var me = this;
		view.getSelectionModel().select(record);
		me.showPropDocsGridMenu(view,record,e);
	},
	
	propertyDocOptionItemClick: function(item, e, eOpts){
		console.log("Doc Menu Item Clicked.");
        var me = this;
        if(item.getItemId()==="propertydocedit"){
            console.log("Doc Edit Menu Item Clicked");
			me.getController('DocController').showGeneralEditDocPanel(item);
        } else if(item.getItemId()==="dochistory"){
            console.log("Doc History Menu Item Clicked");
        } else if(item.getItemId()==="propertydocadd"){
			me.showRecvDealDocs(item);
        } else if(item.getItemId()==="propertydocdelete"){
			me.getController('DocController').deleteDocument(item);
        } else if(item.getItemId()==="emaildocument" || item.getItemId()==="faxdocument"){
			me.getController('DocController').showEmailDocs(item);
        } else if(item.getItemId()==="printdocument"){
			me.getController('DocController').printDocs(item);
        } else if(item.getItemId()==="downloaddocument"){
			me.getController('DocController').downloadDocs(item);
        } 
	},
	
	showRecvDealDocs: function(cmp) {
		var me = this,
            tabpropertypanel, context, spawnFrom, grid;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			tabpropertypanel = grid.up('tab-properties');
			context = tabpropertypanel.getActiveTab();
		} else {
		    // when cmp is a button.
			tabpropertypanel = cmp.up('tab-properties');
			context = tabpropertypanel.getActiveTab();	    
		}
		if(context.xtype == "tab-property-layout"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDocs',context);	
        }
        else if(context.xtype == "tab-property-detail"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
		}
	},
	
	closeRecvDealDetailDocs: function(context) {
		var me = this;
		if(context.xtype == "tab-property-layout"){
			var eastregion = context.down('#property-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			//eastregion.setVisible(false);
			
			var propmasterdetailsform;
			if(context.down('propmasterdetailsform')){
				console.log("Use Old Panel");
				propmasterdetailsform = context.down('propmasterdetailsform');
			} else {
				console.log("Create New east add deal doc Panel");
				propmasterdetailsform = Ext.create('DM2.view.PropMasterDetailsForm');//me.getPropmasterdetailsform();
				eastregion.add(propmasterdetailsform);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(propmasterdetailsform);			
			eastregion.setTitle("Property Details");
			eastregion.getHeader().show();
			var propertygrid = context.down('tab-property-grid');
			if(propertygrid.getSelectionModel().hasSelection())	{
				row = propertygrid.getSelectionModel().getSelection()[0];
				propmasterdetailsform.getForm().loadRecord(row);
    	   		propmasterdetailsform.setTitle("Property Details");
			} else{
				return false;
			}
        } else if(context.xtype == "tab-property-detail"){
	        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
    	    eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
	},
	
	propertyTabChange: function(tabPanel, newCard, oldCard, eOpts){
		var me = this;
		var propertyhistorytab = tabPanel.down('#dealhistorypropertytab');
		var salehistorypropertytab = tabPanel.down('#salehistorypropertytab');
		if(newCard.xtype=="propertydealhistorygrid" && newCard.getItemId()=="propertydealhistorygrid") {
			if(propertyhistorytab){propertyhistorytab.addCls("docactivebtncls");}
			if(salehistorypropertytab){salehistorypropertytab.removeCls("docactivebtncls");}
		} else if(newCard.xtype=="propertydealhistorygrid" && newCard.getItemId()=="propertysalehistorygrid") {			
			if(salehistorypropertytab){salehistorypropertytab.addCls("docactivebtncls");}
			if(propertyhistorytab){propertyhistorytab.removeCls("docactivebtncls");}
		} else if(newCard.xtype=="propertycontactsgridpanel") {
			me.loadContactsPerProperty(newCard);
			if(propertyhistorytab){propertyhistorytab.removeCls("docactivebtncls");}
			if(salehistorypropertytab){salehistorypropertytab.removeCls("docactivebtncls");}
		} else {
			if(propertyhistorytab){propertyhistorytab.removeCls("docactivebtncls");}
			if(salehistorypropertytab){salehistorypropertytab.removeCls("docactivebtncls");}
		}
	},
	
	clearPropertyContactGrid: function(tabItem){
		console.log("Bind Contact Details");
		var me = this;
		if(tabItem.down('propertycontactsgridpanel')){
			var panel = tabItem.down('propertycontactsgridpanel');
			var prpcntst = panel.getStore();
			prpcntst.removeAll();
			me.afterPrpContactDataLoad(panel);
		}
	},
	
	loadContactsPerProperty: function(panel){
		console.log("Load Contacts Per Property");
		var me = this;
		var context = panel.up('tab-property-layout');
		var propertygrid = context.down('tab-property-grid');
		if(propertygrid.getSelectionModel().hasSelection()) {
			row = propertygrid.getSelectionModel().getSelection()[0];
			propertyid = row.data.idPropertyMaster;
		} else {
			return false;
		}
		
		var prpcntst = panel.getStore();
        prpcntst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        prpcntst.load({
            params:{
                filter :'idPropertyMaster = '+propertyid
            },
            callback: function(records, operation, success){
                me.afterPrpContactDataLoad(panel)
            }
        });
	},
	
	afterPrpContactDataLoad: function(panel){
		var me = this;
		var prpcntst = panel.getStore();
		var count = prpcntst.getCount();		
		var prpcnttoolbar = panel.down('toolbar');
		if(count>0){
			if(prpcnttoolbar){
				prpcnttoolbar.hide();
			}
		} else {
			if(prpcnttoolbar){
				prpcnttoolbar.show();
			}
		}
	},
	
	saleHistoryPropertyTabClick: function(tabItem){
		console.log("Set Sale History Tab");
		var me = this;
		tabItem.up('tabpanel').setActiveTab(tabItem.up('tabpanel').down('#propertysalehistorygrid'));
		me.onCategoryChange("Addressess", tabItem.down('menu'),tabItem.up('tabpanel').down('#propertysalehistorygrid'),"sale");
	},
	
	saleHistoryPropertyTabMenuClick: function(menu, item, e, eOpts){
		console.log("Sale History Menu Clicked");
        var me = this;
        var text = item.text;
		// me.getDealdetailsformpanel().down('#dealdetailtabpanel').setActiveTab(6);
		menu.up('tabpanel').setActiveTab(menu.up('tabpanel').down('#propertysalehistorygrid'));
		me.onCategoryChange(text, menu, menu.up('tabpanel').down('#propertysalehistorygrid'), "sale");
	},
	
	dealHistoryPropertyTabClick: function(tabItem){
		console.log("Set Deal History Tab");
		var me = this;
		tabItem.up('tabpanel').setActiveTab(tabItem.up('tabpanel').down('#propertydealhistorygrid'));
		me.onCategoryChange("Addressess", tabItem.down('menu'), tabItem.up('tabpanel').down('#propertydealhistorygrid'), "deal");
	},
	
	dealHistoryPropertyTabMenuClick: function(menu, item, e, eOpts){
		console.log("Deal History Menu Clicked");
        var me = this;
        var text = item.text;
		// me.getDealdetailsformpanel().down('#dealdetailtabpanel').setActiveTab(6);
		menu.up('tabpanel').setActiveTab(menu.up('tabpanel').down('#propertydealhistorygrid'));
		me.onCategoryChange(text, menu, menu.up('tabpanel').down('#propertydealhistorygrid'), "deal");
	},
	
	onCategoryChange: function(newValue, menu, historyGrid,historyType) {
        var me = this,historyGrid,dealid, row, maingrid, viewname;
		var dealhistory_st = historyGrid.getStore();
        var p = menu.up('tab-property-detail');
        var anchor = "detail";
        if(!p) {
            anchor = "grid";
            p = menu.up('tab-property-layout');
        }
        if(anchor == "grid") {
            maingrid = p.down('tab-property-grid');
			if(maingrid.getSelectionModel().hasSelection()){
            	row = maingrid.getSelectionModel().getSelection()[0];
            	propertyid = row.data.idPropertyMaster;
			} else {
				return false;
			}
        } else {
            propertyid = p.propertyid;
        }
        
		console.log(newValue);
		var filterstarr = [];
		if(newValue=="Addressess"){
			viewname = "address";
		} else if(newValue=="Streets"){
			viewname = "street";
		} else if(newValue=="Contacts"){
			viewname = "contact";
		}
		console.log("viewname"+viewname);
		var filterst = "reqtype="+viewname;
        filterstarr.push(filterst);
		var filterst1 = "propertyid="+propertyid;
        filterstarr.push(filterst1);
		
        var mainViewport = Ext.ComponentQuery.query('mainviewport')[0];
      
		var url;
		if(historyType=="deal"){
			url = DM2.view.AppConstants.apiurl+'DealHistoryByPropertyID';
		} else if(historyType=="sale") {
			url = DM2.view.AppConstants.apiurl+'SaleHistoryByPropertyID';
		}
		
		////
		var dealhistorystproxy = dealhistory_st.getProxy();
		var dealhistorystproxyCfg = dealhistorystproxy.config;
        dealhistorystproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+ mainViewport.config.apikey + ':1'
        };
		dealhistorystproxyCfg.originalUrl = url;
		dealhistorystproxyCfg.url = url;
		dealhistorystproxyCfg.extraParams = {
            filter :filterstarr
        };
		dealhistory_st.setProxy(dealhistorystproxyCfg);
		///		
        dealhistory_st.load();
    },
	
	propertyTabPanelAfterRender: function(cmp, eOpts){
		var me = this,starttab = 0,tabItem;
		var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
		if(menuFunc==1){
			//Insert Deal History Grid & Tab		
			tabItem = Ext.create('DM2.view.property.PropertyDealHistoryGrid',{
				iconCls: 'loanbtn',
				itemId : 'propertydealhistorygrid',
				title: 'Deal History',
				tabConfig: {
					hidden: true
				}
			});
			cmp.add(tabItem);
		}
		
		var menuFunc = me.getController('MainController').getMenuStatus("salemenu");
		if(menuFunc==1){
			//Insert Sale History Grid & Tab		
			tabItem = Ext.create('DM2.view.property.PropertyDealHistoryGrid',{
				iconCls: 'loanbtn',
				itemId : 'propertysalehistorygrid',
				title: 'Sale History',
				tabConfig: {
					hidden: true
				}
			});
			cmp.add(tabItem);
		}
		
		//Insert Docs Grid & Tab		
		tabItem = Ext.create('DM2.view.property.PropertyDocsGridPanel',{
			iconCls: 'docbtn'
		});
		cmp.add(tabItem);
		
		//Insert Contact Grid & Tab		
		tabItem = Ext.create('DM2.view.property.PropertyContactsGridPanel',{
			iconCls: 'contactbtn'
		});
		cmp.add(tabItem);
		
		/////////Deal Menu Check		
		var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
		if(menuFunc==1){			
			var dealhistorypropertytab = Ext.create('DM2.view.DealHistoryTab',{
				itemId:'dealhistorypropertytab',
				style:'margin-right:5px',
				ui:'tabsplitbutton'
			});
			cmp.tabBar.insert(starttab, dealhistorypropertytab);
			dealhistorypropertytab.addCls("docactivebtncls");
			starttab++;			
		}
		
		/////////Sale Menu Check
		var menuFunc = me.getController('MainController').getMenuStatus("salemenu");
		if(menuFunc==1){
			//Add Sale History
			var salehistorypropertytab = Ext.create('DM2.view.DealHistoryTab',{
				itemId:'salehistorypropertytab',
				ui:'tabsplitbutton'
			});
			salehistorypropertytab.setText("Sale History");
			cmp.tabBar.insert(starttab, salehistorypropertytab);
			if(starttab==0){
				salehistorypropertytab.addCls("docactivebtncls");
			}
		}
		cmp.setActiveTab(0);
	},
	
	onPropertyGridHeaderContainerRightClick: function(ct, column, e, t, eOpts){
		console.log("Property Grid header container Right Click");
        e.stopEvent();
        var menu = this.getPropertygrid().headerCt.getMenu();
        menu.showAt(e.getXY());
	},
	
	loadAllPropMaster: function(grid) {
        var me = this;
        //var propmaster_st = Ext.getStore('PropMasterallbuff');
		var propmaster_st = grid.getStore();
        var propmaster_stproxy = propmaster_st.getProxy();
        /*propmaster_stproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var propmaster_stproxyCfg = propmaster_stproxy.config;
        propmaster_stproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		propmaster_st.setProxy(propmaster_stproxyCfg);
        propmaster_st.load({
            scope: this,
            callback: function(records, operation, success) {
                if(success) {
                    if(records.length > 0){
                        grid.getSelectionModel().select(0);
                    }
                }
            }
        });
    },
	loadPropMasterDetailForm: function(selModel, record, index, eOpts) {
        var me = this,propmasterdetailsform;
		var activemenutab = selModel.view.ownerCt.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		var eastregion = context.down('#property-east-panel');
		if(eastregion && eastregion.getLayout().getActiveItem().xtype!="propmasterdetailsform"){
			if(context.down('propmasterdetailsform')){
				propmasterdetailsform = context.down('propmasterdetailsform');
			} else {
				propmasterdetailsform = Ext.create('DM2.view.PropMasterDetailsForm');
				eastregion.add(propmasterdetailsform);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(propmasterdetailsform);			
			eastregion.setTitle("Property Details");
			eastregion.getHeader().show();
		}
		
    	propmasterdetailsform = context.down('propmasterdetailsform');
		if(propmasterdetailsform){
	        propmasterdetailsform.getForm().loadRecord(record);
    	    propmasterdetailsform.setTitle("Property Details");
		}
		///Load Documents Per Property
		console.log(context.down('propertydocsgridpanel'));
		if(context.down('propertydocsgridpanel')){
			me.loadDocsPerProperty(record.get('idPropertyMaster'),context.down('propertydocsgridpanel'));
		}
		if(me.getPropertymenutabpanel().down('#dealhistorypropertytab')){
			if(me.getPropertymenutabpanel().getActiveTab()){
				if(me.getPropertymenutabpanel().getActiveTab().xtype==="propertydealhistorygrid" && me.getPropertymenutabpanel().getActiveTab().getItemId()==="propertydealhistorygrid"){
					me.onCategoryChange("Addressess", me.getPropertymenutabpanel().down('#dealhistorypropertytab').down('menu'),me.getPropertymenutabpanel().getActiveTab(),"deal");
				}
			}
		}
		if(me.getPropertymenutabpanel().getActiveTab()){
			if(me.getPropertymenutabpanel().getActiveTab().xtype==="propertycontactsgridpanel"){
				me.loadContactsPerProperty(me.getPropertymenutabpanel().down('propertycontactsgridpanel'));
			}
		}
		me.loadFromPropertyTable(record.get('idPropertyMaster'),context);
		me.loadAKAList(record,context);
    },
	
	loadAKAList: function(record,context){
		var me = this;
		var propertyakalist = context.down('propertyakalist');
		if(propertyakalist){
			var akalist = record.get('AKAList');
			if(akalist!=null){
				var res = akalist.split(",");			
				var akalistarr = [];
				for(var i=0;i<res.length;i++){
					var tempObj = {
						"text" : res[i]
					};
					akalistarr.push(tempObj);
				}
				propertyakalist.getStore().loadData(akalistarr);
				//console.log(propertyakalist.getStore().getData());
			} else {
				propertyakalist.getStore().removeAll();
			}
		}
	},
	
	loadFromPropertyTable: function(propertymasterid,context){
		var me = this;
		/*var data = [{
            'magicMasterID' : propertymasterid
        }];*/
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
			method:'GET',
            url:DM2.view.AppConstants.apiurl+'mssql:Property',
            params: {
				filter:'magicMasterID='+propertymasterid
			},
            scope:this,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                if(response.status == 200){
					var propmasterdetailsform = context.down('propmasterdetailsform');
					if(propmasterdetailsform){
						if(obj.length>0){
							propmasterdetailsform.getForm().findField('idProperty').setValue(obj[0].idProperty);
							console.log("Id Proeprty"+obj[0].idProperty);
							context.config.propertyid = obj[0].idProperty;
							if(context.down('propertymenutabpanel')){
								context.down('propertymenutabpanel').down('tab[title="Documents"]').show();
								context.down('propertymenutabpanel').down('tab[title="Contacts"]').show();
							}
						} else {
							context.config.propertyid = 0;
							if(context.down('propertymenutabpanel')){
								context.down('propertymenutabpanel').setActiveTab(context.down('propertymenutabpanel').down('#propertydealhistorygrid'));
								context.down('propertymenutabpanel').down('tab[title="Documents"]').hide();
								context.down('propertymenutabpanel').down('tab[title="Contacts"]').hide();
							}
						}
					}
                } else {
                    console.log("Property load Failure.");
                    Ext.Msg.alert('Property load Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Property load Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Property load Failed', obj.errorMessage);
            }
        });		
	},
	
	clearPropertyDocsGrid: function(tabItem){
		console.log("Bind Contact Details");
		var me = this;
		var panel = tabItem.down('propertydocsgridpanel');
		var dealcntst = panel.getStore();
		dealcntst.removeAll();
		me.afterPrpDocsDataLoad(panel);		
	},
	
	loadDocsPerProperty: function(propertyid,propertydocsgridpanel) {
		var me = this;
        var propdoc_st = propertydocsgridpanel.getStore();
        propdoc_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        propdoc_st.load({
            params:{
                filter :'Mast_MasterID = '+propertyid
            },
            callback: function(records, operation, success){
				me.afterPrpDocsDataLoad(propertydocsgridpanel);                
            }
        });
	},
	
	afterPrpDocsDataLoad: function(panel){
		var prpdocsst = panel.getStore();
		var count = prpdocsst.getCount();		
		var doctoolbar = panel.down('toolbar');
		if(count>0){
			if(doctoolbar){
				doctoolbar.hide();
			}
		} else {
			if(doctoolbar){
				doctoolbar.show();
			}
		}
	},
	
    loadAllProperties: function() {
        var me = this;
        var propertyproxy = Ext.getStore('Propertyallbuff').getProxy();
        propertyproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('Propertyallbuff').load({
            scope: this,
            callback: function(records, operation, success) {
                // the operation object
                // contains all of the details of the load operation
                console.log(success);
                if(!success) {
                }
                else{
                    if(records.length > 0){
                        me.getPropertygridpanel().getSelectionModel().select(0);
                    }
                }
            }
        });
    },

    showOnGmap: function() {
        console.log("Show Gmap");
        /*if(this.getPropertygmapwindow().isHidden()){
            this.getPropertygmapwindow().show();
        }
        else{*/
            //this.displayPropertyGmap(this.getPropertygmapwindow().down('#gmapContiner'));
			this.displayPropertyGmap();
        //}
    },

    onGmapContainerAfterRender: function(component, eOpts) {
        console.log("Map Window Rendered");
        this.displayPropertyGmap(component);
    },

    //displayPropertyGmap: function(component) {
	displayPropertyGmap: function() {
        var me = this,stateVal,streetVal,cityVal,streetnoVal,proprec,dealid;
        var geocoder = new google.maps.Geocoder();
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();		
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var propertyQuad = detailpanel.down('dealdetailpropertyfieldset');		
        var dealpropertygrid = propertyQuad.down('grid');
        if (dealpropertygrid.getSelectionModel().hasSelection()) {
            proprec = dealpropertygrid.getSelectionModel().getSelection()[0];
        } else {
            if(dealpropertygrid.getStore().getCount()===1) {
                proprec = dealpropertygrid.getStore().getAt(0);
            } else {
                Ext.Msg.alert('Failed', "Please select property from Property Grid.");
                return false;
            }
        }
		dealid      = proprec.get('dealid');
		stateVal    = proprec.get('state');
		streetVal   = proprec.get('street');
		cityVal     = proprec.get('city');
		streetnoVal = proprec.get('street_no');

		var qst = streetnoVal+"+"+streetVal+"+"+cityVal+"+"+stateVal;
		window.open("http://maps.google.com?q="+qst);
		/*
        //Demo Address Needs to send for getting the GeoCode
        var geocoderRequest = { address: streetVal+', '+cityVal+', '+stateVal };
        //me.getPropertygmapwindow().setLoading(true);
        geocoder.geocode(geocoderRequest, function(results, status){
            //do your result related activities here, maybe push the coordinates to the backend for later use, etc.
            console.log("Success"+status);
            //me.getPropertygmapwindow().setLoading(false);
            //var res=r;//Ext.decode(r.responseText);
            if(status=='OK') {
                var latitude  = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                console.log(latitude);
                console.log(longitude);
                /*
				var myOptions = {
                    center: new google.maps.LatLng(latitude, longitude),
                    zoom: 14,
                    //panControl: false,
                    //mapTypeControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP //ROADMAP
                };

                var map = new google.maps.Map(component.getEl().dom,myOptions);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude,longitude),
                    map: map,
                    title: streetVal+', '+cityVal+', '+stateVal//'Click to zoom'
                });
            }
            else
            {
                Ext.Msg.alert('We\'re Sorry','Can not able to find the address ' + r.status);
            }
        });
		*/
    },

    showOnZillow: function() {
     //   debugger;
        console.log("Show Zillow");
        //if(this.getPropertyzillowwindow().isHidden()){
            //this.getPropertyzillowwindow().show();
        //}
        //else{
            //this.displayPropertyZillow(this.getPropertyzillowwindow().down('#zillowContiner'));
			this.displayPropertyZillow();
        //}
    },

    onZillowContainerAfterRender: function(component, eOpts) {
        console.log("Map Zillow Window Rendered");
        this.displayPropertyZillow(component);
    },

    //displayPropertyZillow: function(component) {
	displayPropertyZillow: function() {		
        console.log("Zillow Container After Render");
        var me = this,streetnoVal,stateVal,streetVal,cityVal,proprec,dealid,zipVal;
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();		
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var propertyQuad = detailpanel.down('dealdetailpropertyfieldset');		
        var dealpropertygrid = propertyQuad.down('grid');
		
        if (dealpropertygrid.getSelectionModel().hasSelection()) {
            proprec = dealpropertygrid.getSelectionModel().getSelection()[0];
        } else {
            if(dealpropertygrid.getStore().getCount()===1){
                proprec = dealpropertygrid.getStore().getAt(0);
            } else {
                Ext.Msg.alert('Failed', "Please select property from Property Grid.");
                return false;
            }
        }
		dealid      = proprec.get('dealid');
		streetnoVal = proprec.get('street_no');
		stateVal    = proprec.get('state');
		streetVal   = proprec.get('street');
		cityVal     = proprec.get('city');
		zipVal     = proprec.get('zip');
		me.openZillowLink(streetnoVal,streetVal,cityVal,stateVal,zipVal);
    },
	
	openZillowLink: function(streetnoVal,streetVal,cityVal,stateVal,zipVal){
		var me = this;
		streetnoVal = streetnoVal.trim();
        console.log("streetnoVal"+streetnoVal);
        console.log("State"+stateVal);
        console.log("StreetVal"+streetVal);
        console.log("cityVal"+cityVal);
		console.log("zipVal"+zipVal);
		var addressSt = "";
        if(streetnoVal!==""){
            addressSt = addressSt+streetnoVal+"+";
        }
        if(streetVal!==""){
            var resSt = streetVal.replace(" ", "+");
            addressSt = addressSt+resSt;
        }

        var citystatezipSt = "";
        if(cityVal!==""){
            var resSt = cityVal.replace(" ", "+");
            citystatezipSt = citystatezipSt+resSt+"+"+stateVal+"+"+zipVal;
        }
		Ext.Ajax.request({
            //url: 'http://www.zillow.com/webservice/GetSearchResults.htm',
            // url: 'GetResults.php',
            url: 'resources/GetResults.php',
            params: {
                'zws-id' : 'X1-ZWz1b7xufgo6x7_9ej1g',
                'address': addressSt,
                'citystatezip': citystatezipSt
            },
            success: function(response){
				//propertyQuad.setLoading(false);
                var myxml = response.responseText;
                var xmlDoc = me.parseToXml(myxml);
                // process server response here
                var search_location_st = Ext.StoreManager.get('SearchLocations');
                if(xmlDoc.getElementsByTagName("results")[0]){

                    console.log(xmlDoc.getElementsByTagName("results"));
                    console.log(xmlDoc.getElementsByTagName("results")[0].childNodes[0]);
                    console.log(xmlDoc.getElementsByTagName("results")[0].childNodes[0].childNodes[1].childNodes[1].childNodes[0].nodeValue);

                    var first_lat_val = xmlDoc.getElementsByTagName("results")[0].childNodes[0].childNodes[2].childNodes[4].childNodes[0].nodeValue;
                    var first_long_val = xmlDoc.getElementsByTagName("results")[0].childNodes[0].childNodes[2].childNodes[5].childNodes[0].nodeValue;

                    /*
					var myOptions = {
                        center: new google.maps.LatLng(first_lat_val, first_long_val),
                        zoom: 14,
                        //panControl: false,
                        //mapTypeControl: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP //ROADMAP
                    };

                    var map = new google.maps.Map(component.getEl().dom,myOptions);
                    console.log("Results Length"+xmlDoc.getElementsByTagName("results")[0].childNodes.length);
                    console.log(xmlDoc.getElementsByTagName("results")[0].childNodes[0]);
					*/
                    //Build the Store For Search Location
                    console.log(search_location_st);
                    search_location_st.loadData([]);
                    var searchlocarr = [];

                    for(var j= 0;j<xmlDoc.getElementsByTagName("results")[0].childNodes.length;j++)
                    {
                        console.log(xmlDoc.getElementsByTagName("results")[0].childNodes[j]);
                        var lat_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[2].childNodes[4].childNodes[0].nodeValue;
                        var long_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[2].childNodes[5].childNodes[0].nodeValue;

                        var zip_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[2].childNodes[1].childNodes[0].nodeValue;
                        var street_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[2].childNodes[0].childNodes[0].nodeValue;
                        var city_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[2].childNodes[2].childNodes[0].nodeValue;
                        var state_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[2].childNodes[3].childNodes[0].nodeValue;

                        var title_val = street_val+ ','+city_val+','+state_val;

                        var homedetailslink_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[1].childNodes[0].childNodes[0].nodeValue;
                        var mapthishomelink_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[1].childNodes[1].childNodes[0].nodeValue;
                        var comparableslink_val = xmlDoc.getElementsByTagName("results")[0].childNodes[j].childNodes[1].childNodes[2].childNodes[0].nodeValue;

                        console.log(title_val);
                        /*var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(lat_val,long_val),
                            map: map,
                            title: title_val
                        });
						*/
                        var searchlocrec = {
                            street : street_val,
                            zipcode: zip_val,
                            city : city_val,
                            state : state_val,
                            homedetailslink: homedetailslink_val,
                            mapthishomelink: mapthishomelink_val,
                            comparableslink: comparableslink_val
                        };

                        searchlocarr.push(searchlocrec);
                    }
					if(searchlocarr.length>0){
						window.open(searchlocarr[0].homedetailslink);
					}
                    search_location_st.loadData(searchlocarr);
                    console.log("Sl Count"+search_location_st.getCount());
                } else {
                    search_location_st.loadData([]);
                    //console.log(xmlDoc.getElementsByTagName("text"));
                    var err = xmlDoc.getElementsByTagName("text")[0].childNodes[0].nodeValue;
                    //me.getPropertyzillowwindow().close();
                    Ext.Msg.alert('Zillow Details load failed', err);
                }
            },
            failure: function(response, opts) {
				//propertyQuad.setLoading(false);
                console.log("Zillow Map Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Zillow Details failed', obj.errorMessage);
            }
        });
	},
	
    parseToXml: function(myxml) {
        if (window.DOMParser){
            var parser = new DOMParser();
            xmlDoc = parser.parseFromString(myxml,"application/xml");
        } else { // Internet Explorer
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async=false;
            xmlDoc.loadXML(myxml);
        }
        return xmlDoc;
    },
	
	showAddContactView: function(cmp){
		var me = this,
            tabpropertypanel, context, spawnFrom, grid;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			tabpropertypanel = grid.up('tab-properties');
			context = tabpropertypanel.getActiveTab();
		} else {
		    // when cmp is a button.
			tabpropertypanel = cmp.up('tab-properties');
			context = tabpropertypanel.getActiveTab();	    
		}
		
		var eastregion = context.down('#property-east-panel');
		
		var addprpcontactform;
		if(context.down('#property-east-panel').down('adddealcontactform')){
			console.log("Use Old Panel");
			addprpcontactform = context.down('#property-east-panel').down('adddealcontactform');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	addprpcontactform = Ext.create('DM2.view.AddDealContactForm');//me.getAdddealcontactform();
		}
   		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(addprpcontactform);	
		eastregion.setTitle("Add Contact");
		eastregion.getHeader().hide();			
		eastregion.config.backview = "propertycontacts";
		addprpcontactform.config.backview = "propertycontacts";
		
		addprpcontactform.getForm().reset();
		var filterstarr = [];
		me.getController('DealDetailController').loadContactAllSelBuff(filterstarr,context);
	},
	closeAddPropertyContactView: function(panel){
		var me = this;
		var tabpropertypanel = panel.up('tab-properties');
		var context = tabpropertypanel.getActiveTab();		
		me.closeRecvDealDetailDocs(context);
	},
	addContactToPropertyBtnClick: function(btn){
		var me = this;
		console.log("Add Contact To Property");
		var context = btn.up('tab-property-layout');
		var propertygrid = context.down('tab-property-grid');
		if (propertygrid.getSelectionModel().hasSelection()) {
			var row = propertygrid.getSelectionModel().getSelection()[0];
			associateid = row.get('idPropertyMaster');
			//parmname = 'idProperty';
		}
		
		var addpropertycontactform = btn.up('adddealcontactform');		
        var allcontactgrid = addpropertycontactform.down('#adddealcontactgrid');
		
        if (allcontactgrid.getSelectionModel().hasSelection()) {
            var row = allcontactgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('contactid'));
            var contactid = row.get('contactid');
            var contactTypeval = addpropertycontactform.getForm().findField('contactType').getValue();
        } else {
            Ext.Msg.alert('Failed', "Please select contact from Allcontactsgrid.");
            return false;
        }
		
		if(context.config.propertyid==0){
			Ext.Msg.alert('Failed', "Property is not exist in property table for selected master property.");
            return false;
		}
		
        var data = [{
            'idProperty': context.config.propertyid,
            'idContact' : contactid,
            'contactType' : contactTypeval
        }];
		addpropertycontactform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:PropxCont',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
				addpropertycontactform.setLoading(false);
                console.log("Add contact to property is submitted.");
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 201) {
					me.loadContactsPerProperty(me.getPropertymenutabpanel().down('propertycontactsgridpanel'));
                    me.closeRecvDealDetailDocs(context);                   
                } else {
                    console.log("Add Contact to property Failure.");
                    Ext.Msg.alert('Contact add failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
				addpropertycontactform.setLoading(false);
                console.log("Add Contact to property Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Contact add failed', obj.errorMessage);
            }
        });
	},
	onPropContactsGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts){
		var me = this;
        me.showPropContactGridMenu(view,record,e);
	},
	
	showPropContactGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
		console.log("Show Menu for Contacts Add");
        if(me.getPropertycontactoptionmenu()){
			me.getPropertycontactoptionmenu().spawnFrom = view;
            me.getPropertycontactoptionmenu().showAt(e.getXY());
			var contactid = record.get('contactid');
			me.getController('ContactController').buildContactSubMenu(contactid,me.getPropertycontactoptionmenu());
        }
	},
	
	onPropContactsGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showPropContactGridMenu(view,record,e);
	},
	
	propertyContactOptionItemClick: function(item, e, eOpts){
		console.log("Contact Menu Item Clicked.");
        var me = this;
        if(item.getItemId()==="propertyaddcontact"){
			me.showAddContactView(item);
        }else if(item.getItemId()==="propertyremovecontact"){
			me.removePropertyContact();
        }
	},
	removePropertyContact: function() {
        var me = this;
        // CG
        console.log("Remove Contact from the Proeprty");
        var tabpropertypanel = me.getController('MainController').getMainviewport().down('tab-properties');
		var context = tabpropertypanel.getActiveTab();
        
        var propcontactgrid = context.down('propertycontactsgridpanel');
        if (propcontactgrid.getSelectionModel().hasSelection()) {
            var row = propcontactgrid.getSelectionModel().getSelection()[0];
            //console.log(row.get('dealid'));
            // var dealid = row.get('dealid');
            // var contactid = row.get('contactid');
            var idPropxCont = row.get('idPropxCont');
        } else {
            Ext.Msg.alert('Failed', "Please select contact from Contact Grid.");
         	return false;
        }

        var data = {
            'checksum': "override"
        };
        
        Ext.Msg.show({
            title:'Delete Contact?',
            message: 'Are you sure you want to delete the contact from property?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:PropxCont/'+idPropxCont+'?checksum=override',
                        /*params: {
                            checksum : 'a89eddc41efde773'
                        },*///Ext.util.JSON.encode(data),
                        scope:this,
                        success: function(response, opts) {
                            console.log("Add contact to property is submitted.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200)
                            {
								me.loadContactsPerProperty(me.getPropertymenutabpanel().down('propertycontactsgridpanel'));
                            }
                            else{
                                console.log("Delete Contact Failure.");
                                Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Delete Contact Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },
	
	onPropDocsGridLoad: function(cmp, e, file){
		var me = this;
		console.log("Done Reading");
		var activemenutab = cmp.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		if(context.xtype == "tab-property-layout"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDocs',context);
		}else if(context.xtype == "tab-property-detail"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
		}		
		me.getController('GeneralDocSelController').onGeneralAddDealDocPanelDrop(cmp, e, file);
	},
	
	onPropertyDocGridItemDbClick: function(grid, record, item, index, e, eOpts){
		console.log("Property Doc Grid Item Double Click.");
		var me = this;
		var docurlname = record.get('url');
		var targetframe = DM2.app.getController('MainController').getTargetFrame(record.get('ext'));
		window.open(docurlname,targetframe);
	},
	
	onPropertyDocGridItemSelect: function(selModel, record, index, eOpts){
		var me = this;
		console.log("Property Docs Grid Item Select event fired & delayed");
		
		var docgrid;
		var tabItem = selModel.view.ownerCt.up('tab-deal-detail');
		if(tabItem){
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		}
		var tabpropertyayout = selModel.view.ownerCt.up('tab-property-layout');
		if(tabpropertyayout){
			docgrid = tabpropertyayout.down('propertydocsgridpanel');
		}		
		docgrid.config.allowSelection = true;
		docgrid.config.openWindow = true;
		setTimeout(function() {
			console.log("property Docs Grid Item Select");
			console.log(docgrid.config.allowSelection);
			if(docgrid.config.allowSelection){
				console.log(docgrid.config.openWindow);
				if(docgrid.config.openWindow){
					var docurl = record.get('url');
					var extension = record.get('ext');
					var docsupportformat = "no";
					if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xlsx" || extension==="pdf"){
						docsupportformat = "yes";
					}
					me.getController('DocController').openWopiDocWindow(docurl,docsupportformat);
				} else {
					docgrid.config.openWindow = true;
				}
			} else {
				docgrid.config.allowSelection = true;
			}
		}, 300);
	},
	
	onPropertyDocGridItemMouseDown: function(view, record, item, index, e, eOpts){
		console.log("Property Doc Grid Item Mouse Down");
		var me = this;
		
		var docgrid;
		var tabItem = view.up('tab-deal-detail');
		//var tabItem = tabdealpanel.getActiveTab();
		if(tabItem){
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		}
		var tabpropertylayout = view.up('tab-property-layout');
		if(tabpropertylayout){
			docgrid = tabpropertylayout.down('propertydocsgridpanel');
		}
		if (e.button==0) {
			var doclink = e.getTarget('.doclink');
			if(doclink) {
				docgrid.config.openWindow = false;
			} else {
				docgrid.config.openWindow = true;
			}
			docgrid.config.allowSelection=true;
		} else { 
			docgrid.config.allowSelection=false;
		}
	},
	
	onPropertyDocGridItemClick: function(grid, record, item, index, e, eOpts ){
		console.log("Property Docs Grid Item Click");
		var me = this;
		console.log(e.button);
		var docgrid;
		var tabItem = grid.up('tab-deal-detail');
		if(tabItem){
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		}
		var tabpropertylayout = grid.up('tab-property-layout');
		if(tabpropertylayout){
			docgrid = tabpropertylayout.down('propertydocsgridpanel');
		}
		if(e.button == 2) {      
			/*console.log("cancel rightclick");*/
			  docgrid.config.allowSelection=false;
		}else{
			var doclink = e.getTarget('.doclink');
			if(doclink) {
				docgrid.config.openWindow = false;
			} else {
				docgrid.config.openWindow = true;
				var docurl = record.get('url');
				var extension = record.get('ext');
				//if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xls" || extension==="xlsx" || extension==="pdf"){
				var docsupportformat = "no";
				if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xlsx" || extension==="pdf"){
					docsupportformat = "yes";
				}
				me.getController('DocController').openWopiDocWindow(docurl,docsupportformat);
			}
		}
	},
	
	onPropertyGridItemDblClick: function(viewTable , td , cellIndex , record , tr , rowIndex , e , eOpts) {
		var me = this;
		console.log("onPropertyGridItemCellDblClick");
		console.log(cellIndex);
		//console.log(viewTable);
		//console.log(viewTable.up('grid'));
		var propgrid = viewTable.up('grid');	
		var colname = viewTable.getHeaderCt().getHeaderAtIndex(cellIndex).dataIndex;
		var cellvalue = record.get(''+colname+'');
		console.log(cellvalue);
		var coltitle = viewTable.getHeaderCt().getHeaderAtIndex(cellIndex).text;
		this.showPropertyFilterView(record, propgrid,cellvalue,rowIndex,colname,coltitle);
    },
	
	detailClass: 'DM2.view.component.tab.property.Layout',
	showPropertyFilterView: function(record, dataview,value,rowIndex,colname,coltitle) {        
        var me = this;		

		var idPropertyMaster = record.data.idPropertyMaster;
        var baseTabPanel = dataview.up('tab-basecardpanel');
        var tabLength = baseTabPanel.items.length;
        var tabItems = baseTabPanel.items.items;
        var tabItem;
        var createPanel = false;
        var x;
		var tabname = coltitle+":"+value;
        
        
        if(tabLength < 2) {
            createPanel = true;
        } else {
            for(x = 1; x < tabLength; x++) {
                tabItem = tabItems[x];
                if(tabItem.tabname == tabname) {
                    createPanel = false;
                    break;
                } else {
                    createPanel = true;
                }
            }
        }
        
        if(createPanel == true) {
			var cleanName = '',tooltipval = '';
			if(tabname){
				cleanName = tabname.replace(/\s\s+/g, ' ');
			}
			var title;
			if(cleanName.length < 1) {
			    title = coltitle;
				tooltipval = coltitle;
			} else {
			    title = cleanName.substring(0,10);
				tooltipval = tabname;
			}
			tabItem = Ext.create(me.detailClass, {
				title: title,
				closable:true,
				html: title,
				tabname: tabname,
				record: record,
				filterTab:true//,
				//iconCls: 'readonlybtn',
				//iconAlign:'right'
			});
			tabItem.tabConfig = {
				tooltip: tooltipval
			};
			baseTabPanel.add(tabItem);
			//me.loadRDealDetailsNewTab(tabItem,baseTabPanel);			                
        }
		baseTabPanel.setActiveItem(tabItem);
		////////
		//var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-sales');
		//var tabdealgrid = tabdealpanel.down('tab-sales-grid');
		////////
		me.filterNewPropertyGrid(dataview,tabItem,rowIndex,colname,value);
    },
	
	filterNewPropertyGrid: function(oldGrid,newTabItem,rowIndex,colname,newFieldValue){
		var me = this,
		fields = oldGrid.HFAPI.getFieldsForRow(0),
		l = fields.length,
		x = 0,
		store, proxy, url, proxyCfg, fullUrl,
		field, filterMode, isHidden, val, otherVal, temp, dataIndex,  QueryStr, filterDataType, fullQueryStr = "",
		msg = [],
		filterModeMatrix = {
			"string": {
				"Starts With": "{0} LIKE'{1}%25'", 
				"Ends With": "{0} LIKE'%25{1}'", 
				"Contains": "{0} LIKE'%25{1}%25'", 
				"Equal": "{0} ='{1}'", 
				"IN": "{0} IN({1})" 
				
			},
			"number": {
				"Equal": "{0} ={1}", 
				"Greater Than": "{0} >={1}", 
				"Lesser Than": "{0} <={1}", 
				"Between": "{0}>={1}&filter={0}<={2}", 
				"IN": "{0} IN({1})"
			},
			"date": {
				"Between": "{0}>='{1}'&filter={0}<='{2}'", 
				"On": "{0} ='{1}'",
				"Before": "{0} <='{1}'",
				"After": "{0} >='{1}'",
				"IN": "{0} IN({1})"
			},
			"boolean": {
				"Equal": "{0} ={1}"
			}
		 };
		console.log("filterNewPropertyGrid"+rowIndex);
		console.log(fields);
		var newGrid = newTabItem.down('tab-property-grid');
		if(fields.length) {
			//newGrid.
            for(x = 0; x < l; x++) {
                field = fields[x];
                val = field.getValue();
				
				dataIndex = field.up().dataIndex;
				if(dataIndex==colname){					
					val = newFieldValue;
				}
                if(val) {
					
					var newSetField = newGrid.HFAPI.getField(''+dataIndex+'');
					console.log("newSetField");
					console.log(newSetField);
					
					//newSetField[0].setValue(val);
					console.log("dataIndex : "+val);
					newSetField[0].setRawValue(val);	
					
                    filterMode = field.filterMode;
                    filterDataType = field.filterDataType;
                    if(field.xtype.indexOf("datefield") != -1) {
                        val = Ext.Date.format(val, "m/d/Y");
                    }
                    
                    otherVal = "";
                    var branchMode = filterModeMatrix[filterDataType][filterMode];
                    
                    var greaterBranchMode;
                    var lesserBranchMode;
                    
                    if(filterDataType == "number") {
                        greaterBranchMode = filterModeMatrix[filterDataType]["Greater Than"];
                        lesserBranchMode = filterModeMatrix[filterDataType]["Lesser Than"];                  
                    }
                    
                    if(filterDataType == "date") {
                        greaterBranchMode = filterModeMatrix[filterDataType]["After"];
                        lesserBranchMode = filterModeMatrix[filterDataType]["Before"];
                    }                     
                    
                    if(filterMode == "Between") {
                        otherVal = field.next().getValue();
                        
                        if(field.xtype.indexOf("datefield") != -1) {
                            if(val) {
                                val = Ext.Date.format(new Date(val), "m/d/Y");
                            }
                            if(otherVal) {
                                otherVal = Ext.Date.format(new Date(otherVal), "m/d/Y");    
                            }
                        } 
                        if(val && otherVal) {
                            queryStr = branchMode;
                            queryStr = Ext.String.format(queryStr, dataIndex, val, otherVal);
                        } else {
                            if(val) {
                                queryStr = greaterBranchMode;
                                queryStr = Ext.String.format(queryStr, dataIndex, val);
                            } else {
                                if(otherVal) {
                                    queryStr = lesserBranchMode;
                                    queryStr = Ext.String.format(queryStr, dataIndex, otherVal);
                                } else {

                                    
                                }
                            }
                        }
                        
                    } else {
                        
                        if(filterMode == "IN") {
                            val = joinWithQuotes(val);
                            if(val.length >= 1) {
                                queryStr = branchMode;
                                queryStr = Ext.String.format(queryStr, dataIndex, val);    
                            }
                        } else {
                            queryStr = branchMode;
                            queryStr = Ext.String.format(queryStr, dataIndex, val);
                        }
                    }
                    
                    if((val && filterMode != "Between") || ((val || otherVal) && filterMode == "Between")) {
                        if(filterMode != "IN") {
                            if(fullQueryStr === "") {
                                fullQueryStr += ("filter=" + queryStr);
                            } else {
                                fullQueryStr += "&filter=" + queryStr;
                            }   
                        } else {
                            if(val.length > 0) {
                                if(fullQueryStr === "") {
                                    fullQueryStr += ("filter=" + queryStr);
                                } else {
                                    fullQueryStr += ("&filter=" + queryStr);
                                }   
                            }
                        }
                    }
					
                }
            }
            
            store = newGrid.getStore();
            store.removeAll();
            proxy = store.getProxy();
            proxyCfg = proxy.config;
            url = proxyCfg.originalUrl;
            fullUrl = url + "?" + fullQueryStr;
			
			proxyCfg.headers = {
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			};			
            proxyCfg.url = fullUrl;
            store.setProxy(proxyCfg);
            store.load();
        }
	},
	
	showAddNewPropertyView: function(){
        console.log("Show Add New Property View");
        var me = this,addnewmasterpropertyview;
		var tabpropertypanel = me.getController('MainController').getMainviewport().down('tab-properties');
		var context = tabpropertypanel.getActiveTab();
		
		var eastregion = context.down('#property-east-panel');		
		if(context.down('#property-east-panel').down('addnewmasterpropertyview')){
			console.log("Use Old Panel");
			addnewmasterpropertyview = context.down('#property-east-panel').down('addnewmasterpropertyview');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	addnewmasterpropertyview = Ext.create('DM2.view.AddNewMasterPropertyView');
		}
   		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(addnewmasterpropertyview);	
		eastregion.setTitle("Create New Property");
		eastregion.getHeader().hide();
		eastregion.config.backview = "addnewmasterpropertyview";
		addnewmasterpropertyview.config.backview = "propertymastergrid";
		addnewmasterpropertyview.setTitle("Create New Property");
	},
	
	showEditPropertyView: function(){
		var me = this;
	},
	
	addNewMasterPropertyViewClose: function(panel){
		var me = this;
		if(panel.config.backview == "propertymastergrid"){
			var context = panel.up('tab-property-layout');
			me.getController('PropertyController').closeRecvDealDetailDocs(context);
		} else if(panel.config.backview == "adddealxpropertyview"){
			var eastregion = panel.up('[region=east]');	
			eastregion.getLayout().setActiveItem(eastregion.down('adddealpropertyform'));
		}
	},
	
	createNewPropertyBtnClick: function(btn){
		var me = this,salerec,idSale,methodname;
		console.log("Save New Master Property To Table");
        var addnewmasterpropertyview = btn.up('addnewmasterpropertyview');
		var addnewmasterpropertyForm = addnewmasterpropertyview.getForm();
		
		var idPropertyMaster = addnewmasterpropertyForm.findField('idPropertyMaster').getValue();
		var data = {
            //'idPropertyMaster' :addnewmasterpropertyForm.findField('idPropertyMaster').getValue(),
            'Township'       : addnewmasterpropertyForm.findField('Township').getValue(),
			'Municipality'   : addnewmasterpropertyForm.findField('Municipality').getValue(),
			'Boro'           : addnewmasterpropertyForm.findField('Boro').getValue(),
			'Block'          : addnewmasterpropertyForm.findField('Block').getValue(),
			'Lot'            : addnewmasterpropertyForm.findField('Lot').getValue(),
			'StreetNumber'   : addnewmasterpropertyForm.findField('StreetNumber').getValue(),
			'HighStreetNumber': addnewmasterpropertyForm.findField('HighStreetNumber').getValue(),
			'StreetName'     : addnewmasterpropertyForm.findField('StreetName').getValue(),
			'BuildingName'     : addnewmasterpropertyForm.findField('BuildingName').getValue(),
			'CurrentAddedValue'     : addnewmasterpropertyForm.findField('CurrentAddedValue').getValue(),
			'FullValue'     : addnewmasterpropertyForm.findField('FullValue').getValue(),
			//'BuildingClass'     : addnewmasterpropertyForm.findField('BuildingClass').getValue(),
			'Zoning'     : addnewmasterpropertyForm.findField('Zoning').getValue(),
			'Owner'     : addnewmasterpropertyForm.findField('Owner').getValue(),
			'FrontLot'  : addnewmasterpropertyForm.findField('FrontLot').getValue(),
			'LotDepth'     : addnewmasterpropertyForm.findField('LotDepth').getValue(),
			'BuildingFront'     : addnewmasterpropertyForm.findField('BuildingFront').getValue(),
			'BuildingDepartment'     : addnewmasterpropertyForm.findField('BuildingDepartment').getValue(),
			'LandArea'     : addnewmasterpropertyForm.findField('LandArea').getValue(),
			'Buildings'     : addnewmasterpropertyForm.findField('Buildings').getValue(),
			'dataSource'  : 'USER'
		};
		
		if(idPropertyMaster===0 || idPropertyMaster===null || idPropertyMaster==="") {
            console.log("Add New Property To Property Master");
            methodname = "POST";			
        } else {
            console.log("Save Edit Property To Property Master");
            data['idPropertyMaster'] = idPropertyMaster;
            data['@metadata'] = {'checksum' : 'override'};
            methodname = "PUT";			
        }

        addnewmasterpropertyview.setLoading(true);
        Ext.Ajax.request({
            method: methodname,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:propertyMaster',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Save Property To Property Master submitted.");
                addnewmasterpropertyview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					addnewmasterpropertyview.getForm().reset();
					if(addnewmasterpropertyview.config.backview == "propertymastergrid"){
						var tabpropertylayout = addnewmasterpropertyview.up('tab-property-layout');
						me.addNewMasterPropertyViewClose(addnewmasterpropertyview);					
						me.getController('PropertyController').loadAllPropMaster(tabpropertylayout.down('tab-property-grid'));
					} else if(addnewmasterpropertyview.config.backview == "adddealxpropertyview"){
						var row = Ext.create('DM2.model.PropertyMaster',obj.txsummary[0]);
						var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
						var context = activemenutab.getActiveTab();
						me.getController('CreateNewDealFromPropertyController').loadFromPropertyTable(context,row,'addpropertytodeal');
					}					
                } else {
                    console.log("Save Property To Property Master Failure.");
                    Ext.Msg.alert('Save Property To Property Master failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Save Property To Property Master Failure.");
                addnewmasterpropertyview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Property To Property Master failed', obj.errorMessage);
            }
        });
	},
	
	clearPropertyDealHistory: function(tabItem){
		console.log("Clear Property Deal History");
		var me = this;
		var panel = tabItem.down('#propertydealhistorygrid');
		var dealhistst = panel.getStore();
		dealhistst.removeAll();
	}
});