Ext.define('DM2.controller.SendDocumentController', {
    extend: 'Ext.app.Controller',

    refs: {
        senddocumentpanel: {
            autoCreate: true,
            selector: 'senddocumentpanel',
            xtype: 'senddocumentpanel'
        },
        dealdocssendloigridmenu: {
            autoCreate: true,
            selector: 'dealdocssendloigridmenu',
            xtype: 'dealdocssendloigridmenu'
        }
    },

    init: function(application) {
        this.control({
            'senddocumentpanel': {
                close: 'onSendDocumentPanelClose'
            },
            'senddocumentpanel #senduploaddocbtn': {
                click: 'onSendUploadDocBtnClick'
            },
            '#dealdocssendloigridpanel #senddoctoolbarmenu': {
                click: 'onSendDocMenuClick'
            },
            'senddocumentpanel #dealdocssendloigridpanel':{
                load: this.onDealDocsDrop,
                containercontextmenu: this.onDealDocsGridPanelRightClick,
                rowcontextmenu: this.onDealDocsGridItemRightClick,
                select: this.onDealDocsSendLOIGridItemSelect,
                selectionchange: 'onSendDocsSelectionChange'
            },
            'dealdocssendloigridmenu':{
                click:this.onSendDocMenuClick
            },
            'senddocumentpanel #contactcombo':{
                select: this.onChangeContacts
            },
            'senddocumentpanel #loancombo':{
                select: this.onChangeLoans
            },
			'senddocumentpanel #printlabelbtn':{
                click: this.onPrintLabelBtnClick
            },
            '#dealdocssendloigridpanel gridview':{
                beforedrop: this.dropDealDocsToLOIPad
            },
			'senddocumentpanel button[action="doneActionBtn"]': {
                click: 'onSendDocDoneBtnClick'
            }
        });
    },
	
    showSendDocumentView: function(backview,context,activitytext) {
        var me = this,senddocumentpanel,title = "";
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('senddocumentpanel')){
			console.log("Use Old Panel");
			senddocumentpanel = detailpanel.down('senddocumentpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	senddocumentpanel = Ext.create('DM2.view.SendDocumentPanel');
		}
        eastregion.getLayout().setActiveItem(senddocumentpanel);

        eastregion.config.backview = backview;
		title = "Activities > "+activitytext;
		
        senddocumentpanel.setTitle(title);
        //senddocumentpanel.down('#activityid').setValue(activityid);
		
		var record = context.record;
        var dealid = record.data.idDeal;
		
        //Load deal contacts
        me.loadDealContactsSendLOI(dealid,context);

        var emailbtn = senddocumentpanel.down('#emailbtn');
        var printbtn = senddocumentpanel.down('#printbtn');
        var faxbtn   = senddocumentpanel.down('#faxbtn');
        var printlabelbtn = senddocumentpanel.down('#printlabelbtn');

        if(backview === "sendloi" || backview === "SnCntToB" || backview === "SnCntToS" || backview === "SnBrCntToS" || backview === "SnBrCntToB" || backview === "ModiFN"){
            senddocumentpanel.down('#sendloiloanfrm').hide();
            printlabelbtn.hide();
        } else if(backview === "application" || backview === "reiterate" || backview === "sendmodification"){
            senddocumentpanel.down('#sendloiloanfrm').show();
            //Load deal Selected Loans
           
            printlabelbtn.show();
            //printlabelbtn.disable();
        } else if(backview === "reiterate"){
        } else {
			
		}
		
		if(backview === "sendmodification"){
			console.log("Set All fields read Only");
			me.getController('ApplicationController').loadDealSelectedLoans(dealid,context,"sendmode");
			me.setReadOnlyForAll(senddocumentpanel.down('#sendloiloanfrm'),true);
        } else if(backview === "application" || backview === "reiterate"){
			me.getController('ApplicationController').loadDealSelectedLoans(dealid,context,"apprtmode");
           // me.setReadOnlyForAll(senddocumentpanel.down('#sendloiloanfrm'),false);
		    me.setReadOnlyForAll(senddocumentpanel.down('#sendloiloanfrm'),true);
			senddocumentpanel.down('#sendloiloanfrm').getForm().findField('loanid').setReadOnly(false);
        }

        emailbtn.disable();
        printbtn.disable();
        faxbtn.disable();

        //Load Send Documents
        me.loadSendDocuments(dealid,context);
    },
	
	setReadOnlyForAll: function (loanform,bReadOnly) {
		loanform.getForm().getFields().each (function (field) {
		  field.setReadOnly (bReadOnly);
		});
    },

    loadSendDocuments: function(dealid,context) {
        var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        var clsfctn = "";
        var backview = eastregion.config.backview;
        if(backview === "sendloi"){
            clsfctn = "LOI";
        } else if(backview === "application"){
            clsfctn = "Application";
        } else if(backview === "reiterate"){
            clsfctn = "Reiterate";
        } else if(backview === "sendmodification"){
            clsfctn = "s.m";
        } else if(backview === "SnCntToB" || backview === "SnCntToS" || backview === "SnBrCntToS" || backview === "SnBrCntToB" || backview === "ModiFN"){
            clsfctn = backview;
        }

		var record = context.record;
        var dealid = record.data.idDeal;

        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
        filterstarr.push("(clsfctn like '%"+clsfctn+"%')");

        var senddocsperdeal_st = context.down('senddocumentpanel').down('dealdocssendloigridpanel').getStore();
        senddocsperdeal_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        senddocsperdeal_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                console.log(success);
            }
        });
    },

    onSendDocumentPanelClose: function(panel) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);	
        var eastregion = detailpanel.down('[region=east]');
        var backview = eastregion.config.backview;

        if(backview==="sendmodification"){
            apprcntrl = me.getController('ApprovalController');
            //var tmprec = apprcntrl.getApprovalpanel().config.approvalRec;
            //apprcntrl.loadApproval(tmprec);
            apprcntrl.showApprovalView(context);
        } else {
            eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
        }
        var dealcontactssendLOIst = Ext.getStore('DealContactsSendLOI');
        dealcontactssendLOIst.removeAll();
    },

    loadDealContactsSendLOI: function(dealid,context) {
        var me = this;
		var dealcontactssendLOI_st = context.down('senddocumentpanel').down('#contactcombo').getStore();//Ext.getStore('DealContactsSendLOI');
        dealcontactssendLOI_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealcontactssendLOI_st.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
                var contdealcnt = dealcontactssendLOI_st.getCount();
                if(contdealcnt>0){
                    console.log("select rec");
                    var cntrec = dealcontactssendLOI_st.getAt(0);//.get('contactid');
                    var cntcombo = context.down('senddocumentpanel').down('#contactcombo');
                    cntcombo.select(cntrec);

                    cntcombo.fireEvent('select',cntcombo,cntrec);
                    //var cntdtfrm = me.getSenddocumentpanel().down('#sendloicontactfrm');
                    //cntdtfrm.getForm().loadRecord(cntrec);
                }
            }
        });
    },

    onDealDocsDrop: function(cmp, e, file) {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();		
        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
        me.getController('GeneralDocSelController').showGeneralDocSelPanel(eastregion.config.backview,context);
        me.getController('GeneralDocSelController').onGeneralAddDealDocPanelDrop(cmp, e, file);
    },

    onDealDocsGridPanelRightClick: function(component, e, eOpts) {
        console.log("Deal Docs Grid Right click");
        e.stopEvent();
        this.showDealDocsSendLOIGridMenu(e);
    },

    onDealDocsGridItemRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        e.stopEvent();
        this.showDealDocsSendLOIGridMenu(e);
    },

    showDealDocsSendLOIGridMenu: function(e) {
        console.log("Show Menu for Deal Docs");
        if(this.getDealdocssendloigridmenu()){
            this.getDealdocssendloigridmenu().showAt(e.getXY());
        }
    },

    setEmailFaxLink: function(context) {
        var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
		var senddocumentpanel = detailpanel.down('senddocumentpanel');
        var sendloidocgrid = senddocumentpanel.down('#dealdocssendloigridpanel');

        var recs = [];
        if (sendloidocgrid.getSelectionModel().hasSelection()) {
            recs = sendloidocgrid.getSelectionModel().getSelection();
        }

        //var senddocsperdeal_st = Ext.getStore('SendDocsPerDeal');
        //var doccnt = senddocsperdeal_st.getCount();

        var emailbtn = senddocumentpanel.down('#emailbtn');
        var printbtn = senddocumentpanel.down('#printbtn');
        var faxbtn = senddocumentpanel.down('#faxbtn');
		var printlabelbtn = senddocumentpanel.down('#printlabelbtn');

        if(recs.length > 0){
            emailbtn.enable();
            printbtn.enable();
            faxbtn.enable();
			printlabelbtn.enable();
			
            var docurllistarr = [];
            var docnamelistarr = [];
            //Get the Docs
            Ext.Array.each(recs, function(sendloidocrec) {
                var docurl = sendloidocrec.get('url');
                //docurl = "http://ravenoustech.com/company-profile.pdf";
                var docname = sendloidocrec.get('name');
				var tmpdocurlarr = docurl.split("http://");
				var finaldocurl = "http://"+tmpdocurlarr[1];
                docurllistarr.push(finaldocurl);
                docnamelistarr.push(docname);
            });

            var docurllistst  = docurllistarr.join("&ATTACHITEM#");
            var docnamelistst = docnamelistarr.join("&ATTACHNAME#");

            var bodyMsg = "";
            var subject = me.getController('GeneralDocSelController').getClassification(context);

			var sendloicntfrm = senddocumentpanel.down('#sendloicontactfrm');
			var dealcontactssendLOIst = sendloicntfrm.getForm().findField('contactid').getStore();
            var emaillistst = "";
            var faxlistst = "";
            if(dealcontactssendLOIst.getCount() > 0){
                var emailval = sendloicntfrm.getForm().findField('email').getValue();
                var faxval = sendloicntfrm.getForm().findField('faxNumber').getValue();

                if(emailval!=="" && emailval!==null && emailval!==undefined){
                    emaillistst = emaillistst +emailval + ";";
                }
                if(faxval!=="" && faxval!==null && faxval!==undefined){
                    faxlistst = faxlistst +faxval + "@fax.local;";
                }
            }

            var mailsep = "&MAILPART#";
            var mailhrefst = "mcgmt:"+emaillistst+mailsep+subject+mailsep+bodyMsg+mailsep+docurllistst+mailsep+docnamelistst;
			console.log(mailhrefst);
            var faxhrefst = "mcgmt:"+faxlistst+mailsep+subject+mailsep+bodyMsg+mailsep+docurllistst+mailsep+docnamelistst;

            faxbtn.setHref(faxhrefst);
            emailbtn.setHref(mailhrefst);
			if(recs.length>0){
				printbtn.setHref(recs[0].get('url'));
			}
        } else {
            emailbtn.disable();
            printbtn.disable();
            faxbtn.disable();
        }
    },

    onDealDocsSendLOIGridItemSelect: function(grid, record, index, eOpts) {
        /*var me = this;
        me.getSendloi().down('#emailbtn').enable();
        me.getSendloi().down('#printbtn').enable();
        me.getSendloi().down('#faxbtn').enable();
        me.setEmailFaxLink();*/
    },

    onChangeContacts: function(combo, rec, eOpts) {
        var me = this;
        console.log("Fire select event");
		var context = me.getController('DealDetailController').getTabContext(combo);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);	
        var cntdtfrm = detailpanel.down('senddocumentpanel').down('#sendloicontactfrm');
        cntdtfrm.getForm().loadRecord(rec);
        me.loadContactsFax(rec,context);
        me.loadContactsEmail(rec,context);
        me.setEmailFaxLink(context);
    },
	
	loadContactsFax: function(rec,context){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var sendloicontactfrm = detailpanel.down('senddocumentpanel').down('#sendloicontactfrm');
        var cntfaxSt = sendloicontactfrm.getForm().findField('faxNumber').getStore();//Ext.getStore('ContactFax').getProxy();
        cntfaxSt.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        //cntfaxSt.removeAll();
        cntfaxSt.load({
            params:{
                filter :'idContact = '+rec.get('contactid')
            }
        });
	},
	
	loadContactsEmail: function(rec,context){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var sendloicontactfrm = detailpanel.down('senddocumentpanel').down('#sendloicontactfrm');
        var cntemailSt = sendloicontactfrm.getForm().findField('email').getStore();//Ext.getStore('ContactEmails').getProxy();
        cntemailSt.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        //cntemailSt.removeAll();
        cntemailSt.load({
            params:{
                filter :'idContact = '+rec.get('contactid')
            }
        });
	},

    onChangeLoans: function(combo, rec, eOpts) {
        var me = this;
        console.log("Fire select event");
		var context = me.getController('DealDetailController').getTabContext(combo);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);	
        var loandtfrm = detailpanel.down('senddocumentpanel').down('#sendloiloanfrm');
        loandtfrm.getForm().loadRecord(rec);
        //me.setEmailFaxLink();
    },

    dropDealDocsToLOIPad: function(node, data, overModel, dropPosition, dropHandlers, eOpts) {
        console.log("Drop Docs To LOI Pad.");
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
        var rec = data.records[0];
        var docid = rec.get('docid');
		
		var row = context.record;
        var dealid = row.data.idDeal;

        var filename = rec.get('name');

        var tempclsf = rec.get('clsfctn');
        if(tempclsf==="" || tempclsf===null){
            tempclsf = me.getController('GeneralDocSelController').getClassification(context);
        }

        ///// Add the file item to Store
        var tmpdocrec = Ext.create('DM2.model.Document',{
            'name' : filename,
            'size' : rec.get('size'),
            'clsfctn' :tempclsf,
            'desc' :rec.get('desc'),
            'url' : rec.get('url')
        });
		
		var senddocumentpanel = context.down('dealdetailsformpanel').down('senddocumentpanel');
        var docgrid = senddocumentpanel.down('#dealdocssendloigridpanel');
        var senddocsperdeal_st = docgrid.getStore();
        senddocsperdeal_st.add(tmpdocrec);
        docgrid.getView().refresh();

        me.setEmailFaxLink(context);
        return false;
    },

    goToSendDocument: function(context) {
        var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
		var senddocumentpanel = detailpanel.down('senddocumentpanel');
        eastregion.getLayout().setActiveItem(senddocumentpanel);
    },

    onSendDocMenuClick: function(menu, item, e, eOpts) {
        var me = this;
        console.log("Send Deal Docs Menu Clicked");
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eventname = item.itemevent;		
        var eastregion = detailpanel.down('[region=east]');
        if(eventname === "addfile"){
            //var docstempst = Ext.getStore('DocsTemp');
            //docstempst.removeAll();
            me.getController('GeneralDocSelController').showGeneralDocSelPanel(eastregion.config.backview,context);
        } else if(eventname === "filetemplate"){
            me.getController('GeneralDocSelController').showGeneralTemplateSelGridPanel("senddoc");
        } else if(eventname === "filerepository"){
            me.getController('GeneralDocSelController').showGeneralDocSelRepositoryPanel("senddoc");
        } else if(eventname === "uploaddoc"){
            //var docstempst = Ext.getStore('DocsTemp');
            //docstempst.removeAll();
            me.getController('GeneralDocSelController').showGeneralDocSelPanel(eastregion.config.backview,context);
        }
    },

    onSendDocsSelectionChange: function(selModel, selected, eOpts) {
        console.log("Send Document Selection Change");
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(selModel.view.ownerCt);
        this.setEmailFaxLink(context);
    },

    selectSendDocumentAfterLoad: function(context) {
        var me = this;
        var senddocsperdeal_st = context.down('senddocumentpanel').down('dealdocssendloigridpanel').getStore();//Ext.getStore('SendDocsPerDeal');
        var docgrid = context.down('senddocumentpanel').down('#dealdocssendloigridpanel');
        //docgrid.getSelectionModel().select((senddocsperdeal_st.getCount()-1));
        for(var i=0;i<(senddocsperdeal_st.getCount());i++){
            var tmprec = senddocsperdeal_st.getAt(i);
            console.log(tmprec);
            var docid = tmprec.get('docid');
            if(docid){
                console.log("DocId Exist");
                console.log("docid"+docid);
            }else{
                console.log("Select Row"+i);
                docgrid.getSelectionModel().select(i,true);
            }
        }
    },

    onSendUploadDocBtnClick: function(btn) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        //var docstempst = Ext.getStore('DocsTemp');
        //docstempst.removeAll();
        me.getController('GeneralDocSelController').showGeneralDocSelPanel(eastregion.config.backview,context);
    },
	
	onPrintLabelBtnClick: function(btn){
		var me = this;
		console.log("onPrintLabelBtnClick");
		var context = me.getController('DealDetailController').getTabContext(btn);
		var row = context.record;
        var dealid = row.data.idDeal;
		
		var loanform = context.down('senddocumentpanel').down('#sendloiloanfrm');
		var loanid = loanform.getForm().findField('loanid').getValue();		
		
		var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
		var apikey = Ext.util.Cookies.get("apikey-"+DM2.view.AppConstants.apiurl);
		window.open("./resources/printLable.php?apikey="+apikey+"&username="+userid+"&dealid="+dealid+"&loanid="+loanid+"&apiurl="+DM2.view.AppConstants.apiurl,"_blank");
	},
	
	onSendDocDoneBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	}
});