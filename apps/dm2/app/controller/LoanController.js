Ext.define('DM2.controller.LoanController', {
    extend: 'Ext.app.Controller',

    refs: {
        selectloanpanel: {
            autoCreate: true,
            selector: 'selectloanpanel',
            xtype: 'selectloanpanel'
        },
        selectloanquotesgridpanel: 'selectloanquotesgridpanel',
        selectloanmenu: {
            autoCreate: true,
            selector: 'selectloanmenu',
            xtype: 'selectloanmenu'
        }
    },

    init: function(application) {
        this.control({
            'selectloanpanel':{
                close:this.selectloanpanelclose
            },
            'selectloanquotesgridpanel':{
                select:this.selectLoanQuotesGridItem,
                rowcontextmenu: this.onLoanQuotesGridRightClick
            },
            'selectloanmenu':{
                click: this.selectLoanMenuClick
            },
            'selectloanquotesgridpanel #selectloandonebtn':{
                click:this.onSelectLoanDoneBtnClick
            }
        });
    },

    SelectLoanClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this;
        //Set the Quote Editor in Active Area		
		var eastregion = tabdealdetail.down('dealdetailsformpanel').down('[region=east]');
		var selectloanpanel;
		if(tabdealdetail.down('dealdetailsformpanel').down('selectloanpanel')){
			console.log("Use Old Panel");
			selectloanpanel = tabdealdetail.down('dealdetailsformpanel').down('selectloanpanel');
		}
		else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	selectloanpanel = Ext.create('DM2.view.SelectLoanPanel');//me.getSelectloanpanel();
		}
        eastregion.getLayout().setActiveItem(selectloanpanel);
		selectloanpanel.setTitle("Activities > "+activitytext);

        //Set the QuotePanel Form Values
        selectloanpanel.getForm().reset();  //Reset The Form
        selectloanpanel.getForm().findField('activityid').setValue(activityid);

        ///Load All Deal Quotes
		var record = tabdealdetail.record;
        var dealid = record.data.dealid;
        me.loadAllDealLoans(dealid);
    },

    selectloanpanelclose: function(panel) {
		var me = this;
		var context = panel.up('tab-deal-detail');
        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    loadAllDealLoans: function(dealid) {
        console.log("All Quotes Load");
        var me = this;
        var dealquoteallproxy = Ext.getStore('DealQuotesAll').getProxy();
        dealquoteallproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);

        Ext.getStore('DealQuotesAll').load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){
                if(success){
                    var dealquoteallcnt = Ext.getStore('DealQuotesAll').getCount();

                    var quotegrid = me.getSelectloanquotesgridpanel();
                    var quotegridtoolbar = quotegrid.down('#selectloangridtoolbar');
                    if(dealquoteallcnt > 0){
                        quotegrid.getSelectionModel().select(0);
                        if(quotegridtoolbar){
                            quotegridtoolbar.hide();
                        }
                    }
                    else{
                        if(quotegridtoolbar){
                            quotegridtoolbar.show();
                        }
                    }
                }
            }
        });
    },

    selectLoanQuotesGridItem: function(dataview, record, item, index, e, eOpts) {
        var me = this;
        me.getSelectloanpanel().getForm().loadRecord(record);
        me.loadLoanProperties(record.get('loanid'));
        me.getSelectloanpanel().getForm().findField('usedInDeal').setValue(record.get('selected'));
    },

    onLoanQuotesGridRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        var me = this;
        e.stopEvent();
        console.log("Show Menu for Loan Grid");
        if(this.getSelectloanmenu()){
            this.getSelectloanmenu().showAt(e.getXY());
        }
        me.getSelectloanmenu().removeAll();

        var menutext = "";
        if(record.get('selected')==="F"){
            menutext = "Select Quote";
        }else{
            menutext = "UnSelect Quote";
        }
        var menuitem = Ext.create('Ext.menu.Item', {
            text    : menutext,
            loanid  : record.get('loanid')
        });
        me.getSelectloanmenu().add(menuitem);
        var menuitem = Ext.create('Ext.menu.Item', {
            text    : 'Mark activity as done'
        });
        me.getSelectloanmenu().add(menuitem);
    },

    selectLoanMenuClick: function(menu, item, e, eOpts) {
        console.log("Select Loan Menu Click");
        if(item.text === "Select Quote"){
            console.log("Select Quote");
            this.setSelectUnselectQuote("T",item.loanid);
        }else if(item.text === "UnSelect Quote"){
            console.log("UnSelect Quote");
            this.setSelectUnselectQuote("F",item.loanid);
        }else if(item.text === "Mark activity as done"){
            console.log("Mark activity as done");
            this.onSelectLoanDoneBtnClick();
        }
    },

    onSelectLoanDoneBtnClick: function() {
        var me = this;
        var dealdetailform = me.getController('DealDetailController').getDealdetailsformpanel();
        var dealid = dealdetailform.dealrec.get('dealid');
        var actDetailForm = me.getSelectloanpanel();
        me.getController('DealActivityController').postActivityHistory(dealid, actDetailForm);
    },

    setSelectUnselectQuote: function(usedindealval, loanid) {
        console.log("Save Deal Detail Quote");
        var me = this;
        var dealdetailform = me.getController('DealDetailController').getDealdetailsformpanel();
        var dealid = dealdetailform.dealrec.get('dealid');

        var data = {
            'idLoan' : loanid,
            "usedInDeal"  : usedindealval,
            "@metadata" : {'checksum' : 'override'}
        };

        var methodname = "PUT";

        me.getSelectloanquotesgridpanel().setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Loan',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                me.getSelectloanquotesgridpanel().setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
                    me.loadAllDealLoans(dealid);
                    me.getController('QuoteController').loadQuoteDetails(dealid);  //Load First Quote not as loan
                    me.getController('DealDetailController').loadNewLoanDetails(dealid);
                }
                else{
                    console.log("Select/UnSelect Quote Failure.");
                    Ext.Msg.alert('Select/UnSelect Quote failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                me.getSelectloanquotesgridpanel().setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Select/UnSelect Quote', obj.errorMessage);
            }
        });
    },

    loadLoanProperties: function(loanid) {
        var me = this;
        var propertygrid = me.getSelectloanpanel().down('#selectloanpropertygridpanel');
        propertygrid.getSelectionModel().deselectAll();

        ///Load the properties_has_loan
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:PropxLoan',
            params:{
                filter :"idLoan = '"+loanid+"'"
            },
            scope:this,
            method:'GET',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);

                if(obj.length !== 0){
                    Ext.getStore('Dealproperty').each(function(propertyrecord){
                        console.log (propertyrecord);
                        for(var k=0;k<obj.length;k++){
                            if(propertyrecord.get('propertyid')===obj[k].idProperty){
                                propertygrid.getSelectionModel().select(propertyrecord,true);
                            }
                        }
                    });
                }
            },
            failure: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Failed', obj.errorMessage);
            }
        });
        ////
    }
});