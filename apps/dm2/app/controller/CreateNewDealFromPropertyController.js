Ext.define('DM2.controller.CreateNewDealFromPropertyController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.CreateNewSalesDealFromPropertyPanel'],
    refs: {
        createnewdealfrompropertypanel: {
            autoCreate: true,
            selector: 'createnewdealfrompropertypanel',
            xtype: 'createnewdealfrompropertypanel'
        },
		createnewsalesdealfrompropertypanel: 'createnewsalesdealfrompropertypanel',
		createnewdealfrompropertygrid: 'createnewdealfrompropertygrid',
		createnewdealfrompropertycontactgrid: 'createnewdealfrompropertycontactgrid'
    },

    init: function(application) {
        this.control({
            'createnewdealfrompropertypanel':{
                close: 'onCreateNewDealFromPropertyPanelClose',
				afterrender: 'loadCreateNewDealFromPropertyPanelDetails'
            },
            'createnewdealfrompropertypanel #createnewdealbtn':{
                click : 'onCreateNewDealBtnClick'
            },
            'createnewdealfrompropertygrid':{
                selectionchange: 'onCreateNewDealPropertySelChange',
				afterrender: 'onCreateNewDealFromPropertyGridRender'
            },
			'createnewdealfrompropertygrid #primaryProperty':{
                beforecheckchange: this.primaryPropertyCheckboxChanged
            },
			'tab-property-layout #adddealpropertygrid':{
                itemclick: this.onDealAllPropertiesGridpanelItemClick
            },
			/*'tab-property-layout button[action="savedealpropertybtn"]':{
				click: 'saveDealPropertyBtnClick'
			},*/
			'createnewsalesdealfrompropertypanel':{
                close: 'onCreateNewSalesDealFromPropertyPanelClose',
				afterrender: 'ldCrtNwSalesDealFrmPropPanelDt'
            },
			'createnewsalesdealfrompropertypanel #createnewdealbtn':{
                click : 'onCreateNewSalesDealBtnClick'
            },
			'createnewsalesdealfrompropertypanel createnewdealfrompropertycontactgrid':{
				afterrender: 'onCreateNewDealContactGridRightClick',
				validateedit: this.beforeCntTypeComplete
            },			
			'createnewdealfrompropertycontactgrid button[action="showaddnewdealcontactbtn"]':{
				click: 'showAddDealContactView'
			},
			'createnewdealcontactgridmenu menuitem':{
                click: this.createNewDealContactGridMenuItemClick
            },
			'createnewdealfrompropertygrid button[action="showaddnewdealpropertybtn"]':{
				click: 'showAddDealPropertyView'
			}
        });
    },
	
	/******************************************** Create New Sale Deal Starts ******************************/	
	
	CreateNewSalesDealClick: function(gridType) {
        console.log("Create New Sales Deal Click");
        var me = this,createnewsalesdealfrompropertypanel,tabpanel,maingrid,context,rec,eastregion;
		if(gridType=="propertygrid"){
			tabpanel = me.getController('MainController').getMainviewport().down('tab-properties');
			context = tabpanel.getActiveTab();
			maingrid = context.down('tab-property-grid');
			eastregion = context.down('#property-east-panel');
			eastregion.config.backview = "propertydetail";
		} else if(gridType=="cntgrid"){
			tabpanel = me.getController('MainController').getMainviewport().down('tab-contacts');
			context = tabpanel.getActiveTab();
			maingrid = context.down('tab-contact-grid');
			eastregion = context.down('#contact-east-panel');
			eastregion.config.backview = "contactdetail";
		} else if(gridType=="salesgrid"){
			tabpanel = me.getController('MainController').getMainviewport().down('tab-sales');
			context = tabpanel.getActiveTab();
			rec = context.record;
			//maingrid = context.down('tab-sales-grid');
			eastregion = context.down('[region=east]');
			eastregion.config.backview = "salesdetail";
		}
		if(maingrid){
			if(maingrid.getSelectionModel().hasSelection()){
				rec = maingrid.getSelectionModel().getSelection()[0];
			}
		}
		
		if(eastregion.down('createnewsalesdealfrompropertypanel')){
			createnewsalesdealfrompropertypanel = eastregion.down('createnewsalesdealfrompropertypanel');
			me.ldCrtNwSalesDealFrmPropPanelDt(createnewsalesdealfrompropertypanel);
		} else {
        	createnewsalesdealfrompropertypanel = Ext.create('DM2.view.CreateNewSalesDealFromPropertyPanel');
		}
		if(gridType!="salesgrid"){
   			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.setTitle("Create New Deal");
			eastregion.getHeader().hide();
		}
		eastregion.getLayout().setActiveItem(createnewsalesdealfrompropertypanel);		
		createnewsalesdealfrompropertypanel.setTitle("Create New Sale");
    },
	
    onCreateNewSalesDealFromPropertyPanelClose: function(panel) {
		var me = this;
		if(panel.up('tab-property-layout')){
			var context = panel.up('tab-property-layout');
			me.getController('PropertyController').closeRecvDealDetailDocs(context);
		} else if(panel.up('tab-contact-layout')){
			var context = panel.up('tab-contact-layout');
			me.getController('ContactController').closeRecvDealDetailDocs(context);
		} else if(panel.up('tab-sales-detail')){
			var context = panel.up('tab-sales-detail');
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
    },
	
	ldCrtNwSalesDealFrmPropPanelDt: function(panel){
		var me = this;	
		var createnewdealpanel = panel;
		panel.add(Ext.create('DM2.view.CreateNewDealFromPropertyGrid'));
		panel.add(Ext.create('DM2.view.CreateNewDealFromPropertyContactGrid'));
		panel.add(Ext.create('DM2.view.CreateNewDealFromPropertyDocsGridPanel'));
		if(!panel.down('createnewdealusergridpanel')){
			panel.add(Ext.create('DM2.view.CreateNewDealUserGridPanel'));
		}
		
		var statuscmb = createnewdealpanel.down('#status');		
		statuscmb.setValue(statuscmb.getStore().getAt(0));
			
		var office_fld = createnewdealpanel.down('#office');
		var office_st = office_fld.getStore();
		
		var userst = Ext.getStore('Users');
		var glboffice_st = Ext.getStore('Office');

		if(glboffice_st.isLoaded()){
			office_st.loadData(glboffice_st.data.items)
			office_fld.setValue(office_st.getAt(0));
			office_st.each(function(record) {
				if(record.get('code')==userst.getAt(0).get('office')) {
					office_fld.setValue(record.get('code'));
				}     
			},this);
		}
					
		if(panel.up('tab-property-layout')){
			context = panel.up('tab-property-layout');
			//Load Property
			var propertygrid = context.down('tab-property-grid');
			if(propertygrid.getSelectionModel().hasSelection()) {
				row = propertygrid.getSelectionModel().getSelection()[0];
				me.loadFromPropertyTable(context,row,'createnewdeal');
			}			
			//Load Property Contacts
			me.loadContactsPerProperty(context,panel);
			
			//Load Property Docs
			me.loadDocsPerProperty(context,panel);
		}
		if(panel.up('tab-contact-layout')){
			context = panel.up('tab-contact-layout');
			//Load Contact
			var cntgrid = context.down('tab-contact-grid');
			if(cntgrid.getSelectionModel().hasSelection()) {
				row = cntgrid.getSelectionModel().getSelection()[0];
				me.addContactToContactGrid(row,context);
			}
			var saleTypeCmb = createnewdealpanel.down('#saleType');
			saleTypeCmb.setValue('B');
		}
		//load Logged In User Info
		var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
		me.getController('CreateNewDealController').addLoggedInUser(userid,panel);
	},
	
	onCreateNewSalesDealBtnClick: function(btn) {
        var me = this,context;
		if(btn.up('tab-property-layout')){
			context = btn.up('tab-property-layout');
		} else if(btn.up('tab-contact-layout')){
			context = btn.up('tab-contact-layout');
		} else if(btn.up('tab-sales-detail')){
			context = btn.up('tab-sales-detail');
		}
				
        var createnewsalesdealfrompropertypanel = context.down('createnewsalesdealfrompropertypanel');
        //var fileno    = Number(createnewsalesdealfrompropertypanel.down('#filenumber').getValue());
        var status    = createnewsalesdealfrompropertypanel.down('#status').getValue();
        var nameval   = createnewsalesdealfrompropertypanel.down('#name').getValue();
        var officeval = createnewsalesdealfrompropertypanel.down('#office').getValue();
		var saleTypeval   = createnewsalesdealfrompropertypanel.down('#saleType').getValue();
        var priceeval = createnewsalesdealfrompropertypanel.down('#price').getValue();
		
		var percentCommissionval = createnewsalesdealfrompropertypanel.down('numberfield[name="percentCommission"]').getValue();
		var flatAmountCommissionval = createnewsalesdealfrompropertypanel.down('field[name="flatAmountCommission"]').getValue();
		var commissionTypeVal = createnewsalesdealfrompropertypanel.down('field[name="commissionType"]').getValue();

        var propgrid = createnewsalesdealfrompropertypanel.down('#createnewdealfrompropertygrid');
        var propSel = propgrid.getSelectionModel().getSelection();
        var deal_has_property = [];
		var newdeal_has_property = [];
        for(var j=0;j<propSel.length;j++){
			//console.log("primaryProperty : "+propSel[j].get('primaryProperty'));
            var property_id = propSel[j].get('propertyid');
            /*var propObj = {
                'idProperty'  :property_id,
				'saleType'  :saleTypeval,
				'price'  :priceeval,
				'percentCommission'  :percentCommissionval,
				'flatAmountCommission'  :flatAmountCommissionval,
				'commissionType' : commissionTypeVal
            };
            deal_has_property.push(propObj);*/
			
			var propObj = {
                'idProperty'  :property_id,
				'isPrimary' : propSel[j].get('primaryProperty')
            };
            newdeal_has_property.push(propObj);
        }
		
		//if(deal_has_property.length==0){
			var propObj = {
				'saleType'  :saleTypeval,
				'price'  :priceeval,
				'percentCommission'  :percentCommissionval,
				'flatAmountCommission'  :flatAmountCommissionval,
				'commissionType' : commissionTypeVal
            };
            deal_has_property.push(propObj);
		//}
		if(saleTypeval=="O"){
			deal_has_property[0].saleType = "S";
			var propObj = {
				'saleType'  :"B",
				'price'  :priceeval,
				'percentCommission'  :percentCommissionval,
				'flatAmountCommission'  :flatAmountCommissionval,
				'commissionType' : commissionTypeVal
            };
            deal_has_property.push(propObj);
		}
		//Get Contacts List
		var cntgrid  = createnewsalesdealfrompropertypanel.down('#createnewdealfrompropertycontactgrid');
        var contSel = cntgrid.getSelectionModel().getSelection();
        var deal_has_contact = [];
        for(var j=0;j<contSel.length;j++){
            var contact_id = contSel[j].get('contactid');
            var cntObj = {
                'idContact'  :contact_id,
				'contactType' : contSel[j].get('type')
            };
            deal_has_contact.push(cntObj);
        }
		
		//Get Docs List
		var docgrid  = createnewsalesdealfrompropertypanel.down('#createnewdealfrompropertydocsgridpanel');
        var docSel = docgrid.getSelectionModel().getSelection();
        var deal_has_doc = [];
		for(var j=0;j<docSel.length;j++){
            var docid = docSel[j].get('docid');
            var name  = docSel[j].get('name');
            var desc  = docSel[j].get('desc');
            var ext     = docSel[j].get('ext');
            var format  = docSel[j].get('format');
            var clsfctn = docSel[j].get('clsfctn');
            var url  = docSel[j].get('url');
            /*var docObj = {
                //'doc_id'  :docid,
                "name": name,
                "description": desc,
                "ext": ext,
                "format": format,
                "classification": clsfctn,
                "url": url
            };*/
			var docObj = {
                'requestType' : "RepositoryCopyGeneral",
                //'paramater1'  :  associateid.toString(),
                'paramater2'  :  docid.toString(),
                'paramater3'  :  name,
                'paramater4'  :  clsfctn,
                'paramater5'  :  desc
            };
            deal_has_doc.push(docObj);
        }
		
		// Get The User List
		var usergrid = createnewsalesdealfrompropertypanel.down('#createnewdealusergridpanel');
        var userSel = usergrid.getSelectionModel().getSelection();
        var deal_has_user = [];
		var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
        for(var j=0;j<userSel.length;j++){
            var userid = userSel[j].get('idDealxUser');
            var roleid = userSel[j].get('Role_idRole');
            var active = userSel[j].get('active');
            var csrid = userSel[j].get('csrid');
			var userName = userSel[j].get('user');
            var userObj = {
                //'CSR_idCSR'  :csrid,
				'userName' : userName,
                //'assignUserName' : userid,
                'assignDateTime' : new Date(),
                'idRole':roleid/*,
                'active':active*/
            };
            deal_has_user.push(userObj);
        }
		
		var data = [{
            'dealType' : 'S',
			'status' : status,
            //'fileNumber' : fileno,
            'dealName' : nameval,
            'office' : officeval.toString(),
            'SaleList' : deal_has_property,
			'DealxPropList' : newdeal_has_property,
			'DealxContList' : deal_has_contact,
			//'DocumentList' : deal_has_doc,
			'DealxUserList' : deal_has_user
        }];
		me.createNewSalesDeal(data,context);
    },
	
	createNewSalesDeal: function(data,context){
		console.log("Create New Sales Deal Call.");
		console.log(data);
		var me = this;
		var createnewsalesdealfrompropertypanel = context.down('createnewsalesdealfrompropertypanel');
		createnewsalesdealfrompropertypanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'RSaleDeal',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                createnewsalesdealfrompropertypanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					var tmpdealid = 0;
                    for(var k=0;k<obj.txsummary.length;k++){
                        if(obj.txsummary[k].idDeal){
                            tmpdealid = obj.txsummary[k].idDeal;
                        }
                    }
					
                    console.log("Temp Deal Id"+tmpdealid);
					
                    me.onCreateNewSalesDealFromPropertyPanelClose(createnewsalesdealfrompropertypanel);
					var filterstarr = [];
					me.getController("SalesController").loadAllSales(filterstarr); //Load All Deals
		
					me.getController('DashboardController').showSales(me.getController('DashboardController').getDashboardcontainer().down('#salesbtn'));
					
					me.loadModifiedRecord(tmpdealid,context,"sd");
					//me.uploadDealDocs(context,deal_has_doc,tmpdealid);					
                } else {
                    Ext.Msg.alert('Failed', "Please try again.");
                }
            },
            failure: function(response, opts) {
                createnewsalesdealfrompropertypanel.setLoading(false);
                Ext.Msg.alert('Failed', "Please try again.");
            }
        });
	},
	/******************************************** Create New Sale Deal Ends ******************************/

    CreateNewDealClick: function() {
        console.log("Create New Deal Click");
        var me = this;
		var tabpropertypanel = me.getController('MainController').getMainviewport().down('tab-properties');
		var context = tabpropertypanel.getActiveTab();
		
		var propertygrid = context.down('tab-property-grid');
		if(propertygrid.getSelectionModel().hasSelection()){
			rec = propertygrid.getSelectionModel().getSelection()[0];
		}
		
		var eastregion = context.down('#property-east-panel');
		
		var createnewdealfrompropertypanel;
		if(context.down('#property-east-panel').down('createnewdealfrompropertypanel')){
			console.log("Use Old Panel");
			createnewdealfrompropertypanel = context.down('#property-east-panel').down('createnewdealfrompropertypanel');
			me.loadCreateNewDealFromPropertyPanelDetails(createnewdealfrompropertypanel);
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	createnewdealfrompropertypanel = Ext.create('DM2.view.CreateNewDealFromPropertyPanel');//me.getCreatenewdealfrompropertypanel();
		}
   		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(createnewdealfrompropertypanel);	
		eastregion.setTitle("Create New Deal");
		eastregion.getHeader().hide();
		eastregion.config.backview = "propertydetail";
		//addprpcontactform.config.backview = "propertycontacts";
		createnewdealfrompropertypanel.setTitle("Create New Deal");
		//var filterstarr = [];
		//me.getController('DealDetailController').loadContactAllSelBuff(filterstarr,context);
    },
	
	loadCreateNewDealFromPropertyPanelDetails: function(panel){
		var me = this;	
		var createnewdealpanel = panel;
		var statuscmb = createnewdealpanel.down('#status');		
		statuscmb.setValue("CX");
			
		var office_fld = createnewdealpanel.down('#office');
		var office_st = office_fld.getStore();
		
		var userst = Ext.getStore('Users');
		var glboffice_st = Ext.getStore('Office');

		if(glboffice_st.isLoaded()){
			office_st.loadData(glboffice_st.data.items)
			office_fld.setValue(office_st.getAt(0));
			office_st.each(function(record) {
				if(record.get('code')==userst.getAt(0).get('office')) {
					office_fld.setValue(record.get('code'));
				}     
			},this);
		}		
		
		var mtgtypeSt = createnewdealpanel.down('combobox[name="mortgageType"]').getStore();		
		var selectionList_st = Ext.getStore('SelectionList');		
		if(selectionList_st.isLoaded()){
			mtgtypeSt.loadData(selectionList_st.data.items);
			mtgtypeSt.filter("selectionType","MT");
			mtgtypeSt.each( function(record){
				if(record.get('isDefault')){
					createnewdealpanel.down('combobox[name="mortgageType"]').setRawValue(record.get('selectionDesc'));
				}
			});
		}
		
		if(panel.up('tab-property-layout')){
			context = panel.up('tab-property-layout');
			//Load Property
			var propertygrid = context.down('tab-property-grid');
			if(propertygrid.getSelectionModel().hasSelection()) {
				row = propertygrid.getSelectionModel().getSelection()[0];
				me.loadFromPropertyTable(context,row,'createnewdeal');
			}			
			//Load Property Contacts
			me.loadContactsPerProperty(context,panel);
			
			//Load Property Docs
			me.loadDocsPerProperty(context,panel);
			
			//load Logged In User Info
			var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
			me.getController('CreateNewDealController').addLoggedInUser(userid,panel);
		}
	},
	
	loadFromPropertyTable: function(context,row,mode){
		var me = this;
		var propertymasterid = row.data.idPropertyMaster;
		Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
			method:'GET',
            url:DM2.view.AppConstants.apiurl+'mssql:Property',
            params: {
				filter:'magicMasterID='+propertymasterid
			},
            scope:this,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                if(response.status == 200){
					if(obj.length>0){
						if(mode=="createnewdeal"){
							//Property exist so add it in grid
							console.log("Property exist so add it in grid");
							me.addPropertyToPropertyGrid(obj[0],context);
						} else if(mode=="addpropertytodeal") {
							me.getController('DealDetailController').addPropertyToDeal(context,obj[0].idProperty);
						} else if(mode=="createnewdealaddprop") {
							me.getController('CreateNewDealController').addNewPropertyToDeal(context,obj[0]);
						} else if(mode=="createnewdealfrompropaddprop") {
							me.getController('CreateNewDealFromPropertyController').addNewPropertyToDeal(context,obj[0]);
						}
					} else {
						console.log("Property does not exist so create it");
						 /*Ext.Msg.show({
							title:'Create Property?',
							message: 'Property does not exist.Are you sure you want to create the property?',
							buttons: Ext.Msg.YESNO,
							icon: Ext.Msg.QUESTION,
							fn: function(btn) {
								if (btn === 'yes') {
									*/
									//Create Property Record linked to Property master id
									me.createPropertyRec(context,row,mode);
								/*} else {
									var createnewdealfrompropertygrid;
									if(context.down('#property-east-panel').getLayout().getActiveItem().xtype =="createnewsalesdealfrompropertypanel"){
										createnewdealfrompropertygrid = context.down('createnewsalesdealfrompropertypanel').down('#createnewdealfrompropertygrid');										
									} else {
										createnewdealfrompropertygrid = context.down('createnewdealfrompropertypanel').down('#createnewdealfrompropertygrid');
									}
									createnewdealfrompropertygrid.store.removeAll();
								}
							}
						});*/			
					}
                } else {
                    console.log("Property load Failure.");
                    Ext.Msg.alert('Property load Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Property load Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Property load Failed', obj.errorMessage);
            }
        });		
	},
	
	createPropertyRec: function(context,row,mode){
		var me = this;
		console.log("Create Property Record");		
		var data = {
			magicMasterID : row.data.idPropertyMaster,
			streetNumber : row.data.StreetNumber,
			streetName : row.data.StreetName,
			city : row.data.Township,
			state : row.data.State,
			zipCode : row.data.ZipCode,
			buildingClass: row.data.BuildingClass,
			stories : row.data.Stories,
			//yearBuilt : row.data.YearBuilt.toString(),
			units : row.data.Units
		};
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'mssql:Property',
			scope:this,
			method:"POST",
			params: Ext.util.JSON.encode(data),
			success: function(response, opts) {
				var obj = Ext.decode(response.responseText);
				if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201) {
					if(obj.txsummary.length > 0){
						if(mode=="createnewdeal"){
							me.addPropertyToPropertyGrid(obj.txsummary[0],context);
						} else if(mode=="addpropertytodeal") {
							me.getController('DealDetailController').addPropertyToDeal(context,obj.txsummary[0].idProperty);
						} else if(mode=="createnewdealaddprop") {
							me.getController('CreateNewDealController').addNewPropertyToDeal(context,obj.txsummary[0]);
						} else if(mode=="createnewdealfrompropaddprop") {
							me.getController('CreateNewDealFromPropertyController').addNewPropertyToDeal(context,obj.txsummary[0]);
						}
					}
				} else {
					console.log("Create Property Failed.");
					Ext.Msg.alert('Create Property Failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				//docselrepopanel.setLoading(false);
				var obj = Ext.decode(response.responseText);
				Ext.Msg.alert('Create Property', obj.errorMessage);
			}
		});
	},
	
	addPropertyToPropertyGrid: function(obj,context){
		var me = this;
		console.log("Add Property to Property grid in create new deal view");
		
		var rec = Ext.create('DM2.model.Property',{
		    propertyid  : obj.idProperty,
            street_no   : obj.streetNumber,
			street      : obj.streetName,
            city        : obj.city,
            state       : obj.state,
			primaryProperty : true
        });
		
		var createnewdealfrompropertygrid;
		if(context.down('#property-east-panel').getLayout().getActiveItem().xtype =="createnewsalesdealfrompropertypanel"){
			createnewdealfrompropertygrid = context.down('createnewsalesdealfrompropertypanel').down('#createnewdealfrompropertygrid');
		} else {
			createnewdealfrompropertygrid = context.down('createnewdealfrompropertypanel').down('#createnewdealfrompropertygrid');
		}
		createnewdealfrompropertygrid.store.removeAll();		
        createnewdealfrompropertygrid.store.insert(0, rec);
		createnewdealfrompropertygrid.getSelectionModel().select(0,true);
	},
	
	loadContactsPerProperty: function(context,panel){
		var me = this,propertyid;
		
		var propertygrid = context.down('tab-property-grid');
		if(propertygrid.getSelectionModel().hasSelection()) {
			row = propertygrid.getSelectionModel().getSelection()[0];
			propertyid = row.data.idPropertyMaster;
		}else{
			return false;
		}
		
		var grid = panel.down('#createnewdealfrompropertycontactgrid');  
		var prpcntst = grid.getStore();
		
        prpcntst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        prpcntst.load({
            params:{
                filter :'idPropertyMaster = '+propertyid
            },
            callback: function(records, operation, success){
			}
		});
	},
	
	loadDealProperty: function(dealid,panel){
		var me = this;
		var grid = panel.down('#createnewdealpropertygrid');  
        var dealpropertyst = grid.getStore();
		
        dealpropertyst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealpropertyst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				if(grid)
				{
					grid.getSelectionModel().selectAll();
				}
			}
		});
	},
	
	loadDocsPerProperty: function(context,panel) {
        var me = this,propertyid;
		
		var propertygrid = context.down('tab-property-grid');
		if(propertygrid.getSelectionModel().hasSelection()) {
			row = propertygrid.getSelectionModel().getSelection()[0];
			propertyid = row.data.idPropertyMaster;
		} else {
			return false;
		}
		
		var grid = panel.down('#createnewdealfrompropertydocsgridpanel');  
		var propdoc_st = grid.getStore();
        propdoc_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        propdoc_st.load({
            params:{
                filter :'Mast_MasterID = '+propertyid
            },
            callback: function(records, operation, success){
            }
        });
    },
	
    onCreateNewDealFromPropertyPanelClose: function(panel) {
		var me = this;
		var context = panel.up('tab-property-layout');
		me.getController('PropertyController').closeRecvDealDetailDocs(context);
    },
	
    onCreateNewDealBtnClick: function(btn) {
        var me = this;
		
		var context = btn.up('tab-property-layout');
				
        var createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
        var fileno    = Number(createnewdealfrompropertypanel.down('#filenumber').getValue());
        var status    = createnewdealfrompropertypanel.down('#status').getValue();
        var nameval   = createnewdealfrompropertypanel.down('#name').getValue();
        var officeval = createnewdealfrompropertypanel.down('#office').getValue();
		var mortgageTypeval = createnewdealfrompropertypanel.down('combobox[name="mortgageType"]').getValue();

        var propgrid = createnewdealfrompropertypanel.down('#createnewdealfrompropertygrid');
        var propSel = propgrid.getSelectionModel().getSelection();
        var deal_has_property = [];
        for(var j=0;j<propSel.length;j++){
			//console.log("primaryProperty : "+propSel[j].get('primaryProperty'));
            var property_id = propSel[j].get('propertyid');
            var propObj = {
                'idProperty'  :property_id,
				'isPrimary' : propSel[j].get('primaryProperty'),
				'mortgageType' : mortgageTypeval
            };
            deal_has_property.push(propObj);
        }

        var cntgrid  = createnewdealfrompropertypanel.down('#createnewdealfrompropertycontactgrid');
        var contSel = cntgrid.getSelectionModel().getSelection();
        var deal_has_contact = [];
        for(var j=0;j<contSel.length;j++){
            var contact_id = contSel[j].get('contactid');
            var cntObj = {
                'idContact'   : contact_id,
				'contactType' : contSel[j].get('type')
            };
            deal_has_contact.push(cntObj);
        }

        var docgrid  = createnewdealfrompropertypanel.down('#createnewdealfrompropertydocsgridpanel');
        var docSel = docgrid.getSelectionModel().getSelection();
        var deal_has_doc = [];
		for(var j=0;j<docSel.length;j++){
            var docid = docSel[j].get('docid');
            var name  = docSel[j].get('name');
            var desc  = docSel[j].get('desc');
            var ext     = docSel[j].get('ext');
            var format  = docSel[j].get('format');
            var clsfctn = docSel[j].get('clsfctn');
            var url  = docSel[j].get('url');
            /*var docObj = {
                //'doc_id'  :docid,
                "name": name,
                "description": desc,
                "ext": ext,
                "format": format,
                "classification": clsfctn,
                "url": url
            };*/
			var docObj = {
                'requestType' : "RepositoryCopyGeneral",
                //'paramater1'  :  associateid.toString(),
                'paramater2'  :  docid.toString(),
                'paramater3'  :  name,
                'paramater4'  :  clsfctn,
                'paramater5'  :  desc
            };
            deal_has_doc.push(docObj);
        }

        var usergrid = createnewdealfrompropertypanel.down('#createnewdealusergridpanel');
        var userSel = usergrid.getSelectionModel().getSelection();
        var deal_has_user = [];
		var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
        for(var j=0;j<userSel.length;j++){
            var userid = userSel[j].get('idDealxUser');
            var roleid = userSel[j].get('Role_idRole');
            var active = userSel[j].get('active');
            var csrid = userSel[j].get('csrid');
			var userName = userSel[j].get('user');
            var userObj = {
                //'CSR_idCSR'  :csrid,
				'userName' : userName,
                //'assignUserName' : userid,
                'assignDateTime' : new Date(),
                'idRole':roleid/*,
                'active':active*/
            };
            deal_has_user.push(userObj);
        }
		
		var dealproperty_has_loan = [];
		var data = [{
            'status' : status,
			'dealType' : 'F',
			//'statusDate': "2015-01-08",
            'fileNumber' : fileno,
            'dealName' : nameval,
            'office' : officeval.toString(),
            'Deal_has_PropertyList' : deal_has_property,
            'Deal_has_ContactList'  : deal_has_contact,
            'Deal_has_UserList'     : deal_has_user//,
            //'DocumentList'   : deal_has_doc,
			//'LoanList' : dealproperty_has_loan
        }];
		me.createNewDeal(data,context,deal_has_doc);
    },
	createNewDeal: function(data,context,deal_has_doc) {
		console.log("Create New Deal Call.");
		console.log(data);
		var me = this;
		var createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
		createnewdealfrompropertypanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'Rdeals',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                createnewdealfrompropertypanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					var tmpdealid = 0;
                    for(var k=0;k<obj.txsummary.length;k++){
                        if(obj.txsummary[k].idDeal){
                            tmpdealid = obj.txsummary[k].idDeal;
                        }
                    }
					
                    console.log("Temp Deal Id"+tmpdealid);
					
                    me.onCreateNewDealFromPropertyPanelClose(createnewdealfrompropertypanel);
					
					me.uploadDealDocs(context,deal_has_doc,tmpdealid);
					
					/*
                    if(data[0].Deal_has_UserList.length>0){
                        var tmp_deal_has_user = [];
                        for(var i=0;i<data[0].Deal_has_UserList.length;i++){
                            var userObj = {
                                'idDeal'  :tmpdealid,
                                'assignerCSRID' : 1,
                                'eventDate' : new Date(),
                                'CSR_idCSR'   : data[0].Deal_has_UserList[i].CSR_idCSR,
                                'Role_idRole' : data[0].Deal_has_UserList[i].Role_idRole,
                                'active' : data[0].Deal_has_UserList[i].active
                            };
                            tmp_deal_has_user.push(userObj);
                        }
                        me.addDealHasUser(tmp_deal_has_user,tmpdealid,context);
                    }*/
                } else {
                    Ext.Msg.alert('Failed', "Please try again.");
                }
            },
            failure: function(response, opts) {
                createnewdealfrompropertypanel.setLoading(false);
                Ext.Msg.alert('Failed', "Please try again.");
            }
        });
	},
	
	uploadDealDocs: function(context,deal_has_doc,tmpdealid){
		var me = this;
		for(var i=0;i<deal_has_doc.length;i++){
			deal_has_doc[i].paramater1 = tmpdealid.toString();
		}
		if(deal_has_doc.length>0){
			//docselrepopanel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
                scope:this,
                method:"POST",
                params: Ext.util.JSON.encode(deal_has_doc),
                success: function(response, opts) {
                    //docselrepopanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
						me.getController("DealController").loadAllDeals(); //Load All Deals
			
						me.getController('DashboardController').showDeals(me.getController('DashboardController').getDashboardcontainer().down('#dealsbtn'));
						me.loadModifiedRecord(tmpdealid,context,"d");
                    } else {
                        console.log("Generate File Failed.");
                        Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    //docselrepopanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Generate File', obj.errorMessage);
                }
            });
		} else {
			me.getController("DealController").loadAllDeals(); //Load All Deals
			
			me.getController('DashboardController').showDeals(me.getController('DashboardController').getDashboardcontainer().down('#dealsbtn'));
			me.loadModifiedRecord(tmpdealid,context,"d");
		}
	},
	
    loadModifiedRecord: function(dealid,context,dealtype) {
        var me = this,urlendpoint="";
		if(dealtype=="sd"){
			urlendpoint = "v_rsaledeals";
		}else if(dealtype=="d"){
			urlendpoint = "v_rdeals";
		}
        //var dealdetailform = context.down('dealdetailsformpanel');
        //dealdetailform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+urlendpoint,
            scope:this,
            method:'GET',
            params: {
                filter:'idDeal='+dealid
            },
            success: function(response, opts) {
                //dealdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
					if(obj.length>0){
						console.log("Load Modified Deal Record.");
						//Refresh & Load Details;
						var tmprec = Ext.create("DM2.model.Deal",obj[0]);
						
						var baseTabPanel = context.up('tab-basecardpanel');
						if(dealtype=="sd"){
							var tabsalespanel = me.getController('MainController').getMainviewport().down('tab-sales');
							var tabsalesdealgrid = tabsalespanel.down('tab-sales-grid');
							//Show Deal Detail Record
							me.getController('SalesController').showDealDetail(tmprec,tabsalesdealgrid);
						} else if(dealtype=="d"){
							var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
							var tabdealgrid = tabdealpanel.down('tab-deal-grid');
							//Show Deal Detail Record
							me.getController('DealController').showDealDetail(tmprec,tabdealgrid);
						}
					}
                } else {
                    console.log("Load Modified Deal Record Failed.");
                    Ext.Msg.alert('Modify Deal Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                //dealdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Load Modified Deal', obj.errorMessage);
            }
        });
    },

    addDealHasUser: function(deal_has_user_arr,dealid,context) {
        console.log("Add User to Deal");
        var me = this;
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Deal_has_User',
            params: Ext.util.JSON.encode(deal_has_user_arr),
            scope:this,
            success: function(response, opts) {
                console.log("All User is added to deal.");
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 201){
					if(context.xtype == 'tab-deal-detail'){
						usergridpanel = context.down('dealdetailsformpanel').down('dealdetailusergridpanel');
					}
                    me.getController('DealUserController').loadDealUserDetailsWithParams(usergridpanel.config.alloractiveusermode,dealid,usergridpanel);
                } else {
                    console.log("All User is added to deal Failure.");
                    Ext.Msg.alert('Add user failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("All User is added to deal Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Add user failed', obj.errorMessage);
            }
        });
    },

    onCreateNewDealPropertySelChange: function(selModel, selected, eOpts) {
        console.log("Property Grid Selection Change.");
        var me = this,createnewdealfrompropertypanel,tmprec,tmpdealname = "",selRecPrimFlag=false;
		//var context = selModel.view.ownerCt.up('tab-deal-detail');
		if(selModel.view.ownerCt.up('createnewdealfrompropertypanel')){
			createnewdealfrompropertypanel = selModel.view.ownerCt.up('createnewdealfrompropertypanel');
		}
		if(selModel.view.ownerCt.up('createnewsalesdealfrompropertypanel')){
			createnewdealfrompropertypanel = selModel.view.ownerCt.up('createnewsalesdealfrompropertypanel');
		}
       for(var i=0;i<selected.length;i++){
            tmprec = selected[i];
            console.log(tmprec);
            console.log(tmprec.get('primaryProperty'));
            if(tmprec.get('primaryProperty')===true){
                tmpdealname = tmprec.get('street_no')+" "+tmprec.get('street');
				selRecPrimFlag = true;
            }
        }
        createnewdealfrompropertypanel.down('#name').setValue(tmpdealname);
		var createnewdealpropertygrid =  createnewdealfrompropertypanel.down("createnewdealfrompropertygrid");
		if(selRecPrimFlag==false && selected.length>0){
			for (var i = 0; i < createnewdealpropertygrid.getStore().getCount(); i++) {
				createnewdealpropertygrid.getStore().getAt(i).set('primaryProperty', false);
			}
			selected[0].set('primaryProperty',true);
		}
    },	
	
	onDealAllPropertiesGridpanelItemClick: function(grid, record, item, index, e, eOpts){
		var me = this;
		//var context = grid.up('tab-deal-detail');
        //context.down('dealdetailsformpanel').down('adddealpropertyform').getForm().loadRecord(record);
		grid.up('adddealpropertyform').getForm().loadRecord(record);
	},
	
	saveDealPropertyBtnClick: function(btn){
		var me = this;
		var tabpropertypanel = btn.up('tab-properties');
		var context = tabpropertypanel.getActiveTab();
		var createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
		var allpropertiesgridpanel = context.down('adddealpropertyform').down('#adddealpropertygrid');
		if (allpropertiesgridpanel.getSelectionModel().hasSelection()) {
			tmprec = allpropertiesgridpanel.getSelectionModel().getSelection()[0];
		}
		
		var rec = Ext.create('DM2.model.Property',{
		    propertyid      : tmprec.get('idProperty'),
            street_no       : tmprec.get('streetNumber'),
			street          : tmprec.get('streetName'),
            city            : tmprec.get('city'),
            state           : tmprec.get('state'),
			primaryProperty : false
        });
		var recnum = createnewdealfrompropertypanel.down('#createnewdealfrompropertygrid').store.getCount()+1;
        createnewdealfrompropertypanel.down('#createnewdealfrompropertygrid').store.insert(recnum, rec);
		createnewdealfrompropertypanel.down('#createnewdealfrompropertygrid').getSelectionModel().select(recnum,true);
		
		var eastregion = context.down('#property-east-panel');
		eastregion.getLayout().setActiveItem(createnewdealfrompropertypanel);
	},
	
	showAddDealContactView: function(btn){
		var me = this,adddealcontactform;
		console.log("Show Add Deal Contact View");
		var context = me.getController('DealDetailController').getTabContext(btn);
		var eastregion = context.down('[region=east]');
	
		if(eastregion.down('adddealcontactform')){
			adddealcontactform = eastregion.down('adddealcontactform');
		} else {
			console.log("Add New Deal Contact");
        	adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
		}
		eastregion.getLayout().setActiveItem(adddealcontactform);	
		eastregion.config.backview = "adddealcontactform";
		adddealcontactform.getForm().reset();
		var filterstarr = [];
		me.getController('DealDetailController').loadContactAllSelBuff(filterstarr,context);
		
		if(btn.up('createnewdealfrompropertypanel')){
			createpanelval = "createnewdealfrompropertypanel";
		} else if(btn.up('createnewsalesdealfrompropertypanel')){
			createpanelval = "createnewsalesdealfrompropertypanel";
		}
		adddealcontactform.config.backview = createpanelval;
	},
	
	onCreateNewDealContactGridRightClick: function(panel){
		var me = this;
		panel.getEl().on("contextmenu",function(e, t, eOpts) {
			console.log("Show Menu After Render of contact grid.");
			e.stopEvent();
			panel.contextMenu.showAt(e.getXY());	
			panel.contextMenu.config.viewPanel = panel;
		});		
	},
	
	createNewDealContactGridMenuItemClick: function(item, e, eOpts){
		var me = this;
        if(item.action==="addcontact"){
			var btn = item.up('menu').config.viewPanel.down('button[action="showaddnewdealcontactbtn"]');
			me.showAddDealContactView(btn);
        }
	},
	
	addNewContactToDealClick: function(adddealcontactform){
		var me = this,createnewdealpanel;
		var context = me.getController('DealDetailController').getTabContext(adddealcontactform);
		var eastregion = context.down('[region=east]');
		if(context.down('createnewdealfrompropertypanel')){
			createnewdealpanel = context.down('createnewdealfrompropertypanel');
		} else if(context.down('createnewsalesdealfrompropertypanel')){
			createnewdealpanel = context.down('createnewsalesdealfrompropertypanel');
		}
		
		var adddealcontactgrid = context.down('adddealcontactform').down('adddealcontactgrid');
		if (adddealcontactgrid.getSelectionModel().hasSelection()) {
			row = adddealcontactgrid.getSelectionModel().getSelection()[0];
		} else {
			Ext.Msg.alert('Failed', "Please select property from Allproperties Grid.");
			return false;
		}		
		
		var mainCntCount=0,cntTypeVal;
		var cntGrid = createnewdealpanel.down('createnewdealfrompropertycontactgrid');
		var cntSt = cntGrid.getStore();
		for(var j=0;j<cntSt.getCount();j++){
			var rec = cntSt.getAt(j);
			if(rec.get('contactType')=="MAIN CONTACT"){
				mainCntCount++;
			}
		}
		if(mainCntCount>0){
			cntTypeVal = "ACCOUNT MANAGER";
		} else {
			cntTypeVal = adddealcontactform.getForm().findField('contactType').getValue();
		}
		var rec = Ext.create('DM2.model.Contact',{
		    contactid : row.get('contactid'),
		    name      : row.get('name'),
            type      : cntTypeVal,
			phone     : row.get('phone'),
            email     : row.get('email'),
			isprimary : false
        });

		var recnum = cntGrid.store.getCount();//+1;
        cntGrid.store.insert(recnum, rec);
		cntGrid.getSelectionModel().select(recnum,true);

		eastregion.getLayout().setActiveItem(createnewdealpanel);		
	},
	
	beforeCntTypeComplete: function(editor, context){
		var me = this,mainCntCount=0;
		console.log(context.value);
		console.log(context.originalValue);
		if (context.value == "MAIN CONTACT") {
			var cntGrid = context.grid;
			var cntSt = cntGrid.getStore();
			for(var j=0;j<cntSt.getCount();j++){
				var rec = cntSt.getAt(j);
				if(rec.get('type')=="MAIN CONTACT"){
					mainCntCount++;
				}
			}
			console.log("mainCntCount"+mainCntCount);
			if(mainCntCount>0){
				context.cancel = true;
				return false;
			}
		}
	},
	
	showAddDealPropertyView: function(btn){
		var me = this,adddealpropertyform;
		console.log("Show Add Deal Property View");
		var context = me.getController('DealDetailController').getTabContext(btn);
		var eastregion = context.down('[region=east]');
	
		if(eastregion.down('adddealpropertyform')){
			adddealpropertyform = eastregion.down('adddealpropertyform');
		} else {
        	adddealpropertyform = Ext.create('DM2.view.AddDealPropertyForm');
		}
		eastregion.getLayout().setActiveItem(adddealpropertyform);	
		eastregion.config.backview = "adddealpropertyform";
		if(btn.up('createnewdealfrompropertypanel')){
			createpanelval = "createnewdealfrompropertypanel";
		} else if(btn.up('createnewsalesdealfrompropertypanel')){
			createpanelval = "createnewsalesdealfrompropertypanel";
		}
		adddealpropertyform.config.backview = createpanelval;
	},
	
	onCreateNewDealFromPropertyGridRender: function(panel){
		var me = this;
		panel.getEl().on("contextmenu",function(e, t, eOpts) {
			console.log("Show Menu After Render of property grid.");
			e.stopEvent();
			panel.contextMenu.showAt(e.getXY());	
			panel.contextMenu.config.viewPanel = panel;
		});
		
		var mtgtypeSt = Ext.getStore('MortgageTypeList');		
		var selectionList_st = Ext.getStore('SelectionList');		
		if(selectionList_st.isLoaded()){
			mtgtypeSt.loadData(selectionList_st.data.items);
			mtgtypeSt.filter("selectionType","MT");
			/*mtgtypeSt.each( function(record){
				if(record.get('isDefault')){
					//panel.down('#mortgageTypeProp').setRawValue(record.get('selectionDesc'));
				}
			});*/
		}
	},
	
	addNewPropertyToDealClick: function(adddealpropertyform){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(adddealpropertyform);
		var eastregion = context.down('[region=east]');
		var allpropertiesgridpanel = context.down('adddealpropertyform').down('#adddealpropertygrid');
		if (allpropertiesgridpanel.getSelectionModel().hasSelection()) {
			row = allpropertiesgridpanel.getSelectionModel().getSelection()[0];
			console.log(row.get('idPropertyMaster'));
			propertyid = row.get('idPropertyMaster');
			lastDealID = row.get('lastDealID');
			lastDealStatus = row.get('lastDealStatus');
			if(adddealpropertyform.config.backview  && (adddealpropertyform.config.backview == "createnewdealfrompropertypanel")){
				if((lastDealID==null) || (lastDealID!=null && lastDealStatus=='CL')){
				} else if(lastDealID!=null && lastDealStatus!='CL'){
					var title = "Property add failed";
					errText = "Property has deal with status not closed. Please try to add another property.";
					me.getController('DealDetailController').showToast(errText,title,'error',null,'tr',true);
					return false;
				}
			} 
		} else {
			Ext.Msg.alert('Failed', "Please select property from Allproperties Grid.");
			return false;
		}
		me.getController('CreateNewDealFromPropertyController').loadFromPropertyTable(context,row,'createnewdealfrompropaddprop');
	},
	
	addNewPropertyToDeal: function(context,obj){
		var me = this,createnewdealpanel;
		var eastregion = context.down('[region=east]');
		if(context.down('createnewdealfrompropertypanel')){
			createnewdealpanel = context.down('createnewdealfrompropertypanel');
		} else if(context.down('createnewsalesdealfrompropertypanel')){
			createnewdealpanel = context.down('createnewsalesdealfrompropertypanel');
		}
		
		var rec = Ext.create('DM2.model.Property',{
		    propertyid  : obj.idProperty,
            street_no   : obj.streetNumber,
			street      : obj.streetName,
            city        : obj.city,
            state       : obj.state,
			primaryProperty   : false,
			mortgageType : context.down('adddealpropertyform').getForm().findField('mortgageType').getValue()
        });
		
		var recnum = createnewdealpanel.down('createnewdealfrompropertygrid').store.getCount();//+1;
        createnewdealpanel.down('createnewdealfrompropertygrid').store.insert(recnum, rec);
		createnewdealpanel.down('createnewdealfrompropertygrid').getSelectionModel().select(recnum,true);

		eastregion.getLayout().setActiveItem(createnewdealpanel);		
	},
	
	primaryPropertyCheckboxChanged: function( checkColumn , rowIndex , checked , record , eOpts){
		var me = this,tmpdealname = "",createnewdealpanel;
		console.log("primary Property Checkbox Changed");
		var createnewdealpropertygrid = checkColumn.up('createnewdealfrompropertygrid');
		var record = createnewdealpropertygrid.getStore().getAt(rowIndex);
		//console.log(record);
		if(checked){
			/*var record = this.grid.store.getAt(index);
			if (record.data[this.dataIndex]) {
				record.set(this.dataIndex, false);
			} else {
				for (var i = 0; i < this.grid.store.getCount(); i++) {
					this.grid.store.getAt(i).set(this.dataIndex, false);
				}
				record.set(this.dataIndex, true);
			}*/
			createnewdealpropertygrid.getSelectionModel().select(rowIndex,true);
			for (var i = 0; i < createnewdealpropertygrid.getStore().getCount(); i++) {
				createnewdealpropertygrid.getStore().getAt(i).set('primaryProperty', false);
			}			
			record.set('primaryProperty', true);
			
			if(createnewdealpropertygrid.up('createnewdealfrompropertypanel')){
				createnewdealpanel = createnewdealpropertygrid.up('createnewdealfrompropertypanel');
			} else if(createnewdealpropertygrid.up('createnewsalesdealfrompropertypanel')){
				createnewdealpanel = createnewdealpropertygrid.up('createnewsalesdealfrompropertypanel');
			}

			tmpdealname = record.get('street_no')+" "+record.get('street');
			createnewdealpanel.down('textfield[name="name"]').setValue(tmpdealname);
		} else {
			//record.set('primaryProperty', true);
			return false;
		}
	},
	
	addContactToContactGrid: function(obj,context){
		var me = this,createnewdealfromcntgrid;
		console.log("Add Contact to Contact grid in create new deal view");
		
		var rec = Ext.create('DM2.model.Contact',{
		    contactid  : obj.data.contactid,
            name       : obj.data.displayName,
			type       : obj.data.contactType,
            phone      : obj.data.phone,
            email      : obj.data.email,
			isprimary  : true
        });
		
		createnewdealfromcntgrid = context.down('createnewsalesdealfrompropertypanel').down('#createnewdealfrompropertycontactgrid');
		createnewdealfromcntgrid.store.removeAll();		
        createnewdealfromcntgrid.store.insert(0, rec);
		createnewdealfromcntgrid.getSelectionModel().select(0,true);
	},		

	bindCreateNewSalesDealViewInfo: function(context){
		var me = this,createnewsalepanel;
		console.log("Bind Create New Sale View Info");
		if(context.down('salesdealdetailsformpanel')) {
			if(context.down('salesdealdetailsformpanel').down('createnewsalesdealfrompropertypanel')) {
				createnewsalepanel = context.down('salesdealdetailsformpanel').down('createnewsalesdealfrompropertypanel');			
				var createnewdealpropertygrid = createnewsalepanel.down('#createnewdealfrompropertygrid');
				if(createnewdealpropertygrid) {
					var newsaleprpst = createnewdealpropertygrid.getStore();
					newsaleprpst.loadData(context.dealPropertyData);			
					createnewdealpropertygrid.getSelectionModel().selectAll();
				}
				var createnewdealcontactgrid = createnewsalepanel.down('#createnewdealfrompropertycontactgrid');
				if(createnewdealcontactgrid) {
					var newsalecntst = createnewdealcontactgrid.getStore();
					newsalecntst.loadData(context.dealContactData);
				}
				var createnewdealdocsgridpanel = createnewsalepanel.down('#createnewdealfrompropertydocsgridpanel');
				if(createnewdealdocsgridpanel) {
					var newsaledocst = createnewdealdocsgridpanel.getStore();
					newsaledocst.loadData(context.dealDocsData);
				}
			}
		}
	}
});