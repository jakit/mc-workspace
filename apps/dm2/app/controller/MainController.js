Ext.define('DM2.controller.MainController', {
    extend: 'Ext.app.Controller',

    refs: {
        logincontainer: {
            autoCreate: true,
            selector: 'logincontainer',
            xtype: 'logincontainer'
        },
        mainviewport: 'mainviewport',
        dashboardcontainer: 'dashboardcontainer',
        dealbutton: 'dashboardcontainer splitbutton[itemId=dealsbtn]',
        dealbuttongotodeal: 'dashboardcontainer splitbutton textfield[itemId=gotodealfield]'
    },

    init: function(application) {
        this.control({
            'logincontainer field[name="username"]': {
                specialkey: this.enterBtnPressed
            },
            'logincontainer field[name="password"]': {
                specialkey: this.enterBtnPressed
            },
            'logincontainer #loginbtn': {
                click: this.loginBtnClick
            },
            'logincontainer #resetbtn': {
                click: this.resetBtnClick
            },
            // 'dealbutton': {
                // click: {
                    // fn: function(btn) {
                        // debugger;
                    // }
                // }
            // },
            'dealbuttongotodeal': {
                'keyup': {
                    fn: function(field, e) {
                        var me = this;
                        var splitbutton = field.up('splitbutton');
                        var val = Ext.String.trim(field.getValue());      
                        if(e.getKey() == e.ENTER && val) {
                            Ext.Ajax.request({
                                headers: {
                                    'Content-Type': 'application/json',
                                    Accept: 'application/json',
                                    Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                                },
                                url:DM2.view.AppConstants.apiurl+'v_rdeals',
                                params:{
                                    //filter : "idDeal = '" + val + "'"
									filter : "magicDealID = '" + val + "'"
                                },
                                scope: {
                                    me: me,
                                    splitbutton: splitbutton,
                                    field: field,
                                    val: val
                                },
                                method:'GET',
                                success: function(response, opts) {
                                    var me = this.me;
                                    var splitbutton = this.splitbutton;
                                    var field = this.field;
                                    var val = this.val;
                                    var rec;
                                    var grid = field.up('mainviewport').down('tab-deal-layout').down('dataview');
                                    splitbutton.setText('History Deals');
                                    field.setValue(null);
                                    splitbutton.menu.hide();
                                    var obj = Ext.decode(response.responseText);
                                    if(obj.length > 0) {
                                        rec = Ext.create('DM2.model.Deal', obj[0]);
                                        me.getController('DealController').showDealDetail(rec, grid);    
                                    } else {
                                        Ext.Msg.alert('Failed', "Please enter valid dealno.");
                                    }
                                },
                                failure: function(response, opts) {
                                    var me = this.me;
                                    var splitbutton = this.splitbutton;
                                    var field = this.field;
                                    var val = this.val;
                                    splitbutton.setText('History Deals');
                                    field.setValue(null);
                                    splitbutton.menu.hide();
                                    Ext.Msg.alert('Failed', "Please enter valid dealno.");
                                }
                            });                            
                        }
                    }
                }
            }
        });
    },
	
	onLaunch: function(){
		var me = this;
		console.log("Api Url : "+DM2.view.AppConstants.apiurl);
		//console.log(DM2.view.AppConstants.apiurl);
        var apiKeyVal = Ext.util.Cookies.get("apikey-"+DM2.view.AppConstants.apiurl);
		console.log("apiKeyVal"+apiKeyVal);
		//console.log(Ext.util.Cookies.clear("apikey-"+DM2.view.AppConstants.apiurl));
		if(apiKeyVal && apiKeyVal!=null){
            console.log("Api Key exist. So check whether it is valid or not?");
			var keyexpiretimeval = Ext.util.Cookies.get("keyexpiretime-"+DM2.view.AppConstants.apiurl);
			var expdate  = new Date(keyexpiretimeval);
			var currdate = new Date();
			console.log(expdate);
			console.log(currdate);
			
			if (expdate > currdate) {
				console.log("Expdate is greater than today date");
				me.reloadOnExpire(); /// Set the time interval for calling Reload on expire of token
				me.getMainviewport().config.apikey = apiKeyVal;
				
				me.getController('QuoteController').loadSelectionList(null,null,null);
				me.getController('DealModifyController').loadOfficeList();
				me.getController('ContactController').loadContactTypeStore();
				me.getController('UnderwritingController').loadUnderwrittingActions();
				me.getController('DealActivityController').loadAllActivityTracks();
				me.getController('DealUserController').loadRoles();
				me.loadLoggedInUserInfo();
				me.loadFunctionalPermission();
			} else {
				Ext.util.Cookies.clear("apikey-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("username-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("userid-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("keyexpiretime-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("apiserver-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("logintime-"+DM2.view.AppConstants.apiurl);
				me.showLoginView();
			}			
        } else {
            me.showLoginView();
        }
	},
	
	loadLoggedInUserInfo: function(){
		var me = this;
		var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
        var userst =  Ext.getStore('Users');
		
		var filterstarr = [];

        var filterst = "userName = '"+userid+"'";
        filterstarr.push(filterst);
		
        userst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        userst.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){
			}
		});
	},
	
	reloadOnExpire: function(){
		var me = this;
		console.log("Reload Page on Expire");
		var keyexpiretimeval = Ext.util.Cookies.get("keyexpiretime-"+DM2.view.AppConstants.apiurl);
		var expdate  = new Date(keyexpiretimeval);
		var currdate = new Date();
		console.log(expdate);
		console.log(currdate);
		
		if (expdate > currdate) {
			console.log("expdate > currdate");
			var rmTime = expdate - currdate;
			console.log("Expire Function will be called after : "+rmTime);
			setTimeout(function(){ 
				//console.log("Expire Function will be called after : "+rmTime);				
				Ext.util.Cookies.clear("apikey-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("username-"+DM2.view.AppConstants.apiurl);
				//Ext.util.Cookies.clear("userid-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("keyexpiretime-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("apiserver-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("logintime-"+DM2.view.AppConstants.apiurl);
				window.location.reload();
			}, rmTime);
			//}, 30000);
		}
	},
	
    showLoginView: function() {
        var me = this;
        var mainviewport = me.getMainviewport();
        var logincnt = me.getLogincontainer();
	
		mainviewport.removeAll();
		mainviewport.add(logincnt);
		
        //mainviewport.getLayout().setActiveItem(logincnt);
        logincnt.down('form').getForm().reset();
		
		var useridVal = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
		console.log("useridVal"+useridVal);
		//console.log(Ext.util.Cookies.clear("apikey-"+DM2.view.AppConstants.apiurl));
		if(useridVal && useridVal!=null){
			logincnt.down('form').getForm().findField('username').setValue(useridVal);
		}
		logincnt.down('form').getForm().findField('username').focus();
		me.getIpAddress();
    },

    loginBtnClick: function(btn) {
        console.log("Login Button Clicked");
        var me = this;
        var form = this.getLogincontainer().down('form').getForm();
        if (form.isValid()) {
            console.log("Form Is Valid.");
			console.log(form.getValues());
			var formvalobj = form.getValues();
			var browserarr = me.getBrowserParams();
			formvalobj.browsername = browserarr[0]; //
			formvalobj.browserversion = browserarr[1];
			formvalobj.ipaddress = DM2.view.AppConstants.ipaddress;
			
			formvalobj.projecturl = DM2.view.AppConstants.projecturl;
			formvalobj.versionurl = DM2.view.AppConstants.versionurl;
			formvalobj.group = DM2.view.AppConstants.group;
			
            me.getLogincontainer().down('form').setLoading(true);
            Ext.Ajax.request({
                headers: { 'Content-Type': 'application/json' },
                url:DM2.view.AppConstants.apiurl+'@authentication',
                params: Ext.util.JSON.encode(formvalobj),
                scope:this,
                success: function(response, opts) {
                    console.log("Login Success.");
                    me.getLogincontainer().down('form').setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    //Ext.Msg.alert('Success', action.result.msg);
                    if(obj.apikey){
                        me.getMainviewport().config.apikey = obj.apikey;
                        //Set the Api Key as cookie var.
                        console.log(obj.expiration);
                        var expireDate = new Date(obj.expiration);
                        Ext.util.Cookies.set("apikey-"+DM2.view.AppConstants.apiurl,obj.apikey,expireDate);
                        Ext.util.Cookies.set("username-"+DM2.view.AppConstants.apiurl,obj.name,expireDate);
                        Ext.util.Cookies.set("userid-"+DM2.view.AppConstants.apiurl,form.findField('username').getValue(),expireDate);
						Ext.util.Cookies.set("keyexpiretime-"+DM2.view.AppConstants.apiurl,expireDate,expireDate);
						Ext.util.Cookies.set("apiserver-"+DM2.view.AppConstants.apiurl,obj.apiserver,expireDate);
						var d = new Date();
						Ext.util.Cookies.set("logintime-"+DM2.view.AppConstants.apiurl,d,expireDate);								
			
						me.getController('QuoteController').loadSelectionList(null,null,null);
						me.getController('DealModifyController').loadOfficeList();
						me.getController('ContactController').loadContactTypeStore();
						me.getController('UnderwritingController').loadUnderwrittingActions();
						me.getController('DealActivityController').loadAllActivityTracks();
						me.getController('DealUserController').loadRoles();
						me.loadLoggedInUserInfo();
						me.loadFunctionalPermission();
						me.reloadOnExpire(); /// Set the time interval for calling Reload on expire of token						
                    } else {
                        Ext.Msg.alert('Failed', "Please enter correct username & password.");
                    }
                },
                failure: function(response, opts) {
                    console.log("Login Failure.");
                    me.getLogincontainer().down('form').setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    var errmsg = obj.errorMessage.replace("API key not found or not authorized: ", '');
                    Ext.Msg.alert('Login Failed', errmsg);
                }
            });
        }
    },

    enterBtnPressed: function(field, e) {
        // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
        // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
        if (e.getKey() == e.ENTER) {
            this.loginBtnClick();
        }
    },

    resetBtnClick: function(me) {
        this.getLogincontainer().down('form').getForm().reset();
		this.getLogincontainer().down('form').getForm().findField('username').focus();
    },

    getExtImg: function(url) {
        //console.log("Get Ext Image");
        var ext = /^.+\.([^.]+)$/.exec(url);
        if(ext === null){
            ext = "unknown";
        }else {
            ext = ext[1];
        }
        if(ext.toLowerCase()==="csv"){
            ext = "csv";
        }else if(ext.toLowerCase()==="doc" || ext.toLowerCase()==="docx"){
            ext = "doc";
        }else if(ext.toLowerCase()==="jpg" || ext.toLowerCase()==="png" || ext.toLowerCase()==="image" || ext.toLowerCase()==="gif"){
            ext = "image";
        }else if(ext.toLowerCase()==="mp3"){
            ext = "mp3";
        }else if(ext.toLowerCase()==="msg"){
            ext = "msg";
        }else if(ext.toLowerCase()==="pdf"){
            ext = "pdf";
        }else if(ext.toLowerCase()==="rar"){
            ext = "rar";
        }else if(ext.toLowerCase()==="txt"){
            ext = "txt";
        }else if(ext.toLowerCase()==="xls" || ext.toLowerCase()==="xlsx"){
            ext = "xls";
        }else if(ext.toLowerCase()==="zip"){
            ext = "zip";
        }else{
            ext = "unknown";
        }
        return ext;
    },
	
	getTargetFrame: function(extension){
		var target = "_blank";
		if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xls" || extension==="xlsx"){
			target = "_self";
		}
		return target;
	},
	
	loadFunctionalPermission: function(){
		var me = this;
		console.log("Load Functional Permission");
		
		var funPermStore = Ext.getStore('FunctionalPermissions');
		me.getMainviewport().config.funPermStore = funPermStore;
		
		funPermStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		me.getMainviewport().setLoading(true);
		funPermStore.load({
			scope: this,
			callback: function(records, operation, success) {
				me.getMainviewport().setLoading(false);
				if(success){
					me.createMainViewPortItems();
				}
			}
		});
	},
	
	createMainViewPortItems: function(){
		var me = this,btn,dashboardcontainer,activeItem;
		me.getController('DashboardController').showDashboardContainer();	
		// Set the Username Value
		var usernameval = "Hi, "+Ext.util.Cookies.get("username-"+DM2.view.AppConstants.apiurl);
		me.getController('DashboardController').setUserNameText(usernameval);
		
		dashboardcontainer = me.getDashboardcontainer();
		var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
		if(menuFunc==1){
			btn = dashboardcontainer.down('splitbutton[itemId=dealsbtn]');
			activeItem = me.getController('MainController').getMainviewport().down('tab-panel').down('tab-deals');
		} else {
			menuFunc = me.getController('MainController').getMenuStatus("salemenu");
			if(menuFunc==1){
				btn = dashboardcontainer.down('splitbutton[itemId=salesbtn]');
				activeItem = me.getController('MainController').getMainviewport().down('tab-panel').down('tab-sales');
			} else {
				btn = dashboardcontainer.down('button[itemId=propmasterbtn]');
				activeItem = me.getController('MainController').getMainviewport().down('tab-panel').down('tab-properties');
			}
		}
		me.getController('DashboardController').setTabPanel(btn, activeItem);
	},
	
	getGridKeyData: function(grid,gridname){
		var me = this;
		//var funpermdata = me.getController('MainController').getMainviewport().config.funPermStore.data.items;
		var funpermdata = Ext.getStore('FunctionalPermissions').data.items;
		console.log("funpermdata");
		console.log(funpermdata);
		if(funpermdata){
			Ext.Array.each(funpermdata, function(arritem, index, countriesItSelf) {
				//console.log(arritem);
				if (arritem.data.gui === gridname) {
					me.getController('MainController').applyFilterSortPermissionOnGrid(grid,arritem.data.col,arritem.data.functionActivity,arritem.data.allowed)
				}
			});
		}
	},
	
	applyFilterSortPermissionOnGrid: function(grid,col,func,allowed){
		var me = this;
		//console.log("col : "+col);
		if(col){
			//Disable Header Field Filter
			if(func=="filter" && allowed==0){
				var field = grid.HFAPI.getField(col);
				field[0].setDisabled(true);
			} else if(func=="sort" && allowed==0){			//Disable Header Field Sorter
				var gridColumns = grid.getHeaderContainer().getGridColumns();
				for (var i = 0; i < gridColumns.length; i++) {
				   if (gridColumns[i].dataIndex == col) {
					gridColumns[i].sortable = false;
				   }  
				 }
			}
		}
	},
	
	getMenuStatus: function(menuname){
		var me = this;
		//var funpermdata = me.getController('MainController').getMainviewport().config.funPermStore.data.items;
		var funpermdata = Ext.getStore('FunctionalPermissions').data.items;
		console.log("funpermdata");
		console.log(funpermdata);
		var allowed=1;
		if(funpermdata){
			Ext.Array.each(funpermdata, function(arritem, index, funpermdataItSelf) {
				//console.log(arritem);
				//console.log(arritem.data.gui + ":" + arritem.data.column);				
				if (arritem.data.functionActivity && arritem.data.functionActivity==menuname) {
					allowed = arritem.data.allowed;
				}
			});
		}
		/*
		if(menuname=="dealmenu"){
			allowed=1;
		} else if(menuname=="salemenu"){
			allowed=1;
		} else if(menuname=="bankmenu"){
			allowed=1;
		}*/
		return allowed;
	},
	
	getFunPermData: function(guiname){
		var me = this,guiitem = [];
		//var funpermdata = me.getController('MainController').getMainviewport().config.funPermStore.data.items;
		var funpermdata = Ext.getStore('FunctionalPermissions').data.items;
		console.log("funpermdata");
		console.log(funpermdata);
		if(funpermdata){
			Ext.Array.each(funpermdata, function(arritem, index, countriesItSelf) {
				//console.log(arritem);
				//console.log(arritem.data.column);				
				if (arritem.data.gui === guiname) {
					guiitem = arritem.data;
				}
			});
		}
		return guiitem;
	},
	
	getBrowserParams: function(){
		var me = this;
		var nVer = navigator.appVersion;
		var nAgt = navigator.userAgent;
		var browserName  = navigator.appName;
		var fullVersion  = ''+parseFloat(navigator.appVersion); 
		var majorVersion = parseInt(navigator.appVersion,10);
		var nameOffset,verOffset,ix;
		
		// In Opera 15+, the true version is after "OPR/" 
		if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
		 browserName = "Opera";
		 fullVersion = nAgt.substring(verOffset+4);
		}
		// In older Opera, the true version is after "Opera" or after "Version"
		else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
		 browserName = "Opera";
		 fullVersion = nAgt.substring(verOffset+6);
		 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
		   fullVersion = nAgt.substring(verOffset+8);
		}
		// In MSIE, the true version is after "MSIE" in userAgent
		else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
		 browserName = "Microsoft Internet Explorer";
		 fullVersion = nAgt.substring(verOffset+5);
		}
		// In Chrome, the true version is after "Chrome" 
		else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
		 browserName = "Chrome";
		 fullVersion = nAgt.substring(verOffset+7);
		}
		// In Safari, the true version is after "Safari" or after "Version" 
		else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
		 browserName = "Safari";
		 fullVersion = nAgt.substring(verOffset+7);
		 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
		   fullVersion = nAgt.substring(verOffset+8);
		}
		// In Firefox, the true version is after "Firefox" 
		else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
		 browserName = "Firefox";
		 fullVersion = nAgt.substring(verOffset+8);
		}
		// In most other browsers, "name/version" is at the end of userAgent 
		else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
				  (verOffset=nAgt.lastIndexOf('/')) ) 
		{
		 browserName = nAgt.substring(nameOffset,verOffset);
		 fullVersion = nAgt.substring(verOffset+1);
		 if (browserName.toLowerCase()==browserName.toUpperCase()) {
		  browserName = navigator.appName;
		 }
		}
		// trim the fullVersion string at semicolon/space if present
		if ((ix=fullVersion.indexOf(";"))!=-1)
		   fullVersion=fullVersion.substring(0,ix);
		if ((ix=fullVersion.indexOf(" "))!=-1)
		   fullVersion=fullVersion.substring(0,ix);
		
		majorVersion = parseInt(''+fullVersion,10);
		if (isNaN(majorVersion)) {
		 fullVersion  = ''+parseFloat(navigator.appVersion); 
		 majorVersion = parseInt(navigator.appVersion,10);
		}
		var browserarr = [];
		browserarr.push(browserName);
		browserarr.push(fullVersion);
		//browserarr.push(browserName);
		return browserarr;
		/*document.write(''
		 +'Browser name  = '+browserName+'<br>'
		 +'Full version  = '+fullVersion+'<br>'
		 +'Major version = '+majorVersion+'<br>'
		 +'navigator.appName = '+navigator.appName+'<br>'
		 +'navigator.userAgent = '+navigator.userAgent+'<br>'
		)*/
	},
	
	getIpAddress: function(){
		var me = this;
		Ext.Ajax.request({
                //headers: { 'Content-Type': 'application/json' },
                url:'resources/getip.php',
				method:'GET',
                scope:this,
                success: function(response, opts) {
					var obj = Ext.decode(response.responseText);
                    console.log("GetIpAddress Success : "+obj.ipaddress);
					DM2.view.AppConstants.ipaddress = obj.ipaddress;
                },
                failure: function(response, opts) {
                    console.log("Login Failure.");
                    //var obj = Ext.decode(response.responseText);
                    var errmsg = "Get Ip Address Failed";
                    Ext.Msg.alert('GetIpAddress Failed', errmsg);
                }
            });
	}
});