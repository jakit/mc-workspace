Ext.define('DM2.controller.BankController', {
    extend: 'Ext.app.Controller',

    refs: {
        bankcontactmenu: {
            autoCreate: true,
            selector: 'bankcontactmenu',
            xtype: 'bankcontactmenu'
        },
        addbankcontactform: {
            autoCreate: true,
            selector: 'addbankcontactform',
            xtype: 'addbankcontactform'
        },
        bankmenu: {
            autoCreate: true,
            selector: 'bankmenu',
            xtype: 'bankmenu'
        },
		bankgrid: 'tab-bank-grid',
		tabbanklayout : 'tab-bank-layout',
		banktabpanel : 'banktabpanel'
    },

    init: function(application) {
         this.control({
			 'tabbanklayout': {
				 afterrender: 'afterTabBankLayoutRender'
			 },
             'bankgrid': {
				 afterrender: 'afterBankGridRender',
                 select: this.loadBankDetailForm,
                 rowcontextmenu: this.onBankGridPanelRightClick
             },
			 'tab-bank-grid dataview': {
				itemlongpress: this.onBankGridItemLongPress
             },
             'bankcontactsgridpanel': {
                 rowcontextmenu: this.onBankContactsGridPanelRightClick
             },
			 'bankcontactsgridpanel dataview': {
				 itemlongpress: this.onBankContactsGridItemLongPress
             },
             'bankcontactmenu':{
                 click: this.onBankContactMenuClick
             },
             'addbankcontactform':{
                 close: this.addCloseBankContactForm
             },
             'addbankcontactform grid':{
                 select: this.onSelectFromAllContactBank
             },
             'addbankcontactform #savebankcontactbtn':{
                 click: this.addContactToBankBtnClick
             },
             'bankcontactsgridpanel #addbankcontactbtn':{
                 click: this.showAddBankContact
             },
             'bankmenu':{
                 click: this.onBankMenuClick
             },
             'bankdetailsform #savebankbtn':{
                 click:this.onSaveBankBtnClick
             },
			 'banktabpanel':{
				afterrender: this.bankTabPanelAfterRender,
				tabchange: this.bankTabChange
			 },
			 'banktabpanel #dealhistorycontacttab':{
				click: this.dealHistoryBankTabClick
			 },
			 'banktabpanel #dealhistorycontacttab menu':{
				click: this.dealHistoryBankTabMenuClick
			 },
			 'banktabpanel contactdealhistorygrid':{
				itemdblclick: this.onContactDealHistoryGridItemDblClick
			 }
         });
    },
	
	bankTabPanelAfterRender: function(cmp, eOpts){
		var me = this,starttab=1,tabItem;		
		var menuFunc = me.getController('MainController').getMenuStatus("dealmenu");
		if(menuFunc==1){
			var dealhistorycontacttab = Ext.create('DM2.view.DealHistoryContactTabMenu',{
				itemId:'dealhistorycontacttab',
				style:'margin-right:5px',
				ui:'tabsplitbutton'
			});
			cmp.tabBar.insert(starttab, dealhistorycontacttab);
			starttab++;
			//Insert Deal History Grid & Tab		
			tabItem = Ext.create('DM2.view.contact.ContactDealHistoryGrid',{
				iconCls: 'loanbtn',
				itemId : 'contactdealhistorygrid',
				title: 'Deal History',
				tabConfig: {
					hidden: true
				}
			});
			cmp.add(tabItem);
			starttab++;
		}		
	},
	
	bankTabChange: function(tabPanel, newCard, oldCard, eOpts){
		var me = this;
		console.log("Bank Tab Change");
		var dealhistorycontacttab = tabPanel.down('#dealhistorycontacttab');
		if(newCard.xtype=="contactdealhistorygrid" && newCard.getItemId()=="contactdealhistorygrid"){
			if(dealhistorycontacttab){dealhistorycontacttab.addCls("docactivebtncls");}
		} else /*if(newCard.xtype=="propertycontactsgridpanel")*/{
			if(dealhistorycontacttab){dealhistorycontacttab.removeCls("docactivebtncls");}
		}
	},
	dealHistoryBankTabClick: function(tabItem){
		console.log("Set Deal History Tab");
		var me = this;
		tabItem.up('tabpanel').setActiveTab(tabItem.up('tabpanel').down('#contactdealhistorygrid'));
		me.onCategoryChange("Contacts", tabItem.down('menu'),tabItem.up('tabpanel').down('#contactdealhistorygrid'),"deal");
	},
	
	onCategoryChange: function(newValue, menu, historyGrid,historyType) {
        var me = this,contactid,row,maingrid,dealid;
		dealhistory_st = historyGrid.getStore();
        var p = menu.up('tab-bank-detail');
        var anchor = "detail";
        if(!p) {
            anchor = "grid";
            p = menu.up('tab-bank-layout');            
        }
        if(anchor == "grid") {
            maingrid = p.down('tab-bank-grid');
			if(maingrid.getSelectionModel().hasSelection())	{
            	row = maingrid.getSelectionModel().getSelection()[0];
            	bankid = row.data.idBank;
			}else{
				return false;
			}
        } else {
            bankid = p.idBank;
        }
        
		var viewname;
		console.log(newValue);
		var filterstarr = [];
		if(newValue=="Addressess"){
			viewname = "address";
		}else if(newValue=="Streets"){
			viewname = "street";
		}else if(newValue=="Contacts"){
			viewname = "contact";
		}else if(newValue=="Company"){
			viewname = "company";
		}
		console.log("viewname"+viewname);
		var filterst = "reqtype="+viewname;
        filterstarr.push(filterst);
		var filterst1 = "bankid="+bankid;
        filterstarr.push(filterst1);
		
        var mainViewport = Ext.ComponentQuery.query('mainviewport')[0];
        
		var url;
		if(historyType=="deal"){
			url = DM2.view.AppConstants.apiurl+'DealHistoryByBankID';
		}
		
		/////
		var dealhistorystproxy = dealhistory_st.getProxy();
		var dealhistorystproxyCfg = dealhistorystproxy.config;
        dealhistorystproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+ mainViewport.config.apikey + ':1'
        };
		dealhistorystproxyCfg.originalUrl = url;
		dealhistorystproxyCfg.url = url;
		dealhistorystproxyCfg.extraParams = {
            filter :filterstarr
        };
		dealhistory_st.setProxy(dealhistorystproxyCfg);
		/////
		
        dealhistory_st.load();
    },
	
	dealHistoryBankTabMenuClick: function(menu, item, e, eOpts){
		console.log("Deal History Menu Clicked");
        var me = this;
        var text = item.text;
		// me.getDealdetailsformpanel().down('#dealdetailtabpanel').setActiveTab(6);
		menu.up('tabpanel').setActiveTab(menu.up('tabpanel').down('#contactdealhistorygrid'));
		me.onCategoryChange(text, menu, menu.up('tabpanel').down('#contactdealhistorygrid'), "deal");
	},
	
	onContactDealHistoryGridItemDblClick: function(dataview, record, item, index, e, eOpts){
		var me = this;
		var dealsbtn = dataview.up('viewport').down('#dealsbtn');
		me.getController('DashboardController').setTabPanel(dealsbtn, me.getController('MainController').getMainviewport().down('tab-panel').down('tab-deals'));
		
		var dealid = record.get('DM-dealNo');
		var magicDealID = record.get('LT-dealNo');
		record.set('dealid',dealid);
		record.set('idDeal',dealid);
		record.set('magicDealID',magicDealID);
		
		me.getController('DealController').showDealDetail(record, dataview.up('viewport').down('tab-deal-grid'));
	},
	
	afterTabBankLayoutRender: function(panel){
		var me = this;
		panel.down('#bank-east-panel').getHeader().hide();
	},
	
	afterBankGridRender: function(grid){
		var me = this;
		//var filterstarr = [];
		grid.store.on( 'load', function( store, records, options ) {
			//console.log("Bank Store Cnt : "+store.getCount());
			//console.log("Bank Store Total Cnt : "+store.totalCount);
			if(store.getCount()==0){
				// Clear Deal Tabs Store Data
				grid.getSelectionModel().deselectAll();
				var context = grid.up('tab-bank-layout');
				bankdetailsform = context.down('bankdetailsform');
				if(bankdetailsform){
					bankdetailsform.getForm().reset();
				}
				me.getController('BankController').clearBankContactGrid(context);
			} else {
				// Select first item if wants
			}
		});
	},
	
    loadAllBanks: function(filterstarr) {
        var me = this;
		
		var bank_st = Ext.getStore('Bankallbuff');
        var banksproxy = bank_st.getProxy();
		var banksproxyCfg = banksproxy.config;
        banksproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		bank_st.setProxy(banksproxyCfg);
		//bank_st.loadPage(1); // Load page 1
        bank_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                //console.log(success);
                if(success){
                    if(records.length > 0){
                        me.getBankgrid().getSelectionModel().select(0);
                    }
                }
            }
        });
    },

    loadBankDetailForm: function(selModel, record, index, eOpts) {
        var me = this;
		var activemenutab = selModel.view.ownerCt.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
        //var eastregion = this.getBankmenucontainer().down('#eastbankpanel');
        //eastregion.getLayout().setActiveItem(this.getBankmenucontainer().down('#bankdetailform'));
        me.getTabbanklayout().down('bankdetailsform').loadRecord(record);
        me.getTabbanklayout().down('bankdetailsform').setTitle("Bank Details");
        me.loadBankContacts(record.get('idBank'),context.down('bankcontactsgridpanel'));
    },
	
	clearBankContactGrid: function(tabItem){
		console.log("Clear Bank Contact");
		var me = this;
		var panel = tabItem.down('bankcontactsgridpanel');
		var bankcntst = panel.getStore();
		bankcntst.removeAll();
		me.afterBankContactDataLoad(panel);		
	},
	
	afterBankContactDataLoad: function(panel){
		var bankcntst = panel.getStore();
		var count = bankcntst.getCount();		
		var bankcnttoolbar = panel.down('toolbar');
		if(count>0){
			if(bankcnttoolbar){
				bankcnttoolbar.hide();
			}
		} else {
			if(bankcnttoolbar){
				bankcnttoolbar.show();
			}
		}
	},
	
    loadBankContacts: function(bankid,grid) {
        var me = this;
		if(grid){
			var bankcntst = grid.getStore();
			bankcntst.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			bankcntst.load({
				params:{
					filter :'bankid = '+bankid
				},
				callback: function(records, operation, success){
					me.afterBankContactDataLoad(grid);
				}
			});
		}
    },

    onBankContactsGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
        var me = this;
        me.showBankCntGridMenu(view,record,e);
    },
	
	showBankCntGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
        console.log("Show Menu for Bank Contact Add/Dealte");
        if(this.getBankcontactmenu()){
			this.getBankcontactmenu().spawnFrom = view;
            this.getBankcontactmenu().showAt(e.getXY());
			var contactid = record.get('contactid');
			me.getController('ContactController').buildContactSubMenu(contactid,this.getBankcontactmenu());
        }
	},
	
	onBankContactsGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showBankCntGridMenu(view,record,e);
	},

    onBankContactMenuClick: function(menu, item, e, eOpts) {
        console.log("Menu Item Clicked");
        if(item.text === "Add Contact"){
            console.log("Add Contact");
            this.showAddBankContact(item);
        }else if(item.text === "Remove Contact"){
            console.log("Remove Contact");
            this.removeBankContact(item);
        }
    },

    removeBankContact: function(cmp) {
        console.log("Remove Bank Contact");
        var me = this;
		//if(cmp.xtype == 'menuitem') {
		grid = cmp.up().spawnFrom;
		tabbankpanel = grid.up('tab-banks');
		context = tabbankpanel.getActiveTab();
		//}
        var bankcntgrid = context.down("#bankcontactsgridpanel");
        var row = bankcntgrid.getSelectionModel().getSelection()[0];
        console.log(row.get('idBank_has_contact'));
        var idBankxCont = row.get('idBankxCont');
        var bankid = row.get('bankid');

        Ext.Msg.show({
            title:'Delete Contact?',
            message: 'Are you sure you want to delete the Contact from Bank?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    bankcntgrid.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:BankxCont/'+idBankxCont+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            bankcntgrid.setLoading(false);
                            console.log("Removed Bank Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                me.loadBankContacts(bankid,bankcntgrid);
                            } else {
                                console.log("Delete Bank Contact Failure.");
                                Ext.Msg.alert('Delete Bank Contact failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            bankcntgrid.setLoading(false);
                            console.log("Delete Bank Contact Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    showAddBankContact: function(cmp) {
		var me = this,
            tabbankpanel, context, spawnFrom, grid;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			tabbankpanel = grid.up('tab-banks');
			context = tabbankpanel.getActiveTab();
		} else {
		    // when cmp is a button.
			tabbankpanel = cmp.up('tab-banks');
			context = tabbankpanel.getActiveTab();	    
		}
		eastregion  = context.down('#bank-east-panel');
		//eastregion.removeAll();
		var addbankcontactform;
		if(context.down('addbankcontactform')){
			console.log("Use Old Panel");
			addbankcontactform = context.down('addbankcontactform');
		}
		else {
			console.log("Create New east add deal doc Panel");
			addbankcontactform = Ext.create('DM2.view.AddBankContactForm');//me.getAddbankcontactform();
			eastregion.add(addbankcontactform);
		}
		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(addbankcontactform);			
		eastregion.setTitle("Add Contact");
		eastregion.getHeader().hide();			
        addbankcontactform.getForm().reset();  //Reset The Form

        var filterstarr = [];
        me.getController('ContactController').loadAllContacts(filterstarr);
    },

    addCloseBankContactForm: function(panel) {
		var me = this;
		var tabbankpanel = panel.up('tab-banks');
		var context = tabbankpanel.getActiveTab();	
		
		var eastregion = context.down('#bank-east-panel');
		eastregion.removeAll();
		//eastregion.setCollapsed(false);
		//eastregion.setVisible(false);
		
		var bankdetailsform;
		if(context.down('bankdetailsform')){
			console.log("Use Old Panel");
			bankdetailsform = context.down('bankdetailsform');
		}
		else {
			console.log("Create New east add deal doc Panel");
			bankdetailsform = Ext.create('DM2.view.BankDetailsForm');//me.getBankdetailsform();
			eastregion.add(bankdetailsform);
		}
		eastregion.setCollapsed(false);
		eastregion.setVisible(true);
		eastregion.getLayout().setActiveItem(bankdetailsform);			
		eastregion.setTitle("Bank Details");
		eastregion.getHeader().hide();
		
		var bankgrid = context.down('tab-bank-grid');
		if(bankgrid.getSelectionModel().hasSelection())
		{
			row = bankgrid.getSelectionModel().getSelection()[0];
			bankgrid.fireEvent('select',bankgrid.getSelectionModel(),row);
			//propmasterdetailsform.getForm().loadRecord(row);
			//propmasterdetailsform.setTitle("Property Details");
		}else{
			return false;
		}
    },

    onSelectFromAllContactBank: function(selModel, record, index, eOpts) {
		var me = this;
		var context = selModel.view.ownerCt.up('tab-banks').getActiveTab();	
        context.down('addbankcontactform').getForm().loadRecord(record);
    },

    addContactToBankBtnClick: function(btn) {
        console.log("Add Contact To Bank");
        var me = this;
		var context = btn.up('tab-banks').getActiveTab();
		
        var bankgrid = context.down("tab-bank-grid");
        if (bankgrid.getSelectionModel().hasSelection()) {
            var row = bankgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('idBank'));
            var bankid = row.get('idBank');
        } else {
            Ext.Msg.alert('Failed', "Please select bank from Allbankgrid.");
            return false;
        }
		var addbankcontactform = context.down('addbankcontactform');
        var allcntbankgrid = addbankcontactform.down('grid');
        if (allcntbankgrid.getSelectionModel().hasSelection()) {
            var row = allcntbankgrid.getSelectionModel().getSelection()[0];
            var idContact = row.get('contactid');
        } else {
            Ext.Msg.alert('Failed', "Please select bank from Allbankgrid.");
            return false;
        }

        var data = [{
            'idBank': bankid,
            'idContact' : idContact
        }];
        addbankcontactform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:BankxCont',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Add contact to bank is submitted.");
                addbankcontactform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 201){
                    me.loadBankContacts(bankid,context.down('bankcontactsgridpanel'));
                    me.addCloseBankContactForm(addbankcontactform);
                } else {
                    console.log("Add Contact to Bank Failure.");
                    Ext.Msg.alert('Contact add failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Add Contact to Bank Failure.");
                addbankcontactform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Contact add failed', obj.errorMessage);
            }
        });
    },
	
	onBankGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		console.log("Bank Grid Item Long Presss");
		var grid = view.up('tab-bank-grid');
		grid.getSelectionModel().select(record);
		if(this.getBankmenu()){
            this.getBankmenu().showAt(e.getXY());
        }
	},
	
    onBankGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
        var me = this;
        e.stopEvent();
        console.log("Show Menu for Bank Add/Dealte");
        if(this.getBankmenu()){
            this.getBankmenu().showAt(e.getXY());
        }
    },

    onBankMenuClick: function(menu, item, e, eOpts) {
        console.log("Menu Item Clicked");
        if(item.text === "Add Bank"){
            console.log("Add Bank");
            this.showAddBank();
        }else if(item.text === "Remove Bank"){
            console.log("Remove Bank");
            this.removeBank();
        }
    },

    removeBank: function() {
        console.log("Remove Bank");
        var me = this;
		var tabbankpanel = me.getController('MainController').getMainviewport().down('tab-banks');
		var context = tabbankpanel.getActiveTab();		
        var bankgrid = context.down("tab-bank-grid");
        var row = bankgrid.getSelectionModel().getSelection()[0];
        console.log(row.get('idBank'));
        var idBank = row.get('idBank');

        Ext.Msg.show({
            title:'Delete Bank?',
            message: 'Are you sure you want to delete the Bank?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    bankgrid.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Bank/'+idBank+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            bankgrid.setLoading(false);
                            console.log("Removed Bank.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                var filterstarr = [];
                                me.loadAllBanks(filterstarr);
                            } else {
                                console.log("Delete Bank Failure.");
                                Ext.Msg.alert('Delete Bank failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            bankgrid.setLoading(false);
                            console.log("Delete Bank Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    showAddBank: function() {
        var me = this;
		var tabbankpanel = me.getController('MainController').getMainviewport().down('tab-banks');
		var context = tabbankpanel.getActiveTab();
        var eastregion = context.down('#bank-east-panel');
        eastregion.getLayout().setActiveItem(context.down('#bankdetailform'));
        context.down('#bankdetailform').setTitle("Add Bank");
        context.down('#bankdetailform').getForm().reset();
    },

    onSaveBankBtnClick: function(btn) {
        var me = this;
		var tabbankpanel = me.getController('MainController').getMainviewport().down('tab-banks');
		var context = tabbankpanel.getActiveTab();
        var eastregion = context.down('#bank-east-panel');
		
        var bankform =context.down('#bankdetailform');
        var idBank = bankform.getForm().findField('idBank').getValue();
        var data = {
            'fullName'         : bankform.getForm().findField('fullName').getValue(),
            'shortName'        : bankform.getForm().findField('shortName').getValue(),
            'meridianBank'     : bankform.getForm().findField('meridianBank').getValue(),
            'payoutNoticeDays' : bankform.getForm().findField('payoutNoticeDays').getValue(),
            'defaultExecution' : bankform.getForm().findField('defaultExecution').getValue(),
            'office'           : bankform.getForm().findField('office').getValue(),
            'address'          : bankform.getForm().findField('address').getValue(),
            'state'            : bankform.getForm().findField('state').getValue(),
            'city'             : bankform.getForm().findField('city').getValue(),
            'zip'              : bankform.getForm().findField('zip').getValue()
        };
        if(idBank===0 || idBank===null){
            console.log("Add New Bank");
            var methodname = "POST";
        }
        else{
            console.log("Save Bank");
            data['idBank'] = idBank;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";
        }

        bankform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Bank',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                bankform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201) {
                    var filterstarr = [];
                    me.loadAllBanks(filterstarr);
                } else {
                    console.log("Save Bank Failure.");
                    Ext.Msg.alert('Save Bank failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                bankform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Bank', obj.errorMessage);
            }
        });
    },

    loadSelBanks: function(filterstarr,context) {
        var me = this;
		var bcselbankgrid = context.down('bankchooserpanel').down('#bcselbankgridpanel');
        var bankselst = bcselbankgrid.getStore();

        var rowindex = 0;
        if (bcselbankgrid.getSelectionModel().hasSelection()) {
            var selrecord = bcselbankgrid.getSelectionModel().getSelection()[0];
            if(context.down('bankchooserpanel').config.savemode){
                if(context.down('bankchooserpanel').config.savemode === "new"){
                    var rowindex = 0;
                    context.down('bankchooserpanel').config.savemode = "edit";
                }
                else{
                    var rowindex = bankselst.indexOf(selrecord);
                }
            }
            else{
                var rowindex = bankselst.indexOf(selrecord);
            }
        }
        console.log("RowIndex"+rowindex);

        var banksselproxy = bankselst.getProxy();
        banksselproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        bankselst.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                console.log(success);
                if(success)
                {
                    var bankselcnt = bankselst.getCount();
					var showallfld = context.down('bankchooserpanel').down('checkboxfield[name="showall"]').getValue();
					if(showallfld){
						bankselst.clearFilter();
					}else{
						bankselst.filter('isSubmitted', true);
					}
                    var submissiontoolbar = bcselbankgrid.down('#submissiongridtoolbar');
                    if(bankselcnt > 0){
                       context.down('bankchooserpanel').down('#bankdocspanel').show();
                       bcselbankgrid.getSelectionModel().select(rowindex);
                       if(submissiontoolbar){
                           //submissiontoolbar.hide();
                       }
                    }
                    else{
                        if(submissiontoolbar){
                            submissiontoolbar.show();
                        }
						context.down('bankchooserpanel').down('#bankdocsgridpanel').getStore().removeAll();
						context.down('bankchooserpanel').down('#submissioncontactsgridpanel').getStore().removeAll();
                        //Ext.getStore('SubmissionContacts').removeAll();
                        context.down('bankchooserpanel').down('#bankdocspanel').hide();
                    }
                }
            }
        });
    }

});
