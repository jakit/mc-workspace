Ext.define('DM2.controller.DealDetailController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.DealUserTab',
			  'Ext.window.Toast',
			  'DM2.view.EditDealPropertyForm'],
    refs: {
        dealdetailsformpanel: {
            autoCreate: true,
            selector: 'dealdetailsformpanel',
            xtype: 'dealdetailsformpanel'
        },
        adddealcontactform: {
            autoCreate: true,
            selector: 'adddealcontactform',
            xtype: 'adddealcontactform'
        },
        adddealpropertyform: {
            autoCreate: true,
            selector: 'adddealpropertyform',
            xtype: 'adddealpropertyform'
        },
        // defaultworkareapanel: 'dealdetailsformpanel #defaultworkareapanel',
        defaultworkareapanel: 'defaultworkareapanel',
        dealdetailcontacttoolbar: 'dealdetailsformpanel #dealdetailcontacttoolbar',
        dealdetailcontactmenu: {
            autoCreate: true,
            selector: 'dealdetailcontactmenu',
            xtype: 'dealdetailcontactmenu'
        },
        changerolecontactform: {
            autoCreate: true,
            selector: 'changerolecontactform',
            xtype: 'changerolecontactform'
        },
        dealdetailpropertyfieldset: 'dealdetailpropertyfieldset',
        dealdetailpropertiesmenu: {
            autoCreate: true,
            selector: 'dealdetailpropertiesmenu',
            xtype: 'dealdetailpropertiesmenu'
        },
        dealdetailexistingloanfieldset: 'dealdetailexistingloanfieldset',
		dealdetailexistingloanmenu: {
            autoCreate: true,
            selector: 'dealdetailexistingloanmenu',
            xtype: 'dealdetailexistingloanmenu'
        },
        dealdetailnewloanfieldset: 'dealdetailnewloanfieldset',
		dealdetailnewloanmenu: {
            autoCreate: true,
            selector: 'dealdetailnewloanmenu',
            xtype: 'dealdetailnewloanmenu'
        },
        dealdetailmasterloanfieldset: 'dealdetailmasterloanfieldset',
        dealdetailcontactfieldset: 'dealdetailcontactfieldset',
        dealdetailoptionmenu: {
            autoCreate: true,
            selector: 'dealdetailoptionmenu',
            xtype: 'dealdetailoptionmenu'
        }
    },

    init: function(application) {
        this.control({
             'dealdetailsformpanel':{
                 close:this.dealDetailsFormClose
             },
             'dealdetailsformpanel #adddealcontactbtn':{
                 click: this.addDealToContactBtnClick
             },
			 'adddealcontactform':{
                close: this.addDealContactCloseBtnClick
             },
			 'adddealcontactform #adddealcontactgrid':{
                 itemclick: this.onDealAllContactsGridpanelItemClick
             },
             'adddealcontactform button[action="savedealcontactbtn"]':{
                 click: this.addContactToDealBtnClick
             },
			 'adddealcontactform button[action="createnewcontactbtn"]':{
                 click: this.createNewContactBtnClick
             },
            'dealdetailcontactfieldset grid':{
                rowcontextmenu: this.dealDetailContactGridRightClick//,
                // itemclick: this.loadContactDetailForm
            },
            'dealdetailcontactmenu menuitem':{
                click: this.dealDetailContactOptionItemClick
            },
            'changerolecontactform': {
                close: this.changeRoleContactCloseBtnClick
            },
            'changerolecontactform #savechangerolecontactbtn':{
                 click: this.changeRoleContactSaveBtnClick
            },
            'dealdetailsformpanel #dealdetailpropertygridpanel':{
                rowcontextmenu: this.dealDetailPropertiesGridRightClick,
                itemclick: this.loadPropertiesDetailForm
            },
            'dealdetailpropertiesmenu menuitem':{
                click: this.dealDetailPropertiesOptionItemClick
            },
			'dealdetailnewloanmenu menuitem':{
                click: this.dealDetailNewLoanOptionItemClick
            },
			'dealdetailexistingloanmenu menuitem':{
                click: this.dealDetailExistingLoanOptionItemClick
            },
            // CG
            // 'dealdetailpropertyfieldset #propertydetailclosebtn':{
                // click: this.dealDetailPropertiesCloseBtnClick
            // },
            'dealdetailpropertyfieldset #propertydetailclosebtn':{
                click: this.dealDetailPropertiesCloseBtnClick
            },
            'dealdetailsformpanel #adddealpropertybtn':{
                 click: this.addPropertyToDealBtnClick
             },
            'adddealpropertyform':{
                close: this.addDealPropertyCloseBtnClick,
				afterrender: 'loadAddDealPropertyDetails'
            },        
            'dealdetailsformpanel #filterpropertybtn':{
                 click: this.filterPropertyBtnClick
            },
            'adddealpropertyform #adddealpropertygrid':{
                itemclick: this.onDealAllPropertiesGridpanelItemClick,
				select: this.onDealAllPropertiesGridpanelItemSelect
            },
            'adddealpropertyform #savedealpropertybtn':{
                click: this.savePropertyToDealBtnClick
            },
			'adddealpropertyform button[action="createnewpropertybtn"]':{
                click: this.createNewPropertyBtnClick
            },
            'dealdetailresearchpanel #gmapbtn':{
                click:this.showGmapPanel
            },
            'dealdetailgmapcontainer':{
                afterrender: this.onDealDetailGmapAfterRender
            },
            'defaultworkareapanel combo[name="activity"]':{
                afterrender: this.afterActivityComboRender,
                change: this.onActivityChange
            },
            'adddealcontactform combo[name="contactType"]':{
                afterrender: this.afterContactTypeComboRender
            },
            'changerolecontactform combo[name="contactType"]':{
                afterrender: this.afterChangeContactTypeComboRender
            },
            'dealdetailsformpanel #dealdetailexistingloangridpanel':{
                itemclick: this.loadLoanDetailForm
            },
            'dealdetailsformpanel #loandetailclosebtn':{
                click: this.dealDetailLoansCloseBtnClick
            },
            // 'dealdetailnewloanfieldset #newloandetailclosebtn':{
                // click: this.dealDetailNewLoansCloseBtnClick
            // },
            // 'dealdetailcontactfieldset #contactdetaildealclosebtn':{
                // click: this.dealDetailContactCloseBtnClick
            // },
            'dealdetailpropertiesmenu #researchmenu':{
                click: this.researchMenuClick
            },
            "#activecheck": {
                beforecheckchange: 'onActiveDealUserChange'
            },
            'dealdetailpropertyfieldset #dealdetailpropertyform':{
                afterrender: this.onDealdetailpropertyformAfterRender
            },            
            // 'dealdetailsformpanel #dealdetailnewloangridpanel':{
                // itemclick: this.loadNewLoanDetailForm
            // },
            // 'dealdetailmasterloanfieldset #dealdetailmasterloangridpanel':{
                // itemclick: this.loadMasterLoanDetailForm
            // },
            // 'dealdetailmasterloanfieldset #masterloandetailclosebtn':{
                // click: this.dealDetailMasterLoansCloseBtnClick
            // },
            // 'dealdetailsformpanel #defaultworkareapanel':{
            'defaultworkareapanel':{
				activate:this.defaultWorkPanelActivate
            },            
            '#dealdetailuppersection':{
                afterrender: {
                    fn: function(panel) {
                        panel.getEl().on("contextmenu",this.showContextMenu, this, {args:[panel]});
						panel.getEl().on("longpress",this.showContextMenu, this, {args:[panel]});
						this.showHideDealDetailUpperSectionFields(panel);
                    }
                }
            },
            'dealdetailpropertyfieldset': {
                afterrender: this.afterDealDetailPropertyFieldsetRender
            },
            'dealdetailcontactfieldset': {
                afterrender: {
                    fn: function(panel) {
                    }
                }
            },       
            'dealdetailexistingloanfieldset': {
                afterrender: {
                    fn: function(panel) {
                        //Ext.Function.bind(this.loadExistingLoanDetails, this, [panel])();
                    }
                }
            },            
            'dealdetailnewloanfieldset': {
                afterrender: {
                    fn: function(panel) {
                        //Ext.Function.bind(this.loadNewLoanDetails, this, [panel])();
                    }
                }
            },    
            'dealdetailmasterloanfieldset': {
                afterrender: {
                    fn: function(panel) {
                        //Ext.Function.bind(this.loadMasterLoanDetails, this, [panel])();
                    }
                }
            },    
            'dealdetailoptionmenu':{
                click:'onDealDetailOptionMenuClick'
            },
            'dealdetailsformpanel tabpanel[action="dealdetailtabpanel"]':{
                afterrender: this.dealDetailTabPanelAfterRender,
                beforetabchange: this.dealDetailTabChange
            },            
			'dealdetailsformpanel splitbutton[action="dealdetaildocstab"]':{
				click: this.dealDetailDocsTabClick
			},
			'dealdetailsformpanel #dealdocstabmenu':{
				click: this.dealDocsTabMenuClick
			},
			'dealdetailsformpanel #dealdetailshistorytab':{
				click: this.dealDetailsHistoryTabClick
			},
			'#dealdetailshistorytab menu':{
				click: this.dealDetailsHistoryTabMenuClick
			},
			'viewmasterdataform':{
                close: this.viewMasterDataFormCloseBtnClick
            },
			'listacrisdocuments':{
                close: this.listACRISDocumentsCloseBtnClick
            },
			'listacrisdocuments acrispropertydocsgrid':{
                select: this.acrisPropertyDocsGridItemSelect
            },
			'listacrisdocuments acriscontactsgrid':{
                select: this.acrisContactsGridItemSelect
            },
			'dealdetailsformpanel #dealdetailusertab':{
				click: this.dealDetailUsersTabClick
			},
			'#dealdetailusertab menu':{
				click: this.dealDetailUsersTabMenuClick
			},
			'editdealpropertyform':{
                close: this.editDealPropertyFormCloseBtnClick
            },
			'editdealpropertyform button[action="savedealpropertybtn"]':{
                click: this.saveDealPropertyBtnClick
            },
			'#dealdetailuppersection #csrlabel':{
                afterrender: {
                    fn: function(cmp) {
                        cmp.getEl().on("click",this.csrLabelClicked, this, {args:[cmp]});
                    }
                }
            }
        });
    },
	
	csrLabelClicked: function(cmp,e){
		var me = this;
		console.log("CSR Label Clicked.");
		console.log(e);
		console.log(cmp);
		var context = me.getController('DealDetailController').getTabContext(cmp);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		context.down('tabpanel[region="south"]').setActiveTab(detailpanel.down('dealdetailusergridpanel'));
	},

	bindActivityDetails: function(tabItem){
		console.log("Bind Activity Details");
		var me = this;
		var panel = tabItem.down('defaultworkareapanel');		
		var combo = panel.down('combobox[name="activity"]');
        var store = combo.getStore();
		
		var activities_st = Ext.getStore('Activities');
		if(activities_st.isLoaded()){
			store.loadData(activities_st.data.items);
			me.filterActivity(store,panel);
			me.bindActivityHistory(tabItem);
			combo.reset();
		}		
	},
	
	filterActivity: function(store,panel){
		var me = this,track,context;
		if(panel.up('tab-deal-detail') || panel.up('tab-deal-layout')){
			track = "D";
			store.filter("track",track);
		} else if(panel.up('tab-sales-detail') || panel.up('tab-sales-layout')){
			track = "S";
			store.filter("track",track);
			if(panel.up('tab-sales-detail')){
				context = panel.up('tab-sales-detail');
			} else if(panel.up('tab-sales-layout')){
				context = panel.up('tab-sales-layout');
			}
			var rec = context.record;
			var salesType = rec.get('saleType');			
			store.filter("subTrack",salesType);
		}		
	},
	
	bindActivityHistory: function(tabItem){
		var me = this;
		//var obj = Ext.decode(tabItem.dealDetailStore.data.items[0].data.permission);
		//console.log(obj);
		var permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
		console.log("Permission : "+permission);
		if(parseInt(permission)===0){
			tabItem.down('defaultworkareapanel').disable();
		}else{
			tabItem.down('defaultworkareapanel').enable();
		}
		var panel = tabItem.down('defaultworkareapanel');
		var combo = panel.down('combobox[name="activity"]');
        var store = combo.getStore();
		
		//combo.config.firstchange = true;
		//combo.select(store.getAt(0));
		
		panel.down('menu').removeAll();
		
		var acthisst = panel.activityHistoryStore;
		acthisst.loadData(tabItem.dealDetailStore.data.items[0].data.ActivityHistory);
		me.afterActivityHistoryDataLoad(panel);
	},
	
	getTabContext: function(cmp){
		var me = this,context;
		if(cmp.up('tab-deal-detail')){
			context = cmp.up('tab-deal-detail');
		} else if(cmp.up('tab-sales-detail')){
			context = cmp.up('tab-sales-detail');
		} else if(cmp.up('tab-property-layout')){
			context = cmp.up('tab-property-layout');
		} else if(cmp.up('tab-contact-layout')){
			context = cmp.up('tab-contact-layout');
		} else if(cmp.up('tab-deal-layout')){
			context = cmp.up('tab-deal-layout');
		} else if(cmp.up('tab-sales-layout')){
			context = cmp.up('tab-sales-layout');
		}
		return context;
	},
	
	getDealSaleDetailPanel: function(context){
		var me = this,dealdetailpanel;
		if(context.xtype == "tab-deal-detail"){
			dealdetailpanel = context.down('dealdetailsformpanel');
		} else if(context.xtype == "tab-sales-detail"){
			dealdetailpanel = context.down('salesdealdetailsformpanel');
		}
		return dealdetailpanel;
	},
	
	afterActivityHistoryDataLoad: function(panel){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var record = context.record;
		console.log("St"+record.get('status'));
        var dealid = record.data.idDeal;    
		
		var combo = panel.down('combobox[name="activity"]');
        var store = combo.getStore();
		
		var acthisst = panel.activityHistoryStore;
		
		var count = store.getCount();
		var substatusflag;
		if(count > 0){
			store.sort('idActivity', 'ASC');
			for(var j=0; j<count; j++){
				var item = store.getAt(j); //[j];
				if(item.get('status')===record.get('status')){
					if(item.get('subTrack')!=null){
						if(item.get('subTrack')===record.get('saleType')){
							substatusflag = true;
						} else {
							substatusflag = false;
						}
					} else {
						substatusflag = true;
					}
					if(substatusflag){
						console.log("Icon"+item.get('iconname'));
						var iconCls = '';
						iconCls = item.get('iconname');
	
						///Check if activity is in history
						var isActInHistory = false;
						var menuitemcls = 'activityleft';
						
						for(var m=0;m<acthisst.getCount();m++){
							var acthisitem = acthisst.getAt(m);
							if(item.get('idActivity')===acthisitem.get('idActivity')){
								isActInHistory = true;
								menuitemcls = 'activitydone';
								break;
							}
						}
	
						// CG - this can be a problem.. 
						var checkmenuitem = Ext.create('Ext.menu.CheckItem', {
							activityid : item.get('idActivity'),
							h_fname    : item.get('handler_fname'),
							iconCls   : iconCls,
							cls       : menuitemcls,
							iconAlign : 'right',
							tooltip : item.get('description'),
							text    : Ext.util.Format.ellipsis(item.get('description'), 50),
							textAlign:'left',
							checked : isActInHistory,
							checkChangeDisabled : true
						});
						panel.down('menu').add(checkmenuitem);
					}
				}
			}
			store.sort('description', 'ASC');
			combo.config.firstchange = true;
			combo.select(store.getAt(0));
		}
	},
	
    loadActivityDetails: function(panel) {
        console.log("Load Activity");
        var me = this;
        var combo = panel.down('combobox[name="activity"]');
        var store = combo.getStore();
		
		var activities_st = Ext.getStore('Activities');
		if(activities_st.isLoaded()){
			store.loadData(activities_st.data.items);
			me.filterActivity(store,panel);
			me.loadActivityHistory(panel);
			combo.reset();
		}
    },
	
	loadActivityHistory: function(panel){		
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
        var record = context.record;
		console.log("St"+record.get('status'));
        var dealid = record.data.idDeal;    
		
		var combo = panel.down('combobox[name="activity"]');
        var store = combo.getStore();            
		
		var acthisst = panel.activityHistoryStore;
		var acthisproxy = acthisst.getProxy();
		acthisproxy.setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
		});
		
		//combo.config.firstchange = true;
		//combo.select(store.getAt(0));
		
		panel.down('menu').removeAll();
		//console.log("All Menus removed");
		////
		panel.setLoading(true);
		panel.activityHistoryStore.load({
			params:{
				filter :"idDeal = '"+dealid+"'"
			},
			scope: {
				me: me,
				context: context,
				record: record,
				dealid: dealid,
				combo: combo,
				store: store,
				panel: panel
			},
			callback: function(acthisrecords, acthisoperation, acthissuccess){

				var me = this.me;
				var context = this.context;
				var record = this.record;
				var dealid = this.dealid;
				var combo = this.combo;
				var store = this.store;
				var panel = this.panel;
				var acthisst = panel.activityHistoryStore;
				panel.setLoading(false);
				me.afterActivityHistoryDataLoad(panel);
			}
		});
		////
	
	},

    dealDetailsFormClose: function(panel, eOpts) {
        //this.getDealdetailsformpanel().hide();
        this.getController('DealController').getDealmenupanel().show();
    },

    onDealAllContactsGridpanelItemClick: function(grid, record, item, index, e, eOpts) {
        var me = this;
		var adddealcontactform = grid.up('#adddealcontactform');
        adddealcontactform.getForm().loadRecord(record);
    },

    addContactToDealBtnClick: function(btn) {
        var me = this,contactid,contactTypeval;
        console.log("Add Contact To Deal");		
		
		var context = me.getController('DealDetailController').getTabContext(btn);
		var adddealcontactform = btn.up('adddealcontactform');

		if(adddealcontactform.config.backview  && (adddealcontactform.config.backview == "underwritingactivity" || adddealcontactform.config.backview == "loanmanager" || adddealcontactform.config.backview == "quotedbyloanmanager" || adddealcontactform.config.backview == "letterAddrloanmanager" || adddealcontactform.config.backview == "loaneditor" || adddealcontactform.config.backview == "quotedbyloaneditor" || adddealcontactform.config.backview == "letterAddrloaneditor" || adddealcontactform.config.backview == "salebid")){
			me.getController('UnderwritingController').saveContactToExecBtnClick(btn);
		} else if(adddealcontactform.config.backview  && adddealcontactform.config.backview == "submcnts"){
			me.getController('DealActivityController').onSaveContactToSubmissionBtnClick(btn);
		} else if(adddealcontactform.config.backview  && adddealcontactform.config.backview == "createnewdeal"){
			me.getController('CreateNewDealController').addNewContactToDealClick(adddealcontactform);
		} else if(adddealcontactform.config.backview  && adddealcontactform.config.backview == "createnewsalesdealfrompropertypanel"){
			me.getController('CreateNewDealFromPropertyController').addNewContactToDealClick(adddealcontactform);
		} else if(adddealcontactform.config.backview  && adddealcontactform.config.backview == "createnewdealfrompropertypanel"){
			me.getController('CreateNewDealFromPropertyController').addNewContactToDealClick(adddealcontactform);
		} else if(adddealcontactform.config.backview  && adddealcontactform.config.backview == "addsalebid"){
			me.getController('SalesDealDetailController').selectContactForBid(adddealcontactform);
		} else if(context.xtype=="tab-deal-detail" || context.xtype=="tab-sales-detail" || context.xtype=="tab-sales-layout" || context.xtype=="tab-deal-layout"){
			var record = context.record;
			var dealid = record.data.idDeal;
					
			var allcontactgrid = adddealcontactform.down('#adddealcontactgrid');
			
			if (allcontactgrid.getSelectionModel().hasSelection()) {
				var row = allcontactgrid.getSelectionModel().getSelection()[0];
				console.log(row.get('contactid'));
				contactid = row.get('contactid');
				contactTypeval = adddealcontactform.getForm().findField('contactType').getValue();
			} else {
				Ext.Msg.alert('Failed', "Please select contact from Allcontactsgrid.");
				return false;
			}
			me.getController('DealDetailController').saveContactToDeal(contactid,adddealcontactform,context);
		} else if(context.xtype=="tab-property-layout"){
			me.getController('PropertyController').addContactToPropertyBtnClick(btn);
		}
    },
	
	saveContactToDeal: function(contactid,adddealcontactform,context){
		var me = this,isPrimary = false;
		var record = context.record;
		var dealid = record.data.idDeal;
		var contactTypeval = adddealcontactform.getForm().findField('contactType').getValue();
		if(contactTypeval=="MAIN CONTACT"){
			isPrimary = true;
		}
		var data = [{
			'idDeal': dealid,
			'idContact' : contactid,
			'contactType' : contactTypeval
		}];
		me.saveDealXContact(data,adddealcontactform,context);
	},
	
	saveDealXContact: function(data,form,context){
		var me = this;
		console.log("Save Deal X Contact");
		form.setLoading(true);
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'mssql:DealxCont',
			params: Ext.util.JSON.encode(data),
			scope:this,
			success: function(response, opts) {
				form.setLoading(false);
				console.log("Add contact to deal is submitted.");
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 201) {
					var panel;
					if(form.config.backview==="dealcontactgrid"){
						panel = context.down('dealcontactsgridpanel');
					} else if(form.config.originbackview==="dealcontactgrid"){
						panel = context.down('dealcontactsgridpanel');
					} else {
						var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
						panel = detailpanel.down('dealdetailcontactfieldset');
						panel.switchPanel(0);
					}
					//me.getController('ContactController').loadDealContactDetails(dealdetailcontactfieldset);
					me.loadDealContactDetails(panel);
					if(form.xtype=="adddealcontactform"){
						me.addDealContactCloseBtnClick(form);
					}					
				} else {
					console.log("Add Contact to Deal Failure.");
					Ext.Msg.alert('Add Contact Failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				form.setLoading(false);
				console.log("Add Contact to Deal Failure.");
				var obj = Ext.decode(response.responseText);
				var errMsg = obj.errorMessage;
				var errText = "";
				var n = errMsg.indexOf("MAIN CONTACT Already exist");
				if(n==-1){
					var g = errMsg.indexOf("Violation of UNIQUE KEY constraint");
					if(g==-1){
						errText = obj.errorMessage;
						//Ext.Msg.alert('Add Contact Failed', obj.errorMessage);
					} else {
						errText = "Contact already exist.Please try to add another contact.";
						//Ext.Msg.alert('Add Contact Failed', "Contact already exist.Please try to add another contact.");
					}
				} else {
					errText = "MAIN CONTACT already exist.";
					//Ext.Msg.alert('Add Contact Failed', "MAIN CONTACT Already exist.");
				}
				var title = "Add Contact Failed";
				me.showToast(errText,title,'error',null,'tr',true);
			}
		});
	},
	
	showToastWin: function(s, title,iconvalue,anchorCmp,alignPos,autoCloseVal,closeDelay) {
		//var icon = Ext.MessageBox[iconvalue.toUpperCase()];
		var fontcolortext = "red";
		var closeDelay = closeDelay;
		if(iconvalue=="error"){
			fontcolortext = "red";
		} else if(iconvalue=="success"){
			fontcolortext = "green";
		}
        var toast = Ext.toast({
						html: '<span style="color:'+fontcolortext+';font: bold 13px/15px helvetica, arial, verdana, sans-serif;">'+s+'</span>',
						title: title,
						autoClose: autoCloseVal,
						autoCloseDelay: closeDelay,
						anchor: anchorCmp,
						//closeOnMouseOut: true,
						//icon: icon,
						//iconCls : 'x-message-box-warning',
						//closable: false,
						align: alignPos,
						slideInDuration: 400,
						minWidth: 400
					});
		return toast;
    },
	
	showToast: function(s, title,iconvalue,anchorCmp,alignPos,autoCloseVal) {
		//var icon = Ext.MessageBox[iconvalue.toUpperCase()];
		var fontcolortext = "red";
		var closeDelay = 30000;
		if(iconvalue=="error"){
			fontcolortext = "red";
			closeDelay = 30000;
		} else if(iconvalue=="success"){
			fontcolortext = "green";
			closeDelay = 10000;
		} else if(iconvalue=="default"){
			fontcolortext = "red";
			closeDelay = 10000;
		}
        Ext.toast({
            html: '<span style="color:'+fontcolortext+';font: bold 13px/15px helvetica, arial, verdana, sans-serif;">'+s+'</span>',
			title: title,
			autoClose: autoCloseVal,
			autoCloseDelay: closeDelay,
			anchor: anchorCmp,
			//closeOnMouseOut: true,
			//icon: icon,
			//iconCls : 'x-message-box-warning',
            //closable: false,
            align: alignPos,
            slideInDuration: 400,
            minWidth: 400
        });
    },

    addDealToContactBtnClick: function(btn) {
        var eastregion = this.getDealdetailsformpanel().down('[region=east]');
        eastregion.getLayout().setActiveItem(this.getAdddealcontactform());
        this.getAdddealcontactform().getForm().reset();  //Reset The Form
        var ctypesst = Ext.getStore('ContactTypes');
        if(ctypesst.isLoaded()){
            this.getAdddealcontactform().down('combo[name="contactType"]').select(ctypesst.getAt(0));
        }
        var filterstarr = [];
        this.loadContactAllSelBuff(filterstarr);
    },

    addDealContactCloseBtnClick: function(panel) {
		var me = this;
		var backview = panel.config.backview;
		if(backview == "underwritingactivity"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			activeItem = detailpanel.down('underwriting');
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(activeItem);
		} else if(backview == "loanmanager" || backview == "quotedbyloanmanager" || backview == "letterAddrloanmanager"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			activeItem = detailpanel.down('dealdetailquotepanel');
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(activeItem);
		} else if(backview == "loaneditor" || backview == "quotedbyloaneditor" || backview == "letterAddrloaneditor"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			activeItem = detailpanel.down('quotedetailspanel');
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(activeItem);
		} else if(backview == "salebid"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			activeItem = detailpanel.down('addsaledealbid');
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(activeItem);
		} else if(backview==="propertycontacts"){
			me.getController('PropertyController').closeAddPropertyContactView(panel);
		} else if(backview =="createnewdeal"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			createnewdealpanel = context.down('createnewdealpanel');
			eastregion.getLayout().setActiveItem(createnewdealpanel);
		} else if(backview =="createnewdealfrompropertypanel"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var eastregion = context.down('[region=east]');
			createnewdealpanel = context.down('createnewdealfrompropertypanel');
			eastregion.getLayout().setActiveItem(createnewdealpanel);
		} else if(backview =="createnewsalesdealfrompropertypanel"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var eastregion = context.down('[region=east]');
			createnewdealpanel = context.down('createnewsalesdealfrompropertypanel');
			eastregion.getLayout().setActiveItem(createnewdealpanel);
		} else if(backview =="addsalebid"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var eastregion = context.down('[region=east]');
			addsaledealbid = context.down('addsaledealbid');
			eastregion.getLayout().setActiveItem(addsaledealbid);
		} else if(backview =="submcnts"){
			me.getController('DealActivityController').closeAddSubmissionContactView(panel);
			/*var context = me.getController('DealDetailController').getTabContext(panel);
			var eastregion = context.down('[region=east]');
			bankchooserpanel = context.down('bankchooserpanel');
			eastregion.getLayout().setActiveItem(bankchooserpanel);*/
		}  else if(backview =="dealcontactgrid"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
				if(context.xtype == "tab-deal-layout"){
					eastregion = context.down('#deal-east-panel');
				} else if(context.xtype == "tab-sales-layout"){
					eastregion = context.down('#sales-east-panel');
				}				
				eastregion.removeAll();
				//eastregion.setCollapsed(false);
				eastregion.setVisible(false);
			}
		} else {
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
    },

    addPropertyToDealBtnClick: function(btn) {
		var me = this,adddealpropertyform;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('adddealpropertyform')){
			console.log("Use Old Panel");
			adddealpropertyform = detailpanel.down('adddealpropertyform');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	adddealpropertyform = Ext.create('DM2.view.AddDealPropertyForm');
		}
        eastregion.getLayout().setActiveItem(adddealpropertyform);
		adddealpropertyform.setTitle("Add Property");
		adddealpropertyform.config.viewMode = "addproperty";
		
		if(adddealpropertyform.up('tab-sales-detail')){
			adddealpropertyform.getForm().findField('mortgageType').hide();
		} else {
			adddealpropertyform.getForm().findField('mortgageType').show();
		}
    },
	
	loadAddDealPropertyDetails: function(panel){
		var me = this;
		console.log("After Add Deal Property");
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();		
		//var context = panel.up('tab-deal-detail');
		
		var adddealpropertyform = panel;
		adddealpropertyform.getForm().reset();  //Reset The Form
		
        var filterstarr = [];
        me.loadAllPropertiesStore(filterstarr,context);
		
		//Loan Mortgage Type
        var mtgtypeSt = adddealpropertyform.getForm().findField('mortgageType').getStore();
		
		var  selectionList_st = Ext.getStore('SelectionList');		
		if(selectionList_st.isLoaded()){
			mtgtypeSt.loadData(selectionList_st.data.items);
			mtgtypeSt.filter("selectionType","MT");
			mtgtypeSt.each( function(record){
				if(record.get('isDefault')){
					adddealpropertyform.getForm().findField('mortgageType').setRawValue(record.get('selectionDesc'));
				}
			});
		}
	},
	
    addDealPropertyCloseBtnClick: function(panel) {		
		var me = this;
		//var adddealpropertyform = btn.up('adddealpropertyform');
		if(panel.config.backview =="createnewdealfromproperty" || panel.config.backview == "createnewdealfrompropertypanel"){
			var tabpropertypanel = panel.up('tab-properties');
			var context = tabpropertypanel.getActiveTab();
			if(context.xtype == "tab-property-layout"){
				var eastregion = context.down('#property-east-panel');
				createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
				eastregion.getLayout().setActiveItem(createnewdealfrompropertypanel);
			}
		}  else if(panel.config.backview =="createnewsalesdealfrompropertypanel"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var eastregion = context.down('[region=east]');
			createnewsalesdealfrompropertypanel = context.down('createnewsalesdealfrompropertypanel');
			eastregion.getLayout().setActiveItem(createnewsalesdealfrompropertypanel);
		} else if(panel.config.backview =="createnewdeal"){
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			createnewdealpanel = context.down('createnewdealpanel');
			eastregion.getLayout().setActiveItem(createnewdealpanel);
		} else {
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
    },
	
	bindDealPrimaryDetails: function(tabItem){
		console.log("Bind Deal Primary Details");
		var me = this;
		var panel = tabItem.down('dealdetailuppersection');
		var dealprmdtst = panel.dealPrimaryDetailStore;
		dealprmdtst.loadData(tabItem.dealDetailStore.data.items[0].data.DealDetails);
		var temprec =  dealprmdtst.getAt(0);
		temprec.set('scheduledCloseDate',tabItem.dealDetailStore.data.items[0].data.scheduledCloseDate);
		temprec.set('closeDate',tabItem.dealDetailStore.data.items[0].data.closeDate);
		temprec.set('billDate',tabItem.dealDetailStore.data.items[0].data.billDate);
		temprec.set('endDate',tabItem.dealDetailStore.data.items[0].data.endDate);
		if(tabItem.dealDetailStore.data.items[0].data.magicDealID){
			panel.config.magicDealID = tabItem.dealDetailStore.data.items[0].data.magicDealID;
		}
		me.afterDealPrimaryDataLoad(panel);		
	},
	afterDealPrimaryDataLoad: function(panel){
		var dealprmdtst = panel.dealPrimaryDetailStore;
		var count = dealprmdtst.getCount();
		if(count > 0) {
			var dealprmdtrec = dealprmdtst.getAt(0);
			var dealdetailform = panel.getForm();
			panel.config.dealdetailrec = dealprmdtrec;
			
			dealdetailform.loadRecord(dealprmdtrec);
			dealdetailform.findField('magicDealID').setValue(panel.config.magicDealID);
			if(dealprmdtrec.get('statusDate')!=null){
				//dealdetailform.findField('statusDate').setValue(dealprmdtrec.get('statusDate'));
				//dealdetailform.findField('statusDate').setValue(new Date(dealprmdtrec.get('statusDate')));
			}
		}
	},
	
    loadDealDetails: function(panel) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
        var record = context.record;
        var dealid = record.data.idDeal;
		
		var dealprmdtst = panel.dealPrimaryDetailStore;
		dealprmdtst.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		dealprmdtst.load({
			scope: this,
			params: {
				'filter' : 'dealid = '+dealid
			},
			callback: function(records, operation, success) {				
				me.afterDealPrimaryDataLoad(panel);
			}
		});
    },

    dealDetailContactGridRightClick: function(view, record, tr, rowIndex, e, eOpts) {
		var me = this;
        e.stopEvent();
        console.log("On Right Click of Contact Load Phone/email/fax Records.");		
		var listtodetail = view.ownerCt.up('cmp-container-listtodetail');
		listtodetail.setData(record);
						
        var idContact = record.get('contactid');
		var dealdetailcntmenu = view.up('dealdetailcontactfieldset').contextMenu;
		me.getController('ContactController').buildContactSubMenu(idContact,dealdetailcntmenu);
        //me.getController('ContactController').loadPhoneRecords(idContact);
		//me.getController('ContactController').loadEmailRecords(idContact);
    },

    dealDetailContactOptionItemClick: function(item, e, eOpts) {
     //   debugger;
        console.log("Deal Detail Contact Menu Item Clicked.");
		var me = this;
        if(item.text==="Add Contact"){
            console.log("Contact Add Menu Item Clicked");
			me.showAddDealContactForm("dealdetailcontacts","");
        }else if(item.text==="Remove Contact"){
            console.log("Remove Contact Menu Item Clicked");
            this.removeDealContact();
        }else if(item.text==="Change Role"){
            console.log("Change Role Menu Item Clicked");
            this.openChangeRole();
        }else if(item.text==="Edit Contact"){
            console.log("Edit Contact Menu Item Clicked");
            this.editDealContact();
        }
    },

	showAddDealContactForm: function(backview,backfrm) {
		var me = this,adddealcontactform;		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('adddealcontactform')){
			adddealcontactform = detailpanel.down('adddealcontactform');
		} else {
			//create new Activity Detail Panel
        	adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
		}
        eastregion.getLayout().setActiveItem(adddealcontactform);
		
		adddealcontactform.getForm().reset();
		var filterstarr = [];
		me.loadContactAllSelBuff(filterstarr,context);
		adddealcontactform.config.backview = backview;
		adddealcontactform.config.backfrm = backfrm;
    },

    removeDealContact: function() {
        var me = this;
        // CG
        console.log("Remove Contact from the Deal");
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var dealContact = detailpanel.down('dealdetailcontactfieldset');
        var contextMenu = dealContact.contextMenu;
        var row = contextMenu.getRec();
        
        // when the context menu is invoked from a form, there won't be any data set to the context menu
        if(!row) {
            row = dealContact.getData();
        } else {
            dealContact.setData(row);
        }
        
        var dealid = row.get('dealid');
        var contactid = row.get('contactid');
        var idDealxCont = row.get('idDealxCont');
        contextMenu.clearRec();         
        
        var data = {
            'checksum': "override"
        };
        
        Ext.Msg.show({
            title:'Delete Contact?',
            message: 'Are you sure you want to delete the contact from deal?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:DealxCont/'+idDealxCont+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            console.log("Add contact to deal is submitted.");
                            var obj = Ext.decode(response.responseText);
                            dealContact.switchPanel(0);
                            if(obj.statusCode == 200) {
								var dealdetailcontactfieldset = detailpanel.down('dealdetailcontactfieldset');
								me.loadDealContactDetails(dealdetailcontactfieldset);                                
                            } else {
                                console.log("Delete Contact Failure.");
                                Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Delete Contact Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    openChangeRole: function() {
		var me = this,changerolecontactform;		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
		
        var contact = detailpanel.down('dealdetailcontactfieldset');
        var rec = contact.getData();
        
        //if(!rec) {
            rec = contact.down('grid').getSelectionModel().getSelection()[0];
        //}
        
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('changerolecontactform')){
			console.log("Use Old Panel");
			changerolecontactform = detailpanel.down('changerolecontactform');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	changerolecontactform = Ext.create('DM2.view.ChangeRoleContactForm');
		}
        eastregion.getLayout().setActiveItem(changerolecontactform);
		
        //this.getAdddealcontactform().getForm().reset();  //Reset The Form
		changerolecontactform.getForm().reset();  //Reset The Form
        //var ctypesst = Ext.getStore('ContactTypes');
        var cnttype = rec.get('contactType');
        var cntname = rec.get('fullName');
        changerolecontactform.down('combo[name="contactType"]').select(cnttype);
        changerolecontactform.down('textfield[name="fullName"]').setValue(cntname);
/*
        if(ctypesst.isLoaded()){
            
//            var dealcontactgrid = this.getDealdetailsformpanel().down('#dealdetailcontactgrid');
            if (rec) {
                // var row = dealcontactgrid.getSelectionModel().getSelection()[0];
                var cnttype = rec.get('contactType');
                var cntname = rec.get('fullName');
            }
            else{
                if(dealcontactgrid.getStore().getCount()===1){
                    var tmprec = dealcontactgrid.getStore().getAt(0);
                    var cnttype = tmprec.get('contactType');
                    var cntname = tmprec.get('fullName');
                }
            }

            this.getChangerolecontactform().down('combo[name="contactType"]').select(cnttype);
            this.getChangerolecontactform().down('textfield[name="fullName"]').setValue(cntname);
        }*/

    },

    changeRoleContactCloseBtnClick: function(panel) {
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    loadAllContactsStore: function(filterstarr) {
        var dealhascontactsproxy = Ext.getStore('Dealhascontacts').getProxy();
        dealhascontactsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('Dealhascontacts').load({
            params:{
                filter :filterstarr
            }
        });
    },

    loadDealContactDetails: function(panel) {
        var me = this,grid;
		var context = me.getController('DealDetailController').getTabContext(panel);
        var record = context.record;
		var dealid = record.data.idDeal;
		if(panel.xtype=="dealcontactsgridpanel"){
			grid = panel;
		} else {
        	grid = panel.down('grid');
		}
        var store = grid.getStore();
        
        var dealdetailcontactsproxy = store.getProxy();
        
        dealdetailcontactsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: ('Espresso ' + me.getController('MainController').getMainviewport().config.apikey + ':1')            
        });
        store.load({
            params:{
                filter :'dealid = '+dealid
            },
            scope: {
                me: me,
                context: context,
                record: record,
                dealid: dealid,
                grid: grid,
                store: store,
                panel: panel 
            },
            callback: function(records, operation, success){
                
                var me = this.me;
                var panel = this.panel;
				if(panel.xtype=="dealcontactsgridpanel"){
					me.getController('ContactController').afterContactDataLoad(panel);
				} else {
					me.afterContactDataLoad(panel);
				}                
				context.dealContactData = store.data.items;
            }
        });
    },
	bindContactDetails: function(tabItem){
		console.log("Bind Contact Details");
		var me = this;
		var panel = tabItem.down('dealdetailcontactfieldset');
		var dealcntst = panel.down('grid').getStore();
		dealcntst.loadData(tabItem.dealDetailStore.data.items[0].data.ContactPerDeal);
		me.afterContactDataLoad(panel);		
	},
	afterContactDataLoad: function(panel){
		var dealcntst = panel.down('grid').getStore();
		var count = dealcntst.getCount();	    
		var singleDetail = panel.detailOnSingleRecord; 
		
		// works like other quadrants
		// only show form detail when these 2 are true, else the grid is already displayed
		if(count === 1 && singleDetail == true) {
			panel.setData(dealcntst.getAt(0));
			panel.loadForm(panel);
			panel.down('grid').getHeaderContainer().show();
		} else if(count === 0){
			panel.down('grid').getHeaderContainer().hide();	
		} else {
			panel.down('grid').getHeaderContainer().show();	
		}
	},
	
	disableAddEditMenu: function(panel){
		var me = this;
		console.log("Disable Add Edit Menu if no entries exist.");
		var menuitems = panel.contextMenu.items.items;
		//console.log(menuitems);
		var st = panel.down('grid').getStore();
		if(st.getCount() == 0){
			for(var i=0;i<menuitems.length;i++){
				if(!menuitems[i].isDisabled()){
					if(menuitems[i].menuType == "add"){
						menuitems[i].enable();
					} else if(menuitems[i].menuType == "edit"){
						menuitems[i].disable();
					}
				}
			}
		} else {
			for(var i=0;i<menuitems.length;i++){
				if(!menuitems[i].isDisabled()){
					menuitems[i].enable();
				}
			}
		}
	},

    loadPropertyDetails: function(panel) {
		
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
        var record = context.record;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
        var grid = panel.down('grid');
        
        var dealprptyst = grid.getStore();
        var rowindex = 0;

        var propertiesproxy = dealprptyst.getProxy();
        
        propertiesproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealprptyst.load({
            params:{
                filter :'dealid = '+dealid
            },
            scope: {
                me: me,
                panel: panel,
                dealprptyst: dealprptyst
            },
            callback: function(records, operation, success){
                var me = this.me;
                var panel = this.panel;
                var dealprptyst = this.dealprptyst;
				me.afterPropertyDataLoad(panel);
				context.dealPropertyData = dealprptyst.data.items;
            }
        });
    },
	bindPropertyDetails: function(tabItem){
		console.log("Bind Property Details");
		var me = this;
		var panel = tabItem.down('dealdetailpropertyfieldset');
		var dealprptyst = panel.down('grid').getStore();
		dealprptyst.loadData(tabItem.dealDetailStore.data.items[0].data.PropertiesPerDeal);
		me.afterPropertyDataLoad(panel);		
	},
	afterPropertyDataLoad: function(panel){
		var dealprptyst = panel.down('grid').getStore();
		var count = dealprptyst.getCount();	    
		var singleDetail = panel.detailOnSingleRecord; 
		
		// works like other quadrants
		// only show form detail when these 2 are true, else the grid is already displayed
		if(count === 1 && singleDetail == true) {
			panel.setData(dealprptyst.getAt(0));
			panel.loadForm(panel);
			panel.down('grid').getHeaderContainer().show();
		} else if(count === 0){
			panel.down('grid').getHeaderContainer().hide();
		} else {
			panel.down('grid').getHeaderContainer().show();
		}
	},
    // ORIGINAL
    // loadPropertyDetails: function(dealid) {
        // var me = this;
//         
        // var dealprptyst = Ext.getStore('Dealproperty');
// 
        // var loansizegrid = me.getController('DealActivityController').getLoansizingpanel().down('#loanpropertygridpanel');
        // if (loansizegrid.getSelectionModel().hasSelection()) {
            // var selrecord = loansizegrid.getSelectionModel().getSelection()[0];
            // var rowindex = dealprptyst.indexOf(selrecord);
        // }
        // else{
            // var rowindex = 0;
        // }
// 
        // var propertiesproxy = dealprptyst.getProxy();
        // propertiesproxy.setHeaders({
            // Accept: 'application/json',
            // Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        // });
        // dealprptyst.load({
            // params:{
                // filter :'dealid = '+dealid
            // },
            // callback: function(records, operation, success){
                // var count = dealprptyst.getCount();
                // var panel = me.getDealdetailpropertyfieldset();
                // var singleDetail = panel.detailOnSingleRecord; 
//                 
                // // works like other quadrants
                // // only show form detail when these 2 are true, else the grid is already displayed
                // if(count === 1 && singleDetail == true) {
                    // panel.setData(records[0]);
                    // panel.loadForm(panel);
                // } 
// 
                // // CG: seems like useless code now
                // // if(propertiescnt === 0){
                    // // //Show the Add Property Button
                    // // for(i=0;i<frmarrels.length;i++){
                        // // frmarrels[i].hide();
                    // // }
                    // // me.getDealdetailpropertyfieldset().down('gridpanel').hide();
                    // // me.getDealdetailsformpanel().down('#propertydetailclosebtn').hide();
                // // }else if(propertiescnt === 1){
                    // // //Load the one property Details into form & Show Add Property btn
                    // // for(i=0;i<frmarrels.length;i++){
                        // // frmarrels[i].show();
                    // // }
                    // // me.getDealdetailpropertyfieldset().down('gridpanel').hide();
                    // // me.getDealdetailpropertyfieldset().getForm().loadRecord(records[0]);
                // // }else if(propertiescnt > 1){
                    // // //Hide the form & Load the property grid
                    // // for(i=0;i<frmarrels.length;i++){
                        // // frmarrels[i].hide();
                    // // }
                    // // me.getDealdetailpropertyfieldset().down('gridpanel').show();
                    // // me.getDealdetailsformpanel().down('#propertydetailclosebtn').hide();
                // // }
//                 
                // // not sure why, but leave it for now
                // if(count > 0){
                   // var loansizegrid = me.getController('DealActivityController').getLoansizingpanel().down('#loanpropertygridpanel');
                    // if(loansizegrid){
                        // console.log("Loan Size Gird Exist");
                        // loansizegrid.getSelectionModel().select(rowindex);
                    // }
					// var propgrid = me.getController('CreateNewDealController').getCreatenewdealpanel().down('#createnewdealpropertygrid');
					// if(propgrid)
					// {
			        	// propgrid.getSelectionModel().selectAll();
					// }
                // }
                // me.setLoanSize();
            // }
        // });
    // },

    dealDetailPropertiesGridRightClick: function(me, record, tr, rowIndex, e, eOpts) {
        e.stopEvent();
        console.log("Show Menu for Properties Add");
        if(this.getDealdetailpropertiesmenu()){
            this.getDealdetailpropertiesmenu().showAt(e.getXY());
        }
    },

    dealDetailPropertiesOptionItemClick: function(item, e, eOpts) {
    //    debugger;
        console.log("Deal Detail Contact Menu Item Clicked.");
        if(item.text==="Add Property"){
            console.log("Properties Add Menu Item Clicked");
            this.addPropertyToDealBtnClick();
        } else if(item.text==="Remove Property"){
            console.log("Properties Remove Menu Item Clicked");
            this.removePropertyFromDealBtnClick();
        } else if(item.text==="View Master Data"){
            console.log("View Master Data Menu Item Clicked");
            this.viewMasterDataItemClick();
        } else if(item.text==="List ACRIS Documents"){
            console.log("List ACRIS Documents Menu Item Clicked");
            this.listACRISDocumentsItemClick();
        } else if(item.action==="changeprimary"){
            console.log("Change Primary Menu Item Clicked");
            this.changePrimaryPropertyMenuItemClick();
        } else if(item.action==="editproperty"){
            console.log("Edit Property Menu Item Clicked");
            this.editPropertyMenuItemClick();
        }
    },
	
	viewMasterDataItemClick: function(){
		console.log("View Master Data Menu Item Click");
		var me = this,viewmasterdataform;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('viewmasterdataform')){
			console.log("Use Old Panel");
			viewmasterdataform = detailpanel.down('viewmasterdataform');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	viewmasterdataform = Ext.create('DM2.view.ViewMasterDataForm');
		}
        eastregion.getLayout().setActiveItem(viewmasterdataform);
		me.loadMasterDataForm(viewmasterdataform);
	},
	
	viewMasterDataFormCloseBtnClick: function(btn){
		console.log("Close View Master Data Form");
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
	},
	
	loadMasterDataForm: function(viewmasterdataform){
		var me = this;
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var dealproperty = detailpanel.down('dealdetailpropertyfieldset');
			
        var contextMenu = dealproperty.contextMenu;
        var row = contextMenu.getRec();
        
        // when the context menu is invoked from a form, there won't be any data set to the context menu
        if(!row) {
            row = dealproperty.getData();
        } else {
            dealproperty.setData(row);
        }
        var dealid = row.get('dealid');
        var idDeal_has_Property = row.get('idDeal_has_Property');
		var propertyid = row.get('propertyid');
		var idPropertyMaster = row.get('idPropertyMaster');
		var idmainMasterFileTable = row.get('idmainMasterFileTable');
		
		viewmasterdataform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:propertyMaster',
            scope:this,
            method:'GET',
			params:{
				filter:'idPropertyMaster = '+idmainMasterFileTable
			},
            success: function(response, opts) {
                viewmasterdataform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200){
                    var propertyrec = Ext.create('DM2.model.PropertyMaster',obj[0]);
                    viewmasterdataform.getForm().loadRecord(propertyrec);
					me.getController('PropertyController').loadAKAList(propertyrec,context);
                } else {
                    console.log("Get Loan Sizing Info Failure.");
                    Ext.Msg.alert('Get Loan Sizing Info failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                viewmasterdataform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Get Loan Sizing Info', obj.errorMessage);
            }
        });
	},

	listACRISDocumentsItemClick: function(){
		console.log("list ACRIS Documents Menu Item Click");
		var me = this,listacrisdocuments;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('listacrisdocuments')){
			console.log("Use Old Panel");
			listacrisdocuments = detailpanel.down('listacrisdocuments');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	listacrisdocuments = Ext.create('DM2.view.ListACRISDocuments');//me.getAdddealpropertyform();
		}
        eastregion.getLayout().setActiveItem(listacrisdocuments);
		var acrispropertydocsgrid = listacrisdocuments.down('acrispropertydocsgrid');
		
		var dealproperty = detailpanel.down('dealdetailpropertyfieldset');
			
        var contextMenu = dealproperty.contextMenu;
        var row = contextMenu.getRec();
        
        // when the context menu is invoked from a form, there won't be any data set to the context menu
        if(!row) {
            row = dealproperty.getData();
        } else {
            dealproperty.setData(row);
        }
        var dealid = row.get('dealid');
        var idDeal_has_Property = row.get('idDeal_has_Property');
		var propertyid = row.get('propertyid');
		var APN = row.get('APN');
		var block = row.get('block');
		var Lot = row.get('Lot');
		var Boro = row.get('Boro');
        contextMenu.clearRec();
		me.loadAcrisDocumentsbyAPN(acrispropertydocsgrid,Boro,block,Lot);
	},
	
	loadAcrisDocumentsbyAPN: function(acrispropertydocsgrid,Boro,block,Lot){
		console.log("Remove Property from the Deal");
        var me = this;
		
	 	var filterstarr = [];
        /*
		var filterst = "apn = '"+APN+"'";
        filterstarr.push(filterst);
		*/
		var filterst1 = "boro = '"+Boro+"'";
        filterstarr.push(filterst1);
		var filterst2 = "block = '"+block+"'";
        filterstarr.push(filterst2);
		var filterst3 = "lot = '"+Lot+"'";
        filterstarr.push(filterst3);
		
		//Load Acris Document Store
		var acrisDocsSt = acrispropertydocsgrid.getStore();
        acrisDocsSt.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        acrisDocsSt.load({
            params:{
                filter :filterstarr,
				order:'recordedDate DESC'
            },
            callback: function(records, operation, success){
            }
        });
	},
	
	listACRISDocumentsCloseBtnClick: function(panel){
		console.log("Close list ACRIS Documents Form");
		var me = this;
		if(panel.up('tab-deal-detail')){
			var context = panel.up('tab-deal-detail');
			var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		} else if(panel.up('tab-sales-detail')){
			var context = panel.up('tab-sales-detail');
			var eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		} else {
			var tabpropertypanel = panel.up('tab-properties');
			var context = tabpropertypanel.getActiveTab();		
			me.getController('PropertyController').closeRecvDealDetailDocs(context);
		}
	},
	
	acrisPropertyDocsGridItemSelect: function(selModel, record, index, eOpts){
		console.log("Acris Property Docs Grid Item Select");
		var me = this;
		//var context = selModel.view.ownerCt.up('tab-deal-detail');
		var listacrisdocuments = selModel.view.ownerCt.up('listacrisdocuments');
		
		var acriscontactsgrid = listacrisdocuments.down('acriscontactsgrid');
		var acrisnotesgrid = listacrisdocuments.down('acrisnotesgrid');
		var acrispropertydocsgrid = listacrisdocuments.down('acrispropertydocsgrid');
		
		acrispropertydocsgrid.down('form').getForm().loadRecord(record);
		
		var acrisid = record.get('idAcris');
		
		var filterstarr = [];
		var filterst1 = "idAcris = '"+acrisid+"'";
        filterstarr.push(filterst1);
		
		//Load Acris Contacts Store
		var acrisContactsSt = acriscontactsgrid.getStore(); //('Notes');
        acrisContactsSt.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        acrisContactsSt.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){
				if(acrisContactsSt.getCount()>0){
					acriscontactsgrid.getSelectionModel().select(0);
				}
            }
        });
		
		//Load Acris Notes Store
		var acrisNotesSt = acrisnotesgrid.getStore(); //('Notes');
        acrisNotesSt.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        acrisNotesSt.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){
				/*if(acrisnotesgrid.getCount()>0){
					acrisnotesgrid.getSelectionModel().select(0);
				}*/
            }
        });
	},
	
	acrisContactsGridItemSelect: function(selModel, record, index, eOpts){
		console.log("Acris Property Docs Grid Item Select");
		var me = this;
		var listacrisdocuments = selModel.view.ownerCt.up('listacrisdocuments');
		var acriscontactsgrid = listacrisdocuments.down('acriscontactsgrid');
		acriscontactsgrid.down('form').getForm().loadRecord(record);
	},
	
    loadPropertiesDetailForm: function(grid, record, item, index, e, eOpts) {
        console.log("On Click of property item, hide the grid & load the form with data");
        var me = this;
        //console.log(e.getKey());
        //console.log(eOpts.getKey());
        me.getDealdetailpropertyfieldset().down('gridpanel').hide();
        var arrels = me.getDealdetailpropertyfieldset().query('panel[formel=yes]');
        console.log(arrels);
        var j;
        for(j=0;j<arrels.length;j++){
            arrels[j].show();
        }
        me.getDealdetailpropertyfieldset().getForm().loadRecord(record);
        me.getDealdetailsformpanel().down('#propertydetailclosebtn').show();
    },

    dealDetailPropertiesCloseBtnClick: function(btn) {
        console.log("Close Deal Properties Detail Form");
        var me = this;
        btn.hide();
        var arrels = me.getDealdetailpropertyfieldset().query('panel[formel=yes]');
        console.log(arrels);
        var j;
        for(j=0;j<arrels.length;j++){
            arrels[j].hide();
        }
        me.getDealdetailpropertyfieldset().getForm().reset();
        me.getDealdetailpropertyfieldset().down('gridpanel').show();
    },

    loadAllPropertiesStore: function(filterstarr,context) {
		var me = this;
		//var adddealpropertyform = context.down('dealdetailsformpanel').down('adddealpropertyform');
		var adddealpropertyform = context.down('adddealpropertyform');
		var allpropertiesst = adddealpropertyform.down('#adddealpropertygrid').getStore();
		
		var allpropertiesstproxy = allpropertiesst.getProxy();
        /*allpropertiesstproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var allpropertiesstproxyCfg = allpropertiesstproxy.config;
        allpropertiesstproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		allpropertiesst.setProxy(allpropertiesstproxyCfg);
        allpropertiesst.load({
            params:{
                filter :filterstarr
            }
        });
    },

    filterPropertyBtnClick: function(btn) {
        var me = this;
        var propcityval = me.getAdddealpropertyform().getForm().findField('propcity').getValue();
        var propaddressval = me.getAdddealpropertyform().getForm().findField('propaddress').getValue();
        var filterstarr = [];
        if(propcityval!==''){
            var filterst = "city like '%"+propcityval+"%'";
            filterstarr.push(filterst);
        }
        if(propaddressval!==''){
            var filterst = "address like '%"+propaddressval+"%'";
            filterstarr.push(filterst);
        }
        ///Load All the Properties Store///
        this.loadAllPropertiesStore(filterstarr);
        ///
    },

    onDealAllPropertiesGridpanelItemClick: function(grid, record, item, index, e, eOpts) {
        var me = this;
        grid.up('adddealpropertyform').getForm().loadRecord(record);
    },
	
	onDealAllPropertiesGridpanelItemSelect: function(selModel, record, index , eOpts) {
        var me = this;
		var adddealpropertyform = selModel.view.ownerCt.up('adddealpropertyform');
        var lastDealID = record.get('lastDealID');
		var lastDealStatus = record.get('lastDealStatus');
		/*if(adddealpropertyform.config.backview  && (adddealpropertyform.config.backview == "createnewdeal" || adddealpropertyform.config.backview == "createnewdealfrompropertypanel" || adddealpropertyform.config.backview == "createnewsalesdealfrompropertypanel")){*/
			if(adddealpropertyform.config.toast){
				console.log("Old Toast Closed");
				adddealpropertyform.config.toast.close();
			}
			if((lastDealID==null) || (lastDealID!=null && lastDealStatus=='CL')){
				adddealpropertyform.down('button[action="savedealpropertybtn"]').enable();
			} else if(lastDealID!=null && lastDealStatus!='CL'){
				var title = "Property add failed";
				errText = "Property has deal with status not closed. Please try to add another property.";
				toast = me.getController('DealDetailController').showToastWin(errText,title,'default',null,'tr',true,10000);
				adddealpropertyform.config.toast = toast;
				adddealpropertyform.down('button[action="savedealpropertybtn"]').disable();
				//return false;
			}
		/*} else {
			adddealpropertyform.down('button[action="savedealpropertybtn"]').enable();
		}*/
    },

    savePropertyToDealBtnClick: function(btn) {
        console.log("Add Property To Deal");
        var me = this,propertyid,row;
		var adddealpropertyform = btn.up('adddealpropertyform');
		if(adddealpropertyform.config.backview  && adddealpropertyform.config.backview == "createnewdeal"){
			me.getController('CreateNewDealController').addNewPropertyToDealClick(adddealpropertyform);
		} else if(adddealpropertyform.config.backview  && (adddealpropertyform.config.backview == "createnewdealfrompropertypanel" || adddealpropertyform.config.backview == "createnewsalesdealfrompropertypanel")){
			me.getController('CreateNewDealFromPropertyController').addNewPropertyToDealClick(adddealpropertyform);
		} else {
			var context = me.getController('DealDetailController').getTabContext(btn);
			if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
				var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
				
				var record = context.record;		
				var dealid = record.data.idDeal;
		
				var allpropertygrid = detailpanel.down('#adddealpropertygrid');
				if (allpropertygrid.getSelectionModel().hasSelection()) {
					row = allpropertygrid.getSelectionModel().getSelection()[0];
					console.log(row.get('idPropertyMaster'));
					propertyid = row.get('idPropertyMaster');
				} else {
					Ext.Msg.alert('Failed', "Please select property from Allproperties Grid.");
					return false;
				}
				me.getController('CreateNewDealFromPropertyController').loadFromPropertyTable(context,row,'addpropertytodeal');
				///If result row is already from property Table Then Direct insert it.
				//me.addPropertyToDeal(context,obj[0].idProperty);
			}
		}
    },
		
	addPropertyToDeal: function(context,propertyid){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);			
		var record = context.record;		
		var dealid = record.data.idDeal;
		
		var adddealpropertyform = detailpanel.down('adddealpropertyform');
		if(adddealpropertyform.config.viewMode=="changeprimary"){
			me.changePrimaryPropertySave(context,propertyid,dealid);
		} else {
			var data = [{
				'idDeal': dealid,
				'idProperty' : propertyid,
				'mortgageType' : adddealpropertyform.getForm().findField('mortgageType').getValue(),
				'isPrimary' : false
			}];
			Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
				},
				url:DM2.view.AppConstants.apiurl+'mssql:DealxProp',
				params: Ext.util.JSON.encode(data),
				scope:this,
				success: function(response, opts) {
					console.log("Add Property to deal is submitted.");
					var obj = Ext.decode(response.responseText);
					if(obj.statusCode == 201){
						var dealdetailpropertyfieldset = detailpanel.down('dealdetailpropertyfieldset');
						me.loadPropertyDetails(dealdetailpropertyfieldset);
						me.addDealPropertyCloseBtnClick(detailpanel.down('adddealpropertyform'));
						dealdetailpropertyfieldset.switchPanel(0);
					} else {
						console.log("Add Property to Deal Failure.");
						Ext.Msg.alert('Property add failed', obj.errorMessage);
					}
				},
				failure: function(response, opts) {
					console.log("Add Property to Deal Failure.");
					var obj = Ext.decode(response.responseText);
					var errMsg = obj.errorMessage;
					var errText = "";
					var n = errMsg.indexOf("Violation of UNIQUE KEY constraint");
					if(n==-1){
						errText = obj.errorMessage;
						//Ext.Msg.alert('Property add failed', obj.errorMessage);
					} else {
						errText = "Property already exist. Please try to add another property.";
						//Ext.Msg.alert('Property add failed', "Property already exist.Please try to add another property.");
					}
					var title = "Property add failed";
					me.showToast(errText,title,'error',null,'tr',true);
				}
			});
		}
	},

    showGmapPanel: function(btn) {
        console.log("Show Gmap Panel");
        var eastregion = this.getDealdetailsformpanel().down('[region=east]');
        eastregion.getLayout().setActiveItem(this.getDealdetailgmapcontainer());
        this.getGmapwindow().show();
    },

    onDealDetailGmapAfterRender: function(component, eOpts) {
        var me = this;
        console.log("Map Window Rendered");
        var geocoder = new google.maps.Geocoder();

        var propertiescnt = Ext.getStore('Properties').getCount();
        console.log("Properties Cnt"+propertiescnt);
        if(propertiescnt > 0){
            var stateVal;
            var streetVal;
            var cityVal;
            if(propertiescnt===1){
                var propval   = Ext.getStore('Properties').getAt(0);
                stateVal  = propval.get('state');
                streetVal = propval.get('street');
                cityVal   = propval.get('city');
            }
            else{
                var propgrid = me.getDealdetailsformpanel().down('#dealdetailpropertygridpanel');
                if (propgrid.getSelectionModel().hasSelection()) {
                    var row = propgrid.getSelectionModel().getSelection()[0];

                    stateVal  = row.get('state');
                    streetVal = row.get('street');
                    cityVal   = row.get('city');
                }
                else{
                    Ext.Msg.alert('Failed', "Please select property from propertygrid.");
                    return false;
                }
            }

            //Demo Address Needs to send for getting the GeoCode
            var geocoderRequest = { address: streetVal+', '+cityVal+', '+stateVal };

            /*geocoder.geocode(geocoderRequest, function(results, status){
                        //do your result related activities here, maybe push the coordinates to the backend for later use, etc.
                        console.log("Success"+status);
                        //var res=r;//Ext.decode(r.responseText);
                        if(status=='OK') {*/
            var latitude  = -34.397;//results[0].geometry.location.lat();
            var longitude = 150.644;//results[0].geometry.location.lng();
            console.log(latitude);
            console.log(longitude);
            var myOptions = {
                center: new google.maps.LatLng(latitude, longitude),
                zoom: 14,
                //panControl: false,
                //mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP //ROADMAP
            };

            var map = new google.maps.Map(component.body.dom,myOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude,longitude),
                map: map,
                title: streetVal+', '+cityVal+', '+stateVal//'Click to zoom'
            });
            /*}
                        else
                        {
                            Ext.Msg.alert('We\'re Sorry','Can not able to find the address ' + r.status);
                        }
                    });*/
        }
        else
        {
            Ext.Msg.alert('We\'re Sorry','There is no property for this deal');
        }
    },

    changeRoleContactSaveBtnClick: function(btn) {
        console.log("Save Change Role for Deal Contact");
        var me = this, dealid, idDealxCont;
		
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        // var dealcontactgrid = me.getDealdetailsformpanel().down('#dealdetailcontactgrid');
        var dealcontact = detailpanel.down('dealdetailcontactfieldset');

        var rec = dealcontact.getData();

        if(!rec) {
            rec = dealcontact.down('grid').getSelectionModel().getSelection()[0];
        }

        if (rec) {
            console.log(rec.get('dealid'));
            dealid = rec.get('dealid');
            idDealxCont = rec.get('idDealxCont');
        } else {
            Ext.Msg.alert('Failed', "Please select contact from Contact Grid.");
            return false;
        }
        var changerolecontactform = detailpanel.down('changerolecontactform');
        var contactTypeval = changerolecontactform.getForm().findField('contactType').getValue();

        var data = [{
            '@metadata': { 'checksum': 'override' },
            'idDealxCont' :idDealxCont,
            'contactType' : contactTypeval
        }];
        console.log("Edit PUT for Update");

        changerolecontactform.setLoading(true);
        Ext.Ajax.request({
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:DealxCont/'+idDealxCont,
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Note is submitted.");
                changerolecontactform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200){
					var dealdetailcontactfieldset = detailpanel.down('dealdetailcontactfieldset');
					me.loadDealContactDetails(dealdetailcontactfieldset);

                    changerolecontactform.getForm().reset();
                    me.changeRoleContactCloseBtnClick(changerolecontactform);
                    dealcontact.switchPanel(0);    
                } else {
                    console.log("Change Role  Failure.");
                    Ext.Msg.alert('Change Role failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Change Role Failure.");
                changerolecontactform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Change Role failed', obj.errorMessage);
            }
        });
    },

    afterActivityComboRender: function(combo, eOpts) {
        /*console.log("After Activity Combo Render");
        var activityproxy = Ext.getStore('Activities').getProxy();
        activityproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('Activities').load({
            callback: function(records, operation, success){
                combo.select(combo.getStore().getAt(0));
            }
        });*/
    },

    afterContactTypeComboRender: function(combo, eOpts) {
        var me = this;
        var contacttypeproxy = Ext.getStore('ContactTypes').getProxy();
        contacttypeproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('ContactTypes').load({
            callback: function(records, operation, success){
                combo.select(combo.getStore().getAt(0));
            }
        });
    },
    
    loadExistingLoanDetails: function(panel) {
        var me = this;
        var context = panel.up('tab-deal-detail');
        var record = context.record;
        var dealid = record.data.idDeal;    
        var grid = panel.down('grid');
        var store = grid.getStore();        
        
        var exstloanst = store;
        
        exstloanst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        exstloanst.load({
            params:{
                filter :'dealid = '+dealid
            },
            scope: {
                me: me,
                context:context,
                record: record,
                dealid: dealid,
                grid: grid,
                store: store,
                panel: panel
            },
            callback: function(records, operation, success){
                
                var me = this.me;
				me.afterExistingLoanDataLoad();
            }
        });
    },
	
	bindExistingLoanDetails: function(tabItem){		
		console.log("Bind Existing Loan Details");
		var me = this;
		var panel = tabItem.down('dealdetailexistingloanfieldset');
		var existloan_st = panel.down('grid').getStore();
		existloan_st.loadData(tabItem.dealDetailStore.data.items[0].data.ExistingLoanPerDeal);
		me.afterExistingLoanDataLoad(panel);		
	},
	afterExistingLoanDataLoad: function(panel){
		var existloan_st = panel.down('grid').getStore();
		var count = existloan_st.getCount();	    
		var singleDetail = panel.detailOnSingleRecord; 
		
		if(count === 1 && singleDetail == true) {
			panel.setData(existloan_st.getAt(0));
			panel.loadForm(panel);
			panel.down('grid').getHeaderContainer().show();
		} else if(count===0){
			panel.down('grid').getHeaderContainer().hide();
		} else {
			panel.down('grid').getHeaderContainer().show();
		}
	},
    
    loadLoanDetailForm: function(grid, record, item, index, e, eOpts) {
        console.log("On Click of property item, hide the grid & load the form with data");
        var me = this;
        me.getDealdetailexistingloanfieldset().down('gridpanel').hide();
        var arrels = me.getDealdetailexistingloanfieldset().query('panel[formel=yes]');
        console.log(arrels);
        for(var j=0;j<arrels.length;j++){
            arrels[j].show();
        }
        me.getDealdetailexistingloanfieldset().getForm().loadRecord(record);
        me.getDealdetailsformpanel().down('#loandetailclosebtn').show();
    },

    dealDetailLoansCloseBtnClick: function(btn) {
        console.log("Close Deal Properties Detail Form");
        var me = this;
        btn.hide();
        var arrels = me.getDealdetailexistingloanfieldset().query('panel[formel=yes]');
        console.log(arrels);
        for(var j=0;j<arrels.length;j++){
            arrels[j].hide();
        }
        me.getDealdetailexistingloanfieldset().getForm().reset();
        me.getDealdetailexistingloanfieldset().down('gridpanel').show();
    },

    afterChangeContactTypeComboRender: function(combo, eOpts) {
        var me = this;
		var tabdealdetail = me.getController('DealDetailController').getTabContext(combo);
        var cntTypeSt = combo.getStore('ContactTypes');
        cntTypeSt.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        cntTypeSt.load({
            callback: function(records, operation, success){
                
                var contact = tabdealdetail.down('dealdetailcontactfieldset'); //me.getDealdetailcontactfieldset();
                var rec = contact.getData();
        
                if(!rec) {
                    rec = contact.down('grid').getSelectionModel().getSelection()[0];
                }
                
                // CG
                // var dealcontactgrid = me.getDealdetailsformpanel().down('#dealdetailcontactgrid');
                tabdealdetail.down('changerolecontactform').down('combo[name="contactType"]').select(rec.get('contactType'));
                tabdealdetail.down('changerolecontactform').down('textfield[name="fullName"]').setValue(rec.get('fullName'));
                
                // if (dealcontactgrid.getSelectionModel().hasSelection()) {
                    // // var row = dealcontactgrid.getSelectionModel().getSelection()[0];
                    // // console.log(row.get('type'));
                // }
            }
        });
    },

    onActivityChange: function(combo, newValue, oldValue, eOpts) {
        var me = this;
        console.log("Activity Value Changed");
        if(combo.config.firstchange!==true){
            console.log("Activity Change"+newValue);
            combo.config.firstchange = false;
			
			var context = me.getController('DealDetailController').getTabContext(combo);		
            me.getController('DealActivityController').showActivity(newValue,combo.getRawValue(),context);
        }
        if(combo.config.firstchange===true){
            combo.config.firstchange = false;
        }
    },
    
    bindNewLoanDetails: function(tabItem){
		console.log("Bind New Loan Details");
		var me = this;
		var panel = tabItem.down('dealdetailnewloanfieldset');
		var dealnewloanst = panel.down('grid').getStore();
		dealnewloanst.loadData(tabItem.dealDetailStore.data.items[0].data.QuotePerDeal);
		dealnewloanst.filter('selected', 'S');
		//dealnewloanst.filter('selected', 1);
		//dealnewloanst.filter('selected', true);
		me.afterDealNewLoansDataLoad(panel);		
	},
	
	afterDealNewLoansDataLoad: function(panel){
		var dealnewloanst = panel.down('grid').getStore();
		var count = dealnewloanst.getCount();	    
		var singleDetail = panel.detailOnSingleRecord; 
		
		if(count === 1 && singleDetail == true) {
			panel.setData(dealnewloanst.getAt(0));
			panel.loadForm(panel);
			panel.down('grid').getHeaderContainer().show();
		} else if(count === 0){
			panel.switchPanel(0);
			panel.down('grid').getHeaderContainer().hide();
		} else {
			panel.down('grid').getHeaderContainer().show();
		}
	},
	
    loadNewLoanDetails: function(panel) {
        
        var me = this;
        var context = panel.up('tab-deal-detail');
        var record = context.record;
        var dealid = record.data.idDeal;    
        var grid = panel.down('grid');        
        var store = grid.getStore();
               
        var quotesproxy = store.getProxy();
        quotesproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });

        var filterstarr = [];
        filterstarr.push('dealid = '+ dealid);
        //filterstarr.push("selected=1");
		//filterstarr.push("selected=true");
		filterstarr.push("selected='S'");

        store.load({
            params:{
                filter : filterstarr
            },
            scope: {
                me: me,
                context: context,
                record: record,
                dealid: dealid,
                grid: grid,
                store: store,
                panel: panel
            },
            callback: function(records, operation, success){
                var me = this.me;
                var panel = this.panel;
                me.afterDealNewLoansDataLoad(panel);
            }
        });
    },

    loadMasterLoanDetails: function(panel) {
        
        var me = this;
        var context = panel.up('tab-deal-detail');
        var record = context.record;
        var dealid = record.data.idDeal;    
        var grid = panel.down('grid');        
        var store = grid.getStore();        
        
        var msloan_st = store;
        
        msloan_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });

        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
        //filterstarr.push("selected='T'");

        msloan_st.load({
            params:{
                filter : filterstarr
            },
            scope: {
                me: me,
                context: context,
                record: record,
                dealid: dealid,
                grid: grid,
                store: store,
                panel: panel
            },            
            callback: function(records, operation, success){
                var me = this.me;
				me.afterMasterLoanDataLoad();                
            }
        });
    },
	
	bindMasterLoanDetails: function(tabItem){
		console.log("Bind Master Loan Details");
		var me = this;
		var panel = tabItem.down('dealdetailmasterloanfieldset');
		var msloan_st = panel.down('grid').getStore();
		msloan_st.loadData(tabItem.dealDetailStore.data.items[0].data.MasterLoansPerDeal);
		me.afterMasterLoanDataLoad(panel);		
	},
	afterMasterLoanDataLoad: function(panel){
		var msloan_st = panel.down('grid').getStore();
		var count = msloan_st.getCount();	    
		var singleDetail = panel.detailOnSingleRecord; 
		
		if(count === 1 && singleDetail == true) {
			panel.setData(msloan_st.getAt(0));
			panel.loadForm(panel);
			panel.down('grid').getHeaderContainer().show();
		}else if(count===0){
			panel.down('grid').getHeaderContainer().hide();
		} else {
			panel.down('grid').getHeaderContainer().show();
		}
	},

    removePropertyFromDealBtnClick: function() {
        
        console.log("Remove Property from the Deal");
        var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var dealproperty = detailpanel.down('dealdetailpropertyfieldset');
			
        var contextMenu = dealproperty.contextMenu;
        var row = contextMenu.getRec();
        
        // when the context menu is invoked from a form, there won't be any data set to the context menu
        if(!row) {
            row = dealproperty.getData();
        } else {
            dealproperty.setData(row);
        }
		var primaryProperty = row.get('primaryProperty');
		if(primaryProperty==true){
			Ext.Msg.alert('Delete Property Alert', "Primary property can not be removed.");
			return false;
		}
        var dealid = row.get('dealid');
        //var idDealxProp = row.get('idDealxProp');
		var idSetup = row.get('idSetup');
        contextMenu.clearRec(); 
       
        // if (dealpropertygrid.getSelectionModel().hasSelection()) {
            // var row = dealpropertygrid.getSelectionModel().getSelection()[0];
            // console.log(row.get('dealid'));
            // var dealid = row.get('dealid');
            // var idDeal_has_Property = row.get('idDeal_has_Property');
        // }
        // else{
            // if(dealpropertygrid.getStore().getCount()===1){
                // var tmprec = dealpropertygrid.getStore().getAt(0);
                // var dealid = tmprec.get('dealid');
                // var idDeal_has_Property = tmprec.get('idDeal_has_Property');
            // }
            // else
            // {
                // Ext.Msg.alert('Failed', "Please select property from Property Grid.");
                // return false;
            // }
        // }

        var data = {
            'checksum': "override"
        };
        
        // needs to be further refined to deal with when there is only 1 record left.
        Ext.Msg.show({
            title:'Delete Property?',
            message: 'Are you sure you want to delete the proprty from deal?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:DealxProp/'+idSetup+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            console.log("Property is removed from deal.");
                            var obj = Ext.decode(response.responseText);
                            
                            dealproperty.switchPanel(0);
                            if(obj.statusCode == 200) {
								var dealdetailpropertyfieldset = detailpanel.down('dealdetailpropertyfieldset');
                                me.loadPropertyDetails(dealdetailpropertyfieldset);
                            } else {
                                console.log("Delete Property Failure.");
                                Ext.Msg.alert('Delete Property failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Delete Property Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Property failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    loadContactDetailForm: function(grid, record, item, index, e, eOpts) {
        console.log("On Click of contact item, hide the grid & load the form with data");
        var me = this;
        me.displayContactDetailForm(record);
    },

    dealDetailContactCloseBtnClick: function() {
        console.log("Close Deal Contact Detail Form");
        var me = this;
        var cntfldset = me.getDealdetailcontactfieldset();

        var grid = cntfldset.down('#dealdetailcontactgrid');
        var cntform = cntfldset.down('#dealdetailcontactform');

        cntform.getForm().reset();
        cntfldset.getLayout().setActiveItem(grid);
        cntfldset.down('#contactdetaildealclosebtn').hide();
    },

    dealDetailNewLoansCloseBtnClick: function(btn) {
        console.log("Close Deal Properties Detail Form");
        var me = this;
        btn.hide();
        var arrels = me.getDealdetailnewloanfieldset().query('panel[formel=yes]');
        console.log(arrels);
        for(var j=0;j<arrels.length;j++){
            arrels[j].hide();
        }
        me.getDealdetailnewloanfieldset().getForm().reset();
        me.getDealdetailnewloanfieldset().down('gridpanel').show();
    },

    loadNewLoanDetailForm: function(grid, record, item, index, e, eOpts) {
        console.log("On Click of loan item, hide the grid & load the form with data");
        var me = this;
        me.getDealdetailnewloanfieldset().down('gridpanel').hide();
        var arrels = me.getDealdetailnewloanfieldset().query('panel[formel=yes]');
        console.log(arrels);
        for(var j=0;j<arrels.length;j++){
            arrels[j].show();
        }
        me.getDealdetailnewloanfieldset().getForm().loadRecord(record);
        me.getDealdetailsformpanel().down('#newloandetailclosebtn').show();
    },

    researchMenuClick: function(menu, item, e, eOpts) {
        console.log("Research Menu Clicked");
		var me = this;
        if(item.text==="Gmap"){
            this.getController('PropertyController').showOnGmap();
        } else if(item.text==="Zillow") {
            this.getController('PropertyController').showOnZillow();
        } else if(item.text==="Reonomy") {
            console.log("Open the reonomy link on new page");
            this.getREOLURL();
        } else if(item.text==="Oasis") {
            console.log("Open the Oasis link on new page");
			var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
			var context = activemenutab.getActiveTab();		
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
			
			var propertyQuad = detailpanel.down('dealdetailpropertyfieldset');
			var dealproperty_st = propertyQuad.down('grid').getStore();
			var propertiescnt = dealproperty_st.getCount();
			if(propertiescnt > 0){
				if(propertiescnt===1){
					proprec   = dealproperty_st.getAt(0);
				} else {
					proprec = propertyQuad.getData();
					if(!proprec) {
						proprec = propertyQuad.down('grid').getSelectionModel().getSelection()[0]
					}
					
					if(proprec) {
					} else{
						Ext.Msg.alert('Failed', "Please select property from propertygrid.");
						return false;
					}
				}
			}
			blockval  = proprec.get('block');
			lotval    = proprec.get('Lot');
			boroval   = proprec.get('Boro');
			me.getController('PropertyController').displayOasisMap(boroval,blockval,lotval);
        }
    },

    editDealContact: function() {
        console.log("Edit Deal Contact");
        var me = this,contactdetailsform;
        var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var dealcontact = detailpanel.down('dealdetailcontactfieldset');
        var rec = dealcontact.getData();
        
        if(!rec) {
            rec = dealcontact.down('grid').getSelectionModel().getSelection()[0];
        }
        
        var dealid = rec.get('dealid');
        var contactid = rec.get('contactid');
        var idDeal_has_Contact = rec.get('iddeal_has_contact');

		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('contactdetailsform')){
			contactdetailsform = detailpanel.down('contactdetailsform');
		} else {
        	contactdetailsform = Ext.create('DM2.view.ContactDetailsForm');
		}
        eastregion.getLayout().setActiveItem(contactdetailsform);		
		contactdetailsform.config.backview = "contactquadrant";
		        		
        me.getController('ContactController').loadContactDetails(contactid,context);
		contactdetailsform.down('tool[type="close"]').show();		
    },

    onActiveDealUserChange: function(checkcolumn, rowIndex, checked, eOpts) {
        console.log("DealUser State Changed to"+checked);
        /*var me = this;
        var msg = "";
        if(checked){
            msg = "Are you sure you want to activate the user?";
        } else {
            msg = "Are you sure you want to deactivate the user?";
        }
        var dealusrid = Ext.getStore('DealUsers').getAt(rowIndex).get('idDealxUser');
        var dealid = Ext.getStore('DealUsers').getAt(rowIndex).get('dealid');
        var data = [{
            '@metadata': { 'checksum': 'override' },
            'idDealxUser' :dealusrid,
            'active':checked
        }];
        Ext.Msg.show({
            title:'Activate User?',
            message: msg,
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            scope:me,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'PUT',
                        url:DM2.view.AppConstants.apiurl+'mssql:Deal_has_User/'+dealusrid,
                        params: Ext.util.JSON.encode(data),
                        scope:this,
                        success: function(response, opts) {
                            console.log("User status is updated.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){                                
                                me.getController('DealUserController').loadDealUserDetails(dealid);
                                me.getController('CreateNewDealController').loadDealActiveUser(dealid);
                            } else {
                                console.log("User status change Failure.");
                                Ext.Msg.alert('User status change Failure', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            console.log("User status change Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('User Status change failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
        return false;
		*/
    },

    onDealdetailpropertyformAfterRender: function(component, eOpts) {
        var me = this;
        component.getEl().on('contextmenu', function(e) {
            console.log("Right Click on panel");
            e.preventDefault();
            //e.stopEvent();
            console.log("Show Menu for Properties Add");
			console.log(me.getDealdetailpropertiesmenu().getItems());
            if(me.getDealdetailpropertiesmenu()){
                me.getDealdetailpropertiesmenu().showAt(e.getXY());
            }
             //contextMenu.show(Ext.getCmp('your-button').getEl());
        });
    },

    displayContactDetailForm: function(record) {
        var me= this;
        var cntcontpanel = me.getDealdetailcontactfieldset();      
        
        var cntdtform = cntcontpanel.down('#dealdetailcontactform');

        cntcontpanel.getLayout().setActiveItem(cntdtform);

        cntdtform.getForm().loadRecord(record);

        //cntcontpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            method:'GET', //Delete Method with primary key
            url:DM2.view.AppConstants.apiurl+'mssql:Contact/'+record.get('contactid'),
            scope:this,
            success: function(response, opts) {
                //cntcontpanel.setLoading(false);
                console.log("Get the Contact Detail for contact id.");
                var obj = Ext.decode(response.responseText);
                console.log(obj);
                console.log(response);
                if(response.status == 200)
                {
                    console.log(obj);
                    var itemval = obj[0];
                    cntcontpanel.down('#contactdetaildealclosebtn').show();
                    cntdtform.getForm().findField('fullName').setValue(itemval.fullName);
                    cntdtform.getForm().findField('title').setValue(itemval.title);
                    cntdtform.getForm().findField('companyName').setValue(itemval.companyName);

                    cntdtform.getForm().findField('email_1').setValue(itemval.email_1);
                    cntdtform.getForm().findField('email_2').setValue(itemval.email_2);
                    cntdtform.getForm().findField('officePhone_1').setValue(itemval.officePhone_1);
                    cntdtform.getForm().findField('officePhone_2').setValue(itemval.officePhone_2);

                    cntdtform.getForm().findField('address').setValue(itemval.address);
                    cntdtform.getForm().findField('contactType').setValue(record.get('type'));
                    cntdtform.getForm().findField('zipCode').setValue(itemval.zipCode);
                    cntdtform.getForm().findField('city').setValue(itemval.city);
                    cntdtform.getForm().findField('state').setValue(itemval.state);

                }
                else{
                    console.log("Get the Contact Detail Failure.");
                    Ext.Msg.alert('Get the Contact Detail failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                //cntcontpanel.setLoading(false);
                console.log("Get the Contact Detail Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Get the Contact Detail failed', obj.errorMessage);
            }
        });
    },

    setLoanSize: function(panel) {
        var me = this;
        var totalLoanSize = 0;
		
		var grid = panel.down('#loanpropertygridpanel');  
        var dealpropertyst = grid.getStore();
		
        var propertiescnt = dealpropertyst.getCount();
        if(propertiescnt>0){
            dealpropertyst.each(function(record) {
                totalLoanSize = totalLoanSize + record.get('estimatedloansize');
            }, this);
        }
        console.log("totalLoanSize"+totalLoanSize);
        if(panel){
			if (Ext.isEmpty(totalLoanSize))  {
			    panel.down('textfield[name=totalloan]').setRawValue(totalLoanSize);
			} else {
				panel.down('textfield[name=totalloan]').setRawValue(Ext.util.Format.currency(totalLoanSize));
			}
        }
    },

    loadMasterLoanDetailForm: function(grid, record, item, index, e, eOpts) {
        console.log("On Click of loan item, hide the grid & load the form with data");
        var me = this;
        me.getDealdetailmasterloanfieldset().down('gridpanel').hide();
        var arrels = me.getDealdetailmasterloanfieldset().query('panel[formel=yes]');
        console.log(arrels);
        for(var j=0;j<arrels.length;j++){
            arrels[j].show();
        }
        me.getDealdetailmasterloanfieldset().getForm().loadRecord(record);
        me.getDealdetailsformpanel().down('#masterloandetailclosebtn').show();
    },

    dealDetailMasterLoansCloseBtnClick: function(btn) {
        console.log("Close Deal Master Loan Detail Form");
        var me = this;
        btn.hide();
        var arrels = me.getDealdetailmasterloanfieldset().query('panel[formel=yes]');
        console.log(arrels);
        for(var j=0;j<arrels.length;j++){
            arrels[j].hide();
        }
        me.getDealdetailmasterloanfieldset().getForm().reset();
        me.getDealdetailmasterloanfieldset().down('gridpanel').show();
    },

    loadContactAllSelBuff: function(filterstarr,context) {
		var me = this;
		//var adddealcontactform = context.down('dealdetailsformpanel').down('adddealcontactform');
		var adddealcontactform = context.down('adddealcontactform');
		var allcntsst = adddealcontactform.down('#adddealcontactgrid').getStore();
		
        var allcntsstproxy = allcntsst.getProxy();
        /*allcntsstproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var allcntsstproxyCfg = allcntsstproxy.config;
        allcntsstproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		allcntsst.setProxy(allcntsstproxyCfg);
        allcntsst.load({
            params:{
                filter :filterstarr
            }
        });
    },

    getREOLURL: function() {
        var me = this,blockval,lotval,boroval,proprec;
		
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();		
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
		
		var propertyQuad = detailpanel.down('dealdetailpropertyfieldset');
		var dealproperty_st = propertyQuad.down('grid').getStore();
		var propertiescnt = dealproperty_st.getCount();
        if(propertiescnt > 0){
            if(propertiescnt===1){
                proprec   = dealproperty_st.getAt(0);
            } else {
                proprec = propertyQuad.getData();
                if(!proprec) {
                    proprec = propertyQuad.down('grid').getSelectionModel().getSelection()[0]
                }
                
                if(proprec) {
                } else{
                    Ext.Msg.alert('Failed', "Please select property from propertygrid.");
                    return false;
                }
            }
        }
		blockval  = proprec.get('block');
		lotval    = proprec.get('Lot');
		boroval   = proprec.get('Boro');
		me.openReonomyLink(boroval,blockval,lotval);
    },
	
	openReonomyLink: function(boroval,blockval,lotval){
		var me = this;
        
		var filterstarr = [];
        filterstarr.push("REOL_Boro = '"+boroval+"'");
        filterstarr.push("REOL_Lot = '"+lotval+"'");
        filterstarr.push("REOL_Block = '"+blockval+"'");
		
		Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            method:'GET',
            url:DM2.view.AppConstants.apiurl+'mssql:REOL_ReonomyLinks',
            scope:this,
            params: {
                filter : filterstarr
            },
            success: function(response, opts) {
                //propertyQuad.setLoading(false);
                console.log("Get the REOL URL.");
                var obj = Ext.decode(response.responseText);
                console.log(obj);
                if(response.status == 200)
                {
                    console.log(obj);
                    if(obj.length > 0){
                        var itemval = obj[0];
                        //console.log(itemval.REOL_URL);
                        window.open(itemval.REOL_URL);
                    } else {
                        Ext.Msg.alert('Info', "No data is present for this property.");
                    }
                } else {
                    console.log("Get the Contact Detail Failure.");
                    Ext.Msg.alert('Get the Contact Detail failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                //propertyQuad.setLoading(false);
                console.log("Get the Contact Detail Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Get the Contact Detail failed', obj.errorMessage);
            }
        });
	},

    defaultWorkPanelActivate: function(panel) {
        console.log("Defalut Work Area Panel Activated");
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
        panel.down('combo[name="activity"]').reset();
    },
	/*	
    showDealDetailOptionMenu: function(cmp,e, t, eOpts) {
        e.stopEvent();
        console.log("Show Menu for Deal Info.");
		var me = this;
        if(cmp.contextMenu){
			var context = me.getController('DealDetailController').getTabContext(cmp);
		    DM2.app.getController('DealDetailController').disableModifyMenu(context,cmp.contextMenu);
            cmp.contextMenu.showAt(e.getXY());
        }
    },*/
	
	showContextMenu: function(cmp,e){
		var me = this;
		e.preventDefault();
		e.stopEvent();
        if(cmp.contextMenu){
			var context = me.getController('DealDetailController').getTabContext(cmp);
		    DM2.app.getController('DealDetailController').disableModifyMenu(context,cmp.contextMenu);
            cmp.contextMenu.showAt(e.getXY());
        }		
	},

    onDealDetailOptionMenuClick: function(menu, item, e, eOpts) {
		if(item){
			console.log("Deal Detail Option Menu Clicked"+item.getItemId());
			if(item.getItemId() === "dealpermission"){
				this.getController('PermissionController').dealPermissionBtnClick();
			} else if(item.getItemId() === "modifydeal"){
				this.getController('DealModifyController').showDealModifyView();
			} else if(item.getItemId() === "refreshdeal"){
				this.refreshDealData();
			}
		}
    },
	
	refreshDealData: function() {
        var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();	
		if(context.xtype=="tab-sales-detail"){
			me.getController('SalesDealDetailController').refreshSaleDealDetailData(context);
		} else {
			me.getController('DealDetailController').refreshDealDetailData(context);
		}
    },
	
	refreshDealDetailData: function(context){
		var me = this;
		var dealdetailform = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var dealDetailStore = context.dealDetailStore;
		
		dealDetailStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		dealdetailform.setLoading(true);
		
		dealDetailStore.load({
			scope: this,
			params: {
				'filter' : 'idDeal = '+context.dealid
			},
			callback: function(records, operation, success) {
				dealdetailform.setLoading(false);
				console.log("Refresh SaleDealDetailData Callback.");
				console.log(records);
				if(success && records.length>0){
					var recitem = records[0].data;
					var tmprec = Ext.create("DM2.model.Deal",recitem);
					if(typeof recitem.permission.modifiable!= "undefined"){
						tmprec.set('modifiable',recitem.permission.modifiable);
					}
					if(typeof recitem.permission.readable!= "undefined"){
						tmprec.set('readable',recitem.permission.readable);
					}
					/*if(recitem.SaleList.length>0){
						tmprec.set('saleType',recitem.SaleList[0].saleType);
					}*/
					context.record = tmprec;
					console.log("Context New Rec");
					console.log(context.record);
					me.getController('DealController').afterRDealDetailsLoad(context);
					me.getController('DealController').loadAllDeals([]);
				}
			}
		});
	},
	
	dealDetailTabPanelAfterRender: function(tabpanel, eOpts){
		var me = this,tabItem;
		console.log("After Sales Detail Tab Panel Render");
		dealdetailtabpanel = tabpanel;
		
		tabItem = Ext.create('DM2.view.DealNotesGridPanel',{
					iconCls: 'notebtn'
				  });
		dealdetailtabpanel.add(tabItem);
		
		tabItem = Ext.create('DM2.view.DealDocsGridPanel',{
					iconCls: 'docbtn'/*,
					tabConfig: {
						hidden: true
					}*/
				  });
		dealdetailtabpanel.add(tabItem);
		/*
		dealdetailtabpanel.tabBar.insert(1, {
			 xtype:'splitbutton',
			 iconCls: 'docbtn',
			 text:'Documents',
			 cls:'docbtncls',
			 ui:'tabsplitbutton',
			 //itemId:'dealdocstab',
			 action:'dealdetaildocstab',
			 menu:{
				itemId: 'dealdocstabmenu',
				items:[{
					group: 'dealdtopts',
					checked : true,
					text:'Deal Only'
				},{
					group: 'dealdtopts',
					checked : false,
					text:'All'
				}]
			 }
		});
		*/
		tabItem = Ext.create('DM2.view.DealDetailQuotesGridPanel',{
					iconCls: 'loanbtn'
				  });
		dealdetailtabpanel.add(tabItem);
		
		tabItem = Ext.create('DM2.view.DealDetailUserGridPanel',{
					iconCls: 'userbtn',
					title: 'Users',
					tabConfig: {
						hidden: true
					}
				  });
		dealdetailtabpanel.add(tabItem);
		
		//Insert Users Tab
		var dealdetailuserstab = Ext.create('DM2.view.DealUserTab',{
									itemId:'dealdetailusertab',
									ui:'tabsplitbutton',
									style:'margin-right:5px;'
								});
        dealdetailtabpanel.tabBar.insert(3, dealdetailuserstab);
		
		tabItem = Ext.create('DM2.view.DealHistoryGrid',{
			iconCls: 'loanbtn',
			itemId : 'dealdetailshistorygrid',
			title: 'Deal History',
			tabConfig: {
				hidden: true
			}
	    });
		dealdetailtabpanel.add(tabItem);
		
		var dealdetailhistorytab = Ext.create('DM2.view.DealHistoryTab',{
									itemId:'dealdetailshistorytab',
									ui:'tabsplitbutton'
								});
		dealdetailtabpanel.tabBar.insert(4, dealdetailhistorytab);
		
		tabItem = Ext.create('DM2.view.DealDetailStatusHistoryPanel',{
					iconCls: 'dealbtn',
					title : 'Status History'
				  });
		dealdetailtabpanel.add(tabItem);
		dealdetailtabpanel.setActiveTab(0);	
	},
	
	dealDetailDocsTabClick: function(btn) {
		console.log("Set Document Tab");
		var me = this;
		var context = btn.up('tab-deal-detail');
		var dealdetailsformpanel = context.down('dealdetailsformpanel');
		//dealdetailsformpanel.down('#dealdetailtabpanel').setActiveTab(1);
		dealdetailsformpanel.down('tabpanel[region=south]').setActiveTab(1);
		me.loadDealDetailDocs(context,"deal");
	},
	
	loadDealDetailDocs: function(context,loadtype){
		var me = this;
		var record = context.record;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
		
		var dealdetaildocsgrid = context.down('dealdetailsformpanel').down('dealdocsgridpanel');		
        var documents_st = dealdetaildocsgrid.getStore();//('Documents').getProxy();
		var filterst = "";
		if(loadtype=="all"){
			documents_st.getProxy().setUrl(DM2.view.AppConstants.apiurl+"mssql:v_docsPerDealAll");
			filterst = filterst+'idDeal = '+dealid;
		}else{
			filterst = filterst+'dealid = '+dealid;
		}
		
        documents_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        documents_st.load({
            params:{
                filter :filterst
            },
            callback: function(records, operation, success){
                var detaildoctoolbar = dealdetaildocsgrid.down('toolbar');
                console.log("Doc Cnt"+documents_st.getCount());
                if(documents_st.getCount()>0){
                    if(detaildoctoolbar){
                        detaildoctoolbar.hide();
                    }
                }else{
                    if(detaildoctoolbar){
                        detaildoctoolbar.show();
						var tabItem = context;// panel.up('tab-deal-detail');
						var permission;
						if(tabItem){
							permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
							if(parseInt(permission)===0){						
								detaildoctoolbar.down('button').disable();
							}else{
								detaildoctoolbar.down('button').enable();
							}
						}
                    }
                }
				documents_st.getProxy().setUrl(DM2.view.AppConstants.apiurl+"mssql:v_docsPerDeal");
				context.dealDocsData = documents_st.data.items;
            }
        });
	},
	
	dealDocsTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Deal Docs Menu Clicked");
        var me = this;
        var text = item.text;
		menu.up('tabpanel').setActiveTab(1);		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
        if(text === "All"){
            //Show All Docs
			console.log("Show All Docs");
			me.loadDealDetailDocs(context,"all");
        }else if(text === "Deal Only"){
			console.log("Show Deal Only Docs");
			me.loadDealDetailDocs(context,"deal");		
        }
	},
	
	dealDetailTabChange: function(tabPanel, newCard, oldCard, eOpts){
	    
		console.log("Deal Detail Tab Change");
		var me = this;
		
        //var dealdocstab = tabPanel.down('splitbutton[action="dealdetaildocstab"]'); //tabPanel.down('dealdocsgridpanel');
        var dealdetailshistorytab = tabPanel.down('dealhistorytab');
		var dealusertab = tabPanel.down('dealusertab');
        
        /*if(newCard.xtype == "dealdocsgridpanel" ) {
            dealdocstab.addCls("docactivebtncls");
            dealdetailshistorytab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
        } else*/ if(newCard.xtype =="dealhistorygrid") {
            dealdetailshistorytab.addCls("docactivebtncls");
            //dealdocstab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
        } else if(newCard.xtype =="dealdetailusergridpanel") {
            dealdetailshistorytab.removeCls("docactivebtncls");
            //dealdocstab.removeCls("docactivebtncls");
			dealusertab.addCls("docactivebtncls");
        } else {
            //dealdocstab.removeCls("docactivebtncls");
            dealdetailshistorytab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
        }		
		
	},
	dealDetailsHistoryTabClick: function(tabItem) {
		console.log("Set Deal History Tab");
		var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var dealhistorygrid = context.down('dealhistorygrid');
		
		tabItem.up('tabpanel').setActiveTab(dealhistorygrid);
		me.getController('DealHistoryController').onCategoryChange("Addressess", tabItem.down('menu'),"deal");
	},
	
	dealDetailsHistoryTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Deal Docs Menu Clicked");
        var me = this;
        var text = item.text;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var dealhistorygrid = context.down('dealhistorygrid');
		
		menu.up('tabpanel').setActiveTab(dealhistorygrid);
		me.getController('DealHistoryController').onCategoryChange(text, menu, "deal");
	},
	
	disableModifyMenu: function(tabItem,menu){
		var me = this;
		var menuitems = menu.items.items;
		console.log(menuitems);
		//var obj = Ext.decode(tabItem.dealDetailStore.data.items[0].data.permission);
		//console.log(obj);
		if(tabItem.dealDetailStore.data.items.length>0){
			var permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
			console.log("Permission : "+permission);
			//permission = 0;
			if(parseInt(permission)===0){						
				for(var i=0;i<menuitems.length;i++){
					if(menuitems[i].modifyMenu === "yes"){
						menuitems[i].disable();
					}
				}
			} else {
				for(var i=0;i<menuitems.length;i++){
					if(menuitems[i].modifyMenu === "yes"){
						menuitems[i].enable();
					}
				}
			}
		}
	},
	
	dealDetailUsersTabClick: function(tabItem){
		console.log("Deal Detail Users Tab Click");
		var me = this;
		//tabItem.up('tabpanel').setActiveTab(4);
		// var dealmenugridpanel = me.getDealmenugridpanel();
		var p = tabItem.up('tab-deal-detail');
		var dealid = p.dealid;

		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var usergrid = context.down('dealdetailusergridpanel');
		tabItem.up('tabpanel').setActiveTab(usergrid);
		
		me.getController('DealUserController').loadDealUserDetailsWithParams('active',dealid,usergrid);
		usergrid.config.alloractiveusermode = "active";		
	},
	
	dealDetailUsersTabMenuClick: function(menu, item, e, eOpts){
		console.log("Deal Detail Users Tab Menu Click");
		var me = this;
		var text = item.text;
		
		var p = menu.up('tab-deal-detail');
		var dealid = p.dealid;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var usergrid = context.down('dealdetailusergridpanel');
		
		menu.up('tabpanel').setActiveTab(usergrid);
		
        if(text === "All"){
			console.log("Show All Users");
			me.getController('DealUserController').loadDealUserDetailsWithParams('all',dealid,usergrid);
			usergrid.config.alloractiveusermode = "all";
        }else if(text === "Active Only"){
			console.log("Show Active Users Only");
			me.getController('DealUserController').loadDealUserDetailsWithParams('active',dealid,usergrid);
			usergrid.config.alloractiveusermode = "active";
        }
	},
	
	dealDetailNewLoanOptionItemClick: function(item, e, eOpts) {
        console.log("Deal Detail New Loan Menu Item Clicked.");
		var me = this;
        if(item.action==="editloan"){
            console.log("Loan Edit Item Clicked");
			var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
			var context = tabdealpanel.getActiveTab();

       		var dealnewloan = context.down('dealdetailsformpanel').down('dealdetailnewloanfieldset');
			var contextMenu = dealnewloan.contextMenu;
			var row = contextMenu.getRec();
			
			// when the context menu is invoked from a form, there won't be any data set to the context menu
			if(!row) {
				row = dealnewloan.getData();
			} else {
				dealnewloan.setData(row);
			}
			contextMenu.clearRec();
			var title = "Loan Editor";
			me.getController('QuoteController').callLoanEditor(row);
        }
    },
	
	dealDetailExistingLoanOptionItemClick: function(item, e, eOpts) {
        console.log("Deal Detail Existing Loan Menu Item Clicked.");
		var me = this;
        if(item.action==="editloan"){
            console.log("Loan Edit Item Clicked");
			var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
			var context = tabdealpanel.getActiveTab();

       		var dealexistingloan = context.down('dealdetailsformpanel').down('dealdetailexistingloanfieldset');
			var contextMenu = dealexistingloan.contextMenu;
			var row = contextMenu.getRec();
			
			// when the context menu is invoked from a form, there won't be any data set to the context menu
			if(!row) {
				row = dealexistingloan.getData();
			} else {
				dealexistingloan.setData(row);
			}
			contextMenu.clearRec();
			var title = "Loan Editor";
			me.getController('QuoteController').callLoanEditor(row);
        }
    },
	
	changePrimaryPropertyMenuItemClick: function(){
		var me = this,adddealpropertyform;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('adddealpropertyform')){
			console.log("Use Old Panel");
			adddealpropertyform = detailpanel.down('adddealpropertyform');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	adddealpropertyform = Ext.create('DM2.view.AddDealPropertyForm');
		}
        eastregion.getLayout().setActiveItem(adddealpropertyform);
		adddealpropertyform.setTitle("Change Primary");
		adddealpropertyform.config.viewMode = "changeprimary";
	},
	
	changePrimaryPropertySave: function(context,propertyid,dealid){
		var me = this;
		var data = [{
			'dealid': dealid,
			'propertyid' : propertyid
		}];
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'changePrimaryPropertyPerDeal',
			params: Ext.util.JSON.encode(data),
			scope:this,
			success: function(response, opts) {
				console.log("Change Primary Property to deal.");
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 201 || obj.statusCode == 200){
					var dealdetailpropertyfieldset = context.down('dealdetailsformpanel').down('dealdetailpropertyfieldset');
					me.loadPropertyDetails(dealdetailpropertyfieldset);
					me.addDealPropertyCloseBtnClick(context.down('dealdetailsformpanel').down('adddealpropertyform'));
					dealdetailpropertyfieldset.switchPanel(0);
				} else {
					console.log("Change Primary Property to Deal Failure.");
					Ext.Msg.alert('Property Change failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				console.log("Change Primary Property to Deal Failure.");
				var obj = Ext.decode(response.responseText);
				var errMsg = obj.errorMessage;
				var errText = "";
				var n = errMsg.indexOf("Violation of UNIQUE KEY constraint");
				if(n==-1){
					errText = obj.errorMessage;
					//Ext.Msg.alert('Property add failed', obj.errorMessage);
				} else {
					errText = "Property already exist. Please try to add another property.";
					//Ext.Msg.alert('Property add failed', "Property already exist.Please try to add another property.");
				}
				var title = "Property Change failed";
				me.showToast(errText,title,'error',null,'tr',true);
			}
		});
	},
	
	showHideDealDetailUpperSectionFields: function(panel){
		var me = this;
		var infofrm = panel.getForm();
		if(panel.up('tab-sales-detail')){
			infofrm.findField('magicDealID').hide();
			infofrm.findField('filenumber').hide();
			infofrm.findField('statusSetMode').hide();
			infofrm.findField('statusSetBy').hide();
			infofrm.findField('mtgtype').hide();
			infofrm.findField('saleType').show();
			//infofrm.findField('dealname').setLableWidth(65);
			panel.down('#dealidnamepanel').add({
				xtype: 'textfield',
				flex: 1,
				fieldLabel: 'Name',
				labelAlign: 'right',
				labelWidth: 65,
				name: 'dealname',
				readOnly: true							   
			});
		} else {
			infofrm.findField('magicDealID').show();
			infofrm.findField('filenumber').show();
			infofrm.findField('statusSetMode').show();
			infofrm.findField('statusSetBy').show();
			infofrm.findField('mtgtype').show();
			infofrm.findField('saleType').hide();
			//infofrm.findField('dealname').setLableWidth(45);
			panel.down('#dealidnamepanel').add({
				xtype: 'textfield',
				flex: 1,
				fieldLabel: 'Name',
				labelAlign: 'right',
				labelWidth: 45,
				name: 'dealname',
				readOnly: true							   
			});
		}
	},
	
	afterDealDetailPropertyFieldsetRender: function(panel){
		var me = this;
		var detailLayout = panel.down('form[itemId="detailLayout"]');
        var form = detailLayout.getForm();
		if(panel.up('tab-sales-detail')){
			form.findField('primaryProperty').hide();
			form.findField('mortgageType').hide();
		} else {
			form.findField('primaryProperty').show();
			form.findField('mortgageType').show();
		}
	},
	
	createNewPropertyBtnClick: function(btn){
        console.log("Show Add New Property View");
        var me = this,addnewmasterpropertyview;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = btn.up('[region=east]');	
		if(eastregion.down('addnewmasterpropertyview')){
			console.log("Use Old Panel");
			addnewmasterpropertyview = eastregion.down('addnewmasterpropertyview');
		} else {
			console.log("Create New Panel");
        	addnewmasterpropertyview = Ext.create('DM2.view.AddNewMasterPropertyView');
		}
		eastregion.getLayout().setActiveItem(addnewmasterpropertyview);		
        addnewmasterpropertyview.setTitle("Create New Property");		
		//eastregion.setTitle("Create New Property");
		eastregion.config.backview = "addnewmasterpropertyview";
		addnewmasterpropertyview.config.backview = "adddealxpropertyview";
	},
	
	createNewContactBtnClick: function(btn){
		console.log("Show Add New Contact View");
        var me = this,contactdetailsform;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = btn.up('[region=east]');	
		if(eastregion.down('contactdetailsform')){
			console.log("Use Old Panel");
			contactdetailsform = eastregion.down('contactdetailsform');
		} else {
			console.log("Create New Panel");
        	contactdetailsform = Ext.create('DM2.view.ContactDetailsForm');
		}
		eastregion.getLayout().setActiveItem(contactdetailsform);		
        contactdetailsform.setTitle("Create New Contact");		
		//eastregion.setTitle("Create New Property");
		eastregion.config.backview = "contactdetailsform";
		var adddealcontactform = btn.up('adddealcontactform');
		contactdetailsform.config.backview = "adddealxcontactview";
		contactdetailsform.config.backfrm = adddealcontactform;
		
		contactdetailsform.config.originbackview = adddealcontactform.config.backview;
		contactdetailsform.config.originbackfrm = adddealcontactform.config.backfrm;
		me.getController('ContactController').setContactDetailsFormToAdd(contactdetailsform);
	},
	
	editPropertyMenuItemClick: function(){
		var me = this,editdealpropertyform;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('editdealpropertyform')){
			console.log("Use Old Panel");
			editdealpropertyform = detailpanel.down('editdealpropertyform');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	editdealpropertyform = Ext.create('DM2.view.EditDealPropertyForm');
		}
        eastregion.getLayout().setActiveItem(editdealpropertyform);
		
		var dealproperty = detailpanel.down('dealdetailpropertyfieldset');
			
        var contextMenu = dealproperty.contextMenu;
        var row = contextMenu.getRec();        
        // when the context menu is invoked from a form, there won't be any data set to the context menu
        if(!row) {
            row = dealproperty.getData();
        } else {
            dealproperty.setData(row);
        }
		var mtgtypeSt = editdealpropertyform.down('combobox[name="mortgageType"]').getStore();		
		var selectionList_st = Ext.getStore('SelectionList');		
		if(selectionList_st.isLoaded()){
			mtgtypeSt.loadData(selectionList_st.data.items);
			mtgtypeSt.filter("selectionType","MT");
			mtgtypeSt.each( function(record){
				if(record.get('isDefault')){
					editdealpropertyform.down('combobox[name="mortgageType"]').setRawValue(record.get('selectionDesc'));
				}
			});
		}
		editdealpropertyform.getForm().loadRecord(row);
	},
	
	editDealPropertyFormCloseBtnClick: function(btn){
		console.log("Close Edit Master Data Form");
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
	},
	
	saveDealPropertyBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);			
		var record = context.record;		
		var dealid = record.data.idDeal;
		
		var editdealpropertyform = detailpanel.down('editdealpropertyform');
		var data = {
			'idDealxProp' : editdealpropertyform.getForm().findField('idSetup').getValue(),
			'mortgageType' : editdealpropertyform.getForm().findField('mortgageType').getValue(),
			'@metadata': { 'checksum': 'override' }
		};
		editdealpropertyform.setLoading(true);
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DM2.view.AppConstants.apiurl+'mssql:DealxProp',
			method:'PUT',
			params: Ext.util.JSON.encode(data),
			scope:this,
			success: function(response, opts) {
				console.log("Edit Property to deal is submitted.");
				editdealpropertyform.setLoading(false);
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 200){
					var dealdetailpropertyfieldset = detailpanel.down('dealdetailpropertyfieldset');
					me.loadPropertyDetails(dealdetailpropertyfieldset);
					me.editDealPropertyFormCloseBtnClick(editdealpropertyform);
					dealdetailpropertyfieldset.switchPanel(0);
				} else {
					console.log("Add Property to Deal Failure.");
					Ext.Msg.alert('Property add failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				console.log("Add Property to Deal Failure.");
				editdealpropertyform.setLoading(false);
				var obj = Ext.decode(response.responseText);
				var errMsg = obj.errorMessage;
				var errText = "";
				var n = errMsg.indexOf("Violation of UNIQUE KEY constraint");
				if(n==-1){
					errText = obj.errorMessage;
					//Ext.Msg.alert('Property add failed', obj.errorMessage);
				} else {
					errText = "Property already exist. Please try to add another property.";
					//Ext.Msg.alert('Property add failed', "Property already exist.Please try to add another property.");
				}
				var title = "Property add failed";
				me.showToast(errText,title,'error',null,'tr',true);
			}
		});
	}
});