Ext.define('DM2.controller.AddRentRollExpenseController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({

        });
    },

    AddRentRollExpenseDocDClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this;
        var classification = '';
		tabdealdetail.config.activityid = activityid;
        if(activitytext==="Receive Rent Roll"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('RentRoll',tabdealdetail);
        } else if(activitytext==="Receive Expenses Document"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('Expenses',tabdealdetail);
        }
    },

    closeRentRollExpense: function(tabdealdetail) {
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(tabdealdetail.down('defaultworkareapanel'));
    },

    afterRentRollExpenseDocUpload: function(context) {
        var me = this;
        ///Post the Activity History
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
    }
});