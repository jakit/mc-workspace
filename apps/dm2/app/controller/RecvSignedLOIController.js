Ext.define('DM2.controller.RecvSignedLOIController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({
        });
    },

    RecvSignLOIClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this;
		if(activitytext==="Received Signed LOI"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvSignLOI',tabdealdetail);
        } else if(activitytext==="Receive Signed Brokerage Contract"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('RxSiBrkCnt',tabdealdetail);
        } else if(activitytext==="Receive Signed Brokerage Contract To Seller"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('RxSiBrCtTS',tabdealdetail);
        } else if(activitytext==="Receive Signed Brokerage Contract To Buyer"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('RxSiBrCtTB',tabdealdetail);
        } else if(activitytext==="Receive Finalized Contract"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('RxFinCntr',tabdealdetail);
        } else if(activitytext==="Create Offering Memorandum"){
            me.getController('GeneralDocSelController').showGeneralDocSelPanel('createofferingmemorandum',tabdealdetail);
        }
    },

    closeRecvSignedLOI: function(context) {
        console.log("Close Recv Sign LOI");
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },
	
	afterRecvSignedLOIDocUpload: function(context) {
        console.log("After Recv Sign LOI Doc Upload");
		var me = this;
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
    }
});