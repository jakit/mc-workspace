Ext.define('DM2.controller.ReiterateController', {
    extend: 'Ext.app.Controller',

    ReiterateClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this;
        var senddoccntrl = me.getController('SendDocumentController');
        senddoccntrl.showSendDocumentView("reiterate",tabdealdetail,activitytext);
    }
});