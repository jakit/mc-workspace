Ext.define('DM2.controller.ApprovalController', {
    extend: 'Ext.app.Controller',
	
    refs: {
        approvalpanel: {
            autoCreate: true,
            selector: 'approvalpanel',
            xtype: 'approvalpanel'
        },
        approvaladddealdocpanel: {
            autoCreate: true,
            selector: 'approvaladddealdocpanel',
            xtype: 'approvaladddealdocpanel'
        }
    },

    init: function(application) {
        this.control({
            'approvalpanel':{
                'close': this.approvalPanelClose
            },
            'approvalpanel #approvalloangridpanel':{
                'select': this.approvalLoanItemSelect
            },
            '#approvalloangridpanel #rxcommitcolumn':{
                beforecheckchange: this.onRxCommitColumnItemClick
            },
            '#approvalloangridpanel #sendmodcolumn':{
                itemClick: this.onSendModColumnItemClick
            },
            '#approvalgridpanel #receivedmodback':{
                beforecheckchange: this.onRecvModColumnItemClick
            },
            'approvalpanel #savecommitmentbtn':{
                'click': this.saveCommitmentBtnClick
            },
            'approvalpanel #resetcommitmentbtn':{
                'click': this.resetCommitmentBtnClick
            },
			'approvalpanel button[action="doneActionBtn"]': {
                click: 'onApprovalDoneBtnClick'
            }
        });
    },
	
	 ApprovalClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this;

        me.showApprovalView(tabdealdetail);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
        var approvalpanel = detailpanel.down('approvalpanel');

        approvalpanel.setTitle("Activities > "+activitytext);
        //approvalpanel.down('#activityid').setValue(activityid);
		
		var record = tabdealdetail.record;
		var dealid = record.data.idDeal;
		
        //Clear Old Mod Docs & Commitment Form
        approvalpanel.down('#commitmentfrm').getForm().reset();
		
        var apprdocs = approvalpanel.down('approvalgridpanel').getStore();//('ApprovalDocs');
        apprdocs.removeAll();
        //Load deal Selected Loans
        me.loadDealSelectedLoans(dealid,tabdealdetail);
    },

    approvalPanelClose: function(panel) {
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    approvalLoanItemSelect: function(selModel, record, index, eOpts) {
        console.log("Loan Item Select From Grid.");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(selModel.view.ownerCt);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
		var approvalpanel = detailpanel.down('approvalpanel');
        approvalpanel.down('#commitmentfrm').getForm().loadRecord(record);

        me.loadApproval(record,context);
        approvalpanel.down('#loandetailform').getForm().loadRecord(record);
    },

    loadApproval: function(rec,context) {
        var me = this;
        var bank = rec.get('bank');
        var amt = rec.get('receivedamount');

		var record = context.record;
        var dealid = record.data.idDeal;
		
        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
        //filterstarr.push("(clsfctn like '%send.modification."+bank+"%')");
		filterstarr.push("(clsfctn like '%"+bank+".s.m%')");
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);	
		var approvalpanel = detailpanel.down('approvalpanel');//me.getApprovalpanel();
		
        var approvaldocs_st = approvalpanel.down('approvalgridpanel').getStore();//('ApprovalDocs');
        approvaldocs_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        approvaldocs_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                console.log(success);
				me.checkRecvModReply(context);
            }
        });
    },

    onRxCommitColumnItemClick: function(chkcolumn,rowIndex, checked, eOpts) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(chkcolumn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var approvalpanel = detailpanel.down('approvalpanel');
        var aprgrid = approvalpanel.down('#approvalloangridpanel');
        var rec = aprgrid.getStore().getAt(rowIndex);

        approvalpanel.config.approvalRec = rec;
        //approvalpanel.config.clsf = "recv.commitment";
		approvalpanel.config.clsf = "r.c";

        me.getController('GeneralDocSelController').showGeneralDocSelPanel('approval',context);
        return false;
    },

    onSendModColumnItemClick: function(view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(view);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var approvalpanel = detailpanel.down('approvalpanel');//me.getApprovalpanel();
        var aprgrid = approvalpanel.down('#approvalloangridpanel');
		var moddocgrid = approvalpanel.down('#approvalgridpanel');
		var moddoccnt = moddocgrid.getStore().getCount()+1;
        var rec = aprgrid.getStore().getAt(rowIndex);

        approvalpanel.config.approvalRec = rec;
        approvalpanel.config.clsf = "s.m"+moddoccnt;

        var senddoccntrl = me.getController('SendDocumentController');
        senddoccntrl.showSendDocumentView("sendmodification",context,"Approval > Send Mod");
    },

    showApprovalView: function(tabdealdetail) {
        var me = this,approvalpanel;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('approvalpanel')){
			console.log("Use Old Panel");
			approvalpanel = detailpanel.down('approvalpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	approvalpanel = Ext.create('DM2.view.ApprovalPanel');
		}
        eastregion.getLayout().setActiveItem(approvalpanel);
	
        if(approvalpanel.config.approvalRec){
            var tmprec = approvalpanel.config.approvalRec;
            approvalpanel.down('#approvalloangridpanel').getSelectionModel().select(tmprec);
        }
		//approvalpanel.down('#commitmentfrm').getForm().findField('receivedamount').focus();
    },

    onRecvModColumnItemClick: function(chkcolumn,rowIndex, checked, eOpts) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(chkcolumn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
		var approvalpanel = detailpanel.down('approvalpanel');//me.getApprovalpanel();
        var aprloangrid = approvalpanel.down('#approvalloangridpanel');
		var rec = aprloangrid.getSelectionModel().getSelection()[0];

        var moddocgrid = approvalpanel.down('#approvalgridpanel');
        var modrec = moddocgrid.getStore().getAt(rowIndex);
		var clsf = modrec.get('clsfctn');
		var recvModRepCnt = clsf.charAt(clsf.length - 1);
		
        approvalpanel.config.approvalRec = rec;
        approvalpanel.config.clsf = "r.m"+recvModRepCnt;

        me.getController('GeneralDocSelController').showGeneralDocSelPanel('approval',context);
		return false;
    },

    loadDealSelectedLoans: function(dealid,context) {
        var me = this;
		var approvalloangrid = context.down('approvalpanel').down('#approvalloangridpanel');
        var aprquoteselected_st = approvalloangrid.getStore();//Ext.getStore('DealQuotesSelected');
	
        aprquoteselected_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });

        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
        filterstarr.push("selected='S'");

        aprquoteselected_st.load({
            params:{
                filter : filterstarr
            },
            callback: function(records, operation, success){
                var quotecnt = aprquoteselected_st.getCount();
                console.log("Quotes Cnt"+quotecnt);

                if(quotecnt > 0){
                    console.log("select rec");
                    approvalloangrid.getSelectionModel().select(0);
					me.loadRecvCommitment(dealid,context);
                }
            }
        });
    },
	
	loadRecvCommitment: function(dealid,context){
		var me = this;
		var approvalloangrid = context.down('approvalpanel').down('#approvalloangridpanel');
        var aprquoteselected_st = approvalloangrid.getStore();//Ext.getStore('DealQuotesSelected');
	
        aprquoteselected_st.each(function(record) {
			me.checkRecvCommit(record,dealid,context);
		}, this);
	},
	
	checkRecvCommit: function(record,dealid,context){
		var me = this;
		var bank = record.get('bank');
		var approvalloangrid = context.down('approvalpanel').down('#approvalloangridpanel');
		
        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
        //filterstarr.push("(clsfctn like '%recv.commitment."+bank+"%')");
		filterstarr.push("(clsfctn like '%"+bank+".r.c%')");
		
		Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            method:'GET',
            url:DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal',
            scope:this,
            params: {
                filter : filterstarr
            },
            success: function(response, opts) {                
                var obj = Ext.decode(response.responseText);
                if(response.status == 200) {
                    console.log(obj);
                    if(obj.length > 0){
						console.log("Set Recv Checkbox Active");
                        record.set('hasCommit',1);
						record.commit();
						approvalloangrid.getView().refresh();
                    } else {
                        //Ext.Msg.alert('Info', "No data is present for this property.");
						console.log("No commitment Doc is present for this bank.");
                    }
                } else {
                    console.log("Get the Commitement Detail Failure.");
                    //Ext.Msg.alert('Get the Commitement Detail failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Get the Contact Detail Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Get the Contact Detail failed', obj.errorMessage);
            }
        });
	},
	
    saveCommitmentBtnClick: function(btn) {
        console.log("Save Commitment");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
		var approvalpanel = detailpanel.down('approvalpanel');
		
        var aprloangrid = approvalpanel.down('#approvalloangridpanel');
        if (aprloangrid.getSelectionModel().hasSelection()) {
            var row = aprloangrid.getSelectionModel().getSelection()[0];
            var loanid = row.get('loanid');
        } else {
            Ext.Msg.alert('Failed', "Please select loan.");
            return false;
        }

        var commitmentfrm = approvalpanel.down("#commitmentfrm");
        var commitmentCheckAmount = commitmentfrm.getForm().findField('commitmentCheckAmount').getValue();
        var commitmentNumber = commitmentfrm.getForm().findField('commitmentNumber').getValue();
        var commitmentCheck = commitmentfrm.getForm().findField('commitmentCheck').getValue();
        var commitExpirationDate = commitmentfrm.getForm().findField('commitExpirationDate').getValue();
        var commitmentDate = commitmentfrm.getForm().findField('commitmentDate').getValue();
		var commitmentCheckDate = commitmentfrm.getForm().findField('commitmentCheckDate').getValue();

        var data = [{
            'idLoan': loanid,
            'commitmentCheckAmount' : commitmentCheckAmount,
            'commitmentNumber' : commitmentNumber,
            'commitmentCheck' : commitmentCheck,
            'commitExpirationDate' : commitExpirationDate,
            'commitmentDate' : commitmentDate,
			'commitmentCheckDate' : commitmentCheckDate,
			"@metadata" : {'checksum' : 'override'}
        }];
        commitmentfrm.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
			method:'PUT',
            url:DM2.view.AppConstants.apiurl+'mssql:Loan',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Commitment to loan is saved.");
                commitmentfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
                    aprloangrid.getSelectionModel().select(row);
                    commitmentfrm.getForm().reset();
                } else {
                    Ext.Msg.alert('Commitment add failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                commitmentfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Commitment add failed', obj.errorMessage);
            }
        });
    },
	
	checkRecvModReply: function(context){
		var me = this;
		console.log("Check for Recv Modification reply");
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var moddocgrid = context.down('approvalpanel').down('#approvalgridpanel');
        var moddocgrid_st = moddocgrid.getStore();
		
		var approvalloangrid = context.down('approvalpanel').down('#approvalloangridpanel');
		var loanrec = approvalloangrid.getSelectionModel().getSelection()[0];
		var bank = loanrec.get('bank');
		
		var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
		filterstarr.push("(clsfctn like '%"+bank+".r.m%')");
		
		Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            method:'GET',
            url:DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal',
            scope:this,
            params: {
                filter : filterstarr
            },
            success: function(response, opts) {                
                var recvrepobj = Ext.decode(response.responseText);
                if(response.status == 200) {
                    console.log(recvrepobj);
                    if(recvrepobj.length > 0){
						moddocgrid_st.each(function(moddocrecord) {
							var clsfn = moddocrecord.get('clsfctn');
							var newclsfn = clsfn.replace(".s.m", ".r.m");
							for(var j=0; j<recvrepobj.length; j++){
								//var tmprec = recvrepobj[j];
								if(recvrepobj[j].clsfctn==newclsfn){
									//If FInds, checkmark Recv Modification Reply Back Checkbox in grid for record
									moddocrecord.set('receivedBack',1);
									moddocrecord.commit();
									moddocgrid.getView().refresh();
									break;
								}
							}
						}, this);
                    } else {
                        //Ext.Msg.alert('Info', "No data is present for this property.");
						console.log("No commitment Doc is present for this bank.");
                    }
                } else {
                    console.log("Get the Modification reply Detail Failure.");
                    //Ext.Msg.alert('Get the Commitement Detail failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Get the Contact Detail Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Get the Contact Detail failed', obj.errorMessage);
            }
        });		
	},
	
	onApprovalDoneBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	}
});