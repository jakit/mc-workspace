Ext.define('DM2.controller.PermissionController', {
    extend: 'Ext.app.Controller',

    refs: {
        permissionview: {
            autoCreate: true,
            selector: 'permissionview',
            xtype: 'permissionview'
        }
    },

    init: function(application) {
        this.control({
            'permissionview':{
                close: this.permissionViewClose
            },
            'permissionview #addpermission':{
                click: this.addPermissionBtnClick
            },
            '#permissionallgridpanel #savepermission':{
                click: this.savePermissionBtnClick
            }
        });
    },

    dealPermissionBtnClick: function() {
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var acttabdealdetail = tabdealpanel.getActiveTab();
		
		var eastregion = acttabdealdetail.down('dealdetailsformpanel').down('[region=east]');
		var permissionview;
		if(acttabdealdetail.down('dealdetailsformpanel').down('permissionview')){
			console.log("Use Old Panel");
			permissionview = acttabdealdetail.down('dealdetailsformpanel').down('permissionview');
		}
		else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	permissionview = Ext.create('DM2.view.PermissionView');//me.getPermissionview();
		}
        eastregion.getLayout().setActiveItem(permissionview);
		
		var record = acttabdealdetail.record;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
		
        permissionview.setTitle("Permission > Deal > "+dealid);
        //permissionview.down('#activityid').setValue(activityid);

        var filterstarr = [];
        if(dealid!==''){
            var filterst = "objectid = '"+dealid+"'";
            filterstarr.push(filterst);
        }
        var filterst = "object = 'deal'";
        filterstarr.push(filterst);

        me.loadSelectedPermission(filterstarr,acttabdealdetail);
    },

    permissionViewClose: function(panel) {		
		var me = this;
		var context = panel.up('tab-deal-detail');
        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    loadSelectedPermission: function(filterstarr,context) {
        var me = this;
		var selpermission_st = context.down('dealdetailsformpanel').down('permissionview').down('permissionselectedgridpanel').getStore();
        selpermission_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        selpermission_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
            }
        });
    },

    loadAllPermission: function(filterstarr,context) {
        var me = this;
		var allpermission_st = context.down('dealdetailsformpanel').down('permissionview').down('permissionallgridpanel').getStore();
        allpermission_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        allpermission_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
            }
        });
    },

    addPermissionBtnClick: function(btn) {
        console.log("Add Permission");
        var me = this;
		var context = btn.up('tab-deal-detail');
        var permissionview = context.down('dealdetailsformpanel').down('permissionview');
		
        if(!permissionview.down('permissionallgridpanel')){
            var perallgrid = Ext.create('DM2.view.PermissionAllGridPanel');
            permissionview.add(perallgrid);
        }
        permissionview.down('permissionallgridpanel').show();

        var filterstarr = [];

        //var filterst = "object = 'deal'";
        //filterstarr.push(filterst);

        me.loadAllPermission(filterstarr,context);
    },

    savePermissionBtnClick: function(btn) {
        console.log("Save Permission");
        var me = this;
		var context = btn.up('tab-deal-detail');
        var perallgrid = context.down('dealdetailsformpanel').down('permissionview').down('permissionallgridpanel');
        var selarr = perallgrid.getSelectionModel().getSelection();

		var record = context.record;
        var dealid = record.data.idDeal;
		
        if(selarr.length>0){
            var data = [];
            for(var i=0;i<selarr.length;i++){
                var perrec = {
                    objectType:1,
                    objectID:dealid,
                    //entityid:selarr[i].entityid,
                    entityname:selarr[i].entityname
                };
                data.push(perrec);
            }
            perallgrid.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:ACL',
                params: Ext.util.JSON.encode(data),
                scope:this,
                success: function(response, opts) {
                    console.log("Save Permission is done.");
                    perallgrid.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(obj.statusCode == 201)
                    {
                        var filterstarr = [];
                        var filterst;
                        if(dealid!==''){
                            filterst = "objectid = '"+dealid+"'";
                            filterstarr.push(filterst);
                        }
                        filterst = "object = 'deal'";
                        filterstarr.push(filterst);
                        me.loadSelectedPermission(filterstarr,context);
                        perallgrid.hide();
                    }
                    else{
                        console.log("Add Contact to Bank Failure.");
                        Ext.Msg.alert('Contact add failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    console.log("Save Permission Failure.");
                    perallgrid.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Warning', obj.errorMessage);
                }
            });
        }else{
            Ext.Msg.alert("Warning","Please select permission.");
        }
    }

});
