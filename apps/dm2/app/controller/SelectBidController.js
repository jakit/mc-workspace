Ext.define('DM2.controller.SelectBidController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.SelectBidPanel'],
    refs: {
        selectbidpanel: {
            autoCreate: true,
            selector: 'selectbidpanel',
            xtype: 'selectbidpanel'
        }
    },

    init: function(application) {
        this.control({
            'selectbidpanel':{
                close: this.selectBidCloseClick,
				afterrender: 'loadBids'
            },
			'selectbidpanel checkcolumn': {
				beforecheckchange : this.selUnSelBid
			},
			'selectbidpanel button[action="doneActionBtn"]': {
				click : this.selectBidDoneBtnClick
			}
        });
    },
	
	SelectBidClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this,selectbidpanel;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('selectbidpanel')){
			console.log("Use Old Panel");
			selectbidpanel = detailpanel.down('selectbidpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	selectbidpanel = Ext.create('DM2.view.SelectBidPanel');
		}
        eastregion.getLayout().setActiveItem(selectbidpanel);
		selectbidpanel.setTitle("Activities > "+activitytext);
		selectbidpanel.config.activityid = activityid;
    },
	
	loadBids: function(panel){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var SaleList = context.dealDetailStore.data.items[0].data.SaleList;
		if(SaleList.length>0){
			var idSale = SaleList[0].idSale;
			
			var salesbid_st = panel.getStore();
			salesbid_st.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			salesbid_st.load({
				params:{
					filter :'idSale = '+idSale
				},
				callback: function(records, operation, success){
				}
			});
		}
	},
	
    selectBidCloseClick: function(panel) {
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },
	
	selUnSelBid: function(checkcolumn , rowIndex , checked , record , eOpts){
		var me = this;
		var selectbidpanel = checkcolumn.up('selectbidpanel');
		var record = selectbidpanel.getStore().getAt(rowIndex);
		console.log(record);
		me.getController('SalesDealDetailController').selectSaleBid(checked,record,selectbidpanel);
	},
	
	onClosingViewDoneBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);	
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	},
	
	selectBidDoneBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	}
});