Ext.define('DM2.controller.NoteController', {
    extend: 'Ext.app.Controller',

    refs: {
        dealnotesgridpanel: 'dealnotesgridpanel',
        noteoptionmenu: {
            autoCreate: true,
            selector: 'noteoptionmenu',
            xtype: 'noteoptionmenu'
        },
        notegridtoolbar: '#notegridtoolbar',
        notepanel: {
            autoCreate: true,
            selector: 'notepanel',
            xtype: 'notepanel'
        }
    },

    init: function(application) {
        this.control({
			'dealnotesgridpanel': {
                 rowcontextmenu: this.notegridrightclick
             },
			 'dealnotesgridpanel dataview': {
                 itemlongpress: this.onNotesGridItemLongPress
             },
             'dealnotesgridpanel #addnotebtn': {
                 click: {
                     fn: this.addNoteBtnClick
                 }
             },            
            'notepanel': {
                 close: this.notePanelClose
             },
            'notepanel #notesubmitbtn': {
                 click: this.submitNoteBtnClick
             },
            'notepanel #noteresetbtn': {
                 click: this.resetNoteBtnClick
             },
            'noteoptionmenu menuitem':{
                click: this.noteoptionitemclick
            }
        });
    },
	
	bindDealNotes: function(tabItem){
		console.log("Bind Note Details");
		var me = this,panel;
		panel = tabItem.down('dealnotesgridpanel');
		var dealnotesst = panel.getStore();
		dealnotesst.loadData(tabItem.dealDetailStore.data.items[0].data.NotePerDeal);
		me.afterDealDetailNotesDataLoad(panel);		
	},
	
	clearDealNotes: function(tabItem){
		console.log("Bind Note Details");
		var me = this,panel;
		panel = tabItem.down('dealnotesgridpanel');
		var dealnotesst = panel.getStore();
		dealnotesst.removeAll();
		me.afterDealDetailNotesDataLoad(panel);		
	},
		
	afterDealDetailNotesDataLoad: function(panel){
		var me = this,tabItem,permission;
		var dealnotesst = panel.getStore();
		var count = dealnotesst.getCount();		
		var detailnotetoolbar = panel.down('toolbar');
		if(count>0){
			if(detailnotetoolbar){
				detailnotetoolbar.hide();
			}
		} else {
			if(detailnotetoolbar){
				detailnotetoolbar.show();
			
				if(panel.up('tab-deal-detail')){
					tabItem = panel.up('tab-deal-detail');
				} else if(panel.up('tab-deal-layout')){
					tabItem = panel.up('tab-deal-layout');
				} else if(panel.up('tab-sales-layout')){
					tabItem = panel.up('tab-sales-layout');
				} else if(panel.up('tab-sales-detail')){
					tabItem = panel.up('tab-sales-detail');
				}
				if(tabItem.dealDetailStore){
					permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
					console.log("Permission : "+permission);
					if(parseInt(permission)===0){						
						detailnotetoolbar.down('button').disable();
					} else {
						detailnotetoolbar.down('button').enable();
					}
				}
			}
		}
	},
	
	loadDealDetailNotes: function(panel){
		console.log("Notes Load");
        var me = this,context,record,dealid;
		if(panel.up('tab-deal-detail')){
			context = panel.up('tab-deal-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-sales-detail')){
			context = panel.up('tab-sales-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-deal-layout')){
			context = panel.up('tab-deal-layout');
			dealgrid = context.down('tab-deal-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		} else if(panel.up('tab-sales-layout')){
			context = panel.up('tab-sales-layout');
			dealgrid = context.down('tab-sales-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		}
		
		var dealnotesst = panel.getStore(); //('Notes');
        dealnotesst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealnotesst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				me.afterDealDetailNotesDataLoad(panel);
            }
        });
	},
	
    addNoteBtnClick: function(btn) {
        console.log("Add Note Button Clicked.");
        var me = this;
		me.openNotePanel(btn, "add");
    },
	openNotePanel: function(cmp, mode){
	    
		var me = this,
            title = "", tabpanel, context, spawnFrom, grid, activeTab, baseCardPanel, borderPanel, noteContainerPanel, notepanel, row, trgtcmp,eastregion;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
		    // when cmp is a button.
			trgtcmp = cmp;
		}
		
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		}
		context = tabpanel.getActiveTab();
        
        switch(mode) {
            case "add":
                title = "Add Note";    
            break;
            
            case "edit":
                title = "Edit Note";
				if(!grid.getSelectionModel().hasSelection()) {
					Ext.Msg.alert('Failed', "Please select note to edit.");
					return;
				} 
            break;
        }
		
        if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
			if(context.xtype == "tab-deal-layout"){
				eastregion = context.down('#deal-east-panel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion = context.down('#sales-east-panel');
			}
			eastregion.removeAll();
			if(context.down('notepanel')){
				console.log("Use Old Panel");
				notepanel = context.down('notepanel');
			} else {
				console.log("Create New Panel");
				notepanel = Ext.create('DM2.view.NotePanel');//me.getNotepanel();
				eastregion.add(notepanel);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(notepanel);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();
        } else if(context.xtype == "tab-deal-detail"){
			eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			if(context.down('dealdetailsformpanel').down('notepanel')){
				console.log("Use Old Panel");
				notepanel = context.down('dealdetailsformpanel').down('notepanel');
			} else {
				console.log("Create New Panel");
				notepanel = Ext.create('DM2.view.NotePanel');//me.getNotepanel();
			}
			eastregion.getLayout().setActiveItem(notepanel);
        } else if(context.xtype == "tab-sales-detail"){
			eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			if(context.down('salesdealdetailsformpanel').down('notepanel')){
				console.log("Use Old Panel");
				notepanel = context.down('salesdealdetailsformpanel').down('notepanel');
			} else {
				console.log("Create New Panel");
				notepanel = Ext.create('DM2.view.NotePanel');//me.getNotepanel();
			}
			eastregion.getLayout().setActiveItem(notepanel);
        }
		notepanel.getHeader().show();			
        notepanel.setTitle(title);
		notepanel.getForm().findField('content').focus();
		switch(mode) {
            case "add":
                notepanel.getForm().reset();    
            break;
            
            case "edit":
                var row = grid.getSelectionModel().getSelection()[0];
                notepanel.getForm().loadRecord(row);
            break;
        }
	},
	
	saveContactNotes: function(btn){
		console.log("Save Contact Note");
		var me = this;
		var contactid, context;
		if(btn.up('tab-contact-detail')){
			context = btn.up('tab-contact-detail');
			var record = context.record;
			contactid = record.data.contactid;
		}else{
			var tabcontactpanel = me.getController('MainController').getMainviewport().down('tab-contacts');
			var tabcontactgrid = tabcontactpanel.down('tab-contact-grid');
			if(!tabcontactgrid.getSelectionModel().hasSelection()) {
				Ext.Msg.alert('Failed', "Please select contact from contactgrid.");
				return;
			}
			var row = tabcontactgrid.getSelectionModel().getSelection()[0];
			contactid = row.data.contactid;
			context = tabcontactpanel.down('tab-contact-layout');
		}
	   var notefrm = btn.up('notepanel');
	   var noteid = notefrm.getForm().findField('noteid').getValue();
	   console.log("noteid Value"+noteid);
	   var data,methodName,urlval;
	   if(noteid==0 || noteid==null){
		   data = [{
				'content':notefrm.getForm().findField('content').getValue(),
				'idContact': contactid
		   }];
		   methodName = "POST";
		   urlval = DM2.view.AppConstants.apiurl+'Rnotes';
	   }else{
		   data = [{
				'@metadata': { 'checksum': 'override' },
				'idNote' :noteid,
				'content':notefrm.getForm().findField('content').getValue()
		   }];
		   methodName = "PUT";
		   urlval = DM2.view.AppConstants.apiurl+'mssql:Note/'+noteid;
	   }
	   notefrm.setLoading(true);
	   Ext.Ajax.request({
		   headers: {
			   'Content-Type': 'application/json',
			   Accept: 'application/json',
			   Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
		   },
		   url:urlval,
		   method:methodName,
		   params: Ext.util.JSON.encode(data),
		   scope:this,
		   success: function(response, opts) {
			   console.log("Note saved.");
			   notefrm.setLoading(false);
			   var obj = Ext.decode(response.responseText);
			   if(obj.statusCode == 201 || obj.statusCode == 200)
			   {
					notefrm.getForm().reset();
					if(btn.up('tab-contact-detail')){
						me.loadDealDetailNotes(btn.up('tab-contact-detail').down('contactdetailnotesgridpanel'));
					}else{
						me.getController('ContactController').loadContactNotesStore(contactid,context);
					}
			   }
			   else{
				   console.log("Note Submittion Failure.");
				   Ext.Msg.alert('Note Save failed', obj.errorMessage);
			   }
		   },
		   failure: function(response, opts) {
			   notefrm.setLoading(false);
			   console.log("Note Submittion Failure.");
			   var obj = Ext.decode(response.responseText);
			   Ext.Msg.alert('Note Save failed', obj.errorMessage);
		   }
	   });
	},
	
    submitNoteBtnClick: function(btn) {
        console.log("Submit Note");
        var me = this,tabpanel,dealgrid;
		var notepanel = btn.up('notepanel');
		if(notepanel.config.backview && notepanel.config.backview == "addcontactnotes"){
			console.log("Add Notes for Contact");
			me.saveContactNotes(btn);
		} else {
			var dealid, context;
			if(btn.up('tab-deal-detail')){
				context = btn.up('tab-deal-detail');
				var record = context.record;
				dealid = record.data.idDeal;
			} else if(btn.up('tab-sales-detail')){
				context = btn.up('tab-sales-detail');
				var record = context.record;
				dealid = record.data.idDeal;
			} else {
				if(btn.up('tab-deals')){
					tabpanel = me.getController('MainController').getMainviewport().down('tab-deals');
					dealgrid = tabpanel.down('tab-deal-grid');
				} else if(btn.up('tab-sales')){
					tabpanel = me.getController('MainController').getMainviewport().down('tab-sales');
					dealgrid = tabpanel.down('tab-sales-grid');
				}
				if(!dealgrid.getSelectionModel().hasSelection()) {
					Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
					return;
				}
				var row = dealgrid.getSelectionModel().getSelection()[0];
				dealid = row.data.idDeal;
			}
		   var notefrm = btn.up('notepanel');
		   var noteid = notefrm.getForm().findField('noteid').getValue();
		   console.log("noteid Value"+noteid);
		   var data,methodName,urlval;
		   if(noteid==0 || noteid==null){
			   data = [{
					'content':notefrm.getForm().findField('content').getValue(),
					'idDeal': dealid
			   }];
			   methodName = "POST";
			   urlval = DM2.view.AppConstants.apiurl+'Rnotes';
		   } else {
			   data = [{
					'@metadata': { 'checksum': 'override' },
					'idNote' :noteid,
					'content':notefrm.getForm().findField('content').getValue()
			   }];
			   methodName = "PUT";
			   urlval = DM2.view.AppConstants.apiurl+'mssql:Note/'+noteid;
		   }
		   notefrm.setLoading(true);
		   Ext.Ajax.request({
			   headers: {
				   'Content-Type': 'application/json',
				   Accept: 'application/json',
				   Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			   },
			   url:urlval,
			   method:methodName,
			   params: Ext.util.JSON.encode(data),
			   scope:this,
			   success: function(response, opts) {
				   console.log("Note saved.");
				   notefrm.setLoading(false);
				   var obj = Ext.decode(response.responseText);
				   if(obj.statusCode == 201 || obj.statusCode == 200){
						notefrm.getForm().reset();
						if(btn.up('tab-deals')) {
							var context = btn.up('tab-deals').getActiveTab();
							me.loadDealDetailNotes(context.down('dealnotesgridpanel'));
						} else if(btn.up('tab-sales')) {
							var context = btn.up('tab-sales').getActiveTab();
							me.loadDealDetailNotes(context.down('dealnotesgridpanel'));
						} else {
							me.loadNotesStore(dealid);
						}
						me.notePanelClose(btn.up('notepanel'));
				   } else {
					   console.log("Note Submittion Failure.");
					   Ext.Msg.alert('Note Save failed', obj.errorMessage);
				   }
			   },
			   failure: function(response, opts) {
				   notefrm.setLoading(false);
				   console.log("Note Submittion Failure.");
				   var obj = Ext.decode(response.responseText);
				   Ext.Msg.alert('Note Save failed', obj.errorMessage);
			   }
		   });
		}
    },

    resetNoteBtnClick: function(btn) {
        console.log("Reset Add Note Form");
		var notefrm = btn.up('notepanel');
        notefrm.getForm().reset();
    },

    loadNotesStore: function(dealid) {
        console.log("Notes Load");
        var me = this;
        var notesproxy = Ext.getStore('Notes').getProxy();
        notesproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('Notes').load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
                var notetoolbar = me.getNotegridtoolbar();
                console.log("Note Cnt"+Ext.getStore('Notes').getCount());
                if(Ext.getStore('Notes').getCount()>0){
                    if(notetoolbar){
                        notetoolbar.hide();
                    }
                }else{
                    if(notetoolbar){
                        notetoolbar.show();
                    }
                }
            }
        });
    },
	
    notegridrightclick: function(view, record, tr, rowIndex, e, eOpts) {
        var me = this;
        console.log("Show Menu for Note Edit/Histroy");
		me.showNotesGridMenu(view,record,e);
    },
	
	showNotesGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
        if(me.getNoteoptionmenu()){	
			var tabItem;
			if(view.ownerCt.up('tab-deal-detail')){
				tabItem = view.ownerCt.up('tab-deal-detail');
			} else if(view.ownerCt.up('tab-deal-layout')){
				tabItem = view.ownerCt.up('tab-deal-layout');
			} else if(view.ownerCt.up('tab-sales-layout')){
				tabItem = view.ownerCt.up('tab-sales-layout');
			} else if(view.ownerCt.up('tab-sales-detail')){
				tabItem = view.ownerCt.up('tab-sales-detail');
			}
			if(tabItem){
			    DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,me.getNoteoptionmenu());
			}			
            me.getNoteoptionmenu().spawnFrom = view;
            me.getNoteoptionmenu().showAt(e.getXY());
        }
	},
	
	onNotesGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showNotesGridMenu(view,record,e);
	},

    noteoptionitemclick: function(item, e, eOpts) {
        console.log("Note Menu Item Clicked.");
        if(item.getItemId()==="noteedit"){
            console.log("Note Edit Menu Item Clicked");
            this.openNotePanel(item, "edit");
        }else if(item.getItemId()==="notehistory"){
            console.log("Note History Menu Item Clicked");
        }else if(item.getItemId()==="noteadd"){
            console.log("Note Add Menu Item Clicked");
			this.openNotePanel(item, "add");
        }
    },

    notePanelClose: function(panel) {
		var me = this,eastregion,context,tabpanel;
		if(panel.config.backview && panel.config.backview == "addcontactnotes"){
			var tabcntpanel = panel.up('tab-contacts');
			context = tabcntpanel.getActiveTab();			
			eastregion = context.down('#contact-east-panel');
			me.getController('ContactController').closeRecvDealDetailDocs(context);
		} else {
			if(panel.up('tab-deals')){
				tabpanel = panel.up('tab-deals');
			} else if(panel.up('tab-sales')){
				tabpanel = panel.up('tab-sales');
			}
			context = tabpanel.getActiveTab();
			if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
				if(context.xtype == "tab-deal-layout"){
					eastregion = context.down('#deal-east-panel');
				} else if(context.xtype == "tab-sales-layout"){
					eastregion = context.down('#sales-east-panel');
				}				
				eastregion.removeAll();
				//eastregion.setCollapsed(false);
				eastregion.setVisible(false);
			} else if(context.xtype == "tab-deal-detail"){
				eastregion = context.down('dealdetailsformpanel').down('[region=east]');
				eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
			} else if(context.xtype == "tab-sales-detail"){
				eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
				eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
			}
		}
    }
});