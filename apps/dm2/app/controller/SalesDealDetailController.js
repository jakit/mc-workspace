Ext.define('DM2.controller.SalesDealDetailController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.DealUserTab',
			  'Ext.window.Toast',
			  'DM2.view.AddSaleDealBid',
			  'DM2.view.SaleBidGridPanel',
			  'DM2.view.SalesBidMenu',
			  'DM2.view.SalesEditView',
			  'DM2.view.CommissionScheduleMenu'],
    refs: {
        salesdealdetailsformpanel: 'salesdealdetailsformpanel',
		dealsalesgridpanel : 'dealsalesgridpanel',
		addsaledealbid : 'addsaledealbid',
		salesbidmenu: {
            autoCreate: true,
            selector: 'salesbidmenu',
            xtype: 'salesbidmenu'
        },
		salebidgridpanel:'salebidgridpanel',
		commissionschdgridpanel:'commissionschdgridpanel',
		commissionschedulemenu: {
            autoCreate: true,
            selector: 'commissionschedulemenu',
            xtype: 'commissionschedulemenu'
        }
    },

    init: function(application) {
        this.control({
             'salesdealdetailsformpanel':{
                 close:this.dealDetailsFormClose
             },
			 'salesdealdetailsformpanel tabpanel[action="salesdealdetailtabpanel"]':{
				 afterrender: this.setUpSalesDealDetailTabPanel,
				 beforetabchange: this.salesDealDetailTabChange
			 },
			 'salesdealdetailsformpanel splitbutton[action="salesdealdetaildocstab"]':{
				click: this.salesDealDetailDocsTabClick
			 },
			 'salesdealdetailsformpanel #salesdealdocstabmenu':{
				click: this.salesDealDocsTabMenuClick
			 },
			 'salesdealdetailsformpanel #salesdealdetailusertab':{
				click: this.salesDealDetailUserTabClick
			 },
			 '#salesdealdetailusertab menu':{
				click: this.salesDealDetailUserTabMenuClick
			 },
			 'dealsalesgridpanel': {
				 rowcontextmenu: this.onDealSalesGridPanelRightClick
			 },
			 'addsaledealbid': {
				 close : this.addSaleDealBidCloseBtnClick
			 },
			 'addsaledealbid button[action="savesalebidbtn"]': {
				 click : this.saveSaleBidBtnClick
			 },
			 'addsaledealbid button[action="doneActionBtn"]': {
				click : this.addSaleDealBidDoneBtnClick
			 },
			 'dealsalesgridpanel gridview': {
                expandbody: this.onSalesGridViewExpandbody
             },
			 'addsaledealbid button[action="contactpickbtn"]':{
				click: this.showContactSelectionPanel
			 },
			 'saledealdetailsalesfieldSet gridview': {
                expandbody: this.onSalesGridViewExpandbody
             },
			 'saledealdetailsalesfieldSet grid': {
                rowcontextmenu: this.salesFieldSetGridRightClick
             },
			 'saledealdetailsalesmenu menuitem':{
                click: this.saleDealDetailSalesGridMenuItemClick
             },
			 'salebidgridpanel':{
				rowcontextmenu: this.saleBidGridRightClick 
			 },
			 'salesbidmenu menuitem':{
                click: this.salesBidMenuItemClick
             },
			 'saleseditview': {
				 close : this.saleEditViewCloseBtnClick
			 },
			 '#saleseditview field':{
                change:this.onSalesEditViewFieldChange
             },
			 'saleseditview button[action="savesalesbtn"]': {
				 click : this.saveSalesBtnClick
			 },
			 'salesdealdetailsformpanel #salesdealdetailshistorytab':{
				click: this.salesDealDetailHistoryTabClick
			 },
			 '#salesdealdetailshistorytab menu':{
				click: this.salesDealDetailHistoryTabMenuClick
			 },
			 'commissionschdgridpanel button[action="addcommsched"]':{
				click: this.onAddCommSchedBtnClick
			 },
			 'commissionschdgridpanel button[action="savecommschedbtn"]':{
				click: this.onSaveCommSchedBtnClick
			 },
			 'commissionschdgridpanel':{
				select: this.onCommGridPanelItemSelect,
				rowcontextmenu: this.onCommissionSchdGridPanelRightClick
			 },
			 'commissionschedulemenu':{
                click: this.commissionScheduleMenuClick
             }
        });
    },
	
	setUpSalesDealDetailTabPanel: function(tabpanel){
		var me = this,tabItem;
		console.log("After Sales Detail Tab Panel Render");
		salestabpanel = tabpanel;
		tabItem = Ext.create('DM2.view.DealNotesGridPanel',{
					iconCls: 'notebtn'
				  });
		salestabpanel.add(tabItem);
		
		tabItem = Ext.create('DM2.view.DealDocsGridPanel',{
					iconCls: 'docbtn'/*,
					tabConfig: {
						hidden: true
					}*/
				  });
		salestabpanel.add(tabItem);
		/*salestabpanel.tabBar.insert(1, {
				xtype:'splitbutton',
				style:'margin-right:5px',
				iconCls: 'docbtn',
				text:'Documents',
				cls:'docbtncls',
				ui:'tabsplitbutton',
				action:'salesdealdetaildocstab',
				menu:{
					itemId: 'salesdealdocstabmenu',
					items:[{
						group: 'saledealdetaildocstabgrp',
						checked : true,
						text:'Deal Only'
					},{
						group: 'saledealdetaildocstabgrp',
						checked: false,
						text:'All'
					}]
			}
		});*/
		
		tabItem = Ext.create('DM2.view.DealDetailUserGridPanel',{
					iconCls: 'userbtn',
					title: 'Users',
					tabConfig: {
						hidden: true
					}
				  });
		salestabpanel.add(tabItem);
		
		///Add CommissionPercent Column for Sales Deal into User Grid
		var column = Ext.create('Ext.grid.column.Column', {
			text: 'Commision(%)',
			flex: 1,
			dataIndex: 'commissionPercent'
		});
	
		tabItem.headerCt.insert(
		  tabItem.columns.length - 1, // that's index column
		  column);
	
		tabItem.getView().refresh();
		
		//Insert Users Tab
		var dealuserstab = Ext.create('DM2.view.DealUserTab',{
                                itemId:'salesdealdetailusertab',
								ui:'tabsplitbutton'
                            });
        salestabpanel.tabBar.insert(2, dealuserstab);
		
		/////		
		tabItem = Ext.create('DM2.view.DealHistoryGrid',{
			iconCls: 'loanbtn',
			itemId : 'salesdealdetailshistorygrid',
			title: 'Deal History',
			tabConfig: {
				hidden: true
			}
	    });
		salestabpanel.add(tabItem);
		
		var salesdealdetailhistorytab = Ext.create('DM2.view.DealHistoryTab',{
			itemId:'salesdealdetailshistorytab',
			ui:'tabsplitbutton',
			style:'margin-left:5px'
		});
		salesdealdetailhistorytab.setText("Sale History");
		salestabpanel.tabBar.insert(3, salesdealdetailhistorytab);
		/////
		
		
		tabItem = Ext.create('DM2.view.DealDetailStatusHistoryPanel',{
					iconCls: 'dealbtn',
					title : 'Status History'
				  });
		salestabpanel.add(tabItem);
		
		/*
		tabItem = Ext.create('DM2.view.DealSalesGridPanel',{
					iconCls: 'dealbtn',
					title : 'Sales'
				  });
		salestabpanel.add(tabItem);
		*/
		salestabpanel.setActiveTab(0);		
	},
	
	salesDealDetailDocsTabClick: function(btn) {
		console.log("Set Sales Deal Document Tab");
		var me = this;
		var context = btn.up('tab-sales-detail');
		var salesdealdetailsformpanel = context.down('salesdealdetailsformpanel');
		salesdealdetailsformpanel.down('tabpanel[action="salesdealdetailtabpanel"]').setActiveTab(1);
		var dealdocsgridpanel = context.down('dealdocsgridpanel');
		me.getController('DocController').loadDocsPanelStore(dealdocsgridpanel);
		//me.loadDealDetailDocs(context,"deal");
	},
	
	salesDealDocsTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Deal Docs Menu Clicked");
        var me = this;
        var text = item.text;
		menu.up('tabpanel').setActiveTab(1);		
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabpanel.getActiveTab();
		var dealdocsgridpanel = context.down('dealdocsgridpanel');
        if(text === "All"){
            //Show All Docs
			console.log("Show All Docs");
			me.getController('DocController').loadAllDocsPerDealPanelStore(dealdocsgridpanel);
			//me.loadDealDetailDocs(context,"all");
        }else if(text === "Deal Only"){
			console.log("Show Deal Only Docs");
			me.getController('DocController').loadDocsPanelStore(dealdocsgridpanel);
			//me.loadDealDetailDocs(context,"deal");		
        }
	},
	
	salesDealDetailTabChange: function(tabPanel, newCard, oldCard, eOpts){
	    
		console.log("Sales Deal Detail Tab Change");
		var me = this;
		
        //var dealdocstab = tabPanel.down('splitbutton[action="salesdealdetaildocstab"]');
		var dealusertab = tabPanel.down('#salesdealdetailusertab');
        var salesdealdetailshistorytab = tabPanel.down('#salesdealdetailshistorytab');
		
        /*if(newCard.xtype == "dealdocsgridpanel" ) {
            dealdocstab.addCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
			if(salesdealdetailshistorytab){salesdealdetailshistorytab.removeCls("docactivebtncls");}
        } else*/ if(newCard.xtype =="dealhistorygrid") {
            if(salesdealdetailshistorytab){salesdealdetailshistorytab.addCls("docactivebtncls");}
            //dealdocstab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
        } else if(newCard.xtype =="dealdetailusergridpanel") {
            //dealdocstab.removeCls("docactivebtncls");
			dealusertab.addCls("docactivebtncls");
			if(salesdealdetailshistorytab){salesdealdetailshistorytab.removeCls("docactivebtncls");}
        } else {
            //dealdocstab.removeCls("docactivebtncls");
			dealusertab.removeCls("docactivebtncls");
			if(salesdealdetailshistorytab){salesdealdetailshistorytab.removeCls("docactivebtncls");}
        }		
	},
	
	salesDealDetailUserTabClick: function(tabItem){
		console.log("Sales Deal Detail Users Tab Click");
		var me = this;
		tabItem.up('tabpanel').setActiveTab(2);
		var p = tabItem.up('tab-sales-detail');
		var dealid = p.dealid;

		var tabpanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabpanel.getActiveTab();
		var usergrid = context.down('dealdetailusergridpanel');
		me.getController('DealUserController').loadDealUserDetailsWithParams('active',dealid,usergrid);
		usergrid.config.alloractiveusermode = "active";		
	},
	
	salesDealDetailUserTabMenuClick: function(menu, item, e, eOpts){
		console.log("Sales Deal Detail Users Tab Menu Click");
		var me = this;
		var text = item.text;
		menu.up('tabpanel').setActiveTab(2);
		var p = menu.up('tab-sales-detail');
		var dealid = p.dealid;
		
		var tabpanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabpanel.getActiveTab();
		var usergrid = context.down('dealdetailusergridpanel');
		
        if(text === "All"){
			console.log("Show All Users");
			me.getController('DealUserController').loadDealUserDetailsWithParams('all',dealid,usergrid);
			usergrid.config.alloractiveusermode = "all";
        } else if(text === "Active Only"){
			console.log("Show Active Users Only");
			me.getController('DealUserController').loadDealUserDetailsWithParams('active',dealid,usergrid);
			usergrid.config.alloractiveusermode = "active";
        }
	},
	
	bindDealSales: function(context){
		console.log("Bind Deal Sales.");
		var me = this,panel,dealsalessst;
		if(context.down('dealsalesgridpanel')){
			panel = context.down('dealsalesgridpanel');
			dealsalessst = panel.getStore();
			dealsalessst.loadData(context.dealDetailStore.data.items[0].data.SaleList);
		}
		
		panel = context.down('saledealdetailsalesfieldSet');
		dealsalessst = panel.down('grid').getStore();
		dealsalessst.loadData(context.dealDetailStore.data.items[0].data.SaleList);
		me.afterSaleDealDetailSalesDataLoad(panel);
		
		var override = panel.down('grid').getPlugin('rowexpander-sales');
		//console.log("Cnt "+override.grid.getStore().getCount());
        for(i=0; i < override.grid.getStore().getCount();i++){
			console.log("Expand");
			var rec = override.grid.getStore().getAt(i);
			if(rec.get('saleType')!="B"){
				rec.config.directLoadFlag = true;
				override.toggleRow(i, override.grid.getStore().getAt(i));
			}
		 	//override.expand(i);
		}
	},
	
	afterSaleDealDetailSalesDataLoad: function(panel){
		var dealsalessst = panel.down('grid').getStore();
		var count = dealsalessst.getCount();	    
		var singleDetail = panel.detailOnSingleRecord; 
		
		// works like other quadrants
		// only show form detail when these 2 are true, else the grid is already displayed
		if(count === 1 && singleDetail == true) {
			panel.setData(dealsalessst.getAt(0));
			//panel.loadForm(panel);
			panel.down('grid').getHeaderContainer().show();
		} else if(count === 0){
			panel.down('grid').getHeaderContainer().hide();
		} else {
			panel.down('grid').getHeaderContainer().show();
		}
		panel.down('grid').getSelectionModel().select(0);
	},
	
	onDealSalesGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
        e.stopEvent();
		var me = this,menutext = "",actionval="";
        console.log("Show Menu for Sales Deal");
		if(record.get('saleType')==='S' || record.get('saleType')==='O'){
            menutext = "Add Bid";
			actionval = 'addbid';
	        if(me.getSaledealdetailsalesgridmenu()){
	            me.getSaledealdetailsalesgridmenu().showAt(e.getXY());
			}
			me.getSaledealdetailsalesgridmenu().removeAll();
			var menuitem = Ext.create('Ext.menu.Item', {
				text    : menutext,
				action  : actionval,
				modifyMenu : 'yes'
			});
			me.getSaledealdetailsalesgridmenu().add(menuitem);
        }		
		var context = me.getController('DealDetailController').getTabContext(view.ownerCt);
		if(context){
			DM2.app.getController('DealDetailController').disableModifyMenu(context,me.getSaledealdetailsalesgridmenu());
		}
    },
	
	saleDealDetailSalesGridMenuItemClick: function(item, e, eOpts){
		console.log("Sales quadrant Menu item Clicked : "+item.action);
		var me = this;
        if(item.action === "addbid") {
            console.log("Add New Bid");
			var tmprec = {};
			var salesmenu = item.up('saledealdetailsalesmenu');
			var salesrec = salesmenu.getRec();
			if(!salesrec){
				var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
				var context = activemenutab.getActiveTab();
				var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);

				var dealsalespanel = detailpanel.down('saledealdetailsalesfieldSet');
				salesrec = dealsalespanel.getData();
			}
            me.openAddSaleBidView("add",salesrec,"menu");
        } else if(item.action === "editsales") {
            console.log("Edit Sales Item");
            me.openEditSaleView();
        }	
	},
	
	openAddSaleBidView: function(mode,rec,fromtype){
		var me = this,addsaledealbid;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('addsaledealbid')){
			console.log("Use Old Panel");
			addsaledealbid = detailpanel.down('addsaledealbid');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	addsaledealbid = Ext.create('DM2.view.AddSaleDealBid');
		}
        eastregion.getLayout().setActiveItem(addsaledealbid);
		var title = "Add Bid";
		addsaledealbid.config.viewMode = "addsaledealbid";
		addsaledealbid.getForm().reset();
		
		if(fromtype=="activity"){
			addsaledealbid.down('button[action="doneActionBtn"]').show();
		} else {
			addsaledealbid.down('button[action="doneActionBtn"]').hide();
		}
		
		addsaledealbid.getForm().findField('idSale').setValue(rec.get('idSale'));
		addsaledealbid.getForm().findField('idBid').setValue(null);
		if(mode=="edit"){
			console.log(rec);
			addsaledealbid.getForm().loadRecord(rec);
			title = "Edit Bid";
			addsaledealbid.getForm().findField('price').setValue(rec.get('salePrice'));
			addsaledealbid.getForm().findField('fullName').setValue(rec.get('Buyer'));
			addsaledealbid.getForm().findField('idBuyerName').setValue(rec.get('idBuyer'));
			console.log(rec.get('targetCloseDate'));
			var targetCloseDateVal = rec.get('targetCloseDate');
			if(targetCloseDateVal!=null){
				var targetCloseDatearr = targetCloseDateVal.split("T");
				myDate = Ext.Date.parse(targetCloseDatearr[0], "Y-m-d");
				addsaledealbid.getForm().findField('targetCloseDate').setValue(myDate);
			}
		}
		addsaledealbid.setTitle(title);
	},
	
	addSaleDealBidCloseBtnClick: function(panel) {		
		var me = this;
		//var adddealpropertyform = btn.up('adddealpropertyform');
		if(panel.config.backview =="createnewdealfromproperty"){
			var tabpropertypanel = panel.up('tab-properties');
			var context = tabpropertypanel.getActiveTab();
			if(context.xtype == "tab-property-layout"){
				var eastregion = context.down('#property-east-panel');
				createnewdealfrompropertypanel = context.down('createnewdealfrompropertypanel');
				eastregion.getLayout().setActiveItem(createnewdealfrompropertypanel);
			}
		} else {
			var context = me.getController('DealDetailController').getTabContext(panel);
			var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
			var eastregion = detailpanel.down('[region=east]');
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
    },
	
	saveSaleBidBtnClick: function(btn){
		var me = this,salerec,idSale,methodname;
		console.log("Save Sale Bid Button Click");
		
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);

        var dealsalespanel = detailpanel.down('saledealdetailsalesfieldSet');			
        //var contextMenu = dealsalespanel.contextMenu;
        //var row = contextMenu.getRec();        
       
        var addsaledealbid = btn.up('addsaledealbid');
		var addsaledealbidForm = addsaledealbid.getForm();
		var idBid = addsaledealbidForm.findField('idBid').getValue();
		idSale = addsaledealbidForm.findField('idSale').getValue();
		
		var contactid = addsaledealbidForm.findField('idBuyerName').getValue();
		var contactTypeval = addsaledealbidForm.findField('contactType').getValue();
		var data = {
			'idSale'      : idSale,
            'idBuyerName' : contactid,
            'price'       : addsaledealbidForm.findField('price').getValue(),
			'targetCloseDate' : addsaledealbidForm.findField('targetCloseDate').getValue()
		};
		
		if(idBid===0 || idBid===null || idBid==="") {
            console.log("Add New Sale Bid");
            methodname = "POST";			
        } else {
            console.log("Save Edit Bid");
            data['idBid'] = idBid;
            data['@metadata'] = {'checksum' : 'override'};
            methodname = "PUT";			
        }

        addsaledealbid.setLoading(true);
        Ext.Ajax.request({
            method: methodname,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Bid',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Sale Bid is submitted.");
                addsaledealbid.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					addsaledealbid.getForm().reset();
					me.addSaleDealBidCloseBtnClick(addsaledealbid);
					me.refreshBidsBySaleId(idSale);
					if(idBid===0 || idBid===null || idBid==="") {
						var record = context.record;
						var dealid = record.data.idDeal;
						var data = [{
							'idDeal': dealid,
							'idContact' : contactid,
							'contactType' : contactTypeval
						}];
						me.getController('DealDetailController').saveDealXContact(data,detailpanel.down('dealdetailcontactfieldset'),context);
					}
                } else {
                    console.log("Add Bid Failure.");
                    Ext.Msg.alert('Add Bid failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Add Bid Failure.");
                addsaledealbid.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Add Bid failed', obj.errorMessage);
            }
        });
	},
	
	onSalesGridViewExpandbody: function(rowNode, record, expandRow, eOpts){
		console.log("Sales Row Expanded.");
        var me = this;
		
		if(record.get('saleType')=='S' || record.get('saleType')=='O'){
			console.log("Bids");
			console.log(record);
			console.log(record.data.bid);
			//var readable = record.get('readable');
			//var modifiable = record.get('modifiable');
			var idSale = record.get('idSale');
			/*if(readable==0){
				return false;
				//me.getController('DealController').dealPermissionNotAllowed();
			}else{*/
				var targetId = 'bidgridrow-' + idSale;
				if(Ext.getCmp(targetId + '_grid')){
					Ext.getCmp(targetId + '_grid').destroy();
				}
				var nestedGrid = Ext.getCmp(targetId + '_grid');
				if (!nestedGrid) {
					nestedGrid = Ext.create('DM2.view.SaleBidGridPanel', {
						renderTo: targetId,
						id: targetId + '_grid'/*,
						cls:'dealmenupropertyloancls'*/
					});
					rowNode.grid = nestedGrid;
					// prevent bubbling of the events
					nestedGrid.getEl().swallowEvent([
						'mousedown', 'mouseup', 'click',
						'contextmenu', 'mouseover', 'mouseout',
						'dblclick', 'mousemove'
					]);
				}
				
				console.log("rec.config.directLoadFlag : "+record.config.directLoadFlag);
				if(record.config.directLoadFlag && record.config.directLoadFlag==true){
					nestedGrid.getStore().loadData(record.data.bid);
					record.config.directLoadFlag = false;
				} else {
					var optionstproxy = nestedGrid.getStore().getProxy();
					optionstproxy.setHeaders({
						Accept: 'application/json',
						Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
					});
					nestedGrid.getStore().load({params:{filter :'idSale = '+idSale}});	
				}				
			//}
		} else {
			return false;
		}
	},
	
	refreshSaleDealDetailData:function(context){
		var me = this;
		
		//var dealdetailform = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var dealDetailStore = context.dealDetailStore;
		
		dealDetailStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		//dealdetailform.setLoading(true);
		context.setLoading(true);
		dealDetailStore.load({
			scope: this,
			params: {
				'filter' : 'idDeal = '+context.dealid
			},
			callback: function(records, operation, success) {
				//dealdetailform.setLoading(false);
				context.setLoading(false);
				console.log("Refresh SaleDealDetailData Callback.");
				console.log(records);
				if(success && records.length>0){
					var recitem = records[0].data;
					var tmprec = Ext.create("DM2.model.Deal",recitem);
					if(typeof recitem.permission.modifiable!= "undefined"){
						tmprec.set('modifiable',recitem.permission.modifiable);
					}
					if(typeof recitem.permission.readable!= "undefined"){
						tmprec.set('readable',recitem.permission.readable);
					}
					if(recitem.SaleList.length>0){
						tmprec.set('saleType',recitem.SaleList[0].saleType);
					}
					context.record = tmprec;
					console.log("Context New Rec");
					console.log(context.record);
					me.getController('SalesController').afterRDealDetailsLoad(context);
					me.getController('SalesController').loadAllSales([]);
				}
			}
		});
	},
	
	showContactSelectionPanel: function(btn){
		var me = this,addsalebid;
		console.log("Show Contact Selction Panel : ");
		addsalebid = btn.up('addsaledealbid');
		me.getController("DealDetailController").showAddDealContactForm("addsalebid",addsalebid);
	},
	
	selectContactForBid: function(adddealcontactform){
		var me = this;
		var allcontactgrid = adddealcontactform.down('#adddealcontactgrid');
			
		if (allcontactgrid.getSelectionModel().hasSelection()) {
			var row = allcontactgrid.getSelectionModel().getSelection()[0];
			//console.log(row.get('contactid'));
			contactid = row.get('contactid');
			var fullName = row.get('name');
			var contactType = row.get('contactType');
			if(adddealcontactform.config.backview == "addsalebid") {
				var addsalebid = adddealcontactform.config.backfrm;
				addsalebid.getForm().findField('fullName').setRawValue(fullName);
				addsalebid.getForm().findField('idBuyerName').setRawValue(contactid);
				addsalebid.getForm().findField('contactType').setRawValue(contactType);
			}
			me.getController("DealDetailController").addDealContactCloseBtnClick(adddealcontactform);
		} else {
			Ext.Msg.alert('Failed', "Please select contact from Allcontactsgrid.");
			return false;
		}
	},
	
	salesFieldSetGridRightClick: function(view, record, tr, rowIndex, e, eOpts) {
		var me = this;
        e.stopEvent();
        console.log("On Right Click of Sales Grid.");
		var salesfieldSet = view.up('saledealdetailsalesfieldSet');									
		if(record.get('saleType')==='S' || record.get('saleType')==='O'){
			salesfieldSet.contextMenu.down('menuitem[action="addbid"]').show();
		} else {
			salesfieldSet.contextMenu.down('menuitem[action="addbid"]').hide();
		}
    },
	
	saleBidGridRightClick: function(view, record, tr, rowIndex, e, eOpts){
		e.stopEvent();
		var me = this;		        
        console.log("Show Menu for Sales bids");
		if(me.getSalesbidmenu()){
			me.getSalesbidmenu().showAt(e.getXY());
			me.getSalesbidmenu().config.bidrec = record;
			me.getSalesbidmenu().config.bidgrid = view.up('salebidgridpanel');
			
			if(record.get('selected')==true){
				me.getSalesbidmenu().down('menuitem[action="selectbid"]').hide();
				me.getSalesbidmenu().down('menuitem[action="unselectbid"]').show();
			} else {
				me.getSalesbidmenu().down('menuitem[action="selectbid"]').show();
				me.getSalesbidmenu().down('menuitem[action="unselectbid"]').hide();
			}
		}
	},
	
	salesBidMenuItemClick: function(item, e, eOpts){
		console.log("Sales Bid Menu item Clicked : "+item.action);
		var me = this;
		var salebidmenu = item.up('salesbidmenu');
		var bidrec = salebidmenu.config.bidrec;
		var bidgrid = salebidmenu.config.bidgrid;
        if(item.action === "addbid") {
            console.log("Add New Bid");
            me.openAddSaleBidView("add",bidrec,"menu");
        } else if(item.action === "editbid") {
            console.log("Edit Bid Item");
            me.openAddSaleBidView("edit",bidrec,"menu");
        } else if(item.action === "deletebid") {
            console.log("Delete Bid Item");
            me.deleteSaleBid(bidrec);
        } else if(item.action === "selectbid") {
            console.log("Select Bid Item");			
            me.selectSaleBid(true,bidrec,bidgrid);
        } else if(item.action === "unselectbid") {
            console.log("UnSelect Bid Item");
            me.selectSaleBid(false,bidrec,bidgrid);
        }	
	},
	
	openEditSaleView: function(){
		var me = this,addsaledealbid;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('saleseditview')){
			console.log("Use Old Panel");
			saleseditview = detailpanel.down('saleseditview');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	saleseditview = Ext.create('DM2.view.SalesEditView');
		}
        eastregion.getLayout().setActiveItem(saleseditview);
		saleseditview.setTitle("Edit Sale");
		saleseditview.config.viewMode = "saleseditview";
		
		var dealsalespanel = detailpanel.down('saledealdetailsalesfieldSet');
			
        var contextMenu = dealsalespanel.contextMenu;
        var row = contextMenu.getRec();        
        // when the context menu is invoked from a form, there won't be any data set to the context menu
        if(!row) {
            row = dealsalespanel.getData();
        } else {
            dealsalespanel.setData(row);
        }
		saleseditview.getForm().loadRecord(row);
		//console.log(context.record);
		//console.log(context.dealDetailStore);
		if(context.record && context.record.get('saleType')=="O"){
			saleseditview.getForm().findField('saleType').setReadOnly(true);
		}
	},
	
	saleEditViewCloseBtnClick: function(panel) {		
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
		eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },
	
	onSalesEditViewFieldChange: function(field, newValue, oldValue, eOpts){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(field);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
        var saleseditview = detailpanel.down('saleseditview');
        saleseditview.down('button[action="savesalesbtn"]').enable();
	},
	
	saveSalesBtnClick: function(btn){
		var me = this;
		console.log("Save Sales btn Click");
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var saleseditview = detailpanel.down('saleseditview');
		
        var idSale   = saleseditview.getForm().findField('idSale').getValue();
        var saleType = saleseditview.getForm().findField('saleType').getValue();
        var price    = saleseditview.getForm().findField('price').getValue();
		var percentCommission     = saleseditview.getForm().findField('percentCommission').getValue();
		var flatAmountCommission  = saleseditview.getForm().findField('flatAmountCommission').getValue();
		var commissionType = saleseditview.getForm().findField('commissionType').getValue();
		
        saleseditview.setLoading(true);
        var data = {
            "@metadata": {
                "checksum": "override"
            },
			'idSale'            : idSale,
            'saleType'            : saleType,
            'price'               : price,
            'percentCommission'   : percentCommission,
			'flatAmountCommission': flatAmountCommission,
			'commissionType' : commissionType
        };

        ///Do Save
        var methodname = "PUT";

        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Sale',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                saleseditview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201) {
                    console.log("Sale is Modified.Now Refresh.");
                    //Refresh & Load Details;
                    me.saleEditViewCloseBtnClick(saleseditview); //Close Modify Sale view
					if(context.xtype=="tab-sales-detail"){
						me.getController('SalesDealDetailController').refreshSaleDealDetailData(context);
					} 
                } else {
                    console.log("Modify Sale Failed.");
                    Ext.Msg.alert('Modify Sale Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                saleseditview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Modify Sale', obj.errorMessage);
            }
        });
	},
	
	getSelectedBidRec: function(bidgrid){
		var me = this,deleteRecBidId = 0;
		var biddata = bidgrid.getStore().data.items;
		console.log(biddata);
		for(var i=0;i<biddata.length;i++){
			if(biddata[i].data.selected){
				deleteRecBidId = biddata[i].data.idBid;
			}
		}
		return deleteRecBidId;
	},
	
	selectSaleBid: function(selectedVal,rec,bidgrid){
		var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var saledealdetailsalesfieldSet = detailpanel.down('saledealdetailsalesfieldSet');
 	    var bidid = rec.get('idBid');
		saledealdetailsalesfieldSet.setLoading(true);
        var dataarr = [];
		var tmpdata = {
            "@metadata": {
                "checksum": "override"
            },
			'idBid'     : bidid,
            'selected'  : selectedVal
        };
		dataarr.push(tmpdata);
		if(selectedVal==true){
			var deleteRecBidId = me.getSelectedBidRec(bidgrid);
			if(deleteRecBidId!=0){
				tmpdata = {
					"@metadata": {
						"checksum": "override"
					},
					'idBid'     : deleteRecBidId,
					'selected'  : false
				};
				dataarr.push(tmpdata);
			}
		}

		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			},
			method:'PUT',
			params: Ext.util.JSON.encode(dataarr),
			url:DM2.view.AppConstants.apiurl+'mssql:Bid',
			scope:this,
			success: function(response, opts) {
				saledealdetailsalesfieldSet.setLoading(false);
				console.log("Edited Bid.");
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 200){
					me.refreshBidsBySaleId(rec.get('idSale'));
					if(bidgrid.xtype=="selectbidpanel"){
						me.getController('SelectBidController').loadBids(bidgrid)
					}
				} else {
					console.log("Edit Bid Failure.");
					Ext.Msg.alert('Edit Bid failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				saledealdetailsalesfieldSet.setLoading(false);
				console.log("Edit Bid Failure.");
				var obj = Ext.decode(response.responseText);
				Ext.Msg.alert('Edit Bid failed', obj.errorMessage);
			}
		});
	},
	
	deleteSaleBid: function(rec){
		var me = this;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var saledealdetailsalesfieldSet = detailpanel.down('saledealdetailsalesfieldSet');
 	    var bidid = rec.get('idBid');
        Ext.Msg.show({
            title:'Delete Bid?',
            message: 'Are you sure you want to delete the sales bid?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    saledealdetailsalesfieldSet.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Bid/'+bidid+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            saledealdetailsalesfieldSet.setLoading(false);
                            console.log("Removed Bid.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200){
                                //me.loadSubmissionContacts(loanid,context);
								me.refreshBidsBySaleId(rec.get('idSale'));
                            } else {
                                console.log("Delete Bid Failure.");
                                Ext.Msg.alert('Delete Bid failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            saledealdetailsalesfieldSet.setLoading(false);
                            console.log("Delete Bid Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Bid failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
	},
	
	refreshBidsBySaleId: function(idSale){
		var me = this;
		var targetId = 'bidgridrow-' + idSale;
		/*if(Ext.getCmp(targetId + '_grid')){
			Ext.getCmp(targetId + '_grid').destroy();
		}*/
		var nestedGrid = Ext.getCmp(targetId + '_grid');
		/*
		if (!nestedGrid) {
			nestedGrid = Ext.create('DM2.view.SaleBidGridPanel', {
				renderTo: targetId,
				id: targetId + '_grid'
			});
			rowNode.grid = nestedGrid;
			// prevent bubbling of the events
			nestedGrid.getEl().swallowEvent([
				'mousedown', 'mouseup', 'click',
				'contextmenu', 'mouseover', 'mouseout',
				'dblclick', 'mousemove'
			]);
		}*/
		if (nestedGrid) {
			var optionstproxy = nestedGrid.getStore().getProxy();
			optionstproxy.setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			nestedGrid.getStore().load({params:{filter :'idSale = '+idSale}});
		}
	},
	
	salesDealDetailHistoryTabClick: function(tabItem) {
		console.log("Set Deal Details History Tab");
		var me = this;
		//var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		//var context = activemenutab.getActiveTab();
		var dealhistorygrid = tabItem.up('tabpanel').down('dealhistorygrid');		
		tabItem.up('tabpanel').setActiveTab(dealhistorygrid);
		me.getController('DealHistoryController').onCategoryChange("Addressess", tabItem.down('menu'),"sale");
	},
	
	salesDealDetailHistoryTabMenuClick: function(menu, item, e, eOpts) {
		console.log("Sales Deal Details Docs Menu Clicked");
        var me = this;
        var text = item.text;
		//var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		//var context = activemenutab.getActiveTab();
		var dealhistorygrid = menu.up('tabpanel').down('dealhistorygrid');		
		menu.up('tabpanel').setActiveTab(dealhistorygrid);
		me.getController('DealHistoryController').onCategoryChange(text, menu,"sale");
	},
	
	onAddCommSchedBtnClick: function(btn){
		var me = this;
		console.log("Add Commission Sched Btn Click");
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var saleseditview = detailpanel.down('saleseditview');
		var commissionschdgridpanel = saleseditview.down('commissionschdgridpanel');
		commissionschdgridpanel.setCollapsed(false);
		commissionschdgridpanel.down('#structuredcommschedform').show();
		me.addCommSchedBtnClick(btn);
	},
	
	addCommSchedBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
		var saleseditview = detailpanel.down('saleseditview');
		
		var structuredcommschedform = saleseditview.down('#structuredcommschedform');
		var commissionschdgridpanel = saleseditview.down('commissionschdgridpanel');
		
		if (saleseditview.getForm().findField('idSale').getValue()!=0) {
            //console.log(quotedetailspanel.config.loanid);
            var idSale = saleseditview.getForm().findField('idSale').getValue();		
		
			commissionschdgridpanel.getSelectionModel().deselectAll();
			
			structuredcommschedform.getForm().reset();
			
			structuredcommschedform.setTitle("Add Structured Commission Schedule");			
			//structuredschedform.getForm().findField('idLoan').setRawValue(loanid);
			structuredcommschedform.getForm().findField('idCommissionSched').setRawValue(0);
			
			//structuredschedform.getForm().findField('formmode').setRawValue("add");
			//structuredschedform.down('button[action="saveloanschedbtn"]').disable();
			
			/*if(quotedetailspanel.config.isQuoteFormDirty == false){
				quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').disable();
				quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').disable();
			}
			quotedetailspanel.config.isOptionFormDirty = false;
			quotedetailspanel.config.isFormDirty = false;*/
        } else {
            Ext.Msg.alert('Failed', "Sale is not selected.");
            return false;
        }
	},
	
	loadCommSchedule: function(saleseditview){
		console.log("Commision Schedule Loaded.");
		var idSale = saleseditview.getForm().findField('idSale').getValue();
        if (idSale!=null && idSale!=0) {
			var commissionschdgridpanel = saleseditview.down('commissionschdgridpanel');
			var commSched_st = commissionschdgridpanel.getStore();
			commSched_st.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			var filterstarr = [];
			var filterst1 = "idSale="+idSale;
			filterstarr.push(filterst1);
			/*var filterst2 = "order=orderID ASC";
			filterstarr.push(filterst2);*/
		
			commSched_st.load({
				params: {
					filter :filterstarr
				},
				scope:this,
				callback: function(records, operation, success){
					if(success){
						console.log("Loaded Commission Scheduled");
						if(commSched_st.getCount()>0){
							commissionschdgridpanel.getSelectionModel().select(0);
						} else {
							//me.addQuoteOptionBtnClick();
						}
					}
				}
			});
        }
	},
	
	onSaveCommSchedBtnClick: function(btn){
		var me = this,orderID,loanid;
		console.log("Save Comm Schedule Structure");
		//var context = btn.up('tab-deal-detail');
		///var record = context.record;
        //var dealid = record.data.idDeal;
		
		var saleseditview = btn.up('saleseditview');        
		
		var commissionschdgridpanel = saleseditview.down('commissionschdgridpanel');
		var structuredschedform = commissionschdgridpanel.down('#structuredcommschedform');		

		var commSched_st = commissionschdgridpanel.getStore();
		var cnt = commSched_st.getCount();
		if(cnt==0){
			orderID = 1;
		} else {
			orderID = commSched_st.getAt(cnt-1).get('orderID')+1;
		}
		
		saleid  = saleseditview.getForm().findField('idSale').getValue();
		
        var commschedForm = structuredschedform.getForm();
        var idCommissionSched = commschedForm.findField('idCommissionSched').getValue();
		var data = {
			'idSale'    : saleid,
			'amount'    : commschedForm.findField('amount').getValue(),
			'percentCommission' : commschedForm.findField('percentCommission').getValue()
			//'quoteRate'   : Number(quoteForm.findField('quoteRate').getValue())
		};
        if(idCommissionSched===0 || idCommissionSched===null || idCommissionSched==="") {
            console.log("Add New Comm Schedule");
            var methodname = "POST";
			data['orderID'] = orderID;
        } else {
            console.log("Save Comm Schedule");
            data['idCommissionSched'] = idCommissionSched;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";			
        }

        structuredschedform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
			url:DM2.view.AppConstants.apiurl+'mssql:CommissionSchedule',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                structuredschedform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201) {
					if(idCommissionSched===0 || idCommissionSched===null || idCommissionSched==="") {
						console.log("Added New Commission Schedule");
						//dealdetailquotepanel.config.reloadQuoteTabData = true;			
					}
					/*if(quotedetailspanel.config.isQuoteFormDirty == false){
						quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').disable();
						quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').disable();
						
						quotedetailspanel.config.isFormDirty = false;
						quotedetailspanel.config.isOptionFormDirty = false;
					}*/
					me.loadCommSchedule(saleseditview);
                } else {
                    console.log("Add/Edit Commission Schedule Option Save Failure.");
                    Ext.Msg.alert('Commission Schedule Save failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                structuredschedform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Commission Schedule', obj.errorMessage);
            }
        });
	},
	
	onCommGridPanelItemSelect: function(grid, record, index, eOpts){
		var me = this;
		var saleseditview = grid.view.ownerCt.up('saleseditview');	
					
		var commissionschdgridpanel = saleseditview.down('commissionschdgridpanel');		
		var structuredcommschedform = commissionschdgridpanel.down('#structuredcommschedform');
		
		structuredcommschedform.getForm().findField('idCommissionSched').setRawValue(record.get('idCommissionSched'));
		structuredcommschedform.getForm().findField('amount').setRawValue(record.get('amount'));
		structuredcommschedform.getForm().findField('percentCommission').setRawValue(record.get('percentCommission'));
		
		structuredcommschedform.setTitle("Edit Structured Commission Schedule");
		//structuredschedform.down('button[action="saveloanschedbtn"]').disable();
	},
	
	onCommissionSchdGridPanelRightClick: function(viewTable , record , tr , rowIndex , e , eOpts){
		var me = this;
		console.log("Commission Schd Grid Panel Right Click");
		e.stopEvent();
		var comschedgrid = viewTable.up('commissionschdgridpanel');
		if(comschedgrid.contextMenu){
            comschedgrid.contextMenu.showAt(e.getXY());
        }
	},
	
	commissionScheduleMenuClick: function(menu, item, e, eOpts) {
        console.log("Commission Schedule Menu Click");
        if(item.action === "editcommissionschedule"){
            console.log("Edit Commission Schedule");
            this.editCommissionScheduleMenuClick();
        } else if(item.action === "deletecommissionschedule"){
            console.log("Delete Commission Schedule");
            this.deleteCommissionScheduleMenuClick();
        } else if(item.action === "deleteallcommissionschedule"){
            console.log("Delete All Commission Schedule");
            this.deleteAllCommissionScheduleMenuClick();
        }
    },
	
	deleteCommissionScheduleMenuClick: function(){
		var me = this,idCommissionSched,idSale;
		var tabsalepanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabsalepanel.getActiveTab();
		
		var commissionschdgridpanel = context.down('commissionschdgridpanel');		
		if (commissionschdgridpanel.getSelectionModel().hasSelection()) {
            var row = commissionschdgridpanel.getSelectionModel().getSelection()[0];
            console.log(row.get('idCommissionSched'));
            idCommissionSched = row.get('idCommissionSched');
			idSale = row.get('idSale');
        } else {
            Ext.Msg.alert('Failed', "Commission is not selected.");
            return false;
        }

        Ext.Msg.show({
            title:'Delete Commission Schedule?',
            message: 'Are you sure you want to delete this Commission Schedule?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    commissionschdgridpanel.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:CommissionSchedule/'+idCommissionSched+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            commissionschdgridpanel.setLoading(false);
                            console.log("Removed Commission Schedule.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200) {
								me.loadCommSchedule(context.down('saleseditview'));
                            } else {
                                console.log("Delete Commission Schedule Failure.");
                                Ext.Msg.alert('Delete Commission Schedule failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            commissionschdgridpanel.setLoading(false);
                            console.log("Delete Commission Schedule Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Commission Schedule failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
	},
	
	deleteAllCommissionScheduleMenuClick: function(){
		var me = this;
		console.log("Delete All Commission Schedule Btn Click");
		var tabsalespanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabsalespanel.getActiveTab();
		
		var commissionschdgridpanel = context.down('commissionschdgridpanel');
		var commSched_st = commissionschdgridpanel.getStore();
		
		var commschedarr = [];
		for(var i=0;i<commSched_st.getCount();i++){
			var recitem = commSched_st.getAt(i);
			var Commission_Schedule_item = {
				"@metadata": {
				  "entity": "CommissionSchedule",
				  "action": "DELETE",
				  "checksum": "override"
				},
				"idCommissionSched": recitem.get('idCommissionSched')
			};
			commschedarr.push(Commission_Schedule_item);
		}
		if(commschedarr.length>0){
			Ext.Msg.show({
				title:'Delete All Commission Schedule?',
				message: 'Are you sure you want to delete all Commission Schedule?',
				buttons: Ext.Msg.YESNO,
				icon: Ext.Msg.QUESTION,
				fn: function(btn) {
					if (btn === 'yes') {
						commissionschdgridpanel.setLoading(true);
						Ext.Ajax.request({
							headers: {
								'Content-Type': 'application/json',
								Accept: 'application/json',
								Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
							},
							method:'PUT', //Delete Method with primary key
							params: Ext.util.JSON.encode(commschedarr),
							url:DM2.view.AppConstants.apiurl+'mssql:LoanSchedule',
							scope:this,
							success: function(response, opts) {
								commissionschdgridpanel.setLoading(false);
								console.log("Removed Commission Schedule.");
								var obj = Ext.decode(response.responseText);
								if(obj.statusCode == 200) {
									me.loadCommSchedule(context.down('saleseditview'));
								} else {
									console.log("Delete All Commission Schedule Failure.");
									Ext.Msg.alert('Delete All Commission Schedule failed', obj.errorMessage);
								}
							},
							failure: function(response, opts) {
								commissionschdgridpanel.setLoading(false);
								console.log("Delete All Commission Schedule Failure.");
								var obj = Ext.decode(response.responseText);
								Ext.Msg.alert('Delete All Commission Schedule failed', obj.errorMessage);
							}
						});
					}
				}
			});
		}
	},
	
	editCommissionScheduleMenuClick: function(){
		var me = this;
		console.log("Edit Commission Schedule Btn Click");
		var tabsalespanel = me.getController('MainController').getMainviewport().down('tab-sales');
		var context = tabsalespanel.getActiveTab();
		
		var commissionschdgridpanel = context.down('commissionschdgridpanel');
		var structuredcommschedform = commissionschdgridpanel.down('#structuredcommschedform');
		
		///Load Commission Schedule Data to form		
		if (commissionschdgridpanel.getSelectionModel().hasSelection()) {
            var record = commissionschdgridpanel.getSelectionModel().getSelection()[0];
            console.log(record.get('idCommissionSched'));
            //idCommissionSched = row.get('idCommissionSched');
			//idSale = row.get('idSale');
			structuredcommschedform.getForm().findField('idCommissionSched').setRawValue(record.get('idCommissionSched'));
			structuredcommschedform.getForm().findField('amount').setRawValue(record.get('amount'));
			structuredcommschedform.getForm().findField('percentCommission').setRawValue(record.get('percentCommission'));
			
			structuredcommschedform.setTitle("Edit Structured Commission Schedule");			
			structuredcommschedform.show();
        } else {
            Ext.Msg.alert('Failed', "Commission Schedule is not selected.");
            return false;
        }			
	},
	
	addSaleDealBidDoneBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	}
});