Ext.define('DM2.controller.QuoteController', {
    extend: 'Ext.app.Controller',
	requires:['DM2.view.PPPListGrid',
			  'DM2.view.IndexSpreadGrid',
			  'DM2.view.QuoteOptionFormPanel',
			  'DM2.view.QuoteOptionsMenu',
			  'DM2.view.LoanScheduleMenu',
			  'DM2.view.QuoteOptionsGridPanel',
			  'DM2.view.DealUserPickerPanel'],
    refs: {
        dealdetailquotesgridpanel: 'dealdetailquotesgridpanel',
        dealdetailquotemenu: {
            autoCreate: true,
            selector: 'dealdetailquotemenu',
            xtype: 'dealdetailquotemenu'
        },
        dealdetailquotepanel: {
            autoCreate: true,
            selector: 'dealdetailquotepanel',
            xtype: 'dealdetailquotepanel'
        },
		quotedetailspanel: {
            autoCreate: true,
            selector: 'quotedetailspanel',
            xtype: 'quotedetailspanel'
        },
        quotemenu: {
            autoCreate: true,
            selector: 'quotemenu',
            xtype: 'quotemenu'
        },
        quotesgridpanel: 'quotesgridpanel',
        quoteaddbankpanel: {
            autoCreate: true,
            selector: 'quoteaddbankpanel',
            xtype: 'quoteaddbankpanel'
        },
		quoteoptionsmenu: {
            autoCreate: true,
            selector: 'quoteoptionsmenu',
            xtype: 'quoteoptionsmenu'
        },
		loanschedulemenu: {
            autoCreate: true,
            selector: 'loanschedulemenu',
            xtype: 'loanschedulemenu'
        },
		ppplistgrid: 'ppplistgrid',
		indexspreadgrid: 'indexspreadgrid',
		dealuserpickerpanel : 'dealuserpickerpanel'
    },

    init: function(application) {
        this.control({
            'dealdetailquotesgridpanel': {
                //itemclick: this.onDealMenuGridpanelItemClick,
                itemdblclick: this.onDealDetailQuoteGridpanelItemDblClick,
                rowcontextmenu: this.onDealDetailQuoteGridPanelRightClick,
				afterrender: 'loadQuoteDetails'
            },
			'dealdetailquotesgridpanel gridview': {
                expandbody: this.onQuoteGridViewExpandbody,
				itemlongpress: this.onQuoteGridItemLongPress
            },
            'dealdetailquotemenu': {
                click: this.dealDetailQuoteMenuClick
            },
            'dealdetailquotesgridpanel #add': {
                click:this.addDealDetailQuote
            },
            'dealdetailquotepanel':{
                close:this.dealdetailquotepanelclose
            },
			'quotedetailspanel':{
                close:this.quoteDetailsFormPanelClose
            },
            'quotedetailspanel button[action="dealdetailquotesavebtn"]': {
                click:this.saveDealDetailQuote
            },
            'quotedetailspanel button[action="dealdetailquoteresetbtn"]': {
                click:this.resetDealDetailQuote
            },
            'quotedetailspanel button[action="dealdetailquoteclearbtn"]': {
                click:this.clearDealDetailQuote
            },
			/*'dealdetailquotepanel button[action="showPPPListBtn"]': {
                click:this.showPPPListDialog
            },*/
            'dealdetailquotepanel #add':{
                click:this.addPPPBtnClick
            },
            'quotesgridpanel':{
				beforeselect: this.onQuoteGridItemBeforeSelect,
                select:this.selectQuoteItem,
                rowcontextmenu: this.onQuoteGridPanelRightClick
            },
			'quotesgridpanel dataview':{
				itemlongpress: this.onQuoteGrigItemLongPress
            },
            'quotemenu':{
                click: this.quoteMenuClick
            },
            'quotesgridpanel #receivequotedonebtn':{
                click:this.onReceiveQuoteDoneBtnClick
            },
            'quotesgridpanel #addquotetoolbarmenu':{
                click: this.quoteMenuClick
            },
            'dealdetailquotepanel #quoteselectbankbtn':{
                click: this.quoteSelectBankBtnClick
            },
            'quoteaddbankpanel':{
                close:this.closeQuoteAddBankPanel,
                itemdblclick: this.quoteAddBankItemDbClick
            },
            'quoteaddbankpanel button[action="savebankbtn"]':{
                click:this.saveQuoteAddBankBtnClick
            },
			'#quotedetailspanel field':{
                change:this.onQuoteDetailSelFieldChange
            },
			'#quotedetailspanel combobox[name="rateType"]':{
                change:this.onRateTypeFieldChange
            },
			'#quotedetailspanel #quotepropertygridpanel':{
				selectionchange: this.onPropertySelectionchange
			},
			'ppplistgrid':{
				itemdblclick: this.onPPPListItemSelect,
				close: this.closePPPListGrid
			},
			'ppplistgrid button[action="savepppbtn"]':{
				click: this.onSavePPPBtnClick
			},
			'quotedetailspanel #addquoteoptionbtn':{
                click:this.addQuoteOptionBtnNewClick
            },
			'quotedetailspanel quoteoptionformpanel':{
				close: this.closeQuoteOptionFormPanel
			},
			'quotedetailspanel button[action="savequoteoptionbtn"]':{
				click: this.onSaveQuoteOptionBtnClick
			},
			'quotedetailspanel #optionsgridpanel':{
				rowcontextmenu: this.onOptionsGridPanelRightClick,
				//itemclick: this.onOptionsGridpanelItemClick
				select: this.onOptionsGridpanelItemSelect
			},
			'quoteoptionsmenu':{
                click: this.quoteOptionsMenuClick
            },
			'quotedetailspanel button[action="addloansched"]':{
				click: this.onAddLoanSchedBtnClick
			},
			'quotedetailspanel button[action="saveloanschedbtn"]':{
				click: this.onSaveLoanSchedBtnClick
			},
			'quotedetailspanel structuredgridpanel':{
				rowcontextmenu: this.onStructuredGridPanelRightClick,
				select: this.onStructuredGridPanelItemSelect
			},
			'loanschedulemenu':{
                click: this.loanScheduleMenuClick
            },
			'quotedetailspanel textfield[name="pppValue"]':{
				ontriggerclick: this.showPPPListDialog
			},
			'quotedetailspanel textfield[name="indexSpread"]':{
				ontriggerclick: this.showPPPListDialog
			},
			'indexspreadgrid':{
				itemdblclick: this.onPPPListItemSelect,
				close: this.closePPPListGrid
			},
			'indexspreadgrid button[action="saveindexspreadbtn"]':{
				click: this.onSavePPPBtnClick
			},
			'quotedetailspanel textfield[name="pppDescription"]':{
				ontriggerclick: this.showPPPListDialog
			},
			'quotedetailspanel textfield[name="indexSpreadOption"]':{
				ontriggerclick: this.showPPPListDialog
			},
			'quotedetailspanel textfield[name="quoteIssuerName"]':{
				ontriggerclick: this.showContactPickerList
			},
			'quotedetailspanel textfield[name="letterAddresseeName"]':{
				ontriggerclick: this.showContactPickerList
			},
			'quotedetailspanel textfield[name="letterSignedByName"]':{
				ontriggerclick: this.showDealUserPickerList
			},
			'dealuserpickerpanel':{
				close: this.closeDealUserPickerPanel
			},
			'dealuserpickerpanel grid':{
				itemclick: this.dealUserPickerPanelItemClick
			},
			'dealuserpickerpanel button[action="dealusersavebtn"]':{
				click: this.dealUserPickerSaveBtnClick
			}
        });
    },
		
	showPPPListDialog: function(btn,callfrom){
		var me = this;
		console.log("Show PPP List Dialog Box"+callfrom);
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		//var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
		var listgrid;
		
		if(callfrom=="spreadindex"|| callfrom=="spreadindexoption"){
			if(eastregion.down('indexspreadgrid')){
				console.log("Use Old Panel");
				listgrid = eastregion.down('indexspreadgrid');
			} else {
				//create new Activity Detail Panel
				console.log("Create New Panel");
				listgrid = Ext.create('DM2.view.IndexSpreadGrid');
			}
		} else if(callfrom=="ppp" || callfrom=="pppoption"){
			if(eastregion.down('ppplistgrid')){
				console.log("Use Old Panel");
				listgrid = eastregion.down('ppplistgrid');
			} else {
				//create new Activity Detail Panel
				console.log("Create New Panel");
				listgrid = Ext.create('DM2.view.PPPListGrid');
			}
		}		
		
		if(btn.up('dealdetailquotepanel')){
			listgrid.config.backview = "loanmanager";
		} else {
			listgrid.config.backview = "loaneditor";
		}
		
		listgrid.config.fieldmode = callfrom;
        eastregion.getLayout().setActiveItem(listgrid);
	
		if(listgrid.config.fieldmode=="spreadindex" || listgrid.config.fieldmode=="spreadindexoption"){
			listgrid.setTitle("Spread Index List");
			me.loadIndexSpreadList(listgrid);
		} else if(listgrid.config.fieldmode=="ppp" || listgrid.config.fieldmode=="pppoption"){
			listgrid.setTitle("PPP List");
			me.loadPPPList(listgrid);
		}
	},
	
	loadPPPList: function(grid){
		var me = this;
		var ppplistst = Ext.getStore('PPPList');
        ppplistst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
		if(!ppplistst.isLoaded()){
        	ppplistst.load({
				callback: function(records, operation, success){
				}
			});
		}
	},
	
	loadIndexSpreadList: function(grid){
		var me = this;
		var indexspreadst = Ext.getStore('IndexSpread');
        indexspreadst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
		if(!indexspreadst.isLoaded()){
        	indexspreadst.load({
				callback: function(records, operation, success){
				}
			});
		}
	},
	
	onPPPListItemSelect: function(dataview, record, item, index, e, eOpts){
		var me = this,listgrid;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		if(dataview.up('ppplistgrid')){
			listgrid = dataview.up('ppplistgrid');
		} else if(dataview.up('indexspreadgrid')){
			listgrid = dataview.up('indexspreadgrid');
		}
		me.setPPPSpreadIndexValue(context,listgrid);
	},
	
	setPPPSpreadIndexValue: function(context,listgrid){
		var me = this;
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		//var ppplistgrid = dealdetailquotepanel.down('ppplistgrid');
        if (listgrid.getSelectionModel().hasSelection()) {
            var row = listgrid.getSelectionModel().getSelection()[0];
			/*var finaldesc = record.get('finaldesc');
			//var res = finaldesc.replace("/<br/>/g", "\n");
			var res = finaldesc.split("<br/>").join("\n")
			*/		
			if(listgrid.config.fieldmode=="spreadindex"){
				quotedetailspanel.down('textfield[name="indexSpread"]').setValue(row.get('indexSpreadValue'));
			} else if(listgrid.config.fieldmode=="ppp"){
				quotedetailspanel.down('textfield[name="pppValue"]').setValue(row.get('pppValue'));
			} else if(listgrid.config.fieldmode=="pppoption"){
				quotedetailspanel.down('quoteoptionformpanel').down('textfield[name="pppDescription"]').setValue(row.get('pppValue'));
			} else if(listgrid.config.fieldmode=="spreadindexoption"){
				quotedetailspanel.down('quoteoptionformpanel').down('textfield[name="indexSpreadOption"]').setValue(row.get('indexSpreadValue'));
			}
			me.closePPPListGrid(listgrid);
        } else {
			Ext.Msg.alert('Alert', "Please select PPP value from PPP List.");
            return false;
		}		
	},
	
	onSavePPPBtnClick: function(btn){
		var me = this;
		var context = btn.up('tab-deal-detail');
		if(btn.up('ppplistgrid')){
			listgrid = btn.up('ppplistgrid');
		} else if(btn.up('indexspreadgrid')){
			listgrid = btn.up('indexspreadgrid');
		}
		me.setPPPSpreadIndexValue(context,listgrid);
	},
	
	closePPPListGrid: function(panel){
		var me = this,activeItem;
		console.log("Close PPP List Grid");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
		if(panel.config.backview =="loanmanager"){
			activeItem = context.down('dealdetailsformpanel').down('dealdetailquotepanel');        
		} else if(panel.config.backview =="loaneditor"){
			activeItem = context.down('dealdetailsformpanel').down('quotedetailspanel');        
		}
		eastregion.getLayout().setActiveItem(activeItem);		
	},
	
	onPropertySelectionchange: function(selModel, selected, eOpts){
		console.log("On Property Selection Change.");
		var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var quotepropertygridpanel = quotedetailspanel.down('#quotepropertygridpanel');
		console.log("Btn Value"+quotepropertygridpanel.enableSaveBtn);
		if(quotepropertygridpanel && quotepropertygridpanel.enableSaveBtn){
			//var quotedetailspanel = dealdetailquotepanel.down('#quotedetailspanel');
			quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').enable();
			quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').enable();
			quotedetailspanel.config.isFormDirty = true;
		} else {
			quotepropertygridpanel.enableSaveBtn = true;
		}
		console.log("Btn Value"+quotepropertygridpanel.enableSaveBtn);
	},
	
    loadQuoteDetails: function(panel) {
        console.log("Quotes Load");
        var me = this;
		var context = panel.up('tab-deal-detail');
        var record = context.record;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
		
        var dealquotest = panel.getStore(); //('DealQuotes').getProxy();
        dealquotest.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
        filterstarr.push("selected!='P'");

        dealquotest.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){

                console.log("DealQuote Cnt"+dealquotest.getCount());
                if(dealquotest.getCount()>0){
                    if(panel.down('toolbar')){
                        panel.down('toolbar').hide();
                    }
                }else{
                    if(panel.down('toolbar')){
                        panel.down('toolbar').show();
						var tabItem = panel.up('tab-deal-detail');
						if(tabItem){
							//var obj = Ext.decode(tabItem.dealDetailStore.data.items[0].data.permission);
							//console.log(obj);
							var permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
							console.log("Permission : "+permission);
							if(parseInt(permission)===0){						
								panel.down('toolbar').down('button').disable();
							}else{
								panel.down('toolbar').down('button').enable();
							}
						}
                    }
                }
            }
        });
    },

    onDealDetailQuoteGridpanelItemDblClick: function(dataview, record, item, index, e, eOpts) {
        var title = "Loan Editor";
		var tabItem = dataview.up('tab-deal-detail');
		var permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
		console.log("Permission : "+permission);
		if(parseInt(permission)===0){						
			Ext.Msg.alert('Alert!', "You don't have the permission to edit quote.");
		} else {
			this.callLoanEditor(record);
		}
    },
	
	onQuoteGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		console.log("Quote Grid Item Long Presss");
		console.log(record);
		view.getSelectionModel().select(record);
		me.showDealDetailQuoteGridMenu(view,record,e);
	},
	
	showDealDetailQuoteGridMenu: function(view,record,e){
		e.stopEvent();
		var me = this;
        console.log("Show Menu for Quote Add/Dealte");
        if(this.getDealdetailquotemenu()){
            this.getDealdetailquotemenu().showAt(e.getXY());
        }
		me.getDealdetailquotemenu().removeAll();
        var menuitem = Ext.create('Ext.menu.Item', {
            text    : 'Add New Quote',
			action  : 'addnewquote',
			modifyMenu : 'yes'
        });
        me.getDealdetailquotemenu().add(menuitem);
		var menuitem = Ext.create('Ext.menu.Item', {
            text    : 'Edit Quote',
			action  : 'editquote',
			modifyMenu : 'yes'
        });
        me.getDealdetailquotemenu().add(menuitem);
		var menuitem = Ext.create('Ext.menu.Item', {
            text    : 'Remove Quote',
			action  : 'removequote',
			modifyMenu : 'yes',
			hidden:true
        });
        me.getDealdetailquotemenu().add(menuitem);
        var menutext = "",actionval="";
        //if(record.get('selected')===false){
		if(record.get('selected')==='Q'){
            menutext = "Select Quote";
			actionval = 'selectquote';
        } else {
            menutext = "UnSelect Quote";
			actionval = 'unselectquote';
        }
        var menuitem = Ext.create('Ext.menu.Item', {
            text    : menutext,
			action  : actionval,
            loanid  : record.get('loanid'),
			modifyMenu : 'yes'
        });
        me.getDealdetailquotemenu().add(menuitem);
		
		var tabItem = view.ownerCt.up('tab-deal-detail');
		//var tabItem = tabdealpanel.getActiveTab();
		if(tabItem){
			DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,me.getDealdetailquotemenu());
		}
	},
	
    onDealDetailQuoteGridPanelRightClick: function(view, record, tr, rowIndex, e, eOpts) {
		var me = this;
        me.showDealDetailQuoteGridMenu(view,record,e);
    },

    dealDetailQuoteMenuClick: function(menu, item, e, eOpts) {
        console.log("Menu Clicked : "+item.action);
		var me = this;
        if(item.action === "addnewquote") {
            console.log("Add New Quote");
            me.addDealDetailQuote();
        } else if(item.action === "editquote") {
            console.log("Edit Quote");			
			var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
			var context = tabdealpanel.getActiveTab();
			var dealdetailquotesgridpanel = context.down('dealdetailquotesgridpanel');
			var title = "Loan Editor";
			if (dealdetailquotesgridpanel.getSelectionModel().hasSelection()) {
				var loanrec = dealdetailquotesgridpanel.getSelectionModel().getSelection()[0];
				me.callLoanEditor(loanrec);
			}
        } else if(item.action === "removequote") {
            console.log("Remove Quote");
        } else if(item.action === "selectquote") {
            this.setSelectUnselectQuote('S',item.loanid);
        } else if(item.action === "unselectquote") {
            this.setSelectUnselectQuote('Q',item.loanid);
        }
    },

    showDealDetailQuote: function(actiontype, record, title, activityid) {
        var me = this;
		console.log("showDealDetailQuote");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
		var dealdetailquotepanel;
		if(context.down('dealdetailsformpanel').down('dealdetailquotepanel')){
			console.log("Use Old Panel");
			dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	dealdetailquotepanel = Ext.create('DM2.view.DealDetailQuotePanel');//me.getDealdetailquotepanel();
		}
        eastregion.getLayout().setActiveItem(dealdetailquotepanel);
		
        //dealdetailquotepanel.getForm().reset();  //Reset The Form
        dealdetailquotepanel.getForm().findField('activityid').setRawValue(activityid);

		//Load Properties
		var dealProperties = context.down('dealdetailpropertyfieldset').down('grid').getStore().getData();        
		dealdetailquotepanel.down('#quotepropertygridpanel').getStore().loadData(dealProperties.items);
		
		dealdetailquotepanel.down('button[action="dealdetailquotesavebtn"]').show();
		dealdetailquotepanel.down('button[action="dealdetailquoteresetbtn"]').show();
		dealdetailquotepanel.down('button[action="dealdetailquoteclearbtn"]').show();
		//dealdetailquotepanel.down('button[itemId="quoteselectbankbtn"]').show();
		
        if(actiontype==="add"){
            var quoteForm = dealdetailquotepanel.getForm();
            quoteForm.findField('usedInDeal').setRawValue(false);
        }

        if(actiontype==="add" || actiontype==="activity"){
            dealdetailquotepanel.setTitle(title);
        } else if(actiontype==="edit"){
			dealdetailquotepanel.editQuote = true;
			dealdetailquotepanel.editQuoteRecord = record;
			var dealrecord = context.record;
			var dealid = dealrecord.data.idDeal;
			me.loadAllDealQuotes(dealid,context);
			
            //dealdetailquotepanel.getForm().loadRecord(record);
            dealdetailquotepanel.setTitle(title);

            //me.loadLoanProperties(record.get('loanid'),context);
        }
		/*
        var banksst = Ext.getStore('Banks');
        var bankcmb = dealdetailquotepanel.getForm().findField('bank');
        if(banksst.isLoaded()){
            if(actiontype==="add" || actiontype==="activity"){
                //bankcmb.select(banksst.getAt(0));
            }else{
                banksst.each(function(rec) {
                    console.log(rec.get('shortName'));
                    if(rec.get('shortName')===record.get('bank')){
                        //bankcmb.select(rec.get('idBank'));
                    }
                });
            }
        } else {
            banksst.getProxy().setHeaders({
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            });
            banksst.load({
                scope: this,
                callback: function(records, operation, success) {
                    console.log(success);
                    if(!success){

                    }
                    else{
                        if(records.length > 0){
                            if(actiontype==="add" || actiontype==="activity"){
                                //bankcmb.select(banksst.getAt(0));
                            }else{
                                banksst.each(function(rec) {
                                    if(rec.get('shortName')===record.get('bank')){
                                        //bankcmb.select(rec.get('idBank'));
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }*/
    },

    dealdetailquotepanelclose: function(panel, eOpts) {
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    addDealDetailQuote: function() {
        var rec = null;
        var title = "Add Quote";
        this.showDealDetailQuote('add',rec,title,0);
		this.addBankToQuote();
    },

    saveDealDetailQuote: function(btn) {
        console.log("Save Deal Detail Quote");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var record = context.record;
        var dealid = record.data.idDeal;

		var quotedetailspanel = detailpanel.down('quotedetailspanel');
		var propertygrid = quotedetailspanel.down('#quotepropertygridpanel');
		
        var quoteForm = quotedetailspanel.getForm();
        var idLoan = quoteForm.findField('idLoan').getValue();
		var operatingAsval = quoteForm.findField('operatingAs').getValue();
		if(operatingAsval == 'P'){
			operatingAsval = 'Q';
		}
		var letterSigner = quoteForm.findField('letterSigner').getValue();
		if(letterSigner==""){letterSigner=null;}
		var data = {
			'idDeal' : dealid,
			//'usedInDeal'  : quoteForm.findField('usedInDeal').getValue(),
			'idBank' : quoteForm.findField('Bank_idBank').getValue(),
			'rateType'    : quoteForm.findField('rateType').getValue(),
			'quoteType'   : quoteForm.findField('quoteType').getValue(),
			'termMonths'  : quoteForm.findField('term').getValue(),
			'initiationFee':quoteForm.findField('initiationFee').getValue(),
			'executionType':quoteForm.findField('executionType').getValue(),
			//'spread'      : quoteForm.findField('spread').getValue(),
			'percentSpread' : quoteForm.findField('percentSpread').getValue(),
			//'indexSpread' : quoteForm.findField('index').getValue(),
			'indexSpread' : quoteForm.findField('indexSpread').getValue(),
			//'amortization': quoteForm.findField('amortterm').getValue(),
			'amortization': quoteForm.findField('amortization').getValue(),
			'interestOnly': quoteForm.findField('interestOnly').getValue(),
			//'amountRequested': quoteForm.findField('sentamount').getValue(),
			//'amountQuoted': quoteForm.findField('receivedamount').getValue(),
			'amountRequested': quoteForm.findField('amountRequested').getValue(),
			'amountQuoted': quoteForm.findField('amountQuoted').getValue(),
			//'dateRequested': quoteForm.findField('sentdate').getValue(),
			//'dateReceived': quoteForm.findField('receiveddate').getValue(),
			'dateRequested': quoteForm.findField('dateRequested').getValue(),
			'dateReceived': quoteForm.findField('dateReceived').getValue(),
			'pppDesc'     : quoteForm.findField('pppValue').getValue(),
			//'pppOptionWindow': quoteForm.findField('pppOptionWindow').getValue(),
			'rateQuoted': quoteForm.findField('rate').getValue(),
			'operatingAs' : operatingAsval,
			'commitExpirationDate'  : quoteForm.findField('commitExpirationDate').getValue(),
            'commitmentNumber'      : quoteForm.findField('commitmentNumber').getValue(),
            'commitmentDate'        : quoteForm.findField('commitmentDate').getValue(),
            'commitmentCheck'       : quoteForm.findField('commitmentCheck').getValue(),
            'commitmentCheckDate'   : quoteForm.findField('commitmentCheckDate').getValue(),
            'commitmentCheckAmount' : quoteForm.findField('commitmentCheckAmount').getValue(),
			'quoteIssuer'           : quoteForm.findField('quoteIssuer').getValue(),
			'letterAddressee'     : quoteForm.findField('letterAddressee').getValue(),
			'letterSigner'        : letterSigner,
			'specialNote'         : quoteForm.findField('specialNote').getValue(),
			'receivedNote'        : quoteForm.findField('receivedNote').getValue(),
			'securedLoan'         : quoteForm.findField('securedLoan').getValue()
		};

        if(idLoan===0 || idLoan===null || idLoan==="") {
            console.log("Add New Loan");
            var methodname = "POST";
			data['existingLoan'] = false;
            /*if (propertygrid.getSelectionModel().hasSelection()) {
                var pselarr = propertygrid.getSelectionModel().getSelection();
                var Property_has_Loan = [];
                for(var j=0;j<pselarr.length;j++){
                    var item = pselarr[j];
                    var Property_has_Loan_item = {
                        idProperty : item.get('propertyid')
                    };
                    Property_has_Loan.push(Property_has_Loan_item);
                }
                data.Property_has_Loan = Property_has_Loan;
            }*/
        } else {
            console.log("Save Loan");
            data['idLoan'] = idLoan;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";

			if(quotedetailspanel.config.isOptionFormDirty == true){
				///Build Options Data
				var quoteoptionformpanel = detailpanel.down('quoteoptionformpanel');		
				var quoteForm = quoteoptionformpanel.getForm();
				var optionDataArray = [];
				
				var idLoanOption = quoteForm.findField('idLoanOption').getValue();
				var optionData = {
					'idLoan'        : quoteForm.findField('idLoan').getValue(),
					'optAmount'     : quoteForm.findField('optAmount').getValue(),
					'rateType'      : quoteForm.findField('rateType').getValue(),
					'quoteRate'     : Number(quoteForm.findField('quoteRate').getValue()),
					'term'          : quoteForm.findField('term').getValue(),
					'indexSpread'   : quoteForm.findField('indexSpreadOption').getValue(),
					'percentSpread' : quoteForm.findField('percentSpread').getValue(),
					'interestOnly'  : quoteForm.findField('interestOnly').getValue(),
					'optionFloor'   : quoteForm.findField('optionFloor').getValue(),
					'startDate'         : quoteForm.findField('startDate').getValue(),
					'monthEnd'          : Number(quoteForm.findField('monthEnd').getValue()),
					'amortizationMonth' : quoteForm.findField('amortizationMonth').getValue(),
					'optionSelected' : quoteForm.findField('optionSelected').getValue(),
					'renewFee' : quoteForm.findField('renewFee').getValue(),
					'renewFeePercent' : quoteForm.findField('renewFeePercent').getValue(),
					'pppDescription' : quoteForm.findField('pppDescription').getValue(),
					'pppWindow' : quoteForm.findField('pppWindow').getValue(),
					'optionDesc' : Number(quoteForm.findField('optionDesc').getValue()),
					'descRecord' : Number(quoteForm.findField('descRecord').getValue())					
				};
				if(idLoanOption===0 || idLoanOption===null || idLoanOption==="") {
					console.log("Add New Options");
					optionData['@metadata'] = {
					  "entity": "OptionsForQuotes",
					  "action": "INSERT"
					};
				} else {
					console.log("Save Modified Options");
					optionData['idLoanOption'] = idLoanOption;
					optionData['@metadata'] = {'checksum' : 'override'};
				}
				optionDataArray.push(optionData);
				data['LoanOptionList'] = optionDataArray;
			}	
			/*
			if (propertygrid.getSelectionModel().hasSelection()) {
                var pselarr = propertygrid.getSelectionModel().getSelection();
                var Property_has_Loan = [];
                for(var j=0;j<pselarr.length;j++){
                    var item = pselarr[j];
                    var Property_has_Loan_item = {
                        idProperty : item.get('propertyid'),
						"idLoan": idLoan
                    };
                    Property_has_Loan.push(Property_has_Loan_item);
                }
                data.Property_has_Loan = Property_has_Loan;
            }*/
        }

        quotedetailspanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'RLoans',
			//url:DM2.view.AppConstants.apiurl+'mssql:Loan',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                quotedetailspanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
					me.savePropertyHasLoan(context,idLoan);
					if(quotedetailspanel.config.isOptionFormDirty == true){
						me.checkAndLoadSubLoanOptions(idLoan,context);
					}
                    /*dealdetailquotepanel.getForm().reset();
                    me.loadQuoteDetails(context.down('dealdetailquotesgridpanel'));  //Load First Quote not as loan
                    me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
                    me.loadAllDealQuotes(dealid,context); //Load All Quotes
					*/
                    //me.dealdetailquotepanelclose();
                }
                else{
                    console.log("Add/Edit Quote Failure.");
                    Ext.Msg.alert('Quote Save failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                quotedetailspanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Quote', obj.errorMessage);
            }
        });
    },
	
	savePropertyHasLoan: function(context,loanid){
		console.log("Save Property To Loan Table");
		var me = this;
		var finalpropertyarr = [];
		
		var quotedetailspanel = context.down('quotedetailspanel');
		var existPropertyObjArr = quotedetailspanel.existPropertyObjArr;
		console.log("existPropertyObjArr");
		console.log(existPropertyObjArr);
		
		var propertygrid = quotedetailspanel.down('#quotepropertygridpanel');
		if (propertygrid.getSelectionModel().hasSelection()) {
			var pselarr = propertygrid.getSelectionModel().getSelection();
			var Property_has_Loan = [];
			for(var j=0;j<pselarr.length;j++){
				var item = pselarr[j];
				var flag = 0;
				//Check whether property exist as selected or not
				for(var k=0; k<existPropertyObjArr.length; k++){
					if(item.get('propertyid')===existPropertyObjArr[k].idProperty){
						flag = 1;
					}
				}
				if(flag!=1){
					///Add Property as Insert into Property_has_loan Table
					var Property_has_Loan_item = {
						"@metadata": {
						  "entity": "PropxLoan",
						  "action": "INSERT"
						},
						"idProperty": item.get('propertyid'),
						"idLoan": loanid
					  };
					finalpropertyarr.push(Property_has_Loan_item);
				}
			}
		}
		//Check whether property not selected but exist in old then delete
		for(var k=0; k<existPropertyObjArr.length; k++){
			var flag = 0;
			if (propertygrid.getSelectionModel().hasSelection()) {
				var pselarr = propertygrid.getSelectionModel().getSelection();
				for(var j=0;j<pselarr.length;j++){
					var item = pselarr[j];
					if(item.get('propertyid')===existPropertyObjArr[k].idProperty){
						flag = 1;
					}
				}
			}
			if(flag!=1){
				///Add Property as Delete into Property_has_loan Table
				var Property_has_Loan_item = {
					"@metadata": {
					  "href": DM2.view.AppConstants.apiurl+'mssql:PropxLoan/'+existPropertyObjArr[k].idPropxLoan,
					  "checksum": "override",
					  "action": "DELETE"
					}
				  };
				finalpropertyarr.push(Property_has_Loan_item);
			}
		}
		console.log(finalpropertyarr);
		if(finalpropertyarr.length>0) {
			quotedetailspanel.setLoading(true);
			Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
				},
				//url:DM2.view.AppConstants.apiurl+'RLoans',
				url:DM2.view.AppConstants.apiurl+'mssql:PropxLoan',
				params: Ext.util.JSON.encode(finalpropertyarr),
				scope:this,
				method:'PUT',
				success: function(response, opts) {
					quotedetailspanel.setLoading(false);
					var obj = Ext.decode(response.responseText);
					if(obj.statusCode == 200 || obj.statusCode == 201)
					{
						me.afterSaveDetailQuote(context);
					} else {
						console.log("Add/Edit Quote Failure.");
						Ext.Msg.alert('Quote Save failed', obj.errorMessage);
					}
				},
				failure: function(response, opts) {
				    quotedetailspanel.setLoading(false);
					var obj = Ext.decode(response.responseText);
					Ext.Msg.alert('Save Quote', obj.errorMessage);
				}
			});
		} else {
			me.afterSaveDetailQuote(context);
		}
	},
	
	afterSaveDetailQuote: function(context){
		var me = this;
		var record = context.record;
        var dealid = record.data.idDeal;

		var quotedetailspanel = context.down('quotedetailspanel');
		quotedetailspanel.config.isFormDirty = false;
		
		quotedetailspanel.config.isOptionFormDirty = false;
		quotedetailspanel.config.isQuoteFormDirty = false;
		
		//dealdetailquotepanel.getForm().reset();
		me.loadQuoteDetails(context.down('dealdetailquotesgridpanel'));  //Load First Quote not as loan
		me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
		if(context.down('dealdetailquotepanel')){
			me.loadAllDealQuotes(dealid,context); //Load All Quotes
		}
	},
	
    resetDealDetailQuote: function(btn) {
        console.log("Reset DealDetail Quote Click");
        var me = this;
		var context = btn.up('tab-deal-detail');
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
        if (quotedetailspanel.editQuoteRecord) {
            var record = quotedetailspanel.editQuoteRecord;
			me.loadQuoteDetailsToForm(context,record);
			/*
            dealdetailquotepanel.getForm().loadRecord(record);
            me.loadLoanProperties(record.get('loanid'),context);
            dealdetailquotepanel.getForm().findField('usedInDeal').setValue(record.get('selected'));
			*/
        }
    },

    addPPPBtnClick: function(btn) {
        console.log("PPP Add");
        var me = this;
		var context = btn.up('tab-deal-detail');
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
        var rec = Ext.create('DM2.model.PPP',{
            year:'',
            ppptype: '',
            rate: ''
        });

        //var celledit = me.getContactphonesgridpanel().getPlugin('contactphoneeditingplugin');

        //celledit.cancelEdit();
        dealdetailquotepanel.down('#pppgridpanel').store.insert(0, rec);
        /*celledit.startEditByPosition({
                    row: 0,
                    column: 0
                });*/
    },

    ReceiveQuoteClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this,dealdetailquotepanel;
        //Set the Quote Editor in Active Area
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('dealdetailquotepanel')){
			detailpanel.down('dealdetailquotepanel').destroy();
		}
		if(detailpanel.down('quotedetailspanel')){
			detailpanel.down('quotedetailspanel').destroy();
		}
		if(detailpanel.down('underwriting')){
			detailpanel.down('underwriting').destroy();
		}
		if(detailpanel.down('dealdetailquotepanel')){
			console.log("Use Old Panel");
			dealdetailquotepanel = detailpanel.down('dealdetailquotepanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	dealdetailquotepanel = Ext.create('DM2.view.DealDetailQuotePanel');//me.getDealdetailquotepanel();
		}
        eastregion.getLayout().setActiveItem(dealdetailquotepanel);
		dealdetailquotepanel.setTitle("Activities > "+activitytext);
		
        //Set the QuotePanel Form Values
        //dealdetailquotepanel.getForm().reset();  //Reset The Form
		quotedetailspanel = detailpanel.down('quotedetailspanel');
        quotedetailspanel.getForm().findField('activityid').setRawValue(activityid);
		
		//Load Properties
		var dealProperties = tabdealdetail.down('dealdetailpropertyfieldset').down('grid').getStore().getData();
		quotedetailspanel.down('#quotepropertygridpanel').getStore().loadData(dealProperties.items);
		
        ///Load All Deal Quotes
		var record = tabdealdetail.record;
        var dealid = record.data.idDeal;
		me.loadAllDealQuotes(dealid,tabdealdetail);

        ///Load Submission for Quote
        var filterstarr = [];
        var filterst = 'dealid = '+dealid;
        filterstarr.push(filterst);
        //me.getController('BankController').loadSelBanks(filterstarr,tabdealdetail);
		/*if(h_fname==="SelectLoanClick"){
			dealdetailquotepanel.down('button[action="dealdetailquotesavebtn"]').hide();
			dealdetailquotepanel.down('button[action="dealdetailquoteresetbtn"]').hide();
			dealdetailquotepanel.down('button[action="dealdetailquoteclearbtn"]').hide();
			//dealdetailquotepanel.down('button[itemId="quoteselectbankbtn"]').hide();
		}else{
			dealdetailquotepanel.down('button[action="dealdetailquotesavebtn"]').show();
			dealdetailquotepanel.down('button[action="dealdetailquoteresetbtn"]').show();
			dealdetailquotepanel.down('button[action="dealdetailquoteclearbtn"]').show();
			//dealdetailquotepanel.down('button[itemId="quoteselectbankbtn"]').show();
		}*/
    },

    loadAllDealQuotes: function(dealid,context) {
        console.log("All Quotes Load");
        var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var dealdetailquotepanel = detailpanel.down('dealdetailquotepanel');
		var quotegrid = dealdetailquotepanel.down('quotesgridpanel');
		
        var dealquoteallst = quotegrid.getStore();
        dealquoteallst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);

        var rowindex = 0;
        var quoteForm = dealdetailquotepanel.getForm();
        var idLoan = quoteForm.findField('idLoan').getValue();
		
        if (quotegrid.getSelectionModel().hasSelection()) {
            var selrecord = quotegrid.getSelectionModel().getSelection()[0];
            rowindex = dealquoteallst.indexOf(selrecord);
        }
        //else if(idLoan===0 || idLoan===null || idLoan===""){
        //}
        
		//var tempsubmissionst = dealdetailquotepanel.submissionst;
        dealquoteallst.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){
                if(success){
					var dealquoteallcnt = dealquoteallst.getCount();
					var quotegridtoolbar = quotegrid.down('#quotegridtoolbar');
					console.log("RowIndex"+rowindex);
					if(dealdetailquotepanel.editQuote){
						var tmpselrec = dealdetailquotepanel.editQuoteRecord;
						rowindex = dealquoteallst.find('loanid',tmpselrec.get('loanid'));
						dealdetailquotepanel.editQuote = false;
					}
					if(dealquoteallcnt > 0){
						quotegrid.getSelectionModel().select(rowindex);
						if(quotegridtoolbar){
							quotegridtoolbar.hide();
						}
						dealdetailquotepanel.down('#quotedetailspanel').show();
					} else {
						me.loadLoanTypes(null,context);
						me.loadRateTypes(null,context);
						me.loadMtgTypes(null,context);
						me.addQuoteOptionToMenu(quotegridtoolbar.down('menu'));
						console.log("Submission Loaded");
						dealdetailquotepanel.down('#quotedetailspanel').hide();
					}
                }
            }
        });
    },

	loadMtgTypes: function(mtgTypeVal,context){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var quotedetailspanel = detailpanel.down('quotedetailspanel');
		var quoteForm = quotedetailspanel.getForm();
        var mtgtypeSt = quoteForm.findField('mtgtype').getStore();
		
		var selectionList_st = Ext.getStore('SelectionList');
		
		if(selectionList_st.isLoaded()){
			mtgtypeSt.loadData(selectionList_st.data.items);
			mtgtypeSt.filter("selectionType","MT");
			me.afterMtgTypeLoad(mtgTypeVal,context);
		} else {
			me.loadSelectionList("MT",mtgTypeVal,context);
		}
	},
	afterMtgTypeLoad: function(mtgTypeVal,context){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var quotedetailspanel = detailpanel.down('quotedetailspanel');
		var quoteForm = quotedetailspanel.getForm();
        var mtgtypeSt = quoteForm.findField('mtgtype').getStore();
		
		if(mtgtypeSt.isLoaded()){
			if(mtgTypeVal!=null)
			{
				quoteForm.findField('mtgtype').setRawValue(mtgTypeVal);
			}else{
				mtgtypeSt.each( function(record){
					if(record.get('isDefault')){
						quoteForm.findField('mtgtype').setRawValue(record.get('selectionDesc'));
					}
				});
			}
		}
	},
	
	loadLoanTypes: function(quoteTypeVal,context){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var quotedetailspanel = detailpanel.down('quotedetailspanel');
		var quoteForm = quotedetailspanel.getForm();
        var quoteTypeSt = quoteForm.findField('quoteType').getStore();
		
		var  selectionList_st = Ext.getStore('SelectionList');
		
		if(selectionList_st.isLoaded()){
			quoteTypeSt.loadData(selectionList_st.data.items);
			quoteTypeSt.filter("selectionType","LT");
			me.afterLoanTypeLoad(quoteTypeVal,context);
		}else{
			me.loadSelectionList("LT",quoteTypeVal,context);
		}
	},
	
	afterLoanTypeLoad: function(quoteTypeVal,context){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var quotedetailspanel = detailpanel.down('quotedetailspanel');
		var quoteForm = quotedetailspanel.getForm();
        var quoteTypeSt = quoteForm.findField('quoteType').getStore();
		
		if(quoteTypeVal!=null) {
			quoteForm.findField('quoteType').setRawValue(quoteTypeVal);
		}else{
			quoteTypeSt.each( function(record){
				if(record.get('isDefault')){
					quoteForm.findField('quoteType').setRawValue(record.get('selectionDesc'));
				}
			});
		}
	},
	
	loadSelectionList: function(seltype,quoteTypeVal,context){
		var me = this;
		var  selectionList_st = Ext.getStore('SelectionList');
		selectionList_st.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
		});
		selectionList_st.load({
			callback: function(records, operation, success){
				if(seltype=="LT"){
					me.afterLoanTypeLoad(quoteTypeVal,context);
				}else if(seltype=="RT"){
					me.afterRateTypeLoad(quoteTypeVal,context);
				}else if(seltype=="MT"){
					me.afterMtgTypeLoad(quoteTypeVal,context);
				}
			}
		});
	},
	
	loadRateTypes: function(rateTypeVal,context){
		var me = this;
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var quoteForm = quotedetailspanel.getForm();
        var rateTypeSt = quoteForm.findField('rateType').getStore();
		
		var  selectionList_st = Ext.getStore('SelectionList');
		
		if(selectionList_st.isLoaded()){
			rateTypeSt.loadData(selectionList_st.data.items);
			rateTypeSt.filter("selectionType","RT");
			me.afterRateTypeLoad(rateTypeVal,context);
		}else{
			me.loadSelectionList("RT",rateTypeVal,context);
		}
	},
	
	afterRateTypeLoad: function(rateTypeVal,context){
		var me = this;
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var quoteForm = quotedetailspanel.getForm();
        var rateTypeSt = quoteForm.findField('rateType').getStore();
		
		if(rateTypeVal!=null){
			quoteForm.findField('rateType').setRawValue(rateTypeVal);
		} else {
			rateTypeSt.each( function(record){
				if(record.get('isDefault')){
					quoteForm.findField('rateType').setRawValue(record.get('selectionDesc'));
				}
			});
		}
	},
	
	onQuoteDetailSelFieldChange: function(field, newValue, oldValue, eOpts ){
		console.log("On Loan Size Field Change Occur");
		var me = this;
		console.log(field);
		console.log(newValue);
		if(field.getItemId()=="propertycombo"){
		}else{
			var context = field.up('tab-deal-detail');		
			var quotedetailspanel = context.down('quotedetailspanel');
			quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').enable();
			quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').enable();
			quotedetailspanel.config.isFormDirty = true;
			if(field.up('quoteoptionformpanel')){
				console.log("Options form is dirty");
				quotedetailspanel.config.isOptionFormDirty = true;
				field.up('quoteoptionformpanel').down('button[action="savequoteoptionbtn"]').enable();
			} else {
				console.log("Main Quote form is dirty");
				quotedetailspanel.config.isQuoteFormDirty = true;
			}
		}
	},
	
	onRateTypeFieldChange: function(field, newValue, oldValue, eOpts ){
		var me = this;
		var context = field.up('tab-deal-detail');		
        me.adjustFieldOnRateType(context);		
	},
	
	adjustFieldOnRateType: function(context){
		var me = this;
		console.log("adjustFieldOnRateType Called");
		var quotedetailspanel = context.down('quotedetailspanel');
		var rateTypeVal = quotedetailspanel.down('combobox[name="rateType"]').getValue();
		console.log("rateTypeVal : "+rateTypeVal);
		var spreadfld = quotedetailspanel.down('textfield[name="percentSpread"]');
		var indexfld = quotedetailspanel.down('textfield[name="indexSpread"]');
		var ratefld = quotedetailspanel.down('textfield[name="rate"]');
		/*if(rateTypeVal=="Fixed"){
			spreadfld.hide();
			indexfld.hide();
			ratefld.show();
		}else if(rateTypeVal=="Variable"){
			spreadfld.show();
			indexfld.show();
			ratefld.show();
		}else{
			spreadfld.show();
			indexfld.show();
			ratefld.show();
		}*/
		if(rateTypeVal!=null && rateTypeVal.toUpperCase()=="VARIABLE"){
			ratefld.setReadOnly(true);
		} else {
			ratefld.setReadOnly(false);
		}
		var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
		if(rateTypeVal=="STRUCTURED"){
			structuredgridpanel.show();
			me.loadLoanSchedule(context);
		} else {
			structuredgridpanel.hide();
		}
	},
	
	loadLoanSchedule: function(context){
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
        if (quotedetailspanel.config.loanid) {
			var loanid = quotedetailspanel.config.loanid;
			var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
			var loanSched_st = structuredgridpanel.getStore();
			loanSched_st.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			var filterstarr = [];
			var filterst1 = "idLoan="+loanid;
			filterstarr.push(filterst1);
			/*var filterst2 = "order=orderID ASC";
			filterstarr.push(filterst2);*/
		
			loanSched_st.load({
				params: {
					filter :filterstarr
				},
				scope:this,
				callback: function(records, operation, success){
					if(success){
						console.log("Loaded Loan Scheduled");
						if(loanSched_st.getCount()>0){
							structuredgridpanel.getSelectionModel().select(0);
						} else {
							//me.addQuoteOptionBtnClick();
						}
					}
				}
			});
        }
	},
	
	onQuoteGridItemBeforeSelect: function(selModel, record, index, eOpts){
		var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');		
        var dealdetailquotepanel = context.down('dealdetailquotepanel');
		var quoteeditorbtmpanel = dealdetailquotepanel.down('#quoteeditorbtmpanel');
		console.log(quoteeditorbtmpanel.getLayout().getActiveItem().getItemId());
		if(quoteeditorbtmpanel.getLayout().getActiveItem().getItemId()=="ppplistgrid"){
			return false;	
		}else{
			var quotedetailspanel = dealdetailquotepanel.down('#quotedetailspanel');
			if(quotedetailspanel.config.isFormDirty){
				console.log("Form is dirty.");
				Ext.Msg.alert('Alert!', "You have unsaved changes.Please save it before changing quote.");
				return false;
			}else{
				console.log("Continue");
			}
		}
	},
	
	loadSelectionTypesOnly: function(field,context,stype){
		var me = this;
		var  selectionList_st = Ext.getStore('SelectionList');
        var quoteTypeSt = field.getStore();
		
		if(selectionList_st.isLoaded()){
			quoteTypeSt.loadData(selectionList_st.data.items);
			quoteTypeSt.filter("selectionType",stype);
			//me.afterLoanTypeLoad(quoteTypeVal,context);
		}else{
			//me.loadSelectionList("LT",quoteTypeVal,context);
		}
	},
	
	setSelectionTypeFieldValue: function(field,context,val){
		var me = this;
        var quoteTypeSt = field.getStore();
		
		if(val!=null) {
			field.setRawValue(val);
		} else {
			quoteTypeSt.each( function(record){
				if(record.get('isDefault')){
					field.setRawValue(record.get('selectionDesc'));
				}
			});
		}
	},
	
    selectQuoteItem: function(selModel, record, item, index, e, eOpts) {
        var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		quotedetailspanel.config.loanid = record.get('loanid');
		quotedetailspanel.editQuoteRecord = record;
		me.loadQuoteDetailsToForm(context,record);
    },
	
	loadQuoteDetailsToForm: function(context,record){
		var me = this;
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
        //quotedetailspanel.getForm().loadRecord(record);
		
        if(quotedetailspanel.down('button[action="dealdetailquotesavebtn"]')){
			quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').disable();
		}
		if(quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]')){
			quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').disable();
		}
		quotedetailspanel.config.isFormDirty = false;
		
		quotedetailspanel.config.isOptionFormDirty = false;
		quotedetailspanel.config.isQuoteFormDirty = false;
		
		//Load Properties
		var dealProperties = context.down('dealdetailpropertyfieldset').down('grid').getStore().getData();
		quotedetailspanel.down('#quotepropertygridpanel').getStore().loadData(dealProperties.items);
		
		//me.loadLoanProperties(record.get('loanid'),context);
		me.loadLoanDetails(record.get('loanid'),context);
		if(record.get('fullName')){
			quotedetailspanel.getForm().findField('fullName').setRawValue(record.get('fullName'));
		}
		if(record.get('selected')){
			quotedetailspanel.getForm().findField('usedInDeal').setRawValue(record.get('selected'));
		}
		if(record.get('hasOption')){
			var hasOptionVal = record.get('hasOption');
			///console.log("Check Has Options"+hasOptionVal);
			if(hasOptionVal==true){
				//console.log("Check Has Options");
				quotedetailspanel.getForm().findField('hasOption').setRawValue(true);
			} else if(hasOptionVal==false){
				quotedetailspanel.getForm().findField('hasOption').setRawValue(false);
			}
		}
		me.loadSelectionTypesOnly(quotedetailspanel.getForm().findField('quoteType'),context,"LT");
		me.loadSelectionTypesOnly(quotedetailspanel.getForm().findField('rateType'),context,"RT");
		me.loadMtgTypes(null,context);
		//me.loadLoanOptions(record.get('loanid'),context);
	},

	loadLoanDetails: function(loanid,context) {
        var me = this;
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
        var propertygrid = quotedetailspanel.down('#quotepropertygridpanel');
		propertygrid.enableSaveBtn = false;
        propertygrid.getSelectionModel().deselectAll();

        ///Load the Loan Details from RLoans Resource
		quotedetailspanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'RLoans',
			//url:DM2.view.AppConstants.apiurl+'RLoans1',
            params:{
                filter :"idLoan = '"+loanid+"'"
            },
            scope:this,
            method:'GET',
            success: function(response, opts) {
				quotedetailspanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
				var loanobj = obj[0];
				///For Loan Information Load
				var loanrec = Ext.create('DM2.model.Loan',loanobj);
				//dealdetailquotepanel.getForm().loadRecord(loanrec);
				var termMonthsval = loanrec.get('termMonths');
				var amountQuotedval = loanrec.get('amountQuoted');
				var amountRequestedval = loanrec.get('amountRequested');
				var amortval = loanrec.get('amortization');
				var rateTypeValue = loanrec.get('rateType');
				var quoteTypeValue = loanrec.get('quoteType');
				
				var qtdtfrm = quotedetailspanel.getForm();
				
				//Set Loan Type Value
				if(quoteTypeValue!=null){
					quoteTypeValue = quoteTypeValue.trim();
					quoteTypeValue = quoteTypeValue.toUpperCase();
				}
				me.setSelectionTypeFieldValue(qtdtfrm.findField('quoteType'),context,quoteTypeValue);
				
				//Set Rate Type Value
				if(rateTypeValue!=null){
					rateTypeValue = rateTypeValue.trim();
					rateTypeValue = rateTypeValue.toUpperCase();
				}
				me.setSelectionTypeFieldValue(qtdtfrm.findField('rateType'),context,rateTypeValue);
				
				//qtdtfrm.findField('quoteType').setRawValue(quoteTypeValue);
				qtdtfrm.findField('idLoan').setRawValue(loanrec.get('idLoan'));
				qtdtfrm.findField('Bank_idBank').setRawValue(loanrec.get('idBank'));
				
				qtdtfrm.findField('term').setRawValue(termMonthsval);
				if(termMonthsval!=0 && termMonthsval!=null && termMonthsval!=""){
					qtdtfrm.findField('termyears').setRawValue((termMonthsval/12).toFixed(0));
				}
				
				qtdtfrm.findField('rate').setRawValue(loanrec.get('rateQuoted'));
				
				if (Ext.isEmpty(amountQuotedval))  {
				   qtdtfrm.findField('amountQuoted').setRawValue(amountQuotedval);
				   qtdtfrm.findField('sentamount').setRawValue(amountQuotedval);
			    } else {
					qtdtfrm.findField('amountQuoted').setRawValue(Ext.util.Format.currency(amountQuotedval));
					qtdtfrm.findField('sentamount').setRawValue(Ext.util.Format.currency(amountQuotedval));
			    }
				
				if (Ext.isEmpty(amountRequestedval))  {
				   qtdtfrm.findField('amountRequested').setRawValue(amountRequestedval);
				   qtdtfrm.findField('receivedamount').setRawValue(amountRequestedval);				
			    } else {
					qtdtfrm.findField('amountRequested').setRawValue(Ext.util.Format.currency(amountRequestedval));
					qtdtfrm.findField('receivedamount').setRawValue(Ext.util.Format.currency(amountRequestedval));
			    }
				
				qtdtfrm.findField('amortization').setRawValue(amortval);
				if(amortval!=0 && amortval!=null && amortval!=""){
					qtdtfrm.findField('amortizationyears').setRawValue((amortval/12).toFixed(0));
				}
				
				qtdtfrm.findField('interestOnly').setRawValue(loanrec.get('interestOnly'));
				
				//qtdtfrm.findField('spread').setRawValue(loanrec.get('spread'));
				//qtdtfrm.findField('index').setRawValue(loanrec.get('spreadIndex'));
				
				qtdtfrm.findField('percentSpread').setRawValue(loanrec.get('percentSpread'));
				qtdtfrm.findField('indexSpread').setRawValue(loanrec.get('indexSpread'));
				
				if(loanrec.get('dateRequested')!=null){
					var dtarr = loanrec.get('dateRequested').split('T');
					qtdtfrm.findField('dateRequested').setRawValue(dtarr[0]);
					qtdtfrm.findField('sentdate').setRawValue(dtarr[0]);
				}
				if(loanrec.get('dateReceived')!=null){
					var dtarr = loanrec.get('dateReceived').split('T');
					qtdtfrm.findField('dateReceived').setRawValue(dtarr[0]);
					qtdtfrm.findField('receiveddate').setRawValue(dtarr[0]);
				}
				
				//qtdtfrm.findField('mtgtype').setRawValue(loanrec.get('mtgtype'));
				qtdtfrm.findField('initiationFee').setRawValue(loanrec.get('initiationFee'));
				qtdtfrm.findField('executionType').setRawValue(loanrec.get('executionType'));
				qtdtfrm.findField('dealid').setRawValue(loanrec.get('idDeal'));
				//qtdtfrm.findField('activityid').setRawValue(loanrec.get('idDeal'));
				qtdtfrm.findField('pppValue').setRawValue(loanrec.get('pppDesc'));
				//qtdtfrm.findField('pppOptionWindow').setRawValue(loanrec.get('pppOptionWindow'));
				qtdtfrm.findField('operatingAs').setRawValue(loanrec.get('operatingAs'));
				
				///Set Commitment Data
				qtdtfrm.findField('commitmentNumber').setRawValue(loanrec.get('commitmentNumber'));
				qtdtfrm.findField('commitmentCheck').setRawValue(loanrec.get('commitmentCheck'));
				qtdtfrm.findField('commitmentCheckAmount').setRawValue(loanrec.get('commitmentCheckAmount'));
				if(loanrec.get('commitmentDate')!=null){
					var dtarr = loanrec.get('commitmentDate').split('T');
					var newDate = Ext.Date.parse(dtarr[0], "Y-m-d");
					var sadt = Ext.Date.format(newDate, "m-d-Y");
					qtdtfrm.findField('commitmentDate').setRawValue(sadt);
				}else{qtdtfrm.findField('commitmentDate').setRawValue(null);}
				if(loanrec.get('commitmentCheckDate')!=null){
					var dtarr = loanrec.get('commitmentCheckDate').split('T');
					var newDate = Ext.Date.parse(dtarr[0], "Y-m-d");
					var sadt = Ext.Date.format(newDate, "m-d-Y");
					qtdtfrm.findField('commitmentCheckDate').setRawValue(sadt);
				}else{qtdtfrm.findField('commitmentCheckDate').setRawValue(null);}
				if(loanrec.get('commitExpirationDate')!=null){
					var dtarr = loanrec.get('commitExpirationDate').split('T');
					var newDate = Ext.Date.parse(dtarr[0], "Y-m-d");
					var sadt = Ext.Date.format(newDate, "m-d-Y");
					qtdtfrm.findField('commitExpirationDate').setRawValue(sadt);
				}else{qtdtfrm.findField('commitExpirationDate').setRawValue(null);}
				
				////////////////////////////
				
				///// Set Information Data ////
				var infoPerLoanobj = loanobj.InfoPerLoan;
				if(infoPerLoanobj.length !== 0){
					qtdtfrm.findField('quoteIssuerName').setRawValue(infoPerLoanobj[0].quoteIssuerName);
					qtdtfrm.findField('letterAddresseeName').setRawValue(infoPerLoanobj[0].letterAddresseName);
					qtdtfrm.findField('letterSignedByName').setRawValue(infoPerLoanobj[0].letterSignerName);
				}
				qtdtfrm.findField('quoteIssuer').setRawValue(loanrec.get('quoteIssuer'));				
				qtdtfrm.findField('letterAddressee').setRawValue(loanrec.get('letterAddressee'));				
				qtdtfrm.findField('letterSigner').setRawValue(loanrec.get('letterSigner'));				
				qtdtfrm.findField('specialNote').setRawValue(loanrec.get('specialNote'));
				qtdtfrm.findField('receivedNote').setRawValue(loanrec.get('receivedNote'));
				qtdtfrm.findField('securedLoan').setRawValue(loanrec.get('securedLoan'));
				//////////////////////////////
				
				me.adjustFieldOnRateType(context);
				
				//For Property_has_loan
				var prphasloanobj = loanobj.Property_has_Loan;
				quotedetailspanel.existPropertyObjArr = prphasloanobj;
				
				propertygrid.enableSaveBtn = true;
				
                if(prphasloanobj.length !== 0){
                    propertygrid.getStore().each(function(propertyrecord){
                        console.log (propertyrecord);
                        for(var k=0;k<prphasloanobj.length;k++){
                            if(propertyrecord.get('propertyid')===prphasloanobj[k].idProperty){
								propertygrid.enableSaveBtn = false;
								console.log(propertygrid.enableSaveBtn);
                                propertygrid.getSelectionModel().select(propertyrecord,true);
                            }
                        }
                    });
                }
				/////////////////////
				
				// Start For Loading of Options for Quotes
				var optionsgridpanel = quotedetailspanel.down('quoteoptionspanel').down('grid');
				var optionsst = optionsgridpanel.getStore();
				var OptionsForQuotesList = loanobj.LoanOptionList;
				if(OptionsForQuotesList.length > 0) {
					quotedetailspanel.down('quoteoptionspanel').setCollapsed(false);
					optionsst.loadData(OptionsForQuotesList);
					optionsgridpanel.getSelectionModel().select(0);
					
					/*optionsst.each(function(record){   
						 record.set('amortization',loanrec.get('amortization'));
						 record.set('optionFee',loanrec.get('optionFee'));
					}, this);*/
					
				} else {
					quotedetailspanel.down('quoteoptionspanel').setCollapsed(true);
					optionsst.removeAll();
					me.addQuoteOptionBtnClick();
				}
				// Ends
            },
            failure: function(response, opts) {
				quotedetailspanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Failed', obj.errorMessage);
            }
        });
        ////
    },
	
    loadLoanProperties: function(loanid,context) {
        var me = this;
		console.log("loadLoanProperties");
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
        var propertygrid = dealdetailquotepanel.down('#quotepropertygridpanel');
		propertygrid.enableSaveBtn = false;
        propertygrid.getSelectionModel().deselectAll();

        ///Load the properties_has_loan
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:PropxLoan',
            params:{
                filter :"idLoan = '"+loanid+"'"
            },
            scope:this,
            method:'GET',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
				
				dealdetailquotepanel.existPropertyObjArr = obj;
				
                if(obj.length !== 0)
                {
                    propertygrid.getStore().each(function(propertyrecord){
                        console.log (propertyrecord);
                        for(var k=0;k<obj.length;k++){
                            if(propertyrecord.get('propertyid')===obj[k].idProperty){
                                propertygrid.getSelectionModel().select(propertyrecord,true);
                            }
                        }
                    });
                }
            },
            failure: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Failed', obj.errorMessage);
            }
        });
        ////
    },

    onQuoteGridPanelRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        var me = this;
        me.showQuoteGridMenu(grid,record,e);
    },
	
	showQuoteGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
        console.log("Show Menu for Quote Grid");
        if(this.getQuotemenu()){
            this.getQuotemenu().showAt(e.getXY());
        }
        me.getQuotemenu().removeAll();
        var menuitem = Ext.create('Ext.menu.Item', {
            bankid    : 0,
            text    : 'Add New Quote'
        });
        me.getQuotemenu().add(menuitem);
		/*var tempsubmissionst = grid.up('dealdetailquotepanel').submissionst;
		console.log(tempsubmissionst);
        //Ext.getStore('BanksSelected').each(function(submirec){
	    tempsubmissionst.each(function(submirec){
            console.log (submirec);
            var menuitem = Ext.create('Ext.menu.Item', {
                bankid    : submirec.get('bankid'),
                text    : 'Add Quote for '+submirec.get('shortName'),
                bankfullName:submirec.get('fullName')
            });
            me.getQuotemenu().add(menuitem);
        });*/
        var menutext = "";
        //if(record.get('selected')===false){
		if(record.get('selected')==='Q'){
            menutext = "Select Quote";
        }else if(record.get('selected')==='S'){
            menutext = "UnSelect Quote";
        }else if(record.get('selected')==='P'){
            menutext = "Change to Quote";
        }
        var menuitem = Ext.create('Ext.menu.Item', {
            text    : menutext,
            loanid  : record.get('loanid')
        });
        me.getQuotemenu().add(menuitem);
        var menuitem = Ext.create('Ext.menu.Item', {
            text    : 'Mark activity as done'
        });
        me.getQuotemenu().add(menuitem);
	},
	
	onQuoteGrigItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showQuoteGridMenu(view,record,e);
	},

    quoteMenuClick: function(menu, item, e, eOpts) {
        console.log("Quote Menu Click");
        if(item.text === "Select Quote"){
            console.log("Select Quote");
            this.setSelectUnselectQuote('S',item.loanid);
        }else if(item.text === "UnSelect Quote"){
            console.log("UnSelect Quote");
            this.setSelectUnselectQuote('Q',item.loanid);
        }else if(item.text === "Change to Quote"){
            console.log("Change to Quote");
            this.setSelectUnselectQuote('Q',item.loanid);
        }else if(item.text === "Mark activity as done"){
            console.log("Mark activity as done");
            this.onReceiveQuoteDoneBtnClick();
        }else if(item.text === "Add New Quote"){
            console.log("Add New Quote");
            this.addNewQuoteMenuItemClick();
        }else{
            this.addNewQuoteSubmissionMenuItemClick(item.bankid,item.bankfullName);
        }
    },

    setSelectUnselectQuote: function(usedindealval, loanid) {
        console.log("Save Deal Detail Quote");
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var record = context.record;
        var dealid = record.data.idDeal;
		
        var data = {
            'idLoan' : loanid,
            //"usedInDeal"  : usedindealval,
			"operatingAs"  : usedindealval,
            "@metadata" : {'checksum' : 'override'}
        };

        var methodname = "PUT";
		
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		if(dealdetailquotepanel){
	        var quotesgridpanel = dealdetailquotepanel.down('quotesgridpanel');
		}else{			
			var quotesgridpanel = context.down('dealdetailquotesgridpanel');
		}
		
        quotesgridpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Loan',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                quotesgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
					if(dealdetailquotepanel){
	                    me.loadAllDealQuotes(dealid,context);
					}
					me.loadQuoteDetails(context.down('dealdetailquotesgridpanel'));  //Load First Quote not as loan
                    me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
					me.getController("DealController").loadAllDeals(); //Load All Deals
					me.getController("DealModifyController").loadModifiedRecord(dealid,context);
                }
                else{
                    console.log("Select/UnSelect Quote Failure.");
                    Ext.Msg.alert('Select/UnSelect Quote failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                quotesgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Select/UnSelect Quote', obj.errorMessage);
            }
        });
    },

    onReceiveQuoteDoneBtnClick: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var record = context.record;
        var dealid = record.data.idDeal;
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
        me.getController('DealActivityController').postActivityHistory(dealid, dealdetailquotepanel);
    },

    addQuoteOptionToMenu: function(menu) {
		var me = this;
        menu.removeAll();
        var menuitem = Ext.create('Ext.menu.Item', {
            bankid    : 0,
            text    : 'Add New Quote'
        });
        menu.add(menuitem);
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		var tempsubmissionst = dealdetailquotepanel.submissionst;
		console.log(tempsubmissionst);
		
        //Ext.getStore('BanksSelected').each(function(submirec){
		tempsubmissionst.each(function(submirec){													  
            console.log (submirec);
            var menuitem = Ext.create('Ext.menu.Item', {
                bankid    : submirec.get('bankid'),
                text    : 'Add Quote for '+submirec.get('shortName'),
                bankfullName:submirec.get('fullName')
            });
            menu.add(menuitem);
        });
    },

    addNewQuoteMenuItemClick: function() {
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var tabdealdetail = tabdealpanel.getActiveTab();
		
		var dealdetailquotepanel = tabdealdetail.down('dealdetailsformpanel').down('dealdetailquotepanel');
		 
        var quoteForm = dealdetailquotepanel.getForm();
        //quoteForm.reset();
        //quoteForm.findField('usedInDeal').setValue(false);

        //Deselect the quote
        dealdetailquotepanel.down('quotesgridpanel').getSelectionModel().deselectAll();

        //Deselect Old Selected Property
        var propertygrid = dealdetailquotepanel.down('#quotepropertygridpanel');
		propertygrid.enableSaveBtn = false;
        propertygrid.getSelectionModel().deselectAll();
		me.loadLoanTypes(null,tabdealdetail);
		me.loadRateTypes(null,tabdealdetail);
		me.loadMtgTypes(null,tabdealdetail);
		
		me.addBankToQuote();
    },

    addNewQuoteSubmissionMenuItemClick: function(bankid,bankfullName) {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var tabdealdetail = tabdealpanel.getActiveTab();
		
		var dealdetailquotepanel = tabdealdetail.down('dealdetailsformpanel').down('dealdetailquotepanel');
		
        var quoteForm = dealdetailquotepanel.getForm();
        //quoteForm.reset();
        //quoteForm.findField('usedInDeal').setValue(false);

        ///Populate the bank of the selected submission//
        console.log("Bank iD"+bankid+" "+bankfullName);
        //quoteForm.findField('bank').setValue(bankfullName);
        //quoteForm.findField('Bank_idBank').setValue(bankid);

        //Deselect the quote
        dealdetailquotepanel.down('quotesgridpanel').getSelectionModel().deselectAll();

        //Deselect Old Selected Property
        var propertygrid = dealdetailquotepanel.down('#quotepropertygridpanel');
		propertygrid.enableSaveBtn = false;
        propertygrid.getSelectionModel().deselectAll();
		me.loadLoanTypes(null,tabdealdetail);
		me.loadRateTypes(null,tabdealdetail);
		me.loadMtgTypes(null,tabdealdetail);
		
		me.saveBankToQuote(bankid,tabdealdetail);
    },

    quoteSelectBankBtnClick: function(btn) {
        console.log("Quote Select Bank");
        var me = this;
		//var context = btn.up('tab-deal-detail');
		me.addBankToQuote();		
    },
	
	addBankToQuote: function(){
		var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');		
		var quoteaddbankpanel;
		if(dealdetailquotepanel.down('quoteaddbankpanel')){
			console.log("Use Old Panel");
			quoteaddbankpanel = dealdetailquotepanel.down('quoteaddbankpanel');
		}
		else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	quoteaddbankpanel = Ext.create('DM2.view.QuoteAddBankPanel');//me.getQuoteaddbankpanel();
		}
		var quoteeditorbtmpanel = dealdetailquotepanel.down('#quoteeditorbtmpanel');
        quoteeditorbtmpanel.getLayout().setActiveItem(quoteaddbankpanel);
        /// Load the All Banks ////
        var filterstarr = [];
        me.loadAllBanks(filterstarr,context);
        ////////////
	},
	
	loadAllBanks: function(filterstarr,context){
		var me = this;
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		var quoteaddbankgrid = dealdetailquotepanel.down('quoteaddbankpanel');
        var quoteaddbankst = quoteaddbankgrid.getStore();//('Bankallbuff').getProxy();
		
		var quoteaddbankstproxy = quoteaddbankst.getProxy();
        /*quoteaddbankstproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var quoteaddbankstproxyCfg = quoteaddbankstproxy.config;
        quoteaddbankstproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		quoteaddbankst.setProxy(quoteaddbankstproxyCfg);
        quoteaddbankst.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
            }
        });
	},
	
    closeQuoteAddBankPanel: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');        
        var quoteeditorbtmpanel = dealdetailquotepanel.down('#quoteeditorbtmpanel');
        var quotedetailspanel = dealdetailquotepanel.down('#quotedetailspanel');
        quoteeditorbtmpanel.getLayout().setActiveItem(quotedetailspanel);
    },
	
	quoteAddBankItemDbClick: function(grid, record, item, index, e, eOpts){
		var me = this;
		var context = grid.up('tab-deal-detail');
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		var bankselgrid = dealdetailquotepanel.down('quoteaddbankpanel');
        if (bankselgrid.getSelectionModel().hasSelection()) {
            var row = bankselgrid.getSelectionModel().getSelection()[0];
			var bankid = row.get('idBank');
			me.saveBankToQuote(bankid,context);
        }
	},
	saveQuoteAddBankBtnClick: function(btn){
		var me = this;
		var context = btn.up('tab-deal-detail');
		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		var bankselgrid = dealdetailquotepanel.down('quoteaddbankpanel');
        if (bankselgrid.getSelectionModel().hasSelection()) {
            var row = bankselgrid.getSelectionModel().getSelection()[0];
			var bankid = row.get('idBank');
			me.saveBankToQuote(bankid,context);
        }
	},

    saveBankToQuote: function(bankid,context) {
        var me = this;
		var record = context.record;
        var dealid = record.data.idDeal;

		var dealdetailquotepanel = context.down('dealdetailsformpanel').down('dealdetailquotepanel');
		
        var quoteForm = dealdetailquotepanel.getForm();

		var data = {
			'idDeal' : dealid,
			//'usedInDeal'  : false, //quoteForm.findField('usedInDeal').getValue(),
			'idBank' : bankid,
			//'existingLoan' : false,
			'operatingAs' : 'Q',
			'isSubmitted' : false
		};
		console.log("Add New Loan");
		var methodname = "POST";

        dealdetailquotepanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'RLoans',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                dealdetailquotepanel.setLoading(false);
				me.closeQuoteAddBankPanel();
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
                    //dealdetailquotepanel.getForm().reset();
                    me.loadQuoteDetails(context.down('dealdetailquotesgridpanel'));  //Load First Quote not as loan
                    me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
                    me.loadAllDealQuotes(dealid,context); //Load All Quotes
                    //me.dealdetailquotepanelclose();
                }
                else{
                    console.log("Add/Edit Quote Failure.");
                    Ext.Msg.alert('Quote Save failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                dealdetailquotepanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Quote', obj.errorMessage);
            }
        });
    },
	
    clearDealDetailQuote: function(btn) {
        var me = this;
		var context = btn.up('tab-deal-detail');
        var record = context.record;
        var dealid = record.data.idDeal;
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');	
		
        var quoteForm = quotedetailspanel.getForm();
        var idLoan = quoteForm.findField('idLoan').getValue();

        if (quotedetailspanel.editQuoteRecord) {
            var selrecord = quotedetailspanel.editQuoteRecord;

            var loanid = quoteForm.findField('idLoan').getValue();
            var bankid = quoteForm.findField('Bank_idBank').getValue();
            var bankfullName   = quoteForm.findField('fullName').getValue();
            var activityid = quoteForm.findField('activityid').getValue();
            var dealid     = quoteForm.findField('dealid').getValue();
            var usedInDeal = quoteForm.findField('usedInDeal').getValue();

            quoteForm.reset();

            quoteForm.findField('idLoan').setValue(loanid);
            quoteForm.findField('Bank_idBank').setValue(bankid);
            quoteForm.findField('fullName').setValue(bankfullName);
            quoteForm.findField('activityid').setValue(activityid);
            quoteForm.findField('dealid').setValue(dealid);
            quoteForm.findField('usedInDeal').setValue(usedInDeal);
        } else {
            quoteForm.reset();
        }
    },
	
	addQuoteOptionBtnNewClick: function(btn){
		var me = this;
		console.log("Add Quote Option Btn Click");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var quoteoptionspanel = quotedetailspanel.down('quoteoptionspanel');
		quoteoptionspanel.setCollapsed(false);
		quoteoptionspanel.down('quoteoptionformpanel').show();
		me.addQuoteOptionBtnClick();
	},
	
	addQuoteOptionBtnClick: function(){
		var me = this;
		console.log("Add Quote Option Btn Click");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		
		var quoteoptionformpanel = quotedetailspanel.down('quoteoptionformpanel');
		var quoteoptionspanel = quotedetailspanel.down('quoteoptionspanel').down('grid');
		
		if (quotedetailspanel.config.loanid) {
            //console.log(quotedetailspanel.config.loanid);
            var loanid = quotedetailspanel.config.loanid;		
		
			quoteoptionspanel.getSelectionModel().deselectAll();
			
			quoteoptionformpanel.getForm().reset();
			
			quoteoptionformpanel.setTitle("Add New Option");			
			quoteoptionformpanel.getForm().findField('idLoan').setRawValue(loanid);
			quoteoptionformpanel.getForm().findField('idLoanOption').setRawValue(0);
			
			quoteoptionformpanel.getForm().findField('formmode').setRawValue("add");
			quoteoptionformpanel.down('button[action="savequoteoptionbtn"]').disable();
			
			if(quotedetailspanel.config.isQuoteFormDirty == false){
				quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').disable();
				quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').disable();
			}
			quotedetailspanel.config.isOptionFormDirty = false;
			quotedetailspanel.config.isFormDirty = false;
        } else {
            Ext.Msg.alert('Failed', "Quote is not selected.");
            return false;
        }
	},
	
	closeQuoteOptionFormPanel: function(panel){
		var me = this;
		console.log("Close Quote Option Form Panel");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');        
        /*var quoteeditorbtmpanel = dealdetailquotepanel.down('#quoteeditorbtmpanel');
        var quotedetailspanel = dealdetailquotepanel.down('#quotedetailspanel');
        quoteeditorbtmpanel.getLayout().setActiveItem(quotedetailspanel);*/
	},
	
	onSaveQuoteOptionBtnClickCopy: function(btn){
		var me = this;
		console.log("Save Quote Options");
		var context = btn.up('tab-deal-detail');
		var record = context.record;
        var dealid = record.data.idDeal;

		var quoteoptionformpanel = context.down('dealdetailsformpanel').down('quoteoptionformpanel');
		var optionsgridpanel = context.down('dealdetailsformpanel').down('quoteoptionspanel').down('grid');		
		var optionsSt = optionsgridpanel.getStore();
		
        var quoteForm = quoteoptionformpanel.getForm();
        var idLoanOption = quoteForm.findField('idLoanOption').getValue();
		
		var formmodeval = quoteoptionformpanel.getForm().findField('formmode').getValue();
		var optAmountval = quoteoptionformpanel.getForm().findField('optAmount').getValue();
		var rateTypeval = quoteoptionformpanel.getForm().findField('rateType').getValue();
		var quoteRateval = Number(quoteoptionformpanel.getForm().findField('quoteRate').getValue());
		var termval = quoteoptionformpanel.getForm().findField('term').getValue();
		var indexSpreadval = quoteoptionformpanel.getForm().findField('indexSpread').getValue();
		var percentSpreadval = quoteoptionformpanel.getForm().findField('percentSpread').getValue();
		var interestOnlyval = quoteoptionformpanel.getForm().findField('interestOnly').getValue();
		var optionFloorval = quoteoptionformpanel.getForm().findField('optionFloor').getValue();
		var monthEndval    = Number(quoteoptionformpanel.getForm().findField('monthEnd').getValue());
		
		var data = {
			'idLoan'    : quoteForm.findField('idLoan').getValue(),
			'optAmount' : optAmountval,
			'rateType'    : rateTypeval,
			'quoteRate'   : quoteRateval,
			'term'        : termval,
			'indexSpread'   : indexSpreadval,
			'percentSpread' : percentSpreadval,
			'interestOnly'  : interestOnlyval,
			'optionFloor'   : optionFloorval,
			//'startDate': quoteForm.findField('startDate').getValue(),
			'monthEnd': monthEndval
		};
        if(idLoanOption===0 || idLoanOption===null || idLoanOption==="") {
            console.log("Add New Loan");
            //var methodname = "POST";
			data['idLoanOption'] = null;
        }
        else {
            console.log("Save Loan");
            data['idLoanOption'] = idLoanOption;
            //data['@metadata'] = {'checksum' : 'override'};
            //var methodname = "PUT";			
        }
		
		if(formmodeval==="add") {
			var rec = Ext.create('DM2.model.QuoteOption',data);
				
			var insertnum = optionsSt.getCount();
    	    optionsSt.insert(insertnum, rec);
			optionsgridpanel.getSelectionModel().select(insertnum,true);
		} else {
            var row = optionsgridpanel.getSelectionModel().getSelection()[0];
            row.set('optAmount',optAmountval,true);
            row.set('rateType',rateTypeval,true);
            row.set('quoteRate',quoteRateval,true);
			row.set('term',termval,true);
			row.set('indexSpread',indexSpreadval,true);
			row.set('percentSpread',percentSpreadval,true);
			row.set('interestOnly',interestOnlyval,true);
			row.set('optionFloor',optionFloorval,true);
			row.set('monthEnd',monthEndval,true);
		}
	},
	
	onSaveQuoteOptionBtnClick: function(btn){
		var me = this;
		console.log("Save Quote Options");
		var context = btn.up('tab-deal-detail');
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');        

		var quoteoptionformpanel = context.down('dealdetailsformpanel').down('quoteoptionformpanel');
		
        var quoteForm = quoteoptionformpanel.getForm();
        var idLoanOption = quoteForm.findField('idLoanOption').getValue();
		var data = {
			'idLoan'    : quoteForm.findField('idLoan').getValue(),
			'optAmount' : quoteForm.findField('optAmount').getValue(),
			'rateType'    : quoteForm.findField('rateType').getValue(),
			'quoteRate'   : Number(quoteForm.findField('quoteRate').getValue()),
			'term'        : quoteForm.findField('term').getValue(),
			'indexSpread'        : quoteForm.findField('indexSpreadOption').getValue(),
			'percentSpread' : quoteForm.findField('percentSpread').getValue(),
			'interestOnly'  : quoteForm.findField('interestOnly').getValue(),
			'optionFloor'   : quoteForm.findField('optionFloor').getValue(),
			'startDate': quoteForm.findField('startDate').getValue(),
			'monthEnd': Number(quoteForm.findField('monthEnd').getValue()),
			'amortizationMonth' : quoteForm.findField('amortizationMonth').getValue(),
			'optionSelected' : quoteForm.findField('optionSelected').getValue(),
			'renewFeePercent' : quoteForm.findField('renewFeePercent').getValue(),
			'renewFee' : quoteForm.findField('renewFee').getValue(),
			'pppDescription' : quoteForm.findField('pppDescription').getValue(),
			'pppWindow' : quoteForm.findField('pppWindow').getValue(),
			'optionDesc' : Number(quoteForm.findField('optionDesc').getValue()),
			'descRecord' : Number(quoteForm.findField('descRecord').getValue())
		};
        if(idLoanOption===0 || idLoanOption===null || idLoanOption==="") {
            console.log("Add New Loan");
            var methodname = "POST";			
        } else {
            console.log("Save Loan");
            data['idLoanOption'] = idLoanOption;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";			
        }

        quoteoptionformpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
			url:DM2.view.AppConstants.apiurl+'mssql:LoanOption',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                quoteoptionformpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201) {
					if(idLoanOption===0 || idLoanOption===null || idLoanOption==="") {
						console.log("Added New Option");
						quotedetailspanel.config.reloadQuoteTabData = true;			
					}
					//me.savePropertyHasLoan(context,idLoan);
					if(quotedetailspanel.config.isQuoteFormDirty == false){
						quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').disable();
						quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').disable();
						
						quotedetailspanel.config.isFormDirty = false;
						quotedetailspanel.config.isOptionFormDirty = false;
					}
					me.loadLoanOptions(quoteForm.findField('idLoan').getValue(),context);
					me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
					//quoteoptionformpanel.getForm().reset();
					//me.closeQuoteOptionFormPanel(quoteoptionformpanel);
                } else {
                    console.log("Add/Edit Quote Option Save Failure.");
                    Ext.Msg.alert('Quote Option Save failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                quoteoptionformpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Quote', obj.errorMessage);
            }
        });
	},
	
	loadLoanOptions: function(loanid,context) {
        var me = this;
		console.log("Load Loan Options");
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var optionsgridpanel = quotedetailspanel.down('quoteoptionspanel').down('grid');
		
        var optionsgridst = optionsgridpanel.getStore();
        optionsgridst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        var filterstarr = [];
        filterstarr.push('idLoan = '+loanid);

        optionsgridst.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){
                if(success){
					if(optionsgridst.getCount()>0){
						optionsgridpanel.getSelectionModel().select(0);
					} else {
						me.addQuoteOptionBtnClick();
					}
					//me.checkAndLoadSubLoanOptions(loanid,optionsgridst.data.items);
					me.checkAndLoadSubLoanOptions(loanid,context);
				}
			}
		});
	},
	
	//checkAndLoadSubLoanOptions: function(loanid,optiondata){
	checkAndLoadSubLoanOptions: function(loanid,context){
		var me = this;
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var targetId = 'optiongridrow-' + loanid;
		if(Ext.getCmp(targetId + '_grid')) {		
			var nestedGrid = Ext.getCmp(targetId + '_grid');
			var optionstproxy = nestedGrid.getStore().getProxy();
			optionstproxy.setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			nestedGrid.getStore().load({
				params:{filter :'idLoan = '+loanid},
				callback: function(records, operation, success){
					if(success){
						if(nestedGrid.getStore().getCount()==0){
							Ext.getCmp(targetId + '_grid').destroy();
							///Refresh Quote Tab to not show + sign of quote for no options
							me.loadQuoteDetails(context.down('dealdetailquotesgridpanel'));
							console.log("Refresh Quote Tab to not show + sign of quote for no options");
						}
					}
				}
			});
			//nestedGrid.getStore().loadDate(optiondata);
		} else if(quotedetailspanel.config.reloadQuoteTabData==true){
			///Refresh Quote Tab to show + sign of quote for first options
			console.log("Refresh Quote Tab to show + sign of quote for first options");
			me.loadQuoteDetails(context.down('dealdetailquotesgridpanel'));
			quotedetailspanel.config.reloadQuoteTabData = false;
		}
	},
	
	onOptionsGridPanelRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        var me = this;
        e.stopEvent();
        console.log("Show Menu for Quote Options Grid");
        if(this.getQuoteoptionsmenu()){
            this.getQuoteoptionsmenu().showAt(e.getXY());
        }
    },
	
	quoteOptionsMenuClick: function(menu, item, e, eOpts) {
        console.log("Quote Options Menu Click");
        if(item.text === "Edit Option"){
            console.log("Edit Option");
            this.editOptionMenuClick();
        }else if(item.text === "Delete Option"){
            console.log("Delete Option");
            this.deleteOptionMenuClick();
        }
    },
	
	deleteOptionMenuClick: function(){
		var me = this,idLoanOption,loanid;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var optionsgridpanel = quotedetailspanel.down('quoteoptionspanel').down('grid');
		
		if (optionsgridpanel.getSelectionModel().hasSelection()) {
            var row = optionsgridpanel.getSelectionModel().getSelection()[0];
            console.log(row.get('idLoanOption'));
            idLoanOption = row.get('idLoanOption');
			loanid = row.get('idLoan');
        } else {
            Ext.Msg.alert('Failed', "Quote is not selected.");
            return false;
        }

        Ext.Msg.show({
            title:'Delete Quote Option?',
            message: 'Are you sure you want to delete this Quote Option?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    optionsgridpanel.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:LoanOption/'+idLoanOption+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            optionsgridpanel.setLoading(false);
                            console.log("Removed Quote Option.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200) {
                                me.loadLoanOptions(loanid,context);
								me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
                            } else {
                                console.log("Delete Quote Option Failure.");
                                Ext.Msg.alert('Delete Quote Option failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            optionsgridpanel.setLoading(false);
                            console.log("Delete Quote Option Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Quote Option failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
	},
	
	editOptionMenuClick: function(){
		var me = this;
		console.log("Edit Quote Option Btn Click");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');		
		var quoteoptionformpanel = quotedetailspanel.down('quoteoptionformpanel');
		
		///Load Quote Option Data to form
		var optionsgridpanel = quotedetailspanel.down('quoteoptionspanel').down('grid');
		
		if (optionsgridpanel.getSelectionModel().hasSelection()) {
            var record = optionsgridpanel.getSelectionModel().getSelection()[0];
            console.log(record.get('idLoanOption'));
            //idLoanOption = row.get('idLoanOption');
			//loanid = row.get('idLoan');
			//quoteoptionformpanel.getForm().loadRecord(row);
			quoteoptionformpanel.getForm().findField('idLoanOption').setRawValue(record.get('idLoanOption'));
			quoteoptionformpanel.getForm().findField('idLoan').setRawValue(record.get('idLoan'));
			if (Ext.isEmpty(record.get('optAmount')))  {
			   quoteoptionformpanel.getForm().findField('optAmount').setRawValue(record.get('optAmount'));
			}else{
				quoteoptionformpanel.getForm().findField('optAmount').setRawValue(Ext.util.Format.currency(record.get('optAmount')));
			}
			
			quoteoptionformpanel.getForm().findField('quoteRate').setRawValue(record.get('quoteRate'));
			quoteoptionformpanel.getForm().findField('indexSpreadOption').setRawValue(record.get('indexSpread'));
			quoteoptionformpanel.getForm().findField('optionFloor').setRawValue(record.get('optionFloor'));
			quoteoptionformpanel.getForm().findField('rateType').setRawValue(record.get('rateType'));
			quoteoptionformpanel.getForm().findField('term').setRawValue(record.get('term'));
			quoteoptionformpanel.getForm().findField('percentSpread').setRawValue(record.get('percentSpread'));
			quoteoptionformpanel.getForm().findField('interestOnly').setRawValue(record.get('interestOnly'));
			quoteoptionformpanel.getForm().findField('monthEnd').setRawValue(record.get('monthEnd'));
			quoteoptionformpanel.getForm().findField('amortizationMonth').setRawValue(record.get('amortizationMonth'));
			quoteoptionformpanel.getForm().findField('optionSelected').setRawValue(record.get('optionSelected'));
			quoteoptionformpanel.getForm().findField('renewFee').setRawValue(record.get('renewFee'));
			quoteoptionformpanel.getForm().findField('renewFeePercent').setRawValue(record.get('renewFeePercent'));
			quoteoptionformpanel.getForm().findField('pppDescription').setRawValue(record.get('pppDescription'));
			quoteoptionformpanel.getForm().findField('pppWindow').setRawValue(record.get('pppWindow'));
			quoteoptionformpanel.getForm().findField('optionDesc').setRawValue(record.get('optionDesc'));
			quoteoptionformpanel.getForm().findField('descRecord').setRawValue(record.get('descRecord'));
			quoteoptionformpanel.getForm().findField('formmode').setRawValue("edit");
			
			if(record.get('startDate')!=null) {
				var dtarr = record.get('startDate').split('T');
				quoteoptionformpanel.getForm().findField('startDate').setRawValue(dtarr[0]);
			}
			quoteoptionformpanel.setTitle("Edit Option Details");
			quoteoptionformpanel.down('button[action="savequoteoptionbtn"]').disable();
			quoteoptionformpanel.show();
        }
        else {
            Ext.Msg.alert('Failed', "Quote Option is not selected.");
            return false;
        }		
	},
	
	onQuoteGridViewExpandbody: function(rowNode, record, expandRow, eOpts){
		console.log("Quote Row Expanded.");
        var me = this;
		if(record.get('hasOption')){
			//var readable = record.get('readable');
			//var modifiable = record.get('modifiable');
			var loanid = record.get('loanid');
			/*if(readable==0){
				return false;
				//me.getController('DealController').dealPermissionNotAllowed();
			}else{*/
				var targetId = 'optiongridrow-' + loanid;
				if(Ext.getCmp(targetId + '_grid')){
					Ext.getCmp(targetId + '_grid').destroy();
				}
				var nestedGrid = Ext.getCmp(targetId + '_grid');
				if (!nestedGrid) {
					nestedGrid = Ext.create('DM2.view.QuoteOptionsGridPanel', {
						renderTo: targetId,
						id: targetId + '_grid'/*,
						cls:'dealmenupropertyloancls'*/
					});
					rowNode.grid = nestedGrid;
					// prevent bubbling of the events
					nestedGrid.getEl().swallowEvent([
						'mousedown', 'mouseup', 'click',
						'contextmenu', 'mouseover', 'mouseout',
						'dblclick', 'mousemove'
					]);
				}
				
				var optionstproxy = nestedGrid.getStore().getProxy();
				optionstproxy.setHeaders({
					Accept: 'application/json',
					Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
				});
				nestedGrid.getStore().load({params:{filter :'idLoan = '+loanid}});
			//}
		}else{
			return false;
		}
	},
	
	onOptionsGridpanelItemSelect: function(grid, record, index, eOpts){
		var me = this;
		var context = grid.view.ownerCt.up('tab-deal-detail');	
					
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');		
		var quoteoptionformpanel = quotedetailspanel.down('quoteoptionformpanel');
		var optionsgridpanel = quotedetailspanel.down('quoteoptionspanel').down('grid');
		
		//quoteoptionformpanel.getForm().loadRecord(record);
		
		quoteoptionformpanel.getForm().findField('idLoanOption').setRawValue(record.get('idLoanOption'));
		quoteoptionformpanel.getForm().findField('idLoan').setRawValue(record.get('idLoan'));
		if (Ext.isEmpty(record.get('optAmount')))  {
		   quoteoptionformpanel.getForm().findField('optAmount').setRawValue(record.get('optAmount'));
		} else {
			quoteoptionformpanel.getForm().findField('optAmount').setRawValue(Ext.util.Format.currency(record.get('optAmount')));
		}
		quoteoptionformpanel.getForm().findField('quoteRate').setRawValue(record.get('quoteRate'));
		quoteoptionformpanel.getForm().findField('indexSpreadOption').setRawValue(record.get('indexSpread'));
		quoteoptionformpanel.getForm().findField('optionFloor').setRawValue(record.get('optionFloor'));
		quoteoptionformpanel.getForm().findField('rateType').setRawValue(record.get('rateType'));
		quoteoptionformpanel.getForm().findField('term').setRawValue(record.get('term'));
		quoteoptionformpanel.getForm().findField('percentSpread').setRawValue(record.get('percentSpread'));
		quoteoptionformpanel.getForm().findField('interestOnly').setRawValue(record.get('interestOnly'));
		quoteoptionformpanel.getForm().findField('monthEnd').setRawValue(record.get('monthEnd'));
		quoteoptionformpanel.getForm().findField('amortizationMonth').setRawValue(record.get('amortizationMonth'));
		quoteoptionformpanel.getForm().findField('optionSelected').setRawValue(record.get('optionSelected'));
		quoteoptionformpanel.getForm().findField('renewFee').setRawValue(record.get('renewFee'));
		quoteoptionformpanel.getForm().findField('renewFeePercent').setRawValue(record.get('renewFeePercent'));
		quoteoptionformpanel.getForm().findField('pppDescription').setRawValue(record.get('pppDescription'));
		quoteoptionformpanel.getForm().findField('pppWindow').setRawValue(record.get('pppWindow'));
		quoteoptionformpanel.getForm().findField('optionDesc').setRawValue(record.get('optionDesc'));
		quoteoptionformpanel.getForm().findField('descRecord').setRawValue(record.get('descRecord'));
		
		if(record.get('startDate')!=null) {
        	var dtarr = record.get('startDate').split('T');
			quoteoptionformpanel.getForm().findField('startDate').setRawValue(dtarr[0]);
			//quoteoptionformpanel.getForm().findField('startDate').setRawValue(new Date(record.get('startDate')));
		}
		
		quoteoptionformpanel.setTitle("Edit Option Details");
		quoteoptionformpanel.getForm().findField('formmode').setRawValue("edit");
		quoteoptionformpanel.down('button[action="savequoteoptionbtn"]').disable();
	},
	
	onAddLoanSchedBtnClick: function(){
		var me = this;
		console.log("Add Loan Sched Btn Click");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
		structuredgridpanel.setCollapsed(false);
		structuredgridpanel.down('#structuredschedform').show();
		me.addLoanSchedBtnClick();
	},
	
	addLoanSchedBtnClick: function(){
		var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		
		var structuredschedform = quotedetailspanel.down('#structuredschedform');
		var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
		
		if (quotedetailspanel.config.loanid) {
            //console.log(quotedetailspanel.config.loanid);
            var loanid = quotedetailspanel.config.loanid;		
		
			structuredgridpanel.getSelectionModel().deselectAll();
			
			structuredschedform.getForm().reset();
			
			structuredschedform.setTitle("Add Structured Rate Schedule");			
			//structuredschedform.getForm().findField('idLoan').setRawValue(loanid);
			structuredschedform.getForm().findField('idLoanSched').setRawValue(0);
			
			//structuredschedform.getForm().findField('formmode').setRawValue("add");
			//structuredschedform.down('button[action="saveloanschedbtn"]').disable();
			
			/*if(quotedetailspanel.config.isQuoteFormDirty == false){
				quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').disable();
				quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').disable();
			}
			quotedetailspanel.config.isOptionFormDirty = false;
			quotedetailspanel.config.isFormDirty = false;*/
        } else {
            Ext.Msg.alert('Failed', "Quote is not selected.");
            return false;
        }
	},
	
	onSaveLoanSchedBtnClick: function(btn){
		var me = this;
		console.log("Save Loan Schedule Structure");
		var context = btn.up('tab-deal-detail');
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');        
		
		var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
		var structuredschedform = context.down('dealdetailsformpanel').down('#structuredschedform');		

		var loanSched_st = structuredgridpanel.getStore();
		var cnt = loanSched_st.getCount();
		var orderID,loanid;
		if(cnt==0){
			orderID = 1;
		} else {
			orderID = loanSched_st.getAt(cnt-1).get('orderID')+1;
		}
		
        if (quotedetailspanel.config.loanid) {
			loanid  = quotedetailspanel.config.loanid;
		}
		
        var loanschedForm = structuredschedform.getForm();
        var idLoanSched = loanschedForm.findField('idLoanSched').getValue();
		var data = {
			'idLoan'    : loanid,
			'months'    : loanschedForm.findField('months').getValue(),
			'rate'      : loanschedForm.findField('rate').getValue()
			//'quoteRate'   : Number(quoteForm.findField('quoteRate').getValue())
		};
        if(idLoanSched===0 || idLoanSched===null || idLoanSched==="") {
            console.log("Add New Loan Schedule");
            var methodname = "POST";
			data['orderID'] = orderID;
        } else {
            console.log("Save Loan Schedule");
            data['idLoanSched'] = idLoanSched;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";			
        }

        structuredschedform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
			url:DM2.view.AppConstants.apiurl+'mssql:LoanSchedule',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                structuredschedform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201) {
					if(idLoanSched===0 || idLoanSched===null || idLoanSched==="") {
						console.log("Added New Option");
						//dealdetailquotepanel.config.reloadQuoteTabData = true;			
					}
					/*if(quotedetailspanel.config.isQuoteFormDirty == false){
						quotedetailspanel.down('button[action="dealdetailquotesavebtn"]').disable();
						quotedetailspanel.down('button[action="dealdetailquoteresetbtn"]').disable();
						
						quotedetailspanel.config.isFormDirty = false;
						quotedetailspanel.config.isOptionFormDirty = false;
					}*/
					me.loadLoanSchedule(context);
                } else {
                    console.log("Add/Edit Loan Schedule Option Save Failure.");
                    Ext.Msg.alert('Loan Schedule Save failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                structuredschedform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Loan Schedule', obj.errorMessage);
            }
        });
	},
	
	onStructuredGridPanelItemSelect: function(grid, record, index, eOpts){
		var me = this;
		var context = grid.view.ownerCt.up('tab-deal-detail');	
					
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');		
		var structuredschedform = quotedetailspanel.down('#structuredschedform');
		
		//structuredschedform.getForm().loadRecord(record);		
		structuredschedform.getForm().findField('idLoanSched').setRawValue(record.get('idLoanSched'));
		//structuredschedform.getForm().findField('idLoan').setRawValue(record.get('idLoan'));
		structuredschedform.getForm().findField('months').setRawValue(record.get('months'));
		structuredschedform.getForm().findField('rate').setRawValue(record.get('rate'));
		
		structuredschedform.setTitle("Edit Structured Rate Schedule");
		//structuredschedform.down('button[action="saveloanschedbtn"]').disable();
	},
	
	onStructuredGridPanelRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        var me = this;
        e.stopEvent();
        console.log("Show Menu for Loan Schedule Grid");
        if(this.getLoanschedulemenu()){
            this.getLoanschedulemenu().showAt(e.getXY());
        }
    },
	
	loanScheduleMenuClick: function(menu, item, e, eOpts) {
        console.log("Loan Schedule Menu Click");
        if(item.action === "editrateschedule"){
            console.log("Edit Rate Schedule");
            this.editRateScheduleMenuClick();
        } else if(item.action === "deleterateschedule"){
            console.log("Delete Rate Schedule");
            this.deleteRateScheduleMenuClick();
        } else if(item.action === "deleteallrateschedule"){
            console.log("Delete All Rate Schedule");
            this.deleteAllRateScheduleMenuClick();
        }
    },
	
	deleteRateScheduleMenuClick: function(){
		var me = this,idLoanOption,loanid;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
		var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
		
		if (structuredgridpanel.getSelectionModel().hasSelection()) {
            var row = structuredgridpanel.getSelectionModel().getSelection()[0];
            console.log(row.get('idLoanSched'));
            idLoanSched = row.get('idLoanSched');
			loanid = row.get('idLoan');
        } else {
            Ext.Msg.alert('Failed', "Rate is not selected.");
            return false;
        }

        Ext.Msg.show({
            title:'Delete Rate Schedule?',
            message: 'Are you sure you want to delete this Rate Schedule?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    structuredgridpanel.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:LoanSchedule/'+idLoanSched+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            structuredgridpanel.setLoading(false);
                            console.log("Removed Rate Schedule.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200) {
                                me.loadLoanSchedule(context);
								//me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
                            } else {
                                console.log("Delete Rate Schedule Failure.");
                                Ext.Msg.alert('Delete Rate Schedule failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            structuredgridpanel.setLoading(false);
                            console.log("Delete Rate Schedule Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Rate Schedule failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
	},
	
	editRateScheduleMenuClick: function(){
		var me = this;
		console.log("Edit Rate Schedule Btn Click");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');		
		var structuredschedform = quotedetailspanel.down('#structuredschedform');
		
		///Load Quote Option Data to form
		var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
		
		if (structuredgridpanel.getSelectionModel().hasSelection()) {
            var record = structuredgridpanel.getSelectionModel().getSelection()[0];
            console.log(record.get('idLoanSched'));
            //idLoanOption = row.get('idLoanOption');
			//loanid = row.get('idLoan');
			//quoteoptionformpanel.getForm().loadRecord(row);
			structuredschedform.getForm().findField('idLoanSched').setRawValue(record.get('idLoanSched'));
			//structuredschedform.getForm().findField('idLoan').setRawValue(record.get('idLoan'));
			structuredschedform.getForm().findField('months').setRawValue(record.get('months'));
			structuredschedform.getForm().findField('rate').setRawValue(record.get('rate'));
			
			structuredschedform.setTitle("Edit Structured Rate Schedule");			
			//structuredschedform.down('button[action="saveloanschedbtn"]').disable();
			structuredschedform.show();
        } else {
            Ext.Msg.alert('Failed', "Rate Schedule is not selected.");
            return false;
        }		
	},
	
	deleteAllRateScheduleMenuClick: function(){
		var me = this;
		console.log("Delete All Rate Schedule Btn Click");
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');		
		var structuredgridpanel = quotedetailspanel.down('structuredgridpanel');
		var loanSched_st = structuredgridpanel.getStore();
		
		var loanschedarr = [];
		for(var i=0;i<loanSched_st.getCount();i++){
			var recitem = loanSched_st.getAt(i);
			var Loan_Schedule_item = {
				"@metadata": {
				  "entity": "LoanSchedule",
				  "action": "DELETE",
				  "checksum": "override"
				},
				"idLoanSched": recitem.get('idLoanSched')
			};
			loanschedarr.push(Loan_Schedule_item);
		}
		if(loanschedarr.length>0){
			Ext.Msg.show({
				title:'Delete All Rate Schedule?',
				message: 'Are you sure you want to delete all Rate Schedule?',
				buttons: Ext.Msg.YESNO,
				icon: Ext.Msg.QUESTION,
				fn: function(btn) {
					if (btn === 'yes') {
						structuredgridpanel.setLoading(true);
						Ext.Ajax.request({
							headers: {
								'Content-Type': 'application/json',
								Accept: 'application/json',
								Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
							},
							method:'PUT', //Delete Method with primary key
							params: Ext.util.JSON.encode(loanschedarr),
							url:DM2.view.AppConstants.apiurl+'mssql:LoanSchedule',
							scope:this,
							success: function(response, opts) {
								structuredgridpanel.setLoading(false);
								console.log("Removed Rate Schedule.");
								var obj = Ext.decode(response.responseText);
								if(obj.statusCode == 200) {
									me.loadLoanSchedule(context);
									//me.getController('DealDetailController').loadNewLoanDetails(context.down('dealdetailnewloanfieldset'));
								} else {
									console.log("Delete All Rate Schedule Failure.");
									Ext.Msg.alert('Delete All Rate Schedule failed', obj.errorMessage);
								}
							},
							failure: function(response, opts) {
								structuredgridpanel.setLoading(false);
								console.log("Delete All Rate Schedule Failure.");
								var obj = Ext.decode(response.responseText);
								Ext.Msg.alert('Delete All Rate Schedule failed', obj.errorMessage);
							}
						});
					}
				}
			});
		}
	},
	
	callLoanEditor: function(record) {
        var me = this;
		if(record!=null){
			var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
			var context = tabdealpanel.getActiveTab();
			//Set the Quote Editor in Active Area		
			var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			var quotedetailspanel;
			if(context.down('dealdetailsformpanel').down('quotedetailspanel')){
				context.down('dealdetailsformpanel').down('quotedetailspanel').destroy();
			}
			if(context.down('dealdetailsformpanel').down('quotedetailspanel')){
				context.down('dealdetailsformpanel').down('quotedetailspanel').destroy();
			}
			if(context.down('dealdetailsformpanel').down('underwriting')){
				context.down('dealdetailsformpanel').down('underwriting').destroy();
			}
			if(context.down('dealdetailsformpanel').down('quotedetailspanel')){
				console.log("Use Old Panel");
				quotedetailspanel = context.down('dealdetailsformpanel').down('quotedetailspanel');
			} else {
				//create new Activity Detail Panel
				console.log("Create New Panel");
				quotedetailspanel = Ext.create('DM2.view.QuoteDetailsPanel',{
										closable : true,
										frame : true,
										margin : 3,
										ui : 'activitypanel'
									});
			}
			eastregion.getLayout().setActiveItem(quotedetailspanel);
			var title = "Loan Editor";
			quotedetailspanel.setTitle(title);
			
			quotedetailspanel.editQuote = true;
			quotedetailspanel.config.loanid = record.get('loanid');
			quotedetailspanel.editQuoteRecord = record;
		
			me.loadQuoteDetailsToForm(context,record);
			///Set all section collapsed excluding loan details and properties.
			quotedetailspanel.down('quoteoptionspanel').setCollapsed(true);
			quotedetailspanel.down('underwriting').setCollapsed(true);
			quotedetailspanel.down('#commitment').setCollapsed(true);
			quotedetailspanel.down('#information').setCollapsed(true);
			quotedetailspanel.down('#deliverydetails').setCollapsed(true);
			//quotedetailspanel.down('#bankinfo').setCollapsed(true);
		}
    },
	
	quoteDetailsFormPanelClose: function(panel, eOpts) {
		var me = this;
		var context = panel.up('tab-deal-detail');
        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },
	
	showContactPickerList: function(btn,callfrom){
		var me = this,backview = "",underwriting;
		console.log("Show Contact Picker List Dialog Box : "+callfrom);
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
				
		if(btn.up('dealdetailquotepanel')){
			if(callfrom=="quotedby"){
				backview = "quotedbyloanmanager";
			} else if(callfrom=="letterAddressed") {
				backview = "letterAddrloanmanager";
			}
		} else if(btn.up('quotedetailspanel')){
			if(callfrom=="quotedby"){
				backview = "quotedbyloaneditor";
			} else if(callfrom=="letterAddressed") {
				backview = "letterAddrloaneditor";
			}
		}
		
		var eastregion = detailpanel.down('[region=east]');		
		if(detailpanel.down('adddealcontactform')){
			adddealcontactform = detailpanel.down('adddealcontactform');
		} else {
			//create new Activity Detail Panel
			adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
		}
		eastregion.getLayout().setActiveItem(adddealcontactform);
						
		//adddealcontactform.config.utactionfrm = actionfrm;
		adddealcontactform.config.backview = backview;
		
		///Load Contacts
		var fullNameVal = btn.up('quotedetailspanel').getForm().findField('fullName').getValue();
		var filterstarr = [];
		if(fullNameVal!==''){
			var filterst = "companyName = '"+fullNameVal.toUpperCase()+"'";
			filterstarr.push(filterst);
			fullNameVal = fullNameVal.toUpperCase();
		}
		me.getController('UnderwritingController').loadUwSelContacts(fullNameVal,adddealcontactform,'companyName');		
	},
	
	showDealUserPickerList: function(btn,callfrom){
		var me = this,backview = "",dealuserpickerpanel;
		console.log("Show User Picker List Dialog Box : "+callfrom);
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
				
		if(btn.up('dealdetailquotepanel')){
			backview = "letterSignedByloanmanager";
		} else if(btn.up('quotedetailspanel')){
			backview = "letterSignedByloaneditor";
		}
		
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('dealuserpickerpanel')){
			console.log("Use Old Panel");
			dealuserpickerpanel = detailpanel.down('dealuserpickerpanel');
		} else {
			console.log("Create New Panel");
			dealuserpickerpanel = Ext.create('DM2.view.DealUserPickerPanel');
		}
		eastregion.getLayout().setActiveItem(dealuserpickerpanel);					
		//dealuserpickerpanel.config.utactionfrm = actionfrm;
		dealuserpickerpanel.config.backview = backview;

		var record = context.record;
        var dealid = record.data.idDeal;

		///Load Contacts
		var store = dealuserpickerpanel.down('grid').getStore();
        var dealuserproxy = store.getProxy();
        dealuserproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        store.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				context.dealUsersData = records;
            }
        });	
	},
	closeDealUserPickerPanel: function(panel){
		var me = this,activeItem;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
		
		if(panel.config.backview == "letterSignedByloanmanager"){
			activeItem = detailpanel.down('dealdetailquotepanel');
		} else if(panel.config.backview == "letterSignedByloaneditor"){
			activeItem = detailpanel.down('quotedetailspanel');
		}
		eastregion.getLayout().setActiveItem(activeItem);
	},
	dealUserPickerPanelItemClick: function(grid, record, item, index, e, eOpts){
		console.log("Select Contact Grid Item Select");
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(grid);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var dealuserpickerpanel = grid.up('dealuserpickerpanel');
		dealuserpickerpanel.getForm().loadRecord(record);
	},
	dealUserPickerSaveBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);

		var dealuserpickerpanel = detailpanel.down('dealuserpickerpanel');
		var selcntgrid = dealuserpickerpanel.down('grid');
        if (selcntgrid.getSelectionModel().hasSelection()) {
            var row = selcntgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('idDealxUser'));
            var idDealxUser = row.get('idDealxUser');
            //var user = row.get('user');
			//var fullName = firstName+' '+lastName;
			var fullName  = row.get('user');
			if(dealuserpickerpanel.config.backview=="letterSignedByloanmanager" || dealuserpickerpanel.config.backview=="letterSignedByloaneditor"){
				var quotedetailspanel = detailpanel.down('quotedetailspanel');
				quotedetailspanel.getForm().findField('letterSignedByName').setValue(fullName);
				//quotedetailspanel.getForm().findField('letterSigner').setRawValue(idDealxUser);
				quotedetailspanel.getForm().findField('letterSigner').setValue(fullName);
			}
			me.closeDealUserPickerPanel(dealuserpickerpanel);
        } else {
            Ext.Msg.alert('Failed', "Please select contact.");
            return false;
        }
	}
});