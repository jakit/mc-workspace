Ext.define('DM2.controller.RecvGoodFaithCheckController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({
        });
    },

    RecvGoodFaithCheckClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this;
        me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvGoodFaithCheck',tabdealdetail);
    },

    closeRecvGoodFaithCheck: function(context) {
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },
	
	afterRecvGoodFaithCheckDocUpload: function(context) {
        console.log("After Recv Good Faith Check Doc Upload");
		var me = this;
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
    }
});