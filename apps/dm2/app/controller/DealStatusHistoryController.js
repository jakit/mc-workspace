Ext.define('DM2.controller.DealStatusHistoryController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({
			'dealdetailstatushistorypanel': {
				 afterrender: 'loadDealStatusHistory'
             }
        });
    },

    loadDealStatusHistory: function(panel) {
        console.log("Load Deal Status History");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
        var record  = context.record;
		var dealid  = record.data.idDeal;
		
		var deal_status_history_st = panel.getStore();
	
        deal_status_history_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        deal_status_history_st.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
            }
        });
    }

});