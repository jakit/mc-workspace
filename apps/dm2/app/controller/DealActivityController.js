Ext.define('DM2.controller.DealActivityController', {
    extend: 'Ext.app.Controller',

    refs: {
        activitydetailpanel: {
            autoCreate: true,
            selector: 'activitydetailpanel',
            xtype: 'activitydetailpanel'
        },
        activitymenu: '#activitymenu',
        loansizingpanel: {
            autoCreate: true,
            selector: 'loansizingpanel',
            xtype: 'loansizingpanel'
        },
        bankchooserpanel: {
            autoCreate: true,
            selector: 'bankchooserpanel',
            xtype: 'bankchooserpanel'
        },
        loansizingdetailsform: {
            selector: 'loansizingdetailsform',
            xtype: 'loansizingdetailsform'
        },
        addselbankpanel: {
            autoCreate: true,
            selector: 'addselbankpanel',
            xtype: 'addselbankpanel'
        },
        bankselectmenu: {
            autoCreate: true,
            selector: 'bankselectmenu',
            xtype: 'bankselectmenu'
        },
        loansizegridmenu: {
            autoCreate: true,
            selector: 'loansizegridmenu',
            xtype: 'loansizegridmenu'
        },
        filesubmissiongridmenu: {
            autoCreate: true,
            selector: 'filesubmissiongridmenu',
            xtype: 'filesubmissiongridmenu'
        },
        templateselgridpanel: {
            autoCreate: true,
            selector: 'templateselgridpanel',
            xtype: 'templateselgridpanel'
        },
        docselpanel: {
            autoCreate: true,
            selector: 'docselpanel',
            xtype: 'docselpanel'
        },
        addsubmissiondocpanel: {
            autoCreate: true,
            selector: 'addsubmissiondocpanel',
            xtype: 'addsubmissiondocpanel'
        },
        addgensubmdocpanel: {
            autoCreate: true,
            selector: 'addgensubmdocpanel',
            xtype: 'addgensubmdocpanel'
        },
        emailfaxsubmission: {
            autoCreate: true,
            selector: 'emailfaxsubmission',
            xtype: 'emailfaxsubmission'
        },
        globaldocsselgridpanel: {
            autoCreate: true,
            selector: 'globaldocsselgridpanel',
            xtype: 'globaldocsselgridpanel'
        },
        globaldocsgridmenu: {
            autoCreate: true,
            selector: 'globaldocsgridmenu',
            xtype: 'globaldocsgridmenu'
        },
        addglobaldocpanel: {
            autoCreate: true,
            selector: 'addglobaldocpanel',
            xtype: 'addglobaldocpanel'
        },
        globaltemplateselgridpanel: {
            autoCreate: true,
            selector: 'globaltemplateselgridpanel',
            xtype: 'globaltemplateselgridpanel'
        },
        globaldocselrepositorypanel: {
            autoCreate: true,
            selector: 'globaldocselrepositorypanel',
            xtype: 'globaldocselrepositorypanel'
        },
        submissioncontactgridmenu: {
            autoCreate: true,
            selector: 'submissioncontactgridmenu',
            xtype: 'submissioncontactgridmenu'
        }
    },

    init: function(application) {
        this.control({
            'activitydetailpanel radio[name="action"]':{
                change: this.onActionChange
            },
            'activitydetailpanel':{
                close: 'onActivitydetailclosebtnClick',
				afterrender: 'loadCallMainContactDetails'/*{
                    fn: function(panel) {
                        Ext.Function.bind(this.loadCallMainContactDetails, this, [panel])();
                    }
                }*/
            },
            'activitydetailpanel button[action="activitydonebtn"]':{
                click: 'onActivitydonebtnClick'
            },
            'addrentrollexpensedocactivity #docfilefield':{
                change: this.docFileFieldChange
            },
            '#activitymenu':{
                click: 'onActivityMenuClick'
            },
            '#activitymenu menuitem':{
                load: this.onMenuDocDrop
            },
            'activitydetailpanel #cntphone': {
                select:this.changeMainContactPhone
            },
            'loansizingpanel':{
                close: 'onActivitydetailclosebtnClick',
				afterrender: 'loadLoanSizingPanelDetails'
            },
            'loansizingpanel #loansizingdonebtn':{
                click: 'onLoanSizingDoneBtnClick'
            },
            'bankchooserpanel':{
                close: 'onActivitydetailclosebtnClick'
            },
            'bankchooserpanel button[action="savebankbtn"]':{
                click: 'onSaveBankBtnClick'
            },
            'loansizingpanel #loanpropertygridpanel':{
				beforeselect: this.loanPropertyGridItemBeforeSelect,
                select: this.loanPropertyGridItemSelect,
                rowcontextmenu: this.onLoanPropertyGridRightClick
            },
            'loansizingdetailsform':{
                close:this.onLoanSizingDetailsFormClose
            },
			'loansizingdetailsform field':{
                change:this.onLoanSizeFieldChange
            },
            'loansizegridmenu':{
                click:this.onLoanSizeGridMenuClick
            },
            'loansizingdetailsform button[action="saveLoanSize"]':{
                click:this.onSaveLoanSizeBtnClick
            },
            'loansizingdetailsform button[action="resetLoanSize"]':{
                click:this.onResetLoanSizeBtnClick
            },
            'loansizingdetailsform button[action="clearLoanSize"]':{
                click:this.onClearLoanSizeBtnClick
            },
			'#bcselbankgridpanel dataview':{
                itemlongpress: this.onBankSelGridItemLongPress
            },
            '#bcselbankgridpanel #selbankbtn':{
                click:this.onSelBankBtnClick
            },
            '#bcselbankgridpanel #glbsbmbtn':{
                click:this.showGlobalSubmission
            },
            'addselbankpanel':{
                close:this.closeAddSelBankBtnClick
            },
            'bankchooserpanel #bcselbankgridpanel':{
				beforeselect: this.onBankSelGridItemBeforeSelect,
                rowcontextmenu: this.onBankSelGridPanelRightClick,
                select: this.onBCSelBankGridPanelItemDBClick
            },
            'bankselectmenu':{
                click:this.onBankSelectMenuClick
            },
            'bankchooserpanel #showContactToSubmissionBtn':{
                click : this.showContactToSubmissionBtnClick
            },
            'bankchooserpanel #bankdocsgridpanel':{
                load: this.onSubmissionDocDrop,
                //afterrender: this.onBankDocsGridAfterRender,
                containercontextmenu: this.onBankDocsGridPanelRightClick,
                rowcontextmenu: this.onBankDocsGridItemRightClick
            },
            '#bankdocsgridpanel gridview':{
                beforedrop: this.dropDocsToSubmission,
				itemlongpress: this.onBankDocsGridItemLongPress
            },
            'bankchooserpanel button[action="saveSubmissionBtn"]':{
                click:this.saveSubmissionBtnClick
            },
            'bankchooserpanel button[action="resetSubmissionBtn"]':{
                click:this.resetSubmissionBtnClick
            },
            'bankchooserpanel button[action="clearSubmissionBtn"]':{
                click:this.clearSubmissionBtnClick
            },
            '#bankdocsgridpanel #selectedclmn':{
                checkchange: this.selectedCheckChange
            },
            'bankchooserpanel #generatefilebtn':{
                click:this.genDocBtnClick
            },
            'bankchooserpanel #bankseldone':{
                click:this.onBankSelDoneBtnClick
            },
            '#bankdocsgridpanel #upcolumn':{
                itemClick: this.onUpColumnItemClick
            },
            '#bankdocsgridpanel #downcolumn':{
                itemClick: this.onDownColumnItemClick
            },
            'filesubmissiongridmenu':{
                click:this.onFileSubGridMenuClick
            },
            'filesubmissiongridmenu #subaddfile':{
                change: this.addSubmDocFileFieldChange
            },
            'templateselgridpanel':{
                close:this.closeTemplateSelGridPaneBtnClick,
                itemdblclick: this.closeTemplateSelGridPaneBtnClick,
                select: this.templateItemSelected
            },
            'templateselgridpanel #createtmpbtn':{
                click:this.closeTemplateSelGridPaneBtnClick
            },
            'addsubmissiondocpanel #docfilefield':{
                change:this.addFileFieldChange
            },
            'docselpanel':{
                close:this.closeDocSelPanel
            },
            'docselpanel #docsselgridpanel':{
                select:this.docsselgriditemselect
            },
            'addsubmissiondocpanel':{
                close:this.addSubmissionDocPanelClose
            },
            'addsubmissiondocpanel grid':{
                select: this.addSubmDocItemSelect
            },
            'addsubmissiondocpanel #docuploadallbtn':{
                click: this.submissionDocUploadAllBtnClick
            },
            'addsubmissiondocpanel #docclearallbtn':{
                click: this.submissionDocClearAllBtnClick
            },
			'addsubmissiondocpanel textareafield[name="desc"]':{
                keyup: this.onChangeAddSubmDocField
            },
            'addsubmissiondocpanel textfield[name="clsfctn"]':{
                keyup: this.onChangeAddSubmDocField
            },
			'addsubmissiondocpanel textfield[name="name"]':{
                keyup: this.onChangeAddSubmDocField
            },
            'docselpanel #savedocbtn':{
                click: this.saveDocRepositoryBtnClick
            },
            'bankchooserpanel #emailbtn':{
                click: 'onEmailSubmissionBtnClick'
            },
            'bankchooserpanel #faxbtn':{
                click: 'onFaxSubmissionBtnClick'
            },
			'bankchooserpanel #printbtn':{
                click: 'onPrintSubmissionBtnClick'
            },
            'emailfaxsubmission':{
                close: this.onEmailFaxSubmissionClose
            },
            'emailfaxsubmission #sendemailresetbtn':{
                click: this.onSendEmailResetBtnClick
            },
            'emailfaxsubmission #sendemailbtn':{
                click: this.onSendEmailBtnClick
            },
            'globaldocsselgridpanel':{
                close: this.closeGlobalDocsSelGridPanel,
                load: this.onGlobalSubmissionDocDrop,
                containercontextmenu: this.onGlobalDocsGridPanelRightClick,
                rowcontextmenu: this.onGlobalDocsGridItemRightClick,
				afterrender: this.onGlobalDocGridPanelRender
            },
            'globaldocsgridmenu':{
                click: this.globalDocsGridMenuClick
            },
            'globaldocsgridmenu #addfile':{
                change:this.globalAddFileFieldChange
            },
            'addglobaldocpanel':{
                close: this.closeAddGlobalDocPanel
            },
            'addglobaldocpanel grid':{
                select: this.addGlobalDocItemSelect
            },
            'addglobaldocpanel #globaldocuploadallbtn':{
                click: this.globalDocUploadAllBtnClick
            },
			'addglobaldocpanel textareafield[name="desc"]':{
                keyup: this.onChangeClsfDesc
            },
            'addglobaldocpanel textfield[name="clsfctn"]':{
                keyup: this.onChangeClsfDesc
            },
			'addglobaldocpanel textfield[name="name"]':{
                keyup: this.onChangeClsfDesc
            },
            'globaltemplateselgridpanel':{
                close: this.closeGlobalTemplateSelGridPanel,
                select: this.globalTemplateSelGridItemSelect,
                itemdblclick: this.closeGlobalTemplateSelGridPanel
            },
            'globaltemplateselgridpanel #createtmpbtn':{
                click:this.closeGlobalTemplateSelGridPanel
            },
            'globaldocselrepositorypanel':{
                close:this.closeGlobalDocSelRepositoryPanel
            },
            'globaldocselrepositorypanel #globaldocsselrepositorygridpanel':{
                select:this.globalDocsSelRepositoryGridItemSelect
            },
            'globaldocselrepositorypanel #savedocbtn':{
                click: this.saveGlobalDocRepositoryBtnClick
            },      
            'bankchooserpanel #submissioncontactsgridpanel':{
                rowcontextmenu: this.onSubmissionContactGridRightClick
            },
			'#submissioncontactsgridpanel dataview':{
				itemlongpress: this.onSubCntGridItemLongPress
			},
            'submissioncontactgridmenu':{
                click: this.submissionContactGridMenuClick
            },
			'#bankdocspanel field':{
                change:this.onBankSelFieldChange
            },
			'bankchooserpanel checkboxfield[name="showall"]':{
                change: 'onShowAllSubmissionCheckboxChange'
            }
        });
    },
	
	loadAllActivityTracks: function(){
		var me = this;
		var activities_st  = Ext.getStore('Activities');		
		var activitiesproxy = activities_st.getProxy();
		var activitiesproxyCfg = activitiesproxy.config;
        activitiesproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		activities_st.setProxy(activitiesproxyCfg);
		activities_st.load({
			callback: function(records, operation, success) {
			}
		});
	},

    loadprenotes: function(panel) {
        var me = this;
		var prenotecombo = panel.getForm().findField('note');
        var prenotest = prenotecombo.getStore();
        prenotest.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        prenotest.load({
            callback: function(records, operation, success){
                if(records!==null)
                {
                    prenotecombo.select(prenotest.getAt(0));
                }
            }
        });
    },

    CallDClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this,detailActivityPanel;
		console.log("CallDClick Called");
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
        var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('activitydetailpanel')){
			console.log("Use Old Panel");
			detailActivityPanel = detailpanel.down('activitydetailpanel');
			detailActivityPanel.getForm().reset();
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	detailActivityPanel = Ext.create('DM2.view.ActivityDetailPanel');
		}
        eastregion.getLayout().setActiveItem(detailActivityPanel);
		detailActivityPanel.setTitle("Activities > "+activitytext);
        detailActivityPanel.down('#activityid').setValue(activityid);
    },
	
	loadCallMainContactDetails: function(panel){
		
        var me = this;
        var detailActivityPanel = panel;
        me.loadprenotes(panel);
		var context = me.getController('DealDetailController').getTabContext(panel);
        var record = context.record;
        var dealid = record.data.idDeal;    
        var grid = context.down('dealdetailcontactfieldset').down('grid');
        
        var dealdetailcntst = grid.getStore();//var dealdetailcntst = Ext.getStore('DealDetailContacts');
        var cntid = 0;
        if(dealdetailcntst.isLoaded()){
            dealdetailcntst.each(function(record) {
                if(record.get('contactType')==="MAIN CONTACT"){
                    //console.log(record.get('contactid'));
                    cntid = record.get('contactid');
                }
            }, this);
        }
        if(cntid!==0){
            ///Load Main Contact
            panel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'RcontactDetails/'+cntid,
                scope:this,
                success: function(response, opts) {
                    panel.setLoading(false);
                    console.log("Main Contact Load is completed.");
                    var obj = Ext.decode(response.responseText);

                    if(response.status == 200) {
                        //detailActivityPanel.down('#callbtn').setText('&nbsp;&nbsp;Call '+obj[0].fullName);
                        //detailActivityPanel.down('#callbtn').setHref('ciscotel:+'+obj[0].officePhone_1);
						panel.getForm().findField('fullName').setValue(obj[0].firstName+" "+obj[0].lastName);
                        panel.getForm().findField('companyName').setValue(obj[0].companyName);
                        panel.getForm().findField('position').setValue(obj[0].position);
						
						///Fill the Address, City, State, Zip
						var cntAddress = "",citystatezipval = "";
						var citystatezip = [];
						if(obj[0].address.length>0) {
							for(var j=0;j<obj[0].address.length;j++){
								var addressItem = obj[0].address[j];
								if(addressItem.isPrimary) {
									cntAddress = addressItem.address;
									if(addressItem.city!="") {
										citystatezip.push(addressItem.city);
									}
									if(addressItem.state!="") {
										citystatezip.push(addressItem.state);
									}
									if(addressItem.zip!="") {
										citystatezip.push(addressItem.zip);
									}
								}
							}
						}
						if(citystatezip.length>0) {
							citystatezipval = citystatezip.join(",");
						}
                        panel.getForm().findField('address').setValue(cntAddress);
                        panel.getForm().findField('citystatezip').setValue(citystatezipval);
                    } else {
                        console.log("Main Contact Load is Failure.");
                        Ext.Msg.alert('Main Contact Load Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    panel.setLoading(false);
                    console.log("Activity for deal Failure.");
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Activity Status Change Failed', obj.errorMessage);
                }
            });
			
			var cntPhSt = panel.getForm().findField('cntphone').getStore();
            cntPhSt.getProxy().setHeaders({
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            });
            cntPhSt.load({
                params:{
                    filter :'idContact = '+cntid
                },
                callback: function(records, operation, success){
                    var cntphonecmbo = panel.getForm().findField('cntphone');
                    cntphonecmbo.select(cntPhSt.getAt(0));
                    //detailActivityPanel.down('#callbtn').setText(cntphonecmbo.getRawValue());
					var phNo = cntphonecmbo.getValue();
					var firstNo = phNo.charAt(0);
					if(firstNo!="1"){
						phNo = "1"+phNo;
					}
                    panel.down('#callbtn').setText(cntphonecmbo.getValue());
                    panel.down('#callbtn').setHref('ciscotel:+'+phNo);
                }
            });
        }
	},
	
    onActionChange: function(cmp, newValue, oldValue, eOpts) {
        console.log("On Action Change"+newValue);
        console.log(cmp.getGroupValue());
        var me = this;
        if(cmp.getGroupValue()==="addreminder"){
            me.getActivitydetailpanel().down('numberfield[name="days"]').show();
        }else{
            me.getActivitydetailpanel().down('numberfield[name="days"]').hide();
        }
    },

    onActivitydetailclosebtnClick: function(panel, eOpts) {
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(detailpanel.down('defaultworkareapanel'));
    },

    onActivitydonebtnClick: function(button, e, eOpts) {
        console.log("Change Call Main Contact to Done");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(button);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var record = context.record;
        var dealid = record.data.idDeal;
        var actdetailform = detailpanel.down('activitydetailpanel');
        ///Post the Note with dealid
        me.postNote(dealid,actdetailform);
    },

    postActivityHistory: function(dealid, actDetailForm) {
        var me = this;
		console.log("Inside Post Activity History");
		var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
		var activityid = null;
		//console.log(actDetailForm);
		//console.log(actDetailForm.xtype);
		if(actDetailForm.xtype!="generaltemplateselgridpanel" && actDetailForm.xtype!="generaldocselrepositorypanel" && actDetailForm.xtype!="senddocumentpanel" && actDetailForm.xtype!="closingview" && actDetailForm.xtype!="underwriting" && actDetailForm.xtype!="approvalpanel" && actDetailForm.xtype!="selectbidpanel"){
			if(actDetailForm.getForm().findField('activityid')){
				activityid = actDetailForm.getForm().findField('activityid').getValue();
			}
		}
		if(activityid==null){
			activityid = actDetailForm.config.activityid;
		}
		console.log("activityid:-"+activityid);
		if(activityid==null){
			var eastregion = actDetailForm.up('[region=east]');
			activityid = eastregion.config.activityid;
		}
		console.log("Activity ID : "+activityid);
        var data = [{
            'idActivity' : activityid,
            'idDeal' : dealid,
            'userName'  : userid
        }];
        actDetailForm.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:ActivityHistory',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Activity for deal is completed.");
                var obj = Ext.decode(response.responseText);
                actDetailForm.setLoading(false);
                if(obj.statusCode == 201){
                    //Close the activity detail
					var context = me.getController('DealDetailController').getTabContext(actDetailForm);
					if(context.xtype=="tab-sales-detail"){
			        	//me.getController('SalesController').loadAllSales([]);
						me.getController('SalesDealDetailController').refreshSaleDealDetailData(context);
					} else {
						me.getController('DealDetailController').refreshDealDetailData(context);
						me.getController('DealStatusHistoryController').loadDealStatusHistory(context.down('dealdetailstatushistorypanel'));
					}
					me.closeActivityDetail(context); //Close Activity Detail Panel
                } else {
                    console.log("Activity for deal is Failure.");
                    Ext.Msg.alert('Activity Status Change Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                actDetailForm.setLoading(false);
                console.log("Activity for deal Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Activity Status Change Failed', obj.errorMessage);
            }
        });
    },

    closeActivityDetail: function(context) {
        var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
		eastregion.getLayout().getActiveItem().close();
        eastregion.getLayout().setActiveItem(detailpanel.down('defaultworkareapanel'));
    },

    postNote: function(dealid, actDetailForm) {
        console.log("Post Note");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(actDetailForm);
        var data = [{
            'content':actDetailForm.getForm().findField('note').getRawValue(),
            'idDeal': dealid
        }];
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'Rnotes',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                console.log("Note is submitted.");
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 201) {
					/*if(actDetailForm){
                    	actDetailForm.getForm().reset();
					}*/
					me.postActivityHistory(dealid,actDetailForm);
				   	//if(btn.up('tab-deal-detail')){
						me.getController('NoteController').loadDealDetailNotes(context.down('dealnotesgridpanel'));
				   	//}else{
				   		me.getController('NoteController').loadNotesStore(dealid);
					//}                    
                } else {
                    console.log("Add Note Submition Failure.");
                    Ext.Msg.alert('Add note failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("Add Note Submition Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Add note failed', obj.errorMessage);
            }
        });
    },

    onActivityMenuClick: function(menu, item, e, eOpts) {
        var me = this;
		var tabdealdetail = me.getController('DealDetailController').getTabContext(menu);
        var h_fname = item.h_fname;
        me.showActivity(item.activityid,item.text,tabdealdetail);
    },

    docFileFieldChange: function(filefield, value, eOpts) {
        var me = this;
        var adddocform = me.getAddrentrollexpensedocactivity().getForm();

        if(filefield.fileInputEl.dom.files.length > 0){

            var file = filefield.fileInputEl.dom.files[0];

            var filesize = (file.size/1024).toFixed(2);

            var clsfctn = me.getAddrentrollexpensedocactivity().config.classification;
            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : file.name,
                'size' : filesize,
                'clsfctn' :clsfctn,
                'desc' :'',
                'fileobj':file
            });
            var docstmpst = Ext.getStore('DocsTemp');
            docstmpst.add(docrec);
            /////

            me.getAddrentrollexpensedocactivity().down('grid').getSelectionModel().select(0);
        }
    },
	/*
	* Not Used
	*/
    postDoc: function(dealid, actDetailForm) {
        console.log("Doc Submit Button Clicked.");
        var me = this;

        var docform = actDetailForm.getForm();

        var docstempst = Ext.getStore('DocsTemp');
        docstempst.each(function(record) {

            var fileObj = record.get('fileobj');
            var filename = record.get('name');

            var ext = /^.+\.([^.]+)$/.exec(filename);

            if(ext === null){
                ext = "";
            }else {
                ext = ext[1];
            }

            var reader = new FileReader();

            reader.readAsDataURL(fileObj);

            reader.onload = function (e) {

                var conentarr = e.target.result.split("base64,");
                var content = '';
                if(conentarr.length > 1){
                    content = conentarr[1];
                }

                var data = [{
                    'name'           : record.get('name'),
                    'description'    : record.get('desc'),
                    'classification' : record.get('clsfctn'),
                    'size'           : fileObj.size,
                    'ext'            : ext,
                    'content'        : content,
                    'idDeal': dealid
                }];

                actDetailForm.setLoading(true);
                Ext.Ajax.request({
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                    },
                    url:DM2.view.AppConstants.apiurl+'Rdocs',
                    params: Ext.util.JSON.encode(data),
                    scope:this,
                    success: function(response, opts) {
                        record.set('uploadPercent',1);
                        actDetailForm.setLoading(false);
                        var obj = Ext.decode(response.responseText);
                        if(obj.statusCode == 201)
                        {
                            me.getController("DocController").loadDocsStore(dealid);
                        }
                        else{
                            Ext.Msg.alert('Failed', "Please try again.");
                        }
                    },
                    failure: function(response, opts) {
                        console.log("Login Failure.");
                        actDetailForm.setLoading(false);
                        Ext.Msg.alert('Failed', "Please try again.");
                    }
                });
            };
            reader.onerror = function (e) {
                console.log(e.target.error);
            };
        });
    },

    onDocDrop: function(cmp, e, file) {
        console.log("Done Reading");
        var me = this;
        var adddocform = me.getAddrentrollexpensedocactivity().getForm();

        var filesize = (file.size/1024).toFixed(2);

        var clsfctn = me.getAddrentrollexpensedocactivity().config.classification;
        ///// Add the file item to Store
        var docrec = Ext.create('DM2.model.Document',{
            'name'    : file.name,
            'size'    : filesize,
            'clsfctn' : clsfctn,
            'desc'    : '',
            'fileobj' : file
        });
        var docstempst = Ext.getStore('DocsTemp');
        docstempst.add(docrec);
        /////

        me.getAddrentrollexpensedocactivity().down('grid').getSelectionModel().select(0);
    },

    onMenuDocDrop: function(cmp, e, file) {
        console.log(cmp);
    },

    changeMainContactPhone: function(combo, records, eOpts) {
        var me = this;
        console.log("Select Event fired");
        var detailActivityPanel = me.getActivitydetailpanel();
        //detailActivityPanel.down('#callbtn').setText(combo.getRawValue());
		var phNo = combo.getValue();
		var firstNo = phNo.charAt(0);
		if(firstNo!="1"){
			phNo = "1"+phNo;
		}
        detailActivityPanel.down('#callbtn').setText(combo.getValue());
        detailActivityPanel.down('#callbtn').setHref('ciscotel:+'+phNo);
		//detailActivityPanel.down('#callbtn').setHref('ciscotel:+'+combo.getValue());
    },

    LoanSizeClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this,loansizingpanel;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('loansizingpanel')){
			console.log("Use Old Panel");
			loansizingpanel = detailpanel.down('loansizingpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	loansizingpanel = Ext.create('DM2.view.LoanSizingPanel');
		}
        eastregion.getLayout().setActiveItem(loansizingpanel);
		loansizingpanel.setTitle("Activities > "+activitytext);			
        loansizingpanel.down('loansizingdetailsform').getForm().findField('activityid').setRawValue(activityid);
		loansizingpanel.down('loansizingdetailsform').config.isFormDirty = false;
    },
	
	loadLoanSizingPanelDetails: function(panel){
		var me = this;
        var loansizingpanel = panel;
		var context = me.getController('DealDetailController').getTabContext(panel);
        var record = context.record;
        var dealid = record.data.idDeal;    
		
        var grid = panel.down('#loanpropertygridpanel');  
        var dealpropertyst = grid.getStore();
		
        dealpropertyst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealpropertyst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				var loansizegridtbar = loansizingpanel.down('#loanpropertygridpanel').down('toolbar');
				if(dealpropertyst.getCount()>0){
					loansizegridtbar.hide();
				} else {
					loansizegridtbar.show();
				}
		        loansizingpanel.down('#loanpropertygridpanel').getSelectionModel().select(0);
				me.getController('DealDetailController').setLoanSize(panel);
			}
		});
	},
	
    BankChooserClick: function(activityid, activitytext, h_fname, tabdealdetail) {
        var me = this,bankchooserpanel;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('bankchooserpanel')){
			console.log("Use Old Panel");
			bankchooserpanel = detailpanel.down('bankchooserpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	bankchooserpanel = Ext.create('DM2.view.BankChooserPanel');
		}
        eastregion.getLayout().setActiveItem(bankchooserpanel);
		bankchooserpanel.setTitle("Activities > "+activitytext);
        bankchooserpanel.down('#bankdocspanel').getForm().findField('activityid').setRawValue(activityid);
		bankchooserpanel.down('#bankdocspanel').config.activityid = activityid;
		
        /// Load the Selected Banks ////
        detailpanel.config.bankselactivityid = activityid;
        detailpanel.config.bankselactivitytext = activitytext;
        detailpanel.config.bankselh_fname = h_fname;
		
        me.loadSubmissionPerDeal(tabdealdetail);
    },

    onSaveBankBtnClickOld: function(btn) {
        // Save the bank to Submission Table //
        var me = this;
		var context = btn.up('tab-deal-detail');	
        console.log("Save the bank to Submission Table");
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');		
		var bankgrid = bankchooserpanel.down("#addselbankpanel");
        if (bankgrid.getSelectionModel().hasSelection()) {
            var rec = bankgrid.getSelectionModel().getSelection()[0];
        }
        else{
            Ext.Msg.alert('Failed', "Please select bank for submission.");
            return false;
        }
		
		var row = context.record;
		var dealid = row.data.idDeal;

        var data = {
            'idDeal' : dealid,
            'idBank' : rec.get('idBank'),
            'submitDate': new Date()
        };

        data['@metadata'] = {'checksum' : 'override'};
        var methodname = "POST";
		var addselbankpanel = bankchooserpanel.down('#addselbankpanel');
        addselbankpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Submission',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                addselbankpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
                    me.closeAddSelBankBtnClick(addselbankpanel);

                    bankchooserpanel.config.savemode = "new";
                    me.loadSubmissionPerDeal(context);
                }
                else{
                    console.log("Save the bank to Submission Table");
                    Ext.Msg.alert('Save the bank to Submission Table failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                addselbankpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save the bank to Submission Table', obj.errorMessage);
            }
        });
        ////
    },
	
	onSaveBankBtnClick: function(btn) {
        // Save the bank to Submission Table //
        var me = this;
		var context = btn.up('tab-deal-detail');	
        console.log("Save the bank to Submission Table");
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');		
		var bankgrid = bankchooserpanel.down("#addselbankpanel");
        if (bankgrid.getSelectionModel().hasSelection()) {
            var rec = bankgrid.getSelectionModel().getSelection()[0];
        }
        else{
            Ext.Msg.alert('Failed', "Please select bank for submission.");
            return false;
        }
		
		var row = context.record;
		var dealid = row.data.idDeal;

        var data = {
            'idDeal' : dealid,
            'idBank' : rec.get('idBank'),
			'operatingAs' : 'P',
			'isSubmitted' : true,
            'createDateTime': new Date()
        };

        data['@metadata'] = {'checksum' : 'override'};
        var methodname = "POST";
		var addselbankpanel = bankchooserpanel.down('#addselbankpanel');
        addselbankpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Loan',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                addselbankpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
                    me.closeAddSelBankBtnClick(addselbankpanel);

                    bankchooserpanel.config.savemode = "new";
                    me.loadSubmissionPerDeal(context);
                }
                else{
                    console.log("Save the bank to Submission Table");
                    Ext.Msg.alert('Save the bank to Submission Table failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                addselbankpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save the bank to Submission Table', obj.errorMessage);
            }
        });
        ////
    },
	
	loanPropertyGridItemSelect: function( selModel, prrecord, index, eOpts) {
        var me = this;
	 	var context = selModel.view.ownerCt.up('tab-deal-detail');		
		me.loadLoanSizingSetupPropertyRecord(context,prrecord);
    },
	
	loadLoanSizingSetupPropertyRecord: function(context,prrecord){
		var me = this;
		var record = context.record;
        var dealid = record.data.idDeal;

        var loansizingpanel = context.down('loansizingpanel');
		var loansizingdtfrm = loansizingpanel.down('loansizingdetailsform');		
		
        //var idDealhasPropertyval = prrecord.get('idDealxProp');
		var idDealhasPropertyval = prrecord.get('idSetup');

        var methodname = "GET";
		
		if(loansizingdtfrm.down('#saveloansizebtn')){
			loansizingdtfrm.down('#saveloansizebtn').disable();
		}
		if(loansizingdtfrm.down('#resetloansizebtn')){
			loansizingdtfrm.down('#resetloansizebtn').disable();
		}
		loansizingdtfrm.config.isFormDirty = false;
		
        loansizingdtfrm.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Setup/'+idDealhasPropertyval,
            scope:this,
            method:methodname,
            success: function(response, opts) {
                loansizingdtfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200){
                    var dealpropertyrec = Ext.create('DM2.model.DealHasProperty',obj[0]);
					
                    var lndtfrm = loansizingdtfrm.getForm();
					lndtfrm.findField('idSetup').setRawValue(dealpropertyrec.get('idSetup'));					
					lndtfrm.findField('idDeal').setRawValue(dealpropertyrec.get('idDeal'));
					lndtfrm.findField('idProperty').setRawValue(dealpropertyrec.get('idProperty'));
					
					if (Ext.isEmpty(dealpropertyrec.get('estimatedLoanSize'))) {
					    lndtfrm.findField('estimatedLoanSize').setRawValue(dealpropertyrec.get('estimatedLoanSize'));
					} else {
						lndtfrm.findField('estimatedLoanSize').setRawValue(Ext.util.Format.currency(dealpropertyrec.get('estimatedLoanSize')));
					}
					
					lndtfrm.findField('income').setRawValue(dealpropertyrec.get('income'));
					lndtfrm.findField('expenses').setRawValue(dealpropertyrec.get('expenses'));
					lndtfrm.findField('numberOfBuildings').setRawValue(dealpropertyrec.get('numberOfBuildings'));
					lndtfrm.findField('numberOfStories').setRawValue(dealpropertyrec.get('numberOfStories'));
					lndtfrm.findField('purchasePrice').setRawValue(dealpropertyrec.get('purchasePrice'));
					lndtfrm.findField('percentSold').setRawValue(dealpropertyrec.get('percentSold'));
					lndtfrm.findField('percentRented').setRawValue(dealpropertyrec.get('percentRented'));
					lndtfrm.findField('vacancyAmt').setRawValue(dealpropertyrec.get('vacancyAmt'));
					lndtfrm.findField('assessedValue').setRawValue(dealpropertyrec.get('assessedValue'));
					lndtfrm.findField('assessedValueYear').setRawValue(dealpropertyrec.get('assessedValueYear'));
					lndtfrm.findField('transactionValue').setRawValue(dealpropertyrec.get('transactionValue'));
					lndtfrm.findField('yearAcquired').setRawValue(dealpropertyrec.get('yearAcquired'));
					lndtfrm.findField('numberOfApartments').setRawValue(dealpropertyrec.get('numberOfApartments'));
					lndtfrm.findField('numberOfRooms').setRawValue(dealpropertyrec.get('numberOfRooms'));
					lndtfrm.findField('numberOfCommercialApartments').setRawValue(dealpropertyrec.get('numberOfCommercialApartments'));
					lndtfrm.findField('commercialSquareFeet').setRawValue(dealpropertyrec.get('commercialSquareFeet'));
					lndtfrm.findField('elevatorWalk').setRawValue(dealpropertyrec.get('elevatorWalk'));
                } else {
                    console.log("Get Loan Sizing Info Failure.");
                    Ext.Msg.alert('Get Loan Sizing Info failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                loansizingdtfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Get Loan Sizing Info', obj.errorMessage);
            }
        });
	},
	
    onLoanSizingDetailsFormClose: function() {
        var me = this;
        var lnbtmregion = me.getLoansizingpanel().down('#loansizingbottompanel');
        var loansizingbtmtabpanel = me.getLoansizingpanel().down('#loansizingbtmtabpanel');
        lnbtmregion.getLayout().setActiveItem(loansizingbtmtabpanel);
    },

    onSaveLoanSizeBtnClick: function(btn) {
        var me = this;
        console.log("Save Loan Size Form");
		var context = btn.up('tab-deal-detail');		
        var loansizingpanel = context.down('loansizingpanel');
		var loansizingdtfrm = loansizingpanel.down('loansizingdetailsform');
		
		var assessedValueYear = loansizingdtfrm.getForm().findField('assessedValueYear').getValue();
		var yearAcquired = loansizingdtfrm.getForm().findField('yearAcquired').getValue();
		if(assessedValueYear!=null){
			assessedValueYear = Ext.Date.format(assessedValueYear, 'Y-m-d');
		}
		if(yearAcquired!=null){
			yearAcquired = Ext.Date.format(yearAcquired, 'Y-m-d');
		}
        var data = {
            'idSetup'            : loansizingdtfrm.getForm().findField('idSetup').getValue(),
            'income'           	 : loansizingdtfrm.getForm().findField('income').getValue(),
            'expenses'           : loansizingdtfrm.getForm().findField('expenses').getValue(),
            'estimatedLoanSize'  : loansizingdtfrm.getForm().findField('estimatedLoanSize').getValue(),
            'assessedValue'       : loansizingdtfrm.getForm().findField('assessedValue').getValue(),
            'assessedValueYear'   : assessedValueYear,
            'transactionValue'   : loansizingdtfrm.getForm().findField('transactionValue').getValue(),
            'yearAcquired'       : yearAcquired,
            'numberOfBuildings'  : loansizingdtfrm.getForm().findField('numberOfBuildings').getValue(),
            'numberOfStories'    : loansizingdtfrm.getForm().findField('numberOfStories').getValue(),
            'percentSold'        : loansizingdtfrm.getForm().findField('percentSold').getValue(),
            'purchasePrice'      : loansizingdtfrm.getForm().findField('purchasePrice').getValue(),
            'vacancyAmt'        : loansizingdtfrm.getForm().findField('vacancyAmt').getValue(),
            'percentRented'      : loansizingdtfrm.getForm().findField('percentRented').getValue(),
            //'mortgageType'       : loansizingdtfrm.getForm().findField('mortgageType').getValue(),
            //'propertyType'       : loansizingdtfrm.getForm().findField('propertyType').getValue(),
            'numberOfApartments' : loansizingdtfrm.getForm().findField('numberOfApartments').getValue(),
            'numberOfRooms'      : loansizingdtfrm.getForm().findField('numberOfRooms').getValue(),
            'numberOfCommercialApartments': loansizingdtfrm.getForm().findField('numberOfCommercialApartments').getValue(),
            'commercialSquareFeet': loansizingdtfrm.getForm().findField('commercialSquareFeet').getValue(),
            'environmentalDate'  : loansizingdtfrm.getForm().findField('environmentalDate').getValue(),
            'appraisalDate'      : loansizingdtfrm.getForm().findField('appraisalDate').getValue(),
            'appraiser'          : loansizingdtfrm.getForm().findField('appraiser').getValue(),
            //'commitExpirationDate': loansizingdtfrm.getForm().findField('commitExpirationDate').getValue(),
            //'commitmentNumber'   : loansizingdtfrm.getForm().findField('commitmentNumber').getValue(),
            //'commitmentDate'     : loansizingdtfrm.getForm().findField('commitmentDate').getValue(),
            //'commitmentCheck'    : loansizingdtfrm.getForm().findField('commitmentCheck').getValue(),
            //'commitmentCheckDate': loansizingdtfrm.getForm().findField('commitmentCheckDate').getValue(),
            //'commitmentCheckAmount'    : loansizingdtfrm.getForm().findField('commitmentCheckAmount').getValue(),
            'elevatorWalk'    : loansizingdtfrm.getForm().findField('elevatorWalk').getValue()
        };
        var dealid = loansizingdtfrm.getForm().findField('idDeal').getValue();
        data['@metadata'] = {'checksum' : 'override'};
        var methodname = "PUT";

        loansizingdtfrm.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Setup',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                loansizingdtfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
                    loansizingdtfrm.config.isFormDirty = false;
                    me.getController('DealDetailController').loadPropertyDetails(context.down('dealdetailpropertyfieldset'));
					me.loadLoanSizingPanelDetails(loansizingpanel);
                } else {
                    console.log("Save Loan Sizing Info Failure.");
                    Ext.Msg.alert('Save Loan Sizing Info failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                loansizingdtfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Loan Sizing Info', obj.errorMessage);
            }
        });
    },

    onSelBankBtnClick: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var bankselbtmpanel = bankchooserpanel.down('#bankselbtmpanel');
		var addselbankpanel;
		if(bankselbtmpanel.down('addselbankpanel')){
			console.log("Use Old Panel");
			addselbankpanel = bankselbtmpanel.down('addselbankpanel');
		}
		else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	addselbankpanel = Ext.create('DM2.view.AddSelBankPanel');//me.getAddselbankpanel();
		}
        bankselbtmpanel.getLayout().setActiveItem(addselbankpanel);
	
        /// Load the All Banks ////
        var filterstarr = [];
        me.loadAllBanks(filterstarr,context);
        ////////////
    },
	
	loadAllBanks: function(filterstarr,context){
		var me = this;
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		addselbankpanel = bankchooserpanel.down('#bankselbtmpanel').down('addselbankpanel');
        var bankallbuffSt = addselbankpanel.getStore(); //('Bankallbuff').getProxy();
		
		var bankallbuffStproxy = bankallbuffSt.getProxy();
        /*bankallbuffStproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var bankallbuffStproxyCfg = bankallbuffStproxy.config;
        bankallbuffStproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		bankallbuffSt.setProxy(bankallbuffStproxyCfg);
        bankallbuffSt.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
            }
        });
	},
	
    closeAddSelBankBtnClick: function(panel) {
        var me = this;
		var context = panel.up('tab-deal-detail');		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');		
        var eastregion = bankchooserpanel.down('#bankselbtmpanel');
        var bankdocspanel = bankchooserpanel.down('#bankdocspanel');
        eastregion.getLayout().setActiveItem(bankdocspanel);
    },

    onBankSelGridPanelRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        console.log("Bank selgrid Right click");
		var me = this;
        me.showSubmissionMenu(grid,record,e);
    },
	
	showSubmissionMenu: function(grid,record,e){
		var me = this;
		e.stopEvent();
        console.log("Show Menu for Sel Bank Add/Dealte");
        if(this.getBankselectmenu()){
            this.getBankselectmenu().showAt(e.getXY());
        }
	},
	
	onBankSelGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showSubmissionMenu(view,record,e);
	},

    onBankSelectMenuClick: function(menu, item, e, eOpts) {
        console.log("Menu Clicked");
        if(item.text === "Add Submission"){
            console.log("Add Submission");
            this.onSelBankBtnClick();
        }else if(item.text === "Remove Submission"){
            console.log("Remove Submission");
            this.removeSelBank();
        }else if(item.text === "Global Submission"){
            console.log("Global Submission");
            this.showGlobalSubmission();
        }else if(item.text === "Mark activity as done"){
            console.log("Mark activity as done");
            this.onBankSelDoneBtnClick();
        }
    },

    removeSelBank: function() {
        console.log("Remove Selected Bank");
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');		
        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        var row = bankselgrid.getSelectionModel().getSelection()[0];
        var idSubmission = row.get('idLoan');
        var dealid = row.get('dealid');
		
		var data = [{
            '@metadata': { 'checksum': 'override' },
            'idLoan' :idSubmission,
            'isSubmitted' : false
        }];
		
        Ext.Msg.show({
            title:'Delete Submission?',
            message: 'Are you sure you want to delete the Submission?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    bankselgrid.setLoading(true);
                    /*Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Loan/'+idSubmission+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            bankselgrid.setLoading(false);
                            console.log("Removed Submission.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200)
                            {
                                me.loadSubmissionPerDeal(context);
                            }
                            else{
                                console.log("Delete Submission Failure.");
                                Ext.Msg.alert('Delete Submission failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            bankselgrid.setLoading(false);
                            console.log("Delete Submission Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Submission failed', obj.errorMessage);
                        }
                    });*/
					Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'PUT', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:Loan/'+idSubmission,
						params: Ext.util.JSON.encode(data),
                        scope:this,
                        success: function(response, opts) {
                            bankselgrid.setLoading(false);
                            console.log("Removed Submission.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200)
                            {
                                me.loadSubmissionPerDeal(context);
                            }
                            else{
                                console.log("Delete Submission Failure.");
                                Ext.Msg.alert('Delete Submission failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            bankselgrid.setLoading(false);
                            console.log("Delete Submission Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Submission failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },
	
	onBankSelFieldChange: function(field, newValue, oldValue, eOpts ){
		console.log("On Loan Size Field Change Occur");
		var me = this;
		console.log(field);
		console.log(newValue);
		var context = field.up('tab-deal-detail');		
        var bankchooserpanel = context.down('bankchooserpanel');
		var bankdetailfrm = bankchooserpanel.down('#bankdocspanel');
		bankdetailfrm.down('#saveSubmissionBtn').enable();
		bankdetailfrm.down('#resetSubmissionBtn').enable();
		bankdetailfrm.config.isFormDirty = true;
	},
	
	onBankSelGridItemBeforeSelect: function( selModel, record, index, eOpts ){
		var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');		
        var bankchooserpanel = context.down('bankchooserpanel');
		var bankdetailfrm = bankchooserpanel.down('#bankdocspanel');
		if(bankdetailfrm.config.isFormDirty){
			console.log("Form is dirty.");
			Ext.Msg.alert('Alert!', "You have unsaved changes.Please save it before changing submission.");
			return false;
		}else{
			console.log("Continue");
		}
	},
	
    onBCSelBankGridPanelItemDBClick: function(selModel, record, item, index, e, eOpts) {
        console.log("Selected Bank Panel Item Double Click");
        var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		me.loadSubmissionRecordToForm(context,record);
    },
	
	loadSubmissionRecordToForm: function(context,record){
		var me = this;
		var bankchooserpanel = context.down('bankchooserpanel');
		var bankdetailfrm = bankchooserpanel.down('#bankdocspanel');
		
		if(bankdetailfrm.down('#saveSubmissionBtn'))
		{
			bankdetailfrm.down('#saveSubmissionBtn').disable();
		}
		if(bankdetailfrm.down('#resetSubmissionBtn'))
		{
			bankdetailfrm.down('#resetSubmissionBtn').disable();
		}
		bankdetailfrm.config.isFormDirty = false;
		
        me.loadDocsPerSubmission(record.get('idLoan'),context);
        me.loadGenFilesPerSubmission(record.get('idLoan'),context);
		
        //bankdetailfrm.getForm().loadRecord(record);
		bankdetailfrm.getForm().findField('fullName').setRawValue(record.get('fullName'));
		bankdetailfrm.getForm().findField('shortName').setRawValue(record.get('shortName'));
		bankdetailfrm.getForm().findField('userName').setRawValue(record.get('userName'));
		bankdetailfrm.getForm().findField('submitmethod').setRawValue(record.get('submitmethod'));
		bankdetailfrm.getForm().findField('submitnote').setRawValue(record.get('submitnote'));
		
		//bankdetailfrm.getForm().findField('amountRequested').setRawValue(record.get('amountRequested'));
		if (Ext.isEmpty(record.get('amountRequested')))  {
		    bankdetailfrm.getForm().findField('amountRequested').setRawValue(record.get('amountRequested'));
		} else {
			bankdetailfrm.getForm().findField('amountRequested').setRawValue(Ext.util.Format.currency(record.get('amountRequested')));
		}
		
		bankdetailfrm.getForm().findField('idsubmission').setRawValue(record.get('idLoan'));
		bankdetailfrm.getForm().findField('activityid').setRawValue(record.get('activityid'));
		if(record.get('submitdate')!=null){
        	var dtarr = record.get('submitdate').split('T');
	        //bankdetailfrm.getForm().findField('submitdate').setValue(dtarr[0]);
			bankdetailfrm.getForm().findField('submitdate').setRawValue(dtarr[0]);
		}
		
        var bankshrtname = bankdetailfrm.getForm().findField('shortName').getValue();
        var submissionid = bankdetailfrm.getForm().findField('idsubmission').getValue();

        //var outputfilename = "submission."+submissionid+"."+bankshrtname+".pdf";
		var outputfilename = "submission."+submissionid+"."+bankshrtname;
		
        //bankdetailfrm.getForm().findField('genfilename').setValue(outputfilename);
		bankdetailfrm.getForm().findField('genfilename').setRawValue(outputfilename);

        bankchooserpanel.down('#emailbtn').disable();
        bankchooserpanel.down('#printbtn').disable();
        bankchooserpanel.down('#faxbtn').disable();

        //me.loadSubmissionContacts(submissionid,record.get('bankid'),context);
		me.loadSubmissionContacts(submissionid,context);
	},

    loadDocsPerSubmission: function(idsubmission,context) {
        console.log("Load Docs Per Submission");
        var me = this;
        var filterstarr = [];

        var filterst = 'loanid = '+idsubmission;
        filterstarr.push(filterst);

        var filterst1 = "clsfctn != 'generated'";
        filterstarr.push(filterst1);
		
		var docspersubm_st = context.down('bankchooserpanel').down('#bankdocsgridpanel').getStore();

        docspersubm_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        docspersubm_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
                console.log(success);
                if(success)
                {
                    var submdocscnt = docspersubm_st.getCount();
                    if(submdocscnt > 0){
                        context.down('bankchooserpanel').down('#generatefilebtn').enable();
                    }
                }
            }
        });
    },

    loadGenFilesPerSubmission: function(idsubmission,context) {
        console.log("Load Generated Files Per Submission");
        var me = this;
        var filterstarr = [];

        var filterst = 'loanid = '+idsubmission;
        filterstarr.push(filterst);

        var filterst1 = "clsfctn = 'generated'";
        filterstarr.push(filterst1);
		
		var genfilessubmissionst = context.down('bankchooserpanel').down('#genfilespersubmissiongridpanel').getStore();
		
        genfilessubmissionst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        genfilessubmissionst.load({
            scope: this,
            params:{
                filter :filterstarr,
                order:'idSubxDoc DESC',
                pagesize:1
            },
            callback: function(records, operation, success) {
                if(success)
                {
                    console.log("cnt"+genfilessubmissionst.getCount());
                    if(genfilessubmissionst.getCount()>0){
                        context.down('bankchooserpanel').down('#emailbtn').enable();
                        context.down('bankchooserpanel').down('#printbtn').enable();
                        context.down('bankchooserpanel').down('#faxbtn').enable();

                        me.setEmailFaxLink(context);
                    }else{
                        context.down('bankchooserpanel').down('#emailbtn').disable();
                        context.down('bankchooserpanel').down('#printbtn').disable();
                        context.down('bankchooserpanel').down('#faxbtn').disable();
                    }
                }
            }
        });
    },

    onSubmissionDocDrop: function(cmp, e, file) {
        console.log("Done Reading");
        var me = this;
        console.log(file);
		var context = cmp.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var eastregion = bankchooserpanel.down('#bankselbtmpanel');
		
		var addsubmissiondocpanel;
		if(context.down('dealdetailsformpanel').down('addsubmissiondocpanel')){
			console.log("Use Old Panel");
			addsubmissiondocpanel = context.down('dealdetailsformpanel').down('addsubmissiondocpanel');
		}
		else {
			//create new GeneralAddDealDoc Panel
			console.log("Create New Panel");
			addsubmissiondocpanel = Ext.create('DM2.view.AddSubmissionDocPanel');//me.getAddsubmissiondocpanel();
		}
		eastregion.getLayout().setActiveItem(addsubmissiondocpanel);	
	
        var filesize = (file.size/1024).toFixed(2);

        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var bankshrtname = bankdocsfrm.getForm().findField('shortName').getValue();
        var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();
        //var classification = "submission-"+bankshrtname+"-"+submissionid;
		var classification = "";
        addsubmissiondocpanel.getForm().findField('clsfctn').setValue(classification);

        ///// Add the file item to Store
        var docrec = Ext.create('DM2.model.Document',{
            'name' : file.name,
            'size' : filesize,
            'clsfctn' :classification,
            'desc' :'',
            'fileobj':file
        });
        var docstempst = addsubmissiondocpanel.down('grid').getStore(); //('DocsTemp');
        docstempst.add(docrec);
        /////

        addsubmissiondocpanel.down('grid').getSelectionModel().select(0);

        addsubmissiondocpanel.getForm().findField('name').focus();
    },

    saveSubmissionBtnClick: function(btn) {
        console.log("Save Submission");
        var me = this;
		var context = btn.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var idSubmission = bankdocsfrm.getForm().findField('idsubmission').getValue();

        var data = {
            'idLoan': idSubmission,
            'dateRequested'  : bankdocsfrm.getForm().findField('submitdate').getValue(),
			'amountRequested' : bankdocsfrm.getForm().findField('amountRequested').getValue(),
            'specialNote'  : bankdocsfrm.getForm().findField('submitnote').getValue()/*,
            'quoteMethod': bankdocsfrm.getForm().findField('submitmethod').getValue()*/
        };
        data['@metadata'] = {'checksum' : 'override'};
        var methodname = "PUT";

        bankdocsfrm.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Loan',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                bankdocsfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201)
                {
					bankdocsfrm.config.isFormDirty = false;
                    /// Load the Selected Banks ////
                    me.loadSubmissionPerDeal(context);
                    ////////////
                }
                else{
                    console.log("Save Submission Failure.");
                    Ext.Msg.alert('Save Submission failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                bankdocsfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Submission Info', obj.errorMessage);
            }
        });
    },

    selectedCheckChange: function(checkcolumn, rowIndex, checked, eOpts) {
        var me = this;
		var context = checkcolumn.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var bankdocsgridpanel = bankchooserpanel.down('#bankdocsgridpanel');
        var rec = bankdocsgridpanel.getStore().getAt(rowIndex);
        console.log(rec.get('loanid'));
        console.log("Check Column Change"+checked);
        var idsubmission = rec.get('loanid');
        var idSubmissionxDocs = rec.get('idSubxDoc');

        var data = {
            'idSubxDoc': idSubmissionxDocs,
            'selected'  : checked
        };
        data['@metadata'] = {'checksum' : 'override'};
        var methodname = "PUT";

        bankdocsgridpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:SubxDoc',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                bankdocsgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
                    //Load the Docs Per Submission
                    me.loadDocsPerSubmission(idsubmission,context);
                } else {
                    console.log("Save Selected Failure.");
                    Ext.Msg.alert('Save Selected failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                bankdocsgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Selected Docs', obj.errorMessage);
            }
        });
    },

    onBankSelDoneBtnClick: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var row = context.record;
		var dealid = row.data.idDeal;		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');		
        var actDetailForm = bankchooserpanel.down('#bankdocspanel');
        me.postActivityHistory(dealid, actDetailForm);
    },

    onLoanSizingDoneBtnClick: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var acttabdealdetail = tabdealpanel.getActiveTab();
		
		var record = acttabdealdetail.record;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
		
        var loansizingdtfrm = tabdealpanel.down('dealdetailsformpanel').down('#loansizingdetailsform'); //me.getLoansizingdetailsform();
        me.postActivityHistory(dealid, loansizingdtfrm);
    },

    selBankContact: function(grid, record, item, index, e, eOpts) {
        console.log("Banks Contact Selected");
        var me = this;
        me.getBankchooserpanel().down('#bankdocspanel').getForm().findField('contactid').setValue(record.get('contactid'));
        me.getBankchooserpanel().down('#bankdocspanel').getForm().findField('contactname').setValue(record.get('name'));
        var eastregion = me.getBankchooserpanel().down('#bankselbtmpanel');
        var bankdocspanel = me.getBankchooserpanel().down('#bankdocspanel');
        eastregion.getLayout().setActiveItem(bankdocspanel);
    },

    onUpColumnItemClick: function(view, rowIndex, colIndex, item, e, record, row) {
        console.log("Up Button Click");
        console.log("rowIndex " + rowIndex);
        var me = this;
        if(rowIndex!==0)
        {
            //Get the previous Record
            var prevRec = view.getStore().getAt(rowIndex-1);

            var firstidsubdoc = prevRec.get('idSubxDoc');
            var firstdocid = record.get('docid');
            var firstselected = record.get('selected');
            var firstisglobal = record.get('isglobal');

            var secondidsubdoc = record.get('idSubxDoc');
            var seconddocid    = prevRec.get('docid');
            var secondselected = prevRec.get('selected');
            var secondisglobal = prevRec.get('isglobal');

            //Call the Put & change the position
            me.docUpDown(firstidsubdoc,firstdocid,firstselected,secondidsubdoc,seconddocid,secondselected,firstisglobal,secondisglobal);
        }
        console.log("DocID " + record.get('docid'));
        console.log("idSubmissionxDocs " + record.get('idSubxDoc'));
    },

    docUpDown: function(firstidsubdoc, firstdocid, firstselected, secondidsubdoc, seconddocid, secondselected, firstisglobal, secondisglobal) {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		
        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        if (bankselgrid.getSelectionModel().hasSelection()) {
            var row = bankselgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('idLoan'));
            var idsubmission = row.get('idLoan');

            var bankdocsgridpanel = bankchooserpanel.down('#bankdocsgridpanel');

            var data = [{
                'idSubxDoc': firstidsubdoc,
                'idDocument': firstdocid,
                'selected'  : firstselected,
                'isGlobal'  : firstisglobal,
                '@metadata' : {'checksum' : 'override'}
            },{
                'idSubxDoc': secondidsubdoc,
                'idDocument': seconddocid,
                'selected'  : secondselected,
                'isGlobal'  : secondisglobal,
                '@metadata' : {'checksum' : 'override'}
            }];

            var methodname = "PUT";

            bankdocsgridpanel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:SubxDoc',
                params: Ext.util.JSON.encode(data),
                scope:this,
                method:methodname,
                success: function(response, opts) {
                    bankdocsgridpanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(obj.statusCode == 200 || obj.statusCode == 201)
                    {
                        //Load the Docs Per Submission
                        me.loadDocsPerSubmission(idsubmission,context);
                    }
                    else{
                        console.log("Save Selected Failure.");
                        Ext.Msg.alert('Save Selected failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    bankdocsgridpanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Save Selected Docs', obj.errorMessage);
                }
            });
        }
        else{
            Ext.Msg.alert('Failed', "Please select submission.");
        }
    },

    onDownColumnItemClick: function(view, rowIndex, colIndex, item, e, record, row) {
        console.log("Down Button Click");
        console.log("rowIndex " + rowIndex);
        var me = this;
        if(rowIndex!==(view.getStore().getCount()-1))
        {
            //Get the previous Record
            var nextRec = view.getStore().getAt(rowIndex+1);

            var firstidsubdoc = nextRec.get('idSubxDoc');
            var firstdocid = record.get('docid');
            var firstselected = record.get('selected');
            var firstisglobal = record.get('isglobal');

            var secondidsubdoc = record.get('idSubxDoc');
            var seconddocid    = nextRec.get('docid');
            var secondselected = nextRec.get('selected');
            var secondisglobal = nextRec.get('isglobal');

            //Call the Put & change the position
            me.docUpDown(firstidsubdoc,firstdocid,firstselected,secondidsubdoc,seconddocid,secondselected,firstisglobal,secondisglobal);
        }
        //console.log("DocID " + record.get('docid'));
        //console.log("idSubmissionxDocs " + record.get('idSubmissionxDocs'));
    },

    onLoanPropertyGridRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        console.log("Loan Property Grid Right Click");
        var me = this;
        e.stopEvent();
        console.log("Show Menu for Loan Property Grid");
        if(this.getLoansizegridmenu()){
            this.getLoansizegridmenu().showAt(e.getXY());
        }
    },

    onLoanSizeGridMenuClick: function(menu, item, e, eOpts) {
        var me = this;
        var event = item.itemevent;

        if(event === "activitydone"){
            me.onLoanSizingDoneBtnClick();
        }
    },

    onBankDocsGridPanelRightClick: function(component, e, eOpts) {
        console.log("Bank Docs Grid Right click");
        e.stopEvent();
        this.showFileSubmissionGridMenu(e);
    },
	
	onBankDocsGridItemLongPress: function(view, record , item , index , e , eOpts){
		e.stopEvent();
        this.showFileSubmissionGridMenu(e);
	},

    onResetLoanSizeBtnClick: function(btn) {
        var me = this;
		
		var context = btn.up('tab-deal-detail');		
        var loansizingpanel = context.down('loansizingpanel');
		var loansizingdtfrm = loansizingpanel.down('loansizingdetailsform');
		
        var loansizegrid = loansizingpanel.down('#loanpropertygridpanel');
        if (loansizegrid.getSelectionModel().hasSelection()) {
            var record = loansizegrid.getSelectionModel().getSelection()[0];
			me.loadLoanSizingSetupPropertyRecord(context,record);
        }
    },
	
    showFileSubmissionGridMenu: function(e) {
        console.log("Show Menu for Bank Docs");
		var me = this;
        if(me.getFilesubmissiongridmenu()){
            me.getFilesubmissiongridmenu().showAt(e.getXY());
			
			var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
			var context = tabdealpanel.getActiveTab();
			var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
			var bankdocsgridpanel = bankchooserpanel.down('#bankdocsgridpanel');
	
			var row = context.record;
			var dealid = row.data.idDeal;
	
			if (bankdocsgridpanel.getSelectionModel().hasSelection()) {
				var submdocrec = bankdocsgridpanel.getSelectionModel().getSelection()[0];
				if(submdocrec.get('isglobal')){
					me.getFilesubmissiongridmenu().down('#removefilefromsubmission').hide();
				} else {
					me.getFilesubmissiongridmenu().down('#removefilefromsubmission').show();
				}
			}
        }
    },

    onBankDocsGridItemRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        e.stopEvent();
        this.showFileSubmissionGridMenu(e);
    },

    onFileSubGridMenuClick: function(menu, item, e, eOpts) {
        console.log("Bank Docs Menu Clicked");
        var me = this;
        var eventname = item.itemevent;
        if(eventname === "addfile"){
            console.log("Doc Add Menu Item Clicked");
			var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
			var context = tabdealpanel.getActiveTab();
		
			var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
			var addsubmissiondocpanel = context.down('dealdetailsformpanel').down('addsubmissiondocpanel');
            var docstempst = addsubmissiondocpanel.down('grid').getStore(); //('DocsTemp');
            docstempst.removeAll();
            addsubmissiondocpanel.getForm().reset();
        }
        else if(eventname === "filetemplate"){
            me.showTemplateSelGridPanel();
        }
        else if(eventname === "filerepository"){
            me.showDocSelPanel();
        }
        else if(eventname === "removefile"){
            me.removeFileFromSubmission();
        }
    },

    showTemplateSelGridPanel: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var eastregion = bankchooserpanel.down('#bankselbtmpanel');
		var templateselgridpanel;
		if(context.down('dealdetailsformpanel').down('templateselgridpanel')){
			console.log("Use Old Panel");
			templateselgridpanel = context.down('dealdetailsformpanel').down('templateselgridpanel');
		}
		else {
			console.log("Create New Panel");
			templateselgridpanel = Ext.create('DM2.view.TemplateSelGridPanel');//me.getTemplateselgridpanel();
		}
		eastregion.getLayout().setActiveItem(templateselgridpanel);

        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        var row = bankselgrid.getSelectionModel().getSelection()[0];
        var bankid = row.get('bankid');
        console.log(bankid);

        //Load Selected Bank's Contacts
        me.loadTemplates(templateselgridpanel);
        templateselgridpanel.down('form').getForm().reset();
    },

    loadTemplates: function(grid) {
        //Load Templates & add to submenu
        var me = this;
        var templatesst = grid.getStore();
		
		var templatesstproxy = templatesst.getProxy();
        /*templatesstproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var templatesstproxyCfg = templatesstproxy.config;
        templatesstproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		templatesst.setProxy(templatesstproxyCfg);		
        templatesst.load({
            scope: this,
            callback: function(records, operation, success) {
            }
        });
    },

    closeTemplateSelGridPaneBtnClick: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var templateselgridpanel = context.down('dealdetailsformpanel').down('templateselgridpanel');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		
        if (templateselgridpanel.getSelectionModel().hasSelection()) {
            var row = templateselgridpanel.getSelectionModel().getSelection()[0];
            console.log(row.get('idTemplate'));
            me.createDocFileFromTemplate(row.get('idTemplate'),context);
        }
        var eastregion = bankchooserpanel.down('#bankselbtmpanel');
        var bankdocspanel = bankchooserpanel.down('#bankdocspanel');
        eastregion.getLayout().setActiveItem(bankdocspanel);
    },

    createDocFileFromTemplate: function(templateid,context) {
        console.log("Create Doc File From Template");
        var me = this;
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var templateselgridpanel = context.down('dealdetailsformpanel').down('templateselgridpanel');
        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        if (bankselgrid.getSelectionModel().hasSelection()) {
            var row = bankselgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('idLoan'));
            var idsubmission = row.get('idLoan');

			var dealrec = context.record;
        	var dealid = dealrec.data.idDeal;

            var descval = templateselgridpanel.down('form').getForm().findField('description').getValue();
			var nameval = templateselgridpanel.down('form').getForm().findField('name').getValue();
            var clsfctnval = templateselgridpanel.down('form').getForm().findField('clsfctn').getValue();

            var methodname = "POST";
            var data = {
                'requestType' : "templateForSubmission",
                'paramater1'  :  idsubmission.toString(),
                'paramater2'  :  templateid.toString(),
				'paramater3'  :  nameval,
                'paramater4'  :  clsfctnval,
                'paramater5'  :  descval
            };

            var bankdocsgridpanel = bankchooserpanel.down('#bankdocsgridpanel');
            bankdocsgridpanel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
                scope:this,
                method:methodname,
                params: Ext.util.JSON.encode(data),
                success: function(response, opts) {
                    bankdocsgridpanel.setLoading(false);
                    console.log(response);
                    var obj = Ext.decode(response.responseText);
                    if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201)
                    {
                        //Load the Docs Per Submission
                        me.loadDocsPerSubmission(idsubmission,context);
                        me.loadGenFilesPerSubmission(idsubmission,context);
                        me.getController("DocController").loadDocsStore(dealid);
                    }
                    else{
                        console.log("Generate File Failed.");
                        Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    bankdocsgridpanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Generate File', obj.errorMessage);
                }
            });
        } else {
            Ext.Msg.alert('Failed', "Please select submission.");
        }
    },

    addFileFieldChange: function(filefield, value, eOpts) {
        var me = this;
        var newValue = value.replace(/C:\\fakepath\\/g, '');
        console.log(newValue);
        filefield.setRawValue(newValue);
        var adddocform = me.getAddsubmissiondocpanel().getForm();
        var filename = newValue.substr(0, newValue.lastIndexOf('.'));
        adddocform.findField('name').setValue(filename);

        var filesize = filefield.fileInputEl.dom.files[0].size;
        filesize = (filesize/1024).toFixed(2);
        adddocform.findField('docfilesize').setValue(filesize);
    },

    showDocSelPanel: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
	
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');		
		var eastregion = bankchooserpanel.down('#bankselbtmpanel');
		var docselpanel;
		if(context.down('dealdetailsformpanel').down('docselpanel')){
			console.log("Use Old Panel");
			docselpanel = context.down('dealdetailsformpanel').down('docselpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	docselpanel = Ext.create('DM2.view.DocSelPanel');//me.getDocselpanel();
		}
        eastregion.getLayout().setActiveItem(docselpanel);

        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        var row = bankselgrid.getSelectionModel().getSelection()[0];
        var bankid = row.get('bankid');
        console.log(bankid);

        //Load Documents
        var filterstarr = [];
        me.loadAllDocs(filterstarr,docselpanel.down('grid'));
    },

    closeDocSelPanel: function(panel) {
        var me = this;
		var context = panel.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        /*var tmpgrid = me.getTemplateselgridpanel();
        if (tmpgrid.getSelectionModel().hasSelection()) {
            var row = tmpgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('idTemplate'));
            me.createDocFileFromTemplate(row.get('idTemplate'));
        }*/
        var eastregion = bankchooserpanel.down('#bankselbtmpanel');
        var bankdocspanel = bankchooserpanel.down('#bankdocspanel');
        eastregion.getLayout().setActiveItem(bankdocspanel);
    },

    onClearLoanSizeBtnClick: function(btn) {
        var me = this;
		var context = btn.up('tab-deal-detail');		
        var loansizingpanel = context.down('loansizingpanel');
		var loansizingdtfrm = loansizingpanel.down('loansizingdetailsform');
		
		var activityid = loansizingdtfrm.getForm().findField('activityid').getValue();
        var idSetup = loansizingdtfrm.getForm().findField('idSetup').getValue();
        var idProperty = loansizingdtfrm.getForm().findField('idProperty').getValue();
        var idDeal = loansizingdtfrm.getForm().findField('idDeal').getValue();

        loansizingdtfrm.getForm().reset();

        loansizingdtfrm.getForm().findField('activityid').setValue(activityid);
        loansizingdtfrm.getForm().findField('idSetup').setValue(idSetup);
        loansizingdtfrm.getForm().findField('idProperty').setValue(idProperty);
        loansizingdtfrm.getForm().findField('idDeal').setValue(idDeal);
    },

    clearSubmissionBtnClick: function(btn) {
        var me = this;
		var context = btn.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var bankdetailfrm = bankchooserpanel.down('#bankdocspanel');

        //bankdetailfrm.getForm().findField('contactid').reset();
        //bankdetailfrm.getForm().findField('contactname').reset();
        bankdetailfrm.getForm().findField('submitdate').reset();
        bankdetailfrm.getForm().findField('submitnote').reset();
        bankdetailfrm.getForm().findField('submitmethod').reset();
		bankdetailfrm.getForm().findField('amountRequested').reset();
    },

    resetSubmissionBtnClick: function(btn) {
        console.log("Reset Submission Click");
        var me = this;
		
		var context = btn.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var bankdetailfrm = bankchooserpanel.down('#bankdocspanel');
		
        var bcselbankgrid = bankchooserpanel.down('#bcselbankgridpanel');
        if (bcselbankgrid.getSelectionModel().hasSelection()) {
            var record = bcselbankgrid.getSelectionModel().getSelection()[0];
			me.loadSubmissionRecordToForm(context,record);
        }
    },

    showAddSubmissionDocPanel: function() {
        var me = this;
        var eastregion = me.getBankchooserpanel().down('#bankselbtmpanel');
        var addsubmissiondocpanel = me.getAddsubmissiondocpanel();
        eastregion.getLayout().setActiveItem(addsubmissiondocpanel);

        var bankselgrid = me.getBankchooserpanel().down('#bcselbankgridpanel');
        var row = bankselgrid.getSelectionModel().getSelection()[0];
        var bankid = row.get('bankid');
        console.log(bankid);

        addsubmissiondocpanel.getForm().findField('name').focus();
        addsubmissiondocpanel.getForm().findField('adddoctype').setValue('normalupload');

        var bankdocsfrm = me.getBankchooserpanel().down('#bankdocspanel');
        var bankshrtname = bankdocsfrm.getForm().findField('shortName').getValue();
        var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();
        //var classification = "submission-"+bankshrtname+"-"+submissionid;
		var classification = "";
        addsubmissiondocpanel.getForm().findField('clsfctn').setValue(classification);
    },

    addSubmissionDocPanelClose: function(panel) {
        var me = this;
		var context = panel.up('tab-deal-detail');	
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var eastregion = bankchooserpanel.down('#bankselbtmpanel');
        var bankdocspanel = bankchooserpanel.down('#bankdocspanel');
        eastregion.getLayout().setActiveItem(bankdocspanel);
        me.submissionDocClearAllBtnClick(panel);
    },

    submissionDocUploadAllBtnClick: function(btn) {
        console.log("Doc Submit Button Clicked.");
        var me = this;
		var context = btn.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var addsubmissiondocpanel = context.down('dealdetailsformpanel').down('addsubmissiondocpanel');
		
        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        if (bankselgrid.getSelectionModel().hasSelection()) {
            var row = bankselgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('idLoan'));
            var idsubmission = row.get('idLoan');

			var dealrec = context.record;
        	var dealid = dealrec.data.idDeal;

            var docstempst = addsubmissiondocpanel.down('grid').getStore(); //('DocsTemp');
            docstempst.each(function(record) {

                var fileObj = record.get('fileobj');
                var filename = record.get('name');

                var ext = /^.+\.([^.]+)$/.exec(filename);
                if(ext === null){
                    ext = "";
                }else {
                    ext = ext[1];
                }

                var uploadmode =  record.get('uploadmode');

                if(uploadmode==="outlook"){
                    var data = [{
                        'name'           : filename,
                        'description'    : record.get('desc'),
                        'classification' : record.get('clsfctn'),
                        'size'           : record.get('filesize'),
                        'ext'            : ext,
                        'content'        : fileObj,
                        //'idDeal': dealid,
                        'SubxDocList':[{
                            'idLoan': idsubmission,
                            'selected': true
                        }]
                    }];

                    addsubmissiondocpanel.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        url:DM2.view.AppConstants.apiurl+'RSubmissionDocs',
                        params: Ext.util.JSON.encode(data),
                        scope:this,
                        success: function(response, opts) {
                            addsubmissiondocpanel.setLoading(false);
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 201){
                                //me.getController("DocController").loadDocsStore(dealid);
                                //Load Docs Per Submission
                                me.loadDocsPerSubmission(idsubmission,context);

                                addsubmissiondocpanel.getForm().reset();

                                var recindex = docstempst.indexOf(record);
                                if(recindex === (docstempst.getCount()-1)){
                                    me.addSubmissionDocPanelClose(addsubmissiondocpanel);
                                }
                            } else{
                                Ext.Msg.alert('Failed', "Please try again.");
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Login Failure.");
                            addsubmissiondocpanel.setLoading(false);
                            Ext.Msg.alert('Failed', "Please try again.");
                        }
                    });
                } else {
                    var reader = new FileReader();

                    reader.readAsDataURL(fileObj);

                    reader.onload = function (e) {
                        var conentarr = e.target.result.split("base64,");
                        var content = '';
                        if(conentarr.length > 1){
                            content = conentarr[1];
                        }
						
						var prepend = 'b64:';
						content = prepend + content;
						
                        var data = [{
                            'name'           : filename,
                            'description'    : record.get('desc'),
                            'classification' : record.get('clsfctn'),
                            'size'           : fileObj.size,
                            'ext'            : ext,
                            'content'        : content,
                            //'idDeal': dealid,
                            'SubxDocList':[{
                                 'idLoan': idsubmission,
                                 'selected': true
                             }]
                        }];

                        addsubmissiondocpanel.setLoading(true);
                        Ext.Ajax.request({
                            headers: {
                                'Content-Type': 'application/json',
                                Accept: 'application/json',
                                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                            },
                            url:DM2.view.AppConstants.apiurl+'RSubmissionDocs',
                            params: Ext.util.JSON.encode(data),
                            scope:this,
                            success: function(response, opts) {
                                record.set('uploadPercent',1);
                                addsubmissiondocpanel.setLoading(false);
                                var obj = Ext.decode(response.responseText);
                                if(obj.statusCode == 201){
                                    //me.getController("DocController").loadDocsStore(dealid);
                                    //Load Docs Per Submission
                                    me.loadDocsPerSubmission(idsubmission,context);

                                    addsubmissiondocpanel.getForm().reset();

                                    var recindex = docstempst.indexOf(record);
                                    if(recindex === (docstempst.getCount()-1)){
                                        me.addSubmissionDocPanelClose(addsubmissiondocpanel);
                                    }
                                } else {
                                    Ext.Msg.alert('Failed', "Please try again.");
                                }
                            },
                            failure: function(response, opts) {
                                console.log("Login Failure.");
                                addsubmissiondocpanel.setLoading(false);
                                Ext.Msg.alert('Failed', "Please try again.");
                            }
                        });
                    };
                    reader.onerror = function (e) {
                        console.log(e.target.error);
                    };
                }
            });
        } else {
            Ext.Msg.alert('Failed', "Please select submission.");
        }
    },

    submissionDocClearAllBtnClick: function(cmp) {
        var me = this;		
		var context = cmp.up('tab-deal-detail');	
		var addsubmissiondocpanel = context.down('dealdetailsformpanel').down('addsubmissiondocpanel');		
        var docstempst = addsubmissiondocpanel.down('grid').getStore(); //('DocsTemp');
        docstempst.removeAll();
        addsubmissiondocpanel.getForm().reset();
    },

    templateItemSelected: function(selModel, record, item, index, e, eOpts) {
        var me = this;
        console.log("Template Selected");
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		var templateselgridpanel = context.down('dealdetailsformpanel').down('templateselgridpanel');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		
		templateselgridpanel.down('form').getForm().loadRecord(record);
		
        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var bankshrtname = bankdocsfrm.getForm().findField('shortName').getValue();
        var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();
        var templateid = record.get('idTemplate');

        //var clasfnval = "submission-"+submissionid+"-"+bankshrtname+"-template-"+templateid;
		var clasfnval = "";
        templateselgridpanel.down('form').getForm().findField('clsfctn').setValue(clasfnval);
    },

    dropDocsToSubmission: function(node, data, overModel, dropPosition, dropHandlers, eOpts) {
        console.log("Drop Docs To Submissions.");
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var idsubmission = bankdocsfrm.getForm().findField('idsubmission').getValue();
        var docid = data.records[0].get('docid');

        //Post to Submission Has Document
        var methodname = "POST";
        var data = {
            'idLoan' : idsubmission,
            'idDocument'  :  docid
        };

        var bankdocsgridpanel = bankchooserpanel.down('#bankdocsgridpanel');
        bankdocsgridpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:SubxDoc',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                bankdocsgridpanel.setLoading(false);
                console.log(response);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                    //Load the Docs Per Submission
                    me.loadDocsPerSubmission(idsubmission,context);
                    me.loadGenFilesPerSubmission(idsubmission,context);
                } else {
                    console.log("Document Save Failed.");
                    Ext.Msg.alert('Document Save Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                bankdocsgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Document Save', obj.errorMessage);
                return false;
            }
        });
        return false;
    },

    genDocBtnClick: function(btn) {
        var me = this;
		var context = btn.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var idsubmission = bankdocsfrm.getForm().findField('idsubmission').getValue();
        var fileval     = bankdocsfrm.getForm().findField('genfilename').getValue();
		var filedescval = bankdocsfrm.getForm().findField('genfiledesc').getValue();
		var fileclsval  = bankdocsfrm.getForm().findField('genfileclsfctn').getValue();
		
		///For File Type
		fileval = fileval+".pdf";
		var row = context.record;
		var dealid = row.data.idDeal;
		
        var methodname = "POST";
        var data = {
            'requestType' : "Generate",
            'paramater1'  : idsubmission.toString(),
            'paramater2'  : fileval,
			'paramater3'  : filedescval,
			'paramater4'  : fileclsval
        };

        var bankdocsgridpanel = bankchooserpanel.down('#bankdocsgridpanel');
        bankdocsgridpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                bankdocsgridpanel.setLoading(false);
                console.log(response);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
					me.getController("DocController").loadDocsStore(dealid);
					me.getController("DealDetailController").loadDealDetailDocs(context,"deal");
	                //Load the Docs Per Submission					
                    me.loadDocsPerSubmission(idsubmission,context);
                    me.loadGenFilesPerSubmission(idsubmission,context);
                } else {
                    console.log("Generate File Failed.");
                    Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                bankdocsgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Generate File', obj.errorMessage);
            }
        });
    },

    loadAllDocs: function(filterstarr,grid) {
        var me = this;
        var documents_st = grid.getStore(); //('Docsall').getProxy();
		
		var documents_stproxy = documents_st.getProxy();
        /*documents_stproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var documents_stproxyCfg = documents_stproxy.config;
        documents_stproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		documents_st.setProxy(documents_stproxyCfg);		
        documents_st.load({
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success){
            }
        });
    },

    docsselgriditemselect: function(selModel, record, item, index, e, eOpts) {
        var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var docselpanel = context.down('dealdetailsformpanel').down('docselpanel');
        docselpanel.down('form').getForm().loadRecord(record);

        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var bankshrtname = bankdocsfrm.getForm().findField('shortName').getValue();
        var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();

        //var clasfnval = "submission-"+bankshrtname+"-"+submissionid;
		var clasfnval = "";
        docselpanel.down('form').getForm().findField('classification').setValue(clasfnval);
    },

    saveDocRepositoryBtnClick: function(btn) {
        console.log("Create Doc File From Repo");
        var me = this;
		var context = btn.up('tab-deal-detail');
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		var docselpanel = context.down('dealdetailsformpanel').down('docselpanel');
        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        if (bankselgrid.getSelectionModel().hasSelection()) {
            var row = bankselgrid.getSelectionModel().getSelection()[0];
            console.log(row.get('idLoan'));
            var idsubmission = row.get('idLoan');

			var dealrow = context.record;
        	var dealid = dealrow.data.idDeal;
			
            var docselfrm = docselpanel.down('form');
            var idDocument = docselfrm.getForm().findField('idDocument').getValue();
            var clsfctnval = docselfrm.getForm().findField('classification').getValue();
            var nameval = docselfrm.getForm().findField('name').getValue();
            var descval = docselfrm.getForm().findField('description').getValue();

            var methodname = "POST";
            var data = {
                'requestType' : "RepositoryCopy",
                'paramater1'  :  idsubmission.toString(),
                'paramater2'  :  idDocument.toString(),
                'paramater3'  :  nameval,
                'paramater4'  :  clsfctnval,
                'paramater5'  :  descval
            };

            docselfrm.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
                scope:this,
                method:methodname,
                params: Ext.util.JSON.encode(data),
                success: function(response, opts) {
                    docselfrm.setLoading(false);
                    console.log(response);
                    var obj = Ext.decode(response.responseText);
                    if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                        //Load the Docs Per Submission
                        me.closeDocSelPanel(docselpanel);
                        me.loadDocsPerSubmission(idsubmission,context);
                        me.loadGenFilesPerSubmission(idsubmission,context);
                        me.getController("DocController").loadDocsStore(dealid);
                    } else {
                        console.log("Generate File Failed.");
                        Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    docselfrm.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Generate File', obj.errorMessage);
                }
            });
        } else {
            Ext.Msg.alert('Failed', "Please select submission.");
        }
    },

    onEmailSubmissionBtnClick: function() {
        console.log("Email Submission CLick");
        /*
        var me = this;
        me.showEmailfaxsubmission();
        me.getEmailfaxsubmission().setTitle("Send Email");
        me.getEmailfaxsubmission().getForm().findField('requestType').setValue('mail');
        */
    },

    onFaxSubmissionBtnClick: function() {
        console.log("Fax Submission CLick");
        /*
        var me = this;
        me.showEmailfaxsubmission();
        me.getEmailfaxsubmission().setTitle("Send Fax");
        me.getEmailfaxsubmission().getForm().findField('requestType').setValue('fax');
        */
    },
	
	onPrintSubmissionBtnClick: function(btn){
		console.log("Print Submission Click");
		//window.open("./resources/printSnapshot.php?apikey="+apikey+"&username="+userid+"&dealid="+dealid+"&apiurl="+DM2.view.AppConstants.apiurl,"_blank");
	},

    addSubmDocItemSelect: function(selModel, record, index, eOpts) {
		var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		var addsubmissiondocpanel = context.down('dealdetailsformpanel').down('addsubmissiondocpanel');
        var adddocform = addsubmissiondocpanel.getForm();
        adddocform.findField('name').focus();
        adddocform.loadRecord(record);
        adddocform.findField('docfilesize').setValue(record.get('size'));
        console.log(record.get('fileobj'));
		
		var extension = me.getController('GeneralDocSelController').getFilePathExtension(record.get('name'));
		var filenameonly = me.getController('GeneralDocSelController').getFilePathName(record.get('name'));
		adddocform.findField('name').setValue(filenameonly);
		adddocform.findField('ext').setValue(extension);
    },

    onChangeAddSubmDocField: function(fld, e, eOpts) {
        var me = this;
        console.log("Doc Item Save");
		var context = fld.up('tab-deal-detail');

        var adddocform = context.down('dealdetailsformpanel').down('addsubmissiondocpanel').getForm();
        var nameval = adddocform.findField('name').getValue();
        var descval = adddocform.findField('desc').getValue();
        var clsfctnval = adddocform.findField('clsfctn').getValue();

        var docgrid = context.down('dealdetailsformpanel').down('addsubmissiondocpanel').down('grid');
        if(docgrid.getSelectionModel().hasSelection()){
            var row = docgrid.getSelectionModel().getSelection()[0];
            row.set('name',nameval,true);
            row.set('desc',descval,true);
            row.set('clsfctn',clsfctnval,true);
        }		
    },

    addSubmDocFileFieldChange: function(field, value, eOpts) {
        console.log("File Selected through Add Doc Button");
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
        if(field.fileInputEl.dom.files.length > 0){
			var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
			var eastregion = bankchooserpanel.down('#bankselbtmpanel');
			var addsubmissiondocpanel;
			if(context.down('dealdetailsformpanel').down('addsubmissiondocpanel')){
				console.log("Use Old Panel");
				addsubmissiondocpanel = context.down('dealdetailsformpanel').down('addsubmissiondocpanel');
			} else {
				//create new Addsubmissiondocpanel Panel
				console.log("Create New Panel");
				addsubmissiondocpanel = Ext.create('DM2.view.AddSubmissionDocPanel');//me.getAddsubmissiondocpanel();
			}
			eastregion.getLayout().setActiveItem(addsubmissiondocpanel);	
			
            var file = field.fileInputEl.dom.files[0];

            var filesize = (file.size/1024).toFixed(2);

            var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
            var bankshrtname = bankdocsfrm.getForm().findField('shortName').getValue();
            var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();
            //var classification = "submission-"+bankshrtname+"-"+submissionid;
			var classification = "";
            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : file.name,
                'size' : filesize,
                'clsfctn' :classification,
                'desc' :'',
                'fileobj':file
            });
            var docstempst = addsubmissiondocpanel.down('grid').getStore(); //('DocsTemp');
            docstempst.add(docrec);
            /////

            addsubmissiondocpanel.down('grid').getSelectionModel().select(0);
            me.getFilesubmissiongridmenu().hide();
        }
    },

    onEmailFaxSubmissionClose: function() {
        var me = this;
        var eastregion = me.getBankchooserpanel().down('#bankselbtmpanel');
        var bankdocspanel = me.getBankchooserpanel().down('#bankdocspanel');
        eastregion.getLayout().setActiveItem(bankdocspanel);
    },

    showEmailfaxsubmission: function() {
        var me = this;
        var eastregion = me.getBankchooserpanel().down('#bankselbtmpanel');
        var emailfaxsubmission = me.getEmailfaxsubmission();
        eastregion.getLayout().setActiveItem(emailfaxsubmission);

        var genfilesgrid = me.getBankchooserpanel().down('#genfilespersubmissiongridpanel');
        var row = genfilesgrid.getSelectionModel().getSelection()[0];
        var docid = row.get('docid');
        var docname = '<a target="_blank" href="'+row.get('url')+'">'+row.get('name')+'</a>';
        console.log(docid);

        emailfaxsubmission.getForm().reset();
        emailfaxsubmission.getForm().findField('docid').setValue(docid);
        emailfaxsubmission.getForm().findField('docname').setValue(docname);
    },

    onSendEmailResetBtnClick: function() {
        var me = this;
        var emailfaxsubmission = me.getEmailfaxsubmission();
        emailfaxsubmission.getForm().findField('docsubject').reset();
        emailfaxsubmission.getForm().findField('docbody').reset();
    },

    onSendEmailBtnClick: function() {
        var me = this;
        var emailfaxsubmission = me.getEmailfaxsubmission();
        var rType = emailfaxsubmission.getForm().findField('requestType').getValue();

        var obj = {
            to    : emailfaxsubmission.getForm().findField('docto').getValue(),
            subject : emailfaxsubmission.getForm().findField('docsubject').getValue(),
            body  : emailfaxsubmission.getForm().findField('docbody').getValue(),
            docid : emailfaxsubmission.getForm().findField('docid').getValue()
        };
        var methodname = "POST";
        var data = {
            'requestType' : rType,
            'paramater1'  : obj
        };

        emailfaxsubmission.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                emailfaxsubmission.setLoading(false);
                console.log(response);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                    me.onEmailFaxSubmissionClose();
                } else {
                    console.log("Send Email Failed.");
                    Ext.Msg.alert('Send Email Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                emailfaxsubmission.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Send Email', obj.errorMessage);
            }
        });
    },

    showGlobalSubmission: function() {
        console.log("Show Global Submission");
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var acttabdealdetail = tabdealpanel.getActiveTab();
		
		var eastregion = acttabdealdetail.down('dealdetailsformpanel').down('[region=east]');
		var globaldocsselgridpanel;
		if(acttabdealdetail.down('dealdetailsformpanel').down('globaldocsselgridpanel')){
			console.log("Use Old Panel");
			globaldocsselgridpanel = acttabdealdetail.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	globaldocsselgridpanel = Ext.create('DM2.view.GlobalDocsSelGridPanel');//me.getGlobaldocsselgridpanel();
		}
        eastregion.getLayout().setActiveItem(globaldocsselgridpanel);

        console.log("first Global Submission");
		var record = acttabdealdetail.record;
		var dealid = record.data.idDeal;
		/*
        var filterstarr = [];
        var filterst = "isGlobal = 'true'";
        filterstarr.push(filterst);

        console.log("Second Global Submission");

        var filterst = "idDeal = '"+dealid+"'";
        filterstarr.push(filterst);

        console.log("Filter Submission");

        //Get the Global Submission Id from server if exist else create new one.
        globaldocsselgridpanel.getView().setLoading(true);
        console.log("Calling");
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Submission',
            method:'GET',
            params:{
                filter :filterstarr
            },
            success: function(response, opts) {
                globaldocsselgridpanel.getView().setLoading(false);
                console.log(response);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201)
                {
                    if(obj.length>0){
                        var submissionid = obj[0].idSubmission;
                        console.log("subm id"+submissionid);
                        globaldocsselgridpanel.config.submissionid = submissionid;*/
                        me.loadGlobalDocsPerSubmission(0,acttabdealdetail);
                    /*}
                    else{
                        console.log("Create Submission");
                        me.createGlobalSubmission(acttabdealdetail);
                    }
                }
                else{
                    console.log("Send Email Failed.");
                    Ext.Msg.alert('Send Email Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                globaldocsselgridpanel.getView().setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Send Email', obj.errorMessage);
            }
        });*/
    },

    closeGlobalDocsSelGridPanel: function(panel) {
        var me = this;
		
		var context = panel.up('tab-deal-detail');
        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('bankchooserpanel'));
		
        ///Load Docs for submission, If any submission is selected
        var submgrid = context.down('bankchooserpanel').down('#bcselbankgridpanel');
        if (submgrid.getSelectionModel().hasSelection()) {
            var row = submgrid.getSelectionModel().getSelection()[0];
            var submissionid = row.get('idLoan');
            me.loadDocsPerSubmission(submissionid,context);
        }
    },

    loadGlobalDocsPerSubmission: function(submissionid,context) {
        console.log("Load Global Docs Per Submission");
        var me = this;
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		
        var filterstarr = [];

        var filterst = 'dealid = '+dealid;
        filterstarr.push(filterst);

        //var filterst1 = "clsfctn != 'generated'";
        //filterstarr.push(filterst1);

        //var filterst1 = "isglobal = 'true'";
        //filterstarr.push(filterst1);

        var glbdocssubmission_st = globaldocsselgridpanel.getStore();
        glbdocssubmission_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        glbdocssubmission_st.load({
            scope: this,
            params:{
                filter :filterstarr
            }
        });
    },

    createGlobalSubmission: function(context) {
        var me = this;
		
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
        globaldocsselgridpanel.setLoading(true);

		var record = context.record;
        var dealid = record.data.idDeal;
		
        var data = {
            'idDeal' : dealid,
            'idBank' : 1,
            'isGlobal'  : true
        };
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Submission',
            scope:this,
            method:'POST',
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                globaldocsselgridpanel.setLoading(false);
                console.log(response);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                    var submissionid = obj.txsummary[0].idSubmission;
                    console.log("subm id"+submissionid);
                    globaldocsselgridpanel.config.submissionid = submissionid;
                    me.loadGlobalDocsPerSubmission(submissionid,context);
                } else {
                    console.log("Create Global Submission Failed.");
                    Ext.Msg.alert('Create Global Submission Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                globaldocsselgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Create Global Submission', obj.errorMessage);
            }
        });
    },

    onGlobalDocsGridPanelRightClick: function(cmp, e, eOpts) {
        e.stopEvent();
        this.showGlobalDocsGridMenu(e);
    },

    onGlobalSubmissionDocDrop: function(cmp, e, file) {
        console.log("Done Reading");
        var me = this;
        console.log(file);
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var acttabdealdetail = tabdealpanel.getActiveTab();
		
		var eastregion = acttabdealdetail.down('dealdetailsformpanel').down('[region=east]');
		var addglobaldocpanel;
		if(acttabdealdetail.down('dealdetailsformpanel').down('addglobaldocpanel')){
			console.log("Use Old Panel");
			addglobaldocpanel = acttabdealdetail.down('dealdetailsformpanel').down('addglobaldocpanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	addglobaldocpanel = Ext.create('DM2.view.AddGlobalDocPanel');//me.getAddglobaldocpanel();
		}
        eastregion.getLayout().setActiveItem(addglobaldocpanel);

        var filesize = (file.size/1024).toFixed(2);

		var globaldocsselgridpanel = acttabdealdetail.down('dealdetailsformpanel').down('globaldocsselgridpanel');		
        //var classification = "submission-"+globaldocsselgridpanel.config.submissionid;
		var classification = "";

        ///// Add the file item to Store
        var docrec = Ext.create('DM2.model.Document',{
            'name' : file.name,
            'size' : filesize,
            'clsfctn' :classification,
            'desc' :'',
            'fileobj':file
        });
        var docstempst = addglobaldocpanel.down('grid').getStore();//Ext.getStore('DocsTemp');
        docstempst.add(docrec);

        addglobaldocpanel.down('grid').getSelectionModel().select(0);

        addglobaldocpanel.getForm().findField('name').focus();
    },
	
	onGlobalDocGridPanelRender: function(panel){
		var me = this;
		panel.getEl().on("longpress",function(e, t, eOpts) {
			console.log("Show Menu After Global Docs Render.");
			e.stopEvent();
			me.showGlobalDocsGridMenu(e);
		});
	},
	
    onGlobalDocsGridItemRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        e.stopEvent();
        this.showGlobalDocsGridMenu(e);
    },

    showGlobalDocsGridMenu: function(e) {
        console.log("Show Menu for Global Docs");
        if(this.getGlobaldocsgridmenu()){			
            this.getGlobaldocsgridmenu().showAt(e.getXY());
        }
    },

    globalDocsGridMenuClick: function(menu, item, e, eOpts) {
        console.log("Global Docs Menu Clicked");
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
        var eventname = item.itemevent;
        if(eventname === "addfile"){
            console.log("Doc Add Menu Item Clicked");
			if(context.down('dealdetailsformpanel').down('addglobaldocpanel')){
				console.log("Use Old Panel");
				addglobaldocpanel = context.down('dealdetailsformpanel').down('addglobaldocpanel');
				var docstempst = addglobaldocpanel.down('grid').getStore();
				docstempst.removeAll();
				addglobaldocpanel.getForm().reset();
			}
        } else if(eventname === "filetemplate"){
            me.showGlobalTemplateSelGridPanel();
        } else if(eventname === "filerepository"){
            me.showGlobalDocSelRepositoryPanel();
        } else if(eventname === "removefile"){
            me.removeGlobalDocMenuItemClick();
        }
    },

    globalAddFileFieldChange: function(field, value, eOpts) {
        console.log("File Selected through Global Add Doc Button");
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		
        if(field.fileInputEl.dom.files.length > 0){

			var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			var addglobaldocpanel;
			if(context.down('dealdetailsformpanel').down('addglobaldocpanel')){
				console.log("Use Old Panel");
				addglobaldocpanel = context.down('dealdetailsformpanel').down('addglobaldocpanel');
			} else {
				console.log("Create New Panel");
				addglobaldocpanel = Ext.create('DM2.view.AddGlobalDocPanel');//me.getAddglobaldocpanel();
			}
			eastregion.getLayout().setActiveItem(addglobaldocpanel);
			
            var file = field.fileInputEl.dom.files[0];

            var filesize = (file.size/1024).toFixed(2);

            //var classification = "submission-"+globaldocsselgridpanel.config.submissionid;
			var classification = "";

            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : file.name,
                'size' : filesize,
                'clsfctn' :classification,
                'desc' :'',
                'fileobj':file
            });
			var docstempst = addglobaldocpanel.down('grid').getStore();
            //docstempst.removeAll();
            docstempst.add(docrec);
            /////

            addglobaldocpanel.down('grid').getSelectionModel().select(0);
            me.getGlobaldocsgridmenu().hide();
        }
    },

    closeAddGlobalDocPanel: function(panel) {
        var me = this;
		var context = panel.up('tab-deal-detail');
        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
        eastregion.getLayout().setActiveItem(globaldocsselgridpanel);

        var docstempst = panel.down('grid').getStore(); //('DocsTemp');
        docstempst.removeAll();
    },

    addGlobalDocItemSelect: function(selModel, record, index, eOpts) {
		var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');		 	
		var addglobaldocpanel = context.down('dealdetailsformpanel').down('addglobaldocpanel');
        var adddocform = addglobaldocpanel.getForm();
        adddocform.findField('name').focus();
        adddocform.loadRecord(record);
        adddocform.findField('docfilesize').setValue(record.get('size'));
        console.log(record.get('fileobj'));
		
		var extension = me.getController('GeneralDocSelController').getFilePathExtension(record.get('name'));
		var filenameonly = me.getController('GeneralDocSelController').getFilePathName(record.get('name'));
		adddocform.findField('name').setValue(filenameonly);
		adddocform.findField('ext').setValue(extension);
    },
	
    globalDocUploadAllBtnClick: function(btn) {
        console.log("Doc Submit Button Clicked.");
        var me = this;
		var context = btn.up('tab-deal-detail');
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
        var idsubmission = globaldocsselgridpanel.config.submissionid;

		var row = context.record;
		var dealid = row.data.idDeal;
		
		var addglobaldocpanel = context.down('dealdetailsformpanel').down('addglobaldocpanel');
        var docstempst = addglobaldocpanel.down('grid').getStore(); //('DocsTemp');
        docstempst.each(function(record) {

            var fileObj = record.get('fileobj');
            var filename = record.get('name');

            var ext = /^.+\.([^.]+)$/.exec(filename);
            if(ext === null){
                ext = "";
            }else {
                ext = ext[1];
            }

            var uploadmode =  record.get('uploadmode');
            console.log("UploadMode"+uploadmode);
            console.log("idsubmission"+idsubmission);
            if(uploadmode==="outlook"){
                var allsubm = [];
                var globalsubmid = {
					'idDeal' : dealid//,
                    //'idSubmission': idsubmission,
                    //'isGlobal': true
                };
                allsubm.push(globalsubmid);
                var data = [{
                    'name'           : filename,
                    'description'    : record.get('desc'),
                    'classification' : record.get('clsfctn'),
                    'size'           : record.get('filesize'),
                    'ext'            : ext,
                    'content'        : fileObj,
                    //'idDeal': dealid,
                    'GSubxDocList':allsubm
                }];

                addglobaldocpanel.setLoading(true);
                Ext.Ajax.request({
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                    },
                    url:DM2.view.AppConstants.apiurl+'GSubmissionxDocs',
                    params: Ext.util.JSON.encode(data),
                    scope:this,
                    success: function(response, opts) {
                        addglobaldocpanel.setLoading(false);
                        var obj = Ext.decode(response.responseText);
                        if(obj.statusCode == 201){
                            //me.getController("DocController").loadDocsStore(dealid);
                            //Load Global Docs Per Submission
                            me.loadGlobalDocsPerSubmission(idsubmission,context);

                            addglobaldocpanel.getForm().reset();
                            var recindex = docstempst.indexOf(record);
                            if(recindex === (docstempst.getCount()-1)){
                                me.closeAddGlobalDocPanel(addglobaldocpanel);
                            }
                        } else {
                            Ext.Msg.alert('Failed', "Please try again.");
                        }
                    },
                    failure: function(response, opts) {
                        console.log("Login Failure.");
                        addglobaldocpanel.setLoading(false);
                        Ext.Msg.alert('Failed', "Please try again.");
                    }
                });
            } else {
                var reader = new FileReader();

                reader.readAsDataURL(fileObj);

                reader.onload = function (e) {
                    var conentarr = e.target.result.split("base64,");
                    var content = '';
                    if(conentarr.length > 1){
                        content = conentarr[1];
                    }
					
					var prepend = 'b64:';
					content = prepend + content;
					
                    //console.log(content);
                    var allsubm = [];
                    var globalsubmid = {
						'idDeal' : dealid//,
                        //'idSubmission': idsubmission,
                        //'isGlobal': true
                    };
                    allsubm.push(globalsubmid);

                    var data = [{
                        'name'           : filename,
                        'description'    : record.get('desc'),
                        'classification' : record.get('clsfctn'),
                        'size'           : fileObj.size,
                        'ext'            : ext,
                        'content'        : content,
                        //'idDeal': dealid,
                        'GSubxDocList':allsubm
                    }];

                    addglobaldocpanel.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        url:DM2.view.AppConstants.apiurl+'GSubmissionDocs',
                        params: Ext.util.JSON.encode(data),
                        scope:this,
                        success: function(response, opts) {                    
                            addglobaldocpanel.setLoading(false);
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 201){
                                //me.getController("DocController").loadDocsStore(dealid);
                                //Load Global Docs Per Submission
                                me.loadGlobalDocsPerSubmission(idsubmission,context);

                                addglobaldocpanel.getForm().reset();
                                var recindex = docstempst.indexOf(record);
                                if(recindex === (docstempst.getCount()-1)){
                                    me.closeAddGlobalDocPanel(addglobaldocpanel);
                                }
                            } else {
                                Ext.Msg.alert('Failed', "Please try again.");
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Login Failure.");
                            addglobaldocpanel.setLoading(false);
                            Ext.Msg.alert('Failed', "Please try again.");
                        }
                    });
                };
                reader.onerror = function (e) {
                    console.log(e.target.error);
                };
            }
        });
    },

    showGlobalTemplateSelGridPanel: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();

		var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
		var glbtemplateselgridpanel;
		if(context.down('dealdetailsformpanel').down('globaltemplateselgridpanel')){
			console.log("Use Old Panel");
			glbtemplateselgridpanel = context.down('dealdetailsformpanel').down('globaltemplateselgridpanel');
		} else {
			console.log("Create New Panel");
			glbtemplateselgridpanel = Ext.create('DM2.view.GlobalTemplateSelGridPanel');//me.getGlobaltemplateselgridpanel();
		}
		eastregion.getLayout().setActiveItem(glbtemplateselgridpanel);
		
        //Load Selected Bank's Contacts
        me.loadTemplates(glbtemplateselgridpanel);
        glbtemplateselgridpanel.down('form').getForm().reset();
    },

    closeGlobalTemplateSelGridPanel: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		
		var tmpgrid = context.down('dealdetailsformpanel').down('globaltemplateselgridpanel');
        if (tmpgrid.getSelectionModel().hasSelection()) {
            var row = tmpgrid.getSelectionModel().getSelection()[0];
            me.createGlobalDocFileFromTemplate(row.get('idTemplate'),context);
        }
        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
        eastregion.getLayout().setActiveItem(globaldocsselgridpanel);
    },

    globalTemplateSelGridItemSelect: function(selModel, record, index, eOpts) {
        var me = this;
        console.log("Template Selected");
		var context = selModel.view.ownerCt.up('tab-deal-detail');	
		var globaltemplateselgridpanel = context.down('dealdetailsformpanel').down('globaltemplateselgridpanel');
		globaltemplateselgridpanel.down('form').getForm().loadRecord(record);
		
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
        var submissionid = globaldocsselgridpanel.config.submissionid;
        var templateid = record.get('idTemplate');

        //var clasfnval = "submission-"+submissionid+"-template-"+templateid;
		var clasfnval = "";
        globaltemplateselgridpanel.down('form').getForm().findField('clsfctn').setValue(clasfnval);
    },

    loadSubmissionPerDeal: function(context) {
        var me = this;
		
		var record = context.record;
        var dealid = record.data.idDeal;
		
        var filterstarr = [];
        var filterst = 'dealid = '+dealid;
        filterstarr.push(filterst);

		//var filterst = "isSubmitted = 1";
        //filterstarr.push(filterst);
        me.getController('BankController').loadSelBanks(filterstarr,context);
    },

    createGlobalDocFileFromTemplate: function(templateid,context) {
        console.log("Create Doc File From Template");
        var me = this;
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		
		var globaltemplateselgridpanel = context.down('dealdetailsformpanel').down('globaltemplateselgridpanel');
		
        var idsubmission = globaldocsselgridpanel.config.submissionid;

		var row = context.record;
		var dealid = row.data.idDeal;

        var descval = globaltemplateselgridpanel.down('form').getForm().findField('description').getValue();
		var nameval = globaltemplateselgridpanel.down('form').getForm().findField('name').getValue();
        var clsfctnval = globaltemplateselgridpanel.down('form').getForm().findField('clsfctn').getValue();

        var methodname = "POST";
        var data = {
			'requestType' : "GtemplateForSubmission",
            //'requestType' : "templateForSubmission",
            //'paramater1'  :  idsubmission.toString(),
			'paramater1'  :  dealid.toString(),
            'paramater2'  :  templateid.toString(),
            'paramater3'  :  nameval,
            'paramater4'  :  clsfctnval,
            'paramater5'  :  descval
        };

        globaltemplateselgridpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                globaltemplateselgridpanel.setLoading(false);
                console.log(response);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                    //Load the Docs Per Submission
                    me.loadGlobalDocsPerSubmission(idsubmission,context);
                    me.loadGenFilesPerSubmission(idsubmission,context);
                    me.getController("DocController").loadDocsStore(dealid);
                } else {
                    console.log("Generate File Failed.");
                    Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                globaltemplateselgridpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Generate File', obj.errorMessage);
            }
        });
    },

    showGlobalDocSelRepositoryPanel: function() {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();

		var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
		var glbdocselrepogridpanel;
		if(context.down('dealdetailsformpanel').down('globaldocselrepositorypanel')){
			console.log("Use Old Panel");
			glbdocselrepogridpanel = context.down('dealdetailsformpanel').down('globaldocselrepositorypanel');
		} else {
			console.log("Create New Panel");
			glbdocselrepogridpanel = Ext.create('DM2.view.GlobalDocSelRepositoryPanel');//me.getGlobaldocselrepositorypanel();
		}
		eastregion.getLayout().setActiveItem(glbdocselrepogridpanel);

        //Load Documents
        var filterstarr = [];
        me.loadAllDocs(filterstarr,glbdocselrepogridpanel.down('grid'));
    },

    closeGlobalDocSelRepositoryPanel: function(panel) {
        var me = this;
		var context = panel.up('tab-deal-detail');	
        var eastregion =  context.down('dealdetailsformpanel').down('[region=east]');
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
        eastregion.getLayout().setActiveItem(globaldocsselgridpanel);
    },

    globalDocsSelRepositoryGridItemSelect: function(selModel, record, index, eOpts) {
        var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		var glbdocselrepogridpanel = context.down('dealdetailsformpanel').down('globaldocselrepositorypanel');
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		
        glbdocselrepogridpanel.down('form').getForm().loadRecord(record);

        var submissionid = globaldocsselgridpanel.config.submissionid;

        //var clasfnval = "submission-"+submissionid;
		var clasfnval = "";
        glbdocselrepogridpanel.down('form').getForm().findField('classification').setValue(clasfnval);
    },

    saveGlobalDocRepositoryBtnClick: function(btn) {
        console.log("Create Doc File From Repo");
        var me = this;
		var context = btn.up('tab-deal-detail');
		var glbdocselrepopanel = context.down('dealdetailsformpanel').down('globaldocselrepositorypanel');
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		
        var idsubmission = globaldocsselgridpanel.config.submissionid;
		var row = context.record;
		var dealid = row.data.idDeal;

        var docselfrm = glbdocselrepopanel.down('form');
        var idDocument = docselfrm.getForm().findField('idDocument').getValue();
        var clsfctnval = docselfrm.getForm().findField('classification').getValue();
        var nameval = docselfrm.getForm().findField('name').getValue();
        var descval = docselfrm.getForm().findField('description').getValue();

        var methodname = "POST";
        var data = {
            //'requestType' : "RepositoryCopy",
			'requestType' : "GRepositoryCopy",
            //'paramater1'  :  idsubmission.toString(),
			'paramater1'  :  dealid.toString(),
            'paramater2'  :  idDocument.toString(),
            'paramater3'  :  nameval,
            'paramater4'  :  clsfctnval,
            'paramater5'  :  descval
        };

        docselfrm.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                docselfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                    //Load the Docs Per Submission
                    me.closeGlobalDocSelRepositoryPanel(glbdocselrepopanel);
                    me.loadGlobalDocsPerSubmission(idsubmission,context);
                    me.getController("DocController").loadDocsStore(dealid);
                } else {
                    console.log("Generate File Failed.");
                    Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                docselfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Generate File', obj.errorMessage);
            }
        });
    },

    getGlobalDocFromOutlook: function(fna, fca) {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');		
		var addglobaldocpanel = context.down('dealdetailsformpanel').down('addglobaldocpanel');
        //console.log(fna);
        for (var i=0; i<fna.length; i++) {
            //console.log(fna[i]);
            //console.log(fca[i]);
            var filename = fna[i];
            var filecnt = fca[i];
            //var objArray = new VBArray(filecnt);
            //var bArray = objArray.toArray();
            //var filesize = bArray.length;
            var filesize = filecnt.length;
            var filesizekb = (filesize/1024).toFixed(2);
            //var base64st = me.converByteToBase64(bArray);

            //var classification = "submission-"+globaldocsselgridpanel.config.submissionid;
			var classification = "";

            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : filename,
                'size' : filesizekb,
                'clsfctn' :classification,
                'desc' :'',
                'fileobj':filecnt,
                'uploadmode':'outlook',
                'filesize':filesize
            });
            var docstempst = addglobaldocpanel.down('grid').getStore(); //('DocsTemp');
            docstempst.add(docrec);
            /////
        }
		
		var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
		var addglobaldocpanel;
		if(context.down('dealdetailsformpanel').down('addglobaldocpanel')){
			console.log("Use Old Panel");
			addglobaldocpanel = context.down('dealdetailsformpanel').down('addglobaldocpanel');
		}
		else {
			console.log("Create New Panel");
			addglobaldocpanel = Ext.create('DM2.view.AddGlobalDocPanel');//me.getAddglobaldocpanel();
		}
		eastregion.getLayout().setActiveItem(addglobaldocpanel);
		
        addglobaldocpanel.down('grid').getSelectionModel().select(0);
        addglobaldocpanel.getForm().findField('name').focus();
    },

    saveContactToSubmissionBtnClick: function() {
        var me = this;

        var submcnts_st = Ext.getStore('SubmissionContacts');

        var bankselcntgrid = me.getBankselcontactsgridpanel();
        var bankdocsfrm = me.getBankchooserpanel().down('#bankdocspanel');

        var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();

        if (bankselcntgrid.getSelectionModel().hasSelection()) {
            var pselarr = bankselcntgrid.getSelectionModel().getSelection();

            var Submission_has_Contact = [];

            for(var j=0;j<pselarr.length;j++){
                var item = pselarr[j];
                var flag = true;

                submcnts_st.each(function(subcntrec){
                    if(subcntrec.get('idContact')===item.get('contactid')){
                      flag = false;
                    }
                });

                if(flag){
                    var Submission_has_Contact_item = {
                        "@metadata": {
                          "entity": "SubxCont",
                          "action": "INSERT"
                        },
                        "idContact": item.get('contactid'),
                        "idLoan": submissionid
                    };
                    Submission_has_Contact.push(Submission_has_Contact_item);
                }
            }

            ///Get the Contacts for Delete
            submcnts_st.each(function(subcntrec){
                var flag = true;
                for(var j=0;j<pselarr.length;j++){
                    var item = pselarr[j];
                    if(subcntrec.get('idContact')===item.get('contactid')){
                        flag = false;
                    }
                }
                if(flag){
                    var Submission_has_Contact_item = {
                        "@metadata": {
                            "href": DM2.view.AppConstants.apiurl+"mssql:SubxCont/"+subcntrec.get('idSubxCont'),
                            "checksum": "override",
                            "action": "DELETE"
                        }
                    };
                    Submission_has_Contact.push(Submission_has_Contact_item);
                }
            });
        }

        var submissiongrid = me.getBankchooserpanel().down('#bcselbankgridpanel');
        var submrow = submissiongrid.getSelectionModel().getSelection()[0];
        var bankid = submrow.get('bankid');

        if(Submission_has_Contact.length>0){
            me.getBankselcontactsgridpanel().setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:SubxCont',
                params: Ext.util.JSON.encode(Submission_has_Contact),
                scope:this,
                method:'PUT',
                success: function(response, opts) {
                    me.getBankselcontactsgridpanel().setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(obj.statusCode == 200 || obj.statusCode == 201){
                        me.loadSubmissionContactsAfterSave(submissionid,bankid); //Load Submission Cnts
                    } else {
                        console.log("Save contact failure.");
                        Ext.Msg.alert('Save contact failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    me.getBankselcontactsgridpanel().setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Save Contact', obj.errorMessage);
                }
            });
        }
    },

    loadSubmissionContacts: function(submissionid,context) {
        //Load Selected Bank's Contacts
        var me = this;
		var submcnts_st = context.down('bankchooserpanel').down('#submissioncontactsgridpanel').getStore();
        var submcontactsgridpanel = context.down('bankchooserpanel').down('#submissioncontactsgridpanel');

        submcnts_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        submcnts_st.load({
            params:{
                filter :'idLoan = '+submissionid
            },
            callback: function(submrecords, submoperation, submsuccess){
                var submcntscnt = submcnts_st.getCount();

                var submcnttoolbar = submcontactsgridpanel.down('#submissioncontacttoolbar');
                if(submcntscnt > 0){
                    if(submcnttoolbar){
                        submcnttoolbar.hide();
                    }
                } else {
                    if(submcnttoolbar){
                        submcnttoolbar.show();
                    }
                }

                me.setEmailFaxLink(context);
            }
        });
    },

    loadSubmissionContactsAfterSave: function(submissionid, bankid) {
        var me = this;
        //Load Selected Bank's Contacts
        var bankcontacts_st = Ext.getStore('BankContacts');
        var submcnts_st = Ext.getStore('SubmissionContacts');
        var emaillistst = "";

        var bankselcontactsgridpanel = me.getBankselcontactsgridpanel();
        bankselcontactsgridpanel.getSelectionModel().deselectAll();

        submcnts_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        submcnts_st.load({
            params:{
                filter :'idLoan = '+submissionid
            },
            callback: function(submrecords, submoperation, submsuccess){
                var submcntscnt = submcnts_st.getCount();

                if(submcntscnt > 0){
                    var bankcntscnt = bankcontacts_st.getCount();

                    if(bankcntscnt > 0){

                        bankcontacts_st.each(function(bankcntrec){
                            submcnts_st.each(function(subcntrec){
                                if(subcntrec.get('contactid')===bankcntrec.get('contactid')){
                                    bankselcontactsgridpanel.getSelectionModel().select(bankcntrec,true);
                                    emaillistst = emaillistst +subcntrec.get('email') + "; ";
                                }
                            });
                        });
                        me.getBankchooserpanel().config.emaillistst = emaillistst;
                    }
                }
            }
        });
    },

    removeGlobalDocMenuItemClick: function() {
        console.log("Remove Global Doc File");
        var me = this;
		
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var globaldocsselgridpanel = context.down('dealdetailsformpanel').down('globaldocsselgridpanel');
		
        var idsubmission = globaldocsselgridpanel.config.submissionid;
		
		var row = context.record;
		var dealid = row.data.idDeal;
		
        if (globaldocsselgridpanel.getSelectionModel().hasSelection()) {
            var glbdocrec = globaldocsselgridpanel.getSelectionModel().getSelection()[0];
            var glbsubmdocid = glbdocrec.get('idGSubxDoc');

            var methodname = "DELETE";

            globaldocsselgridpanel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:GSubxDoc/'+glbsubmdocid+'?checksum=override',
                scope:this,
                method:methodname,
                success: function(response, opts) {
                    globaldocsselgridpanel.setLoading(false);
                    console.log(response);
                    var obj = Ext.decode(response.responseText);
                    if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
                        //Load the Docs Per Submission
                        me.loadGlobalDocsPerSubmission(idsubmission,context);
                        me.getController("DocController").loadDocsStore(dealid);
                    } else {
                        console.log("Global Doc Remove Failed.");
                        Ext.Msg.alert('Global Doc Remove Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    globaldocsselgridpanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Global Doc Remove Failed', obj.errorMessage);
                }
            });
        } else {
            Ext.Msg.alert('Failed', "Please select global document to delete.");
        }
    },

    converByteToBase64: function(arrayBuffer) {
        var base64    = '';
        var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

        var bytes         = new Uint8Array(arrayBuffer);
        var byteLength    = bytes.byteLength;
        var byteRemainder = byteLength % 3;
        var mainLength    = byteLength - byteRemainder;

        var a, b, c, d;
        var chunk;

        // Main loop deals with bytes in chunks of 3
        for (var i = 0; i < mainLength; i = i + 3) {
            // Combine the three bytes into a single integer
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048)   >> 12; // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032)     >>  6; // 4032     = (2^6 - 1) << 6
            d = chunk & 63;               // 63       = 2^6 - 1

            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
        }

        // Deal with the remaining bytes and padding
        if (byteRemainder == 1) {
            chunk = bytes[mainLength];

            a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

            // Set the 4 least significant bits to zero
            b = (chunk & 3)   << 4; // 3   = 2^2 - 1

            base64 += encodings[a] + encodings[b] + '==';
        } else if (byteRemainder == 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

            a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008)  >>  4; // 1008  = (2^6 - 1) << 4

            // Set the 2 least significant bits to zero
            c = (chunk & 15)    <<  2; // 15    = 2^4 - 1

            base64 += encodings[a] + encodings[b] + encodings[c] + '=';
        }

        return base64;
    },

    removeFileFromSubmission: function() {
        console.log("Remove File from submission");
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var bankdocsgridpanel = bankchooserpanel.down('#bankdocsgridpanel');

		var row = context.record;
        var dealid = row.data.idDeal;

        if (bankdocsgridpanel.getSelectionModel().hasSelection()) {
            var submdocrec = bankdocsgridpanel.getSelectionModel().getSelection()[0];
			if(submdocrec.get('isglobal')){
				Ext.Msg.alert('Alert', "You are not allowed for deletion of a submission document that is a reflection of a global document.");
			} else {
				var submdocid = submdocrec.get('idSubxDoc');
				var submissionid = submdocrec.get('loanid');
	
				var methodname = "DELETE";
	
				bankdocsgridpanel.setLoading(true);
				Ext.Ajax.request({
					headers: {
						'Content-Type': 'application/json',
						Accept: 'application/json',
						Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
					},
					url:DM2.view.AppConstants.apiurl+'mssql:SubxDoc/'+submdocid+'?checksum=override',
					scope:this,
					method:methodname,
					success: function(response, opts) {
						bankdocsgridpanel.setLoading(false);
						console.log(response);
						var obj = Ext.decode(response.responseText);
						if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201)
						{
							//Load the Docs Per Submission
							me.loadDocsPerSubmission(submissionid,context);
						}
						else{
							console.log("Doc Remove Failed.");
							Ext.Msg.alert('Doc Remove Failed', obj.errorMessage);
						}
					},
					failure: function(response, opts) {
						bankdocsgridpanel.setLoading(false);
						var obj = Ext.decode(response.responseText);
						Ext.Msg.alert('Doc Remove Failed', obj.errorMessage);
					}
				});
			}
        }
        else{
            Ext.Msg.alert('Failed', "Please select global document to delete.");
        }
    },

    getSubmissionDocFromOutlook: function(fna, fca) {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
		
		var eastregion = bankchooserpanel.down('#bankselbtmpanel');
		var addsubmissiondocpanel;
		if(context.down('dealdetailsformpanel').down('addsubmissiondocpanel')){
			console.log("Use Old Panel");
			addsubmissiondocpanel = context.down('dealdetailsformpanel').down('addsubmissiondocpanel');
		}
		else {
			console.log("Create New Panel");
			addsubmissiondocpanel = Ext.create('DM2.view.AddSubmissionDocPanel');//me.getAddsubmissiondocpanel();
		}
        eastregion.getLayout().setActiveItem(addsubmissiondocpanel);
		
        for (var i=0; i<fna.length; i++) {
            //console.log(fna[i]);
            //console.log(fca[i]);
            var filename = fna[i];
            var filecnt = fca[i];

            var filesize = filecnt.length;
            var filesizekb = (filesize/1024).toFixed(2);		
			
            var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
            var bankshrtname = bankdocsfrm.getForm().findField('shortName').getValue();
            var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();
            //var classification = "submission-"+bankshrtname+"-"+submissionid;
			var classification = "";

            ///// Add the file item to Store
            var docrec = Ext.create('DM2.model.Document',{
                'name' : filename,
                'size' : filesizekb,
                'clsfctn' :classification,
                'desc' :'',
                'fileobj':filecnt,
                'uploadmode':'outlook',
                'filesize':filesize
            });
            var docstempst = addsubmissiondocpanel.down('grid').getStore(); //('DocsTemp');
            docstempst.add(docrec);
            /////
        }
        addsubmissiondocpanel.down('grid').getSelectionModel().select(0);
        addsubmissiondocpanel.getForm().findField('name').focus();
    },

    showContactToSubmissionBtnClick: function(cmp) {
        var me = this,context;
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			context = grid.up('tab-deal-detail');
		} else {
		    // when cmp is a button.
			context = cmp.up('tab-deal-detail');
		}
		
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('adddealcontactform')){
			adddealcontactform = detailpanel.down('adddealcontactform');
		} else {
        	adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
		}
        eastregion.getLayout().setActiveItem(adddealcontactform);
		
		var bankchooserpanel = detailpanel.down('bankchooserpanel');
        var bankselgrid = bankchooserpanel.down('#bcselbankgridpanel');
        var row = bankselgrid.getSelectionModel().getSelection()[0];
        var bankid = row.get('bankid');
        var fullName = row.get('fullName');

        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();

        var filters = [];
		var filterstarr = [];
        if(fullName!==''){
            var filterst = "companyName = '"+fullName+"'";
            filterstarr.push(filterst);
			var newObj = {
				id: 'companyName',
				property: 'companyName',
				value: fullName,
				exactMatch: false,
				caseSensitive: false
			};
			newObj.operator = "eq";
			filters.push(newObj);
        }		
        me.loadSubmissionSelContacts(fullName,adddealcontactform);
		adddealcontactform.config.backview = "submcnts";
		adddealcontactform.config.backfrm = bankchooserpanel;
		//Set Value For Contact Type
		//var submcntselgrid  = adddealcontactform.down('grid');
		//var cmpname = submcntselgrid.HFAPI().getField('companyName');
		//cmpname.setValue(fullName);
		//cmpname.setRawValue(fullName);
    },

    closeAddSubmissionContactView: function(panel) {
        var me = this;		
		var context = panel.up('tab-deal-detail');		
        var dealdetailform = context.down('dealdetailsformpanel');
        var activityid = dealdetailform.config.bankselactivityid;
        var activitytext = dealdetailform.config.bankselactivitytext;
        var h_fname = dealdetailform.config.bankselh_fname;

        me.BankChooserClick(activityid,activitytext,h_fname,context);
    },

    onSaveContactToSubmissionBtnClick: function(btn) {
        // Save the Contact to Submission Table //
        console.log("Save the contact to submission table : "+btn.action);
        var me = this;
		var context = btn.up('tab-deal-detail');
		var submcntselfrm = btn.up('adddealcontactform');
        var rec = submcntselfrm.getForm().getRecord();

		var row = context.record;
        var dealid = row.data.idDeal;
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');		
        var bankdocsfrm = bankchooserpanel.down('#bankdocspanel');
        var submissionid = bankdocsfrm.getForm().findField('idsubmission').getValue();

        var data = {
            "idLoan": submissionid,
			"idContact": rec.get('contactid')
        };

        data['@metadata'] = {'checksum' : 'override'};
        var methodname = "POST";

        submcntselfrm.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:SubxCont',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                submcntselfrm.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201) {
					me.loadSubmissionContacts(submissionid,context);
					if(btn.action=="saveContactToSubmissionBtn"){
						submcntselfrm.down('grid').getSelectionModel().deselectAll();
					} else if(btn.action=="saveAndCloseContactToSubmissionBtn"){
    	                me.closeAddSubmissionContactView(submcntselfrm);
					} else {
						me.closeAddSubmissionContactView(submcntselfrm);
					}
                } else {
                    Ext.Msg.alert('Add contact to submission failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
				console.log("Add Contact to Submission Failure.");
                submcntselfrm.setLoading(false);				
                var obj = Ext.decode(response.responseText);
				var errMsg = obj.errorMessage;
				var n = errMsg.indexOf("Violation of UNIQUE KEY constraint");
				if(n==-1){
	                Ext.Msg.alert('Add contact to submission failed', obj.errorMessage);
				} else {
					Ext.Msg.alert('Add contact to submission failed', "Contact already exist to submission.Please try to add another contact.");
				}
            }
        });
    },

    loadSubmissionSelContacts: function(fullName,view) {
        //Filter Selected Bank's Contacts
        var me = this;
        var submcontactallselbuff_st = view.down('grid').getStore(); 		
		var submcontactallselbuff_stproxy = submcontactallselbuff_st.getProxy();
		var submcontactallselbuff_stproxyCfg = submcontactallselbuff_stproxy.config;
        submcontactallselbuff_stproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		submcontactallselbuff_st.setProxy(submcontactallselbuff_stproxyCfg);		
		submcontactallselbuff_st.load({
			callback: function(records, operation, success){
				var submcntselgrid  = view.down('grid');
				var cmpname = submcntselgrid.HFAPI.getField('companyName');
				console.log(cmpname);
				//var cmpname = submcntselgrid.HFAPI().getField('companyName');
				cmpname[0].setValue(fullName);
				//cmpname.setRawValue(fullName);
			}
		});
        /*if(filterstarr.length > 0) {
			submcontactallselbuff_st.addFilter(filterstarr);
			/*submcontactallselbuff_st.filter([
			  {property: 'companyName', value: filterstarr[0].value, operator:"eq"}
			]);
		}*/
    },

    onSubmissionContactGridRightClick: function(grid, record, tr, rowIndex, e, eOpts) {
        var me = this;
		me.showSubCntGridMenu(grid,record,e);
    },
	
	showSubCntGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
        console.log("Show Menu for Submission Contacts");
        if(this.getSubmissioncontactgridmenu()){
			this.getSubmissioncontactgridmenu().spawnFrom = view;
            this.getSubmissioncontactgridmenu().showAt(e.getXY());
        }
	},
	
	onSubCntGridItemLongPress: function(view, record , item , index , e , eOpts){
		var me = this;
		view.getSelectionModel().select(record);
		me.showSubCntGridMenu(view,record,e);
	},

    submissionContactGridMenuClick: function(menu, item, e, eOpts) {
        console.log("Submission Contacts Menu Clicked");
        var me = this;
        var eventname = item.itemevent;
        if(eventname === "addcontact"){
            console.log("Add Contact Menu Item Clicked");
            me.showContactToSubmissionBtnClick(item);
        } else if(eventname === "removecontact"){
            me.removeContactFromSubmission(item);
        }
    },

    removeContactFromSubmission: function(cmp) {
        console.log("Remove Contact From Submission");
        var me = this;
		var context;
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			context = grid.up('tab-deal-detail');
		}
		
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');			
        var submcontactsgridpanel = bankchooserpanel.down('#submissioncontactsgridpanel');

        var row = submcontactsgridpanel.getSelectionModel().getSelection()[0];
        console.log(row.get('idSubxCont'));
        var idSubxCont = row.get('idSubxCont');
        var loanid = row.get('idLoan');
        Ext.Msg.show({
            title:'Delete Contact?',
            message: 'Are you sure you want to delete the Contact from Submission?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    submcontactsgridpanel.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DM2.view.AppConstants.apiurl+'mssql:SubxCont/'+idSubxCont+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            submcontactsgridpanel.setLoading(false);
                            console.log("Removed Contact.");
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200)
                            {
                                me.loadSubmissionContacts(loanid,context);
                            } else {
                                console.log("Delete Contact Failure.");
                                Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            submcontactsgridpanel.setLoading(false);
                            console.log("Delete Contact Failure.");
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Contact failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
    },

    setEmailFaxLink: function(context) {
        var me = this;
		var bankchooserpanel = context.down('dealdetailsformpanel').down('bankchooserpanel');
        var genfilesgrid = context.down('bankchooserpanel').down('#genfilespersubmissiongridpanel');
		var genfilessubmissionst = genfilesgrid.getStore();

        if(genfilessubmissionst.getCount()>0){
            var row = genfilessubmissionst.getAt(0);
            var docid = row.get('docid');
            var docurl = row.get('url');
            //var docurl = "http://ravenoustech.com/company-profile.pdf";
            var docname = row.get('name');
            var bodyMsg = "";
			
			var submcnts_st = context.down('bankchooserpanel').down('#submissioncontactsgridpanel').getStore(); //Ext.getStore('SubmissionContacts');
            var emaillistst = "";
            var faxlistst = "";
            if(submcnts_st.getCount() > 0){
                submcnts_st.each(function(subcntrec){
                    var cntemail = subcntrec.get('email');
                    var cntfax = subcntrec.get('faxNumber');
                    if(cntemail!=="" && cntemail!==null && cntemail!==undefined){
                        emaillistst = emaillistst +cntemail + "; ";
                    }
                    if(cntfax!=="" && cntfax!==null && cntfax!==undefined){
                        faxlistst = faxlistst +cntfax + "@fax.local; ";
                    }
                });
            }

            var mailsep = "&MAILPART#";
            var mailhrefst = "mcgmt:"+emaillistst+mailsep+docname+mailsep+bodyMsg+mailsep+docurl+mailsep+docname;
			console.log(mailhrefst);
            var faxhrefst = "mcgmt:"+faxlistst+mailsep+docname+mailsep+bodyMsg+mailsep+docurl+mailsep+docname;

            bankchooserpanel.down('#faxbtn').setHref(faxhrefst);
            bankchooserpanel.down('#emailbtn').setHref(mailhrefst);
			bankchooserpanel.down('#printbtn').setHref(docurl);
        }
    },

    loadContactsEmail: function(rec) {
        var me = this;
        var cntemailproxy = Ext.getStore('ContactEmails').getProxy();
        cntemailproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('ContactEmails').removeAll();
        Ext.getStore('ContactEmails').load({
            params:{
                filter :'idContact = '+rec.get('contactid')
            }
        });
    },

    loadContactsFax: function(rec) {
        var me = this;
        var cntfaxproxy = Ext.getStore('ContactFax').getProxy();
        cntfaxproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('ContactFax').removeAll();
        Ext.getStore('ContactFax').load({
            params:{
                filter :'idContact = '+rec.get('contactid')
            }
        });
    },

    showActivity: function(activityid, activityname,tabdealdetail) {
        var me = this;
        console.log("Activity Menu Item Clicked : "+activityname);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
        var eastregion = detailpanel.down('[region=east]');
		//var eastregion = tabdealdetail.down('[region=east]');
        eastregion.config.activityid = activityid;

        if(activityname=="Call Main Contact"){
            me.CallDClick(activityid,activityname,'CallDClick',tabdealdetail);
        } else if(activityname=="Receive Rent Roll" || activityname=="Receive Expenses Document"){
            me.getController('AddRentRollExpenseController').AddRentRollExpenseDocDClick(activityid,activityname,'AddDocDClick',tabdealdetail);
        } else if(activityname=="Receive Bid"){
            me.getController('ReceiveBidController').ReceiveBidClick(activityid,activityname,'ReceiveBidClick',tabdealdetail);
        } else if(activityname=="Select Bid"){
            me.getController('SelectBidController').SelectBidClick(activityid,activityname,'SelectBidClick',tabdealdetail);
        } else if(activityname === "Loan Sizing & Setup"){
            me.LoanSizeClick(activityid,activityname,'LoanSizeClick',tabdealdetail);
        } else if(activityname === "Bank Selection & Submission"){
            me.BankChooserClick(activityid,activityname,'BankChooserClick',tabdealdetail);
        } else if(activityname === "Receive and Manage a Quote"){
            me.getController('QuoteController').ReceiveQuoteClick(activityid,activityname,'ReceiveQuoteClick',tabdealdetail);
        } else if(activityname === "Select a Loan"){
			me.getController('QuoteController').ReceiveQuoteClick(activityid,activityname,'SelectLoanClick',tabdealdetail);
            //me.getController('LoanController').SelectLoanClick(activityid,activityname,'SelectLoanClick',tabdealdetail);
        } else if(activityname === "Send an LOI"){
            me.getController('SendLOIController').SendLOIClick(activityid,activityname,'SendLOIClick',tabdealdetail);
        } else if(activityname === "Send Contract To Buyer" || activityname === "Send Contract To Seller" || activityname === "Send Brokerage Contract To Seller" || activityname === "Send Brokerage Contract To Buyer"){
            me.getController('SendLOIController').SendLOIClick(activityid,activityname,'SendLOIClick',tabdealdetail);
        } else if(activityname === "Received Signed LOI"){
            me.getController('RecvSignedLOIController').RecvSignLOIClick(activityid,activityname,'RecvSignLOIClick',tabdealdetail);
        } else if(activityname === "Receive Signed Brokerage Contract" || activityname==="Receive Finalized Contract" || activityname === "Receive Signed Brokerage Contract To Seller" || activityname ==="Receive Signed Brokerage Contract To Buyer" || activityname ==="Create Offering Memorandum"){
            me.getController('RecvSignedLOIController').RecvSignLOIClick(activityid,activityname,'RecvSignLOIClick',tabdealdetail);
        } else if(activityname === "Received Good Faith Check"){
            me.getController('RecvGoodFaithCheckController').RecvGoodFaithCheckClick(activityid,activityname,'RecvGoodFaithCheckClick',tabdealdetail);
        } else if(activityname === "Underwriting"){
            me.getController('UnderwritingController').UnderwritingClick(activityid,activityname,'UnderwritingClick',tabdealdetail);
        } else if(activityname === "Application"){
            me.getController('ApplicationController').ApplicationClick(activityid,activityname,'ApplicationClick',tabdealdetail);
        } else if(activityname === "Reiterate"){
            me.getController('ReiterateController').ReiterateClick(activityid,activityname,'ReiterateClick',tabdealdetail);
        } else if(activityname === "Modification"){
            me.getController('SendLOIController').SendLOIClick(activityid,activityname,'SendLOIClick',tabdealdetail);
        } else if(activityname === "Approval"){
            me.getController('ApprovalController').ApprovalClick(activityid,activityname,'ApprovalClick',tabdealdetail);
        } else if(activityname === "Set Closing Date"){
            me.getController('ApprovalController').ApprovalClick(activityid,activityname,'ApprovalClick',tabdealdetail);
        } else if(activityname === "Closing"){
            me.getController('ClosingController').ClosingClick(activityid,activityname,'ClosingClick',tabdealdetail);
        } else if(activityname === "Create New Deal"){
            me.getController('CreateNewDealController').CreateNewDealClick(activityid,activityname,'CreateNewDealClick',tabdealdetail);
        } else if(activityname === "Billing"){
            me.getController('BillingController').BillingClick(activityid,activityname,'BillingClick',tabdealdetail);
        } else if(activityname === "Send Invoice"){
            me.getController('SendInvoiceController').SendInvoiceClick(activityid,activityname,'SendInvoiceClick',tabdealdetail);
        }
    },
	onLoanSizeFieldChange: function(field, newValue, oldValue, eOpts ){
		console.log("On Loan Size Field Change Occur");
		var me = this;
		var context = field.up('tab-deal-detail');		
        var loansizingpanel = context.down('loansizingpanel');
		var loansizingdtfrm = loansizingpanel.down('loansizingdetailsform');
		loansizingdtfrm.down('#saveloansizebtn').enable();
		loansizingdtfrm.down('#resetloansizebtn').enable();
		loansizingdtfrm.config.isFormDirty = true;
	},
	loanPropertyGridItemBeforeSelect: function( selModel, record, index, eOpts ){
		var me = this;
		var context = selModel.view.ownerCt.up('tab-deal-detail');		
        var loansizingpanel = context.down('loansizingpanel');
		var loansizingdtfrm = loansizingpanel.down('loansizingdetailsform');
		if(loansizingdtfrm.config.isFormDirty){
			console.log("Form is dirty.");
			Ext.Msg.alert('Alert!', "You have unsaved changes.Please save it before changing property.");
			return false;
		}else{
			console.log("Continue");
		}
	},
	onChangeClsfDesc: function(fld, e, eOpts) {
        console.log("Change Clsf Doc Item Save");
        var me = this;
		var context = fld.up('tab-deal-detail');

        var adddocform = context.down('dealdetailsformpanel').down('addglobaldocpanel').getForm();
		
		var nameval = adddocform.findField('name').getValue();
		var extval = adddocform.findField('ext').getValue();
		var fullname = nameval+"."+extval;
		
        var descval = adddocform.findField('desc').getValue();
        var clsfctnval = adddocform.findField('clsfctn').getValue();

        var docgrid = context.down('dealdetailsformpanel').down('addglobaldocpanel').down('grid');
        if(docgrid.getSelectionModel().hasSelection()){
            var row = docgrid.getSelectionModel().getSelection()[0];
            row.set('name',fullname,true);
            row.set('desc',descval,true);
            row.set('clsfctn',clsfctnval,true);
        }
    },
	onShowAllSubmissionCheckboxChange: function(field, newValue, oldValue, eOpts){
		console.log("onShowAllSubmissionCheckboxChange");
		var me = this;
		var context = field.up('tab-deal-detail');
		var bcselbankgrid = context.down('bankchooserpanel').down('#bcselbankgridpanel');
        var bankselst = bcselbankgrid.getStore();
		if(newValue){
			//Show All Submission
			bankselst.clearFilter();
		}else{
			bankselst.filter('isSubmitted', true);
		}
	}

});
