Ext.define('DM2.controller.CreateNewDealController', {
    extend: 'Ext.app.Controller',
	
	requires:['DM2.view.CreateNewDealPropertyGridMenu'],
    
	refs: {
        createnewdealpanel: {
            autoCreate: true,
            selector: 'createnewdealpanel',
            xtype: 'createnewdealpanel'
        },
		createnewdealpropertygridmenu : 'createnewdealpropertygridmenu',
		createnewdealcontactgridmenu : 'createnewdealcontactgridmenu'
    },

    init: function(application) {
        this.control({
            'createnewdealpanel':{
                close: 'onCreateNewDealPanelClose',
				afterrender: 'loadCreateNewDealPanelDetails'
            },
            'createnewdealpanel #createnewdealbtn':{
                click : 'onCreateNewDealBtnClick'
            },
            'createnewdealpanel #createnewdealpropertygrid':{
                selectionchange: 'onCreateNewDealPropertySelChange',
				afterrender: 'onCreateNewDealPropertyGridRightClick'
            },
			'createnewdealusergridpanel button[action="showaddnewdealuserbtn"]':{
				click: 'showAddDealUserView'
			},
			'createnewdealusergridpanel':{
				beforeselect: 'beforeSelectUser'
			},
			'createnewdealpanel #primaryProperty':{
                beforecheckchange: this.primaryPropertyCheckboxChanged
            },
			'createnewdealpanel button[action="showaddnewdealpropertybtn"]':{
				click: 'showAddDealPropertyView'
			},
			'createnewdealpropertygridmenu menuitem':{
                click: this.createNewDealPropertyGridMenuItemClick
            },
			'createnewdealpanel createnewdealcontactgrid':{
				afterrender: 'onCreateNewDealContactGridRightClick',
				validateedit: this.beforeCntTypeComplete
            },			
			'createnewdealpanel button[action="showaddnewdealcontactbtn"]':{
				click: 'showAddDealContactView'
			},
			'createnewdealcontactgridmenu menuitem':{
                click: this.createNewDealContactGridMenuItemClick
            }
        });
    },

    CreateNewDealClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        console.log("Create New Deal Click");
        var me = this;
		
		var eastregion = tabdealdetail.down('dealdetailsformpanel').down('[region=east]');
		var createnewdealpanel;
		if(tabdealdetail.down('dealdetailsformpanel').down('createnewdealpanel')){
			console.log("Use Old Panel");
			createnewdealpanel = tabdealdetail.down('dealdetailsformpanel').down('createnewdealpanel');
		}
		else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	createnewdealpanel = Ext.create('DM2.view.CreateNewDealPanel');//me.getCreatenewdealpanel();
		}
        eastregion.getLayout().setActiveItem(createnewdealpanel);
		createnewdealpanel.setTitle("Activities > "+activitytext);
        //createnewdealpanel.down('#activityid').setValue(activityid);
    },
	
	bindCreateNewDealViewInfo: function(context){
		var me = this;
		console.log("Bind Create New Deal View Info");
		var createnewdealpanel;
		if(context.down('dealdetailsformpanel')) {
			if(context.down('dealdetailsformpanel').down('createnewdealpanel')) {
				createnewdealpanel = context.down('dealdetailsformpanel').down('createnewdealpanel');			
				var createnewdealpropertygrid = createnewdealpanel.down('#createnewdealpropertygrid');
				if(createnewdealpropertygrid) {
					var dealpropertyst = createnewdealpropertygrid.getStore();
					//Filter Already Covered property From New Deal Creation
					var tempprparr = context.dealPropertyData;
					var newprparr = [];
					for(var i=0;i<tempprparr.length;i++){
						if(tempprparr[i].data){
							if(tempprparr[i].data.covered==false){
								newprparr.push(tempprparr[i]);
							}
						} else {
							if(tempprparr[i].covered==false){
								newprparr.push(tempprparr[i]);
							}
						}
					}
					dealpropertyst.loadData(newprparr);			
					//dealpropertyst.filter('covered', 1);
					createnewdealpropertygrid.getSelectionModel().selectAll();
				}
				var createnewdealcontactgrid = createnewdealpanel.down('#createnewdealcontactgrid');
				if(createnewdealcontactgrid) {
					var dealcntst = createnewdealcontactgrid.getStore();
					dealcntst.loadData(context.dealContactData);
				}
				var createnewdealdocsgridpanel = createnewdealpanel.down('#createnewdealdocsgridpanel');
				if(createnewdealdocsgridpanel) {
					var dealdocsst = createnewdealdocsgridpanel.getStore();
					dealdocsst.loadData(context.dealDocsData);
				}
				var createnewdealusergridpanel = createnewdealpanel.down('#createnewdealusergridpanel');
				if(createnewdealusergridpanel) {
					var dealusersst = createnewdealusergridpanel.getStore();
					var tmpdealusersdt = context.dealUsersData;
					//console.log("contextUserData");
					//console.log(context.dealUsersData);
					var dealusersdt = [];
					///FIlter active users from an array
					for(var i=0;i<tmpdealusersdt.length;i++){
						//console.log("Active"+tmpdealusersdt[i].active);
						if(typeof tmpdealusersdt[i].active !== 'undefined'){
							if(tmpdealusersdt[i].active==1){
								dealusersdt.push(tmpdealusersdt[i]);
							}
						}else if(typeof tmpdealusersdt[i].data.active !== 'undefined'){
							if(tmpdealusersdt[i].data.active==1){
								dealusersdt.push(tmpdealusersdt[i]);
							}
						}
					}					
					dealusersst.loadData(dealusersdt);
					var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
					var userExist = false;
					dealusersst.each(function(record) {
						var username = record.get('user');
						if(username===userid){
							userExist = true;
							console.log("Matched"+username);
							createnewdealusergridpanel.getSelectionModel().select(record,true);
						}
					});
					if(!userExist){
						me.addLoggedInUser(userid,createnewdealpanel);	
					}
				}
			}
		}
	},
	
	loadCreateNewDealPanelDetails: function(panel){
		var me = this;
		console.log("After Render Create New Deal Panel");
		var createnewdealpanel = panel;
		var statuscmb = createnewdealpanel.down('#status');		
		statuscmb.setValue("CX");
		
		
		var office_fld = createnewdealpanel.down('#office');
		var office_st = office_fld.getStore();
		
		var userst = Ext.getStore('Users');
		var glboffice_st = Ext.getStore('Office');
		
		if(glboffice_st.isLoaded()){
			office_st.loadData(glboffice_st.data.items)
			office_fld.setValue(office_st.getAt(0));
			office_st.each(function(record) {
				if(record.get('code')==userst.getAt(0).get('office')) {
					office_fld.setValue(record.get('code'));
				}     
			},this);
		}

		var context = panel.up('tab-deal-detail');
		if(context){
			var record = context.record;
			var dealid = record.data.idDeal;
			
			//Load Deal Property
			//me.loadDealProperty(dealid,panel);
			if(context.isRDealDetailsLoaded == 'yes'){
				me.bindCreateNewDealViewInfo(context);
				//Load in future if data doen't sync
				//Load Deal Active Users
		        //me.loadDealActiveUser(dealid,panel);
			}						
		} else if(panel.up('tab-property-layout')){
			context = panel.up('tab-property-layout');
			//Load Property Contacts
			//me.loadContactsPerProperty(context.down('propertycontactsgridpanel'));
		}
	},
	
	loadContactsPerProperty: function(dealid,panel){
		var me = this;
		context.down('propertycontactsgridpanel');
		var grid = panel.down('#createnewdealcontactgrid');  
		var dealdetailcontactst = grid.getStore();
		
        dealdetailcontactst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealdetailcontactst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
			}
		});
	},
	
	loadDealProperty: function(dealid,panel){
		var me = this;
		var grid = panel.down('#createnewdealpropertygrid');  
        var dealpropertyst = grid.getStore();
		
		//var context = panel.up('tab-deal-detail');
		//dealpropertyst.loadData(context.dealDetailStore.data.items[0].data.PropertiesPerDeal);
		
        dealpropertyst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealpropertyst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				if(grid){
					grid.getSelectionModel().selectAll();
				}
			}
		});
	},
	
	loadDealContacts: function(dealid,panel){
		var me = this;
		var grid = panel.down('#createnewdealcontactgrid');  
		var dealdetailcontactst = grid.getStore();
		
        dealdetailcontactst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealdetailcontactst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
			}
		});
	},
	
	loadDealDocs: function(dealid,panel) {
        var me = this;
		var grid = panel.down('#createnewdealdocsgridpanel');  
		var dealdetaildocsst = grid.getStore();
        dealdetaildocsst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealdetaildocsst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
            }
        });
    },
	
    onCreateNewDealPanelClose: function(panel) {
		var me = this;
		var context = panel.up('tab-deal-detail');
		if(context){
			var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}else{
			context = panel.up('tab-property-layout');
			me.getController('PropertyController').closeRecvDealDetailDocs(context);
		}
    },

    loadDealActiveUser: function(dealid,panel) {
        var me = this;
        var filterstarr = [];

        var filterst = "dealid = '"+dealid+"'";
        filterstarr.push(filterst);

        filterst = "active = 1";
        filterstarr.push(filterst);
		
		var dealactiveuser_st = panel.down('createnewdealusergridpanel').getStore();
        dealactiveuser_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealactiveuser_st.load({
            params:{
                filter : filterstarr
            },
			callback: function(records, operation, success){
				var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
				var userExist = false;
				dealactiveuser_st.each(function(record) {
	                var username = record.get('user');
					if(username===userid){
						userExist = true;
						console.log("Matched"+username);
				        panel.down('createnewdealusergridpanel').getSelectionModel().select(record,true);
					}
				});
				if(!userExist){
					me.addLoggedInUser(userid,panel);	
				}
			}
        });
    },
	
	addLoggedInUser: function(userid,panel){
		var me = this,cnt=0;
		var userst = Ext.getStore('Users');
		var createnewdealusergridpanel = panel.down('createnewdealusergridpanel');
		var rec = Ext.create('DM2.model.DealUser',{
			active      : userst.getAt(0).get('active'),
            user        : userid,
			role        : "Administrator",
            Role_idRole : 4,
            csrid       : userst.getAt(0).get('idUser')
        });
		if(panel.up('tab-deal-detail')){
		} else {
			createnewdealusergridpanel.store.removeAll();
		}
		cnt = createnewdealusergridpanel.getStore().getCount();
        createnewdealusergridpanel.store.insert(cnt, rec);
		createnewdealusergridpanel.getSelectionModel().select(cnt,true);
	},
	
    onCreateNewDealBtnClick: function(btn) {
        var me = this;
		console.log("Create New Deal Button CLick");
		var context = btn.up('tab-deal-detail');
        var record = context.record;
        //var dealid = record.data.dealid;
		var dealid = record.data.idDeal;
				
        var crtnewdealpanel = context.down('dealdetailsformpanel').down('createnewdealpanel');
        //var fileno    = Number(crtnewdealpanel.down('#filenumber').getValue());
        var status    = crtnewdealpanel.down('#status').getValue();
        var nameval   = crtnewdealpanel.down('#name').getValue();
        //var officeval = crtnewdealpanel.down('#office').getValue();
		var officeval = crtnewdealpanel.down('#office').getRawValue();

        var propgrid = crtnewdealpanel.down('#createnewdealpropertygrid');
        var propSel = propgrid.getSelectionModel().getSelection();
        var deal_has_property = [],isAnyPrimaryFlag=false;
        for(var j=0;j<propSel.length;j++){
			//console.log("primaryProperty : "+propSel[j].get('primaryProperty'));
            var property_id = propSel[j].get('propertyid');
            var propObj = {
                'idProperty'  :property_id,
				'isPrimary' : propSel[j].get('primaryProperty'),
				'mortgageType' : propSel[j].get('mortgageType')
            };
            deal_has_property.push(propObj);
			if(propSel[j].get('primaryProperty')==true){
				isAnyPrimaryFlag = true;
			}
        }
		if(isAnyPrimaryFlag==false && propSel.length>0){
			Ext.Msg.alert('Error!', "Please select atleast one primary property.");
			return false;
		}

        var cntgrid  = crtnewdealpanel.down('#createnewdealcontactgrid');
        var contSel = cntgrid.getSelectionModel().getSelection();
        var deal_has_contact = [];
        for(var j=0;j<contSel.length;j++){
            var contact_id = contSel[j].get('contactid');
            var cntObj = {
                'idContact'   : contact_id,
				'contactType' : contSel[j].get('contactType')
            };
            deal_has_contact.push(cntObj);
        }

        var docgrid  = crtnewdealpanel.down('#createnewdealdocsgridpanel');
        var docSel = docgrid.getSelectionModel().getSelection();
        var deal_has_doc = [];
		for(var j=0;j<docSel.length;j++){
            var docid = docSel[j].get('docid');
            var name  = docSel[j].get('name');
            var desc  = docSel[j].get('desc');
            var ext     = docSel[j].get('ext');
            var format  = docSel[j].get('format');
            var clsfctn = docSel[j].get('clsfctn');
            var url  = docSel[j].get('url');
            /*var docObj = {
                //'doc_id'  :docid,
                "name": name,
                "description": desc,
                "ext": ext,
                "format": format,
                "classification": clsfctn,
                "url": url
            };*/
			var docObj = {
                'requestType' : "RepositoryCopyGeneral",
                //'paramater1'  :  associateid.toString(),
                'paramater2'  :  docid.toString(),
                'paramater3'  :  name,
                'paramater4'  :  clsfctn,
                'paramater5'  :  desc
            };
            deal_has_doc.push(docObj);
        }

        var usergrid = crtnewdealpanel.down('#createnewdealusergridpanel');
        var userSel = usergrid.getSelectionModel().getSelection();
        var deal_has_user = [];
		var userid = Ext.util.Cookies.get("userid-"+DM2.view.AppConstants.apiurl);
        for(var j=0;j<userSel.length;j++){
            var userid = userSel[j].get('idDealxUser');
            var roleid = userSel[j].get('Role_idRole');
            var active = userSel[j].get('active');
            var csrid = userSel[j].get('csrid');
			var userName = userSel[j].get('user');
            var userObj = {
                //'CSR_idCSR'  :csrid,
				'userName' : userName,
                //'assignUserName' : userid,
                'assignDateTime' : new Date(),
                'idRole':roleid/*,
                'active':active*/
            };
            deal_has_user.push(userObj);
        }
		
		//var dealproperty_has_loan = [];
		var data = [{
            'status' : status,
			'dealType' : 'F',
			//'statusDate': "2015-01-08",
            //'fileNumber' : fileno,
            'dealName' : nameval,
            'office' : officeval.toString(),
            'Deal_has_PropertyList' : deal_has_property,
            'Deal_has_ContactList'  : deal_has_contact,
            'Deal_has_UserList'     : deal_has_user//,
            //'DocumentList'   : deal_has_doc,
			//'LoanList' : dealproperty_has_loan
        }];
		//if(propSel.length>0){
			//Get the Loans List
			//crtnewdealpanel.setLoading(true);
		 	/*Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
				},
				url:DM2.view.AppConstants.apiurl+'mssql:v_loansToClonePerDeal?filter=dealid='+dealid,
				scope:this,
				success: function(response, opts) {
					crtnewdealpanel.setLoading(false);
					console.log(response);
					console.log(response.status);
					if(response.status==200){
						var obj = Ext.decode(response.responseText);
						var loanidarr = [];
						if(obj.length>0){
							console.log("Object Length"+obj.length);
							console.log(obj);
							
							for(var k=0;k<obj.length;k++){
								if(propSel.length>0){
									var flag = 0;
									for(var j=0;j<propSel.length;j++){
										var property_id = propSel[j].get('propertyid');
										if(property_id==obj[k].idproperty){
											flag = 1;
										}
									}
									if(flag==1){
										loanidarr.push(obj[k].loanid);
									}
								} else {
									loanidarr.push(obj[k].loanid);
								}
							}
							if(loanidarr.length>0){
								//Loan Exist for the Deal & selected Property
								me.getLoanList(loanidarr,data,context,deal_has_doc);								
							} else {
								//No Loan exist for Deal
								me.createNewDeal(data,context,deal_has_doc);
							}
						} else {
							//No Loan exist for Deal
							me.createNewDeal(data,context,deal_has_doc);
						}
					} else {
						Ext.Msg.alert('Failed', "Please try again.");
					}
				},
				failure: function(response, opts) {
					crtnewdealpanel.setLoading(false);
					Ext.Msg.alert('Failed', "Please try again.");
				}
			});*/
			///
			
			///
			/*Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
				},
				url:DM2.view.AppConstants.apiurl+"mssql:Loan?filter=idDeal="+dealid+"&filter=operatingAs='S'",
				scope:this,
				success: function(response, opts) {
					crtnewdealpanel.setLoading(false);
					console.log(response);
					//console.log(response.status);
					if(response.status==200){
						var obj = Ext.decode(response.responseText);
						var loanidarr = [];
						if(obj.length>0){
							console.log("Object Length"+obj.length);
							console.log(obj);							
							var tmploanlist = [];					
							for(var k=0;k<obj.length;k++){
								delete obj[k].idLoan;
								delete obj[k].idDeal;
								delete obj[k]["@metadata"];
								
								//obj[0].existingLoan = 1;
								//obj[0].usedInDeal = false;
								obj[k].operatingAs = 'E';
								//obj[0].usedInDeal = false;
								obj[k].magicLoanID = null;
								
								tmploanlist.push(obj[0]);
							}
							console.log("loanidarr"+loanidarr.length);
							data[0].LoanList = tmploanlist;
							me.createNewDeal(data,context,deal_has_doc);
							
						} else {*/
							//No Loan exist for Deal
							me.createNewDeal(data,context,deal_has_doc);
						/*}
					} else {
						Ext.Msg.alert('Failed', "Please try again.");
					}
				},
				failure: function(response, opts) {
					crtnewdealpanel.setLoading(false);
					Ext.Msg.alert('Failed', "Please try again.");
				}
			});*/
			////
		//}else{
			//me.createNewDeal(data);
		//}
        
    },
	getLoanList: function (loanidarr,data,context,deal_has_doc){
		var me = this;
		//Fetch the Loan details
		console.log("Fetch the Loan details");
		var crtnewdealpanel = context.down('dealdetailsformpanel').down('createnewdealpanel');
		
		var tmploanlist = [];
		for(var i =0;i<loanidarr.length;i++){
			crtnewdealpanel.setLoading(true);
			Ext.Ajax.request({
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
				},
				url:DM2.view.AppConstants.apiurl+'mssql:Loan/'+loanidarr[i],
				scope:this,
				method:'GET',
				async:false,
				success: function(response, opts) {
					crtnewdealpanel.setLoading(false);
					console.log(response);
					console.log(response.status);
					if(response.status==200){
						var obj = Ext.decode(response.responseText);
						if(obj.length>0) {
							console.log("Object Length"+obj.length);
							console.log(obj);
							delete obj[0].idLoan;
							delete obj[0].idDeal;
							delete obj[0]["@metadata"];
							
							//obj[0].existingLoan = 1;
							//obj[0].usedInDeal = false;
							obj[0].operatingAs = 'E';
							//obj[0].usedInDeal = false;
							obj[0].magicLoanID = null;
							
							tmploanlist.push(obj[0]);
							console.log("i"+i);
							console.log("loanidarr"+loanidarr.length);
							if(i==(loanidarr.length-1)){
								data[0].LoanList = tmploanlist;
								me.createNewDeal(data,context,deal_has_doc);
							}						
						} else {
							//No Loan exist for Deal
							me.createNewDeal(data,context,deal_has_doc);
						}
					} else {
						Ext.Msg.alert('Failed', "Please try again.");
					}
				},
				failure: function(response, opts) {
					crtnewdealpanel.setLoading(false);
					Ext.Msg.alert('Failed', "Please try again.");
				}
			});
		}
	},
	createNewDeal: function(data,context,deal_has_doc) {
		console.log("Create New Deal Call.");
		console.log(data);
		var me = this;
		var crtnewdealpanel = context.down('dealdetailsformpanel').down('createnewdealpanel');
		crtnewdealpanel.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'Rdeals',
            params: Ext.util.JSON.encode(data),
            scope:this,
            success: function(response, opts) {
                crtnewdealpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					var tmpdealid = 0;
                    for(var k=0;k<obj.txsummary.length;k++){
                        if(obj.txsummary[k].idDeal){
                            tmpdealid = obj.txsummary[k].idDeal;
                        }
                    }
                    console.log("Temp Deal Id"+tmpdealid);
					
                    me.onCreateNewDealPanelClose(crtnewdealpanel);
					me.uploadDealDocs(context,deal_has_doc,tmpdealid);
					/*
                    if(data[0].Deal_has_UserList.length>0){
                        var tmp_deal_has_user = [];
                        for(var i=0;i<data[0].Deal_has_UserList.length;i++){
                            var userObj = {
                                'idDeal'  :tmpdealid,
                                'assignerCSRID' : 1,
                                'eventDate' : new Date(),
                                'CSR_idCSR'   : data[0].Deal_has_UserList[i].CSR_idCSR,
                                'Role_idRole' : data[0].Deal_has_UserList[i].Role_idRole,
                                'active' : data[0].Deal_has_UserList[i].active
                            };
                            tmp_deal_has_user.push(userObj);
                        }
                        me.addDealHasUser(tmp_deal_has_user,tmpdealid,context);
                    }*/
                } else {
                    Ext.Msg.alert('Failed', "Please try again.");
                }
            },
            failure: function(response, opts) {
                crtnewdealpanel.setLoading(false);
                Ext.Msg.alert('Failed', "Please try again.");
            }
        });
	},
	
	uploadDealDocs: function(context,deal_has_doc,tmpdealid){
		var me = this;
		for(var i=0;i<deal_has_doc.length;i++){
			deal_has_doc[i].paramater1 = tmpdealid.toString();
		}
		if(deal_has_doc.length>0){
			//docselrepopanel.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DM2.view.AppConstants.apiurl+'mssql:DocProcessing',
                scope:this,
                method:"POST",
                params: Ext.util.JSON.encode(deal_has_doc),
                success: function(response, opts) {
                    //docselrepopanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
						me.getController("DealController").loadAllDeals(); //Load All Deals                 
						me.loadModifiedRecord(tmpdealid,context);
                    } else {
                        console.log("Generate File Failed.");
                        Ext.Msg.alert('Generate File Failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    //docselrepopanel.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Generate File', obj.errorMessage);
                }
            });
		} else {
			me.getController("DealController").loadAllDeals(); //Load All Deals                 
			me.loadModifiedRecord(tmpdealid,context);
		}
	},

    loadModifiedRecord: function(dealid,context) {
        var me = this;
		
        var dealdetailform = context.down('dealdetailsformpanel');
        dealdetailform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'v_rdeals',
            scope:this,
            method:'GET',
            params: {
                filter:'idDeal='+dealid
            },
            success: function(response, opts) {
                dealdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201) {
					if(obj.length>0){
						console.log("Load Modified Deal Record.");
						//Refresh & Load Details;
						var tmprec = Ext.create("DM2.model.Deal",obj[0]);
						
						var baseTabPanel = context.up('tab-basecardpanel');
						var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
						var tabdealgrid = tabdealpanel.down('tab-deal-grid');
						//Show Deal Detail Record
						me.getController('DealController').showDealDetail(tmprec,tabdealgrid);
					}
                } else {
                    console.log("Load Modified Deal Record Failed.");
                    Ext.Msg.alert('Modify Deal Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                dealdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Load Modified Deal', obj.errorMessage);
            }
        });
    },

    addDealHasUser: function(deal_has_user_arr,dealid,context) {
        console.log("Add User to Deal");
        var me = this;
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Deal_has_User',
            params: Ext.util.JSON.encode(deal_has_user_arr),
            scope:this,
            success: function(response, opts) {
                console.log("All User is added to deal.");
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 201){
					if(context.xtype == 'tab-deal-detail'){
						usergridpanel = context.down('dealdetailsformpanel').down('dealdetailusergridpanel');
					}
                    me.getController('DealUserController').loadDealUserDetailsWithParams(usergridpanel.config.alloractiveusermode,dealid,usergridpanel);
                } else {
                    console.log("All User is added to deal Failure.");
                    Ext.Msg.alert('Add user failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                console.log("All User is added to deal Failure.");
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Add user failed', obj.errorMessage);
            }
        });
    },

    onCreateNewDealPropertySelChange: function(selModel, selected, eOpts) {
        console.log("Property Grid Selection Change.");
        var me = this,tmprec,tmpdealname = "",selRecPrimFlag=false;
		var context = selModel.view.ownerCt.up('tab-deal-detail');
		var crtnewdealpanel = context.down('dealdetailsformpanel').down('createnewdealpanel');
        for(var i=0;i<selected.length;i++){
            tmprec = selected[i];
            console.log(tmprec);
            console.log(tmprec.get('primaryProperty'));
            if(tmprec.get('primaryProperty')===true){
                tmpdealname = tmprec.get('street_no')+" "+tmprec.get('street');
				selRecPrimFlag = true;
            }
        }
        crtnewdealpanel.down('#name').setValue(tmpdealname);
		var createnewdealpropertygrid =  crtnewdealpanel.down("#createnewdealpropertygrid");
		if(selRecPrimFlag==false && selected.length>0){
			for (var i = 0; i < createnewdealpropertygrid.getStore().getCount(); i++) {
				createnewdealpropertygrid.getStore().getAt(i).set('primaryProperty', false);
			}
			selected[0].set('primaryProperty',true);
		}
    },
	
	showAddDealUserView: function(cmp){
		var me = this,activepanel;
		var btn = cmp;
		if(cmp.xtype == 'menuitem') {
		    btn = cmp.up().spawnFrom;
		}
		
		if(btn.up('createnewdealpanel')){
			activepanel = btn.up('createnewdealpanel');
		} else if(btn.up('createnewdealfrompropertypanel')){
			activepanel = btn.up('createnewdealfrompropertypanel');
		} else if(btn.up('createnewsalesdealfrompropertypanel')){
			activepanel = btn.up('createnewsalesdealfrompropertypanel');
		}
		me.showAddDealUserPanel(activepanel,"add");
	},
	
	showAddDealUserPanel: function(cmp,frmmode) {
        var me = this,adddealuserpanel,tmprec,title,tabdealpanel,usergridpanel,sbmbtn;
		if(cmp.up('tab-properties')){
			tabpropertypanel = cmp.up('tab-properties');
			context = tabpropertypanel.getActiveTab();
		} else if(cmp.up('tab-contacts')){
			tabcntpanel = cmp.up('tab-contacts');
			context = tabcntpanel.getActiveTab();
		} else if(cmp.up('tab-sales')){
			tabsalespanel = cmp.up('tab-sales');
			context = tabsalespanel.getActiveTab();
		} else if(cmp.up('tab-deals')){
			tabdealspanel = cmp.up('tab-deals');
			context = tabdealspanel.getActiveTab();
		}
        switch(frmmode) {
            case "add":
                title = "Add User";    
            break;
            
            case "edit":
                title = "Edit User";
            break;
        }    
		
      	if(context.xtype == "tab-property-layout"){
            var eastregion = context.down('#property-east-panel');
			//eastregion.removeAll();
			if(context.down('adddealuserpanel')){
				console.log("Use Old Panel");
				adddealuserpanel = context.down('adddealuserpanel');
			} else {
				console.log("Create New Panel");
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');
				eastregion.add(adddealuserpanel);
			}
			//eastregion.setCollapsed(false);
			//eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(adddealuserpanel);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();
        } else if(context.xtype == "tab-property-detail"){
			var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			if(context.down('dealdetailsformpanel').down('adddealuserpanel')){
				adddealuserpanel = context.down('dealdetailsformpanel').down('adddealuserpanel');
			} else {
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');//me.getAdddealuserpanel();
			}
			eastregion.getLayout().setActiveItem(adddealuserpanel);
			adddealuserpanel.getHeader().show();
        } else if(context.xtype == "tab-contact-layout"){
            var eastregion = context.down('#contact-east-panel');
			//eastregion.removeAll();
			if(context.down('adddealuserpanel')){
				console.log("Use Old Panel");
				adddealuserpanel = context.down('adddealuserpanel');
			} else {
				console.log("Create New Panel");
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');
				eastregion.add(adddealuserpanel);
			}
			//eastregion.setCollapsed(false);
			//eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(adddealuserpanel);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();
        } else if(context.xtype == "tab-sales-detail"){
			var eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			if(context.down('salesdealdetailsformpanel').down('adddealuserpanel')){
				console.log("Use Old Panel");
				adddealuserpanel = context.down('salesdealdetailsformpanel').down('adddealuserpanel');
			} else {
				console.log("Create New Panel");
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');
			}
			eastregion.getLayout().setActiveItem(adddealuserpanel);
			adddealuserpanel.getHeader().show();
        } else if(context.xtype == "tab-deal-detail"){
			var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			var adddealuserpanel;
			if(context.down('dealdetailsformpanel').down('adddealuserpanel')){
				adddealuserpanel = context.down('dealdetailsformpanel').down('adddealuserpanel');
			} else {
				adddealuserpanel = Ext.create('DM2.view.AddDealUserPanel');
			}
			eastregion.getLayout().setActiveItem(adddealuserpanel);
			adddealuserpanel.getHeader().show();
        }
		usergridpanel = cmp.down('createnewdealusergridpanel');
		
		adddealuserpanel.setTitle(title);
		sbmbtn = adddealuserpanel.down('#adddealusersubmitbtn');

        adddealuserpanel.getForm().reset();

        if(frmmode==="edit"){
			adddealuserpanel.down('#adddealusersubmitbtn').setDisabled(true);
			sbmbtn.enable();
            if (usergridpanel.getSelectionModel().hasSelection()) {
                tmprec = usergridpanel.getSelectionModel().getSelection()[0];
            }
            //usrfrm.getForm().loadRecord(tmprec); 
			adddealuserpanel.getForm().findField('userName').setValue(tmprec.get('user'));
			adddealuserpanel.getForm().findField('idCSR').setValue(tmprec.get('csrid'));
			adddealuserpanel.getForm().findField('active').setValue(tmprec.get('active'));
			adddealuserpanel.getForm().findField('idDealxUser').setValue(tmprec.get('idDealxUser'));
			
			adddealuserpanel.getForm().findField('firstName').hide();
			adddealuserpanel.getForm().findField('address').hide();
			adddealuserpanel.getForm().findField('state').hide();
			adddealuserpanel.getForm().findField('lastName').hide();
			adddealuserpanel.getForm().findField('city').hide();
			adddealuserpanel.getForm().findField('zip').hide();
			adddealuserpanel.getForm().findField('position').hide();
			adddealuserpanel.down('grid').hide();
			adddealuserpanel.down('panel[region="north"]').setHeight("100%");
        } else {
			adddealuserpanel.getForm().findField('firstName').show();
			adddealuserpanel.getForm().findField('address').show();
			adddealuserpanel.getForm().findField('state').show();
			adddealuserpanel.getForm().findField('lastName').show();
			adddealuserpanel.getForm().findField('city').show();
			adddealuserpanel.getForm().findField('zip').show();
			adddealuserpanel.getForm().findField('position').show();
			adddealuserpanel.down('grid').show();
		}
		
		var rolestatic = Ext.getStore("RolesStatic");
        var rolest = adddealuserpanel.getForm().findField('role').getStore();		
        if(rolestatic.isLoaded()){
			rolest.loadData(rolestatic.data.items);   
			if(adddealuserpanel.isVisible()){
				if(frmmode==="add"){
					adddealuserpanel.getForm().findField('role').select(rolest.getAt(0));
				} else {
					adddealuserpanel.getForm().findField('role').select(tmprec.get('Role_idRole'));
					adddealuserpanel.down('#adddealusersubmitbtn').setDisabled(false);
				}
			}
        }
        
		if(frmmode==="add"){
			var csrst = adddealuserpanel.down('grid').getStore();
			if(csrst.isLoaded()) {
				if(adddealuserpanel.isVisible()) {
	                adddealuserpanel.down('grid').getSelectionModel().select(0);
				}
            } else {
				var csrstproxy = csrst.getProxy();
				var csrstproxyCfg = csrstproxy.config;
				csrstproxyCfg.headers = {
					Accept: 'application/json',
					Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
				};
				csrst.setProxy(csrstproxyCfg);
				csrst.load({
					callback: function(records, operation, success){
						console.log(adddealuserpanel.isVisible());
						if(adddealuserpanel.isVisible()) {
						   adddealuserpanel.down('grid').getSelectionModel().select(0);
						}
					}
				});
	        }
		}
		adddealuserpanel.config.backview = cmp;
    },
	
	addUserToNewDeal : function(panel,cmp){
		var me = this,eastregion,context;
		if(panel.up('tab-properties')){
			tabpropertypanel = panel.up('tab-properties');
			context = tabpropertypanel.getActiveTab();
			eastregion = context.down('#property-east-panel');
		} else if(panel.up('tab-contacts')){
			tabcntpanel = panel.up('tab-contacts');
			context = tabcntpanel.getActiveTab();
			eastregion = context.down('#contact-east-panel');
		} else if(panel.up('tab-sales')){
			tabsalespanel = panel.up('tab-sales');
			context = tabsalespanel.getActiveTab();
			eastregion = context.down('[region=east]');
		} else if(panel.up('tab-deals')){
			tabdealpanel = panel.up('tab-deals');
			context = tabdealpanel.getActiveTab();
			eastregion = context.down('[region=east]');
		}
		
		var rec = Ext.create('DM2.model.DealUser',{
		    active      : panel.getForm().findField('active').getValue(),
            user        : panel.getForm().findField('userName').getRawValue(),
			role        : panel.getForm().findField('role').getRawValue(),
            Role_idRole : panel.getForm().findField('role').getValue(),
            csrid       : panel.getForm().findField('idUser').getValue()
        });

        cmp.down('createnewdealusergridpanel').store.insert(0, rec);
		cmp.down('createnewdealusergridpanel').getSelectionModel().select(0,true);		
		
		eastregion.getLayout().setActiveItem(cmp);
	},
	
	beforeSelectUser: function(selModel, record, index, eOpts ){
		var me = this,createpanel;
		console.log("Before Select User");
		if(selModel.view.ownerCt.up('createnewdealpanel')){
			createpanel = selModel.view.ownerCt.up('createnewdealpanel');
		} else if(selModel.view.ownerCt.up('createnewdealfrompropertypanel')){
			createpanel = selModel.view.ownerCt.up('createnewdealfrompropertypanel');
		} else if(selModel.view.ownerCt.up('createnewsalesdealfrompropertypanel')){
			createpanel = selModel.view.ownerCt.up('createnewsalesdealfrompropertypanel');
		}
		if(record.get('role')=="Primary Broker" && record.get('Role_idRole')==1){
			var selRecords = createpanel.down('createnewdealusergridpanel').getSelectionModel().getSelection();
			var flag = false;
			for(var j=0;j<selRecords.length;j++){
				var temprec = selRecords[j];
				if(temprec.get('role')=="Primary Broker" && temprec.get('Role_idRole')==1){
					flag = true;
				}
			}
			if(flag){
				console.log("Primary broker is already selected.");
				Ext.Msg.alert('Alert!', "One Primary broker is already selected.Please deselect it before save another.");
				return false;
			}
		}
	},
	
	primaryPropertyCheckboxChanged: function( checkColumn , rowIndex , checked , record , eOpts){
		var me = this,tmpdealname = "";
		console.log("primary Property Checkbox Changed");
		var createnewdealpropertygrid = checkColumn.up('#createnewdealpropertygrid');
		var record = createnewdealpropertygrid.getStore().getAt(rowIndex);
		//console.log(record);
		if(checked){
			/*var record = this.grid.store.getAt(index);
			if (record.data[this.dataIndex]) {
				record.set(this.dataIndex, false);
			} else {
				for (var i = 0; i < this.grid.store.getCount(); i++) {
					this.grid.store.getAt(i).set(this.dataIndex, false);
				}
				record.set(this.dataIndex, true);
			}*/
			createnewdealpropertygrid.getSelectionModel().select(rowIndex,true);
			for (var i = 0; i < createnewdealpropertygrid.getStore().getCount(); i++) {
				createnewdealpropertygrid.getStore().getAt(i).set('primaryProperty', false);
			}			
			record.set('primaryProperty', true);
			
			var createnewdealpanel = createnewdealpropertygrid.up('createnewdealpanel');			
			tmpdealname = record.get('street_no')+" "+record.get('street');
			createnewdealpanel.down('textfield[name="name"]').setValue(tmpdealname);
		} else {
			//record.set('primaryProperty', true);
			return false;
		}
	},
	
	showAddDealPropertyView: function(btn){
		var me = this,adddealpropertyform;
		console.log("Show Add Deal Property View");
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
	
		if(eastregion.down('adddealpropertyform')){
			adddealpropertyform = eastregion.down('adddealpropertyform');
		} else {
			console.log("Add New Deal Property");
        	adddealpropertyform = Ext.create('DM2.view.AddDealPropertyForm');
		}
		eastregion.getLayout().setActiveItem(adddealpropertyform);	
		eastregion.config.backview = "adddealpropertyform";
		adddealpropertyform.config.backview = "createnewdeal";
	},
	
	addNewPropertyToDealClick: function(adddealpropertyform){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(adddealpropertyform);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
		var createnewdealpanel = context.down('createnewdealpanel');
		var allpropertiesgridpanel = context.down('adddealpropertyform').down('#adddealpropertygrid');
		if (allpropertiesgridpanel.getSelectionModel().hasSelection()) {
			row = allpropertiesgridpanel.getSelectionModel().getSelection()[0];
			console.log(row.get('idPropertyMaster'));
			propertyid = row.get('idPropertyMaster');
			lastDealID = row.get('lastDealID');
			lastDealStatus = row.get('lastDealStatus');
			if(adddealpropertyform.config.backview  && (adddealpropertyform.config.backview == "createnewdeal")){
				if((lastDealID==null) || (lastDealID!=null && lastDealStatus=='CL')){
				} else if(lastDealID!=null && lastDealStatus!='CL'){
					var title = "Property add failed";
					errText = "Property has deal with status not closed. Please try to add another property.";
					me.getController('DealDetailController').showToast(errText,title,'error',null,'tr',true);
					return false;
				}
			}
		} else {
			Ext.Msg.alert('Failed', "Please select property from Allproperties Grid.");
			return false;
		}
		me.getController('CreateNewDealFromPropertyController').loadFromPropertyTable(context,row,'createnewdealaddprop');
	},
	
	addNewPropertyToDeal: function(context,obj){
		var me = this;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
		var createnewdealpanel = context.down('createnewdealpanel');
		
		var rec = Ext.create('DM2.model.Property',{
		    propertyid  : obj.idProperty,
            street_no   : obj.streetNumber,
			street      : obj.streetName,
            city        : obj.city,
            state       : obj.state,
			primaryProperty   : false,
			mortgageType : context.down('adddealpropertyform').getForm().findField('mortgageType').getValue()
        });
		
		var recnum = createnewdealpanel.down('#createnewdealpropertygrid').store.getCount();//+1;
        createnewdealpanel.down('#createnewdealpropertygrid').store.insert(recnum, rec);
		createnewdealpanel.down('#createnewdealpropertygrid').getSelectionModel().select(recnum,true);

		eastregion.getLayout().setActiveItem(createnewdealpanel);		
	},
	
	onCreateNewDealPropertyGridRightClick: function(panel){
		var me = this;
		panel.getEl().on("contextmenu",function(e, t, eOpts) {
			console.log("Show Menu After Render of property grid.");
			e.stopEvent();
			var mainCmp = panel.up('createnewdealpanel');
			mainCmp.contextMenu.showAt(e.getXY());	
			mainCmp.contextMenu.config.viewPanel = panel;
		});
		
		var mtgtypeSt = Ext.getStore('MortgageTypeList');		
		var selectionList_st = Ext.getStore('SelectionList');		
		if(selectionList_st.isLoaded()){
			mtgtypeSt.loadData(selectionList_st.data.items);
			mtgtypeSt.filter("selectionType","MT");
			/*mtgtypeSt.each( function(record){
				if(record.get('isDefault')){
					//panel.down('#mortgageTypeProp').setRawValue(record.get('selectionDesc'));
				}
			});*/
		}
	},
	
	createNewDealPropertyGridMenuItemClick: function(item, e, eOpts){
		var me = this;
        if(item.action==="addproperty"){
			var btn = item.up('menu').config.viewPanel.down('button[action="showaddnewdealpropertybtn"]');
			if(item.up('menu').config.viewPanel.up('createnewdealpanel')){
				me.showAddDealPropertyView(btn);
			} else {
				me.getController('CreateNewDealFromPropertyController').showAddDealPropertyView(btn);
			}
        }
	},
	
	showAddDealContactView: function(btn){
		var me = this,adddealcontactform;
		console.log("Show Add Deal Contact View");
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
	
		if(eastregion.down('adddealcontactform')){
			adddealcontactform = eastregion.down('adddealcontactform');
		} else {
			console.log("Add New Deal Contact");
        	adddealcontactform = Ext.create('DM2.view.AddDealContactForm');
		}
		eastregion.getLayout().setActiveItem(adddealcontactform);	
		eastregion.config.backview = "adddealcontactform";
		adddealcontactform.getForm().reset();
		var filterstarr = [];
		me.getController('DealDetailController').loadContactAllSelBuff(filterstarr,context);
		adddealcontactform.config.backview = "createnewdeal";
	},
	
	onCreateNewDealContactGridRightClick: function(panel){
		var me = this;
		panel.getEl().on("contextmenu",function(e, t, eOpts) {
			console.log("Show Menu After Render of contact grid.");
			e.stopEvent();
			panel.contextMenu.showAt(e.getXY());	
			panel.contextMenu.config.viewPanel = panel;
		});		
	},
	
	createNewDealContactGridMenuItemClick: function(item, e, eOpts){
		var me = this;
        if(item.action==="addcontact"){
			var btn = item.up('menu').config.viewPanel.down('button[action="showaddnewdealcontactbtn"]');
			if(item.up('menu').config.viewPanel.xtype=="createnewdealcontactgrid"){
				me.showAddDealContactView(btn);
			} else {
				me.getController('CreateNewDealFromPropertyController').showAddDealContactView(btn);
			}
        }
	},
	
	addNewContactToDealClick: function(adddealcontactform){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(adddealcontactform);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
		var createnewdealpanel = context.down('createnewdealpanel');
		
		var adddealcontactgrid = context.down('adddealcontactform').down('adddealcontactgrid');
		if (adddealcontactgrid.getSelectionModel().hasSelection()) {
			row = adddealcontactgrid.getSelectionModel().getSelection()[0];
		} else {
			Ext.Msg.alert('Failed', "Please select property from Allproperties Grid.");
			return false;
		}		
		
		var mainCntCount=0,cntTypeVal;
		var cntGrid = createnewdealpanel.down('createnewdealcontactgrid');
		var cntSt = cntGrid.getStore();
		for(var j=0;j<cntSt.getCount();j++){
			var rec = cntSt.getAt(j);
			if(rec.get('contactType')=="MAIN CONTACT"){
				mainCntCount++;
			}
		}
		if(mainCntCount>0){
			cntTypeVal = "ACCOUNT MANAGER";
		} else {
			cntTypeVal = adddealcontactform.getForm().findField('contactType').getValue();
		}
		var rec = Ext.create('DM2.model.Contact',{
		    contactid     : row.get('contactid'),
		    fullName      : row.get('name'),
            contactType   : cntTypeVal,
			officePhone_1 : row.get('phone'),
            email_1       : row.get('email'),
			isprimary : false
        });

		var recnum = cntGrid.store.getCount();//+1;
        cntGrid.store.insert(recnum, rec);
		cntGrid.getSelectionModel().select(recnum,true);

		eastregion.getLayout().setActiveItem(createnewdealpanel);		
	},
	
	beforeCntTypeComplete: function(editor, context){
		var me = this,mainCntCount=0;
		console.log(context.value);
		console.log(context.originalValue);
		if (context.value == "MAIN CONTACT") {
			var cntGrid = context.grid;
			var cntSt = cntGrid.getStore();
			for(var j=0;j<cntSt.getCount();j++){
				var rec = cntSt.getAt(j);
				if(rec.get('contactType')=="MAIN CONTACT"){
					mainCntCount++;
				}
			}
			console.log("mainCntCount"+mainCntCount);
			if(mainCntCount>0){
				context.cancel = true;
				return false;
			}
		}
	}
});