Ext.define('DM2.controller.SendInvoiceController', {
    extend: 'Ext.app.Controller',
	
	requires:[
		'DM2.view.SendInvoicePanel'
	],
	
    refs: {
        sendinvoicepanel: {
            autoCreate: true,
            selector: 'sendinvoicepanel',
            xtype: 'sendinvoicepanel'
        }
    },

    init: function(application) {
        this.control({
            'sendinvoicepanel':{
                close: this.SendInvoicePanelCloseClick,
				afterrender: 'loadSendInvoiceDetails'
            },
			'sendinvoicepanel combobox[name="contactid"]':{
                select: this.onChangeContacts
            },
			'sendinvoicepanel #propertycmb':{
                select: this.onPropertyChange
            },
			'sendinvoicepanel button[action="sendinvoicebtn"]':{
                click: this.onSendInvoiceBtnClick
            }
        });
    },
	
	SendInvoiceClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this,sendinvoicepanel;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = tabdealdetail.down('[region=east]');
		if(detailpanel.down('sendinvoicepanel')){
			console.log("Use Old Panel");
			sendinvoicepanel = detailpanel.down('sendinvoicepanel');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	sendinvoicepanel = Ext.create('DM2.view.SendInvoicePanel');
		}
        eastregion.getLayout().setActiveItem(sendinvoicepanel);
		sendinvoicepanel.setTitle("Activities > "+activitytext);
    },
	
	loadSendInvoiceDetails: function(panel){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var cntcombo = panel.down('combobox[name="contactid"]');
		var cnt_st = cntcombo.getStore();
		cnt_st.loadData(context.dealContactData);
		var cnt_stcnt = cnt_st.getCount();
		if(cnt_stcnt>0){
			//Get the First Record
			var cntrec = cnt_st.getAt(0);           
			cntcombo.select(cntrec);
			cntcombo.fireEvent('select',cntcombo,cntrec);
		}
		
		var prpcombo = panel.down('#propertycmb');
		var prp_st = prpcombo.getStore();
		prp_st.loadData(context.dealPropertyData);
		var prp_stcnt = prp_st.getCount();
		if(prp_stcnt>0){
			//Get the First Record
			var prprec = prp_st.getAt(0);           
			prpcombo.select(prprec);
			prpcombo.fireEvent('select',prpcombo,prprec);
		}
        //me.loadClosingActions();
	},
	
    SendInvoicePanelCloseClick: function(panel) {
		var me = this;
		console.log("Send Invoice Panel Close");
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    onChangeContacts: function(combo, rec, eOpts) {
        var me = this;
        console.log("Fire select event");
		var context = me.getController('DealDetailController').getTabContext(combo);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var contactfrm = combo.up('sendinvoicepanel').down('#contactfrm');
        contactfrm.getForm().loadRecord(rec);
    },

    onPropertyChange: function(combo, rec, eOpts) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(combo);
        console.log("Fire select event");
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var propertyfrm = combo.up('sendinvoicepanel').down('#propertydetailfrm');
        propertyfrm.getForm().loadRecord(rec);
    },
	
	onSendInvoiceBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var record = context.record;
        var dealid = record.data.idDeal;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	}
});