Ext.define('DM2.controller.ReceiveBidController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({
        });
    },

    ReceiveBidClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this;
		tabdealdetail.config.activityid = activityid;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);

		var dealsalespanel = detailpanel.down('saledealdetailsalesfieldSet');
		salesrec = dealsalespanel.getData();

		me.getController('SalesDealDetailController').openAddSaleBidView("add",salesrec,"activity");
    }
});