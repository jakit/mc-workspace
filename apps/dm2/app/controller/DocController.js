Ext.define('DM2.controller.DocController', {
    extend: 'Ext.app.Controller',
	
	requires:['DM2.view.WopiWindow',
			  'DM2.view.ContactSelPanel',
			  'DM2.view.GeneralEditDocPanel'],
	
    refs: {
        dealdocsgridpanel: 'dealdocsgridpanel',
        docoptionmenu: {
            autoCreate: true,
            selector: 'docoptionmenu',
            xtype: 'docoptionmenu'
        },
        docgridtoolbar: '#docgridtoolbar',
		wopiwindow : 'wopiwindow'
    },

    init: function(application) {
        this.control({
            'dealdocsgridpanel': {
				activate: this.afterDealDocsGridPanelActivate,
                load: this.onDocgridLoad,
                rowcontextmenu : this.onDealDocsGrigRightClick,
				select : this.onDealDocGridItemSelect,
				beforeitemmousedown: this.onDealDocGridItemMouseDown,
				itemclick : this.onDocGridItemClick
				//cellclick : this.onDocGridCellItemClick
            },
			'dealdocsgridpanel dataview': {
                itemlongpress: this.onDocsGrigItemLongPress
            },
            'dealdocsgridpanel #adddocfilefieldbtn': {
                click: this.showRecvDealDocs
            },
            'docoptionmenu menuitem':{
                click: this.docoptionitemclick
            },
			'wopiwindow':{
                close:this.wopiWindowCloseBtnClick
            },
			'contactselpanel':{
                close:this.contactSelPanelCloseClick
            },
			'contactselpanel button[action="emailbtn"]': {
				click: this.emailFaxBtnClick
			},
			'contactselpanel button[action="faxbtn"]': {
				click: this.emailFaxBtnClick
			},
			'contactselpanel button[action="filterbtn"]': {
				click: this.filterBtnClick
			},
			'generaleditdocpanel':{
				close: this.generalEditDocPanelClose
			},
			'generaleditdocpanel button[action="docsavebtn"]':{
				click: this.generalEditDocBtnSaveClick
			}
        });
    },
	
	afterDealDocsGridPanelActivate: function(panel){
		var me = this,context,record,dealid;
		if(panel.up('tab-deal-detail')){
			context = panel.up('tab-deal-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-sales-detail')){
			context = panel.up('tab-sales-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-deal-layout')){
			context = panel.up('tab-deal-layout');
			dealgrid = context.down('tab-deal-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				//Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		} else if(panel.up('tab-sales-layout')){
			context = panel.up('tab-sales-layout');
			dealgrid = context.down('tab-sales-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				//Ext.Msg.alert('Failed', "Please select sales deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		}
		
		var documentsproxy = panel.getStore().getProxy();
		var documentsproxyCfg = documentsproxy.config;
        documentsproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		if(dealid){
			var filterstarr = [];
			var filterst1 = "dealid="+dealid;
			filterstarr.push(filterst1);
			documentsproxyCfg.extraParams = {
				filter :filterstarr
			};
		}
		documentsproxyCfg.url = DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal';
		documentsproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal';
		panel.getStore().setProxy(documentsproxyCfg);
	},

    onDocgridLoad: function(cmp, e, file) {
        console.log("Done Reading");
        var me = this;
		var activemenutab = cmp.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		permission = context.dealDetailStore.data.items[0].data.permission.modifiable;
		console.log("Permission Modifiable : "+permission);
		//permission = 1;
		if(parseInt(permission)===0){						
			return false;
		}
		//console.log("Stop Permission Modifiable : "+permission);
		
		if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDocs',context);
		} else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
		}		
		me.getController('GeneralDocSelController').onGeneralAddDealDocPanelDrop(cmp, e, file);
    },

    loadDocsStore: function(dealid) {
        var me = this;
        var documentsproxy = Ext.getStore('Documents').getProxy();
        documentsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('Documents').load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
                var doctoolbar = me.getDocgridtoolbar();
                console.log("Doc Cnt"+Ext.getStore('Documents').getCount());
                if(Ext.getStore('Documents').getCount()>0){
                    if(doctoolbar){
                        doctoolbar.hide();
                    }
                } else {
                    if(doctoolbar){
                        doctoolbar.show();
                    }
                }
            }
        });
    },
	
	loadDocsPanelStore: function(panel) {
		var me = this,context,record,dealid;
		if(panel.up('tab-deal-detail')){
			context = panel.up('tab-deal-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-sales-detail')){
			context = panel.up('tab-sales-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-deal-layout')){
			context = panel.up('tab-deal-layout');
			dealgrid = context.down('tab-deal-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				//Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		} else if(panel.up('tab-sales-layout')){
			context = panel.up('tab-sales-layout');
			dealgrid = context.down('tab-sales-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				//Ext.Msg.alert('Failed', "Please select sales deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		}
		
		var documentsproxy = panel.getStore().getProxy();
		var documentsproxyCfg = documentsproxy.config;
        documentsproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		var filterstarr = [];
		var filterst1 = "dealid="+dealid;
        filterstarr.push(filterst1);
		documentsproxyCfg.extraParams = {
            filter :filterstarr
        };
		documentsproxyCfg.url = DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal';
		documentsproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal';
		panel.getStore().setProxy(documentsproxyCfg);
		
        panel.getStore().load({
            /*params:{
                filter :'dealid = '+dealid
            },*/
            callback: function(records, operation, success){
                me.afterDealDocsDataLoad(panel);
            }
        });
    },
	
	loadAllDocsPerDealPanelStore: function(panel) {
        var me = this,context,record,dealid;
		if(panel.up('tab-deal-detail')){
			context = panel.up('tab-deal-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-sales-detail')){
			context = panel.up('tab-sales-detail');
			record = context.record;
			dealid = record.data.idDeal;
		} else if(panel.up('tab-deal-layout')){
			context = panel.up('tab-deal-layout');
			dealgrid = context.down('tab-deal-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		} else if(panel.up('tab-sales-layout')){
			context = panel.up('tab-sales-layout');
			dealgrid = context.down('tab-sales-grid');
			if(!dealgrid.getSelectionModel().hasSelection()) {
				Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
				return;
			}
			var row = dealgrid.getSelectionModel().getSelection()[0];
			dealid = row.data.idDeal;
		}
		
		var documentsproxy = panel.getStore().getProxy();
		var documentsproxyCfg = documentsproxy.config;
        documentsproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		var filterstarr = [];
		var filterst1 = "idDeal="+dealid;
        filterstarr.push(filterst1);
		documentsproxyCfg.extraParams = {
            filter :filterstarr
        };
		documentsproxyCfg.url = DM2.view.AppConstants.apiurl+'mssql:v_docsPerDealAll';
		documentsproxyCfg.originalUrl = DM2.view.AppConstants.apiurl+'mssql:v_docsPerDealAll';
		panel.getStore().setProxy(documentsproxyCfg);
		
        /*var documentsproxy = panel.getStore().getProxy();		
		documentsproxy.setUrl(DM2.view.AppConstants.apiurl+"mssql:v_docsPerDealAll");
        documentsproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
        panel.getStore().load({
            /*params:{
                filter :'idDeal = '+dealid
            },*/
            callback: function(records, operation, success){
				me.afterDealDocsDataLoad(panel);
                /*var doctoolbar = me.getController('DocController').getDocgridtoolbar();
                console.log("Doc Cnt"+Ext.getStore('Documents').getCount());
                if(Ext.getStore('Documents').getCount()>0){
                    if(doctoolbar){
                        doctoolbar.hide();
                    }                    
                } else {
                    if(doctoolbar){
                        doctoolbar.show();
                    }                    
                }*/
				documentsproxy.setUrl(DM2.view.AppConstants.apiurl+"mssql:v_docsPerDeal");
            }
        });
    },
	
	bindDealDocs: function(tabItem){
		console.log("Bind Deal Docs");
		var me = this;
		var panel = tabItem.down('dealdocsgridpanel');
		var dealdocsst = panel.getStore();
		dealdocsst.loadData(tabItem.dealDetailStore.data.items[0].data.DocsPerDeal);
		me.afterDealDocsDataLoad(panel);		
	},
	
	clearDealDocs: function(tabItem){
		console.log("Clear Deal Docs");
		var me = this;
		var panel = tabItem.down('dealdocsgridpanel');
		var dealdocsst = panel.getStore();
		dealdocsst.removeAll();
		me.afterDealDocsDataLoad(panel);		
	},
	
	afterDealDocsDataLoad: function(panel){
		var me = this,permission,tabItem;
		var dealdocsst = panel.getStore();
		var count = dealdocsst.getCount();		
		var doctoolbar = panel.down('toolbar');
		if(count>0){
			if(doctoolbar){
				doctoolbar.hide();
			}
		} else {
			if(doctoolbar){
				doctoolbar.show();
				
				if(panel.up('tab-deal-detail')){
					tabItem = panel.up('tab-deal-detail');
				} else if(panel.up('tab-deal-layout')){
					tabItem = panel.up('tab-deal-layout');
				} else if(panel.up('tab-sales-layout')){
					tabItem = panel.up('tab-sales-layout');
				 }else if(panel.up('tab-sales-detail')){
					tabItem = panel.up('tab-sales-detail');
				}
				if(tabItem.dealDetailStore){
					permission = tabItem.dealDetailStore.data.items[0].data.permission.modifiable;
					console.log("Permission : "+permission);
					if(parseInt(permission)===0){						
						doctoolbar.down('button').disable();
					} else {
						doctoolbar.down('button').enable();
					}
				}
			}
		}
	},
	
	onDocsGrigItemLongPress: function(view, record , item , index , e , eOpts) {
		var me = this;
		view.getSelectionModel().select(record);
		me.showDealDocsGridMenu(view,record,e);
	},
	
	showDealDocsGridMenu: function(view,record,e){
		var me = this;
		e.stopEvent();
		if(me.getDocoptionmenu()){
			var tabItem = view.ownerCt.up('tab-deal-detail');
			//var tabItem = tabdealpanel.getActiveTab();
			if(tabItem){
			    DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,me.getDocoptionmenu());
			}
			var tabdeallayout = view.ownerCt.up('tab-deal-layout');
			if(tabdeallayout){
			    DM2.app.getController('DealDetailController').disableModifyMenu(tabdeallayout,me.getDocoptionmenu());
			}
			me.getDocoptionmenu().spawnFrom = view;
            me.getDocoptionmenu().showAt(e.getXY());
        }
	},
	
    onDealDocsGrigRightClick: function(view, record, tr, rowIndex, e, eOpts) {        
		var me = this;
        console.log("Show Menu for Doc Add/Edit/Histroy");
        me.showDealDocsGridMenu(view,record,e);
    },

    docoptionitemclick: function(item, e, eOpts) {
        console.log("Doc Menu Item Clicked.");
        var me = this;
        if(item.getItemId()==="docedit"){
            console.log("Doc Edit Menu Item Clicked");
			me.showGeneralEditDocPanel(item);
        } else if(item.getItemId()==="dochistory"){
            console.log("Doc History Menu Item Clicked");
        } else if(item.getItemId()==="detaildocadd"){
			me.showRecvDealDocs(item);
        } else if(item.getItemId()==="emaildocument" || item.getItemId()==="faxdocument"){
			me.showEmailDocs(item);
        } else if(item.getItemId()==="printdocument"){
			me.printDocs(item);
        } else if(item.getItemId()==="deletedoc"){
			me.deleteDocument(item);
        } else if(item.getItemId()==="downloaddocument"){
			me.downloadDocs(item);
        } 
    },
	
	showRecvDealDocs: function(cmp) {
		var me = this,
            tabpanel, context, spawnFrom, grid,trgtcmp;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
		    // when cmp is a button.
			trgtcmp = cmp;
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		}
		context = tabpanel.getActiveTab();
		
		if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDocs',context);	
        } else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
		}
	},
	
	closeRecvDealDetailDocs: function(context) {
		var me = this;
		if(context.xtype == "tab-deal-layout"){
			var eastregion = context.down('#deal-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        } else if(context.xtype == "tab-sales-layout"){
			var eastregion = context.down('#sales-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        } else if(context.xtype == "tab-deal-detail"){
	        var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
    	    eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		} else if(context.xtype == "tab-sales-detail"){
	        var eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
    	    eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
	},
	
	/*
	 * Get Deal Doc From Outlook
	 */
	getDealDocFromOutlook: function(fna, fca) {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDocs',context);
		me.getController('GeneralDocSelController').getGeneralDocFromOutlook(fna, fca);
    },
	
	/*
	 * Get Deal Detail Doc From Outlook
	 */
	getDealDetailDocFromOutlook: function(fna, fca) {
        var me = this;
		var tabdealpanel = me.getController('MainController').getMainviewport().down('tab-deals');
		var context = tabdealpanel.getActiveTab();
		me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvDealDetailDocs',context);
		me.getController('GeneralDocSelController').getGeneralDocFromOutlook(fna, fca);
    },
	
	onDealDocGridItemSelect: function(selModel, record, index, eOpts){
		var me = this,docgrid,tabItem;
		console.log("Deal Docs Grid Item Select event fired & delayed");
		
		if(selModel.view.ownerCt.up('tab-deal-detail')){
			tabItem = selModel.view.ownerCt.up('tab-deal-detail');
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		} else if(selModel.view.ownerCt.up('tab-sales-detail')){
			tabItem = selModel.view.ownerCt.up('tab-sales-detail');
			docgrid = tabItem.down('salesdealdetailsformpanel').down('dealdocsgridpanel');
		} else if(selModel.view.ownerCt.up('tab-deal-layout')){
			tabItem = selModel.view.ownerCt.up('tab-deal-layout');
			docgrid = tabItem.down('dealdocsgridpanel');
		} else if(selModel.view.ownerCt.up('tab-sales-layout')){
			tabItem = selModel.view.ownerCt.up('tab-sales-layout');
			docgrid = tabItem.down('dealdocsgridpanel');
		}
		docgrid.config.allowSelection = true;
		docgrid.config.openWindow = true;
		setTimeout(function() {
			console.log("Deal Docs Grid Item Select");
			console.log(docgrid.config.allowSelection);
			if(docgrid.config.allowSelection){
				console.log(docgrid.config.openWindow);
				if(docgrid.config.openWindow){
					var docurl = record.get('url');
					var extension = record.get('ext');
					var docsupportformat = "no";
					if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xlsx" || extension==="pdf"){
						docsupportformat = "yes";
					}
					me.openWopiDocWindow(docurl,docsupportformat);
				} else {
					docgrid.config.openWindow = true;
				}
			} else {
				docgrid.config.allowSelection = true;
			}
		}, 300);
	},
	
	onDealDocGridItemMouseDown: function(view, record, item, index, e, eOpts){
		console.log("Deal Doc Grid Item Mouse Down");
		var me = this,docgrid,tabItem;

		if(view.up('tab-deal-detail')){
			tabItem = view.up('tab-deal-detail');
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		}else if(view.up('tab-sales-detail')){
			tabItem = view.up('tab-sales-detail');
			docgrid = tabItem.down('salesdealdetailsformpanel').down('dealdocsgridpanel');
		} else if(view.up('tab-deal-layout')){
			tabItem = view.up('tab-deal-layout');
			docgrid = tabItem.down('dealdocsgridpanel');
		} else if(view.up('tab-sales-layout')){
			tabItem = view.up('tab-sales-layout');
			docgrid = tabItem.down('dealdocsgridpanel');
		}
		
		if (e.button==0) {
			var doclink = e.getTarget('.doclink');
			if(doclink) {
				docgrid.config.openWindow = false;
			} else {
				docgrid.config.openWindow = true;
			}
			docgrid.config.allowSelection=true;
		} else { 
			docgrid.config.allowSelection=false;
		}
	},
	/*
	 * Not Used
	 */
	onDocGridCellItemClick: function(viewTable, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		console.log("Deal Docs Grid Item Cell Click");
		var me = this;
		console.log(e.button);
		if(e.button == 2) {      
			/*console.log("cancel rightclick");*/
			  //docgrid.config.allowSelection=false;
		} else {
			console.log(viewTable.ownerCt.columns[cellIndex].text);
			var docurl = record.get('url');
			var extension = record.get('ext');
			
			var extImg = DM2.app.getController('MainController').getExtImg(docurl);
			var targetframe = DM2.app.getController('MainController').getTargetFrame(extension);
			
			if(viewTable.ownerCt.columns[cellIndex].text && viewTable.ownerCt.columns[cellIndex].text=="Doc Name"){
				window.open(docurl, targetframe);
			} else {
				var docsupportformat = "no";
				if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xlsx" || extension==="pdf"){
					docsupportformat = "yes";
				}
				me.openWopiDocWindow(docurl,docsupportformat);
			}
		}
	},
	
	onDocGridItemClick: function(grid, record, item, index, e, eOpts ){
		console.log("Deal Docs Grid Item Click");
		var me = this,tabItem,docgrid;
		console.log(e.button);
		if(grid.up('tab-deal-detail')){
			tabItem = grid.up('tab-deal-detail');
			docgrid = tabItem.down('dealdetailsformpanel').down('dealdocsgridpanel');
		} else if(grid.up('tab-sales-detail')){
			tabItem = grid.up('tab-sales-detail');
			docgrid = tabItem.down('salesdealdetailsformpanel').down('dealdocsgridpanel');
		} else if(grid.up('tab-deal-layout')){
			tabItem = grid.up('tab-deal-layout');
			docgrid = tabItem.down('dealdocsgridpanel');
		} else if(grid.up('tab-sales-layout')){
			tabItem = grid.up('tab-sales-layout');
			docgrid = tabItem.down('dealdocsgridpanel');
		}
		if(e.button == 2) {      
			/*console.log("cancel rightclick");*/
			  docgrid.config.allowSelection=false;
		}else{
			var doclink = e.getTarget('.doclink');
			if(doclink) {
				docgrid.config.openWindow = false;
			} else {
				docgrid.config.openWindow = true;
				var docurl = record.get('url');
				var extension = record.get('ext');
				//if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xls" || extension==="xlsx" || extension==="pdf"){
				var docsupportformat = "no";
				if(extension==="docx" || extension==="pptx" || extension==="doc" || extension==="ppt" || extension==="xlsx" || extension==="pdf"){
					docsupportformat = "yes";
				}
				me.openWopiDocWindow(docurl,docsupportformat);
			}
		}
	},
	
	openWopiDocWindow: function(docurl,docsupportformat){
		var me = this,cmp,tabdealpanel, context, spawnFrom, grid,eastregion,title = "";
		var apiserver = DM2.view.AppConstants.apiurl;
		var httpVal = me.getHTTP(apiserver);
		var docurldasharr = docurl.split("/");
		console.log("docurl:"+docurldasharr[2]);
		
		var docurlarr = docurl.split(docurldasharr[2]);
		console.log("url");
		console.log(docurlarr[1]);
		
		/*var wopiwindow = me.getWopiwindow();
		if(wopiwindow){
			wopiwindow.close();
		}*/
		if(docsupportformat=="yes"){
			cmp = Ext.create('Ext.Component', {
				autoEl : {
					tag : "iframe",
					//src: 'http://devdmdocs/_layouts/15/wopiframe.aspx?sourcedoc=/Shared%20Documents/00812/00812403.doc'
					src: httpVal+docurldasharr[2]+'/_layouts/15/wopiframe2.aspx?sourcedoc='+docurlarr[1]+'&action=embedview&Embed=1'
				}
			});
		} else {
			cmp = Ext.create('Ext.panel.Panel', {
				layout:{
					type:'hbox',
					align:'center'
				},				
				items:[{
					layout:{
						type:'vbox',
						align:'stretch'
					},
					style:'text-align:center;',
					width:'100%',
					items:[{
						html : '<span style="font-size: 18px;font-weight: bold;color: red;">This file type can\'t be displyed.</span>'
				    }]
				}]
			});
		}
		
		//wopiwindow.show();
		
		////			
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();			
		
		if(context.xtype == "tab-deal-layout" || context.xtype == "tab-property-layout" || context.xtype == "tab-contact-layout" || context.xtype == "tab-sales-layout"){
			if(context.xtype == "tab-deal-layout"){
				eastregion  = context.down('#deal-east-panel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion  = context.down('#sales-east-panel');
			} else if(context.xtype == "tab-property-layout"){
				eastregion = context.down('#property-east-panel');
			} else if(context.xtype == "tab-contact-layout"){
				eastregion = context.down('#contact-east-panel');
			}

			var wopiwindow;
			if(context.down('wopiwindow')){
				console.log("Use Old Panel");
				wopiwindow = context.down('wopiwindow');
				wopiwindow.close();
			}
			wopiwindow = Ext.create('DM2.view.WopiWindow',{
				closeAction : 'destroy'
			});
			wopiwindow.add(cmp);
			eastregion.add(wopiwindow);
			
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(wopiwindow);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();			
			eastregion.config.backview = "wopiwindow";
        } else {
			var wopiwindow,eastregion;
			if(context.xtype == "tab-deal-detail"){
				eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			} else if(context.xtype == "tab-sales-detail"){
				eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			}			
			//if(context.down('dealdetailsformpanel').down('wopiwindow')){
			if(context.down('wopiwindow')){
				console.log("Use Old Panel");
				wopiwindow = context.down('wopiwindow');
				wopiwindow.close();
			}
			wopiwindow = Ext.create('DM2.view.WopiWindow',{
				closeAction : 'destroy'
			});
			wopiwindow.add(cmp);
			eastregion.add(wopiwindow);
			
			if(wopiwindow.getHeader()){
				wopiwindow.getHeader().show();
			}
			eastregion.getLayout().setActiveItem(wopiwindow);		
			eastregion.config.backview = "wopiwindow";
		}
        title = "Docviewer";
        wopiwindow.setTitle(title);
		////
	},
	wopiWindowCloseBtnClick: function(panel){
		console.log("Wopi Window Close Button Click");
		var me = this;
		var activemenutab = panel.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		if(context.xtype == "tab-deal-layout"){
			var eastregion = context.down('#deal-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        }else if(context.xtype == "tab-sales-layout"){
			var eastregion = context.down('#sales-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        } else if(context.xtype == "tab-property-layout"){
			me.getController('PropertyController').closeRecvDealDetailDocs(context);
        } else if(context.xtype == "tab-contact-layout"){
			me.getController('ContactController').closeRecvDealDetailDocs(context);
        } else if(context.xtype == "tab-deal-detail"){
			var eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			var backview = eastregion.config.backview;
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		} else if(context.xtype == "tab-sales-detail"){
			var eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			var backview = eastregion.config.backview;
			eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
		}
	},
	
	showEmailDocs: function(cmp) {
		var me = this,
            tabpanel, context, spawnFrom, grid,trgtcmp;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
		    // when cmp is a button.
			trgtcmp = cmp;
			grid = cmp.up('grid');
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		} else if(trgtcmp.up('tab-properties')){
			tabpanel = trgtcmp.up('tab-properties');
		} else if(trgtcmp.up('tab-contacts')){
			tabpanel = trgtcmp.up('tab-contacts');
		}
		context = tabpanel.getActiveTab();
		
		if(context.xtype == "tab-deal-layout" || context.xtype == "tab-sales-layout" || context.xtype == "tab-property-layout" || context.xtype == "tab-contact-layout"){
			me.getController('DocController').showContactSelPanel(context,grid,"layout");	
        } else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			me.getController('DocController').showContactSelPanel(context,grid,"detail");
		}
	},
	
	showContactSelPanel: function(context,grid,st){
		var me = this,eastregion,contactselpanel,st;
		
		if(st==="layout"){
			console.log(context.xtype);
			if(context.xtype == "tab-property-layout"){
				eastregion = context.down('#property-east-panel');
			} else if(context.xtype == "tab-contact-layout"){
				eastregion  = context.down('#contact-east-panel');
			} else if(context.xtype == "tab-sales-layout"){
				eastregion  = context.down('#sales-east-panel');
			} else {
				eastregion  = context.down('#deal-east-panel');
			}

			if(context.down('contactselpanel')){
				console.log("Use Old Panel");
				contactselpanel = context.down('contactselpanel');
			} else {
				console.log("Create New east contact selection doc Panel");
				contactselpanel = Ext.create('DM2.view.ContactSelPanel');
				eastregion.add(contactselpanel);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(contactselpanel);			
			eastregion.setTitle("Docs");
			eastregion.getHeader().hide();			
			eastregion.config.backview = st;
        } else {
			
			var eastregion = context.down('[region=east]');
			if(context.down('contactselpanel')){
				console.log("Use Old Panel");
				contactselpanel = context.down('contactselpanel');
			} else {
				console.log("Create New Panel");
				contactselpanel = Ext.create('DM2.view.ContactSelPanel');
			}
			if(contactselpanel.getHeader()){
				//contactselpanel.getHeader().show();
			}
			if(context.config.activityid){
				contactselpanel.config.activityid = context.config.activityid;
			}
			eastregion.getLayout().setActiveItem(contactselpanel);		
			eastregion.config.backview = st;
		}		
		me.filterBtnClick(contactselpanel.down('button[action="filterbtn"]'));
		//me.loadContactSelectionPanelData(contactselpanel);
		var docgrid = contactselpanel.down('#docsgrid');
		//docgrid.getStore().loadData(grid.getStore().data.items);
		if (grid.getSelectionModel().hasSelection()) {
			recs = grid.getSelectionModel().getSelection();
			docgrid.getStore().loadData(recs);
		}
	},
	
	loadContactSelectionPanelData: function(contactselpanel,recs){
		var me = this,filterstarr;
		var cntsel_st = contactselpanel.down('contactselgrid').getStore();
		var cntsel_stproxy = cntsel_st.getProxy();
      
		var cntsel_stproxyCfg = cntsel_stproxy.config;
        cntsel_stproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		cntsel_st.setProxy(cntsel_stproxyCfg);
		
		///Build FIlter Param String
		if(recs.length>0){
			var cntidarr = [];
			for(var i=0;i<recs.length;i++){
				var tmprec = recs[i];
				console.log(tmprec);
				if(tmprec.contactid){
					cntidarr.push(tmprec.contactid);
				} else {
					cntidarr.push(tmprec.get('contactid'));
				}
			}
			filterstarr = "contactid in ("+cntidarr.join()+")";
		} else {
			filterstarr = [];
		}
        cntsel_st.load({
            params:{
                filter :filterstarr
            }
        });
	},
	
	contactSelPanelCloseClick: function(panel){
		var me = this;
		var activemenutab = panel.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		if(context.xtype == "tab-deal-layout"){
			var eastregion = context.down('#deal-east-panel');
			eastregion.removeAll();
			eastregion.setVisible(false);
        } else if(context.xtype == "tab-sales-layout"){
			var eastregion = context.down('#sales-east-panel');
			eastregion.removeAll();
			eastregion.setVisible(false);
        } else if(context.xtype == "tab-property-layout"){
			me.getController('PropertyController').closeRecvDealDetailDocs(context);
        } else if(context.xtype == "tab-contact-layout"){
			me.getController('ContactController').closeRecvDealDetailDocs(context);
        } else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			var eastregion;
			if(context.xtype == "tab-deal-detail"){
				eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			} else if(context.xtype == "tab-sales-detail"){
				eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			}

			var backview = eastregion.config.backview;
			me.getController('DocController').closeRecvDealDetailDocs(context);
		}
	},
	
	emailFaxBtnClick: function(btn){
		var me = this;
		console.log("Email Btn Click");
		me.setEmailFaxLink(btn,btn.action);
	},
	
	setEmailFaxLink: function(btn,action) {
        var me = this;
		var contactselpanel = btn.up('contactselpanel');
        var docsgrid = contactselpanel.down('#docsgrid');
		var contactselgrid = contactselpanel.down('contactselgrid');
		
        var recs = [];
        recs = docsgrid.getStore().data.items;

        //var emailbtn = senddocumentpanel.down('#emailbtn');
        //var faxbtn = senddocumentpanel.down('#faxbtn');
		console.log(recs);
		console.log("Length"+recs.length);
        if(recs.length > 0){
			
            var docurllistarr = [];
            var docnamelistarr = [];
            //Get the Docs
            Ext.Array.each(recs, function(sendloidocrec) {
                var docurl = sendloidocrec.get('url');
                //docurl = "http://ravenoustech.com/company-profile.pdf";
                var docname = sendloidocrec.get('name');
				var tmpdocurlarr = docurl.split("http://");
				var finaldocurl = "http://"+tmpdocurlarr[1];
                docurllistarr.push(finaldocurl);
                docnamelistarr.push(docname);
            });

            var docurllistst  = docurllistarr.join("&ATTACHITEM#");
            var docnamelistst = docnamelistarr.join("&ATTACHNAME#");

            var bodyMsg = "";
            //var subject = recs[0].get('clsfctn');
			var subject = recs[0].get('desc');

			var contactselgridst = contactselgrid.getStore();
            var emaillistst = "",faxlistst = "";
            if (contactselgrid.getSelectionModel().hasSelection()) {
				recs = contactselgrid.getSelectionModel().getSelection();
				for(var k=0; k < recs.length; k++){
					var tmprec = recs[k];
					var emailval = tmprec.get('email');
					var faxval = tmprec.get('fax');
	
					if(emailval!=="" && emailval!==null && emailval!==undefined){
						emaillistst = emaillistst +emailval + ";";
					}
					if(faxval!=="" && faxval!==null && faxval!==undefined){
						faxlistst = faxlistst +faxval + "@fax.local;";
					}
				}
            }

            var mailsep = "&MAILPART#";
            var mailhrefst = "mcgmt:"+emaillistst+mailsep+subject+mailsep+bodyMsg+mailsep+docurllistst+mailsep+docnamelistst;
			//console.log(mailhrefst);
            var faxhrefst = "mcgmt:"+faxlistst+mailsep+subject+mailsep+bodyMsg+mailsep+docurllistst+mailsep+docnamelistst;

            //faxbtn.setHref(faxhrefst);
            //emailbtn.setHref(mailhrefst);
			
			if(action=="emailbtn"){
				console.log(mailhrefst);
				window.open(mailhrefst,"_self");
			} else if(action=="faxbtn") {
				console.log(faxhrefst);
				window.open(faxhrefst,"_self");
			}
        }
    },
	
	filterBtnClick:function(btn){
		var me = this;
		console.log("Filter Btn Click.");
		if(btn.up('tab-contacts')){
			var tabpanel = btn.up('tab-contacts');
			var context = tabpanel.getActiveTab();
			var contactselpanel = btn.up('contactselpanel');
			me.loadContactSelectionPanelData(contactselpanel,[]);
			me.setButtonText(btn);
		} else {
			btn.show();
			if(btn.actiontext=="all"){
				//Load All the Records And change the text of the button
				var contactselpanel = btn.up('contactselpanel');
				me.loadContactSelectionPanelData(contactselpanel,[]);						
				btn.actiontext = "filter";
				me.setButtonText(btn);
			} else if(btn.actiontext=="filter"){
				///Filter Records based on deal,property,contact menu
				if(btn.up('tab-deals')){
					var tabpanel = btn.up('tab-deals');
					var context = tabpanel.getActiveTab();
					var contactselpanel = btn.up('contactselpanel');
					me.loadContactSelectionPanelData(contactselpanel,context.dealContactData);
				} else if(btn.up('tab-sales')){
					var tabpanel = btn.up('tab-sales');
					var context = tabpanel.getActiveTab();
					var contactselpanel = btn.up('contactselpanel');
					me.loadContactSelectionPanelData(contactselpanel,context.dealContactData);
				} else if(btn.up('tab-properties')){
					var tabpanel = btn.up('tab-properties');
					var context = tabpanel.getActiveTab();
					var contactselpanel = btn.up('contactselpanel');
					me.loadContactSelectionPanelData(contactselpanel,context.dealContactData);
				}
				btn.actiontext = "all";
				me.setButtonText(btn);
			}
		}
	},
	
	setButtonText: function(btn){
		var me = this;
		var contactselgrid = btn.up('contactselgrid');
		if(btn.up('tab-deals')){
			if(btn.actiontext=="filter"){
				contactselgrid.setTitle("Contacts");
				btn.setText("Use DealContacts");
			} else if(btn.actiontext=="all"){
				contactselgrid.setTitle("Deal Contacts");
				btn.setText("Use Contacts");
			}
		} else if(btn.up('tab-sales')){
			if(btn.actiontext=="filter"){
				contactselgrid.setTitle("Contacts");
				btn.setText("Use SaleContacts");
			} else if(btn.actiontext=="all"){
				contactselgrid.setTitle("Sale Contacts");
				btn.setText("Use Contacts");
			}
		} else if(btn.up('tab-contacts')){
			if(btn.actiontext=="filter"){
				contactselgrid.setTitle("Contacts");
				btn.hide();
			}
		}
	},
	
	printDocs: function(cmp) {
		var me = this,
            tabpanel, context, spawnFrom, grid,trgtcmp;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
		    // when cmp is a button.
			trgtcmp = cmp;
			grid = cmp.up('grid');
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		} else if(trgtcmp.up('tab-properties')){
			tabpanel = trgtcmp.up('tab-properties');
		} else if(trgtcmp.up('tab-contacts')){
			tabpanel = trgtcmp.up('tab-contacts');
		}
		context = tabpanel.getActiveTab();
		if (grid.getSelectionModel().hasSelection()) {
			recs = grid.getSelectionModel().getSelection();
			var urlval = recs[0].get('url');
			window.open(urlval);
		}		
	},
	
	deleteDocument: function(cmp){
		console.log("Delete Document.");
		var me = this,grid;
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
			grid = cmp.up('grid');		    // when cmp is a button.
			trgtcmp = cmp;
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		} else if(trgtcmp.up('tab-properties')){
			tabpanel = trgtcmp.up('tab-properties');
		} else if(trgtcmp.up('tab-contacts')){
			tabpanel = trgtcmp.up('tab-contacts');
		}
		context = tabpanel.getActiveTab();
		if (grid.getSelectionModel().hasSelection()) {
			rec = grid.getSelectionModel().getSelection()[0];
		}
		////Delete Document
	  	var docid = rec.get('docid');
		grid.setLoading(true);
		var tmpdata = {
            "@metadata": {
                "checksum": "override"
            },
			'idDocument' : docid,
            'deleted'  : true
        };

		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
			},
			method:'PUT',
			params: Ext.util.JSON.encode(tmpdata),
			url:DM2.view.AppConstants.apiurl+'mssql:Document',
			scope:this,
			success: function(response, opts) {
				grid.setLoading(false);
				console.log("Deleted Document.");
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 200){
					me.refreshDocuments(grid);					
				} else {
					console.log("Deleted Document Failure.");
					Ext.Msg.alert('Deleted Document failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				grid.setLoading(false);
				console.log("Deleted Document Failure.");
				var obj = Ext.decode(response.responseText);
				Ext.Msg.alert('Deleted Document failed', obj.errorMessage);
			}
		});
	},
	
	refreshDocuments: function(grid){
		var me = this,context,tabpanel;
		if(grid.up('tab-deals')){
			tabpanel = grid.up('tab-deals');
		} else if(grid.up('tab-sales')){
			tabpanel = grid.up('tab-sales');
		} else if(grid.up('tab-properties')){
			tabpanel = grid.up('tab-properties');
		} else if(grid.up('tab-contacts')){
			tabpanel = grid.up('tab-contacts');
		}
		context = tabpanel.getActiveTab();
		if(context.getXType()=="tab-deal-detail" || context.getXType()=="tab-sales-detail"){
			var dealdocsgridpanel = context.down('dealdocsgridpanel');
			me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
		} else if(context.getXType()=="tab-deal-layout" || context.getXType()=="tab-sales-layout"){
			var dealdocsgridpanel = context.down('dealdocsgridpanel');
			me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
		} else if(grid.up('tab-property-layout')){
			context = grid.up('tab-property-layout');
			var propertygrid = context.down('tab-property-grid');
			if (propertygrid.getSelectionModel().hasSelection()) {
				var row = propertygrid.getSelectionModel().getSelection()[0];
				var propertyid = row.get('idPropertyMaster');
				me.getController("PropertyController").loadDocsPerProperty(propertyid,context.down('propertydocsgridpanel'));
			}
		} else if(grid.up('tab-contact-layout')){
			context = grid.up('tab-contact-layout');
			var contactgrid = context.down('tab-contact-grid');
			if(contactgrid.getSelectionModel().hasSelection()){
				row = contactgrid.getSelectionModel().getSelection()[0];
				var contactid = row.get('contactid');
				//contactgrid.fireEvent('select',contactgrid.getSelectionModel(),row);
				me.getController("ContactController").loadContactDocsStore(contactid,context.down('contactdocsgridpanel'));
			}			
		}
	},
	
	showGeneralEditDocPanel: function(cmp){
		var me = this,tabpanel, context, spawnFrom, grid,trgtcmp;
		
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
			grid = cmp.up('grid');// when cmp is a button.
			trgtcmp = cmp;
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		} else if(trgtcmp.up('tab-properties')){
			tabpanel = trgtcmp.up('tab-properties');
		} else if(trgtcmp.up('tab-contacts')){
			tabpanel = trgtcmp.up('tab-contacts');
		}
		context = tabpanel.getActiveTab();
		if (grid.getSelectionModel().hasSelection()) {
			rec = grid.getSelectionModel().getSelection()[0];
		}
		me.getController('DocController').displayGeneralEditDocPanel(context,rec);	
	},
	
	displayGeneralEditDocPanel: function(context,rec) {
        var me = this,eastregion,generaleditdocpanel;
		console.log(context.xtype);
		if(context.xtype == "tab-property-layout" || context.xtype == "tab-contact-layout" || context.xtype == "tab-sales-layout" || context.xtype == "tab-deal-layout"){
			eastregion = context.down('[region=east]');
	
			if(context.down('generaleditdocpanel')){
				generaleditdocpanel = context.down('generaleditdocpanel');
			} else {
				console.log("Create New east edit deal doc Panel");
				generaleditdocpanel = Ext.create('DM2.view.GeneralEditDocPanel');
				eastregion.add(generaleditdocpanel);
			}
			eastregion.setCollapsed(false);
			eastregion.setVisible(true);
			eastregion.getLayout().setActiveItem(generaleditdocpanel);			
			eastregion.setTitle(title);
			eastregion.getHeader().hide();			
			eastregion.config.backview = context;
        } else {			
			eastregion = context.down('[region=east]');
			if(context.down('generaleditdocpanel')){
				console.log("Use Old Panel");
				generaleditdocpanel = context.down('generaleditdocpanel');
			} else {
				//create new GeneralAddDealDoc Panel
				console.log("Create New Panel");
				generaleditdocpanel = Ext.create('DM2.view.GeneralEditDocPanel');
			}
			/*if(generaleditdocpanel.getHeader()){
				generaleditdocpanel.getHeader().show();
			}*/
			if(context.config.activityid){
				generaleditdocpanel.config.activityid = context.config.activityid;
			}
			eastregion.getLayout().setActiveItem(generaleditdocpanel);		
			eastregion.config.backview = context;
		}
        var title = "Edit Document";
        generaleditdocpanel.setTitle(title);

        var docstempst = generaleditdocpanel.down('grid').getStore();
        docstempst.removeAll();
        generaleditdocpanel.getForm().reset();
		
		var cls_st = generaleditdocpanel.down('combobox[name="clsfctn"]').getStore();
		me.getController('GeneralDocSelController').loadClassification(cls_st);
		docstempst.add(rec);
		generaleditdocpanel.getForm().loadRecord(rec);
    },
	
	generalEditDocPanelClose: function(panel){
		var me = this,eastregion;
		var activemenutab = panel.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		
		if(context.xtype == "tab-deal-layout"){
			eastregion = context.down('#deal-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        } else if(context.xtype == "tab-sales-layout"){
			eastregion = context.down('#sales-east-panel');
			eastregion.removeAll();
			//eastregion.setCollapsed(false);
			eastregion.setVisible(false);
        } else if(context.xtype == "tab-property-layout"){
			me.getController('PropertyController').closeRecvDealDetailDocs(context);
        } else if(context.xtype == "tab-contact-layout"){
			me.getController('ContactController').closeRecvDealDetailDocs(context);
        } else if(context.xtype == "tab-deal-detail" || context.xtype == "tab-sales-detail"){
			if(context.xtype == "tab-deal-detail"){
				eastregion = context.down('dealdetailsformpanel').down('[region=east]');
			} else if(context.xtype == "tab-sales-detail"){
				eastregion = context.down('salesdealdetailsformpanel').down('[region=east]');
			}
			//var eastregion = context.down('[region=east]');
			var backview = eastregion.config.backview;
			me.getController('DocController').closeRecvDealDetailDocs(context);   
		}
	},
	
	generalEditDocBtnSaveClick: function(btn){
		var me = this;
		var activemenutab = btn.up('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var generaleditdocpanel = btn.up('generaleditdocpanel');
        var generaleditdocForm = generaleditdocpanel.getForm();
        var docid = generaleditdocForm.findField('docid').getValue();
        console.log("docid"+ docid);
		
        var desc = generaleditdocForm.findField('desc').getValue();
        var clsfctn = generaleditdocForm.findField('clsfctn').getValue();
      
        var data = {
            'description': desc,
            'classification': clsfctn
        };
        
        if(docid===0 || docid===null){
            console.log("Add New Doc");
            var methodname = "POST";
        } else {
            console.log("Save Document");
            data['idDocument'] = docid;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";
        }

        generaleditdocpanel.setLoading(true);

        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Document',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                generaleditdocpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
                    if(context.getXType()=="tab-deal-detail" || context.getXType()=="tab-sales-detail"){						
						var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
						var dealdocsgridpanel = context.down('dealdocsgridpanel');
						me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
					} else if(context.getXType()=="tab-deal-layout" || context.getXType()=="tab-sales-layout"){
						var dealdocsgridpanel = context.down('dealdocsgridpanel');
						me.getController("DocController").loadDocsPanelStore(dealdocsgridpanel);
					} else if(context.getXType()=="tab-property-layout"){
						var propertygrid = context.down('tab-property-grid');
						if (propertygrid.getSelectionModel().hasSelection()) {
							var row = propertygrid.getSelectionModel().getSelection()[0];
							var propertyid = row.get('idPropertyMaster');
							me.getController("PropertyController").loadDocsPerProperty(propertyid,context.down('propertydocsgridpanel'));
						}
					} else if(context.getXType()=="tab-contact-layout"){						
						var contactgrid = context.down('tab-contact-grid');
						if(contactgrid.getSelectionModel().hasSelection()){
							row = contactgrid.getSelectionModel().getSelection()[0];
							var contactid = row.get('contactid');
							//contactgrid.fireEvent('select',contactgrid.getSelectionModel(),row);
							me.getController("ContactController").loadContactDocsStore(contactid,context.down('contactdocsgridpanel'));
						}
					}
					generaleditdocpanel.getForm().reset();
					generaleditdocpanel.close();
                } else {
                    console.log("Save Document Failure.");
                    Ext.Msg.alert('Save Document failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                generaleditdocpanel.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Document', obj.errorMessage);
            }
        });
	},
	
	downloadDocs: function(cmp){
		var me = this,grid;
		if(cmp.xtype == 'menuitem') {
		    grid = cmp.up().spawnFrom;
			trgtcmp = grid;
		} else {
			grid = cmp.up('grid');		    // when cmp is a button.
			trgtcmp = cmp;
		}
		if(trgtcmp.up('tab-deals')){
			tabpanel = trgtcmp.up('tab-deals');
		} else if(trgtcmp.up('tab-sales')){
			tabpanel = trgtcmp.up('tab-sales');
		} else if(trgtcmp.up('tab-properties')){
			tabpanel = trgtcmp.up('tab-properties');
		} else if(trgtcmp.up('tab-contacts')){
			tabpanel = trgtcmp.up('tab-contacts');
		}
		context = tabpanel.getActiveTab();
		if (grid.getSelectionModel().hasSelection()) {
			rec = grid.getSelectionModel().getSelection()[0];
		}
		////Delete Document
	  	var url = rec.get('url');
		var n = url.toLowerCase().indexOf("http");
		if(n!=-1){
			url = url.substr(n);
		    me.downloadFile(url); // UNCOMMENT THIS LINE TO MAKE IT WORK
		}
	},

	// Source: http://pixelscommander.com/en/javascript/javascript-file-download-ignore-content-type/
	downloadFile: function (sUrl) {
		console.log("Download Doc Url : "+sUrl);
		var downloadFileIsChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
		var downloadFileIsSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;
		//iOS devices do not support downloading. We have to inform user about this.
		if (/(iP)/g.test(navigator.userAgent)) {
		   //alert('Your device does not support files downloading. Please try again in desktop browser.');
		   window.open(sUrl, '_blank');
		   return false;
		}
	
		//If in Chrome or Safari - download via virtual link click
		if (downloadFileIsChrome || downloadFileIsSafari) {
			//Creating new link node.
			var link = document.createElement('a');
			link.href = sUrl;
			link.setAttribute('target','_blank');
	
			if (link.download !== undefined) {
				//Set HTML5 download attribute. This will prevent file from opening if supported.
				var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
				link.download = fileName;
			}
	
			//Dispatching click event.
			if (document.createEvent) {
				var e = document.createEvent('MouseEvents');
				e.initEvent('click', true, true);
				link.dispatchEvent(e);
				return true;
			}
		}
	
		// Force file download (whether supported by server).
		if (sUrl.indexOf('?') === -1) {
			sUrl += '?download';
		}
	
		window.open(sUrl, '_blank');
		return true;
	},
	
	getHTTP: function(apiserver){
		console.log("Apiserver : "+apiserver);
		var httpVal = "http://";
		var n = apiserver.toLowerCase().indexOf("http://");
		if(n==-1){
			var i = apiserver.toLowerCase().indexOf("https://");
			if(i!=-1){
				httpVal = "https://";
			}
		}
		console.log(httpVal);
		return httpVal;
	}
});