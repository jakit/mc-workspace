Ext.define('DM2.controller.DealHistoryController', {
    extend: 'Ext.app.Controller',

    refs: {
		dealhistorytab: {
            //autoCreate: true,
            selector: 'dealhistorytab',
            xtype: 'dealhistorytab'
        }
    },

    init: function(application) {
        this.control({
        });
    },
	
	clearDealHistory: function(tabItem){
		console.log("Clear Deal History");
		var me = this;
		var panel = tabItem.down('dealhistorygrid');
		var dealhistst = panel.getStore();
		dealhistst.removeAll();
	},
	
    onCategoryChange: function(newValue, menu, historyType) {
        var me = this,dealhistory_st,dealid,row,maingrid,viewname,historyGrid;
        var p = menu.up('tab-deal-detail');
		if(!p) {
			p = menu.up('tab-sales-detail');
		}
        var anchor = "detail";
        if(!p) {
            anchor = "grid";
            p = menu.up('tab-deal-layout');
			if(!p) {
				p = menu.up('tab-sales-layout');
			}
            historyGrid = p.down('dealhistorygrid');
            dealhistory_st = historyGrid.getStore();
        }
        if(anchor == "grid") {
            maingrid = p.down('tab-deal-grid');
			if(!maingrid) {
				maingrid = p.down('tab-sales-grid');
			}
			if(maingrid.getSelectionModel().hasSelection()){
            	row = maingrid.getSelectionModel().getSelection()[0];
            	//dealid = row.data.dealid;
				dealid = row.data.idDeal;
			} else {
				return false;
			}
        } else {
            dealid = p.dealid;
			historyGrid = p.down('dealhistorygrid');
            dealhistory_st = historyGrid.getStore();
            //dealhistory_st = Ext.getStore('storedealhistory:' + dealid);
        }
        
		console.log(newValue);
		var filterstarr = [];
		
		if(newValue=="Addressess"){
			viewname = "address";
		} else if(newValue=="Streets"){
			viewname = "street";
		} else if(newValue=="Contacts"){
			viewname = "contact";
		}
		console.log("viewname"+viewname);
		var filterst = "reqtype="+viewname;
        filterstarr.push(filterst);
		var filterst1 = "dealid="+dealid;
        filterstarr.push(filterst1);
		
        var mainViewport = Ext.ComponentQuery.query('mainviewport')[0];
		
        var url;
	    if(historyType=="deal"){
			url = DM2.view.AppConstants.apiurl+'DealHistoryByDealID';
		} else if(historyType=="sale") {
			url = DM2.view.AppConstants.apiurl+'SaleHistoryByDealID';
		}
		
		var dealhistorystproxy = dealhistory_st.getProxy();
        /*dealhistorystproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });*/
		var dealhistorystproxyCfg = dealhistorystproxy.config;
        dealhistorystproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+ mainViewport.config.apikey + ':1'
        };
		dealhistorystproxyCfg.originalUrl = url;
		dealhistorystproxyCfg.url = url;
		dealhistorystproxyCfg.extraParams = {
            filter :filterstarr
        };
		dealhistory_st.setProxy(dealhistorystproxyCfg);
		
		/*
		dealhistory_st.getProxy().setUrl(url);
        dealhistory_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+ mainViewport.config.apikey + ':1'
        });*/
		dealhistory_st.load();
        /*dealhistory_st.load({
            /*params:{
                //filter :'dealid = '+dealid
				'arg.dealid' : dealid
            }
        });*/
    }
});