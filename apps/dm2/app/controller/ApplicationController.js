Ext.define('DM2.controller.ApplicationController', {
    extend: 'Ext.app.Controller',

    ApplicationClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this;
        var senddoccntrl = me.getController('SendDocumentController');
        senddoccntrl.showSendDocumentView("application",tabdealdetail,activitytext);
    },

    loadDealSelectedLoans: function(dealid,context,viewname) {
        var me = this;
		var sendloiloanfrm = context.down('senddocumentpanel').down('#sendloiloanfrm')
        var dealquoteselected_st = sendloiloanfrm.getForm().findField('loanid').getStore();//Ext.getStore('DealQuotesSelected');
        dealquoteselected_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });

        var filterstarr = [];
        filterstarr.push('dealid = '+dealid);
        filterstarr.push("selected='S'");

        dealquoteselected_st.load({
            params:{
                filter : filterstarr
            },
            callback: function(records, operation, success){
                var quotecnt = dealquoteselected_st.getCount();
                console.log("Quotes Cnt"+quotecnt);

                if(quotecnt > 0){
                    console.log("select rec");
					var loanrec;
					if(viewname=="sendmode"){
						var approvalpanel = context.down('dealdetailsformpanel').down('approvalpanel');				
						var apprrec = approvalpanel.config.approvalRec;
						dealquoteselected_st.each( function(record){
							console.log (record);
							if(record.get('loanid')==apprrec.get('loanid')){
								loanrec = record;
							}
						});
					}
					else
					{
                    	loanrec = dealquoteselected_st.getAt(0);//.get('contactid');
					}
                    var loancombo = sendloiloanfrm.down('#loancombo');
                    loancombo.select(loanrec);

                    loancombo.fireEvent('select',loancombo,loanrec);
                }
            }
        });
    }

});
