Ext.define('DM2.controller.SendLOIController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({
        });
    },

    SendLOIClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this;
		if(activitytext==="Send Contract To Buyer"){
            me.getController('SendDocumentController').showSendDocumentView("SnCntToB",tabdealdetail,activitytext);
        } else if(activitytext==="Send Brokerage Contract To Seller"){
            me.getController('SendDocumentController').showSendDocumentView("SnBrCntToS",tabdealdetail,activitytext);
        } else if(activitytext==="Send Brokerage Contract To Buyer"){
            me.getController('SendDocumentController').showSendDocumentView("SnBrCntToB",tabdealdetail,activitytext);
        } else if(activitytext==="Send Contract To Seller"){
            me.getController('SendDocumentController').showSendDocumentView("SnCntToS",tabdealdetail,activitytext);
        } else if(activitytext==="Modification"){
            me.getController('SendDocumentController').showSendDocumentView("ModiFN",tabdealdetail,activitytext);
        } else {
	        me.getController('SendDocumentController').showSendDocumentView("sendloi",tabdealdetail,activitytext);
		}
    }
});