Ext.define('DM2.controller.DealModifyController', {
    extend: 'Ext.app.Controller',

    refs: {
        dealmodifyview: {
            autoCreate: true,
            selector: 'dealmodifyview',
            xtype: 'dealmodifyview'
        }
    },

    init: function(application) {
        this.control({
            'dealmodifyview':{
                close: this.dealModifyViewClose
            },
            'dealmodifyview #savebtn':{
                click: this.onDealModifySaveClick
            },
            'dealmodifyview #resetbtn':{
                click: this.onDealModifyResetClick
            },
			'dealmodifyview combobox[name="office"]':{
                change:this.onFieldsChange
            },
            'dealmodifyview combobox[name="status"]':{
                change:this.onFieldsChange
            },
            'dealmodifyview textfield[name="dealname"]':{
                change:this.onFieldsChange
            },
			'dealmodifyview datefield':{
                change:this.onFieldsChange
            }
        });
    },
	
	getFormatedDateValue: function(dateval){
		var me = this,sadt;	
		if(dateval!=null){
			var dtarr = dateval.split('T');
			var newDate = Ext.Date.parse(dtarr[0], "Y-m-d");
			sadt = Ext.Date.format(newDate, "m/d/Y");
		} else {
			sadt = null;
		}
		return sadt;
	},
	
    showDealModifyView: function() {
        console.log("Show Deal Status Change View");
        var me = this,dealmodifyview;
		var activemenutab = me.getController('MainController').getMainviewport().down('tab-panel').getLayout().getActiveItem();
		var context = activemenutab.getActiveTab();
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('dealmodifyview')){
			console.log("Use Old Panel");
			dealmodifyview = detailpanel.down('dealmodifyview');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	dealmodifyview = Ext.create('DM2.view.DealModifyView');//me.getDealmodifyview();
		}
        eastregion.getLayout().setActiveItem(dealmodifyview);
		
		var record = context.record;
        var dealid = record.data.idDeal;
		
        dealmodifyview.setTitle("Deal Editor > "+dealid);
        //dealmodifyview.down('#activityid').setValue(activityid);
		
        var dealdetailrec = context.down('#dealdetailuppersection').config.dealdetailrec;
		console.log("dealdetailrec");
		console.log(dealdetailrec);
        var dealmodifyform = dealmodifyview.getForm();
		dealmodifyform.loadRecord(dealdetailrec);
        dealmodifyview.down('#savebtn').disable();
        dealmodifyview.down('#resetbtn').disable();
		
		console.log(dealdetailrec.get('scheduledCloseDate'));
		var scheduledCloseDate = me.getFormatedDateValue(dealdetailrec.get('scheduledCloseDate'));
		dealmodifyform.findField('scheduledCloseDate').setRawValue(scheduledCloseDate);
		
		var closeDate = me.getFormatedDateValue(dealdetailrec.get('closeDate'));
		dealmodifyform.findField('closeDate').setRawValue(closeDate);
		
		var billDate = me.getFormatedDateValue(dealdetailrec.get('billDate'));
		dealmodifyform.findField('billDate').setRawValue(billDate);
		
		var endDate = me.getFormatedDateValue(dealdetailrec.get('endDate'));
		dealmodifyform.findField('endDate').setRawValue(endDate);
		
		var office_st = dealmodifyform.findField('office').getStore();
		if(office_st.isLoaded()){
			dealmodifyform.findField('office').setValue(dealdetailrec.get('office'));
		} else {
			office_st.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			office_st.load({
				callback: function(records, operation, success) {
					dealmodifyform.findField('office').setValue(dealdetailrec.get('office'));
				}
			});
		}
		if(context.xtype == "tab-deal-detail"){
			dealmodifyform.findField('mtgtype').show();
			var dealstatus = Ext.getStore('DealStatus');
			dealmodifyform.findField('status').bindStore(dealstatus);
		} else if(context.xtype == "tab-sales-detail"){
			dealmodifyform.findField('mtgtype').hide();
			var saledealstatus = Ext.getStore('SaleDealStatus');
			dealmodifyform.findField('status').bindStore(saledealstatus);
		}
    },

    dealModifyViewClose: function(panel) {
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    onDealModifySaveClick: function(btn) {
        console.log("Deal Modify Save");
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var dealmodifyview = detailpanel.down('dealmodifyview');
		
        var dealid   = dealmodifyview.getForm().findField('dealid').getValue();
        var dealname = dealmodifyview.getForm().findField('dealname').getValue();
        var status   = dealmodifyview.getForm().findField('status').getValue();
		var office   = dealmodifyview.getForm().findField('office').getValue();
		if(office!=null){
			office   = office.toString();
		}
		var scheduledCloseDate = dealmodifyview.getForm().findField('scheduledCloseDate').getValue();
		var closeDate   = dealmodifyview.getForm().findField('closeDate').getValue();
		var billDate    = dealmodifyview.getForm().findField('billDate').getValue();
		var endDate     = dealmodifyview.getForm().findField('endDate').getValue();
		
        dealmodifyview.setLoading(true);
        var data = {
            "@metadata": {
                "checksum": "override"
            },
            'idDeal'   : dealid,
            'dealName' : dealname,
            'status'   : status,
			'office'   : office,
			'scheduledCloseDate' : scheduledCloseDate,
            'closeDate' : closeDate,
			'billDate'  : billDate,
			'endDate'   : endDate
        };

        ///Do Save
        var methodname = "PUT";

        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'mssql:Deal',
            scope:this,
            method:methodname,
            params: Ext.util.JSON.encode(data),
            success: function(response, opts) {
                dealmodifyview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201) {
                    console.log("Deal is Modified.Now Refresh.");
                    //Refresh & Load Details;
                    me.dealModifyViewClose(dealmodifyview); //Close Modify deal view
					if(context.xtype=="tab-sales-detail"){
			        	//me.getController('SalesController').loadAllSales([]);
						me.getController('SalesDealDetailController').refreshSaleDealDetailData(context);
					} else {
						me.getController('DealDetailController').refreshDealDetailData(context);
						me.getController('DealStatusHistoryController').loadDealStatusHistory(context.down('dealdetailstatushistorypanel'));
					}
                } else {
                    console.log("Modify Deal Failed.");
                    Ext.Msg.alert('Modify Deal Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                dealmodifyview.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Modify Deal', obj.errorMessage);
            }
        });
    },

    onDealModifyResetClick: function(btn) {
 		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var dealmodifyview = detailpanel.down('dealmodifyview');
		
        var dealdetailrec = context.down('#dealdetailuppersection').config.dealdetailrec;

        var dealmodifyform = dealmodifyview.getForm();
		dealmodifyform.loadRecord(dealdetailrec);
		
		var scheduledCloseDate = me.getFormatedDateValue(dealdetailrec.get('scheduledCloseDate'));
		dealmodifyform.findField('scheduledCloseDate').setRawValue(scheduledCloseDate);
		
		var closeDate = me.getFormatedDateValue(dealdetailrec.get('closeDate'));
		dealmodifyform.findField('closeDate').setRawValue(closeDate);
		
		var billDate = me.getFormatedDateValue(dealdetailrec.get('billDate'));
		dealmodifyform.findField('billDate').setRawValue(billDate);
		
		var endDate = me.getFormatedDateValue(dealdetailrec.get('endDate'));
		dealmodifyform.findField('endDate').setRawValue(endDate);
		
        dealmodifyview.down('#savebtn').disable();
        dealmodifyview.down('#resetbtn').disable();
    },

    loadModifiedRecord: function(dealid,context) {
        var me = this;
		var dealdetailform = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        dealdetailform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DM2.view.AppConstants.apiurl+'v_rdeals',
            scope:this,
            method:'GET',
            params: {
                filter:'idDeal='+dealid
            },
            success: function(response, opts) {
                dealdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(response.status == 200 || obj.statusCode == 200 || obj.statusCode == 201){
					if(obj.length>0) {
						console.log("Load Modified Deal Record.");
						//Refresh & Load Details;
						var tmprec = Ext.create("DM2.model.Deal",obj[0]);
						//******** Update Deal Grid Record ***********/
						var tabdealgrid;
						if(context.up('tab-deals')){
							tabdealgrid = me.getController('MainController').getMainviewport().down('tab-deals').down('tab-deal-grid');	
						} else if(context.up('tab-sales')){
							tabdealgrid = me.getController('MainController').getMainviewport().down('tab-sales').down('tab-sales-grid');	
						}
						///////////////////////////////////////////////
	
						context.record = tmprec;
						//Show Deal Detail Record
						if(context.up('tab-deals')){
							me.getController('DealController').showDealDetail(tmprec, tabdealgrid);
						} else if(context.up('tab-sales')){
							me.getController('SalesController').showDealDetail(tmprec, tabdealgrid);	
						}						
					}
					
					var dealdetailuppersection = context.down('dealdetailuppersection');
					me.getController('DealDetailController').loadDealDetails(dealdetailuppersection);
					me.getController('DealDetailController').loadActivityDetails(context.down('defaultworkareapanel'));
					
					me.getController('DealStatusHistoryController').loadDealStatusHistory(context.down('dealdetailstatushistorypanel'));
					//context.down('dealdetailstatushistorypanel').getStore().load(); //Load Status History
                } else {
                    console.log("Load Modified Deal Record Failed.");
                    Ext.Msg.alert('Modify Deal Failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                dealdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Load Modified Deal', obj.errorMessage);
            }
        });
    },

    onFieldsChange: function(field, newValue, oldValue, eOpts) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(field);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		
        var dealmodifyview = detailpanel.down('dealmodifyview');
        dealmodifyview.down('#savebtn').enable();
        dealmodifyview.down('#resetbtn').enable();
    },
	
	loadOfficeList: function(){
		var me = this;
		var  office_st = Ext.getStore('Office');
		office_st.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
		});
		office_st.load({
			callback: function(records, operation, success) {
			}
		});
	}
});