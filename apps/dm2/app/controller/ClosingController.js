Ext.define('DM2.controller.ClosingController', {
    extend: 'Ext.app.Controller',

    refs: {
        closingview: {
            autoCreate: true,
            selector: 'closingview',
            xtype: 'closingview'
        }
    },

    init: function(application) {
        this.control({
            'closingview':{
                close: this.closingViewCloseClick,
				afterrender: 'loadClosingViewDetails'
            },
            'closingview #loancombo':{
                select: this.onChangeLoans
            },
            'closingview #cvpropertycombo':{
                select: this.onPropertyChange
            },
			'closingview button[action="doneActionBtn"]': {
                click: 'onClosingViewDoneBtnClick'
            }
        });
    },
	
	ClosingClick: function(activityid, activitytext, h_fname,tabdealdetail) {
        var me = this,closingview;
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(tabdealdetail);
		var eastregion = detailpanel.down('[region=east]');
		if(detailpanel.down('closingview')){
			console.log("Use Old Panel");
			closingview = detailpanel.down('closingview');
		} else {
			//create new Activity Detail Panel
			console.log("Create New Panel");
        	closingview = Ext.create('DM2.view.Closingview');
		}
        eastregion.getLayout().setActiveItem(closingview);
		closingview.setTitle("Activities > "+activitytext);
        //closingview.down('#activityid').setValue(activityid);
    },
	
	loadClosingViewDetails: function(panel){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var loancombo = panel.down('#loancombo');
		var existingLoans_st = loancombo.getStore();//('ExistingLoans');
		if(existingLoans_st.isLoaded()){
			var existingLoanscnt = existingLoans_st.getCount();
			if(existingLoanscnt>0){
				//Get the First Record
				var loanrec = existingLoans_st.getAt(0);           
				loancombo.select(loanrec);
				loancombo.fireEvent('select',loancombo,loanrec);
			}
		} else {	
			existingLoans_st.getProxy().setHeaders({
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			});
			existingLoans_st.load({
				params:{
					filter :'dealid = '+dealid
				},
				callback: function(records, operation, success){        
					var existingLoanscnt = existingLoans_st.getCount();
					if(existingLoanscnt>0){
						//Get the First Record
						var loanrec = existingLoans_st.getAt(0);           
						loancombo.select(loanrec);
						loancombo.fireEvent('select',loancombo,loanrec);
					}
				}
			});
		}
		
		var cvpropertycombo = panel.down('#propertyfrm').down('#cvpropertycombo');
		var dealprptyst = cvpropertycombo.getStore();        
        dealprptyst.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealprptyst.load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
				var prpcnt = dealprptyst.getCount();
				if(prpcnt>0){
					//Get the First Record
					var prprec = dealprptyst.getAt(0);           
					cvpropertycombo.select(prprec);
					cvpropertycombo.fireEvent('select',cvpropertycombo,prprec);
				}
            }
        });
        //me.loadClosingActions();
	},
	
    closingViewCloseClick: function(panel) {
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(panel);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);
        var eastregion = detailpanel.down('[region=east]');
        eastregion.getLayout().setActiveItem(context.down('defaultworkareapanel'));
    },

    onChangeLoans: function(combo, rec, eOpts) {
        var me = this;
        console.log("Fire select event");
		var context = me.getController('DealDetailController').getTabContext(combo);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);		
        var loandtfrm = detailpanel.down('closingview').down('#closingloanfrm');
        loandtfrm.getForm().loadRecord(rec);
    },

    onPropertyChange: function(combo, rec, eOpts) {
        var me = this;
		var context = me.getController('DealDetailController').getTabContext(combo);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);	
        console.log("Fire select event");
        var propertyfrm = detailpanel.down('closingview').down('#propertyfrm');
        propertyfrm.getForm().loadRecord(rec);
    },
	
	onClosingViewDoneBtnClick: function(btn){
		var me = this;
		var context = me.getController('DealDetailController').getTabContext(btn);
		var detailpanel = me.getController('DealDetailController').getDealSaleDetailPanel(context);	
		var record = context.record;
        var dealid = record.data.idDeal;
		
		var eastregion = detailpanel.down('[region=east]');
        var actItem = eastregion.getLayout().getActiveItem();
		me.getController('DealActivityController').postActivityHistory(dealid,actItem);
	}
});