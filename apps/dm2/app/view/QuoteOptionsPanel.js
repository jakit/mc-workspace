Ext.define('DM2.view.QuoteOptionsPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.quoteoptionspanel',

    requires: [
		'Ext.grid.Panel',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.data.proxy.Rest',
		'DM2.view.QuoteOptionFormPanel'
    ],
    
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
       // me.cls = 'gridcommoncls';
        me.title = 'Options';
        //me.maxHeight = 400;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.collapsed  = true;
		me.frame = true;
		me.margin = '0 0 5 0';
        var quoteoptionst = Ext.create('DM2.store.QuoteOptions');
		me.layout = {
			type:'vbox',
			align:'stretch'
		};
		/*me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					itemId: 'addquoteoptionbtn',
					iconCls: 'add',
					text: 'Add'
				}
			]
		};*/
		me.items = [{
        	xtype:'gridpanel',
			maxHeight : 169,
			itemId:'optionsgridpanel',
	        cls : 'gridcommoncls',
			columns : [{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'idLoan',
				text: 'Loan Id',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				//hidden:true,
				dataIndex: 'quoteRate',
				text: 'Rate',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				//hidden:true,
				dataIndex: 'indexSpread',
				text: 'Spread',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				//hidden:true,
				dataIndex: 'percentSpread',
				text: 'PercentSpread',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'term',
				text: 'Term',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'amortizationMonth',
				text: 'Amort',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'pppDescription',
				text: 'PPP',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'optionFee',
				text: 'Fee%',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'optionFloor',
				text: 'Floor',
				flex: 1
			},
			{
				xtype: 'numbercolumn',
				hidden:true,
				dataIndex: 'optAmount',
				text: 'Option Amount',
				flex: 1,
				format: '0,000',
				//align:'right',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {             
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				}
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'rateType',
				text: 'Rate Type',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'interestOnly',
				text: 'Interest Only',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				format: 'm-d-Y',
				dataIndex: 'startDate',
				text: 'Start Date',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				dataIndex: 'monthEnd',
				text: 'Month End',
				flex: 1
			}],
			store:quoteoptionst,
			scrollable : true,
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				items: [{
					xtype: 'button',
					itemId: 'addquoteoptionbtn',
					iconCls: 'add',
					text: 'Add'
				}]
			}]
		},{
			xtype:'quoteoptionformpanel'
		}];

        me.callParent(arguments);
    }
});