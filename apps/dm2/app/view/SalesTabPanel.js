Ext.define('DM2.view.SalesTabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.salestabpanel',
    requires: [
        'Ext.grid.Panel',
        'Ext.tab.Tab',
        'Ext.tab.Bar'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    contactsStore: null,
    initComponent: function() {
        var me = this;
        me.activeTab = 0;
		me.ui = 'submenutabpanel';
        me.items = [];

        me.callParent(arguments);        
    },
    tabBar: {
        xtype: 'tabbar',
        ui: 'submenutabpanel'
    }

});