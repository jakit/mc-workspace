Ext.define('DM2.view.CreateNewDealPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.createnewdealpanel',

    requires: [
        'DM2.view.CreateNewDealDocsGridPanel',
        'DM2.view.CreateNewDealUserGridPanel',
		'DM2.view.CreateNewDealContactGrid',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.view.Table',
        'Ext.selection.CheckboxModel',
        'Ext.grid.column.Check',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
		'DM2.store.DealStatus'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable : true,
    initComponent: function() {
		var me = this;
    	me.frame = true;
	    me.margin = 3;
	    //me.scrollable = true;
	    me.ui = 'activitypanel';
	    me.bodyPadding = 5;
	    me.closable = true;
		
		me.contextMenu = Ext.create('DM2.view.CreateNewDealPropertyGridMenu');
		
		var officest = Ext.create('DM2.store.Office');
		var dealStatusSt = Ext.create('DM2.store.DealStatus');
		var dealPropertySt = Ext.create('DM2.store.Dealproperty');
		var dealDetailContactsSt = Ext.create('DM2.store.DealDetailContacts');
		//var dealDocSt = Ext.create('DM2.store.Documents');
		
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
	    me.items = [
        /*{
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textfield',
                    flex: 1,
                    itemId: 'filenumber',
                    fieldLabel: 'File #',
                    labelAlign: 'right',
                    labelWidth: 40,
                    name: 'filenumber'
                },
                {
                    xtype: 'combobox',
                    flex: 1,
					queryMode:'local',
                    itemId: 'status',
                    fieldLabel: 'Status',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'status',
                    displayField: 'status',
                    store: dealStatusSt,
                    valueField: 'status'
                }
            ]
        },*/
		{
			xtype: 'textfield',
			//flex: 1,
			itemId: 'name',
			fieldLabel: 'Name',
			labelAlign: 'right',
			labelWidth: 50,
			name: 'name'
		},
        {
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
				{
					xtype: 'combobox',
					flex: 1,
					queryMode:'local',
					itemId: 'status',
					fieldLabel: 'Status',
					labelAlign: 'right',
					labelWidth: 50,
					name: 'status',
					displayField: 'statusFullName',
					store: dealStatusSt,
					valueField: 'status'
				},           
                {
                    xtype: 'combobox',
                    flex: 1,
					queryMode:'local',
                    itemId: 'office',
                    fieldLabel: 'Office',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'office',
                    displayField: 'code',
                    store: officest,
                    valueField: 'code'
                }
            ]
        },
		/*{
			xtype: 'datefield',
			fieldLabel: 'Status Date',
			labelAlign: 'right',
			labelWidth: 90,
			name: 'asof',
			//hidden: true
		},*/
        {
            xtype: 'gridpanel',
            cls: 'gridcommoncls',
            frame: true,
            height: 170,
            itemId: 'createnewdealpropertygrid',
            maxHeight: 175,
            minHeight: 100,
            scrollable: true,
            ui: 'activitypanel',
            collapsible: true,
            title: 'Properties',
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: dealPropertySt,
			plugins : [
				{
					ptype: 'cellediting',
					clicksToEdit: 1/*,
					listeners: {
						beforeedit: 'onCellEditingBeforeEdit'
					}*/
				}
			],
            columns: [
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    width: 70,
                    dataIndex: 'street_no',
                    text: 'No.'
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'street',
                    text: 'Street',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'city',
                    text: 'City',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    width: 60,
                    dataIndex: 'state',
                    text: 'St.'
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    hidden: true,
                    dataIndex: 'buildingClass',
                    text: 'Bldg Cls'
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    hidden: true,
                    dataIndex: 'propertyType',
                    text: 'Property Type'
                },
				{
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'mortgageType',
                    text: 'Mortgage Type',
					editor: {
						xtype: 'combobox',
						itemId:'mortgageTypeProp',
						displayField: 'selectionDesc',
						queryMode: 'local',
						store: 'MortgageTypeList',
						valueField: 'selectionDesc'
					}
                },
                {
                    xtype: 'checkcolumn',
					itemId:'primaryProperty',
                    dataIndex: 'primaryProperty',
                    text: 'IsPrimary',
                    flex: 1
                }
            ],
            selModel: {
                selType: 'checkboxmodel'
            },
			dockedItems : [{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						action: 'showaddnewdealpropertybtn',
						iconCls: 'add',
						text: 'Add Property'
					}
				]
			}]
        },
		{
            xtype: 'createnewdealcontactgrid'
        },
        {
            xtype: 'createnewdealdocsgridpanel'
        },
        {
            xtype: 'createnewdealusergridpanel'
        }
    ];
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'createnewdealbtn',
					itemId: 'createnewdealbtn',
					text: 'Create'
				}
			]
		};
		me.callParent(arguments);
    }
});