Ext.define('DM2.view.MainViewport', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.mainviewport',
    requires: [
        'DM2.view.DashboardContainer',
        'DM2.view.component.tab.Panel'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'viewportcls';
        me.layout = {
            type: 'border'
        };
        me.items = [/*{
            xtype: 'dashboardcontainer',
            height: 60,
            region: 'north'
        }, {
            xtype: 'tab-panel',
            region: 'center'
        }*/];
        
        me.callParent(arguments);
    }

});