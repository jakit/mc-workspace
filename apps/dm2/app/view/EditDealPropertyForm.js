Ext.define('DM2.view.EditDealPropertyForm', {
    extend : 'Ext.form.Panel',
    alias : 'widget.editdealpropertyform',
    requires : [
        'Ext.button.Button'
    ],
    constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
        me.frame = true;
        me.itemId = 'editdealpropertyform';
        me.margin = 3;
        me.ui = 'activitypanel';
        me.closable = true;
        me.title = 'Edit Property';
        me.layout = {
			type:'vbox',
			align:'stretch'
		};
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'savedealpropertybtn',
					itemId: 'savedealpropertybtn',
					text: 'Save'
				}
			]
		};
		
        me.items = [{
			xtype: 'panel',
			//split: true,
			//collapsible: true,
            //scrollable: true,
			bodyPadding: 5,
			header:false,
			layout: {
				type: 'column'
			},
			items: [{
				xtype: 'panel',
				columnWidth: 0.65,
				margin: "0px 10px 0 0px",
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [/*{
					xtype: 'panel',
					margin: '0px 0px 6px 0px',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'label',
						width: 56,
						text: 'Address:'
					}, {
						xtype: 'textfield',
						margin: '0 10 0 0',						
						width: 40,
						hideLabel: true,
						name: 'StreetNumber',
						itemId: 'street_no',
						readOnly: true
					}, {
						xtype: 'textfield',
						flex: 1,						
						hideLabel: true,
						name: 'StreetName',
						itemId: 'street',
						readOnly: true
					}]
				}, {
					xtype: 'panel',
					margin: '0px 0px 6px 0px',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Township',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'Township',
						itemId: 'city',
						readOnly: true
					}, {
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'State',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'State',
						itemId: 'state',
						readOnly: true
					}]
				}, {
					xtype: 'panel',
					margin: '0px 0px 6px 0px',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Boro',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'Boro',
						itemId: 'Boro',
						readOnly: true
					}, {
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Block',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'Block',
						itemId: 'block',
						readOnly: true
					},{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Units',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'Units',
						itemId: 'units',
						readOnly: true
					}]
				}, {
					xtype: 'panel',
					margin: '0px 0px 6px 0px',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'APN',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'APN',
						itemId: 'APN',
						readOnly: true
					},{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Lot',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'Lot',
						itemId: 'Lot',
						readOnly: true
					}]
				},*/{
					xtype: 'combobox',
					selectOnFocus: true,
					displayField: 'selectionDesc',
					queryMode: 'local',
					store: 'MortgageTypeList',//ratetypelist_st,
					typeAhead: true,
					valueField: 'selectionDesc',                 
					fieldLabel: 'Mortgage Type',
					labelAlign: 'right',
					labelWidth: 95,
					name: 'mortgageType'
				},
				{
					xtype: 'numberfield',
					hidden:true,
					fieldLabel: 'idSetup',
					labelAlign: 'right',
					labelWidth: 50,
					name: 'idSetup'
				}]
			}/*, {
				xtype: 'panel',
				columnWidth: 0.35,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'textfield',					
					fieldLabel: 'Prop type',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'propertyType',
					itemId: 'propertytype',
					readOnly: true
				}, {
					xtype: 'textfield',					
					fieldLabel: 'Zip',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'ZipCode',
					itemId: 'zip',
					readOnly: true
				}, {
					xtype: 'textfield',
					flex: 1,					
					fieldLabel: 'Bldg Cls',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'BuildingClass',
					itemId: 'buildingClass',
					readOnly: true
				}, {
					xtype: 'textfield',					
					fieldLabel: 'Stories',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'Stories',
					itemId: 'stories',
					readOnly: true
				}]
			}*/]
		}];
        me.callParent(arguments);
    }
}); 