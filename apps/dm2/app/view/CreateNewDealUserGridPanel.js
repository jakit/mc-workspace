Ext.define('DM2.view.CreateNewDealUserGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.createnewdealusergridpanel',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Date',
        'Ext.selection.CheckboxModel'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.height = 170;
		me.itemId = 'createnewdealusergridpanel';
		me.margin = '5 0 0 0';
		me.scrollable = true;
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.title = 'Users';
	
		me.store = Ext.create('DM2.store.DealActiveUsers');
		
		me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'user',
				text: 'USER',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'role',
				text: 'ROLE',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'assigner',
				text: 'Assigner',
				flex: 1
			},
			{
				xtype: 'datecolumn',
				hidden:true,
				dataIndex: 'assigndate',
				text: 'Assigndate',
				flex: 1,
				format: 'm-d-Y'
			}
		];
		me.selModel = {
			selType: 'checkboxmodel'
		};
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						action: 'showaddnewdealuserbtn',
						iconCls: 'add',
						text: 'Add User'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});