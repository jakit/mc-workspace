Ext.define('DM2.view.PropertyMasterGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.propertymastergridmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
            text: 'Add New Property',
			action:'addnewproperty',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            text: 'Edit Property',
			action:'editproperty',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
			action:'createnewdeal',
            itemId: 'createnewdeal',
            text: 'Create New Deal',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
			action:'createnewsaledeal',
            itemId: 'createnewsaledeal',
            text: 'Create New Sale',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			modifyMenu : 'no',
            text: 'Research',
            focusable: true,
            menu: {
                xtype: 'menu',
                itemId: 'researchmenu',
                width: 120,
                items: [
                    {
                        xtype: 'menuitem',
                        text: 'Gmap',
                        focusable: true
                    },
                    {
                        xtype: 'menuitem',
                        text: 'Zillow',
                        focusable: true
                    },
                    /*{
                        xtype: 'menuitem',
                        text: 'Trulia',
                        focusable: true
                    },*/
                    {
                        xtype: 'menuitem',
                        text: 'Reonomy',
                        focusable: true
                    },
					{
                        xtype: 'menuitem',
                        text: 'Oasis',
                        focusable: true
                    }
                ]
            }
        },
		/*{
            xtype: 'menuitem',
            text: 'View Master Data',
            focusable: true,
			modifyMenu : 'no'
        },*/
		{
            xtype: 'menuitem',
			action:'listacrisdocuments',
            text: 'List ACRIS Documents',
            focusable: true,
			modifyMenu : 'no'
        }];
        me.callParent(arguments);
    }
});