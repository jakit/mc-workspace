Ext.define('DM2.view.DashboardContainer', {
    extend : 'Ext.toolbar.Toolbar',
    alias : 'widget.dashboardcontainer',
    requires : [
        'Ext.Img', 
        'Ext.button.Split', 
        'Ext.menu.Menu', 
        'Ext.menu.Item', 
        'Ext.form.field.Text', 
        'Ext.toolbar.Fill', 
        'Ext.toolbar.TextItem'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
       //  me.style = 'background-color:#ffffff;';
        me.ui = 'menutoolbarui';
        me.items = [];        
        
        me.callParent(arguments);
    }

}); 