Ext.define('DM2.view.BankChooserPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.bankchooserpanel',

    requires: [
        'DM2.view.SubmissionContactsGridPanel',
        'DM2.view.BankDocsGridPanel',
        'DM2.view.GenFilesPerSubmissionGridPanel',
        'Ext.grid.Panel',
        'Ext.grid.filters.filter.Number',
        'Ext.grid.filters.filter.String',
        'Ext.view.Table',
        'Ext.grid.column.Date',
        'Ext.grid.filters.Filters',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.Date',
        'Ext.form.field.Number'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
    	me.frame = true;
	    me.itemId = 'bankchooserpanel';
	    me.margin = 3;
	    me.ui = 'activitypanel';
	    me.layout = 'border';
	    me.closable = true;
		
		var BanksSelectedSt = Ext.create('DM2.store.BanksSelected');
		me.items = [
			{
				xtype: 'gridpanel',
				region: 'north',
				split: true,
				cls: 'gridcommoncls',
				itemId: 'bcselbankgridpanel',
				maxHeight: 200,
				collapsible: true,
				header: false,
				sortableColumns: false,
				store: BanksSelectedSt,
				columns: [
					{
						xtype: 'gridcolumn',
						hidden: true,
						dataIndex: 'idBank',
						text: 'Bank Id',
						filter: {
							type: 'number'
						}
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'shortName',
						text: 'Short Name',
						flex: 1,
						filter: {
							type: 'string'
						}
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'fullName',
						text: 'Full Name',
						flex: 1,
						filter: {
							type: 'string'
						}
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'submitdate',
						text: 'Sent Date',
						flex: 1,
						format: 'm-d-Y'
					},
					{
						xtype: 'checkcolumn',
						itemId: 'isSubmitted',
						dataIndex: 'isSubmitted',
						text: 'Submitted',
						processEvent: Ext.emptyFn
					}
				],
				plugins: [
					{
						ptype: 'gridfilters'
					}
				],
				dockedItems: [
					{
						xtype: 'toolbar',
						dock: 'top',
						itemId: 'submissiongridtoolbar',
						items: [
							{
								xtype: 'button',
								itemId: 'selbankbtn',
								iconCls: 'add',
								text: 'Add Submission'
							},
							{
								xtype: 'button',
								itemId: 'glbsbmbtn',
								text: 'Global Submission'
							},
							{
								xtype: 'button',
								hidden: true,
								itemId: 'bankseldone',
								text: 'Done'
							},
							{
								xtype: 'checkboxfield',
								boxLabel  : 'Show All',
								name      : 'showall'
							}
						]
					}
				]
			},
			{
				xtype: 'panel',
				region: 'center',
				split: true,
				itemId: 'bankselbtmpanel',
				layout: 'card',
				collapsible: true,
				header: false,
				items: [
					{
						xtype: 'form',
						itemId: 'bankdocspanel',
						scrollable: true,
						bodyPadding: 5,
						header: {
							titlePosition: 0,
							items: [
								{
									xtype: 'button',
									disabled:true,
									action: 'saveSubmissionBtn',
									margin: '0 10 0 0',
									itemId: 'saveSubmissionBtn',
									text: 'Save'
								},
								{
									xtype: 'button',
									disabled:true,
									action: 'resetSubmissionBtn',
									itemId: 'resetSubmissionBtn',
									text: 'Reset'
								},
								{
									xtype: 'button',
									margin: '0 0 0 10',
									action: 'clearSubmissionBtn',
									itemId: 'clearSubmissionBtn',
									text: 'Clear'
								}
							]
						},
						title: 'Details',
						items: [
							{
								xtype: 'fieldset',
								collapsible: true,
								items: [
									{
										xtype: 'panel',
										margin: '0 0 5 0',
										layout: {
											type: 'hbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',
												flex: 1,												
												fieldLabel: 'Bank',
												labelWidth: 85,
												name: 'fullName',
												readOnly: true
											},
											{
												xtype: 'textfield',
												margin: '0 0 0 5',												
												width: 70,
												fieldLabel: 'ShortName',
												hideLabel: true,
												name: 'shortName',
												readOnly: true
											}
										]
									},
									{
										xtype: 'panel',
										margin: '0 0 5 0',
										layout: {
											type: 'hbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',
												hidden:true,
												flex: 1,												
												fieldLabel: 'User Name',
												labelWidth: 75,
												name: 'userName',
												readOnly: true
											},											
											{
												xtype: 'datefield',
												flex: 1,
												hidden:true,
												fieldLabel: 'Receive Date',
												//labelAlign: 'right',
												labelWidth: 85,
												name: 'recvdate',
												format: 'm-d-Y'
											},
											{
												xtype: 'textfield',
												flex: 1,												
												fieldLabel: 'Submit Note',
												//labelAlign: 'right',
												labelWidth: 85,
												name: 'submitnote'
											}																						
										]
									},
									{
										xtype: 'panel',
										margin: '0 0 5 0',
										layout: {
											type: 'hbox',
											align: 'stretch'
										},
										items: [											
											{
												xtype: 'datefield',
												flex: 1,												
												fieldLabel: 'Submit Date',												
												labelWidth: 85,
												name: 'submitdate',
												format: 'm-d-Y'
											},
											{
												xtype: 'textfield',
												flex: 1,												
												fieldLabel: 'Submit Method',
												labelAlign: 'right',
												labelWidth: 95,
												name: 'submitmethod'
											}
										]
									},
									{
										xtype: 'currencyfield',
										hideTrigger:true,
										labelWidth: 120,
										anchor: '100%',
										fieldLabel: 'Amount Requested',
										name: 'amountRequested'
									},
									{
										xtype: 'numberfield',
										anchor: '100%',
										hidden: true,
										fieldLabel: 'idsubmission',
										name: 'idsubmission'
									},
									{
										xtype: 'numberfield',
										anchor: '100%',
										hidden: true,
										fieldLabel: 'Activityid',
										name: 'activityid'
									}
								]
							},
							{
								xtype: 'submissioncontactsgridpanel'
							},
							{
								xtype: 'bankdocsgridpanel'
							},
							{
								xtype: 'genfilespersubmissiongridpanel',
								margin: '5 0 0 0'
							},
							{
								xtype: 'panel',
								margin: '10 0 5 0',
								layout: {
									type: 'hbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'textfield',
										flex: 1,										
										fieldLabel: 'Name',
										labelWidth: 40,
										name: 'genfilename'
									},
									{
										xtype: 'textfield',
										hidden: true,										
										fieldLabel: 'genfiletype',
										labelWidth: 40,
										name: 'genfiletype'
									},
									{
										xtype: 'button',
										disabled: true,
										itemId: 'generatefilebtn',
										margin: '0 0 0 10',
										text: 'Generate'
									}
								]
							},
							{
								xtype: 'textfield',
								//flex: 1,
								width:'100%',
								fieldLabel: 'Description',
								labelWidth: 80,
								name: 'genfiledesc'
							},
							{
								xtype: 'textfield',
								///flex: 1,
								width:'100%',
								value:'generated',
								readOnly:true,
								fieldLabel: 'Classification',
								labelWidth: 80,
								name: 'genfileclsfctn'
							},
							{
								xtype: 'panel',
								margin: '5 0 0 0',
								layout: {
									type: 'hbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'button',
										flex: 1,
										disabled: true,
										itemId: 'emailbtn',
										margin: '0 5 0 5',
										hrefTarget: '_self',
										text: 'Email'
									},
									{
										xtype: 'button',
										flex: 1,
										disabled: true,
										itemId: 'faxbtn',
										hrefTarget: '_self',
										text: 'Fax'
									},
									{
										xtype: 'button',
										flex: 1,
										disabled: true,
										itemId: 'printbtn',
										margin: '0 0 0 5',
										text: 'Print'
									}
								]
							}
						]
					}
				]
			}
		];
		me.callParent(arguments);
    }
});