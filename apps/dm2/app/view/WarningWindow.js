Ext.define('DM2.view.WarningWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.warningwindow',

    requires: [
        'Ext.window.Window'
    ],

    //height: 500,
    //width: 750,
    //layout: 'fit',
	//title: 'Warning!',
    //message: 'No Permission is allowed for this deal.',
	//html: 'No Permission is allowed for this deal.'//,
	//buttons: Ext.Msg.OK,
	//icon: 'Ext.Msg.WARNING'
	ui:'warningwindow',
	header: false,
	padding:15,
    items: [
        {
            html: '<span style="color:#ff0000;font-weight:bold;">No Permission is allowed for this deal.</span>'
        }
    ]

});