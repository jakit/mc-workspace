Ext.define('DM2.view.DealSalesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealsalesgridpanel',

    requires: [
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],
    
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;		
        me.cls = 'gridcommoncls';
        me.title = 'Sales';
        me.columnLines = false;
		
		me.store = Ext.create('DM2.store.Sales');
		
		me.plugins = [{
            itemId: 'rowexpander-sales',
            ptype: 'rowexpander',
            expandOnDblClick: false,
            rowBodyTpl: [
				'<div id="bidgridrow-{idSale}"></div>'
            ]
        }];
		
		me.viewConfig = {
			stripeRows: true,
			// This renderer to apply conditional css on each grid row based specific column value (dtcc status)
			getRowClass: function (record, rowIndex, rowParams, store) {
				//var hasOption = record.get('hasOption');
				//console.log("HasOp = "+hasOption);
				var saleType = record.get('saleType');
				if(saleType!='S' && saleType!='O'){  // if name column is blank
					return 'hide-row-expander';
				}
			}
		};//end of viewconfig
		
        me.columns = [{
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'idSale',
            text: 'Sale id',
            flex: 1
        }, {
            xtype: 'datecolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                return Ext.util.Format.date(record.get('modifyDateTime'), "m/d/Y h:i:s");
            },
			flex: 1,
            //width: 150,
			dataIndex: 'modifyDateTime',
            text: 'Date/Time'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'saleType',
            text: 'Sale Type',
            flex: 1
        }, {
            xtype: 'gridcolumn',
			cls: 'dealgridcolumncls',
			flex:1,
            dataIndex: 'price',
            text: 'Price',
			format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                //var formatVal = Ext.util.Format.number(value,'0,000');
				/*var readable = record.get('readable');
				if(readable==0){
					return null;
				} else {*/
					if(value==null || value==0){
						return null;  
					} else {
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				//}
            }
        }];
        
        /*me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            hidden: true,
            itemId: 'notegridtoolbar',
            items: [
                {
                    xtype: 'button',
                    action: 'addnote',
                    itemId: 'addnotebtn',
                    text: 'Add Note'
                }
            ]
        }];*/
        
        me.callParent(arguments);
    }    
});