Ext.define('DM2.view.contact.ContactNotesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.contactnotesgridpanel',

    requires: [
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

    cls: 'gridcommoncls',
    title: 'Notes',
    columnLines: false,
    store: 'ContactNotes',

    columns: [
        {
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'noteid',
            text: 'Note id',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                return Ext.util.Format.date(record.get('modifiedDateTime'), "m/d/Y h:i:s");
            },
            width: 150,
            dataIndex: 'modifiedDateTime',
            text: 'Date/Time'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'content',
            text: 'Content',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                return record.get('modifiedby');
            },
            width: 120,
            dataIndex: 'modifiedby',
            text: 'By'
        }/*,
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 120,
            dataIndex: 'assoctype',
            text: 'Assoc Type'
        }*/
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            //hidden: true,
            itemId: 'notegridtoolbar',
            items: [
                {
                    xtype: 'button',
                    action: 'addnotebtn',
                    itemId: 'addnotebtn',
                    text: 'Add Note'
                }
            ]
        }
    ]

});