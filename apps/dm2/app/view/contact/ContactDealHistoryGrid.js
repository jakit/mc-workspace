Ext.define('DM2.view.contact.ContactDealHistoryGrid', {
    extend: 'DM2.view.component.grid.DashboardNew',
    alias: 'widget.contactdealhistorygrid',
    requires: [ 'DM2.model.Deal', 'Ext.data.proxy.Rest','DM2.store.DealHistoryContact'],
	mixins: ['Filters2.InjectHeaderFields', 'Filters2.FiltersMixin'],
   
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
		me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
		//me.ui = 'activitypanel';
		me.framed = true;
        
		if(!me.store) {
            me.store = Ext.create('DM2.store.DealHistoryContact');		    
		}
		
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .4,
            sortable: true,
			hidden:true,
            dataIndex: 'DM-dealNo',
            //menuDisabled: true,
            text: 'DM-DealNo.',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .4,
            sortable: true,
            //dataIndex: 'dealid',
			dataIndex: 'LT-dealNo',
            //menuDisabled: true,
            text: 'LT-DealNo',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: 1.5,
            dataIndex: 'dealName',
            //menuDisabled: true,
            text: 'Name',
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'streetNumber',
            //menuDisabled: true,
            text: 'Street No.',
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'streetName',
            //menuDisabled: true,
            text: 'Street',
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'city',
            //menuDisabled: true,
            text: 'City',
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'state',
            //menuDisabled: true,
            text: 'State',
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: Ext.getStore('StatesList').getData()
            })
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'maincontact',
            //menuDisabled: true,
            text: 'Contact',
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'status',
            //menuDisabled: true,
            text: 'Status',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: [{
                    'display': "AC",
                    'val': "AC"
                }, {
                    'display': "CL",
                    'val': "CL"
                }, {
                    'display': "CX",
                    'val': "CX"
                }, {
                    'display': "DX",
                    'val': "DX"
                }, {
                    'display': "EC",
                    'val': "EC"
                }, {
                    'display': "IP",
                    'val': "IP"
                }, {
                    'display': "LD",
                    'val': "LD"
                }, {
                    'display': "PR",
                    'val': "PR"
                }, {
                    'display': "UL",
                    'val': "UL"
                }],
                filterDataType: "string"
            })
        }, {
            xtype: 'datecolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'startdate',
            //menuDisabled: true,
            text: 'DealDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'bankName',
            //menuDisabled: true,
            text: 'Bank',
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        }, {
            xtype: 'numbercolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'loanAmt',
            //menuDisabled: true,
            text: 'Loan Amount',
            format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                 //var formatVal = Ext.util.Format.number(value,'0,000');
				if(value==null || value==0){
					return null;  
				}else{
					var formatVal = Ext.util.Format.currency(value, '$',0);
					metaData.tdAttr = 'data-qtip="' + formatVal + '"';
					return formatVal;
				}
            },
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'datecolumn',
            cls: 'dealgridcolumncls',
            dataIndex: 'loanDate',
            //menuDisabled: true,
            flex: .5,
            text: 'LoanDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'userList',
            //menuDisabled: true,
            text: 'Broker',
            flex: .5,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        }];
        
        me.callParent(arguments);  
   }
});