Ext.define('DM2.view.contact.ContactDocsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.contactdocsgridpanel',

    requires: [
        'DM2.view.override.ContactDocsGridPanel',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.form.field.File'
    ],

    cls: 'gridcommoncls',
    itemId: 'contactdocsgridpanel',
    title: 'Documents',
    store: 'ContactDocuments',

    columns: [
        {
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'docid',
            text: 'Doc Id',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                return Ext.util.Format.date(record.get('modifydate'), "m/d/Y h:i:s");
            },
            dataIndex: 'modifydate',
            text: 'Date/Time',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				var docurlname = record.get('url');
                var ext = DM2.app.getController('MainController').getExtImg(docurlname);
				//return '<a target="_blank" style="color: -webkit-link;text-decoration: underline;cursor: pointer;"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
				var targetframe = DM2.app.getController('MainController').getTargetFrame(record.get('ext'));
				
                return '<a class="doclink" style="color: -webkit-link;text-decoration: underline;cursor: pointer;" target="'+targetframe+'" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
            },
            dataIndex: 'name',
            text: 'Document Name',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            dataIndex: 'size',
            text: 'Size',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'desc',
            text: 'Description',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'clsfctn',
            text: 'Classification',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'modifiedBy',
            text: 'By',
            flex: 1
        }/*,
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assoctype',
            text: 'Assoc Type',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assocdesc',
            text: 'Assoc Desc',
            flex: 1
        }*/
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            hidden: true,
            items: [
                {
                    xtype: 'button',
                    action: 'adddocbtn',
                    text: 'Add Document'
                }
            ]
        }
    ]

});