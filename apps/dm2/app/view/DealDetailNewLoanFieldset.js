Ext.define('DM2.view.DealDetailNewLoanFieldset', {
    extend: 'DM2.view.component.container.GridToForm',
    alias: 'widget.dealdetailnewloanfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool',
        'DM2.model.Quote',
        'Ext.data.proxy.Rest',
		'DM2.view.CurrencyField',
		'DM2.view.DealDetailNewLoanMenu'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        //me.frame = true;
    //    me.itemId = 'dealdetailcontactfieldset';
        //me.ui = 'newloandealpanel';      
        me.detailOnSingleRecord = true;
        me.contextMenu = Ext.create('DM2.view.DealDetailNewLoanMenu');
        
        // *************
        var store = Ext.create('Ext.data.Store', {
            model: 'DM2.model.Quote',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_quotesPerDeal'
            },
            autoLoad: false     
        });        
        // ***************        
        
        
        me.listLayout= {
            icon: 'resources/images/icons/loan_m.png',
            title: "NEW LOAN",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailnewloangridpanel'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            // store: 'Quotes',
            store: store,
			ui : 'newloandealpanel', 
			frame:true,
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                //dataIndex: 'bank',
				dataIndex: 'fullName',
                text: 'Bank',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                //dataIndex: 'receivedamount',
				dataIndex: 'amountRequested',
                text: 'Amount',
                flex: 1,
				align:'right'
            },
			{
                xtype: 'numbercolumn',
                dataIndex: 'amountQuoted',
                text: 'Quote Amount',
                flex: 1,
				align:'right'
            },
			{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'rate',
                text: 'Rate',
                flex: 1
            },
            /*{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ratetype',
                text: 'Rate Type',
                flex: 1
            },*/
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'quotetype',
                text: 'Loan Type',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'term',
                text: 'Term',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'amortterm',
                text: 'Amort',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ppp',
                text: 'PPP',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/loan_m.png',
            title: "NEW LOAN",     
      		//detailOnSingleRecord: true,       
            //padding: '5 0 0 5',
            scrollable: true,
			ui : 'newloandealpanel', 
			frame:true,
			items:[{
				xtype:'panel',
	            layout: 'column',
	            items: [{
                    xtype: 'panel',
                    columnWidth: 0.45,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Bank',
                            labelAlign: 'right',
                            labelWidth: 50,
                            //name: 'bank'
							name: 'fullName'
                        },                 
                        {
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'PPP',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'ppp'
                        },
						{
							xtype: 'numberfield',
							readOnly:true,
							labelWidth: 50,
							hideTrigger:true,
							labelAlign: 'right',
							fieldLabel: 'PPP Window',
							name: 'pppWindow'
						},
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'checkboxfield',
                                    flex: 1,
									name: 'hasOption',
                                    readOnly:true,
                                    fieldLabel: 'Option',
                                    labelAlign: 'right',
                                    labelWidth: 50
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    readOnly:true,
                                    fieldLabel: 'Amort',
                                    labelAlign: 'right',
                                    labelWidth: 50,
                                    name: 'amortterm'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.25,
                    margin: '0px 5px 0px 0px',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        /*{
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'IO',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'io'
                        },*/
						{
                            xtype: 'currencyfield',
                            readOnly:true,
                            fieldLabel: 'Amount',
                            labelAlign: 'right',
                            labelWidth: 50,
                            //name: 'receivedamount'
							name: 'amountRequested'
                        },
                        /*{
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Index',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'index'
                        },
                        {
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Spread',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'spread'
                        },*/
						{
                            xtype: 'currencyfield',
							hidden:true,
							//margin:'5 0 0 0',
                            readOnly:true,
                            fieldLabel: 'Quote Amt',
                            labelAlign: 'right',
                            labelWidth: 50,
							name: 'amountQuoted'
                        },
						{
							xtype: 'datefield',
							readOnly:true,
							fieldLabel: 'Start',
							labelAlign: 'right',
							labelWidth: 50
						},
						{
							xtype: 'datefield',
							readOnly:true,
							fieldLabel: 'End',
							labelAlign: 'right',
							labelWidth: 50
						},
                        {
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Months',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'term'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.3,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [                   
                        /*{
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Loan Type',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'loantype'
                        },
                        {
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Index Value',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'indexvalue'
                        },*/						
						/*{
							xtype: 'textfield',
							readOnly:true,
							fieldLabel: 'Execution',
							labelAlign: 'right',
							labelWidth: 65
						},*/
						{
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Rate',
                            labelAlign: 'right',
                            labelWidth: 65,
							name: 'rate'
                            //name: 'ratetype'
                        },
                        {
                            xtype: 'textfield',
							hidden:true,
                            readOnly:true,
                            fieldLabel: 'Mortgage Type',
                            labelAlign: 'right',
                            labelWidth: 65
                        },
						{
                            xtype: 'textfield',
                            readOnly:true,
                            fieldLabel: 'Loan Type',
                            labelAlign: 'right',
                            labelWidth: 65,
							name: 'quotetype'
                        }
                    ]
                }]
			}]
        };
        
        me.callParent(arguments);
    }

});