Ext.define('DM2.view.DealGridOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealgridoptionmenu',

    requires: [
        'Ext.menu.Item'
    ],

    width: 200,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'removeallfilter',
            text: 'Remove All Filter',
            focusable: true
        }
    ]

});