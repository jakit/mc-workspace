Ext.define('DM2.view.PropertyMenuTabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.propertymenutabpanel',

    requires: [
		'DM2.view.property.PropertyContactsGridPanel',
		'DM2.view.property.PropertyDealHistoryGrid',
        'DM2.view.property.PropertyDocsGridPanel',
		'DM2.view.PropertyAKAList',
        'Ext.grid.Panel',
        'Ext.tab.Tab',
        'Ext.tab.Bar'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    dealHistoryPropertyStore: null,
    initComponent: function() {
        var me = this;
    	me.activeTab = 0;
    	me.items = [];
		me.callParent(arguments);        
    },
	tabBar:{
		xtype: 'tabbar',
		ui: 'submenutabpanel'
	}
});