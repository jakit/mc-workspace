Ext.define('DM2.view.AcrisPropertyDocsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.acrispropertydocsgrid',

    requires: [
		'Ext.form.Panel',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.grid.plugin.DragDrop',
        'Ext.util.Point',
        'Ext.toolbar.Toolbar',
        'Ext.form.field.File'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.cls = 'gridcommoncls';
		//me.title = 'Documents';
		me.store = Ext.create('DM2.store.AcrisDocuments');
		me.itemId ='acrispropertydocsgrid';
		me.ui = 'activitypanel';
		me.frame = true;
		me.header = false;
		me.collapsible = true;
		me.scrollable = true;
		me.style = "border-width:2px;";
		//me.maxHeight = 300;
		me.columns = [
			{
				xtype: 'gridcolumn',
				align:'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var docid = record.get('idAcris');
					
					var dateFileCreated = record.get('dateFileCreated');
					if(dateFileCreated!=null) {
						var targetframe = "_blank";//DM2.app.getController('MainController').getTargetFrame(record.get('ext'));
						return '<a class="doclink" style="color: -webkit-link;text-decoration: underline;cursor: pointer;" target="'+targetframe+'" href="'+value+'"><img src="resources/images/icons/send_s.png" /></a>';
					} else {
						//return '<img src="resources/images/icons/doc_s.png" />';
						return '';
					}
				},
				dataIndex: 'name',
				text: 'URL',
				width:50
				//flex: 1
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'docType',
				text: 'Doc Type',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden:true,
				dataIndex: 'docDescription',
				text: 'Description',
				flex: 1
			},
			{
				xtype: 'datecolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					return Ext.util.Format.date(record.get('docDate'), "m/d/Y");
				},
				dataIndex: 'docDate',
				text: 'Doc Date',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				//hidden: true,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}					
				},
				format: '0,000',
				align:'right',
				dataIndex: 'docAmount',
				text: 'Amount',
				flex: 1
			},
			{
				xtype: 'datecolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					return Ext.util.Format.date(record.get('dateFileCreated'), "m/d/Y h:i:s");
				},
				hidden:true,
				dataIndex: 'dateFileCreated',
				text: 'Doc Date',
				flex: 1
			},
			{
				xtype: 'datecolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					return Ext.util.Format.date(record.get('recordedDate'), "m/d/Y");
				},
				dataIndex: 'recordedDate',
				text: 'Recorded Date',
				flex: 1
			}
		];
		me.viewConfig = {
			plugins: [
				{
					ptype: 'gridviewdragdrop',
					ddGroup: 'dropsubmissiondoc'
				}
			],
			getRowClass: function(record, rowIndex, rowParams, store) {
				if(record && record.get('assoctype') ===  "Property"){
					return 'property-doc-color-cls';
				}
			}
		};
		me.dockedItems = [
			{
				xtype: 'form',
				dock:'bottom',
				scrollable: true,
				collapsible: true,
				layout: 'column',
				bodyPadding: 5,
				title:'Selected Document Details',
				ui: 'activitypanel',
				frame: true,
				//header:false,
				items: [
					{
						xtype: 'panel',
						columnWidth: 0.5,
						margin: '0 5 0 0',
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [
							/*{
								xtype: 'textfield',								
								fieldLabel: 'Reel',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'RealNBR',
								readOnly: true
							},*/
							{
								xtype: 'textfield',
								fieldLabel: 'ACrIS #',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'idAcris',
								readOnly: true
							},
							{
								xtype: 'textfield',
								fieldLabel: 'Page',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'realPG',
								readOnly: true
							}							
						]
					},
					{
						xtype: 'panel',
						columnWidth: 0.5,
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',								
								fieldLabel: 'CRFN',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'CRFN',
								readOnly: true
							}/*,
							{
								xtype: 'textfield',								
								fieldLabel: 'Master Id',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'masterid',
								readOnly: true
							}*/
						]
					}
				]
			}
		];
		me.callParent(arguments);
    }
});