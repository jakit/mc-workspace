Ext.define('DM2.view.CompanyPickerPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.companypickerpanel',

    requires: [
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.filters.filter.String',
        'Ext.view.Table',
        'Ext.grid.filters.Filters',
        'Ext.button.Button',
        'Ext.form.field.ComboBox',
		'DM2.view.CompanyPickerGrid'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
	    me.frame = true;
	    me.margin = 3;
    	me.ui = 'activitypanel';
	    me.layout = 'border';
    	me.closable = true;
	    me.title = 'Select Company';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					margin: '0 10 0 0',
					action: 'addCompanyBtn',
					text: 'Create New Company'
				},
				{
					xtype: 'button',
					margin: '0 10 0 0',
					action: 'selectCompanyBtn',
					text: 'Select'
				}
			]
		};
		me.items = [
			{
				xtype: 'panel',
				region: 'north',
				split: true,
				collapsible: true,
				scrollable: true,
				layout: 'column',
				bodyPadding: 5,
				header:false,
				items: [
					{
						xtype: 'panel',
						columnWidth: 0.5,
						margin: '0 5 0 0',
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',								
								fieldLabel: 'Company',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'companyName',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'State',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'state',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'Phone 1',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'phone1',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'Phone 2',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'phone2',
								readOnly: true
							}
						]
					},
					{
						xtype: 'panel',
						columnWidth: 0.5,
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',								
								fieldLabel: 'Address',
								labelAlign: 'right',
								labelWidth: 70,
								name: 'address',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'City',
								labelAlign: 'right',
								labelWidth: 70,
								name: 'city',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'Email 1',
								labelAlign: 'right',
								labelWidth: 70,
								name: 'email1',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'Email 2',
								labelAlign: 'right',
								labelWidth: 70,
								name: 'email2',
								readOnly: true
							}
						]
					}
				]
			},
			{
				xtype: 'companypickergrid',
				region: 'center',
				scrollable: true
			}        
		];
		me.callParent(arguments);
    }
});