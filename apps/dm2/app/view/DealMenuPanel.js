Ext.define('DM2.view.DealMenuPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.dealmenupanel',

    requires: [
        'DM2.view.DealMenuGridPanel',
        'DM2.view.DealMenuTabPanel',
        'Ext.grid.Panel',
        'Ext.tab.Panel'
    ],
  
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
        
    },
    initComponent: function() {
        var me = this;

        me.itemId = 'dealmenupanel';
        me.layout = {
            type: 'border'
        };
        me.bodyStyle = '{ background-color:#fff; }';

        me.items = [{
            xtype: 'dealmenugridpanel',
            region: 'center'
        }, {
            xtype: 'dealmenutabpanel',
            height: '25%',
            collapsible: true,
            header: false,
            region: 'south',
            split: true,
            notesStore: null
        }];
        
        me.callParent(arguments);        
    } 
});