Ext.define('DM2.view.DealDetailQuotesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealdetailquotesgridpanel',

    requires: [
        'Ext.grid.column.Number',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.cls = 'gridcommoncls';
		me.title = 'Quotes';
		me.store = Ext.create('DM2.store.DealQuotes');//'DealQuotes';
		/*me.viewConfig = {
            itemId: 'quoteoptiongridview',
            forceFit: true
        };*/
		me.plugins = [{
            itemId: 'rowexpander-quotes',
            ptype: 'rowexpander',
            expandOnDblClick: false,
            rowBodyTpl: [
				'<div id="optiongridrow-{loanid}"></div>'
            ]
        }];
		me.viewConfig = {
			stripeRows: true,
			// This renderer to apply conditional css on each grid row based specific column value (dtcc status)
			getRowClass: function (record, rowIndex, rowParams, store) {
				var hasOption = record.get('hasOption');				
				//console.log("HasOp = "+hasOption);
				//if(hasOption == 'N'){  // if name column is blank
				if(!hasOption){  // if name column is blank
					return 'hide-row-expander';
				}
			}
		};//end of viewconfig
		me.columns = [
			{
				xtype: 'checkcolumn',
				dataIndex: 'selecteddis',
				text: 'S',
				width: 50,
				processEvent: Ext.emptyFn
			},
			/*{
				xtype: 'checkcolumn',
				dataIndex: 'selected',
				text: 'S',
				flex: 1,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(value=='S'){
						return true;
					} else if(value=='Q'){
						return false;
					}					
				}
			},*/
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'bank',
				text: 'Bank',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'rate',
				text: 'Rate',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'quotetype',
				text: 'Loan Type',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				width: 70,
				dataIndex: 'term',
				text: 'Term'
			},
			{
				xtype: 'numbercolumn',
				dataIndex: 'amountRequested',
				text: 'Requested Amount',
				flex: 1,
				format: '0,000',
				align:'right',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					 //var formatVal = Ext.util.Format.number(value,'0,000');
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				}
			},
			{
				xtype: 'numbercolumn',
				dataIndex: 'amountQuoted',
				text: 'Amount',
				flex: 1,
				format: '0,000',
				align:'right',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					 //var formatVal = Ext.util.Format.number(value,'0,000');
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				}
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden:true,
				dataIndex: 'ratetype',
				text: 'Rate Type',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden:true,
				dataIndex: 'spread',
				text: 'Spread',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden:true,
				dataIndex: 'index',
				text: 'Index',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden:true,
				dataIndex: 'indexvalue',
				text: 'Index Value',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'amortterm',
				text: 'Amort Term',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				width: 60,
				dataIndex: 'io',
				text: 'IO',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'ppp',
				text: 'PPP',
				flex: 1
			}
		];
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						itemId: 'add',
						iconCls: 'add',
						text: 'Add'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});