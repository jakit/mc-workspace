Ext.define('DM2.view.DealDetailPropertiesMenu', {
    extend: 'DM2.view.component.container.DealsDetailsContextMenu',
    alias: 'widget.dealdetailpropertiesmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
            text: 'Add Property',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'add'
        },
        {
            xtype: 'menuitem',
			action:'removeproperty',
            text: 'Remove Property',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        },
        {
            xtype: 'menuitem',
			modifyMenu : 'no',
            text: 'Research',
            focusable: true,
			menuType : 'edit',
            menu: {
                xtype: 'menu',
                itemId: 'researchmenu',
                width: 120,
                items: [
                    {
                        xtype: 'menuitem',
                        text: 'Gmap',
                        focusable: true
                    },
                    {
                        xtype: 'menuitem',
                        text: 'Zillow',
                        focusable: true
                    },
                    /*{
                        xtype: 'menuitem',
                        text: 'Trulia',
                        focusable: true
                    },*/
                    {
                        xtype: 'menuitem',
                        text: 'Reonomy',
                        focusable: true
                    },
					{
                        xtype: 'menuitem',
                        text: 'Oasis',
                        focusable: true
                    }
                ]
            }
        },
		{
            xtype: 'menuitem',
            text: 'View Master Data',
            focusable: true,
			modifyMenu : 'no',
			menuType : 'edit'
        },
		{
            xtype: 'menuitem',
            text: 'List ACRIS Documents',
            focusable: true,
			modifyMenu : 'no',
			menuType : 'edit'
        },
		{
            xtype: 'menuitem',
			disabled:true,
			action:'changeprimary',
            text: 'Change Primary',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        },
		{
            xtype: 'menuitem',
			action:'editproperty',
            text: 'Change Mortgage Type',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        }];
        me.callParent(arguments);
    }
});