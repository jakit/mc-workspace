Ext.define('DM2.view.SalesEditView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.saleseditview',

    requires: [
        'Ext.panel.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
		'DM2.view.CommissionSchdGridPanel'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent : function() {
        var me = this;
		me.frame = true;
		me.itemId = 'saleseditview';
		me.margin = 3;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;
		me.header = {
			titlePosition: 0,
			items: [{
				xtype: 'button',
				disabled: true,
				action: 'savesalesbtn',
				margin: '0 10 0 0',
				minWidth: 65,
				text: 'Save'
			}/*,
			{
				xtype: 'button',
				disabled: true,
				itemId: 'resetbtn',
				minWidth: 65,
				text: 'Restore'
			}*/]
		};
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		me.items = [
			{
                xtype: 'textfield',
				hidden:true,
				fieldLabel: 'idSale',
				labelAlign: 'right',
				labelWidth: 60,
				name: 'idSale'
			},
			{
				xtype: 'panel',
				margin: '0px 0px 6px 0px',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
                    xtype: 'combobox',
                    flex: 1,
					//readOnly:true,
                    itemId: 'saleType',
                    fieldLabel: 'SaleType',
                    labelAlign: 'right',
                    labelWidth: 60,
                    name: 'saleType',
					queryMode: 'local',
					//displayField: 'saleName',
					//valueField: 'saleValue',
					store : [['S','Sell'],['B','Buy'],['C','Consulting'],['O','OffMarket']],
					listeners: {
						afterrender: function(combo) {
							var recordSelected = combo.getStore().getAt(0);                     
							combo.setValue(recordSelected.get('field1'));
						}
					}
                },
                {
                    xtype: 'currencyfield',
                    flex: 1,
                    itemId: 'price',
                    fieldLabel: 'Price',
                    labelAlign: 'right',
                    labelWidth: 60,
                    name: 'price'
                }]
			},
			{
				xtype: 'panel',
				margin: '0px 0px 6px 0px',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'combobox',
						flex: 1,
						fieldLabel: 'Commission Type',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'commissionType',
						queryMode: 'local',
						editable : false,
						//displayField: 'saleName',
						//valueField: 'saleValue',
						store : [['P','Percentage'],['F','Flat'],['S','Structure']],
						listeners: {
							afterrender: function(combo) {
								var recordSelected = combo.getStore().getAt(0);                     
								combo.setValue(recordSelected.get('field1'));
							},
							change: function(cmb , newValue , oldValue , eOpts){
								var percentCommission = cmb.up('saleseditview').down('numberfield[name="percentCommission"]');
								var flatAmtCommission = cmb.up('saleseditview').down('currencyfield[name="flatAmountCommission"]');
								var commissionschdgridpanel = cmb.up('saleseditview').down('commissionschdgridpanel');
								if(newValue=="P"){
									percentCommission.show();
									flatAmtCommission.hide();
									commissionschdgridpanel.hide();
								} else if(newValue=="F"){
									percentCommission.hide();
									flatAmtCommission.show();
									commissionschdgridpanel.hide();
								} else if(newValue=="S"){
									percentCommission.hide();
									flatAmtCommission.hide();
									commissionschdgridpanel.show();
									DM2.app.getController('SalesDealDetailController').loadCommSchedule(cmb.up('saleseditview'));
								}
							}
						}
					},
					{
						xtype: 'numberfield',
						hideTrigger:true,
						flex: 1,
						fieldLabel: 'Commission (%)',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'percentCommission'
					},
					{
						xtype: 'currencyfield',
						hidden:true,
						hideTrigger:true,
						flex: 1,
						fieldLabel: 'Commission Amt',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'flatAmountCommission'
					}
				]
			},
			{
				xtype: 'commissionschdgridpanel'
			}]
		me.callParent(arguments);
    }
});