Ext.define('DM2.view.ContactPhonesGridPanelOld', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.contactphonesgridpanelold',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel'
    ],

    frame: true,
    ui: 'activitypanel',
    collapsible: true,
    title: 'Phones',
    store: 'ContactPhones',

    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'tag',
            text: 'Tag',
            flex: 1,
            editor: {
                xtype: 'combobox',
                allowBlank: false,
                editable: false,
                displayField: 'tag',
                forceSelection: true,
                queryMode: 'local',
                store: 'PhoneTags',
                valueField: 'tag'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'phoneNumber',
            text: 'Number',
            flex: 1,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'isPrimary',
            text: 'IsPrimary',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'add',
                    iconCls: 'add',
                    text: 'Add'
                },
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'delete',
                    iconCls: 'delete',
                    text: 'Delete'
                }
            ]
        }
    ],
    plugins: [
        {
            ptype: 'cellediting',
            pluginId: 'contactphoneeditingplugin'
        }
    ],
    selModel: {
        selType: 'cellmodel'
    }

});