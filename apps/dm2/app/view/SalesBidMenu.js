Ext.define('DM2.view.SalesBidMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.salesbidmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
			action: 'addbid',
            text: 'Add Bid',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			action: 'editbid',
            text: 'Edit Bid',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
			action: 'deletebid',
            text: 'Delete Bid',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
			action: 'selectbid',
            text: 'Select Bid',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
			action: 'unselectbid',
            text: 'Unselect Bid',
            focusable: true,
			modifyMenu : 'yes'
        }];
        me.callParent(arguments);
    }
});