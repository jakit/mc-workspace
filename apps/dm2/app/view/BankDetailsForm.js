Ext.define('DM2.view.BankDetailsForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.bankdetailsform',

    requires: [
        'Ext.form.field.Number',
        'Ext.form.field.Checkbox',
        'Ext.panel.Panel',
        'Ext.button.Button',
		'DM2.view.UpperTextField'
    ],

    frame: true,
	title:'Bank Details',
    itemId: 'bankdetailform',
    //margin: 3,
    scrollable: true,
    ui: 'activitypanel',
    width: 400,
    defaults: {
        labelWidth: 120
    },
    bodyPadding: 5,
    //collapsible: true,

    items: [
        {
            xtype: 'numberfield',
            anchor: '100%',
            hidden: true,
            fieldLabel: 'idBank',
            name: 'idBank'
        },
        {
            //xtype: 'textfield',
			xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'Name',
            name: 'fullName'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'ShortName',
            name: 'shortName'
        },
        {
            xtype: 'checkboxfield',
            anchor: '100%',
            fieldLabel: 'MeridianBank',
            name: 'meridianBank',
            boxLabel: 'Yes'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'PayoutNoticeDays',
            name: 'payoutNoticeDays'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'DefaultExecution',
            name: 'defaultExecution'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'Address',
            name: 'address'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'Office',
            name: 'office'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'State',
            name: 'state'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'City',
            name: 'city'
        },
        {
            xtype: 'uppertextfield',
            anchor: '100%',            
            fieldLabel: 'Zip',
            name: 'zip'
        },
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
                    itemId: 'savebankbtn',
                    text: 'Save'
                }
            ]
        },
        {
            xtype: 'textfield',
            anchor: '100%',
            hidden: true,
            fieldLabel: 'DealId',
            name: 'dealid'
        }
    ]
});