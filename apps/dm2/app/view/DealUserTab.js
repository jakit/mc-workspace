Ext.define('DM2.view.DealUserTab', {
    extend: 'Ext.button.Split',
    alias: 'widget.dealusertab',

    requires: [
        'Ext.button.Button',
		'Ext.button.Split'
    ],
	
    iconCls: 'loanbtn',
	text:'Users',
	cls:'docbtncls',
	menu:{
		items:[{
			//xtype: 'menucheckitem',
			group: 'users',
			checked : true,
			text:'Active Only'
		},{
			//xtype: 'menucheckitem',
			group: 'users',
			checked: false,
			text:'All'
		}]
	}
});