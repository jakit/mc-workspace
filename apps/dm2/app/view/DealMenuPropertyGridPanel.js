Ext.define('DM2.view.DealMenuPropertyGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealmenupropertygridpanel',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date'
    ],

    cls: 'dealmenupropertygridcls',
    frame: true,
    ui: 'propertyloansubgrid',
    header: false,
    store: 'Propertiesdeal',

    columns: [
        {
            xtype: 'gridcolumn',
            width: 75,
            dataIndex: 'street_no',
            //locked: true,
            text: 'Str. No.'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 140,
            dataIndex: 'street',
            //locked: true,
            text: 'Street'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			flex:1,
            dataIndex: 'city',
            //locked: true,
            text: 'City'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 75,
            dataIndex: 'state',
            //locked: true,
            text: 'State'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'buildingClass',
            //locked: true,
            text: 'BuildClass'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'propertyType',
            //locked: true,
            text: 'PropertyType'
        }
    ],

    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            store : me.buildStore()
        });

        me.callParent(arguments);
    },

    buildStore: function() {
        return Ext.create('DM2.store.Propertiesdeal', {});
    },
	listeners:{
		'itemmouseenter': function( view, record, item, index, e, eOpts ){
			//console.log("Hover on Property Rec : "+record.get('street'));
			
			var dealmenuprloanpanel = view.grid.ownerCt;
			var grids = dealmenuprloanpanel.query('grid');
			//console.log(grids);
			var prptygrid = grids[0];
			var loangrid  = grids[1];
			
			var propertyid = record.get('propertyid');
			
			var objArr = prptygrid.getStore().getProxy().getReader().rawData;
			Ext.Array.each(objArr, function(obj, index, countriesItSelf) {
				if(obj.propertyid===propertyid){
					var loanid = obj.loanid;
					var loanrec = loangrid.getStore().getById(loanid);
					var rowIndex = loangrid.store.indexOf(loanrec);
					//console.log("rowIndex"+rowIndex);
					//Fine Row Index from grid using rec
					loangrid.getView().addRowCls(rowIndex, 'hover-row-style');
				}
			});
		},
		'itemmouseleave': function( view, record, item, index, e, eOpts ){
			//console.log("Leave Mouse on Property Rec : "+record.get('street'));
			
			var dealmenuprloanpanel = view.grid.ownerCt;
			var grids = dealmenuprloanpanel.query('grid');
			//console.log(grids);
			var prptygrid = grids[0];
			var loangrid  = grids[1];
			
			var propertyid = record.get('propertyid');
			
			var objArr = prptygrid.getStore().getProxy().getReader().rawData;
			Ext.Array.each(objArr, function(obj, index, countriesItSelf) {
				if(obj.propertyid===propertyid){
					var loanid = obj.loanid;
					var loanrec = loangrid.getStore().getById(loanid);
					var rowIndex = loangrid.store.indexOf(loanrec);
					//console.log("rowIndex"+rowIndex);
					//Fine Row Index from grid using rec
					loangrid.getView().removeRowCls(rowIndex, 'hover-row-style');
				}
			});
		}
	}
});