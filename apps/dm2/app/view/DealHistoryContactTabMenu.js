Ext.define('DM2.view.DealHistoryContactTabMenu', {
    extend: 'Ext.button.Split',
    alias: 'widget.dealhistorycontacttabmenu',

    requires: [
        'Ext.button.Button',
		'Ext.button.Split'
    ],

    iconCls: 'loanbtn',
	text:'Deal History',
	cls:'docbtncls',
	menu:{
		items:[{
			text:'Contacts',
			group: 'dealhiscntopts',
			checked : true
		},
		{
			text:'Company',
			group: 'dealhiscntopts',
			checked : false
		}]
	}
});