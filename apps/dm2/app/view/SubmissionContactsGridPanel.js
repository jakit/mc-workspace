Ext.define('DM2.view.SubmissionContactsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.submissioncontactsgridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.form.field.ComboBox',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.selection.CellModel',
        'Ext.grid.plugin.CellEditing'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
	    me.cls = 'gridcommoncls';
	    me.frame = true;
	    me.height = 200;
    	me.itemId = 'submissioncontactsgridpanel';
	    me.margin = '0 0 10 0';
	    me.ui = 'activitypanel';
	    me.collapsible = true;
	    me.title = 'Contacts';
	    me.store = Ext.create('DM2.store.SubmissionContacts');
	    me.defaultListenerScope = true;

    	me.columns = [
        {
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'contactid',
            text: 'Contact Id'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'name',
            text: 'Name',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'company',
            text: 'Company',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'type',
            text: 'Type',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                metaData.css = 'gridcombocss';
                return value;
            },
            dataIndex: 'email',
            text: 'Email',
            flex: 1,
            editor: {
                xtype: 'combobox',
                displayField: 'email',
                queryMode: 'local',
                store: 'ContactEmails',
                valueField: 'email'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                metaData.css = 'gridcombocss';
                return value;
            },
            dataIndex: 'faxNumber',
            text: 'Fax',
            flex: 1,
            editor: {
                xtype: 'combobox',
                displayField: 'faxNumber',
                store: 'ContactFax',
                valueField: 'faxNumber'
            }
        }
    ];
	    me.dockedItems = [
        {
            xtype: 'toolbar',
            dock: 'top',
            itemId: 'submissioncontacttoolbar',
            items: [
                {
                    xtype: 'button',
                    itemId: 'showContactToSubmissionBtn',
                    iconCls: 'add',
                    text: 'Add Contact'
                }
            ]
        }
    ];
		me.selModel = {
			selType: 'cellmodel'
		};
		me.plugins = [
			{
				ptype: 'cellediting',
				clicksToEdit: 1,
				listeners: {
					beforeedit: 'onCellEditingBeforeEdit'
				}
			}
		];
		me.callParent(arguments);
    },
    onCellEditingBeforeEdit: function(editor, context, eOpts) {
        if(context.field==="email"){
            DM2.app.getController('DealActivityController').loadContactsEmail(context.record);
        }
        else if(context.field==="fax"){
            DM2.app.getController('DealActivityController').loadContactsFax(context.record);
        }
    }

});