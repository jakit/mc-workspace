Ext.define('DM2.view.DealDetailExistingLoanFieldset', {
    extend: 'DM2.view.component.container.GridToForm',
    alias: 'widget.dealdetailexistingloanfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool',
        'DM2.model.Quote',
        'Ext.data.proxy.Rest',
		'DM2.view.CurrencyField',
		'DM2.view.DealDetailExistingLoanMenu'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        //me.frame = true;
        me.itemId = 'dealdetailexistingloanfieldset';
        //me.ui = 'existingloandealpanel';      
        me.detailOnSingleRecord = true;
        me.contextMenu = Ext.create('DM2.view.DealDetailExistingLoanMenu');
        
        var store = Ext.create('Ext.data.Store', {
            model: 'DM2.model.Quote',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_existingLoansPerDeal'
            },
            autoLoad: false     
        });
        
        // ***************        
        
        me.listLayout= {
            icon: 'resources/images/icons/loan_m.png',
            title: "EXISTING LOAN",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailexistingloangridpanel'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            // store: 'ExistingLoans',
            store: store,
			ui : 'existingloandealpanel', 
			frame:true,
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'fullName',
                text: 'Bank',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                dataIndex: 'receivedamount',
                text: 'Amount',
                flex: 1,
				align:'right'
            },
			{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'rate',
                text: 'Rate Type',
                flex: 1
            },
            /*{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ratetype',
                text: 'Rate Type',
                flex: 1
            },*/
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'loantype',
                text: 'Loan Type',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'term',
                text: 'Term',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'amortterm',
                text: 'Amort',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ppp',
                text: 'PPP',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/loan_m.png',
            title: "EXISTING LOAN",     
      //      detailOnSingleRecord: true,       
           // padding: '5 0 0 5',
            scrollable: true,
            layout: 'column',      
            ui : 'existingloandealpanel', 
			frame:true,
            items: [{
               xtype: 'container',
                layout: {
                    type: 'column'
                },
                items: [{
                    columnWidth: 0.45,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'textfield',
                        readOnly:true,
                        fieldLabel: 'Bank',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'fullName'
                    },
                    {
                        xtype: 'textfield',
                        readOnly:true,
                        fieldLabel: 'PPP',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'ppp'
                    },
					{
						xtype: 'numberfield',
						readOnly:true,
						labelWidth: 50,
						hideTrigger:true,
						labelAlign: 'right',
						fieldLabel: 'PPP Window',
						name: 'pppWindow'
					},
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'checkboxfield',
                                flex: 1,
								name: 'hasOption',
								//hidden:true,
                                readOnly:true,
                                fieldLabel: 'Option',
                                labelAlign: 'right',
                                labelWidth: 50
                            },
                            {
                                xtype: 'textfield',
                                flex: 1,
                                readOnly:true,
                                fieldLabel: 'Amort',
                                labelAlign: 'right',
                                labelWidth: 50,
                                name: 'amortterm'
                            }
                        ]
                    }]
                }, {
                    columnWidth: 0.25,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'currencyfield',
                        readOnly:true,
                        fieldLabel: 'Amount',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'receivedamount'
                    },
                    /*{
                        xtype: 'textfield',
                        readOnly:true,
                        fieldLabel: 'Index',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'index'
                    },
                    {
                        xtype: 'textfield',
                        readOnly:true,
                        fieldLabel: 'Spread',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'spread'
                    },*/
					{
                        xtype: 'datefield',
                        readOnly:true,
                        fieldLabel: 'Start',
                        labelAlign: 'right',
                        labelWidth: 50
                    },
                    {
                        xtype: 'datefield',
                        readOnly:true,
                        fieldLabel: 'End',
                        labelAlign: 'right',
                        labelWidth: 50
                    },
					{
                        xtype: 'textfield',
                        readOnly:true,
                        fieldLabel: 'Months',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'term'
                    }]
                }, {
                    columnWidth: 0.3,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'textfield',
                        readOnly:true,
                        fieldLabel: 'Rate',
                        labelAlign: 'right',
                        labelWidth: 65,
						name: 'rate'
                        //name: 'rateQuoted'
                    },
                    /*{
                        xtype: 'textfield',
                        readOnly:true,
                        fieldLabel: 'Execution',
                        labelAlign: 'right',
                        labelWidth: 65
                    },*/
                    {
                        xtype: 'textfield',
						hidden:true,
                        readOnly:true,
                        fieldLabel: 'Mortgage Type',
                        labelAlign: 'right',
                        labelWidth: 65
                    },/*,
					{
                        xtype: 'textfield',
						readOnly:true,
						fieldLabel: 'Rate Type',
						labelAlign: 'right',
						labelWidth: 65,
						name: 'ratetype'
                    }*/	
                    {
                        xtype: 'textfield',
						readOnly:true,
						fieldLabel: 'Loan Type',
						labelAlign: 'right',
						labelWidth: 65,
						name: 'loantype'
                    }]
                }] 
            }]
        };
        me.callParent(arguments);
    }

});