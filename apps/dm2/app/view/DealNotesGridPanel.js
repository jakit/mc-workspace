Ext.define('DM2.view.DealNotesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealnotesgridpanel',

    requires: [
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],
    
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    
    initComponent: function() {
        var me = this;		
        me.cls = 'gridcommoncls';
        me.title = 'Notes';
        me.columnLines = false;
		
		me.store = Ext.create('DM2.store.Notes');
		
        me.columns = [{
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'noteid',
            text: 'Note id',
            flex: 1
        }, {
            xtype: 'datecolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                return Ext.util.Format.date(record.get('modifiedDateTime'), "m/d/Y h:i:s");
            },
            width: 150,
            //dataIndex: 'modifieddate',
			dataIndex: 'modifiedDateTime',
            text: 'Date/Time'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'content',
            text: 'Content',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 120,
            dataIndex: 'modifiedby',
            text: 'By'
        }, {
            xtype: 'gridcolumn',
            hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 120,
            dataIndex: 'assoctype',
            text: 'Assoc Type'
        }];
        
        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            hidden: true,
            itemId: 'notegridtoolbar',
            items: [
                {
                    xtype: 'button',
                    action: 'addnote',
                    itemId: 'addnotebtn',
                    text: 'Add Note'
                }
            ]
        }];
        
        me.callParent(arguments);
    }    
});