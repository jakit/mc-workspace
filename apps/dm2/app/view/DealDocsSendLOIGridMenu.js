Ext.define('DM2.view.DealDocsSendLOIGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealdocssendloigridmenu',

    requires: [
        'Ext.menu.Item'
    ],

    items: [
        {
            xtype: 'menuitem',
            itemevent: 'addfile',
            hidden: true,
            text: 'Add File',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemevent: 'filetemplate',
            hidden: true,
            text: 'Create file from Template',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemevent: 'filerepository',
            hidden: true,
            text: 'Import file from Repository',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemevent: 'uploaddoc',
            text: 'UploadDoc',
            focusable: true
        }
    ]

});