Ext.define('DM2.view.SendInvoicePanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.sendinvoicepanel',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Date',
        'Ext.form.field.Checkbox',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Number',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.XTemplate'
    ],
	
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.itemId = 'billingpanel';
		me.margin = 3;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'sendinvoicebtn',
					itemId: 'sendinvoicebtn',
					text: 'Send Invoice'
				}
			]
		};
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		var dealContactsSendLOISt = Ext.create('DM2.store.DealContactsSendLOI');
		var dealpropertySt = Ext.create('DM2.store.Dealproperty');
		var contactsEmailsSt = Ext.create('DM2.store.ContactEmails');
		var contactsFaxSt = Ext.create('DM2.store.ContactFax');
		me.items = [
			{
				xtype: 'form',
				itemId: 'contactfrm',
				scrollable: 'true',
				header: false,
				items: [
					{
						xtype: 'fieldset',
						collapsible: true,
						title: 'Select Contact',
						items: [
							{
								xtype: 'combobox',
								anchor: '100%',
								name:'contactid',
								itemId: 'contactcombo',                            
								fieldLabel: 'Contacts',
								labelWidth: 60,
								displayField: 'displayName',
								queryMode: 'local',
								store: dealContactsSendLOISt,
								typeAhead: true,
								valueField: 'contactid'
							},
							{
								xtype: 'panel',
								layout: 'column',
								items: [
									{
										xtype: 'panel',
										columnWidth: 0.5,
										margin: '0 5 0 0',
										layout: {
											type: 'vbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Name',
												labelWidth: 60,
												name: 'displayName',
												readOnly: true
											},
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Company',
												labelWidth: 60,
												name: 'companyName',
												readOnly: true
											},
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Phone',
												labelWidth: 60,
												name: 'officePhone_1',
												readOnly: true
											}
										]
									},
									{
										xtype: 'panel',
										columnWidth: 0.5,
										layout: {
											type: 'vbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Role',
												labelAlign: 'right',
												labelWidth: 50,
												name: 'contactType',
												readOnly: true
											},
											{
												xtype: 'combobox',                                            
												fieldLabel: 'Email',
												labelAlign: 'right',
												labelWidth: 50,
												name: 'email_1',
												displayField: 'email',
												queryMode: 'local',
												store: contactsEmailsSt,//'ContactEmails',
												valueField: 'email'
											},
											{
												xtype: 'combobox',                                            
												fieldLabel: 'Fax',
												labelAlign: 'right',
												labelWidth: 50,
												name: 'faxNumber',
												displayField: 'faxNumber',
												queryMode: 'local',
												store: contactsFaxSt,//'ContactFax',
												valueField: 'faxNumber'
											}
										]
									}
								]
							}
						]
					}
				]
			},
			{
				xtype: 'form',
				itemId: 'propertydetailfrm',
				scrollable: 'true',
				collapsible: true,
				header: false,
				items: [{
					xtype: 'fieldset',
					collapsible: true,
					title: 'Select Property',
					items: [{
							xtype: 'combobox',
							tpl: '<tpl for="."><div class="x-boundlist-item">{street_no} - {street}</div></tpl>',
							anchor: '100%',
							itemId: 'propertycmb',                    
							fieldLabel: 'Property',
							labelWidth: 60,
							displayField: 'street',
							displayTpl: [
								'<tpl for=".">',
								'{street_no} - {street}',
								'</tpl>'
							],
							queryMode: 'local',
							store: dealpropertySt,
							typeAhead: true,
							//valueField: 'idDealxProp'
							valueField: 'idSetup'
							//valueField: 'propertyid'
						},
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									xtype: 'panel',
									columnWidth: 0.5,
									margin: '0 5 0 0',
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									items: [
										{
											xtype: 'textfield',                                    
											fieldLabel: 'No',
											labelWidth: 60,
											name: 'street_no',
											readOnly: true
										},
										{
											xtype: 'textfield',                                    
											fieldLabel: 'City',
											labelWidth: 60,
											name: 'city',
											readOnly: true
										},
										{
											xtype: 'textfield',                                    
											fieldLabel: 'Property Type',
											labelWidth: 60,
											name: 'propertytype',
											readOnly: true
										}
									]
								},
								{
									xtype: 'panel',
									columnWidth: 0.5,
									layout: {
										type: 'vbox',
										align: 'stretch'
									},
									items: [
										{
											xtype: 'textfield',                                    
											fieldLabel: 'Street',
											labelAlign: 'right',
											labelWidth: 50,
											name: 'street',
											readOnly: true
										},
										{
											xtype: 'textfield',                                    
											fieldLabel: 'State',
											labelAlign: 'right',
											labelWidth: 50,
											name: 'state'
										},
										{
											xtype: 'textfield',                                    
											fieldLabel: 'Zip',
											labelAlign: 'right',
											labelWidth: 50,
											name: 'zip'
										}
									]
								}
							]
						}
					]
				}]
			},
			{
				xtype: 'datefield',
				fieldLabel: 'Date For Invoice',
				name: 'invoicedate'
			},
			{
				xtype: 'numberfield',
				hideTrigger:true,
				fieldLabel: 'Purchase Price',
				name: 'purchaseprice'
			},
			{
				xtype: 'numberfield',
				hideTrigger:true,
				fieldLabel: 'Meridian fee amount',
				name: 'merfeeamt'
			}
		];
		me.callParent(arguments);
    }
});