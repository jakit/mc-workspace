Ext.define('DM2.view.MultiPickListGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.multipicklistgrid',

    requires: [
        'Ext.grid.Panel'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.margin = 3;
		me.ui = 'activitypanel';
		me.closable = true;
		me.title = '';
		me.itemId = 'multipicklistgrid';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action:'savebtn',
					margin: '0 10 0 0',
					text: 'Save'
				}
			]
		};
		me.sortableColumns = false;
		me.store = Ext.create('DM2.store.ItemList');
		me.scrollable = true;
		me.columns = [
			{
				xtype: 'gridcolumn',
				flex:1,
				hidden:true,
				dataIndex: 'itemId',
				text: 'ID'
			},
			{
				xtype: 'gridcolumn',
				flex:1,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'itemValue',
				text: 'Name'
			}
		];
		me.selModel = {
			selType: 'checkboxmodel'
		}
		me.callParent(arguments);
    }
});