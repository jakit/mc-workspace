Ext.define('DM2.view.AddEditCompanyView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addeditcompanyview',

    requires: [
        'Ext.button.Button',
        'Ext.form.field.ComboBox'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
	    me.frame = true;
	    me.margin = 3;
		me.bodyPadding = 10;
    	me.ui = 'activitypanel';
		//me.scrollable = true;
	    me.layout = {
			type:'vbox',
			align:'stretch'
		};
    	me.closable = true;
	    me.title = 'Add Company';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					margin: '0 10 0 0',
					action: 'saveCompanyBtn',
					text: 'Save'
				}
			]
		};
		me.items = [{
			xtype:'panel',
			layout : {
				type : 'hbox',
				align: 'stretch'
			},
			items:[{
				xtype: 'panel',
				flex:1,
				margin: '0 5 0 0',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'numberfield',
						hidden:true,
						anchor: '100%',
						fieldLabel: 'idCompany',
						name: 'idCompany'
					},
					{
						xtype: 'textfield',								
						fieldLabel: 'Company',
						labelAlign: 'right',
						labelWidth: 65,
						//readOnly: true,
						name: 'companyName'
					},
					{
						xtype: 'textfield',								
						fieldLabel: 'State',
						labelAlign: 'right',
						labelWidth: 65,
						//readOnly: true,
						name: 'state'
					},
					{
						xtype: 'textfield',								
						fieldLabel: 'Phone 1',
						labelAlign: 'right',
						labelWidth: 65,
						//readOnly: true,
						name: 'phone1'
					},
					{
						xtype: 'textfield',								
						fieldLabel: 'Phone 2',
						labelAlign: 'right',
						labelWidth: 65,
						//readOnly: true,
						name: 'phone2'
					}
				]
			},
			{
				xtype: 'panel',
				flex:1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'textfield',								
						fieldLabel: 'Address',
						labelAlign: 'right',
						labelWidth: 70,
						//readOnly: true,
						name: 'address'
					},
					{
						xtype: 'textfield',								
						fieldLabel: 'City',
						labelAlign: 'right',
						labelWidth: 70,
						//readOnly: true,
						name: 'city'
					},
					{
						xtype: 'textfield',								
						fieldLabel: 'Email 1',
						labelAlign: 'right',
						labelWidth: 70,
						//readOnly: true,
						name: 'email1'
					},
					{
						xtype: 'textfield',								
						fieldLabel: 'Email 2',
						labelAlign: 'right',
						labelWidth: 70,
						//readOnly: true,
						name: 'email2'
					}
				]
			}]    
		}];
		me.callParent(arguments);
    }
});