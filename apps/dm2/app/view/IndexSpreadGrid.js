Ext.define('DM2.view.IndexSpreadGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.indexspreadgrid',

    requires: [
        'Ext.grid.Panel'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.margin = 3;
		me.ui = 'activitypanel';
		me.closable = true;
		me.title = 'Spread Index List';
		me.itemId = 'indexspreadgrid';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action:'saveindexspreadbtn',
					margin: '0 10 0 0',
					text: 'Save'
				}
			]
		};
		me.sortableColumns = false;
		me.store = 'IndexSpread';
		me.scrollable = true;
		me.columns = [
			{
				xtype: 'gridcolumn',
				flex:1,
				hidden:true,
				dataIndex: 'idIndexSpread',
				text: 'idIndexSpread'
			},
			{
				xtype: 'gridcolumn',
				flex:1,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'indexSpreadValue',
				text: 'Index Spread Value'
			}
		];
		me.callParent(arguments);
    }
});