Ext.define('DM2.view.AcrisContactsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.acriscontactsgrid',

    requires: [
		'Ext.form.Panel',
        'Ext.grid.column.Column',
        'Ext.view.Table'
    ],

    store: null,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'gridcommoncls';
        me.title = 'ACRIS Contacts per Document';
		me.store = Ext.create('DM2.store.AcrisContacts');
		me.ui = 'activitypanel';
		me.frame = true;
        me.columnLines = false;
		me.style = "margin:20px 0px 20px 0px;border-width:2px;";
		me.scrollable = true;
		me.collapsible = true;
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			width:60,
            dataIndex: 'partyType',
            text: 'Code'
            //flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'partyRole',
            text: 'Party Role',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'partyName',
            text: 'Name',
            flex: 1
        }];        
        me.dockedItems = [
			{
				xtype: 'form',
				dock:'bottom',
				scrollable: true,
				layout: 'column',
				bodyPadding: 5,
				title:'Selected Contact Details',
				collapsible: true,
				ui: 'activitypanel',
				frame: true,
				items: [
					{
						xtype: 'panel',
						columnWidth: 0.5,
						margin: '0 5 0 0',
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',								
								fieldLabel: 'Address1',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'addressLine1',
								readOnly: true
							},
							{
								xtype: 'textfield',
								fieldLabel: 'State',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'addressState',
								readOnly: true
							},
							{
								xtype: 'textfield',
								fieldLabel: 'Country',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'addressCounty',
								readOnly: true
							}
						]
					},
					{
						xtype: 'panel',
						columnWidth: 0.5,
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',								
								fieldLabel: 'Address2',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'addressLine2',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'City',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'addressCity',
								readOnly: true
							},
							{
								xtype: 'textfield',								
								fieldLabel: 'zipCode',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'addressZip',
								readOnly: true
							}
						]
					}
				]
			}
		];
        me.callParent(arguments);
    }    
});