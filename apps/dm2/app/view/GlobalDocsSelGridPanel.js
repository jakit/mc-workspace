Ext.define('DM2.view.GlobalDocsSelGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.globaldocsselgridpanel',

    requires: [
        'DM2.view.override.GlobalDocsSelGridPanel',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.grid.plugin.DragDrop',
        'Ext.util.Point',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel',
        'Ext.panel.Panel'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.itemId = 'globaldocsselgridpanel';
		me.margin = 3;
		me.ui = 'activitypanel';
		me.closable = true;
		me.title = 'Global Documents';
		me.store = Ext.create('DM2.store.GlobalDocsPerSubmission');
		me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var docurlname = record.get('url');
					var ext = DM2.app.getController('MainController').getExtImg(docurlname);
					return '<a target="_blank" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
				},
				dataIndex: 'name',
				text: 'Name',
				flex: 1
			}
		];
		me.viewConfig = {
			plugins: [
				{
					ptype: 'gridviewdragdrop',
					ddGroup: 'dropsubmissiondoc',
					enableDrag: false
				}
			]
		};
		me.selModel = {
			selType: 'cellmodel'
		};
		me.callParent(arguments);
    }
});