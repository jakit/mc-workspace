Ext.define('DM2.view.LoanSizingDetailsForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loansizingdetailsform',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.field.Number',
        'Ext.form.field.Date',
        'Ext.tab.Panel',
        'Ext.tab.Tab',
        'Ext.tab.Bar'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable :true,
    initComponent: function() {
		var me = this;
    	me.itemId = 'loansizingdetailsform';
    	//me.scrollable = true;
		me.defaults = {
			labelWidth: 120
		};
		me.cls = ['removenumcls'];
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					disabled:true,
					action: 'saveLoanSize',
					margin: '0 10 0 0',
					itemId: 'saveloansizebtn',
					text: 'Save'
				},
				{
					xtype: 'button',
					disabled:true,
					action: 'resetLoanSize',
					itemId: 'resetloansizebtn',
					text: 'Reset'
				},
				{
					xtype: 'button',
					margin: '0 0 0 10',
					action: 'clearLoanSize',
					itemId: 'clearloansizebtn',
					text: 'Clear'
				}
			]
		};
	    me.title = 'Loan Size Info';
		
		var eWalkSt = Ext.create('DM2.store.ElevatorWalk');
		
    	me.items = [
        {
            xtype: 'fieldset',
            margin: '0 5 0 5',
            collapsible: true,
            items: [
                {
                    xtype: 'numberfield',
                    hidden: true,
                    fieldLabel: 'Label',
                    name: 'activityid'
                },
                {
                    xtype: 'numberfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'Propertyid',
                    name: 'idProperty'
                },
                {
                    xtype: 'numberfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'idSetup',
                    name: 'idSetup'
                },
                {
                    xtype: 'currencyfield',
                    anchor: '100%',
                    fieldLabel: 'Loan Size',
                    labelWidth: 80,
                    name: 'estimatedLoanSize'
                },
                {
                    xtype: 'textfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'Dealid',
                    name: 'idDeal'
                },
                {
                    xtype: 'panel',
                    margin: '5 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            flex: 1,
                            margin: '0 10 0 0',
                            fieldLabel: 'Income',
                            labelWidth: 60,
                            name: 'income'
                        },
                        {
                            xtype: 'numberfield',
                            flex: 1,
                            fieldLabel: 'Expenses',
                            labelAlign: 'right',
                            labelWidth: 60,
                            name: 'expenses'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            margin: '0 5 0 5',
            collapsible: true,
            title: 'Property Information',
            items: [
                {
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            flex: 1,
                            margin: '0 10 0 0',
                            fieldLabel: 'No Of Buildings',
                            labelWidth: 90,
                            name: 'numberOfBuildings'
                        },
                        {
                            xtype: 'numberfield',
                            flex: 1,
                            fieldLabel: 'No Of Stories',
                            labelWidth: 80,
                            name: 'numberOfStories'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    margin: '5 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            flex: 1,
                            fieldLabel: 'Purchase Price',
                            labelWidth: 90,
                            name: 'purchasePrice'
                        },
                        {
                            xtype: 'numberfield',
                            flex: 1,
                            fieldLabel: 'Percent Sold',
                            labelAlign: 'right',
                            labelWidth: 80,
                            name: 'percentSold'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    margin: '5 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            flex: 1,
                            fieldLabel: 'Percent Rented',
                            labelWidth: 90,
                            name: 'percentRented'
                        },
						{
                            xtype: 'numberfield',
                            flex: 1,
                            fieldLabel: 'Vacancy Amount',
                            labelWidth: 90,
                            name: 'vacancyAmt'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            margin: '0 5 0 5',
            collapsible: true,
            title: 'Assessed Value',
            items: [
                {
                    xtype: 'panel',
                    margin: '5 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            flex: 0.9,
                            fieldLabel: 'Act.Value',
                            labelWidth: 70,
                            name: 'assessedValue'
                        },
                        {
                            xtype: 'datefield',
							format: 'm/d/Y',
                            flex: 1.1,
                            fieldLabel: 'Year Assessed',
                            labelAlign: 'right',
                            name: 'assessedValueYear'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    margin: '5 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'center'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            flex: 0.9,
                            fieldLabel: 'Trans.Value',
                            labelWidth: 70,
                            name: 'transactionValue'
                        },
                        {
                            xtype: 'datefield',
							format: 'm/d/Y',
                            flex: 1.1,
                            fieldLabel: 'Year Acquired',
                            labelAlign: 'right',
                            name: 'yearAcquired'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            margin: '0 5 0 5',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'fieldset',
                    flex: 1,
                    collapsible: true,
                    title: 'Resi. Rent Roll',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Units',
                            labelWidth: 60,
                            name: 'numberOfApartments'
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Rooms',
                            labelWidth: 60,
                            name: 'numberOfRooms'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    flex: 1,
                    margin: '0 0 0 5',
                    collapsible: true,
                    title: 'Comm.Rent Roll',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Units',
                            labelWidth: 60,
                            name: 'numberOfCommercialApartments'
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Sqr. Ft.',
                            labelWidth: 60,
                            name: 'commercialSquareFeet'
                        }
                    ]
                }
            ]
        },
		{
			xtype: 'combobox',
			autoSelect : true,
			selectOnFocus: true,
			displayField: 'desc',
			queryMode: 'local',
			store: eWalkSt,
			typeAhead: true,
			valueField: 'id',
			fieldLabel: 'Elev.Walk',
			labelWidth: 70,
			name: 'elevatorWalk',
			anchor:'100%',
			margin: '5 5 5 5'
		},
        {
            xtype: 'fieldset',
			hidden:true,
            margin: '0 5 0 5',
            collapsible: true,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    flex: 1,
                    margin: '0 5 0 0',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'datefield',                            
                            fieldLabel: 'Env.Date',
                            labelWidth: 70,
                            name: 'environmentalDate'
                        },
                        {
                            xtype: 'datefield',                            
                            fieldLabel: 'AppraisalDate',
                            labelWidth: 80,
                            name: 'appraisalDate'
                        },
                        {
                            xtype: 'textfield',                            
                            fieldLabel: 'Comm. No.',
                            labelWidth: 70,
                            name: 'commitmentNumber'
                        },
                        {
                            xtype: 'textfield',                            
                            fieldLabel: 'Comm. Check.',
                            labelWidth: 80,
                            name: 'commitmentCheck'
                        },
                        {
                            xtype: 'numberfield',                            
                            fieldLabel: 'CommChckAmount',
                            labelWidth: 80,
                            name: 'commitmentCheckAmount'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'datefield',                            
                            fieldLabel: 'ComExpiDate',
                            labelWidth: 70,
                            name: 'commitExpirationDate'
                        },
                        {
                            xtype: 'numberfield',                            
                            fieldLabel: 'Appraiser',
                            labelWidth: 70,
                            name: 'appraiser'
                        },
                        {
                            xtype: 'datefield',                            
                            fieldLabel: 'Comm.Date',
                            labelWidth: 70,
                            name: 'commitmentDate'
                        },
                        {
                            xtype: 'datefield',                            
                            fieldLabel: 'CommCheckDate',
                            labelWidth: 70,
                            name: 'commitmentCheckDate'
                        }
                    ]
                }
            ]
        }
    ];
		me.callParent(arguments);
    }
});