Ext.define('DM2.view.LoanScheduleMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.loanschedulemenu',
	requires: [
        'Ext.menu.Item'
    ],

    width: 180,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'editrateschedule',
			action:'editrateschedule',
            text: 'Edit Rate Schedule',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'deleterateschedule',
			action:'deleterateschedule',
            text: 'Delete Rate Schedule',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
            itemId: 'deleteallrateschedule',
			action: 'deleteallrateschedule',
            text: 'Delete All Rate Schedule',
            focusable: true,
			modifyMenu : 'yes'
        }
    ]
});