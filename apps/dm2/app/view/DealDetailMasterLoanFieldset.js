Ext.define('DM2.view.DealDetailMasterLoanFieldset', {
    extend: 'DM2.view.component.container.GridToForm',
    alias: 'widget.dealdetailmasterloanfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool',
        'DM2.model.MasterLoan',
        'Ext.data.proxy.Rest',
		'DM2.view.CurrencyField'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        //me.frame = true;
        me.itemId = 'dealdetailmasterloanfieldset';
        //me.ui = 'masterloandealpanel';      
        me.detailOnSingleRecord = true;
        // me.contextMenu = Ext.create('DM2.view.DealDetailPropertiesMenu');
 
        // *************
        
        var store = Ext.create('Ext.data.Store', {
            model: 'DM2.model.MasterLoan',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_masterLoansPerDeal'
            },
            autoLoad: false     
        });
        
        // *************** 
        
        me.listLayout= {
            icon: 'resources/images/icons/loan_m.png',
            title: "MASTER LOAN",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailmasterloangridpanel'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            // store: 'MasterLoans',
            store: store,
			ui : 'masterloandealpanel', 
			frame:true,
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'DealMakerBankName',
                text: 'Bank',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'FeedBankName',
                text: 'FeedBankName',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                dataIndex: 'MortgageAmount',
                text: 'Amount',
                flex: 1,
				align:'right'
            },
            {
                xtype: 'numbercolumn',
                hidden: true,
                dataIndex: 'PropertyTax',
                text: 'PropTax',
                flex: 1
            },
            {
                xtype: 'datecolumn',
                hidden: true,
                dataIndex: 'SaleDate',
                text: 'SaleDate',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                hidden: true,
                dataIndex: 'SalePrice',
                text: 'SalePrice',
                flex: 1,
				align:'right'
            },
            {
                xtype: 'numbercolumn',
				hidden:true,
                dataIndex: 'CurrentAVTAssess',
                text: 'Assessed Value',
                flex: 1,
				align:'right'
            },
            {
                xtype: 'numbercolumn',
				hidden:true,
                dataIndex: 'FullValue',
                text: 'Full Value',
                flex: 1,
				align:'right'
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'Owner',
                text: 'OwnerName',
                flex: 1
            },
			/*{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'DealMakerBankName',
                text: 'Bank',
                flex: 1
            },*/
            {
                xtype: 'datecolumn',
				format: 'm-d-Y',
                //hidden: true,
                dataIndex: 'LastKnownMortgageDate',
                text: 'LastKnownMtgDate',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'PropertyMasterCodeSource',
                text: 'PropCodeSource',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/loan_m.png',
            title: "MASTER LOAN",     
            scrollable: true,
			ui : 'masterloandealpanel', 
			frame:true,
            items: [{
                xtype: 'container',
                layout: {
                    type: 'column'
                },
                items: [{
                    columnWidth: 0.34,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
					defaults: {
                        anchor: '100%'
                    },
                    items: [{
                        xtype: 'textfield',                        
                        fieldLabel: 'Bank',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'BankName',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
						hidden:true,
                        fieldLabel: 'Feed Bank',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'FeedBankName',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Owner',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'Owner',
                        readOnly: true
                    },
                    {
                        xtype: 'currencyfield',
						hidden:true,
                        fieldLabel: 'Assessed Value',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'CurrentAVTAssess',
                        readOnly: true
                    },
					{
                        xtype: 'textfield',                        
                        fieldLabel: 'Building Class',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'BuildingClassDescription',
                        readOnly: true
                    }]
                }, {
                    columnWidth: 0.34,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
					defaults: {
                        anchor: '100%'
                    },
                    items: [{
                        xtype: 'numberfield',
						hidden:true,
                        fieldLabel: 'Master ID',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'masterID',
                        readOnly: true
                    },          
                    {
                        xtype: 'currencyfield',                        
                        fieldLabel: 'Mortgage Amount',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'MortgageAmount',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Property Tax',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'PropertyTax',
                        readOnly: true
                    },
                    {
                        xtype: 'currencyfield',
						hidden:true,
                        fieldLabel: 'Full Value',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'FullValue',
                        readOnly: true
                    },
					{
                        xtype: 'textfield',                        
                        fieldLabel: 'Property Code Source',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'PropertyMasterCodeSource',
                        readOnly: true
                    }]
                },{
                    columnWidth: 0.32,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%'
                    },
                    items: [{
                        xtype: 'datefield',                        
                        fieldLabel: 'Sale Date',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'SaleDate',
                        readOnly: true
                    },                   
                    {
                        xtype: 'currencyfield',                        
                        fieldLabel: 'Sale Price',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'SalePrice',
                        readOnly: true
                    },                    
					{
                        xtype: 'datefield',                        
                        fieldLabel: 'Last Mortgage Date',
                        labelAlign: 'right',
                        labelWidth: 90,
                        name: 'LastKnownMortgageDate',
                        readOnly: true
                    }]
                }]
            }]
        };
        me.callParent(arguments);
    }

});