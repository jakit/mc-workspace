Ext.define('DM2.view.GenFilesPerSubmissionGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.genfilespersubmissiongridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.itemId = 'genfilespersubmissiongridpanel';
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.title = 'Generated File';
		me.store = Ext.create('DM2.store.GenFilesPerSubmission');//'GenFilesPerSubmission';

	    me.columns = [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                var docurlname = record.get('url');
                var ext = DM2.app.getController('MainController').getExtImg(docurlname);
                return '<a target="_blank" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
            },
            dataIndex: 'name',
            text: 'Document Name',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            dataIndex: 'size',
            text: 'Size',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'desc',
            text: 'Description',
            flex: 1
        }
    ];
		me.callParent(arguments);
    }
});