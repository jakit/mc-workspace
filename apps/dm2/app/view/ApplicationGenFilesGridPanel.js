Ext.define('DM2.view.ApplicationGenFilesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.applicationgenfilesgridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table'
    ],

    cls: 'gridcommoncls',
    frame: true,
    itemId: 'genfilespersubmissiongridpanel',
    ui: 'activitypanel',
    collapsible: true,
    title: 'Generated File',
    store: 'GenFilesPerSubmission',

    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                var docurlname = record.get('url');
                var ext = /^.+\.([^.]+)$/.exec(docurlname);
                if(ext === null){
                    ext = "";
                }else {
                    ext = ext[1];
                }
                return '<a target="_blank" href="'+record.get('url')+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
            },
            dataIndex: 'name',
            text: 'Document Name',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            dataIndex: 'size',
            text: 'Size',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'desc',
            text: 'Description',
            flex: 1
        }
    ]
});