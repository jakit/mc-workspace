Ext.define('DM2.view.ContactNoteOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.contactnoteoptionmenu',

    requires: [
        'Ext.menu.Item'
    ],

    viewModel: {
        type: 'contactnoteoptionmenu'
    },
    width: 120,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'noteedit',
            text: 'Edit Note',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'noteadd',
            text: 'Add Note',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			hidden: true,
            itemId: 'notehistory',
            text: 'Note History',
            focusable: true,
			modifyMenu : 'no'
        }
    ]

});