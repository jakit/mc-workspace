Ext.define('DM2.view.DealDetailUserGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealdetailusergridpanel',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Date',
        'Ext.grid.column.Check',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'DM2.model.DealUser',
        'Ext.data.proxy.Rest'
    ],
    
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);        
    },
    scrollable: true,
    initComponent: function() {
        var me = this;
        me.cls = 'gridcommoncls';
        
        var detailPanel, dealid, mainViewport;        
        
		me.store = Ext.create('DM2.store.DealUsers');
		
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'user',
            text: 'USER',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'role',
            text: 'ROLE',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assigner',
            text: 'Assigner',
            flex: 1
        }, {
            xtype: 'datecolumn',
            dataIndex: 'assigndate',
            text: 'Assigndate',
            flex: 1,
            format: 'm-d-Y'
        },{
            xtype: 'datecolumn',
            dataIndex: 'unassigndate',
            text: 'UnAssigndate',
            flex: 1,
            format: 'm-d-Y'
        }, {
            xtype: 'checkcolumn',
            itemId: 'activecheck',
            dataIndex: 'active',
            text: 'Active'
        }];
        
        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            itemId: 'dealusergridtoolbar',
            items: [
                {
                    xtype: 'button',
                    itemId: 'add',
                    iconCls: 'add',
                    text: 'Add'
                }
            ]
        }];
        
        me.callParent(arguments);
    }
});