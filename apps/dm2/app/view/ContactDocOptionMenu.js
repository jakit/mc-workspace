Ext.define('DM2.view.ContactDocOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.contactdocoptionmenu',

    requires: [
        'Ext.menu.Item',
        'Ext.form.field.File',
        'Ext.form.field.FileButton'
    ],

    width: 160,

    items: [
        {
			xtype: 'menuitem',
            itemId: 'cntdocadd',
            text: 'Add Document',
			focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'cntdocedit',
            text: 'Edit Document',
            focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'cntdocdelete',
            text: 'Delete Document',
			focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			hidden:true,
            itemId: 'cntdochistory',
            text: 'Doc History',
            focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'emaildocument',
            text: 'Email Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'faxdocument',
            text: 'Fax Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'printdocument',
            text: 'Print Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'downloaddocument',
            text: 'Download Document',
            focusable: true,
			modifyMenu : 'no'
        }
    ]
});