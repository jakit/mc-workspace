Ext.define('DM2.view.override.PropertyDocsGridPanel', {
    override: 'DM2.view.property.PropertyDocsGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});