Ext.define('DM2.view.override.BankDocsGridPanel', {
    override: 'DM2.view.BankDocsGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});