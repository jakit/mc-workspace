Ext.define('DM2.view.override.ContactDocsGridPanel', {
    override: 'DM2.view.contact.ContactDocsGridPanel',
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});