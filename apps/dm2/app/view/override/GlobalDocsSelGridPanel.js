Ext.define('DM2.view.override.GlobalDocsSelGridPanel', {
    override: 'DM2.view.GlobalDocsSelGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});