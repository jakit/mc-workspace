Ext.define('DM2.view.override.GeneralAddDealDocPanel', {
    override: 'DM2.view.GeneralAddDealDocPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});