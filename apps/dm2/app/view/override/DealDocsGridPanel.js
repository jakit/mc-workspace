Ext.define('DM2.view.override.DealDocsGridPanel', {
    override: 'DM2.view.DealDocsGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});