Ext.define('DM2.view.override.DealDocsSendLOIGridPanel', {
    override: 'DM2.view.DealDocsSendLOIGridPanel',

    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});