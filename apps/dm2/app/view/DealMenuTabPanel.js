Ext.define('DM2.view.DealMenuTabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.dealmenutabpanel',
    requires: [
        'DM2.view.DealNotesGridPanel',
        'DM2.view.DealDocsGridPanel',
        'DM2.view.DealContactsGridPanel',
        'DM2.view.DealInteractionsGridPanel',
        'DM2.view.DealDetailUserGridPanel',
		'DM2.view.DealHistoryGrid',
        'DM2.view.ResearchPanel',
        'Ext.grid.Panel',
        'Ext.tab.Tab',
        'Ext.tab.Bar'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    dealHistoryStore: null,
    initComponent: function() {
        var me = this;
        me.activeTab = 0;
		//me.ui = 'submenutabpanel';
        me.items = [{
            xtype: 'dealnotesgridpanel',
            iconCls: 'notebtn'
        }, {
            xtype: 'dealdocsgridpanel',
			itemId:'dealdocsgridpanel',
            iconCls: 'docbtn'
            /*tabConfig: {
                hidden: true
            }*/
        }, {
            xtype: 'dealcontactsgridpanel',
            iconCls: 'contactbtn'
        }, {
            xtype: 'dealinteractionsgridpanel',
            hidden: true
        }, {
            xtype: 'dealdetailusergridpanel',
            iconCls: 'userbtn',
            title: 'Users',
            tabConfig: {
                hidden: true
            }
        }, {
            xtype: 'panel',
            hidden: true,
            title: 'Tasks'
        }, {
            xtype: 'dealhistorygrid',
            itemId:'dealhistorygrid',
            iconCls: 'loanbtn',
            title: 'Deal History',
            tabConfig: {
                hidden: true
            },
            store: me.dealHistoryStore
        }/*, {
            xtype: 'researchpanel',
            iconCls: 'researchbtn'
        }*/];

        me.callParent(arguments);        
    },
    tabBar: {
        xtype: 'tabbar',
        ui: 'submenutabpanel'
    }

});