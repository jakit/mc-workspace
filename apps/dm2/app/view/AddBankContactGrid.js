Ext.define('DM2.view.AddBankContactGrid', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    xtype: 'addbankcontactgrid',
    requires: ['DM2.view.component.grid.SelectionGridNew'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
        me.store = Ext.create('DM2.store.Contactallbuff');

		me.columns = [{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'name',
			text: 'Name',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'companyName',
			text: 'Company',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'title',
			text: 'Title',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			hidden: true,
			dataIndex: 'officePhone_1',
			text: 'Phone',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			hidden: true,
			dataIndex: 'email_1',
			text: 'Email',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}];       
        me.callParent(arguments);
    }
});