Ext.define('DM2.view.BankMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.bankmenu',

    requires: [
        'Ext.menu.Item'
    ],

    width: 160,

    items: [
        {
            xtype: 'menuitem',
            text: 'Add Bank',
            focusable: true
        },
        {
            xtype: 'menuitem',
			hidden: true,
            text: 'Remove Bank',
            focusable: true
        }
    ]
});