Ext.define('DM2.view.DocOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.docoptionmenu',

    requires: [
        'Ext.menu.Item',
        'Ext.form.field.File',
        'Ext.form.field.FileButton'
    ],

    width: 160,

    items: [
		{
            xtype: 'menuitem',
            itemId: 'detaildocadd',
            text: 'Add Document',
			focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
            itemId: 'docedit',
            text: 'Edit Document',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
            itemId: 'deletedoc',
            text: 'Delete Document',
			focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			hidden:true,
            itemId: 'dochistory',
            text: 'Doc History',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'emaildocument',
            text: 'Email Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'faxdocument',
            text: 'Fax Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'printdocument',
            text: 'Print Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'downloaddocument',
            text: 'Download Document',
            focusable: true,
			modifyMenu : 'no'
        }	
    ]

});