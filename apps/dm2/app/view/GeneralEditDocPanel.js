Ext.define('DM2.view.GeneralEditDocPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.generaleditdocpanel',

    requires: [
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.form.FieldSet',
        'Ext.form.field.TextArea',
        'Ext.form.field.ComboBox',
        'Ext.button.Split',
        'Ext.menu.Menu',
        'Ext.form.field.File',
        'Ext.form.field.FileButton',
        'Ext.menu.Item'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent : function() {
        var me = this;
	    me.frame = true;
	    me.margin = 3;
	    //scrollable: true,
	    me.ui = 'activitypanel';
    	me.bodyPadding = 5;
	    me.closable = true;
    	me.title = 'Edit Document';
		
		var DocsTempStore = Ext.create('DM2.store.DocsTemp');
		var ClassificationsStore = Ext.create('DM2.store.Classifications');
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					//hidden:true,
					margin:'0 10 0 0',
					action: 'docsavebtn',
					text: 'Save'
				}
			]
		};

		me.items = [{
			xtype: 'gridpanel',
			cls: 'gridcommoncls',
			frame: true,
			ui: 'activitypanel',
			store: DocsTempStore,//'DocsTemp',
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'name',
					text: 'Name',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					width: 65,
					dataIndex: 'size',
					text: 'Size'
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'desc',
					text: 'Description',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'clsfctn',
					text: 'Classification',
					flex: 1
				}
			]
		},
		{
			xtype: 'panel',
			items: [
				{
					xtype: 'fieldset',
					collapsible: true,
					title: 'Document Details',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'panel',
							margin: '0 0 5 0',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							items: [
								{
									xtype: 'numberfield',
									hidden:true,
									fieldLabel: 'Document ID',
									labelWidth: 80,
									name: 'docid'
								},
								{
									xtype: 'textfield',
									hidden:true,
									fieldLabel: 'Ext',
									labelWidth: 80,
									name: 'ext'
								},
								{
									xtype: 'textfield',
									readOnly: true,
									flex: 1.2,
									fieldLabel: 'Name',
									labelWidth: 80,
									name: 'name',
									enableKeyEvents: true,
									allowBlank: false
								},
								{
									xtype: 'textfield',
									flex: 0.8,
									fieldLabel: 'Size (kb)',
									labelAlign: 'right',
									name: 'docfilesize',
									readOnly: true
								}
							]
						},
						{
							xtype: 'textareafield',
							fieldLabel: 'Description',
							labelWidth: 80,
							name: 'desc',
							enableKeyEvents: true
						},
						{
							xtype: 'combobox',
							fieldLabel: 'Classification',
							labelWidth: 80,
							name: 'clsfctn',
							enableKeyEvents: true,
							queryMode: 'local',
							store: ClassificationsStore,
							valueField: 'selectionDesc',
							displayField: 'selectionDesc'
						}
					]
				}
			]
		}];
		me.callParent(arguments);
    }
});