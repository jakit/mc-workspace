Ext.define('DM2.view.UpperTextField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.uppertextfield',

    //configuration
    config: {
        uppercaseValue: true //defaults to true
    },
	
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		Ext.apply(me, {
            fieldStyle: 'text-transform:uppercase'
        });
		me.callParent(arguments);
    },

    //overriden function
    getValue: function () {
        var val = this.callParent();
        return this.getUppercaseValue() ? val.toUpperCase() : val;
    }
});