Ext.define('DM2.view.PropertyGmapWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.propertygmapwindow',

    requires: [
        'Ext.container.Container'
    ],

    viewModel: {
        type: 'propertygmapwindow'
    },
    height: 500,
    width: 750,
    layout: 'fit',
    title: 'Property Location',

    items: [
        {
            xtype: 'container',
            itemId: 'gmapContiner'
        }
    ]

});