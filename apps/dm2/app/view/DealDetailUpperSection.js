Ext.define('DM2.view.DealDetailUpperSection', {
    extend: 'Ext.form.Panel',
    alias: 'widget.dealdetailuppersection',

    requires: [
        'Ext.panel.Panel',
        'Ext.form.field.Date',
		'DM2.view.CurrencyField',
		'DM2.view.DealDetailOptionMenu'
    ],

	dealPrimaryDetailStore : null,
	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.frame = true;
		me.itemId = 'dealdetailuppersection';
		me.margin = '0 0 3px 0px';
		me.ui = 'dealdetailinfopanel';
		me.layout = 'column';
		me.bodyPadding = '5 5 0 5';
		me.contextMenu = Ext.create('DM2.view.DealDetailOptionMenu');
 		me.dealPrimaryDetailStore = Ext.create('DM2.store.DealPrimaryDetails');

		me.items = [
			{
				xtype: 'panel',
				columnWidth: 0.37,
				ui: 'dealdetailinfosubpanel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'panel',
						margin: '0px 0px 6px 0px',
						ui: 'dealdetailinfosubpanel',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'DM-Deal #',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'dealid',
								readOnly: true
							},
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'File #',
								labelAlign: 'right',
								labelWidth: 45,
								name: 'filenumber',
								readOnly: true
							}
						]
					},
					{
						xtype: 'panel',
						itemId:'dealidnamepanel',
						margin: '0px 0px 6px 0px',
						ui: 'dealdetailinfosubpanel',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'LT-Deal #',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'magicDealID',
								readOnly: true
							}/*,
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'Name',
								labelAlign: 'right',
								labelWidth: 45,
								name: 'dealname',
								readOnly: true
							}*/
						]
					},
					/*{
						xtype: 'textfield',
						fieldLabel: 'Name',
						labelAlign: 'right',
						labelWidth: 55,
						name: 'dealname',
						readOnly: true
					},*/
					{
						xtype: 'panel',
						margin: '0px 0px 6px 0px',
						ui: 'dealdetailinfosubpanel',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								//xtype: 'textfield',						
								xtype: 'currencyfield',
								flex: 1,
								fieldLabel: 'Amount',
								labelAlign: 'right',
								labelWidth: 65,
								name: 'amount',
								readOnly: true
							},
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'Status Set Mode',
								labelAlign: 'right',
								labelWidth: 110,
								name: 'statusSetMode',
								readOnly: true
							}
						]
					}
				]
			},
			{
				xtype: 'panel',
				columnWidth: 0.28,
				ui: 'dealdetailinfosubpanel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Status',
						labelAlign: 'right',
						labelWidth: 90,
						name: 'status',
						readOnly: true
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Status Set By',
						labelAlign: 'right',
						labelWidth: 90,
						name: 'statusSetBy',
						readOnly: true
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Status Date',
						labelAlign: 'right',
						labelWidth: 90,
						name: 'statusDate',
						readOnly: true
					},
					{
						xtype: 'combobox',
						store : [['S','Sell'],['B','Buy'],['C','Consulting'],['O','OffMarket']],
						queryMode: 'local',
						fieldLabel: 'Sale Type',
						labelAlign: 'right',
						labelWidth: 90,
						name: 'saleType',
						readOnly: true
					}
				]
			},
			{
				xtype: 'panel',
				columnWidth: 0.35,
				ui: 'dealdetailinfosubpanel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'panel',
						margin: '0px 0px 6px 0px',
						ui: 'dealdetailinfosubpanel',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'Office',
								labelAlign: 'right',
								labelWidth: 92,
								name: 'office',
								readOnly: true
							},
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'Prm. Brok.',
								labelAlign: 'right',
								labelWidth: 70,
								name: 'PrimaryBroker',
								readOnly: true
							}
						]
					},
					{
						xtype:'panel',
						margin: '0px 0px 6px 0px',
						ui: 'dealdetailinfosubpanel',
						layout: {
							type:'hbox',
							align:'stretch'
						},
						items:[{
						   xtype: "label",
						   itemId:'csrlabel',
						   style:'text-align:right;color: #666;text-decoration: underline;',
						   html: "CSRs: ",
						   width:97,
						   padding: '2px 5px 0px 0px'
						},
						{
							xtype: 'textfield',
							flex:1,
							hideLabel:true,
							fieldLabel: 'CSRs',
							labelAlign: 'right',
							labelWidth: 92,
							name: 'userList',
							readOnly: true
						}]
					},
					
					{
						xtype: 'textfield',
						fieldLabel: 'Mortgage Type',
						labelAlign: 'right',
						labelWidth: 92,
						name: 'mtgtype',
						readOnly: true
					}
				]
			}
		];
		me.callParent(arguments);
    }
});