Ext.define('DM2.view.SearchTrigger', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.searchtrigger',

    requires: [
    ],

    config: {
        operator: 'like',
        _operator: 'like',
        action: 'searchline',
        listeners: {
            render: function(){
                var me = this;
                me.ownerCt.on('resize', function(){
                me.setWidth(this.getEl().getWidth());
                });
            },
            change: function() {
                if(this.autoSearch) {
                    this.setFilter(this.up().dataIndex, this.getValue());
                }
            },
            show: function(cmp){
                cmp.setWidth((cmp.ownerCt.getEl().getWidth()+1));
            }
        }
    },

    hidden: true,
    style: 'margin-bottom: 0px;',

    setFilter: function(filterId, value) {
        var store = this.up('grid').getStore();
        if(value){
            store.removeFilter(filterId, false);
            var filter = {
                id: filterId,
                property: filterId,
                value: value
            };
            if(this.anyMatch) {
                filter.anyMatch = this.anyMatch;
            }
            if(this.caseSensitive) {
                filter.caseSensitive = this.caseSensitive;
            }
            if(this.exactMatch) {
                filter.exactMatch = this.exactMatch;
            }
            if(this.operator) {
                filter.operator = this.operator;
            }
            console.log(filter);
            store.addFilter(filter);
        } else {
            console.log("Clear blank");
            store.filters.removeAtKey(filterId);
            //store.reload();
        }
    }

});