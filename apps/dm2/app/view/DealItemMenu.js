Ext.define('DM2.view.DealItemMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealitemmenu',

    requires: [
        'Ext.menu.Item'
    ],

    items: [
        {
            xtype: 'menuitem',
            itemId: 'opendeal',
            text: 'Open',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'printdeal',
            text: 'Print',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'createnewdeal',
            text: 'Create New Deal',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
            itemId: 'refreshdeal',
            text: 'Refresh Deals',
            focusable: true,
			modifyMenu : 'no'
        }
    ]

});