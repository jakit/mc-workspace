Ext.define('DM2.view.CreateNewDealContactGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.createnewdealcontactgrid',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Date',
        'Ext.selection.CheckboxModel',
		'DM2.view.CreateNewDealContactGridMenu'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.height = 170;
		me.maxHeight = 175;
        me.minHeight = 100;
		me.itemId = 'createnewdealcontactgrid';
		me.margin = '5 0 0 0';
		me.scrollable = true;
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.title = 'Contacts';
		me.columnLines = true;
        me.multiColumnSort = true;
        me.sortableColumns = false;
		
		me.contextMenu = Ext.create('DM2.view.CreateNewDealContactGridMenu');
		
		me.store = Ext.create('DM2.store.DealDetailContacts');
		
		me.columns = [
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'fullName',
                    text: 'Name',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'contactType',
                    text: 'Role',
                    flex: 1,
					itemId : "cntType",
					editor: {						
						xtype: 'combobox',
						displayField: 'type',
						queryMode: 'local',
						store: 'ContactTypes',
						valueField: 'type'
					}
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'officePhone_1',
                    text: 'Phone',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'email_1',
                    text: 'Email',
                    flex: 1
                }
            ];
		me.selModel = {
			selType: 'checkboxmodel'
		};
		me.plugins  = [
			{
				ptype: 'cellediting',
				clicksToEdit: 1/*,
				listeners: {
					beforeedit: 'onCellEditingBeforeEdit'
				}*/
			}
		];
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						action: 'showaddnewdealcontactbtn',
						iconCls: 'add',
						text: 'Add Contact'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});