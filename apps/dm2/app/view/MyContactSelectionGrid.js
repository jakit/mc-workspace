Ext.define('DM2.view.MyContactSelectionGrid', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    xtype: 'mycontactselectiongrid',
    requires: ['DM2.view.component.grid.SelectionGridNew',
			   'Ext.selection.CheckboxModel'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        //me.header = false;
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					disabled:true,
					action: 'savemycontactbtn',
					itemId: 'savemycontactbtn',
					text: 'Save'
				}
			]
		};
		me.frame = true;
		me.closable = true;
		me.margin = 3;
		me.ui = 'activitypanel';
        me.itemId = 'mycontactselectiongrid';
		me.title = 'Add To My Contact';
        me.store = Ext.create('DM2.store.ContactAllMySel');
		me.selModel = {
			selType: 'checkboxmodel'
		};
		me.columns = [{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'firstName',
			text: 'FirstName',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'lastName',
			text: 'LastName',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'companyName',
			text: 'Company',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'title',
			text: 'Title',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			hidden: true,
			dataIndex: 'phone',
			text: 'Phone',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			hidden: true,
			dataIndex: 'email',
			text: 'Email',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}];       
        me.callParent(arguments);
    }
});