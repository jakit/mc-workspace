Ext.define('DM2.view.GeneralAddDealDocPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.generaladddealdocpanel',

    requires: [
        'DM2.view.override.GeneralAddDealDocPanel',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.form.FieldSet',
        'Ext.form.field.TextArea',
        'Ext.form.field.ComboBox',
        'Ext.button.Split',
        'Ext.menu.Menu',
        'Ext.form.field.File',
        'Ext.form.field.FileButton',
        'Ext.menu.Item'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent : function() {
        var me = this;
	    me.frame = true;
	    me.margin = 3;
	    //scrollable: true,
	    me.ui = 'activitypanel';
    	me.bodyPadding = 5;
	    me.closable = true;
    	me.title = 'Add Document';
		
		var DocsTempStore = Ext.create('DM2.store.DocsTemp');
		var ClassificationsStore = Ext.create('DM2.store.Classifications');
		
		me.items = [
		{
			xtype: 'gridpanel',
			cls: 'gridcommoncls',
			frame: true,
			ui: 'activitypanel',
			store: DocsTempStore,//'DocsTemp',
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'name',
					text: 'Name',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					width: 65,
					dataIndex: 'size',
					text: 'Size'
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'desc',
					text: 'Description',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'clsfctn',
					text: 'Classification',
					flex: 1
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'bottom',
					items: [
						{
							xtype: 'component',
							flex: 1
						},
						{
							xtype: 'button',
							disabled:true,
							ui : 'default',
							itemId: 'dealdocclearallbtn',
							text: 'Clear'
						},
						{
							xtype: 'button',
							disabled:true,
							ui : 'default',
							itemId: 'dealdocuploadallbtn',
							text: 'Upload'
						}
					]
				}
			]
		},
		{
			xtype: 'panel',
			items: [
				{
					xtype: 'fieldset',
					collapsible: true,
					title: 'Document Details',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [
						{
							xtype: 'panel',
							margin: '0 0 5 0',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							items: [
								{
									xtype: 'textfield',
									hidden:true,
									fieldLabel: 'Ext',
									labelWidth: 80,
									name: 'ext'
								},
								{
									xtype: 'textfield',
									flex: 1.2,
									fieldLabel: 'Name',
									labelWidth: 80,
									name: 'name',
									enableKeyEvents: true,
									allowBlank: false
								},
								{
									xtype: 'textfield',
									flex: 0.8,
									fieldLabel: 'Size (kb)',
									labelAlign: 'right',
									name: 'docfilesize',
									readOnly: true
								}
							]
						},
						{
							xtype: 'textareafield',
							fieldLabel: 'Description',
							labelWidth: 80,
							name: 'desc',
							enableKeyEvents: true
						},
						{
							xtype: 'combobox',
							fieldLabel: 'Classification',
							labelWidth: 80,
							name: 'clsfctn',
							enableKeyEvents: true,
							queryMode: 'local',
							store: ClassificationsStore,
							valueField: 'selectionDesc',
							displayField: 'selectionDesc'
						}
					]
				}
			]
		}
	];
    	me.dockedItems = [
        {
            xtype: 'toolbar',
            dock: 'top',
            itemId: 'selectfiletoolbar',
            items: [
                {
                    xtype: 'splitbutton',
					ui : 'default',
                    text: 'Select',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                xtype: 'filefield',
                                itemevent: 'addfile',
                                itemId: 'selectdocfiletoolbar',
                                margin: '0 0 0 19px',
                                ui: 'cstfilefieldui',
                                buttonOnly: true,
                                buttonText: 'Add File',
                                buttonConfig: {
                                    xtype: 'filebutton',
                                    ui: 'filefieldbtnui-small',
                                    text: 'Add File'
                                }
                            },
                            {
                                xtype: 'menuitem',
                                itemevent: 'filetemplate',
                                text: 'Create file from Template',
                                focusable: true
                            },
                            {
                                xtype: 'menuitem',
                                itemevent: 'filerepository',
                                text: 'Import file from Repository',
                                focusable: true
                            }
                        ]
                    }
                }
            ]
        }
    ];
		me.callParent(arguments);
    }
});