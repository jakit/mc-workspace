Ext.define('DM2.view.DefaultWorkAreaPanel', {
    extend : 'Ext.panel.Panel',
    alias : 'widget.defaultworkareapanel',

    requires : [
    	'DM2.view.DealDetailActivityMenu',
		'Ext.form.field.ComboBox',
		'Ext.menu.Menu',
		'DM2.model.ActivityHistory',
		'DM2.model.Activity',
		'Ext.data.proxy.Rest',
		'Ext.util.Sorter'
	],

    activityHistoryStore : null,
    constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;

        me.frame = true;
        // itemId: 'defaultworkareapanel',
        me.margin = "3";
        me.ui = 'activitypanel';
        me.bodyPadding = "5";
        me.collapsible = true;
        me.title = 'Activities';

        me.layout = {
            type : 'vbox',
            align : 'stretch'
        };

        // *************
       /* var store = Ext.create('Ext.data.Store', {
            model : 'DM2.model.Activity',
            proxy : {
                type : 'rest',
                noCache : false,
                url : DM2.view.AppConstants.apiurl+'mssql:Activity'
            },
            sorters : {
                property : 'description'
            },
            autoLoad : false
        });*/

        me.activityHistoryStore = Ext.create('Ext.data.Store', {
            model : 'DM2.model.ActivityHistory',
            proxy : {
                type : 'rest',
                url : DM2.view.AppConstants.apiurl+'mssql:ActivityHistory'
            },
            autoLoad : false
        });

        // ***************
		var activityStore = Ext.create('DM2.store.Activities');
        me.items = [{
            xtype : 'combobox',
            fieldLabel : 'Activity',
            name : 'activity',
            allowBlank : false,
            emptyText : 'Select Activity....',
            selectOnFocus : true,
            displayField : 'description',
            queryMode : 'local',
            store: activityStore,//'Activities',
            //store : store,
            typeAhead : true,
            valueField : 'idActivity'
        }, {
            xtype : 'dealdetailactivitymenu'
        }];
        
        me.callParent(arguments);
    }
}); 