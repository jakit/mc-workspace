Ext.define('DM2.view.property.PropertyContactsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.propertycontactsgridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table'
    ],

    store: null,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'gridcommoncls';
        me.title = 'Contacts';
		me.store = Ext.create('DM2.store.PropertyContacts');    
        me.columnLines = false;
        me.columns = [{
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'contactid',
            text: 'Contact Id',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'name',
            text: 'Name',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'company',
            text: 'Company',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'type',
            text: 'Type',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'phone',
            text: 'Phone',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'email',
            text: 'Email',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assoctype',
            text: 'Assoc Type',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assocdesc',
            text: 'Assoc Desc',
            flex: 1
        }];        
        
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				hidden: true,
				items: [
					{
						xtype: 'button',
						action: 'addpropertycontactbtn',
						text: 'Add Contact'
					}
				]
			}
		];
        me.callParent(arguments);
    }    
});