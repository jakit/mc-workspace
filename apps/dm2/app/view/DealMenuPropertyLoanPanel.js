Ext.define('DM2.view.DealMenuPropertyLoanPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.dealmenupropertyloanpanel',

    requires: [
        'DM2.view.DealMenuPropertyGridPanel',
        'DM2.view.DealMenuLoanGridPanel'
    ],

    bodyPadding: 2,
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
    items: [
        {
            xtype: 'dealmenupropertygridpanel',
            title: 'Properties',
            flex: 1
        },
        {
            xtype: 'dealmenuloangridpanel',
            title: 'Loans',
            flex: 1
        }
    ]

});