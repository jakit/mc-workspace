Ext.define('DM2.view.DealDetailQuoteMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealdetailquotemenu',

    requires: [
        'Ext.menu.Item'
    ],

    viewModel: {
        type: 'dealdetailquotemenu'
    },
    width: 160,

    items: [
        {
            xtype: 'menuitem',
            text: 'Add Quote',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Remove Quote',
            focusable: true
        }
    ]

});