Ext.define('DM2.view.DealDetailContactMenu', {
    extend: 'DM2.view.component.container.DealsDetailsContextMenu',
    alias: 'widget.dealdetailcontactmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
            text: 'Add Contact',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'add'
        },
        {
            xtype: 'menuitem',
            text: 'Remove Contact',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        },
        {
            xtype: 'menuitem',
            text: 'Change Role',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        },
        {
            xtype: 'menuitem',
            text: 'Edit Contact',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        },
        {
            xtype: 'menuitem',
            itemId: 'callcontact',
            text: 'Call Contact',
            focusable: true,
			menuType : 'edit',
            menu: {
                xtype: 'menu',
                width: 250
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'emailcontact',
            text: 'Email Contact',
			menuType : 'edit',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'faxcontact',
            text: 'Fax Contact',
			menuType : 'edit',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        }];    
        me.callParent(arguments);
    }
});