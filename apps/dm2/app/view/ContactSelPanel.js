Ext.define('DM2.view.ContactSelPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.contactselpanel',

    requires: [
        'Ext.button.Button',
        'Ext.form.field.ComboBox',
		'DM2.view.ContactSelGrid'
    ],
	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.frame = true;
		me.itemId = 'contactselpanel';
		me.margin = 3;
		me.ui = 'activitypanel';
		me.layout = 'border';
		me.closable = true;
		me.title = 'Docs';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					margin: '0 10 0 0',
					action: 'emailbtn',
					text: 'Email'
				},
				{
					xtype: 'button',
					action: 'faxbtn',
					itemId: 'faxbtn',
					text: 'Fax'
				}
			]
		};
		me.items = [
			{
				xtype: 'gridpanel',
				itemId:'docsgrid',
				region: 'north',
				split: true,
				collapsible: true,
				scrollable: true,
				header:false,
				store : Ext.create('DM2.store.Documents'),
    			columns : [{
					xtype: 'gridcolumn',
					hidden: true,
					dataIndex: 'docid',
					text: 'Doc Id',
					flex: 1
				},
				{
					xtype: 'datecolumn',
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						return Ext.util.Format.date(record.get('modifydate'), "m/d/Y h:i:s");
					},
					dataIndex: 'modifydate',
					text: 'Date/Time',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						var docurlname = record.get('url');
						var ext = DM2.app.getController('MainController').getExtImg(docurlname);
						var targetframe = DM2.app.getController('MainController').getTargetFrame(record.get('ext'));				
						return '<a class="doclink" style="color: -webkit-link;text-decoration: underline;cursor: pointer;" target="'+targetframe+'" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
						/*return '<span class="doclink" style="color: -webkit-link;text-decoration: underline;cursor: pointer;"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</span>';*/
					},
					dataIndex: 'name',
					text: 'Document Name',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'size',
					text: 'Size',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						metaData.tdAttr = 'data-qtip="' + value + '"';
						return value;
					},
					dataIndex: 'desc',
					text: 'Description',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						metaData.tdAttr = 'data-qtip="' + value + '"';
						return value;
					},
					dataIndex: 'clsfctn',
					text: 'Classification',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						metaData.tdAttr = 'data-qtip="' + value + '"';
						return value;
					},
					dataIndex: 'modifiedBy',
					text: 'By',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					hidden: true,
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						metaData.tdAttr = 'data-qtip="' + value + '"';
						return value;
					},
					dataIndex: 'assoctype',
					text: 'Assoc Type',
					flex: 1
				},
				{
					xtype: 'gridcolumn',
					hidden: true,
					renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
						metaData.tdAttr = 'data-qtip="' + value + '"';
						return value;
					},
					dataIndex: 'assocdesc',
					text: 'Assoc Desc',
					flex: 1
				}]
			},
			{
				xtype : 'contactselgrid',
				region: 'center',            
				scrollable: true
			}
		];
		me.callParent(arguments);
    }
});