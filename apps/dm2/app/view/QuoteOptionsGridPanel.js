Ext.define('DM2.view.QuoteOptionsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.quoteoptionsgridpanel',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date'
    ],

    cls: 'quoteoptionsubgridcls',
    frame: true,
    header: false,
    store: 'QuoteOptions',
    columns: [{
		xtype: 'gridcolumn',
		hidden:true,
		dataIndex: 'idLoan',
		text: 'Loan Id',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		dataIndex: 'optAmount',
		text: 'Option Amount',
		flex: 1,
        format: '0,000',
		align:'right',
		renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			//var formatVal = Ext.util.Format.number(value,'0,000');
			/*var readable = record.get('readable');
			if(readable==0){
				return null;
			}else{*/
				if(value==null || value==0){
					return null;  
				}else{
					var formatVal = Ext.util.Format.currency(value, '$',0);
					metaData.tdAttr = 'data-qtip="' + formatVal + '"';
					return formatVal;  
				}
			/*}*/
		}
	},
	{
		xtype: 'gridcolumn',
		//hidden:true,
		dataIndex: 'quoteRate',
		text: 'QuoteRate',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		dataIndex: 'rateType',
		text: 'Rate Type',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		//hidden:true,
		dataIndex: 'indexSpread',
		text: 'SpreadIndex',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		//hidden:true,
		dataIndex: 'percentSpread',
		text: 'PercentSpread',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		dataIndex: 'term',
		text: 'Term',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		//hidden:true,
		dataIndex: 'interestOnly',
		text: 'InterestOnly',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		//hidden:true,
		dataIndex: 'optionFloor',
		text: 'Floor',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		dataIndex: 'startDate',
		text: 'Start Date',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		//hidden:true,
		dataIndex: 'monthEnd',
		text: 'MonthEnd',
		flex: 1
	}],

    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            store : me.buildStore()
        });

        me.callParent(arguments);
    },

    buildStore: function() {
        return Ext.create('DM2.store.QuoteOptions', {});
    }
});