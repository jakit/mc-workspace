Ext.define('DM2.view.ContactEmailsGridPanel', {
    extend: 'DM2.view.MetaInfoGrid',
    alias: 'widget.contactemailsgridpanel',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	initComponent: function() {
        var me = this;
		var gridModel = me.generateGridModel();
		me.store = Ext.create('DM2.store.ContactEmails', {
			extend: 'Ext.data.Store',
		
			requires: [
				//'DM2.model.ContactPhone',
				'Ext.data.proxy.Rest',
				'Ext.data.reader.Json',
				'Ext.data.writer.Json'
			],							          
            storeId: 'ContactEmails',
            autoSync: true,
            model: gridModel,//'DM2.model.ContactEmail',
            proxy: {
                type: 'rest',
                api: {
                    create: DM2.view.AppConstants.apiurl+'mssql:Email',
                    read: DM2.view.AppConstants.apiurl+'mssql:Email',
                    update: DM2.view.AppConstants.apiurl+'mssql:Email',
                    destroy: DM2.view.AppConstants.apiurl+'mssql:Email'
                },
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Email',
                reader: {
                    type: 'json'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function(data, request) {
                            // do some manipulation of the unserialized data object
                            //console.log(data);
                            //console.log(request);
                            if(request._action==="create"){
                                Ext.Object.each(data, function(key, value, myself) {
                                    //console.log(key + ":" + value);
                    
                                    if (key === 'idEmail') {
                                        //console.log("Delete the key"+key);
                                        delete data[key];
                                    }
                                });
                            }
                            if(request._action==="update"){
                                data['@metadata'] = {'checksum' : 'override'};
                            }
							console.log(request);
							if(request._action==="destroy"){
								console.log("Calling destroy action");
                                //data['checksum'] = 'override';
								request._url = request._url+"?checksum=override";
                            }
                            //console.log(data);
                            return data;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                write: {
                    /*fn: me.onStoreWrite,
                    scope: me*/
					fn: function(store, operation, eOpts) {
						if(operation.request._action==="create"){
							console.log("Create Write completes");
							//console.log(operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(false);
							store.getAt(store.getCount()-1).set('idEmail',operation._records[0].data.txsummary[0].idEmail);
							//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(true);
						}
					},
					scope: this
                }
            },
        

			onStoreWrite: function(store, operation, eOpts) {
				//console.log(store);
				//console.log(operation);
				if(operation.request._action==="create"){
					//console.log("Create Write completes");
					//console.log(operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(false);
					//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(true);
				}
			}        
        });
		me.callParent(arguments);
	},
    frame: true,
    margin: '0 0 0 0',
    ui: 'activitypanel',
    collapsible: true,
    title: 'Emails',
	settings: {
		editor: {
			type: "combo",
			data: [{
				"label": "Email 1",
				"tag": "email1"
			},
			{
				"label": "Email 2",
				"tag": "email2"
			}]
		},
		grid: {
			fields: [{ "name": "email","type": "string", flex: 1 },{type: 'int',name: 'idEmail'},{name:'idContact'}],
			idPropertyField: 'idEmail',
			columns: [{
				dataIndex: "email",
				header: "Email",
				width: 150,
				flex: 1,
				editor: {
					xtype: 'textfield',
					emptyText: 'Enter a email'
				}
			}],
			useIsPrimary: true,
			buttonLabel: "Add",
			viewConfig: {
				stripeRows: true
			},
			tagFieldWidth: 400,
			tagFieldFlex: 1
		}
		
	}
	/*
    store: 'ContactEmails',

    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'tag',
            text: 'Tag',
            flex: 1,
            editor: {
                xtype: 'combobox',
                allowBlank: false,
                editable: false,
                displayField: 'tag',
                forceSelection: true,
                queryMode: 'local',
                store: 'EmailTags',
                valueField: 'tag'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'email',
            text: 'Email',
            flex: 1,
            editor: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'isPrimary',
            text: 'IsPrimary',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'add',
                    iconCls: 'add',
                    text: 'Add'
                },
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'delete',
                    iconCls: 'delete',
                    text: 'Delete'
                }
            ]
        }
    ],
    plugins: [
        {
            ptype: 'cellediting'
        }
    ],
    selModel: {
        selType: 'cellmodel'
    }
	*/
});