Ext.define('DM2.view.DealDetailsCenterPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.dealdetailscenterpanel',

    requires: [
        'DM2.view.DealDetailUpperSection',
        'DM2.view.DealDetailPropertyFieldSet',
        'DM2.view.DealDetailContactFieldSet',
        'DM2.view.DealDetailNewLoanFieldset',
        'DM2.view.DealDetailExistingLoanFieldset',
        'DM2.view.DealDetailMasterLoanFieldset',
        'Ext.form.Panel'
    ],

    bodyPadding: 3,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'dealdetailuppersection'
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'dealdetailpropertyfieldset',
                            margin: '0 0 3 0',
                            flex: 1
                        },
                        {
                            xtype: 'dealdetailcontactfieldset',
                            flex: 1
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'dealdetailnewloanfieldset',
                            margin: '0 0 3 5',
                            flex: 1
                        },
                        {
                            xtype: 'dealdetailexistingloanfieldset',
                            margin: '0 0 3 5',
                            flex: 1
                        },
                        {
                            xtype: 'dealdetailmasterloanfieldset',
                            margin: '0 0 3 5',
                            flex: 1
                        }
                    ]
                }
            ]
        }
    ]

});