Ext.define('DM2.view.EmailFaxSubmission', {
    extend: 'Ext.form.Panel',
    alias: 'widget.emailfaxsubmission',

    requires: [
        'Ext.form.field.Display',
        'Ext.form.field.HtmlEditor',
        'Ext.form.field.Number',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

    bodyPadding: 10,
    closable: true,
    title: 'My Form',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'textfield',
            fieldLabel: 'To',
            labelWidth: 50,
            name: 'docto'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Subject',
            labelWidth: 50,
            name: 'docsubject'
        },
        {
            xtype: 'displayfield',
            fieldLabel: 'Doc',
            labelWidth: 50,
            name: 'docname'
        },
        {
            xtype: 'htmleditor',
            flex: 1,
            fieldLabel: 'Body',
            labelWidth: 50,
            name: 'docbody'
        },
        {
            xtype: 'numberfield',
            hidden: true,
            name: 'docid'
        },
        {
            xtype: 'textfield',
            hidden: true,
            name: 'requestType'
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            flex: 1,
            dock: 'bottom',
            ui: 'footer',
            defaults: {
                minWidth: 75
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
                    formBind: true,
                    disabled: true,
                    itemId: 'sendemailbtn',
                    text: 'Send'
                },
                {
                    xtype: 'button',
                    itemId: 'sendemailresetbtn',
                    text: 'Reset'
                }
            ]
        }
    ]

});