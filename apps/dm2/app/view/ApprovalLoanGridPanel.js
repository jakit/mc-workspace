Ext.define('DM2.view.ApprovalLoanGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.approvalloangridpanel',

    requires: [
        'Ext.grid.column.Number',
        'Ext.grid.column.Check',
        'Ext.grid.column.Action',
        'Ext.view.Table'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
	    me.cls = 'gridcommoncls';
		me.itemId = 'approvalloangridpanel';
		me.columnLines = true;
		me.multiColumnSort = true;
		me.sortableColumns = false;
		me.store = Ext.create('DM2.store.DealQuotesSelected');//'DealQuotesSelected';		
		me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'bank',
				text: 'Bank',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'loantype',
				text: 'Type',
				flex: 1
			},
			{
				xtype: 'numbercolumn',
				dataIndex: 'receivedamount',
				text: 'Amount',
				flex: 1,
				format: '0,000',
				align:'right',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					//var formatVal = Ext.util.Format.number(value,'0,000');
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				}
			},
			{
				xtype: 'checkcolumn',
				itemId: 'rxcommitcolumn',
				dataIndex: 'hasCommit',
				text: 'Recv. Commitment',
				flex: 1
			},
			{
				xtype: 'actioncolumn',
				handler: function(view, rowIndex, colIndex, item, e, record, row) {
					this.fireEvent('itemClick', view, rowIndex, colIndex, item, e, record, row);
				},
				itemId: 'sendmodcolumn',
				align: 'center',
				dataIndex: 'sendmod',
				text: 'Send Mod',
				tooltip: 'Send Modification',
				flex: 1,
				icon: 'resources/images/sendmod.png'
			}
		];
		me.callParent(arguments);
    }
});