Ext.define('DM2.view.CommissionSchdGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.commissionschdgridpanel',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel',
		'DM2.store.LoanSchedule',
		'DM2.store.CommSchedule',
		'DM2.view.CommissionScheduleMenu'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	initComponent: function() {
        var me = this;
		me.store =  Ext.create('DM2.store.CommSchedule');
		me.contextMenu = Ext.create('DM2.view.CommissionScheduleMenu');
		me.frame = true;
		me.margin = '0 0 0 0';
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.hidden = true;
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					itemId: 'addcommsched',
					action:'addcommsched',
					iconCls: 'add',
					text: 'Add'
				}
			]
		};
	    me.title = 'Structured Commission Schedule';
	    me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden:true,
				dataIndex: 'idSale',
				text: 'idSale',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				hidden:true,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'orderID',
				text: 'OrderID',
				flex: 1
			},
			{
				xtype: 'numbercolumn',
				cls: 'dealgridcolumncls',
				format: '0,000',
				align : 'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(value==null || value==0){
						return null;  
					} else {
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
					metaData.tdAttr = 'data-qtip="' + retVal + '"';
					return retVal;
				},
				dataIndex: 'startAmount',
				text: 'Start Amt',
				flex: 1
			},
			{
				xtype: 'numbercolumn',
				cls: 'dealgridcolumncls',
				format: '0,000',
				align : 'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(value==null || value==0){
						return null;  
					} else {
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
					metaData.tdAttr = 'data-qtip="' + retVal + '"';
					return retVal;
				},
				dataIndex: 'endAmount',
				text: 'End Amt',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'percentCommission',
				text: 'Commission (%)',
				flex: 1
			}
		];
		me.dockedItems = [{
			xtype: 'form',
			hidden:true,
			dock: 'bottom',			
			title:'Add Structured Commission Schedule',
			itemId:'structuredcommschedform',
			closable: true,
			closeAction : 'hide',
			//me.hidden = true;
			header : {
				titlePosition: 0,
				items: [
					{
						xtype: 'button',
						//disabled:true,
						action:'savecommschedbtn',
						text: 'Save'
					}
				]
			},
			layout:{
				type:'vbox',
				align:'stretch'
			},
			bodyPadding:5,
			items: [{
				xtype:'panel',
				margin : '0 0 5 0',
				layout:{
					type:'hbox',
					align:'center'
				},
				items:[{
					xtype: 'numberfield',
					hidden:true,
					fieldLabel: 'idCommissionSched',
					labelAlign: 'left',
					labelWidth: 70,
					name: 'idCommissionSched'
				},{
					xtype: 'numberfield',
					flex:1,
					hidden:true,
					hideTrigger:true,
					//keyNavEnabled:false,
					//mouseWheelEnabled:false,
					fieldLabel: 'OrderID',
					//labelAlign: 'right',
					labelWidth: 85,
					name: 'orderID'
				},
				{
					xtype: 'currencyfield',
					flex:1,
					hideTrigger:true,
					//keyNavEnabled:false,
					//mouseWheelEnabled:false,
					fieldLabel: 'Amount',
					//labelAlign: 'right',
					labelWidth: 90,
					name: 'amount'
				},
				{
					xtype: 'numberfield',
					flex:1,
					hideTrigger:true,
					fieldLabel: 'Commission (%)',
					labelAlign: 'right',
					labelWidth: 90,
					name: 'percentCommission'
				}]
			}]
		}];
		/*me.plugins = [
			{
				ptype: 'cellediting'
			}
		];
		me.selModel = {
			selType: 'cellmodel'
		};*/
		me.callParent(arguments);
	}
});