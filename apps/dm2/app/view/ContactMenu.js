Ext.define('DM2.view.ContactMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.contactmenu',

    requires: [
        'Ext.menu.Item',
        'Ext.menu.Menu'
    ],
	
    width: 180,

    items: [
        {
            xtype: 'menuitem',
            text: 'Add New Contact',
			action: 'addnewcontact',
            focusable: true
        },
		{
            xtype: 'menuitem',
            text: 'Edit Contact',
			action: 'editcontact',
            focusable: true
        },
        {
            xtype: 'menuitem',
			hidden: true,
            text: 'Remove Contact',
			action: 'removecontact',
            focusable: true
        },
		{
            xtype: 'menuitem',
			//hidden: true,
            text: 'Upload Contacts',
			action: 'uploadcontact',
            focusable: true
        },
		{
            xtype: 'menuitem',
			hidden: true,
            text: 'Remove From My Contact',
			action: 'removefrommycontact',
            focusable: true
        },
		{
            xtype: 'menuitem',
            text: 'Add To My Contact',
			action: 'addtomycontact',
            focusable: true
        },
		{
            xtype: 'menuitem',
            text: 'Create New Sale',
			action: 'createnewsaledealfromcnt',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemId: 'callcontact',
            text: 'Call Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 250
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'emailcontact',
            text: 'Email Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'faxcontact',
            text: 'Fax Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        }
    ]

});