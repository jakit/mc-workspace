Ext.define('DM2.view.LoanSizeGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.loansizegridmenu',

    requires: [
        'Ext.menu.Item'
    ],
	
    items: [
        {
            xtype: 'menuitem',
            itemevent: 'activitydone',
            text: 'Mark activity as done',
            focusable: true
        }
    ]
});