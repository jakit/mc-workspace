Ext.define('DM2.view.ResearchPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.researchpanel',

    requires: [
        'Ext.panel.Panel',
        'Ext.button.Button'
    ],

    layout: 'border',
    title: 'Research',

    items: [
        {
            xtype: 'panel',
            region: 'center'
        },
        {
            xtype: 'panel',
            region: 'west',
            split: true,
            width: 150,
            collapsible: true,
            header: false,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'button',
                    itemId: 'gmapbtn',
                    margin: '10 5 10 5',
                    text: 'Gmap'
                },
                {
                    xtype: 'button',
                    itemId: 'zillowbtn',
                    margin: '10 5 10 5',
                    text: 'Zillow'
                }
            ]
        }
    ]

});