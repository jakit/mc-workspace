Ext.define('DM2.view.DealDetailActivityMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealdetailactivitymenu',

    cls: 'activitylist',
    floating: false,
    itemId: 'activitymenu',
    ui: 'activitymenu'
});