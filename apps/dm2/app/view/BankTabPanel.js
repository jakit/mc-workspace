Ext.define('DM2.view.BankTabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.banktabpanel',

    requires: [
        'DM2.view.BankContactsGridPanel',
        'Ext.grid.Panel',
        'Ext.tab.Tab',
        'Ext.tab.Bar'
    ],

    height: 250,
    //width: 400,
    activeTab: 0,

    items: [
        {
            xtype: 'bankcontactsgridpanel',
			iconCls: 'contactbtn'
        }
    ],
    tabBar: {
        xtype: 'tabbar',
        ui: 'submenutabpanel'
    }

});