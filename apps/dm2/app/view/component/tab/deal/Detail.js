Ext.define('DM2.view.component.tab.deal.Detail', {
    extend: 'DM2.view.component.tab.BaseDetailPanel',
    xtype: 'tab-deal-detail',
    requires: [
        'DM2.view.DealDetailsFormPanel',
        'Ext.layout.container.Fit'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.layout = {
            type: 'fit'
        };
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };      

        me.items = [{
            xtype: 'dealdetailsformpanel'
        }];
        me.callParent(arguments);
    }
});