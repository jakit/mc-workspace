Ext.define('DM2.view.component.tab.deal.Layout', {
    extend: 'DM2.view.component.tab.BaseLayoutContainer',
    xtype: 'tab-deal-layout',
    requires: [
        'DM2.view.component.tab.deal.Grid',
        'DM2.view.DealMenuTabPanel'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'tab-deal-grid' 
        };
        
        me.southRegion = {
            xtype: 'dealmenutabpanel',
            dealHistoryStore: 'DealHistory'
        };
        
        me.eastRegion = {
            xtype: 'panel',
			itemId:'deal-east-panel',
            hidden: true,
			layout:{
				type:'card'
			}
            
        };
       
        me.callParent(arguments);
    }
});