Ext.define('DM2.view.component.tab.deal.Grid', {
    extend: 'DM2.view.component.tab.BaseGridPanelNew',
    xtype: 'tab-deal-grid',
    //requires: [],
    uses: [
        'DM2.view.component.tab.deal.Detail'    
    ],    
    counter: 1,
    detailClass: 'DM2.view.component.tab.deal.Detail',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
		me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
        me.store = 'Deals';
        me.viewConfig = {
            itemId: 'dealmenugridview',
            forceFit: true
        };
        me.plugins = [{
            itemId: 'rowexpander-deals',
            ptype: 'rowexpander',
            expandOnDblClick: false,
            rowBodyTpl: [
				'<div id="propertygridrow-{idDeal}"></div>'
            ]
        }];

        me.columns = [{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .4,
            sortable: true,
			hidden:true,
            dataIndex: 'idDeal',
            //menuDisabled: true,
            text: 'DM-DealNo',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .45,
            sortable: true,
            dataIndex: 'magicDealID',
            //menuDisabled: true,
            text: 'LT-DealNo',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        }, /*{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                if(value=='S'){
                    return '<img src="resources/images/simple.png" />';
                }
                else if(value=='P'){
                    return '<img src="resources/images/package.png" />';
                }
                else if(value=='B'){
                    return '<img src="resources/images/blanket.png" />';
                }
                else if(value=='C'){
                    return '<img src="resources/images/complex.png" />';
                }
                else
                {
                    return value;
                }
            },
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'dealtype',
            //menuDisabled: true,
            text: 'Deal Type'
        }, */{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: 1.5,
            dataIndex: 'dealName',
            //menuDisabled: true,
            text: 'Name',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"//,
				//hidden:true,
				//disabled:true
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
			flex: 1,
            //hidden: true,
            dataIndex: 'companyName',
            //menuDisabled: true,
            text: 'Company',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'streetNumber',
            //menuDisabled: true,
            text: 'Street No.',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'streetName',
            //menuDisabled: true,
            text: 'Street',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'city',
            //menuDisabled: true,
            text: 'City',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'state',
            //menuDisabled: true,
            text: 'State',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: Ext.getStore('StatesList').getData(),
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'zipcode',
            //menuDisabled: true,
            text: 'Zipcode',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            xtype: 'checkcolumn',
            cls: 'dealgridcolumncls',
            flex: .37,
			//width:70,
            dataIndex: 'blanket',
            //menuDisabled: true,
            text: 'Blanket',
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "true",
                    'val': "true"
                }, {
                    'display': "false",
                    'val': "false"
                }]
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'mainContact',
			//sortable : false,
            //menuDisabled: true,
            text: 'Contact',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'status',
            //menuDisabled: true,
            text: 'Status',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: [{
                    'display': "AC",
                    'val': "AC"
                }, {
                    'display': "CL",
                    'val': "CL"
                }, {
                    'display': "CX",
                    'val': "CX"
                }, {
                    'display': "DX",
                    'val': "DX"
                }, {
                    'display': "EC",
                    'val': "EC"
                }, {
                    'display': "IP",
                    'val': "IP"
                }, {
                    'display': "LD",
                    'val': "LD"
                }, {
                    'display': "PR",
                    'val': "PR"
                }, {
                    'display': "UL",
                    'val': "UL"
                }],
                filterDataType: "string"
            }) 
        }, {
            xtype: 'datecolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'statusdate',
            //menuDisabled: true,
            text: 'StatusDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        }, {
            xtype: 'datecolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
			hidden:true,
            dataIndex: 'startdate',
            //menuDisabled: true,
            text: 'DealDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				var readable = record.get('readable');
				//readable = 0;
				if(readable==0){
					return null;
				}else{
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				}
            },
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'bankName',
            //menuDisabled: true,
            text: 'Bank',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"
            })
        }, {
            xtype: 'numbercolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'loanAmt',
            //menuDisabled: true,
            text: 'Loan Amount',
            format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                //var formatVal = Ext.util.Format.number(value,'0,000');
				var readable = record.get('readable');
				//readable = 0;
				if(readable==0){
					return null;
				}else{
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				}
            },
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'datecolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				var readable = record.get('readable');
				//readable = 0;
				if(readable==0 || record.get('loanDate')==null){
					return null;
				}else{
					//console.log(value);
					//console.log(new Date(value));
					return Ext.Date.format(new Date(value), 'm-d-Y');
				}
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'loanDate',
            //menuDisabled: true,
            flex: .45,
            text: 'LoanDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				//var readable = record.get('readable');
				//readable = 0;
				//if(readable==0){
					//return null;
				//}else{
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				//}
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'userList',
            //menuDisabled: true,
            text: 'Broker',
            flex: .5,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Starts With",
                filterDataType: "string"
            })
        },
		{
            xtype: 'checkcolumn',
            cls: 'dealgridcolumncls',
			hidden:true,
            dataIndex: 'covered',
            text: 'Covered',
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "true",
                    'val': "true"
                }, {
                    'display': "false",
                    'val': "false"
                }]
            })
        }];   

        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
		var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];
        me.callParent(arguments);
    }
});