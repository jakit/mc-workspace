Ext.define('DM2.view.component.tab.sales.Layout', {
    extend: 'DM2.view.component.tab.BaseLayoutContainer',
    xtype: 'tab-sales-layout',
    requires: [
        'DM2.view.component.tab.sales.Grid',
        'DM2.view.SalesTabPanel'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'tab-sales-grid' 
        };
        
        me.southRegion = {
            xtype: 'salestabpanel'
        };
        
		 me.eastRegion = {
            xtype: 'panel',
			itemId:'sales-east-panel',
            hidden: true,
			layout:{
				type:'card'
			}
            
        };
        
        me.callParent(arguments);
    }
});