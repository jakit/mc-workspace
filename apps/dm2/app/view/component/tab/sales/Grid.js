Ext.define('DM2.view.component.tab.sales.Grid', {
    extend: 'DM2.view.component.tab.BaseGridPanelNew',
    xtype: 'tab-sales-grid',
    requires: ['DM2.view.SalesGridOptionsMenu'],
    uses: [
        'DM2.view.component.tab.sales.Detail'
    ],    
    counter: 1,
    detailClass: 'DM2.view.component.tab.sales.Detail',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
		me.firstLoad = true;
		me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
		
		me.contextMenu = Ext.create('DM2.view.SalesGridOptionsMenu');
		
        me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
        me.store = 'Salesallbuff';
        me.viewConfig = {
            itemId: 'saledealmenugridview',
            forceFit: true
        };
        me.plugins = [{
            itemId: 'rowexpander-deals',
            ptype: 'rowexpander',
            expandOnDblClick: false,
            rowBodyTpl: [
                //'<div id="propertygridrow-{dealid}"></div>'
				'<div id="salespropertygridrow-{idDeal}"></div>'
            ]
        }];

        me.columns = [{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .4,
            sortable: true,
			hidden:true,
            dataIndex: 'idDeal',
            //menuDisabled: true,
            text: 'DM-DealNo',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },/*{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .45,
            sortable: true,
            dataIndex: 'magicDealID',
            //menuDisabled: true,
            text: 'LT-DealNo',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        }, */{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: 1.5,
            dataIndex: 'dealName',
            //menuDisabled: true,
            text: 'Name',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'streetNumber',
            //menuDisabled: true,
            text: 'Street No.',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'streetName',
            //menuDisabled: true,
            text: 'Street',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'city',
            //menuDisabled: true,
            text: 'City',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'state',
            //menuDisabled: true,
            text: 'State',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: Ext.getStore('StatesList').getData(),
                filterDataType: "string"
            })
        },  {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            //dataIndex: 'mainContact',
			dataIndex: 'allPropertyNames',
            //menuDisabled: true,
            text: 'Property',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
			dataIndex: 'buyer',
            //menuDisabled: true,
            text: 'Buyer',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
			dataIndex: 'seller',
            //menuDisabled: true,
            text: 'Seller',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },/*{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
			dataIndex: 'contactType',
            //menuDisabled: true,
            text: 'Consulting',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },*/ {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            //dataIndex: 'mainContact',
			dataIndex: 'allContacts',
            //menuDisabled: true,
            text: 'Contact',
			hidden:true,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'numbercolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'amount',
            //menuDisabled: true,
            text: 'Amount',
            format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                //var formatVal = Ext.util.Format.number(value,'0,000');
				var readable = record.get('readable');
				//readable = 0;
				if(readable==0){
					return null;
				}else{
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				}
            },
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .3,
			align:'center',
            dataIndex: 'saleType',
            //menuDisabled: true,
            text: 'Sale Type',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: [{
                    'display': "B",
                    'val': "B"
                }, {
                    'display': "S",
                    'val': "S"
                }, {
                    'display': "O",
                    'val': "O"
                }, {
                    'display': "C",
                    'val': "C"
                }],
                filterDataType: "string"
            }) 
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .3,
			align:'center',
            dataIndex: 'status',
            //menuDisabled: true,
            text: 'Status',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: [{
                    'display': "PC",
                    'val': "PC"
                }, {
                    'display': "CO",
                    'val': "CO"
                }, {
                    'display': "HC",
                    'val': "HC"
                }, {
                    'display': "CL",
                    'val': "CL"
                }],
                filterDataType: "string"
            }) 
        }, {
            xtype: 'datecolumn',
            cls: 'dealgridcolumncls',
            flex: .45,
            dataIndex: 'statusdate',
            //menuDisabled: true,
            text: 'StatusDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        }, {
            xtype: 'datecolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
			hidden:true,
            dataIndex: 'startdate',
            //menuDisabled: true,
            text: 'DealDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },/* {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				var readable = record.get('readable');
				//readable = 0;
				if(readable==0){
					return null;
				}else{
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				}
            },
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'bankName',
            //menuDisabled: true,
            text: 'Bank',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'numbercolumn',
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'loanAmt',
            //menuDisabled: true,
            text: 'Loan Amount',
            format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                //var formatVal = Ext.util.Format.number(value,'0,000');
				var readable = record.get('readable');
				//readable = 0;
				if(readable==0){
					return null;
				}else{
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				}
            },
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'datecolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				var readable = record.get('readable');
				//readable = 0;
				if(readable==0 || record.get('loanDate')==null){
					return null;
				}else{
					//console.log(value);
					//console.log(new Date(value));
					return Ext.Date.format(new Date(value), 'm-d-Y');
				}
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'loanDate',
            //menuDisabled: true,
            flex: .5,
            text: 'LoanDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },*/
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				//var readable = record.get('readable');
				//readable = 0;
				//if(readable==0){
					//return null;
				//}else{
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				//}
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'userList',
            //menuDisabled: true,
            text: 'Broker',
            flex: .5,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }];   

        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
		var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];  
        
        me.callParent(arguments);
    }
});