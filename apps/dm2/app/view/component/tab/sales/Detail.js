Ext.define('DM2.view.component.tab.sales.Detail', {
    extend: 'DM2.view.component.tab.BaseDetailPanel',
    xtype: 'tab-sales-detail',
    requires: [
        'DM2.view.SalesDealDetailsFormPanel',
        'Ext.layout.container.Fit'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.layout = {
            type: 'fit'
        };
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };      

        me.items = [{
            xtype: 'salesdealdetailsformpanel'
        }];
        me.callParent(arguments);
    }
});