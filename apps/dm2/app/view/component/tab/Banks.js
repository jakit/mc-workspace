Ext.define('DM2.view.component.tab.Banks', {
    extend: 'DM2.view.component.tab.BaseCardPanel',
    xtype: 'tab-banks',
    requires: [
        'DM2.view.component.tab.bank.Layout'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
        me.ui = "default";
        me.cls = "customTabBar";
        me.items = [{
			title: 'Bank Grid',
            xtype: 'tab-bank-layout'
        }];
        me.callParent(arguments);
    }
});

