Ext.define('DM2.view.component.tab.contact.Grid', {
    extend: 'DM2.view.component.tab.BaseGridPanelNew',
    xtype: 'tab-contact-grid',
    uses: [
        'DM2.view.component.tab.contact.Detail'    
    ],    
    counter: 1,
    detailClass: 'DM2.view.component.tab.contact.Detail',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable:true,
    initComponent: function() {
		var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
		
        me.cls = 'gridcommoncls deallistgridcls';
        me.ui = 'dealmenugridui';
        me.store = 'Contactallbuff';
		me.firstLoad = true;
        me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
		me.contextMenu = Ext.create('DM2.view.ContactMenu');
		me.plugins = [{
            ptype: 'rowexpander',
            expandOnDblClick: false,
            rowBodyTpl: [
				'<div></div>'
            ]
        }];
        me.columns = [{
            //xtype: 'gridcolumn',
            hidden: true,
            //dataIndex: 'idContact',
			dataIndex: 'contactid',
            text: 'Contact No.',
            headerField: me.setFilterField("stringfield", {
                grid: me
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'contactType',
            text: 'ContactType',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            flex: .7,
			//hidden: true,
            dataIndex: 'title',
            text: 'Title',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			flex:1,
			//hidden: true,
            dataIndex: 'firstName',
            text: 'First Name',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			flex:1,
			//hidden: true,
            dataIndex: 'lastName',
            text: 'Last Name',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			flex:1,
			//width: 135,
            //hidden: true,
			dataIndex: 'displayName',
            text: 'Display Name',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			flex:1,
			//width: 135,
            hidden: true,
            //dataIndex: 'fullName',
			dataIndex: 'name',
            text: 'Name',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'checkcolumn',
            dataIndex: 'showCompany',
            text: 'Show Company',
			hidden: true,
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "true",
                    'val': "true"
                }, {
                    'display': "false",
                    'val': "false"
                }]
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'companyName',
            text: 'Company',
			flex:2,
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'position',
            text: 'Position',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            //dataIndex: 'officePhone_1',
			dataIndex: 'phone',
            text: 'Phone',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'email',
            text: 'Email',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			//hidden: true,
            dataIndex: 'fax',
            text: 'Fax',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, 
		{
            //xtype: 'gridcolumn',
			hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'address',
            text: 'Address',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
			hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'city',
            text: 'City',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
			hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'state',
            text: 'State',
			align:'center',
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: Ext.getStore('StatesList').getData()
            })
        },
        {
            //xtype: 'gridcolumn',
			hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'zipCode',
            text: 'Zip Code',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }/*,
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'officePhone_2',
            text: 'Office Ph 2'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'homePhone_1',
            text: 'Home Ph 1'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'homePhone_2',
            text: 'Home Ph 2'
        }, 
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'mobilePhone_2',
            text: 'Mobile Ph 2'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'officeFax',
            text: 'Office Fax'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'homeFax',
            text: 'Home Fax'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'pager',
            text: 'Pager'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'email_2',
            text: 'Email2'
        }*/];
		
        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
		var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];  
       
        me.callParent(arguments);
    }
});