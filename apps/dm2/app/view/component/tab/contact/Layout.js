Ext.define('DM2.view.component.tab.contact.Layout', {
    extend: 'DM2.view.component.tab.BaseLayoutContainer',
    xtype: 'tab-contact-layout',
    requires: [
        'DM2.view.component.tab.contact.Grid',
        'DM2.view.ContactTabPanel'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'tab-contact-grid' 
        };
        
        me.southRegion = {
            xtype: 'contacttabpanel'/*,
            notesStore: 'Notes',
            dealHistoryStore: 'DealHistory',
            usersStore: "DealUsers"*/
        };
        
        me.eastRegion = {
            xtype: 'panel',
			layout: 'card',
			//header: false,
			itemId:'contact-east-panel',
            items: [
                {
                    xtype: 'contactdetailsform'
                }
            ]              
        };
        
        me.callParent(arguments);
    }
});