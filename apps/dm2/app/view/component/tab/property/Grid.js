Ext.define('DM2.view.component.tab.property.Grid', {
    extend: 'DM2.view.component.tab.BaseGridPanelNew',
    xtype: 'tab-property-grid',
    requires: [],
    uses: [
        'DM2.view.component.tab.property.Detail'    
    ], 
    counter: 1,   
    detailClass: 'DM2.view.component.tab.property.Detail',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
		me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
		me.firstLoad = true;
		me.store = Ext.create('DM2.store.PropMasterallbuff');
        me.viewConfig = {
            //itemId: 'propmastergridpanel',
            forceFit: true
        };
        me.columns = [{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'State',
            flex: .4,
            text: 'State',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: Ext.getStore('StatesList').getData(),
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			//hidden:true,
            dataIndex: 'Boro',
            flex: .4,
            text: 'Boro',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			//hidden:true,
            dataIndex: 'Block',
            flex: .4,
            text: 'Block',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			//hidden:true,
            dataIndex: 'Lot',
            flex: .4,
            text: 'Lot',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            dataIndex: 'APN',
            flex: .5,
            text: 'APN',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StreetNumber',
            flex: .4,
            text: 'Street No.',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'HighStreetNumber',
            flex: .4,
            text: 'High Street No.',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StreetName',
            flex: 1,
            text: 'Street',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'ZipCode',
            flex: .4,
            text: 'ZipCode',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'Stories',
            flex: .4,
            text: 'Stories',
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingClass',
            flex: .4,
            text: 'Building Class',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Units',
            flex: .4,
            text: 'Units',
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Owner',
            flex: 1,
            text: 'Owner',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            hidden:true,
            dataIndex: 'idPropertyMaster',
            flex: 1,
            text: 'Property No.',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        }, {
            //xtype: 'gridcolumn',
            hidden:true,
            dataIndex: 'PropertyMasterCodeSource',
            flex: 1,
            text: 'PropertyMasterCodeSource',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Township',
            flex: 1,
            text: 'Township',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Municipality',
            flex: 1,
            text: 'Municipality',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingName',
            flex: 1,
            text: 'BuildingName',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            //xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'CurrentAddedValue',
            flex: 1,
            text: 'CurrentAddedValue',
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'CurrentAVTAssess',
            flex: 1,
            text: 'CurrentAVTAssess',
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'FullValue',
            flex: 1,
            text: 'FullValue',
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Zoning',
            flex: 1,
            text: 'Zoning',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'FrontLot',
            flex: 1,
            text: 'FrontLot',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'LotDepth',
            flex: 1,
            text: 'LotDepth',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingFront',
            flex: 1,
            text: 'BuildingFront',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingDepartment',
            flex: 1,
            text: 'BuildingDepartment',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'LandArea',
            flex: 1,
            text: 'LandArea',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Buildings',
            flex: 1,
            text: 'Buildings',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'ResidentialUnits',
            flex: 1,
            text: 'ResidentialUnits',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'CommUnits',
            flex: 1,
            text: 'CommUnits',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'SquareFootage',
            flex: 1,
            text: 'SquareFootage',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'NumberOfBedrooms',
            flex: 1,
            text: 'NumberOfBedrooms',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
			format: '0,000',
            //hidden:true,
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				if(value==null || value==0) {
					return null;  
				} else {
					var formatVal = Ext.util.Format.currency(value, '$',0);
					metaData.tdAttr = 'data-qtip="' + formatVal + '"';
					return formatVal;  
				}
            },
            dataIndex: 'TotalAccumulatedLoanAmount',
            flex: 1,
            text: 'Mortgage Amount',
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'PropertyTax',
            flex: 1,
            text: 'PropertyTax',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'SalePrice',
            flex: 1,
            text: 'SalePrice',
			headerField: me.setFilterField("numberfield", {
                field1: {
                    grid: me,
                    filterMode: "Equal",
                    emptyText: "Start...",
                    filterDataType: "number"
                },
                field2: {
                    grid: me,
                    emptyText: "End..."
                }
            })
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'SaleDate',
            flex: 1,
            text: 'SaleDate',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'LastKnownLender',
            flex: 1,
            text: 'LastKnownLender',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            //hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Bank',
            flex: 1,
            text: 'Bank',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'idBank',
            flex: 1,
            text: 'idBank',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'datecolumn',
            //hidden:true,
            /*renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },*/
			format: 'm-d-Y',
            dataIndex: 'LastKnownMortgageDate',
            flex: 1,
            text: 'Origination Date',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'LastKnownLendee',
            flex: 1,
            text: 'LastKnownLendee',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'ManualBankUpate',
            flex: 1,
            text: 'ManualBankUpate',
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "true",
                    'val': "true"
                }, {
                    'display': "false",
                    'val': "false"
                }]
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'UnitNumber',
            flex: 1,
            text: 'UnitNumber',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'idCondo',
            flex: 1,
            text: 'idCondo',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'YearBuilt',
            flex: 1,
            text: 'YearBuilt',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'EstimatedYearBuilt',
            flex: 1,
            text: 'EstimatedYearBuilt',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'UpdateDate',
            flex: 1,
            text: 'UpdateDate',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StandardStreet',
            flex: 1,
            text: 'StandardStreet',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StandardPrefixStreet',
            flex: 1,
            text: 'StandardPrefixStreet',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StandardStreetName',
            flex: 1,
            text: 'StandardStreetName',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StandardCity',
            flex: 1,
            text: 'StandardCity',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'AddStandardStatus',
            flex: 1,
            text: 'AddStandardStatus',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'AKAList',
            flex: 1,
            text: 'AKAList',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'lot2',
            flex: 1,
            text: 'lot2',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'CSRName',
            flex: 1,
            text: 'CSRName',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'lastDealID',
            flex: 1,
            text: 'lastDealID',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'lastPropertyID',
            flex: 1,
            text: 'lastPropertyID',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'lastPrimaryCSRInitials',
            flex: 1,
            text: 'lastPrimaryCSRInitials',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'numberOfBrothers',
            flex: 1,
            text: 'numberOfBrothers',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'checkcolumn',
            cls: 'dealgridcolumncls',
			hidden:true,
            dataIndex: 'hasDeal',
            text: 'HasDeal',
			flex: .3,
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "true",
                    'val': "true"
                }, {
                    'display': "false",
                    'val': "false"
                }]
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'lastDealStatus',
            text: 'LastDealStatus',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: [{
                    'display': "AC",
                    'val': "AC"
                }, {
                    'display': "CL",
                    'val': "CL"
                }, {
                    'display': "CX",
                    'val': "CX"
                }, {
                    'display': "DX",
                    'val': "DX"
                }, {
                    'display': "EC",
                    'val': "EC"
                }, {
                    'display': "IP",
                    'val': "IP"
                }, {
                    'display': "LD",
                    'val': "LD"
                }, {
                    'display': "PR",
                    'val': "PR"
                }, {
                    'display': "UL",
                    'val': "UL"
                }],
                filterDataType: "string"
            }) 
        }];   

        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
		var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];  
        
        me.callParent(arguments);
    }
});