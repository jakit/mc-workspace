Ext.define('DM2.view.component.tab.property.Detail', {
    extend: 'DM2.view.component.tab.BaseDetailPanel',
    xtype: 'tab-property-detail',
    requires: [],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
    }
});