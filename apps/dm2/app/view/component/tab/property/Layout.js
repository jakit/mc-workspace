Ext.define('DM2.view.component.tab.property.Layout', {
    extend: 'DM2.view.component.tab.BaseLayoutContainer',
    xtype: 'tab-property-layout',
    requires: [
        'DM2.view.component.tab.property.Grid',
        'DM2.view.PropertyMenuTabPanel'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'tab-property-grid' 
        };
        
        me.southRegion = {
            xtype: 'propertymenutabpanel'/*,
            notesStore: 'Notes',
            dealHistoryStore: 'DealHistory',
            usersStore: "DealUsers"*/
        };
        
        me.eastRegion = {
			xtype: 'panel',
			itemId:'property-east-panel',
           // hidden: true,
			layout:{
				type:'card'
			},
			title:'Property Details',
			items:[{
				xtype: 'propmasterdetailsform',
				layout: {
					type: 'vbox',
					align:'stretch'
				}
		   }]            
        };
       
        me.callParent(arguments);
    }
});