Ext.define('DM2.view.component.tab.Panel', {
    extend: 'Ext.panel.Panel',
    xtype: 'tab-panel',
    requires: [
        'DM2.view.component.tab.Deals',
		'DM2.view.component.tab.Sales',
        'DM2.view.component.tab.Properties',
        'DM2.view.component.tab.Contacts',
        'DM2.view.component.tab.Banks',
        'DM2.view.component.tab.Reports'    
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.layout = {
            type: 'card'
        };        
        me.items = [];
        me.callParent(arguments);
    }
});