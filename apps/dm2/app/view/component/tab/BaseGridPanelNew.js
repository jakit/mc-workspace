Ext.define('DM2.view.component.tab.BaseGridPanelNew', {
    extend: 'Ext.grid.Panel',
    xtype: 'tab-basegridpanelnew',
    requires: [
        'Ext.grid.plugin.RowExpander', 'Filters2.HeaderFields' 
    ],
    mixins: ['Filters2.InjectHeaderFields', 'Filters2.FiltersMixin'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    scrollable: 'y',
    detailClass: null,
    initComponent: function() {
        var me = this;
		me.emptyText  = "No data is available";
		/*me.viewConfig = {
			emptyText: 'No data'        
		};*/
        me.cls = 'gridcommoncls';
        //me.multiColumnSort = true;
        me.bodyPadding = "0";
        me.padding = "0";
        me.margin = "0";
        // me.tbar = {
            // xtype: 'toolbar',
            // items: [{
                // xtype: 'button',
                // text: 'Spawn Tab',
                // listeners: {
                    // click: {
                        // fn: function(btn) {
                            // var me = this;
                            // var baseTabPanel = btn.up('tab-basecardpanel');
                            // var title = " Detail " + me.counter;
                            // me.counter++;
                            // var detailPanel = Ext.create(me.detailClass, {
                                // title: title,
                                // html: title
                            // });
                            // baseTabPanel.add(detailPanel);
                            // baseTabPanel.setActiveItem(detailPanel);
                        // },
                        // scope: me
                    // }
                // }
            // }]
        // };
        me.callParent(arguments);
    }
});