Ext.define('DM2.view.component.tab.Deals', {
    extend: 'DM2.view.component.tab.BaseCardPanel',
    xtype: 'tab-deals',
    requires: [
        'DM2.view.component.tab.deal.Layout'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.ui = "customTab";
        me.cls = "customTabBar";
        me.items = [{
            title: 'Deal Grid',
            xtype: 'tab-deal-layout'
        }];
        me.callParent(arguments);
    }
});