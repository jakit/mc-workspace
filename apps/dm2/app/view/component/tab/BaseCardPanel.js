Ext.define('DM2.view.component.tab.BaseCardPanel', {
    extend: 'Ext.tab.Panel',
    xtype: 'tab-basecardpanel',
    requires: [],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.ui = 'customTab';
        me.callParent(arguments);
    },
	tabBar: {
        xtype: 'tabbar',
        ui: 'customTab'
    }
});