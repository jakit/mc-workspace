Ext.define('DM2.view.component.tab.BaseDetailPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'tab-basedetailpanel',
    requires: [],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.closable = true;
        //var name = me.record.data.name;
		var name = me.record.data.dealName;
        //var cleanName = name.replace(/\s\s+/g, ' ');
		var cleanName = '';
		if(name){
			cleanName = name.replace(/\s\s+/g, ' ');
		}
        var title;
        if(cleanName.length < 1) {
            //title = me.record.data.dealid;
			title = me.record.data.idDeal;
        } else {
            title = cleanName
        }        
        me.tabConfig = {
            tooltip: title
            // tooltip: "No: " + me.record.data.dealid + ", Name" + me.record.data.name
        };
        me.callParent(arguments);
    }
});