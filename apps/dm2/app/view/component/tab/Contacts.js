Ext.define('DM2.view.component.tab.Contacts', {
    extend: 'DM2.view.component.tab.BaseCardPanel',
    xtype: 'tab-contacts',
    requires: [
        'DM2.view.component.tab.contact.Layout'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
        me.ui = "default";
        me.cls = "customTabBar";
        me.items = [{
			title: 'Contact Grid',
            xtype: 'tab-contact-layout'
        }];
        me.callParent(arguments);
    }
});