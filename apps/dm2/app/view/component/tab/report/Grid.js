Ext.define('DM2.view.component.tab.report.Grid', {
    extend: 'DM2.view.component.tab.BaseGridPanelNew',
    xtype: 'tab-report-grid',
    requires: [],
    uses: [
        'DM2.view.component.tab.report.Detail'    
    ],    
    counter: 1,
    detailClass: 'DM2.view.component.tab.report.Detail',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };        
        
		me.columns = [
			{
				xtype: 'gridcolumn',
				dataIndex: 'string',
				text: 'String'
			},
			{
				xtype: 'numbercolumn',
				dataIndex: 'number',
				text: 'Number'
			},
			{
				xtype: 'datecolumn',
				dataIndex: 'date',
				text: 'Date'
			},
			{
				xtype: 'booleancolumn',
				dataIndex: 'bool',
				text: 'Boolean'
			}
		];
        me.callParent(arguments);
    }
});