Ext.define('DM2.view.component.tab.Properties', {
    extend: 'DM2.view.component.tab.BaseCardPanel',
    xtype: 'tab-properties',
    requires: [
        'DM2.view.component.tab.property.Layout'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.ui = "default";
        me.cls = "customTabBar";
        me.items = [{
			title: 'Property Grid',
            xtype: 'tab-property-layout'
        }];
        me.callParent(arguments);
    }
});