Ext.define('DM2.view.component.tab.BaseLayoutContainer', {
    extend: 'Ext.panel.Panel',
    xtype: 'tab-baselayoutcontainer',
    requires: [
        'Ext.layout.container.Border',
        'Ext.layout.container.Fit' 
    ],
    centerRegion: {},
    southRegion: {},
    eastRegion: {},
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this,
        centerRegion, southRegion, eastRegion;

        me.layout = {
            type: "border"
        };
        centerRegion = {
            region: 'center'
        };
        Ext.apply(centerRegion, me.centerRegion);
        
        southRegion = {
            height: '25%',
            region: 'south',
            collapsible: true,
            header: false,
            split: true
        };
        Ext.apply(southRegion, me.southRegion);
        
        eastRegion = {
            region: 'east',
            width: 400,
            collapsible: true,
            split: true,
            layout: {
                type: 'fit'
            }
        };
        Ext.apply(eastRegion, me.eastRegion);
        
        me.items = [{
            region: 'center',
            xtype: 'container',
            layout: {
                type: 'border'
            },
            items: [ centerRegion, southRegion ]
        }, eastRegion ];
        
        me.callParent(arguments);
    }
});

            