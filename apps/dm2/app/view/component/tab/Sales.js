Ext.define('DM2.view.component.tab.Sales', {
    extend: 'DM2.view.component.tab.BaseCardPanel',
    xtype: 'tab-sales',
	itemId: 'tab-sales',
    requires: [
        'DM2.view.component.tab.sales.Layout'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.ui = "customTab";
        me.cls = "customTabBar";
        me.items = [{
            title: 'Sales Grid',
            xtype: 'tab-sales-layout'
        }];
        me.callParent(arguments);
    }
});