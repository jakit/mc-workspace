Ext.define('DM2.view.component.tab.bank.Detail', {
    extend: 'DM2.view.component.tab.BaseDetailPanel',
    xtype: 'tab-bank-detail',
    requires: [],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };          
        
        me.callParent(arguments);
    }
});