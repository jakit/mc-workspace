Ext.define('DM2.view.component.tab.bank.Layout', {
    extend: 'DM2.view.component.tab.BaseLayoutContainer',
    xtype: 'tab-bank-layout',
    requires: [
        'DM2.view.component.tab.bank.Grid',
        'DM2.view.BankTabPanel'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'tab-bank-grid'
        };
        
        me.southRegion = {
            xtype: 'banktabpanel'/*,
            notesStore: 'Notes',
            dealHistoryStore: 'DealHistory',
            usersStore: "DealUsers"*/
        };
        
        me.eastRegion = {
            xtype: 'panel',
            title: 'Bank Details',
			layout: 'card',
			//header: false,
			itemId:'bank-east-panel',
            items: [
                {
                    xtype: 'bankdetailsform'
                }
            ]            
        };
       
        me.callParent(arguments);
    }
});