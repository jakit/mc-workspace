Ext.define('DM2.view.component.tab.bank.Grid', {
    extend: 'DM2.view.component.tab.BaseGridPanelNew',
    xtype: 'tab-bank-grid',
    requires: [],
    uses: [
        'DM2.view.component.tab.bank.Detail'    
    ],
    counter: 1,
    detailClass: 'DM2.view.component.tab.bank.Detail',
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
		me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
        me.store = 'Bankallbuff';
		me.firstLoad = true;
		me.viewConfig = {
            //itemId: 'bankmenugridview',
            forceFit: true
        };
        me.columns = [{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'fullName',
            text: 'Full Name',
			flex:1,
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'shortName',
            text: 'Short Name',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
			hidden:true,
            dataIndex: 'idBank',
            text: 'Bank Id',
            headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'meridianBank',
            text: 'Meridian Bank',
			flex:.25,
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "true",
                    'val': "true"
                }, {
                    'display': "false",
                    'val': "false"
                }]
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'office',
			flex:.25,
			align:'center',
            text: 'Office',
            headerField: me.setFilterField("combocheckboxfield", {
                grid:me,
                inlineData: Ext.getStore('Office').getData(),
				filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'address',
            text: 'Address',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			flex:.25,
            dataIndex: 'state',
            text: 'State',
			align:'center',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: Ext.getStore('StatesList').getData(),
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'city',
            text: 'City',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			flex:.25,
            dataIndex: 'zip',
            text: 'Zip',
            headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'payoutNoticeDays',
            text: 'PayoutNoticeDays',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'defaultExecution',
            text: 'DefaultExecution',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
			hidden:true,
            dataIndex: 'createUser',
            text: 'CreateUser',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            //xtype: 'gridcolumn',
			hidden:true,
            dataIndex: 'modifyUser',
            text: 'ModifyUser',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            xtype: 'datecolumn',
			hidden:true,
            dataIndex: 'createDate',
            text: 'CreateDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },
        {
            xtype: 'datecolumn',
			hidden:true,
            dataIndex: 'modifyDate',
            text: 'ModifyDate',
            format: 'm-d-Y',
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        }/*,
        {
            xtype: 'gridcolumn',
			hidden:true,
            dataIndex: 'modifyTime',
            text: 'ModifyTime'
        }*/];   

        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
	    var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];  
       
        me.callParent(arguments);
    }
});