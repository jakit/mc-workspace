Ext.define('DM2.view.component.tab.Reports', {
    extend: 'DM2.view.component.tab.BaseCardPanel',
    xtype: 'tab-reports',
    requires: [
        'DM2.view.component.tab.report.Grid'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.ui = "default";
        me.cls = "customTabBar";
        me.items = [{
			title: 'Report Grid',
            xtype: 'tab-report-grid'
        }];
        me.callParent(arguments);
    }
});