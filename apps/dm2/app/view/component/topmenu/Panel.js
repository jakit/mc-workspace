Ext.define('DM2.view.component.topmenu.Panel', {
    extend: 'Ext.panel.Panel',
    xtype: 'topmenu-panel',
    requires: [],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.html = "menu";
        me.callParent(arguments);
    }
});