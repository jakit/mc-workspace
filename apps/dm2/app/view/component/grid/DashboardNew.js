Ext.define('DM2.view.component.grid.DashboardNew', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cmp-grid-dashboard-new',
    requires: [
        'Ext.grid.column.Column',
        'Ext.grid.column.Date',
		'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.XTemplate',
        'Filters2.HeaderFields'        
    ],
    mixins: ['Filters2.InjectHeaderFields', 'Filters2.FiltersMixin'],
    scrollable: "y",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
		me.emptyText  = "No data is available";
        me.header = false;
        me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
        var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];            
        me.callParent(arguments);  
   }    
});