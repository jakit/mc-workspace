Ext.define('DM2.view.component.container.DealsDetailsContextMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealsdetailscontextmenu',
    requires: [
        'Ext.menu.Item',
        'Ext.menu.Menu'
    ],
    rec: null,
    setRec: function(rec) {
        var me = this;
        me.rec = rec;
    },
    getRec: function() {
        var me = this;
        return me.rec;
    },
    clearRec: function() {
        var me = this;
        me.rec = null;  
    },
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);        
    },
    initComponent: function() {
        var me = this;
        me.width = 170;
        me.callParent(arguments);
    }

});