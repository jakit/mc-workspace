/*
 * Based on ListToDetail.js
 * 
 * a list layout and a detail layout
 * me.listLayout ref as itemId: 'listLayout'
 * me.detailLayout ref as itemId: 'detailLayout'
 * layoutFocus can be set as 'listLayout or 0' or 'detailLayout or 1'
 * switchPanel(index) // 0 or 1
 * setData(data) // this will set the data property
 * getData() // this will retrieve the data property
 * 
 */
Ext.define('DM2.view.component.container.GridToForm', {
    extend: 'DM2.view.component.container.ListToDetail',
    alias: 'widget.cmp-container-gridtoform',
    requires: [
        'Ext.grid.Panel',
        'Ext.form.Panel',
        'Ext.selection.RowModel',
        'Ext.layout.container.Anchor',
        'Ext.form.field.Text',
        'Ext.tip.QuickTip'      
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    createModel: function(data) {
        var fields = [];
        var field;
        for(field in data) {
            fields.push({
                name: field
            });
        };
        var store = Ext.create('Ext.data.Store', {
           fields: fields,
           data: data 
        });
        return store.getAt(0);
        
    },
    loadForm: function(scope) {
        var detailLayout = scope.down('form[itemId="detailLayout"]'),
            form = detailLayout.getForm(),
            data = scope.getData();
        
        if(!(data.store && data.store.data)) {
            data = this.createModel(data);
        }  
        form.loadRecord(data);
		if(scope.xtype && scope.xtype=="dealdetailmasterloanfieldset"){
			if(data.get('DealMakerBankName')==null || data.get('DealMakerBankName')==""){
				form.findField('BankName').setFieldLabel("Bank (F)");
			}
		}
        scope.switchPanel(1);
    },
    contextMenu: null,
    detailOnSingleRecord: true,
    initComponent: function() {
        var me = this,
            listLayout,
            detailLayout,
            x, c, l;        
        
        Ext.tip.QuickTipManager.init();

        listLayout = {
            xtype: 'grid',
            viewConfig: {
                forceFit: true,
				getRowClass: function (record, rowIndex, rowParams, store) {
					if(this.up().up().getItemId()=="saledealdetailsalesfieldSet"){
						var rowclsval = "";
						console.log("saleType = "+record.get('saleType')+" "+rowIndex);
						var saleType = record.get('saleType');
						if(saleType!='S' && saleType!='O'){  // if name column is blank
							rowclsval = rowclsval+ 'hide-row-expander';
						}
						return rowclsval;
					}
				}
            },
            selModel: {
                selType: 'rowmodel',
                mode: 'SINGLE'
            }
        };
        
        detailLayout = {
            xtype: 'form',
            layout: {
                type: 'anchor',
                anchor: '100%'
            },
            defaults: {
                margin: "5",
                readOnly: true,
                xtype: 'textfield',
                anchor: '100%'
            }
                        
        };

        listLayout.listeners = {
            'cellclick': {
                fn: function(view, td, cellIndex, rec, tr, rowIndex, e) {
					console.log("cellclick");
					console.log(e.button);
					if(e.button == 2) {      
						/*console.log("cancel rightclick");						
						  e.preventDefault();            
						  e.stopPropagation();            
						  e.stopEvent();            
						  console.log("Fire rightclick");
						  view.fireEventArgs('rowcontextmenu',arguments);
						  return false;        */
					}else{
						var me = view.up('cmp-container-listtodetail');
						if(me.xtype && me.xtype=="saledealdetailsalesfieldSet"){
						}else{
							me.setData(rec);   
							this.loadForm(me);                        
						}
					}
                },
                scope: me
            }                
        };

   //     debugger;

        if(me.contextMenu) {
            if(!detailLayout.listeners) {
                detailLayout.listeners = {};
            }
            
            Ext.apply(detailLayout.listeners, {
                'afterrender': {
                    fn: function(cmp) {
                        
                        var me = this,
                            form, data,
                            grid = cmp.up().down('grid'),
                            c = grid.getStore().getCount(),
                            rec;
                        
                        // CG: Seems like useless because of how we load data in this app    
                        // // ** this was initially in 'beforerender'
                        // if(grid.up().detailOnSingleRecord && c == 1) {
                            // rec = grid.getStore().getAt(0);
                            // grid.up('cmp-container-listtodetail').setData(rec);
                        // } else {
                            // grid.up().detailOnSingleRecord = false;
                        // }                        
                        // // ** //
                        
                        if(me.contextMenu) {
                            cmp.getEl().on('contextmenu', function(e, el, obj) {
                                e.preventDefault();
								
								var tabItem = DM2.app.getController('DealDetailController').getTabContext(cmp);
							    DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,this.contextMenu);
								DM2.app.getController('DealDetailController').disableAddEditMenu(this);
								
								this.contextMenu.showAt([e.pageX, e.pageY]);
								if(this.getItemId()=="dealdetailcontactfieldset"){
									var cntid = cmp.getForm().findField('contactid').getValue();
									DM2.app.getController('ContactController').buildContactSubMenu(cntid,this.contextMenu);
									//DM2.app.getController('ContactController').loadPhoneRecords(cntid);
									//DM2.app.getController('ContactController').loadEmailRecords(cntid);
								}
								if(this.getItemId()=="saledealdetailsalesfieldSet"){
									var salesrec = this.getData();
									if(salesrec.get('saleType')==='S'){
										this.contextMenu.down('menuitem[action="addbid"]').show();
									} else {
										this.contextMenu.down('menuitem[action="addbid"]').hide();
									}
								}
								if(this.getItemId()=="dealdetailpropertyfieldset"){
									var proprec = this.getData();
									if(proprec.get('primaryProperty')===true){
										this.contextMenu.down('menuitem[action="removeproperty"]').disable();
									} else {
										this.contextMenu.down('menuitem[action="removeproperty"]').enable();
									}
									this.contextMenu.down('menuitem[action="changeprimary"]').disable();
									////
									if(this.up('tab-sales-detail')){
										this.contextMenu.down('menuitem[action="editproperty"]').disable();
									} else {
										this.contextMenu.down('menuitem[action="editproperty"]').enable();
									}
									////
								}
                            }, me);
							cmp.getEl().on('longpress', function(e, el, obj) {
                                e.preventDefault();
								
								var tabItem = DM2.app.getController('DealDetailController').getTabContext(cmp);
							    DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,this.contextMenu);
								DM2.app.getController('DealDetailController').disableAddEditMenu(this);
								
								this.contextMenu.showAt([e.pageX, e.pageY]);
								if(this.getItemId()=="dealdetailcontactfieldset"){
									var cntid = cmp.getForm().findField('contactid').getValue();
									DM2.app.getController('ContactController').buildContactSubMenu(cntid,this.contextMenu);
									//DM2.app.getController('ContactController').loadPhoneRecords(cntid);
									//DM2.app.getController('ContactController').loadEmailRecords(cntid);
								}
								if(this.getItemId()=="saledealdetailsalesfieldSet"){
									var salesrec = this.getData();
									if(salesrec.get('saleType')==='S'){
										this.contextMenu.down('menuitem[action="addbid"]').show();
									} else {
										this.contextMenu.down('menuitem[action="addbid"]').hide();
									}
								}
								if(this.getItemId()=="dealdetailpropertyfieldset"){
									var proprec = this.getData();
									if(proprec.get('primaryProperty')===true){
										this.contextMenu.down('menuitem[action="removeproperty"]').disable();
									} else {
										this.contextMenu.down('menuitem[action="removeproperty"]').enable();
									}
									this.contextMenu.down('menuitem[action="changeprimary"]').disable();
									////
									if(this.up('tab-sales-detail')){
										this.contextMenu.down('menuitem[action="editproperty"]').disable();
									} else {
										this.contextMenu.down('menuitem[action="editproperty"]').enable();
									}
									////
								}
                            }, me);
                        }
						cmp.getEl().on('click', function(e, el, obj) {
							e.preventDefault();
							
							var tabItem = DM2.app.getController('DealDetailController').getTabContext(cmp);
							
							if(this.getItemId()!="saledealdetailsalesfieldSet"){
								var cnt = this.down('grid').getStore().getCount();
								if(cnt>1){
									console.log("Close Form on Left Click : "+cnt);
									this.switchPanel(0);
								}
							}
                        }, me); 
                        // needs to be determined at the controller level.
                        // if(me.detailOnSingleRecord == true) {
                            // form = cmp.getForm();
                            // data = cmp.up().getData();
                            // form.loadRecord(data);
                            // cmp.up().switchPanel(1);
                        // }
                    },
                    scope: me
                    
                } 
            });
            
            Ext.apply(listLayout.listeners, {
                'rowcontextmenu': {
                    fn: function(grid, rec, tr, rowIdx, e, opts) {
						console.log("rowcontextmenu");
                        e.stopEvent();
						
						var tabItem = DM2.app.getController('DealDetailController').getTabContext(grid.ownerCt);
						DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,this.contextMenu);
						DM2.app.getController('DealDetailController').disableAddEditMenu(this);
						
						var listtodetail = grid.ownerCt.up('cmp-container-listtodetail');
						listtodetail.setData(rec); 
						
                        this.contextMenu.setRec(rec);
                        this.contextMenu.showAt(e.getXY());
						if(this.getItemId()=="dealdetailpropertyfieldset"){
							var proprec = rec;
							if(proprec && !this.contextMenu.down('menuitem[action="removeproperty"]').isDisabled()){
								if(proprec.get('primaryProperty')===true){
									this.contextMenu.down('menuitem[action="removeproperty"]').disable();
								} else {
									this.contextMenu.down('menuitem[action="removeproperty"]').enable();
								}
							}
							this.contextMenu.down('menuitem[action="changeprimary"]').disable();
							////
							if(!this.contextMenu.down('menuitem[action="editproperty"]').isDisabled()){
								if(this.up('tab-sales-detail')){
									console.log("Disable Change Mortgage type");
									this.contextMenu.down('menuitem[action="editproperty"]').disable();
								} else {
									console.log("Enable Change Mortgage type");
									this.contextMenu.down('menuitem[action="editproperty"]').enable();
								}
							}
							////
						}
                    },
                    scope: me
                },
                'containercontextmenu': {
                    fn: function(grid, e, opts) {
					   console.log("containercontextmenu");
                       e.preventDefault();
					   /*
					   var tabItem = DM2.app.getController('DealDetailController').getTabContext(grid.ownerCt);					   
					   DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,this.contextMenu);
                       
					   this.contextMenu.showAt(e.getXY());*/
                    },
                    scope: me
                },
				'afterrender': {
                    fn: function(panel) {
                        panel.getEl().on("contextmenu",function(e, t, eOpts) {
							console.log("Show Menu After Render of list layout.");
							e.stopEvent();
							var tabItem = DM2.app.getController('DealDetailController').getTabContext(panel);							
							var mainCmp = panel.up('cmp-container-gridtoform');
						    DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,mainCmp.contextMenu);
							DM2.app.getController('DealDetailController').disableAddEditMenu(mainCmp);
						    mainCmp.contextMenu.showAt(e.getXY());							
							if(mainCmp.getItemId()=="dealdetailpropertyfieldset"){
								var proprec = mainCmp.getData();
								if(proprec && !mainCmp.contextMenu.down('menuitem[action="removeproperty"]').isDisabled()){
									if(proprec.get('primaryProperty')===true){
										mainCmp.contextMenu.down('menuitem[action="removeproperty"]').disable();
									} else {
										mainCmp.contextMenu.down('menuitem[action="removeproperty"]').enable();
									}
								}
								mainCmp.contextMenu.down('menuitem[action="changeprimary"]').disable();
								////
								if(!mainCmp.contextMenu.down('menuitem[action="editproperty"]').isDisabled()){
									if(mainCmp.up('tab-sales-detail')){
										console.log("Disable Change Mortgage type");
										mainCmp.contextMenu.down('menuitem[action="editproperty"]').disable();
									} else {
										console.log("Enable Change Mortgage type");
										mainCmp.contextMenu.down('menuitem[action="editproperty"]').enable();
									}
								}
								////
							}
						});
						panel.getEl().on("longpress",function(e, t, eOpts) {
							console.log("Show Menu After Render of list layout.");
							e.stopEvent();
							var tabItem = DM2.app.getController('DealDetailController').getTabContext(panel);							
							var mainCmp = panel.up('cmp-container-gridtoform');
						    DM2.app.getController('DealDetailController').disableModifyMenu(tabItem,mainCmp.contextMenu);
							DM2.app.getController('DealDetailController').disableAddEditMenu(mainCmp);
						    mainCmp.contextMenu.showAt(e.getXY());							
							if(mainCmp.getItemId()=="dealdetailpropertyfieldset"){
								var proprec = mainCmp.getData();
								if(proprec && !mainCmp.contextMenu.down('menuitem[action="removeproperty"]').isDisabled()){
									if(proprec.get('primaryProperty')===true){
										mainCmp.contextMenu.down('menuitem[action="removeproperty"]').disable();
									} else {
										mainCmp.contextMenu.down('menuitem[action="removeproperty"]').enable();
									}
								}
								mainCmp.contextMenu.down('menuitem[action="changeprimary"]').disable();
								////
								if(!mainCmp.contextMenu.down('menuitem[action="editproperty"]').isDisabled()){
									if(mainCmp.up('tab-sales-detail')){
										console.log("Disable Change Mortgage type");
										mainCmp.contextMenu.down('menuitem[action="editproperty"]').disable();
									} else {
										console.log("Enable Change Mortgage type");
										mainCmp.contextMenu.down('menuitem[action="editproperty"]').enable();
									}
								}
								////
							}
						});
                    }
                }
            });
        }
        
        Ext.apply(me.listLayout, listLayout);
        Ext.apply(me.detailLayout, detailLayout);

        // adding tooltips to the cells        
        x = 0;
        c = me.listLayout.columns;
        l = c.length;
        
        function defRenderer (val, meta, rec, rowIdx, colIdx, store, view) {
            meta.tdAttr = 'data-qtip="' + val + '"';
            return val;    
        };
		function defNumRenderer (val, meta, rec, rowIdx, colIdx, store, view) {
            /*var formatVal = Ext.util.Format.number(val,'0,000');
			//console.log("Rend Value"+formatVal);
			meta.tdAttr = 'data-qtip="' + formatVal + '"';
			return formatVal;*/
			//console.log("Check"+val);
			if(val==null || val==0){
				return null;  
			}else{
				var formatVal = Ext.util.Format.currency(val, '$',0);
				meta.tdAttr = 'data-qtip="' + formatVal + '"';
				return formatVal;  
			}
        };
		function defDateRenderer (val, meta, rec, rowIdx, colIdx, store, view) {
			if(val==null || val==0){
				return null;  
			}else{
				var formatVal = Ext.util.Format.date(val, "m/d/Y");
				meta.tdAttr = 'data-qtip="' + formatVal + '"';
				return formatVal;
			}
        };
        
        for (x = 0; x < l; x++) {
			//console.log("xtype");
			//console.log(c[x].xtype);
			if(c[x].xtype=="numbercolumn"){
				c[x].renderer = defNumRenderer;
			}else if(c[x].xtype=="datecolumn"){
				c[x].renderer = defDateRenderer;
			}else{
            	c[x].renderer = defRenderer;
			}
        }
        
        me.callParent(arguments);  
   }
    
});