Ext.define('DM2.view.ContactMiscGridPanelOld', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.contactmiscgridpanelold',

    requires: [
        'Ext.form.field.Text',
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel'
    ],

    frame: true,
    margin: '0 0 0 0',
    ui: 'activitypanel',
    collapsible: true,
    title: 'Misc',
    store: 'ContactMiscs',

    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'tag',
            text: 'Tag',
            flex: 1,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'miscValue',
            text: 'MiscValue',
            flex: 1,
            editor: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'isPrimary',
            text: 'IsPrimary',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'add',
                    iconCls: 'add',
                    text: 'Add'
                },
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'delete',
                    iconCls: 'delete',
                    text: 'Delete'
                }
            ]
        }
    ],
    plugins: [
        {
            ptype: 'cellediting'
        }
    ],
    selModel: {
        selType: 'cellmodel'
    }

});