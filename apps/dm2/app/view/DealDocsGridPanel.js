Ext.define('DM2.view.DealDocsGridPanel', {
    //extend: 'Ext.grid.Panel',
	extend: 'DM2.view.component.grid.DashboardNew',
    alias: 'widget.dealdocsgridpanel',

    requires: [
        'DM2.view.override.DealDocsGridPanel',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.form.field.File',
		'DM2.view.component.grid.DashboardNew'
    ],
	mixins: ['Filters2.InjectHeaderFields', 'Filters2.FiltersMixin'],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);        
    },
	initComponent: function() {
        var me = this;
        me.cls = 'gridcommoncls';
	    me.title = 'Documents';
		me.store = Ext.create('DM2.store.Documents');
    	me.columns = [
        {
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'docid',
            text: 'Doc Id',
            flex: 1,
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },
        {
            xtype: 'datecolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                return Ext.util.Format.date(record.get('modifydate'), "m/d/Y h:i:s");
            },
            dataIndex: 'modifydate',
            text: 'Date/Time',
            flex: 1,
			headerField: me.setFilterField("datefield", {
                field1: {
                    grid: me,
                    filterMode: "On",
                    filterDataType: "date"
                },
                field2: {
                    grid: me
                }
            })
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				var docurlname = record.get('url');
                var ext = DM2.app.getController('MainController').getExtImg(docurlname);
				var targetframe = DM2.app.getController('MainController').getTargetFrame(record.get('ext'));				
                return '<a class="doclink" style="color: -webkit-link;text-decoration: underline;cursor: pointer;" target="'+targetframe+'" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
				/*return '<span class="doclink" style="color: -webkit-link;text-decoration: underline;cursor: pointer;"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</span>';*/
            },
            dataIndex: 'name',
            text: 'Document Name',
            flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        },
        {
            xtype: 'gridcolumn',
            dataIndex: 'size',
            text: 'Size',
            flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'desc',
            text: 'Description',
            flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'clsfctn',
            text: 'Classification',
            flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'modifiedBy',
            text: 'By',
            flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        },
        {
            xtype: 'gridcolumn',
			hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assoctype',
            text: 'Assoc Type',
            flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        },
        {
            xtype: 'gridcolumn',
			hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assocdesc',
            text: 'Assoc Desc',
            flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
        }
    ];
		me.viewConfig = {
			getRowClass: function(record, rowIndex, rowParams, store) {
				if(record && record.get('assoctype') ===  "Property"){
					return 'property-doc-color-cls';
				}
			}
		};
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				hidden: true,
				itemId: 'docgridtoolbar',
				items: [
					{
						xtype: 'button',
						itemId: 'adddocfilefieldbtn',
						text: 'Add Document'
					}
				]
			}
		];
		
		me.callParent(arguments);
    }
});