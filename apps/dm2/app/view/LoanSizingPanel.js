Ext.define('DM2.view.LoanSizingPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loansizingpanel',

    requires: [
        'DM2.view.LoanSizingDetailsForm',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.form.field.Number',
        'Ext.form.Panel'
    ],
	
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.frame  = true;
		me.itemId = 'loansizingpanel';
		me.margin = 3;
		me.ui     = 'activitypanel';
		me.layout = 'border';
		me.closable = true;
		me.title  = 'Activities > ';
		
		var dealpropertystore = Ext.create('DM2.store.Dealproperty');
		
		me.items = [
			{
				xtype: 'gridpanel',
				scrollable:true,
				region: 'north',
				split: true,
				cls: 'gridcommoncls',
				itemId: 'loanpropertygridpanel',
				maxHeight: 300,
				collapsible: true,
				header: false,
				columnLines: true,
				multiColumnSort: true,
				sortableColumns: false,
				store: dealpropertystore,
				columns: [
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'street_no',
						text: 'No.',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'street',
						text: 'Street',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'city',
						text: 'City',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'state',
						text: 'State',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						hidden: true,
						dataIndex: 'buildingClass',
						text: 'Bldg Cls',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						hidden: true,
						dataIndex: 'propertyType',
						text: 'Property Type',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'estimatedloansize',
						text: 'Loan Size',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'income',
						text: 'Income',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'expenses',
						text: 'Expenses',
						flex: 1
					}
				],
				dockedItems: [
					{
						xtype: 'toolbar',
						dock: 'top',
						items: [
							{
								xtype: 'button',
								itemId: 'loansizingdonebtn',
								text: 'Done'
							}
						]
					},
					{
						xtype: 'toolbar',
						dock: 'bottom',
						items: [
							{
								xtype: 'currencyfield',
								itemId: 'totalloan',								
								width: '100%',
								fieldLabel: 'Total Loan',
								labelWidth: 80,
								name: 'totalloan',
								readOnly: true
							}
						]
					}
				]
			},
			{
				xtype: 'panel',
				region: 'center',
				split: true,
				itemId: 'loansizingbottompanel',
				layout: 'card',
				collapsible: true,
				header: false,
				items: [
					{
						xtype: 'loansizingdetailsform'
					}
				]
			}
		];
		
		me.callParent(arguments);
    }
});