Ext.define('DM2.view.GeneralDocsSelRepositoryGridPanel', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    alias: 'widget.generaldocsselrepositorygridpanel',

    requires: [
		'DM2.view.component.grid.SelectionGridNew',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent : function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
		me.cls = 'gridcommoncls';
		me.itemId = 'generaldocsselrepositorygridpanel';
		me.title = 'Documents';
		me.store =  Ext.create('DM2.store.Docsall');
		//me.scrollable = true;
		me.columns = [
			{
				xtype: 'gridcolumn',
				hidden: true,
				dataIndex: 'idDocument',
				text: 'Doc Id',
				flex: 1,
				headerField: me.setFilterField("stringfield", {
					grid: me
				})
			},
			{
				xtype: 'datecolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					return Ext.util.Format.date(record.get('modifyDateTime'), "m/d/Y h:i:s");
				},
				dataIndex: 'modifyDateTime',
				text: 'Date/Time',
				flex: 1,
				headerField: me.setFilterField("datefield", {
					field1: {
						grid: me,
						filterMode: "On",
						filterDataType: "date"
					},
					field2: {
						grid: me
					}
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var docurlname = record.get('url');
					var ext = DM2.app.getController('MainController').getExtImg(docurlname);
					return '<a target="_blank" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
				},
				dataIndex: 'name',
				text: 'Document Name',
				flex: 1,
				headerField: this.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'size',
				text: 'Size',
				flex: 1,
				headerField: this.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'description',
				text: 'Description',
				flex: 1,
				headerField: this.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'classification',
				text: 'Classification',
				flex: 1,
				headerField: this.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'modifyUserName',
				text: 'By',
				flex: 1,
				headerField: this.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			}
		];
		me.callParent(arguments);
    }
});