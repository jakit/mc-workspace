Ext.define('DM2.view.Underwriting', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.underwriting',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.XTemplate'
    ],

	underwrittingActionsStore : null,
	underwritingStore : null,
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.frame = true;
		
		me.itemId = 'underwriting';
		//me.margin = 3;
		me.ui = 'activitypanel';
		//me.layout = 'border';
		me.layout = {
			type:'vbox',
			align:'stretch'
		};
		//me.closable = true;
		//me.collapsible = true;
		me.title = 'Underwriting';
		var dealPropertySt = Ext.create('DM2.store.Dealproperty');
		
		me.underwrittingActionsStore = Ext.create('DM2.store.UnderwrittingActions');
		me.underwritingStore = Ext.create('DM2.store.Underwriting');
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					hidden:true,
					action: 'doneActionBtn',
					margin: '0 10 0 0',
					text: 'Done'
				}
			]
		};
		me.items = [
			{
				xtype: 'form',
				style:'border-bottom-color: #cecece;border-bottom-width: 1px;border-bottom-style: solid;',
				//region: 'north',
				//split: true,
				itemId: 'propertydetailfrm',
				scrollable: 'true',
				bodyPadding: 10,
				collapsible: true,
				header: false,
				title: 'Property',
				items: [
					{
						xtype: 'combobox',
						tpl: '<tpl for="."><div class="x-boundlist-item">{street_no} - {street}</div></tpl>',
						anchor: '100%',
						itemId: 'propertycombo',                    
						fieldLabel: 'Property',
						labelWidth: 60,
						displayField: 'street',
						displayTpl: [
							'<tpl for=".">',
							'{street_no} - {street}',
							'</tpl>'
						],
						queryMode: 'local',
						store: dealPropertySt,
						typeAhead: true,
						//valueField: 'idDealxProp'
						valueField: 'idSetup'
						//valueField: 'propertyid'
					},
					{
						xtype: 'panel',
						layout: 'column',
						items: [
							{
								xtype: 'panel',
								columnWidth: 0.5,
								margin: '0 5 0 0',
								layout: {
									type: 'vbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'textfield',                                    
										fieldLabel: 'No',
										labelWidth: 60,
										name: 'street_no',
										readOnly: true
									},
									{
										xtype: 'textfield',                                    
										fieldLabel: 'City',
										labelWidth: 60,
										name: 'city',
										readOnly: true
									},
									{
										xtype: 'textfield',                                    
										fieldLabel: 'Property Type',
										labelWidth: 60,
										name: 'propertytype',
										readOnly: true
									}
								]
							},
							{
								xtype: 'panel',
								columnWidth: 0.5,
								layout: {
									type: 'vbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'textfield',                                    
										fieldLabel: 'Street',
										labelAlign: 'right',
										labelWidth: 50,
										name: 'street',
										readOnly: true
									},
									{
										xtype: 'textfield',                                    
										fieldLabel: 'State',
										labelAlign: 'right',
										labelWidth: 50,
										name: 'state'
									},
									{
										xtype: 'textfield',                                    
										fieldLabel: 'Zip',
										labelAlign: 'right',
										labelWidth: 50,
										name: 'zip'
									}
								]
							}
						]
					}
				]
			},
			{
				/*xtype: 'panel',
				flex:1,
				//region: 'center',
				itemId: 'underwritingbtmpanel',
				layout: 'card',
				collapsible: true,
				header: false,
				items: [
					{*/
						xtype: 'panel',
						flex:1,
						//frame : true,
						//ui : 'activitypanel',
						itemId: 'actionspanel',
						scrollable: 'true',
						header: {
							titlePosition: 0,
							items: [
								{
									xtype: 'button',
									action: 'saveActionsBtn',
									margin: '0 10 0 0',
									text: 'Save'
								}/*,
								{
									xtype: 'button',
									action: 'resetActionsBtn',
									itemId: 'resetActionsbtn',
									text: 'Reset'
								},
								{
									xtype: 'button',
									margin: '0 0 0 10',
									action: 'clearActionsBtn',
									itemId: 'clearActionsbtn',
									text: 'Clear'
								}*/
							]
						},
						title: 'Actions',
						layout: {
							type: 'vbox',
							align: 'stretch'
						}
					/*}
				]*/
			}
		];
		me.callParent(arguments);
    }
});