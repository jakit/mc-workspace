Ext.define('DM2.view.PropertyContactOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.propertycontactoptionmenu',

    requires: [
        'Ext.menu.Item',
        'Ext.form.field.File',
        'Ext.form.field.FileButton'
    ],
	
    width: 160,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'propertyaddcontact',
            text: 'Add Contact',
            focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'propertyremovecontact',
            text: 'Remove Contact',
            focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'callcontact',
            text: 'Call Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 250
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'emailcontact',
            text: 'Email Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'faxcontact',
            text: 'Fax Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        }
    ]

});