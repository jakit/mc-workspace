Ext.define('DM2.view.DealDetailPropertyFieldSet', {
    extend: 'DM2.view.component.container.GridToForm',
    alias: 'widget.dealdetailpropertyfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
		'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.panel.Tool',
        'Ext.layout.container.Column',
        'DM2.view.DealDetailPropertiesMenu',
        'DM2.model.Property',
        'Ext.data.proxy.Rest'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        //me.frame = true;
        me.itemId = 'dealdetailpropertyfieldset';
        //me.ui = 'propertydealpanel';      
        
        me.contextMenu = Ext.create('DM2.view.DealDetailPropertiesMenu');
        
        // *************
        
        var store = Ext.create('Ext.data.Store', {
            model: 'DM2.model.Property',
            proxy: {
                type: 'rest',
                url: DM2.view.AppConstants.apiurl+'mssql:v_PropertyPerDealOnly'
            },
            autoLoad: false     
        });
        
        // ***************
        
        
        me.listLayout= {
            icon: 'resources/images/icons/property_m.png',
            title: "PROPERTY INFORMATION",     
      //      detailOnSingleRecord: true,       
            cls: 'gridcommoncls',
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: store,
			ui : 'propertydealpanel',
			frame:true,
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                width: 70,
                dataIndex: 'street_no',
                text: 'No.'
            }, {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'street',
                text: 'Street',
                flex: 1.2
            }, {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'city',
                text: 'City',
                flex: 0.8
            }, {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                width: 60,
                dataIndex: 'state',
                text: 'St.'
            }, {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'buildingClass',
                text: 'Bldg Cls',
                flex: 1
            }, {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'propertyType',
                text: 'Property Type',
                flex: 1
            },
			{
                xtype: 'checkcolumn',
                /*renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },*/
                hidden: true,
                dataIndex: 'primaryProperty',
                text: 'IsPrimary',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/property_m.png',
            title: "PROPERTY INFORMATION",     
            scrollable: true,
			ui : 'propertydealpanel',
			frame:true,
			items: [{
                xtype: 'panel',
                layout: {
                    type: 'column'
                },
                items: [{
                    xtype: 'panel',
                    columnWidth: 0.65,
                    margin: "0px 10px 0 5px",
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'panel',
                        margin: '0px 0px 6px 0px',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'label',
                            width: 56,
                            text: 'Address:'
                        }, {
                            xtype: 'textfield',
                            margin: '0 10 0 0',
                            width: 40,
                            hideLabel: true,
                            name: 'street_no',
                            itemId: 'street_no',
                            readOnly: true
                        }, {
                            xtype: 'textfield',
                            flex: 1,
                            hideLabel: true,
                            name: 'street',
                            itemId: 'street',
                            readOnly: true
                        }]
                    }, {
                        xtype: 'panel',
                        margin: '0px 0px 6px 0px',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'textfield',
                            flex: 1,
                            fieldLabel: 'City',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'city',
                            itemId: 'city',
                            readOnly: true
                        }, {
                            xtype: 'textfield',
                            flex: 1,
                            fieldLabel: 'State',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'state',
                            itemId: 'state',
                            readOnly: true
                        }]
                    }, {
                        xtype: 'panel',
                        margin: '0px 0px 6px 0px',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [{
                            xtype: 'textfield',
                            flex: 1,
                            fieldLabel: 'Boro',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'Boro',
                            itemId: 'Boro',
                            readOnly: true
                        }, {
                            xtype: 'textfield',
                            flex: 1,
                            fieldLabel: 'Block',
                            labelAlign: 'right',
                            labelWidth: 40,
                            name: 'block',
                            itemId: 'block',
                            readOnly: true
                        },{
							xtype: 'textfield',
							flex: 1,
							fieldLabel: 'Lot',
							labelAlign: 'right',
							labelWidth: 30,
							name: 'Lot',
							itemId: 'Lot',
							readOnly: true
						}]
                    }, {
                        xtype: 'panel',
                        margin: '0px 0px 6px 0px',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [{
							xtype: 'textfield',
							flex: 1,
							fieldLabel: 'APN',
							labelAlign: 'right',
							labelWidth: 50,
							name: 'APN',
							itemId: 'APN',
							readOnly: true
						},{
                            xtype: 'textfield',
                            flex: 1,
                            fieldLabel: 'Units',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'units',
                            itemId: 'units',
                            readOnly: true
                        }]
                    },{
						xtype: 'textfield',
						readOnly: true,
						//selectOnFocus: true,
						//displayField: 'selectionDesc',
						//queryMode: 'local',
						//store: 'MortgageTypeList',//ratetypelist_st,
						//typeAhead: true,
						//valueField: 'selectionDesc',                 
						fieldLabel: 'Mortgage Type',
						labelAlign: 'right',
						labelWidth: 95,
						name: 'mortgageType'
					}]
                }, {
                    xtype: 'panel',
                    columnWidth: 0.35,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'Prop type',
                        labelAlign: 'right',
                        labelWidth: 60,
                        name: 'propertytype',
                        itemId: 'propertytype',
                        readOnly: true
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Zip',
                        labelAlign: 'right',
                        labelWidth: 60,
                        name: 'zip',
                        itemId: 'zip',
                        readOnly: true
                    },{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Bldg Cls',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'buildingClass',
						itemId: 'buildingClass',
						readOnly: true
					},{
                        xtype: 'textfield',
                        fieldLabel: 'Stories',
                        labelAlign: 'right',
                        labelWidth: 60,
                        name: 'stories',
                        itemId: 'stories',
                        readOnly: true
                    },{
						xtype: 'checkboxfield',
						//flex: 1,
						fieldLabel: 'Primary',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'primaryProperty',
						itemId: 'primaryProperty',
						readOnly: true
					}/*{
                        xtype: 'panel',
                        margin: '0px 0px 6px 0px',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: []
                    }*/]
                }]
            }]
        };
        me.callParent(arguments);
    }

});