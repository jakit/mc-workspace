Ext.define('DM2.view.DealDetailQuotePanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.dealdetailquotepanel',

    requires: [
        'DM2.view.QuotesGridPanel',
		'DM2.view.QuoteDetailsPanel',
        'Ext.grid.Panel',
        'Ext.form.field.Number',
        'Ext.button.Button',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.grid.plugin.CellEditing',
        'Ext.form.FieldSet',
        'Ext.form.CheckboxGroup',
        'Ext.form.field.TextArea',
        'Ext.selection.CheckboxModel',
		'DM2.view.QuoteOptionsPanel',
		'DM2.view.StructuredGridPanel',
		'DM2.view.Underwriting'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.margin = 3;
		me.ui = 'activitypanel';
		me.layout = 'border';
		me.closable = true;
	 	me.submissionst = Ext.create('DM2.store.BanksSelected');
		me.items = [
			{
				xtype: 'quotesgridpanel',
				maxHeight: 225,
				collapsible: true,
				header: false,
				region: 'north',
				split: true
			},
			{
				xtype: 'panel',
				region: 'center',
				itemId: 'quoteeditorbtmpanel',
				layout: 'card',
				collapsible: true,
				header: false,
				cls: ['removenumcls'],
				items: [
					{
						xtype: 'quotedetailspanel'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});