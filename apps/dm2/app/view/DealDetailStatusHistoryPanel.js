Ext.define('DM2.view.DealDetailStatusHistoryPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealdetailstatushistorypanel',

    requires: [
        'Ext.grid.column.Check',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'DM2.model.DealStatusHistory',
        'Ext.data.proxy.Rest'
    ],
    
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    scrollable: true,
    initComponent: function() {
        var me = this;
        
        me.cls = 'gridcommoncls';
        
		var detailPanel,dealid;
		if(me.up('tab-deal-detail')){
			detailPanel = me.up('tab-deal-detail');
			dealid = detailPanel.dealid;
		} else if(me.up('tab-sales-detail')){
			detailPanel = me.up('tab-sales-detail');
			dealid = detailPanel.dealid;
		}

        var mainViewport = Ext.ComponentQuery.query('mainviewport')[0];
		
		me.store = Ext.create('DM2.store.DealStatusHistory');
        
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Status',
            text: 'Status',
            flex: 1
        },
        /*{
            xtype: 'checkcolumn',
            itemId: 'active',
            dataIndex: 'active',
            text: 'Active'
        },*/
        {
            xtype: 'datecolumn',
            dataIndex: 'Date',
            text: 'Date',
            flex: 1,
            format: 'm-d-Y'
        }, 
        /*{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'createUserID',
            text: 'UserID',
            flex: 1
        },*/
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'SetMode',
            text: 'SetMode',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'User',
            text: 'User',
            flex: 1
        }];        
        me.callParent(arguments);      
    }
});