Ext.define('DM2.view.TemplateSelGridPanel', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    alias: 'widget.templateselgridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.button.Button',
		'DM2.view.component.grid.SelectionGridNew'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
		me.cls = 'gridcommoncls';
		me.closable = true;
		me.title = 'Select Template';
		me.store = Ext.create('DM2.store.Templates'); //'Templates';

		me.columns = [
			{
				xtype: 'gridcolumn',
				hidden: true,
				dataIndex: 'idTemplate',
				text: 'ID',
				headerField: me.setFilterField("stringfield", {
					grid: me
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var docurlname = record.get('uri');
					var ext = DM2.app.getController('MainController').getExtImg(docurlname);
					var targetframe = DM2.app.getController('MainController').getTargetFrame(record.get('extension'));
					
					return '<a class="doclink" style="color: -webkit-link;text-decoration: underline;cursor: pointer;" target="'+targetframe+'" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
				},
				dataIndex: 'name',
				text: 'Name',
				flex: 1,
				headerField: this.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'description',
				text: 'Description',
				flex: 1,
				headerField: this.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			}
		];
		me.dockedItems = [
			{
				xtype: 'form',
				dock: 'top',
				bodyPadding: 10,
				header: false,
				items: [
					{
						xtype: 'panel',
						margin: '0 0 5 0',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: 'Name',
								labelWidth: 90,
								name: 'name'
							},
							{
								xtype: 'button',
								itemId: 'createtmpbtn',
								margin: '0 0 0 10',
								text: 'Create'
							}
						]
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Description',
						labelWidth: 90,
						name: 'description'
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Classification',
						labelWidth: 90,
						name: 'clsfctn',
						//readOnly: true,
						maxLength : 10,
						enforceMaxLength : true
					}
				]
			}
		];
		me.callParent(arguments);
    }
});