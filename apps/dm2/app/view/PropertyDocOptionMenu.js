Ext.define('DM2.view.PropertyDocOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.propertydocoptionmenu',

    requires: [
        'Ext.menu.Item',
        'Ext.form.field.File',
        'Ext.form.field.FileButton'
    ],

    viewModel: {
        type: 'docoptionmenu'
    },
    width: 160,

    items: [
		{
            xtype: 'menuitem',
            itemId: 'propertydocadd',
            text: 'Add Document',
			focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'propertydocedit',
            text: 'Edit Document',
            focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'propertydocdelete',
            text: 'Delete Document',
			focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			hidden:true,
            itemId: 'propertydochistory',
            text: 'Doc History',
            focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'emaildocument',
            text: 'Email Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'faxdocument',
            text: 'Fax Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'printdocument',
            text: 'Print Document',
            focusable: true,
			modifyMenu : 'no'
        },
		{
            xtype: 'menuitem',
            itemId: 'downloaddocument',
            text: 'Download Document',
            focusable: true,
			modifyMenu : 'no'
        }
    ]
});