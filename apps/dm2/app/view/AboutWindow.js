Ext.define('DM2.view.AboutWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.aboutwindow',

    requires: [
        'Ext.window.Window'
    ],

    height: 405,
    width: 505,
    layout: {
		type:'vbox',
		align:'center'
	},
	title: 'About',
	padding:15,
    items: [{
            xtype : 'image',
			style:'background-color:#184f89',
            autoEl : {
                tag : 'a',
                href : '/DealMaker/',
                target : '_self'
            },
            height : 40,
            width : 230,
            src : 'resources/images/logo.png'
        },
		{
            xtype : 'image',
			margin : '15 0 0 0',
			//style:'background-color:#184f89',
            autoEl : {
                tag : 'a',
                href : '/DealMaker/',
                target : '_self'
            },
            height : 77,
            width : 256,
            src : 'resources/images/DealLogo.png'
        },
        {
			itemId:'infopanel',
			style:'text-align:center'
        }
    ]

});