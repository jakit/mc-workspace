Ext.define('DM2.view.ApprovalGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.approvalgridpanel',

    requires: [
        'Ext.grid.column.Date',
        'Ext.grid.column.Check'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.itemId = 'approvalgridpanel';
		me.maxHeight = 200;
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.title = 'Modifications';
		me.store =  Ext.create('DM2.store.ApprovalDocs');//'ApprovalDocs';
		me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var docurlname = record.get('url');
					var ext = DM2.app.getController('MainController').getExtImg(docurlname);
					return '<a target="_blank" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
				},
				dataIndex: 'name',
				text: 'Name',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				dataIndex: 'modifiedBy',
				text: 'User',
				flex: 1
			},
			{
				xtype: 'datecolumn',
				dataIndex: 'modifydate',
				text: 'Date',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden:true,
				dataIndex: 'clsfctn',
				text: 'Classification',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				itemId: 'receivedmodback',
				dataIndex: 'receivedBack',
				text: 'ReceivedBack',
				flex: 1
			}
		];
		me.callParent(arguments);
    }
});