Ext.define('DM2.view.DealHistoryTab', {
    extend: 'Ext.button.Split',
    alias: 'widget.dealhistorytab',

    requires: [
        'Ext.button.Button',
		'Ext.button.Split'
    ],

    iconCls: 'loanbtn',
	text:'Deal History',
	cls:'docbtncls',
	menu:{
		items:[{
			text:'Addressess',
			group: 'hisopts',
			checked : true
		},
		{
			text:'Streets',
			group: 'hisopts',
			checked : false
		},
		{
			text:'Contacts',
			group: 'hisopts',
			checked : false
		}]
	}
});