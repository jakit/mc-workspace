Ext.define('DM2.view.DealUserPickerPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.dealuserpickerpanel',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Number',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);        
    },
    initComponent: function() {
        var me = this;
		me.frame = true;
		me.margin = 3;
		me.ui = 'activitypanel';
		me.closable = true;
		var rolesst = Ext.create('DM2.store.Roles');
		var dealuserst = Ext.create('DM2.store.DealUsers');
		me.header  = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'dealusersavebtn',
					text: 'Save'
				}
			]
		};
		me.title  = 'Add User';
		me.layout  = 'border';
		me.items = [{
				xtype: 'panel',
				region: 'north',
				split: true,
				collapsible: true,
				scrollable: true,
				bodyPadding: 5,
				header:false,
				layout: {
					type: 'column'
				},
				items: [{
					xtype: 'panel',
					columnWidth: 0.5,
					margin: '0 5 0 0',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'First Name',
						name: 'firstName',
						readOnly: true
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Address',
						name: 'address',
						readOnly: true
					},
					{
						xtype: 'textfield',
						fieldLabel: 'State',
						name: 'state',
						readOnly: true
					},
					{
						xtype: 'textfield',
						fieldLabel: 'User Name',
						name: 'user',
						readOnly: true
					},
					{
						xtype: 'combobox',
						anchor: '100%',
						fieldLabel: 'Role',
						name: 'role',
						allowBlank: false,
						displayField: 'description',
						queryMode: 'local',
						store: rolesst, //'Roles',
						valueField: 'idRole'
					},
					{
						xtype: 'numberfield',
						anchor: '100%',
						hidden: true,
						fieldLabel: 'idDealxUser',
						name: 'idDealxUser'
					},
					{
						xtype: 'numberfield',
						anchor: '100%',
						hidden: true,
						fieldLabel: 'idUser',
						name: 'idUser'
					}]
				},{
					xtype: 'panel',
					columnWidth: 0.5,
					margin: '0 5 0 0',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						labelAlign:'right',
						anchor: '100%',
						fieldLabel: 'Last Name',
						name: 'lastName',
						readOnly: true
					},
					{
						xtype: 'textfield',
						labelAlign:'right',
						fieldLabel: 'City',
						name: 'city',
						readOnly: true
					},
					{
						xtype: 'textfield',
						labelAlign:'right',
						fieldLabel: 'Zip',
						name: 'zip',
						readOnly: true
					},
					{
						xtype: 'textfield',
						labelAlign:'right',
						fieldLabel: 'Position',
						name: 'position',
						readOnly: true
					},
					{
						xtype: 'checkboxfield',
						hidden:true,
						labelAlign:'right',
						anchor: '100%',
						fieldLabel: 'Active',
						name: 'active',
						boxLabel: '',
						checked: true
					}]
				}]
			},
			{
				xtype : 'grid',
				region : 'center',
				bodyBorder : true,
				scrollable: true,
				header: false,
				store : dealuserst,
				columns : [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'user',
            text: 'USER',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'role',
            text: 'ROLE',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assigner',
            text: 'Assigner',
            flex: 1
        }, {
            xtype: 'datecolumn',
            dataIndex: 'assigndate',
            text: 'Assigndate',
            flex: 1,
            format: 'm-d-Y'
        },{
            xtype: 'datecolumn',
            dataIndex: 'unassigndate',
            text: 'UnAssigndate',
            flex: 1,
            format: 'm-d-Y'
        }, {
            xtype: 'checkcolumn',
            itemId: 'activecheck',
            dataIndex: 'active',
            text: 'Active'
        }]
			}
		];
		me.callParent(arguments);
	}
});