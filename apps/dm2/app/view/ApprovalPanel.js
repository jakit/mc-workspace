Ext.define('DM2.view.ApprovalPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.approvalpanel',

    requires: [
        'DM2.view.ApprovalLoanGridPanel',
        'DM2.view.ApprovalGridPanel',
        'Ext.grid.Panel',
        'Ext.form.Panel',
        'Ext.form.field.Date',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	frame: true,
    itemId: 'approvalpanel',
    margin: 3,
    scrollable: true,
    ui: 'activitypanel',
    closable: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    dockedItems: [
        {
            xtype: 'approvalloangridpanel',
            maxHeight: 150,
            collapsible: true,
            header: false,
            dock: 'top'
        }
    ],
	header : {
		titlePosition: 0,
		items: [
			{
				xtype: 'button',
				action: 'doneActionBtn',
				margin: '0 10 0 0',
				text: 'Done'
			}
		]
	},
    items: [
        {
            xtype: 'form',
            frame: true,
			tabIndex:-1,
            itemId: 'loandetailform',
            margin: '5 5 2.5 5',
            scrollable: 'true',
            ui: 'activitypanel',
            layout: 'column',
            bodyPadding: 5,
            collapsible: true,
            title: 'Loan Details',
            items: [
                {
                    xtype: 'panel',
                    columnWidth: 0.45,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Bank',
                            labelAlign: 'right',
                            labelWidth: 68,
                            name: 'bank'
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Requested Amount',
                            labelAlign: 'right',
                            labelWidth: 68,
                            name: 'amountRequested'
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'PPP',
                            labelAlign: 'right',
                            labelWidth: 68,
                            name: 'ppp'
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 1.1,
                                    readOnly: true,
                                    fieldLabel: 'Option',
                                    labelAlign: 'right',
                                    labelWidth: 68
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 0.9,
                                    readOnly: true,
                                    fieldLabel: 'Amort',
                                    labelAlign: 'right',
                                    labelWidth: 45,
                                    name: 'amortterm'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.25,
                    margin: '0px 5px 0px 0px',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'IO',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'io'
                        },
						{
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Index Value',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'indexvalue'
                        },              
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Spread',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'spread'
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Term',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'term'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.3,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Rate Type',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'ratetype'
                        },
						{
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Mortgage Type',
                            labelAlign: 'right',
                            labelWidth: 65
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Index',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'index'
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            fieldLabel: 'Loan Type',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'quotetype'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'form',
            frame: true,
			tabIndex:1,
            itemId: 'commitmentfrm',
            margin: '2.5 5 2.5 5',
            ui: 'activitypanel',
            bodyPadding: 5,
            collapsible: true,
            title: 'Commitment',
			layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [                
                {
                    xtype: 'panel',
                    flex: 1,
                    //margin: '0 5 0 0',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [                 
                        {
                            xtype: 'numberfield',
							hideTrigger:true,
                            fieldLabel: 'Commitment Check Amount',
                            labelWidth: 90,							
                            name: 'commitmentCheckAmount'
                        },               
                      	{
                            xtype: 'datefield',                            
                            fieldLabel: 'Commitment Date',
                            labelWidth: 90,
                            name: 'commitmentDate'
                        },
						{
                            xtype: 'textfield',                            
                            fieldLabel: 'Signed by',
                            labelWidth: 90,
                            name: 'commitmentCheck'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
						{
                            xtype: 'textfield',                            
                            fieldLabel: 'Commitment Number',
                            labelWidth: 90,
							labelAlign:'right',
                            name: 'commitmentNumber'
                        },						
                        {
                            xtype: 'datefield',                            
                            fieldLabel: 'Expiration Date',
                            labelWidth: 90,
							labelAlign:'right',
                            name: 'commitExpirationDate'
                        },                                             
                        {
                            xtype: 'datefield',                            
                            fieldLabel: 'Acceptance Date',
                            labelWidth: 90,
							labelAlign:'right',
                            name: 'commitmentCheckDate'
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'footer',
                    defaults: {
                        minWidth: 60
                    },
                    items: [
                        {
                            xtype: 'component',
                            flex: 1
                        },
                        {
                            xtype: 'button',
							tabIndex:6,
                            itemId: 'savecommitmentbtn',
                            text: 'Save'
                        },
                        {
                            xtype: 'button',
							tabIndex:7,
                            itemId: 'resetcommitmentbtn',
                            text: 'Restore'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'approvalgridpanel',
            margin: '2.5 5 5 5'
        }
    ]

});