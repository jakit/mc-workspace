Ext.define('DM2.view.PermissionSelectedGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.permissionselectedgridpanel',

    requires: [
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.height = 250;
		me.itemId = 'permissionselectedgridpanel';
		me.ui = 'activitypanel';
		me.title = 'Selected Permission';
		me.store = Ext.create('DM2.store.SelectedPermission');//'SelectedPermission';
	
		me.columns = [
			{
				xtype: 'gridcolumn',
				dataIndex: 'entityname',
				text: 'Entity Name',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				disabled: true,
				dataIndex: 'isgroup',
				text: 'Group',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				dataIndex: 'list',
				text: 'List',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				dataIndex: 'read',
				text: 'Read',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				dataIndex: 'write',
				text: 'Write',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				dataIndex: 'remove',
				text: 'Delete',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				dataIndex: 'control',
				text: 'Control',
				flex: 1
			}
		];
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						itemId: 'addpermission',
						iconCls: 'add',
						text: 'Add'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});