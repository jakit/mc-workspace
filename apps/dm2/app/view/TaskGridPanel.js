Ext.define('DM2.view.TaskGridPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.taskgridpanel',

    requires: [
    ],

    viewModel: {
        type: 'taskgridpanel'
    },
    height: 250,
    width: 400
});