Ext.define('DM2.view.CreateNewDealFromPropertyDocsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.createnewdealfrompropertydocsgridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.grid.plugin.DragDrop',
        'Ext.util.Point',
        'Ext.selection.CheckboxModel'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
    	me.cls = 'gridcommoncls';
	    me.frame = true;
    	me.height = 170;
	    me.itemId = 'createnewdealfrompropertydocsgridpanel';
	    me.margin = '5 0 0 0';
	    me.maxHeight = 175;
	    me.minHeight = 100;
	    me.scrollable = true;
	    me.ui = 'activitypanel';
    	me.collapsible = true;
	    me.title = 'Documents';
		me.store = Ext.create('DM2.store.PropertyDocuments');
	    me.columns = [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                var docurlname = record.get('url');
                var ext = DM2.app.getController('MainController').getExtImg(docurlname);
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return '<a target="_blank" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
            },
            dataIndex: 'name',
            text: 'Name',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 90,
            dataIndex: 'desc',
            text: 'Description'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 65,
            align: 'center',
            dataIndex: 'clsfctn',
            text: 'Classification'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            width: 70,
            align: 'center',
            dataIndex: 'modifiedBy',
            text: 'By'
        }
    ];
		me.viewConfig = {
			plugins: [
				{
					ptype: 'gridviewdragdrop',
					ddGroup: 'dropsubmissiondoc',
					enableDrag: false
				}
			]
		};
		me.selModel = {
			selType: 'checkboxmodel'
		};
		me.callParent(arguments);
    }

});