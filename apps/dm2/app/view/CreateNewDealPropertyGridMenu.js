Ext.define('DM2.view.CreateNewDealPropertyGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.createnewdealpropertygridmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
			action:'addproperty',
            text: 'Add Property',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        }];
        me.callParent(arguments);
    }
});