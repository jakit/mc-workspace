Ext.define('DM2.view.DealContactsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealcontactsgridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table'
    ],
   
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
		me.store = Ext.create('DM2.store.Contacts');
        me.cls = 'gridcommoncls';
        me.title = 'Contacts';        
        me.columnLines = false;
        me.columns = [{
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'contactid',
            text: 'Contact Id',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'fullName',
            text: 'Name',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'company',
            text: 'Company',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'contactType',
            text: 'Type',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'officePhone_1',
            text: 'Phone',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'email_1',
            text: 'Email',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assoctype',
            text: 'Assoc Type',
            flex: 1
        }, {
            xtype: 'gridcolumn',
            hidden: true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'assocdesc',
            text: 'Assoc Desc',
            flex: 1
        }];        
        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            hidden: true,
            itemId: 'contactgridtoolbar',
            items: [
                {
                    xtype: 'button',
                    action: 'addcontactbtn',
                    itemId: 'addcontactbtn',
                    text: 'Add Contact'
                }
            ]
        }];
        me.callParent(arguments);
    }    
});