Ext.define('DM2.view.QuoteOptionFormPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.quoteoptionformpanel',

    requires: [
        'Ext.form.Panel'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.closable = true;
		me.closeAction = 'hide';
		me.hidden = true,
		me.title = 'Option Details';
		me.itemId = 'quoteoptionformpanel';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					disabled:true,
					action:'savequoteoptionbtn',
					text: 'Save'
				}
			]
		};
		me.items = [{
			xtype: 'panel',
			padding:5,
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype:'panel',
				flex:1,
				layout:{
					type:'vbox',
					align:'stretch'
				},
				items:[{
					xtype: 'textfield',
					hidden:true,
					fieldLabel: 'formmode',
					labelAlign: 'left',
					labelWidth: 70,
					name: 'formmode'
				},{
					xtype: 'numberfield',
					hidden:true,
					fieldLabel: 'idLoanOption',
					labelAlign: 'left',
					labelWidth: 70,
					name: 'idLoanOption'
				},{
					xtype: 'numberfield',
					hidden:true,
					fieldLabel: 'idLoan',
					labelAlign: 'left',
					labelWidth: 70,
					name: 'idLoan'
				},{
					xtype: 'currencyfield',
					flex:1,
					fieldLabel: 'Option Amount',
					labelAlign: 'left',
					labelWidth: 70,
					name: 'optAmount'
				},
				{
					xtype: 'textfield',
					flex: 1.2,                                    
					fieldLabel: 'Rate',
					labelWidth: 70,
					name: 'quoteRate'
				},
				{
					xtype: 'checkboxfield',
					fieldLabel: 'Selected',
					//flex:0.8,
					//align:'right',
					//labelAlign: 'right',
					labelWidth: 70,
					name: 'optionSelected'
				},
				{
					xtype: 'textfield',
					//flex: 0.8,
					fieldLabel: 'Spread',
					//labelAlign: 'right',
					labelWidth: 70,
					name: 'indexSpreadOption',
					triggers: {
						clear: {
							cls: 'x-form-arrow-trigger',
							handler: function() {
								this.fireEvent("ontriggerclick", this, 'spreadindexoption');
							}
						}
					}
				},
				{
					xtype: 'numberfield',
					flex:1,
					fieldLabel: 'Floor',
					labelWidth: 70,
					name: 'optionFloor'
				},
				{
					xtype: 'datefield',
					format: 'm-d-Y',
					flex:1,
					fieldLabel: 'Start Date',
					labelWidth: 70,
					name: 'startDate'
				},
				{
					xtype: 'textfield',
					//flex: 0.8,
					fieldLabel: 'Option Description',
					//labelAlign: 'right',
					labelWidth: 70,
					name: 'optionDesc'
				},
				{
					xtype: 'numberfield',
					flex:1,
					fieldLabel: 'RenewFee',
					labelWidth: 70,
					name: 'renewFee'
				},
				{
					xtype: 'numberfield',
					flex:1,
					fieldLabel: 'RenewFee Percent',
					labelWidth: 70,
					name: 'renewFeePercent'
				}]
			},
			{
				xtype:'panel',
				flex:1,
				layout:{
					type:'vbox',
					align:'stretch'
				},
				items:[{
					xtype: 'numberfield',
					hideTrigger:true,
					flex:1,
					fieldLabel: 'Amortization Month',
					labelWidth: 75,
					labelAlign: 'right',
					name: 'amortizationMonth'
				},{
					xtype: 'combobox',
					selectOnFocus: true,
					displayField: 'selectionDesc',
					queryMode: 'local',
					store: 'RateTypeList',//ratetypelist_st,
					typeAhead: true,
					valueField: 'selectionDesc',                 
					fieldLabel: 'Rate Type',
					labelAlign: 'right',
					labelWidth: 75,
					name: 'rateType'									
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'Term',
					labelAlign: 'right',
					labelWidth: 75,
					name: 'term'
				},
				{
					xtype: 'numberfield',
					flex: 1,                                    
					fieldLabel: 'Spread(%)',
					labelAlign: 'right',
					labelWidth: 75,
					name: 'percentSpread'
				},
				{
					xtype: 'checkboxfield',
					fieldLabel: 'IO',
					//flex:0.8,
					//align:'right',
					labelAlign: 'right',
					labelWidth: 75,
					name: 'interestOnly'
				},
				{
					xtype: 'textfield',
					flex:1,
					fieldLabel: 'Month End',
					labelAlign: 'right',
					labelWidth: 75,
					name: 'monthEnd'
				},
				{
					xtype: 'textfield',
					//flex: 0.8,
					fieldLabel: 'Desc Record',
					labelAlign: 'right',
					labelWidth: 75,
					name: 'descRecord'
				},
				{
					xtype: 'textfield',
					labelWidth: 75,
					labelAlign: 'right',
					flex:1,
					fieldLabel: 'PPP',
					name: 'pppDescription',
					triggers: {
						clear: {
							cls: 'x-form-arrow-trigger',
							handler: function() {
								this.fireEvent("ontriggerclick", this, 'pppoption');
							}
						}
					}
				},
				{
					xtype: 'numberfield',
					hideTrigger:true,
					labelWidth: 75,
					labelAlign: 'right',
					flex:1,
					fieldLabel: 'PPP Window',
					name: 'pppWindow'
				}]
			}]
		}];
		me.callParent(arguments);
    }
});