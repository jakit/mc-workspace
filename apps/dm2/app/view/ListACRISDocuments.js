Ext.define('DM2.view.ListACRISDocuments', {
    extend: 'Ext.form.Panel',
    alias: 'widget.listacrisdocuments',

    requires: [
		'DM2.view.AcrisPropertyDocsGrid',
		'DM2.view.AcrisContactsGrid',
		'DM2.view.AcrisNotesGrid',
        'Ext.form.field.Number',
        'Ext.form.Panel',
        'Ext.button.Button'
    ],
    frame: true,
    itemId: 'listacrisdocuments',
    scrollable: true,
    ui: 'activitypanel',
    margin:3,
    closable:true,
    //collapsible: true,
	title:'List ACRIS Documents',
	/*layout:{
		type:'vbox',
		align:'stretch'
	},*/
	bodyPadding: 5,
    items: [
		{
			xtype:'acrispropertydocsgrid',			
			maxHeight:325//,
			//flex:2
		},
		{
			xtype:'acriscontactsgrid',
			maxHeight:300//,
			//flex:1
		},
		{
			xtype:'acrisnotesgrid',
			maxHeight:300//,
			//flex:1
		}
    ]

});