Ext.define('DM2.view.ContactAddressGridPanel', {
    extend: 'DM2.view.MetaInfoGrid',
    alias: 'widget.contactaddressgridpanel',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	initComponent: function() {
        var me = this;
		var gridModel = me.generateGridModel();
		me.store = Ext.create('DM2.store.ContactAddress', {
			extend: 'Ext.data.Store',
		
			requires: [
				//'DM2.model.ContactPhone',
				'Ext.data.proxy.Rest',
				'Ext.data.reader.Json',
				'Ext.data.writer.Json'
			],							          
            storeId: 'ContactAddress',
            autoSync: true,
            model: gridModel,//'DM2.model.ContactEmail',
            proxy: {
                type: 'rest',
                api: {
                    create: DM2.view.AppConstants.apiurl+'mssql:Address',
                    read: DM2.view.AppConstants.apiurl+'mssql:Address',
                    update: DM2.view.AppConstants.apiurl+'mssql:Address',
                    destroy: DM2.view.AppConstants.apiurl+'mssql:Address'
                },
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Address',
                reader: {
                    type: 'json'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function(data, request) {
                            // do some manipulation of the unserialized data object
                            //console.log(data);
                            //console.log(request);
                            if(request._action==="create"){
                                Ext.Object.each(data, function(key, value, myself) {
                                    //console.log(key + ":" + value);
                    
                                    if (key === 'idAddress') {
                                        //console.log("Delete the key"+key);
                                        delete data[key];
                                    }
                                });
                            }
                            if(request._action==="update"){
                                data['@metadata'] = {'checksum' : 'override'};
                            }
							console.log(request);
							if(request._action==="destroy"){
								console.log("Calling destroy action");
                                //data['checksum'] = 'override';
								request._url = request._url+"?checksum=override";
                            }
                            //console.log(data);
                            return data;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                write: {
                    /*fn: me.onStoreWrite,
                    scope: me*/
					fn: function(store, operation, eOpts) {
						if(operation.request._action==="create"){
							console.log("Create Write completes");
							//console.log(operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(false);
							store.getAt(store.getCount()-1).set('idAddress',operation._records[0].data.txsummary[0].idAddress);
							//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(true);
						}
					},
					scope: this
                }
            },
        

			onStoreWrite: function(store, operation, eOpts) {
				//console.log(store);
				//console.log(operation);
				if(operation.request._action==="create"){
					//console.log("Create Write completes");
					//console.log(operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(false);
					//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(true);
				}
			}        
        });
		me.callParent(arguments);
	},
    frame: true,
    margin: '0 0 0 0',
    ui: 'activitypanel',
    collapsible: true,
    title: 'Address',
	settings: {
		editor: {
			type: "combo",
			data: [
				{ "tag": "address1", "label": "Address 1"},
				{ "tag": "address2", "label": "Address 2"},
				{ "tag": "address3", "label": "Address 3"}
			]
		},
		grid: {
			fields: [{ "name": "address","type": "string", flex: 1 },{type: 'int',name: 'idAddress'},{name:'idContact'},{name: 'city',"type": "string", flex: 1},{name: 'state',"type": "string", flex: 1},{name: 'zip',"type": "string", flex: 1}],
			idPropertyField: 'idAddress',
			columns: [{
				dataIndex: "address",
				header: "Address",
				width: 150,
				flex: 1,
				editor: {
					xtype: 'textfield',
					emptyText: 'Enter a address'
				}
			},
			{
				dataIndex: "city",
				header: "City",
				width: 150,
				flex: 1,
				editor: {
					xtype: 'textfield',
					emptyText: 'Enter a city'
				}
			},
			{
				dataIndex: "state",
				header: "State",
				width: 150,
				flex: 1,
				editor: {
					xtype: 'textfield',
					emptyText: 'Enter a state'
				}
			},{
				dataIndex: "zip",
				header: "Zip",
				width: 150,
				flex: 1,
				editor: {
					xtype: 'textfield',
					emptyText: 'Enter a zip'
				}
			}],
			useIsPrimary: true,
			buttonLabel: "Add",
			viewConfig: {
				stripeRows: true
			},
			tagFieldWidth: 400,
			tagFieldFlex: 1
		}
		
	}
	/*
    store: 'ContactAddress',

    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'tag',
            text: 'Tag',
            flex: 1,
            editor: {
                xtype: 'combobox',
                editable: false,
                displayField: 'tag',
                forceSelection: true,
                queryMode: 'local',
                store: 'AddressTags',
                valueField: 'tag'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'address',
            text: 'Address',
            flex: 1,
            editor: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'city',
            text: 'City',
            flex: 1,
            editor: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'state',
            text: 'State',
            flex: 1,
            editor: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'zip',
            text: 'Zip',
            flex: 1
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'isPrimary',
            text: 'IsPrimary',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'add',
                    iconCls: 'add',
                    text: 'Add'
                },
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'delete',
                    iconCls: 'delete',
                    text: 'Delete'
                }
            ]
        }
    ],
    plugins: [
        {
            ptype: 'cellediting'
        }
    ],
    selModel: {
        selType: 'cellmodel'
    }
	*/
});