Ext.define('DM2.view.ContactPhonesGridPanel', {
    extend: 'DM2.view.MetaInfoGrid',
    alias: 'widget.contactphonesgridpanel',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	initComponent: function() {
        var me = this;
		var gridModel = me.generateGridModel();
		me.store = Ext.create('DM2.store.ContactPhones', {
			extend: 'Ext.data.Store',
		
			requires: [
				//'DM2.model.ContactPhone',
				'Ext.data.proxy.Rest',
				'Ext.data.reader.Json',
				'Ext.data.writer.Json'
			],							  
            //model: gridModel,//me.gridModel,
        
            storeId: 'ContactPhones',
            autoSync: true,
            model: gridModel,//'DM2.model.ContactPhone',
            proxy: {
                type: 'rest',
                api: {
                    create: DM2.view.AppConstants.apiurl+'mssql:Phone',
                    read: DM2.view.AppConstants.apiurl+'mssql:Phone',
                    update: DM2.view.AppConstants.apiurl+'mssql:Phone',
                    destroy: DM2.view.AppConstants.apiurl+'mssql:Phone'
                },
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Phone',
                reader: {
                    type: 'json'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function(data, request) {
                            // do some manipulation of the unserialized data object
                            //console.log(data);
                            //console.log(request);
                            if(request._action==="create"){
                                Ext.Object.each(data, function(key, value, myself) {
                                    //console.log(key + ":" + value);
                    
                                    if (key === 'idPhone') {
                                        //console.log("Delete the key"+key);
                                        delete data[key];
                                    }
                                });
                            }
                            if(request._action==="update"){
                                data['@metadata'] = {'checksum' : 'override'};
                            }
							console.log(request);
							if(request._action==="destroy"){
								console.log("Calling destroy action");
                                //data['checksum'] = 'override';
								request._url = request._url+"?checksum=override";
                            }
                            //console.log(data);
                            return data;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                write: {
                    /*fn: me.onStoreWrite,
                    scope: me*/
					fn: function(store, operation, eOpts) {
						if(operation.request._action==="create"){
							console.log("Create Write completes");
							//console.log(operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(false);
							store.getAt(store.getCount()-1).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
							//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(true);
						}
					},
					scope: this
                }
            },
        

			onStoreWrite: function(store, operation, eOpts) {
				//console.log(store);
				//console.log(operation);
				if(operation.request._action==="create"){
					//console.log("Create Write completes");
					//console.log(operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(false);
					//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(true);
				}
			}        
        });
		me.callParent(arguments);
	},
    frame: true,
    ui: 'activitypanel',
    collapsible: true,
    title: 'Phones',
	settings: {
		editor: {
			type: "combo",
			data: [{
				"tag": "officePhone1",
				"label": "Office Phone 1"
			}, {
				"tag": "officePhone2",
				"label": "Office Phone 2"
			}, {
				"tag": "homePhone1",
				"label": "Home Phone 1"
			}, {
				"tag": "homePhone2",
				"label": "Home Phone 2"
			}, {
				"tag": "cellPhone1",
				"label": "Cell Phone 1"
			}, {
				"tag": "cellPhone2",
				"label": "Cell Phone 2"
			}]
		},
		grid: {
			fields: [{ "name": "phoneNumber","type": "string", flex: 1 },{type: 'int',name: 'idPhone'},{name:'idContact'}],
			idPropertyField: 'idPhone',
			columns: [{
				dataIndex: "phoneNumber",
				header: "Number",
				width: 150,
				flex: 1,
				editor: {
					xtype: 'textfield',
					emptyText: 'Enter a number'
				}
			}],
			useIsPrimary: true,
			buttonLabel: "Add",
			viewConfig: {
				stripeRows: true
			},
			tagFieldWidth: 400,
			tagFieldFlex: 1
		}
		
	}
	/*
    store: 'ContactPhones',

    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'tag',
            text: 'Tag',
            flex: 1,
            editor: {
                xtype: 'combobox',
                allowBlank: false,
                editable: false,
                displayField: 'tag',
                forceSelection: true,
                queryMode: 'local',
                store: 'PhoneTags',
                valueField: 'tag'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'phoneNumber',
            text: 'Number',
            flex: 1,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'isPrimary',
            text: 'IsPrimary',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'add',
                    iconCls: 'add',
                    text: 'Add'
                },
                {
                    xtype: 'button',
                    disabled: true,
                    itemId: 'delete',
                    iconCls: 'delete',
                    text: 'Delete'
                }
            ]
        }
    ],
    plugins: [
        {
            ptype: 'cellediting',
            pluginId: 'contactphoneeditingplugin'
        }
    ],
    selModel: {
        selType: 'cellmodel'
    }
	*/
});