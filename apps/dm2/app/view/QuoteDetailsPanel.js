Ext.define('DM2.view.QuoteDetailsPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.quotedetailspanel',

    requires: [
        'Ext.grid.Panel',
        'Ext.form.field.Number',
        'Ext.button.Button',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.grid.plugin.CellEditing',
        'Ext.form.FieldSet',
        'Ext.form.CheckboxGroup',
        'Ext.form.field.TextArea',
        'Ext.selection.CheckboxModel',
		'DM2.view.QuoteOptionsPanel',
		'DM2.view.StructuredGridPanel',
		'DM2.view.Underwriting'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	bodyPadding : 5,
	scrollable : true,
    initComponent: function() {
		var me = this;
		//me.frame = true;
		//me.margin = 3;
		//me.ui = 'activitypanel';
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		//me.closable = true;
		//me.scrollable = true;
		me.itemId = 'quotedetailspanel';
		me.title = 'Details';
		me.cls = ['removenumcls'];
		var dealproperty_st =  Ext.create('DM2.store.Dealproperty');
		//var loantypelist_st =  Ext.create('DM2.store.LoanTypeList');
		//var ratetypelist_st =  Ext.create('DM2.store.RateTypeList');
		//var mortgagetypelist_st =  Ext.create('DM2.store.MortgageTypeList');
		var quoteoptions_st =  Ext.create('DM2.store.QuoteOptions');
	 	me.submissionst = Ext.create('DM2.store.BanksSelected');
		//me.underwrittingActionsStore = Ext.create('DM2.store.UnderwrittingActions');
		//me.underwritingStore = Ext.create('DM2.store.Underwriting');
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					disabled:true,
					action: 'dealdetailquotesavebtn',
					margin: '0 10 0 0',
					itemId: 'dealdetailquotesavebtn',
					text: 'Save'
				},
				{
					xtype: 'button',
					disabled:true,
					action: 'dealdetailquoteresetbtn',
					itemId: 'dealdetailquoteresetbtn',
					text: 'Reset'
				},
				{
					xtype: 'button',
					margin: '0 0 0 10',
					action: 'dealdetailquoteclearbtn',
					itemId: 'dealdetailquoteclearbtn',
					text: 'Clear'
				}
			]
		};
		me.items = [
			{
				xtype: 'textfield',
				hidden: true,
				fieldLabel: 'idLoan',
				name: 'idLoan'
			},
			{
				xtype: 'numberfield',
				hidden: true,
				fieldLabel: 'BankId',
				name: 'Bank_idBank'
			},
			{
				xtype: 'numberfield',
				hidden: true,
				fieldLabel: 'OperatingAs',
				name: 'operatingAs'								
			},
			{
				xtype: 'panel',
				layout:{
					type:'vbox',
					align:'stretch'
				},
				padding:'10 10 5 10',
				margin: '0 0 5 0',
				frame : true,
				ui: 'activitypanel',
				collapsible: true,
				title: 'Loan Details',
				items:[{
					xtype: 'textfield',
					flex: 1,
					fieldLabel: 'Bank Full Name',
					//labelWidth: 35,
					name: 'fullName',
					readOnly: true
				},
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
						{
							xtype: 'currencyfield',
							flex: 1,										
							fieldLabel: 'Requested Amount',
							//labelWidth: 65,
							//labelAlign:'right',
							name: 'amountRequested'
						},
						{
							xtype: 'currencyfield',
							flex: 1,										
							fieldLabel: 'Amount',
							//labelWidth: 65,
							labelAlign:'right',
							name: 'amountQuoted'
						}
					]
				},
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [{
						xtype: 'combobox',
						flex: 1.2,
						selectOnFocus: true,
						displayField: 'selectionDesc',
						queryMode: 'local',
						store: 'RateTypeList',//ratetypelist_st,
						typeAhead: true,
						valueField: 'selectionDesc',                 
						fieldLabel: 'Rate Type',
						//labelAlign: 'right',
						labelWidth: 35,
						name: 'rateType'
					},											
					{
						xtype: 'textfield',
						flex:0.8,
						//height:24,
						//maxHeight : 24,													
						fieldLabel: 'Rate',
						labelAlign: 'right',
						labelWidth: 39,
						name: 'rate'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Spread %',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'percentSpread'
					},
					{
						xtype: 'textfield',
						flex: 1,                                    
						fieldLabel: 'Spread Index',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'indexSpread',
						triggers: {
							clear: {
								cls: 'x-form-arrow-trigger',
								handler: function() {
									this.fireEvent("ontriggerclick", this, 'spreadindex');
								}
							}
						}
					}]
				},
				{
					xtype: 'combobox',
					hidden:true,
					flex: 1,
					selectOnFocus: true,
					displayField: 'selectionDesc',
					queryMode: 'local',
					store: 'MortgageTypeList',//mortgagetypelist_st,
					typeAhead: true,
					valueField: 'selectionDesc',                 
					fieldLabel: 'Mortgage Type',
					labelAlign: 'right',
					labelWidth: 75,
					name: 'mtgtype'	
				},
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align:'center'/*,
						pack:'center'*/
						//align: 'stretch'
					},
					items: [{
						xtype: 'combobox',
						flex: 1.1,
						selectOnFocus: true,
						displayField: 'selectionDesc',
						queryMode: 'local',
						store: 'LoanTypeList',//loantypelist_st,
						typeAhead: true,
						valueField: 'selectionDesc',               
						fieldLabel: 'Loan Type',
						labelAlign: 'left',
						labelWidth: 35,
						name: 'quoteType'
					},
					{
						xtype: 'numberfield',
						hideTrigger:true,
						//keyNavEnabled:false,
						//mouseWheelEnabled:false,
						flex: 0.9,  
						fieldLabel: 'Term Months',
						labelAlign: 'right',
						labelWidth: 44,
						name: 'term',
						listeners: {
							change: function(field, value) {												
								if(value!=0 && value!=null && value!=""){													
									field.up('quotedetailspanel').getForm().findField('termyears').setRawValue((value/12).toFixed(2));
								}
							}
						}
					},
					{
						xtype: 'numberfield',
						hideTrigger:true,
						decimalPrecision : 2,
						readOnly:true,
						fieldLabel: 'Years',
						//flex:1,
						width:79,
						labelAlign: 'right',
						labelWidth: 48,
						name: 'termyears'
					},
					{
						xtype: 'textfield',
						flex:1,
						fieldLabel: 'Percent',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'percent'
					}]
				},
				{
					layout:{
						type:'hbox',
						align:'center'
					},
					margin:'0 0 5 0',
					items:[{
						xtype: 'textfield',
						labelWidth: 35,
						flex:1,
						fieldLabel: 'PPP',
						name: 'pppValue',
						triggers: {
							clear: {
								cls: 'x-form-arrow-trigger',
								handler: function() {
									this.fireEvent("ontriggerclick", this, 'ppp');
								}
							}
						}
					},{
						xtype:'button',
						hidden:true,
						margin:'0 5 0 5',
						text:'Add',
						action:'showPPPListBtn',
						iconCls:'add'
					},{
						xtype: 'numberfield',
						labelAlign: 'right',
						labelWidth: 80,
						hideTrigger:true,
						flex:1,
						fieldLabel: 'PPP Window',
						name: 'pppOptionWindow'
					},{
						xtype: 'textfield',
						hidden:true,
						labelWidth: 45,
						labelAlign: 'right',
						flex:1,
						fieldLabel: 'Note',
						name: 'pppnote'
					}]
				},
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [/*{
						xtype: 'checkboxfield',
						fieldLabel: 'IO',
						flex:0.25,
						//labelAlign: 'right',
						labelWidth: 22,
						name: 'interestOnly'
					},*/
					{
						xtype: 'numberfield',
						hideTrigger:true,
						fieldLabel: 'Amortization Months',
						flex:0.6,
						//labelAlign: 'right',
						labelWidth: 75,
						name: 'amortization',
						// Add change handler to force user-entered numbers to evens
						listeners: {
							change: function(field, value) {
								if(value!=0 && value!=null && value!=""){													
									field.up('quotedetailspanel').getForm().findField('amortizationyears').setRawValue((value/12).toFixed(2));
								}
							}
						}
					},
					{
						xtype: 'numberfield',
						hideTrigger:true,
						decimalPrecision : 2,
						readOnly:true,
						fieldLabel: 'Years',
						//flex:0.4,
						width:79,
						labelAlign: 'right',
						labelWidth: 45,
						name: 'amortizationyears'
					},
					{
						xtype: 'textfield',
						fieldLabel: 'IO Period Months',
						flex:0.6,
						labelAlign: 'right',
						labelWidth: 75,
						name: 'ioperiod'
					},
					{
						xtype: 'numberfield',
						hideTrigger:true,
						readOnly:true,
						fieldLabel: 'Years',
						//flex:0.4,
						width:79,
						labelAlign: 'right',
						labelWidth: 45,
						name: 'ioperiodyears'
					}]
				},
				/*{
						xtype: 'checkboxfield',
						fieldLabel: 'IO',
						flex:0.25,
						//labelAlign: 'right',
						labelWidth: 22,
						name: 'interestOnly'
					},*/
				/*{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
					{
						xtype: 'textfield',
						fieldLabel: 'IO Period Months',
						flex:1.2,
						//labelAlign: 'right',
						labelWidth: 125,
						name: 'ioperiod'
					},
					{
						xtype: 'numberfield',
						hideTrigger:true,
						readOnly:true,
						fieldLabel: 'Years',
						flex:0.8,
						labelAlign: 'right',
						labelWidth: 45,
						name: 'ioperiodyears'
					}]
				},*/
				{
					xtype: 'checkboxfield',
					hidden:true,
					fieldLabel: 'Options',
					//labelAlign: 'right',
					labelWidth: 50,
					name: 'hasOption',
					readOnly:true
				},
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [{
						xtype: 'numberfield',
						flex: 1,										
						fieldLabel: 'Initiation Fee',
						labelWidth: 80,
						name: 'initiationFee'
					},
					{
						xtype: 'textfield',
						flex: 1,										
						fieldLabel: 'Execution Type',
						labelAlign: 'right',
						name: 'executionType'
					}]
				},
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
						{
							xtype: 'datefield',
							format: 'm-d-Y',
							flex: 1,										
							fieldLabel: 'Requested Date',
							labelWidth: 80,
							name: 'dateRequested'
						},
						{
							xtype: 'datefield',
							format: 'm-d-Y',
							flex: 1,										
							fieldLabel: 'Received Date',
							//labelWidth: 65,
							labelAlign:'right',
							name: 'dateReceived'
						}
					]
				},
				{
					xtype: 'structuredgridpanel'
				}]
			},
			{
				xtype: 'quoteoptionspanel'
			},
			{
				xtype: 'underwriting',
				margin : '0 0 5 0',
				collapsible : true,
				closable : false
			},
			{
				xtype: 'panel',
				itemId:'commitment',
				padding:'10 10 5 10',
				margin: '0 0 5 0',
				frame : true,
				ui: 'activitypanel',
				title:'Commitment',
				collapsible: true,
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'panel',
						flex: 1,
						//margin: '0 5 0 0',
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [                 
							{
								xtype: 'numberfield',
								hideTrigger:true,
								fieldLabel: 'Commitment Check Amount',
								labelWidth: 90,							
								name: 'commitmentCheckAmount'
							},               
							{
								xtype: 'datefield',                            
								fieldLabel: 'Commitment Date',
								labelWidth: 90,
								name: 'commitmentDate'
							},
							{
								xtype: 'textfield',                            
								fieldLabel: 'Signed by',
								labelWidth: 90,
								name: 'commitmentCheck'
							}
						]
					},
					{
						xtype: 'panel',
						flex: 1,
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',                            
								fieldLabel: 'Commitment Number',
								labelWidth: 90,
								labelAlign:'right',
								name: 'commitmentNumber'
							},						
							{
								xtype: 'datefield',                            
								fieldLabel: 'Expiration Date',
								labelWidth: 90,
								labelAlign:'right',
								name: 'commitExpirationDate'
							},                                             
							{
								xtype: 'datefield',                            
								fieldLabel: 'Acceptance Date',
								labelWidth: 90,
								labelAlign:'right',
								name: 'commitmentCheckDate'
							}
						]
					}
				]
			},
			/*{
				xtype: 'panel',
				hidden:true,
				layout:{
					type:'vbox',
					align:'stretch'
				},
				padding:'10 10 5 10',
				margin: '0 0 5 0',
				frame : true,
				ui: 'activitypanel',
				collapsible: true,
				title: 'Contacts',
				items:[{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
						{
							xtype: 'textfield',
							flex: 1.2,										
							fieldLabel: 'Client\'s Attorney',
							name: 'clientsattorney',
							readOnly: true
						},
						{
							xtype: 'textfield',
							flex: 0.8,										
							fieldLabel: 'Phone',
							labelWidth: 50,
							labelAlign: 'right',
							name: 'clientsattorneyphone',
							readOnly: true
						}
					]
				},
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
						{
							xtype: 'textfield',
							flex: 1.2,										
							fieldLabel: 'Bank\'s Attorney',
							name: 'bankattorney',
							readOnly: true
						},
						{
							xtype: 'textfield',
							flex: 0.8,										
							fieldLabel: 'Phone',
							labelWidth: 50,
							labelAlign: 'right',
							name: 'bankattorneyphone',
							readOnly: true
						}
					]
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					fieldLabel: 'Received Note',
					name: 'receivednote'
				}]
			},*/
			{
				xtype: 'panel',
				layout:{
					type:'vbox',
					align:'stretch'
				},
				padding:'10 10 5 10',
				margin: '0 0 5 0',
				frame : true,
				ui: 'activitypanel',
				collapsible: true,
				title: 'Information',
				itemId:'information',
				items:[{
					xtype: 'textfield',
					anchor: '100%',
					fieldLabel: 'Quoted By',
					name: 'quoteIssuerName',
					triggers: {
						clear: {
							cls: 'x-form-arrow-trigger',
							handler: function() {
								this.fireEvent("ontriggerclick", this, 'quotedby');
							}
						}
					}
				},{
					xtype: 'numberfield',
					hidden:true,
					anchor: '100%',
					fieldLabel: 'quoteIssuer',
					name: 'quoteIssuer'
				},{
					xtype: 'textfield',
					anchor: '100%',
					fieldLabel: 'Address To',
					name: 'letterAddresseeName',
					triggers: {
						clear: {
							cls: 'x-form-arrow-trigger',
							handler: function() {
								this.fireEvent("ontriggerclick", this, 'letterAddressed');
							}
						}
					}
				},{
					xtype: 'numberfield',
					hidden:true,
					anchor: '100%',
					fieldLabel: 'letterAddressee',
					name: 'letterAddressee'
				},{
					xtype: 'textfield',
					anchor: '100%',
					fieldLabel: 'Signed by CSR',
					name: 'letterSignedByName',
					triggers: {
						clear: {
							cls: 'x-form-arrow-trigger',
							handler: function() {
								this.fireEvent("ontriggerclick", this, 'letterSignedBy');
							}
						}
					}
				},{
					xtype: 'textfield',
					hidden:true,
					anchor: '100%',
					fieldLabel: 'letterSigner',
					name: 'letterSigner'
				},{
					xtype: 'textfield',
					anchor: '100%',
					fieldLabel: 'Special Note on Letter',
					name: 'specialNote'
				},{
					xtype: 'textfield',
					anchor: '100%',
					fieldLabel: 'Received Note',
					name: 'receivedNote'
				},
				{
					xtype: 'combobox',									
					fieldLabel: 'Secured Loan Y/N',
					//labelWidth: 50,
					//labelAlign: 'right',
					name: 'securedLoan',
					store : ["Y","N"]/*,
					listeners:{
						afterrender: function ( cmb , eOpts ) {
							cmb.setRawValue(cmb.getStore().getAt(0));
						}
					}*/
				}/*,
				{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
						{
							xtype: 'textfield',
							flex: 1,										
							fieldLabel: 'Effective Rate',
							name: 'effectiverate',
							readOnly: true
						},
						{
							xtype: 'textfield',
							flex: 1,										
							fieldLabel: 'Secured Loan Y/N',
							labelWidth: 50,
							labelAlign: 'right',
							name: 'securedloan',
							readOnly: true
						}
					]
				}*/]
			},
			{
				xtype: 'panel',
				hidden:true,
				layout:{
					type:'vbox',
					align:'stretch'
				},
				padding:10,
				margin: '0 0 5 0',
				frame : true,
				ui: 'activitypanel',
				collapsible: true,
				itemId:'deliverydetails',
				title: 'Delivery Details',
				items: [
					{
						xtype: 'checkboxgroup',
						fieldLabel: 'Options',
						hideLabel: true,
						labelWidth: 50,
						columns: 2,
						vertical: true,
						items: [
							{
								xtype: 'checkboxfield',
								boxLabel: ' Include Loan #'
							},
							{
								xtype: 'checkboxfield',
								boxLabel: 'Include Client Name'
							},
							{
								xtype: 'checkboxfield',
								boxLabel: 'Sent Via 1st Class Mail'
							},
							{
								xtype: 'checkboxfield',
								boxLabel: 'Sent via Express Mail'
							},
							{
								xtype: 'checkboxfield',
								boxLabel: 'Sent via Fax'
							},
							{
								xtype: 'checkboxfield',
								boxLabel: 'Sent via E-Mail'
							}
						]
					},
					{
						xtype: 'panel',
						margin: '0 0 5 0',
						layout: {
							type: 'hbox',
							align: 'center'
						},
						items: [
							{
								xtype: 'currencyfield',
								flex:1,
								fieldLabel: 'Sent Amount',
								labelWidth: 75,
								name: 'sentamount'
							},
							{
								xtype: 'datefield',
								flex:1,
								fieldLabel: 'Sent Date',
								labelAlign: 'right',
								labelWidth: 75,
								name: 'sentdate'
							}
						]
					},
					{
						xtype: 'panel',
						margin: '0 0 5 0',
						layout: {
							type: 'hbox',
							align: 'center'
						},
						items: [
							{
								xtype: 'currencyfield',
								flex:1,
								fieldLabel: 'Recv. Amount',
								labelWidth: 75,
								name: 'receivedamount'
							},
							{
								xtype: 'datefield',
								flex:1,
								fieldLabel: 'Recv. Date',
								labelAlign: 'right',
								labelWidth: 75,
								name: 'receiveddate'
							}
						]
					},
					{
						xtype: 'panel',
						margin: '0 0 5 0',
						layout: {
							type: 'hbox',
							align: 'center'
						},
						items: [
							{
								xtype: 'textfield',
								flex: 1,                                    
								fieldLabel: 'Recv By',
								labelWidth: 75,
								name: 'initialfee'
							},
							{
								xtype: 'textfield',
								flex: 1,                                    
								fieldLabel: 'Via',
								labelAlign: 'right',
								labelWidth: 75,
								name: 'exectype'
							}
						]
					},
					{
						xtype: 'textfield',                            
						fieldLabel: 'Quoted By',
						labelWidth: 75
					}
				]
			},
			/*{
				xtype: 'panel',
				layout:{
					type:'vbox',
					align:'stretch'
				},
				padding:10,
				margin: '0 0 5 0',
				frame : true,
				ui: 'activitypanel',
				itemId:'bankinfo',
				title:'Bank Info',
				collapsible: true,
				items: [
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Bank Info'
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Address'
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Letter to'
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Signed by'
					},
					{
						xtype: 'textareafield',
						anchor: '100%',
						fieldLabel: 'Special Note'
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Received Note'
					}
				]
			},*/
			{
				xtype: 'gridpanel',
				cls: 'gridcommoncls',
				frame: true,
				itemId: 'quotepropertygridpanel',
				ui: 'activitypanel',
				collapsible: true,
				title: 'Properties',
				columnLines: true,
				multiColumnSort: true,
				sortableColumns: false,
				store: dealproperty_st,
				columns: [
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						width: 70,
						dataIndex: 'street_no',
						text: 'No.'
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'street',
						text: 'Street',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						dataIndex: 'city',
						text: 'City',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						width: 60,
						dataIndex: 'state',
						text: 'St.'
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						hidden: true,
						dataIndex: 'buildingClass',
						text: 'Bldg Cls'
					},
					{
						xtype: 'gridcolumn',
						renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
							metaData.tdAttr = 'data-qtip="' + value + '"';
							return value;
						},
						hidden: true,
						dataIndex: 'propertyType',
						text: 'Property Type'
					}
				],
				selModel: {
					selType: 'checkboxmodel'
				}
			},
			{
				xtype: 'textfield',
				hidden: true,
				fieldLabel: 'Dealid',
				name: 'dealid'
			},
			{
				xtype: 'numberfield',
				hidden: true,
				fieldLabel: 'Activity Id',
				name: 'activityid'
			},
			{
				//xtype: 'textfield',
				xtype: 'checkboxfield',
				hidden: true,
				fieldLabel: 'UsedInDeal',
				name: 'usedInDeal'
			}
		];
		me.callParent(arguments);
    }
});