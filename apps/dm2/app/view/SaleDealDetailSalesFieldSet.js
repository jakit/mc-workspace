Ext.define('DM2.view.SaleDealDetailSalesFieldSet', {
    extend: 'DM2.view.component.container.GridToForm',
    alias: 'widget.saledealdetailsalesfieldSet',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
		'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.panel.Tool',
        'Ext.layout.container.Column',
        'DM2.view.SaleDealDetailSalesMenu',
        'Ext.data.proxy.Rest'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        //me.frame = true;
        me.itemId = 'saledealdetailsalesfieldSet';
        //me.ui = 'propertydealpanel';      
        
        me.contextMenu = Ext.create('DM2.view.SaleDealDetailSalesMenu');
        
        // *************        
        var store = Ext.create('DM2.store.Sales');        
        // ***************        
        
        me.listLayout= {
            icon: 'resources/images/icons/deal_m.png',
            title: "Sales",     
      //      detailOnSingleRecord: true,       
            cls: 'gridcommoncls salesquadrantgrid',
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: store,
			ui : 'newloandealpanel',
			frame:true,
		 	plugins: [{
				itemId: 'rowexpander-sales',
				pluginId: 'rowexpander-sales',
				ptype: 'rowexpander',
				expandOnDblClick: false,
				rowBodyTpl: [
					'<div id="bidgridrow-{idSale}"></div>'
				]
			}],
			viewConfig: {
				// This renderer to apply conditional css on each grid row based specific column value (dtcc status)
				getRowClass: function (record, rowIndex, rowParams, store) {
					var saleType = record.get('saleType');
					if(saleType!='S' && saleType!='O'){  // if name column is blank
						return 'hide-row-expander';
					}
				}
			},//end of viewconfig
			
            columns : [{
				xtype: 'gridcolumn',
				hidden: true,
				dataIndex: 'idSale',
				text: 'Sale id',
				flex: 1
			}, {
				xtype: 'datecolumn',
				flex: 1,
				format:'m/d/Y',
				//width: 150,
				dataIndex: 'modifyDateTime',
				text: 'Date/Time'
			}, {
				xtype: 'gridcolumn',
				align : 'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'saleTypeDisplay',
				text: 'Sale Type',
				flex: 1
			}, {
				xtype: 'gridcolumn',
				align : 'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'commissionTypeDisplay',
				text: 'Commission Type',
				flex: 1
			},{
				xtype: 'gridcolumn',
				cls: 'dealgridcolumncls',
				align : 'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + retVal + '"';
					return value;
				},
				dataIndex: 'commission',
				text: 'Commission',
				flex: 1
			},{
				xtype: 'numbercolumn',
				cls: 'dealgridcolumncls',
				format: '0,000',
				align : 'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(value==null || value==0){
						return null;  
					} else {
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
					metaData.tdAttr = 'data-qtip="' + retVal + '"';
					return retVal;
				},
				dataIndex: 'calcCommission',
				text: 'Commission Amt',
				flex: 1
			},{
				xtype: 'numbercolumn',
				cls: 'dealgridcolumncls',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				//width: 120,
				flex:1,
				dataIndex: 'price',
				text: 'Price',
				format: '0,000',
				align:'right',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					//var formatVal = Ext.util.Format.number(value,'0,000');
					var readable = record.get('readable');
					if(readable==0){
						return null;
					} else {
						if(value==null || value==0){
							return null;  
						} else {
							var formatVal = Ext.util.Format.currency(value, '$',0);
							metaData.tdAttr = 'data-qtip="' + formatVal + '"';
							return formatVal;  
						}
					}
				}
			}]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/deal_m.png',
            title: "Sales",     
            scrollable: true,
			ui : 'newloandealpanel',
			frame:true,
			items: [{
                xtype: 'panel',
                layout: {
                    type: 'column'
                },
                items: [{
                    xtype: 'panel',
                    columnWidth: 0.50,
                    margin: "0px 10px 0 5px",
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
						xtype: 'textfield',
						fieldLabel: 'Sale Type',
						labelAlign: 'right',
						labelWidth: 70,
						name: 'saleType',
						itemId: 'saleType',
						readOnly: true
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Commission (%)',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'percentCommission',
                        itemId: 'percentCommission',
                        readOnly: true
                    },{
						xtype: 'datefield',
						readOnly: true,            
						fieldLabel: 'Date',
						labelAlign: 'right',
						labelWidth: 70,
						name: 'modifyDateTime'
					}]
                }, {
                    xtype: 'panel',
                    columnWidth: 0.50,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [{
                        xtype: 'currencyfield',
                        fieldLabel: 'Price',
                        labelAlign: 'right',
                        labelWidth: 60,
                        name: 'price',
                        itemId: 'price',
                        readOnly: true
                    },
					{
                        xtype: 'currencyfield',
                        fieldLabel: 'Commission Amt',
                        labelAlign: 'right',
                        labelWidth: 60,
                        name: 'flatAmountCommission',
                        itemId: 'flatAmountCommission',
                        readOnly: true
                    }]
                }]
            }]
        };
        me.callParent(arguments);
    }
});