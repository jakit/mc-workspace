Ext.define('DM2.view.UploadContactView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.uploadcontactview',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.form.field.Number'
    ],
	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	
	scrollable: true,
	
    initComponent : function() {
        var me = this;
	    me.border = false;
    	me.frame = true;
	    me.margin = 3;
    	me.ui = 'activitypanel';
	    me.bodyPadding = 10;
    	me.closable = true;
		me.title = 'Upload Contacts';
		me.header = {
			titlePosition: 0,
			items: [{
				xtype: 'button',
				//disabled: true,
				action: 'savebtn',
				margin: '0 10 0 0',
				minWidth: 65,
				text: 'Upload'
			},
			{
				xtype: 'button',
				//disabled: true,
				action: 'resetbtn',
				minWidth: 65,
				text: 'Reset'
			}]
		};
		var grpSt =  Ext.create('Ext.data.Store', {
			  fields: ['itemId','itemValue']
		   });

		var membersSt = Ext.create('Ext.data.Store', {
			  fields: ['itemId','itemValue']
		   });
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
    	me.items = [
			{
				xtype: 'filefield',
				labelWidth:120,
				anchor: '100%',
				fieldLabel: 'Upload Contacts',
				name: 'contactfile',
				allowBlank: false
			},
			{
				layout:{
					type:'hbox',
					align:'stretch'
				},
				margin:'0 0 5 0',
				items:[{
					xtype: 'tagfield',
					filterPickList: true,
					name:'memebercmb',
					labelWidth:120,
					anchor: '100%',
					fieldLabel: 'User Permission List',
					store: membersSt,
					queryMode: 'local',
					displayField: 'itemValue',
					valueField: 'itemValue',
					//readOnly:true,
					flex:1
				},
				{
					xtype: 'button',
					iconCls:'dots-three-horizontal',
					margin:'0 0 0 5',
					action:'memberspickbtn'
				}]                 
			},
			{
				layout:{
					type:'hbox',
					align:'stretch'
				},
				margin:'0 0 5 0',
				items:[{
					xtype: 'tagfield',
					filterPickList: true,
					name:'grpcmb',
					labelWidth:120,
					anchor: '100%',
					fieldLabel: 'Group Permission List',
					store: grpSt,
					queryMode: 'local',
					displayField: 'itemValue',
					valueField: 'itemValue',
					//readOnly:true,
					flex:1
				},
				{
					xtype: 'button',
					iconCls:'dots-three-horizontal',
					margin:'0 0 0 5',
					action:'groupspickbtn'
				}]                 
			}
		];
		me.callParent(arguments);
    }
});