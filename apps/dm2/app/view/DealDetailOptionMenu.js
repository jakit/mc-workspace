Ext.define('DM2.view.DealDetailOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealdetailoptionmenu',

    requires: [
        'Ext.menu.Item'
    ],

    width: 160,

    items: [
        /*{
            xtype: 'menuitem',
            itemId: 'dealpermission',
            text: 'Deal Permission',
            focusable: true,
			modifyMenu : 'no'
        },*/
        {
            xtype: 'menuitem',
            itemId: 'modifydeal',
            text: 'Edit Deal',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
            itemId: 'refreshdeal',
            text: 'Refresh Deal',
            focusable: true,
			modifyMenu : 'no'
        }
    ]
});