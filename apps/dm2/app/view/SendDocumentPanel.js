Ext.define('DM2.view.SendDocumentPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.senddocumentpanel',

    requires: [
        'DM2.view.DealDocsSendLOIGridPanel',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Number',
        'Ext.button.Button',
        'Ext.grid.Panel'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.itemId = 'senddocumentpanel';
		me.margin= 3;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;

		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'doneActionBtn',
					margin: '0 10 0 0',
					text: 'Done'
				}
			]
		};
		me.cls = ['removenumcls'];
		var dealQuotesSelSt = Ext.create('DM2.store.DealQuotesSelected');//'DealQuotesSelected';
		var dealContactsSendLOISt = Ext.create('DM2.store.DealContactsSendLOI');//'DealContactsSendLOI';
		var contactsEmailsSt = Ext.create('DM2.store.ContactEmails');//'ContactEmails';
		var contactsFaxSt = Ext.create('DM2.store.ContactFax');//'ContactFax';		
		me.items = [
			{
				xtype: 'form',
				hidden: true,
				itemId: 'sendloiloanfrm',
				scrollable: 'true',
				header: false,
				items: [
					{
						xtype: 'fieldset',
						collapsible: true,
						title: 'Loans',
						items: [
							{
								xtype: 'combobox',
								anchor: '100%',
								name:'loanid',
								itemId: 'loancombo',                            
								fieldLabel: 'Loans',
								labelWidth: 65,
								displayField: 'bank',
								queryMode: 'local',
								store: dealQuotesSelSt,//'DealQuotesSelected',
								typeAhead: true,
								valueField: 'loanid'
							},
							{
								xtype: 'panel',
								layout: 'column',
								items: [
									{
										xtype: 'panel',
										columnWidth: 0.5,
										margin: '0 5 0 0',
										layout: {
											type: 'vbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',												
												fieldLabel: 'Bank',
												labelWidth: 65,
												name: 'bank',
												readOnly: true
											},
											{
												xtype: 'numberfield',												
												fieldLabel: 'Requested Amount',
												labelWidth: 65,
												name: 'amountRequested',
												readOnly: true
											},
											{
												xtype: 'textfield',												
												fieldLabel: 'Rate Type',
												labelWidth: 65,
												name: 'ratetype',
												readOnly: true
											},
											{
												xtype: 'textfield',												
												fieldLabel: 'Index',
												labelWidth: 65,
												name: 'index',
												readOnly: true
											},
											{
												xtype: 'textfield',												
												fieldLabel: 'IO',
												labelWidth: 65,
												name: 'io',
												readOnly: true
											}
										]
									},
									{
										xtype: 'panel',
										columnWidth: 0.5,
										layout: {
											type: 'vbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',												
												fieldLabel: 'Loan Type',
												labelAlign: 'right',
												labelWidth: 65,
												name: 'quotetype',
												readOnly: true
											},
											{
												xtype: 'textfield',												
												fieldLabel: 'Index Value',
												labelAlign: 'right',
												labelWidth: 65,
												name: 'indexvalue',
												readOnly: true
											},
											{
												xtype: 'textfield',												
												fieldLabel: 'Term',
												labelAlign: 'right',
												labelWidth: 65,
												name: 'term',
												readOnly: true
											},
											{
												xtype: 'textfield',												
												fieldLabel: 'Spread',
												labelAlign: 'right',
												labelWidth: 65,
												name: 'spread',
												readOnly: true
											},											
											{
												xtype: 'textfield',												
												fieldLabel: 'ppp',
												labelAlign: 'right',
												labelWidth: 65,
												name: 'ppp',
												readOnly: true
											}
										]
									}
								]
							}
						]
					}
				]
			},
			{
				xtype: 'form',
				itemId: 'sendloicontactfrm',
				scrollable: 'true',
				header: false,
				items: [
					{
						xtype: 'fieldset',
						collapsible: true,
						title: 'Contacts',
						items: [
							{
								xtype: 'combobox',
								anchor: '100%',
								name:'contactid',
								itemId: 'contactcombo',                            
								fieldLabel: 'Contacts',
								labelWidth: 60,
								displayField: 'name',
								queryMode: 'local',
								store: dealContactsSendLOISt,//'DealContactsSendLOI',
								typeAhead: true,
								valueField: 'contactid'
							},
							{
								xtype: 'panel',
								layout: 'column',
								items: [
									{
										xtype: 'panel',
										columnWidth: 0.5,
										margin: '0 5 0 0',
										layout: {
											type: 'vbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Name',
												labelWidth: 60,
												name: 'name',
												readOnly: true
											},
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Company',
												labelWidth: 60,
												name: 'company',
												readOnly: true
											},
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Phone',
												labelWidth: 60,
												name: 'phone',
												readOnly: true
											}
										]
									},
									{
										xtype: 'panel',
										columnWidth: 0.5,
										layout: {
											type: 'vbox',
											align: 'stretch'
										},
										items: [
											{
												xtype: 'textfield',                                            
												fieldLabel: 'Role',
												labelAlign: 'right',
												labelWidth: 50,
												name: 'type',
												readOnly: true
											},
											{
												xtype: 'combobox',                                            
												fieldLabel: 'Email',
												labelAlign: 'right',
												labelWidth: 50,
												name: 'email',
												displayField: 'email',
												queryMode: 'local',
												store: contactsEmailsSt,//'ContactEmails',
												valueField: 'email'
											},
											{
												xtype: 'combobox',                                            
												fieldLabel: 'Fax',
												labelAlign: 'right',
												labelWidth: 50,
												name: 'faxNumber',
												displayField: 'faxNumber',
												queryMode: 'local',
												store: contactsFaxSt,//'ContactFax',
												valueField: 'faxNumber'
											}
										]
									}
								]
							}
						]
					}
				]
			},
			{
				xtype: 'dealdocssendloigridpanel'
			}
		];
		me.dockedItems = [
			{
				xtype: 'container',
				dock: 'bottom',
				margin: '5 0 5 0',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'button',
						flex: 1,
						disabled: true,
						itemId: 'emailbtn',
						margin: '0 5 0 5',
						hrefTarget: '_self',
						text: 'Email'
					},
					{
						xtype: 'button',
						flex: 1,
						disabled: true,
						itemId: 'faxbtn',
						hrefTarget: '_self',
						text: 'Fax'
					},
					{
						xtype: 'button',
						flex: 1,
						disabled: true,
						itemId: 'printbtn',
						margin: '0 5 0 5',
						text: 'Print'
					},
					{
						xtype: 'button',
						flex: 1,
						//disabled: true,
						hidden: true,
						itemId: 'printlabelbtn',
						margin: '0 5 0 0',
						text: 'Print File Label'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});