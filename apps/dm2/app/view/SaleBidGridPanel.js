Ext.define('DM2.view.SaleBidGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.salebidgridpanel',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date'
    ],

    cls: 'subgrid',
	ui:'subgrid',
    frame: true,
    header: false,
    store: 'SaleBids',
    columns: [{
		xtype: 'gridcolumn',
		hidden:true,
		dataIndex: 'idSale',
		text: 'idSale',
		flex: 1
	},
	{
		xtype: 'gridcolumn',
		dataIndex: 'Buyer',
		text: 'Bidder',
		flex: 1
	},
	{
		xtype: 'datecolumn',
		renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			return Ext.util.Format.date(record.get('targetCloseDate'), "m/d/Y");
		},
		flex: 1,
		format:'m/d/Y',
		//width: 150,
		dataIndex: 'targetCloseDate',
		text: 'Good Until Date'
	},
	{
		xtype: 'gridcolumn',
		dataIndex: 'salePrice',
		text: 'Price',
		flex: 1,
        format: '0,000',
		align:'right',
		renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			//var formatVal = Ext.util.Format.number(value,'0,000');
			/*var readable = record.get('readable');
			if(readable==0){
				return null;
			}else{*/
				if(value==null || value==0){
					return null;  
				}else{
					var formatVal = Ext.util.Format.currency(value, '$',0);
					metaData.tdAttr = 'data-qtip="' + formatVal + '"';
					return formatVal;  
				}
			/*}*/
		}
	},
	{
		xtype: 'gridcolumn',
		//hidden:true,
		dataIndex: 'companyName',
		text: 'Company Name',
		flex: 1
	},
	{
		xtype: 'checkcolumn',
		//hidden:true,
		dataIndex: 'selected',
		processEvent: Ext.emptyFn,
		text: 'Selected',
		flex: 1
	}],

    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            store : me.buildStore()
        });

        me.callParent(arguments);
    },

    buildStore: function() {
        return Ext.create('DM2.store.SaleBids', {});
    }
});