Ext.define('DM2.view.ViewMasterDataForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.viewmasterdataform',

    requires: [
        'Ext.form.field.Number',
        'Ext.panel.Panel',
        'Ext.button.Button'
    ],
    frame: true,
    itemId: 'viewmasterdataform',
    scrollable: true,
    ui: 'activitypanel',
    margin:3,
    closable:true,
    bodyPadding: 5,
    //collapsible: true,
	title:'View Master Data',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
    items: [
		{
			layout:{
				type:'hbox',
				align:'stretch'
			},
			items:[{
				flex:1,
			    layout:{
					type:'vbox',
					align:'stretch'
				},
				items:[{
					xtype: 'textfield',
					hidden:true,
					readOnly:true,		
					fieldLabel: 'idProperty',
					name: 'idProperty'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Street No.',
					name: 'StreetNumber'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Township',
					name: 'Township'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Municipality',
					name: 'Municipality'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Building Class',
					name: 'BuildingClass'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Year Built',
					name: 'YearBuilt'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Condo Id#',
					name: 'idCondo'
				}]
			},
			{
				flex:1,
			    layout:{
					type:'vbox',
					align:'stretch'
				},
				items:[{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'High Street No.',
					name: 'HighStreetNumber'
				},{
					xtype:'component',
					height:'29px'
				},{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'State',
					name: 'State'   
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Boro',
					name: 'Boro'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Zoning',
					name: 'Zoning'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Unit#',
					name: 'UnitNumber'
				}]
			}]
		},
		{
			xtype: 'panel',
			margin: "0 0 10 0",
			frame : true,
			ui: 'activitypanel',
			collapsible: true,
			bodyPadding: 5,
			title : 'Master Loan',
			layout: {
				type: 'column'
			},
			items: [{
				xtype: 'panel',
				columnWidth: 0.70,
				margin: "0px 8px 0 0px",
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'datefield',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'MortgageDate',
					name: 'LastKnownMortgageDate'
				},{
					xtype: 'datefield',
					//labelWidth: 70,
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'SaleDate',
					name: 'SaleDate'
				}, {
					xtype: 'textfield',
					//labelWidth: 70,
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Current Assets',
					name: 'CurrentAVTAssess'
				},{
					xtype: 'textfield',
					//labelWidth: 70,
					//labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Last Lendee',
					name: 'LastKnownLendee'
				},{
					xtype: 'textfield',
					//labelWidth: 70,
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Last Known Lender',
					name: 'LastKnownLender'
				}, {
					xtype: 'textfield',
					//labelWidth: 70,
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Building Name',
					name: 'BuildingName'
				}, {
					xtype: 'panel',
					margin: '0px 0px 6px 0px',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						//labelWidth: 70,
						flex: 1.1,
						//labelAlign:'right',
						anchor: '100%',
						readOnly:true,
						fieldLabel: 'No. of Bldgs.',
						name: 'Buildings'
					},{
						xtype: 'textfield',
						labelAlign:'right',
						labelWidth: 70,
						anchor: '100%',
						readOnly:true,
						fieldLabel: 'Res. Units',
						name: 'ResidentialUnits',
						flex: 0.9
					}]
				},{
					xtype: 'textfield',
					//labelWidth: 70,
					//labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Bank',
					name: 'Bank'
				}]
			}, {
				xtype: 'panel',
				columnWidth: 0.30,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'textfield',
					labelWidth: 70,
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Amount',
					name: 'TotalAccumulatedLoanAmount'
				},{
					xtype: 'textfield',
					labelWidth: 70,
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Amount',
					name: 'SalePrice'
				},{
					xtype: 'textfield',
					labelWidth: 70,
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'Value',
					name: 'CurrentAddedValue'
				},{
					xtype:'component',
					height:'29px'
				},{
					xtype:'component',
					height:'29px'
				},{
					xtype:'component',
					height:'29px'
				},{
					xtype: 'textfield',
					labelWidth: 70,
					labelAlign:'right',
					anchor: '100%',
					readOnly:true,
					fieldLabel: 'CommUnits',
					name: 'CommUnits'
				}, {
					xtype:'component',
					height:'29px'
				}]
			}]
		},
		{
			xtype: 'propertyakalist',
			frame : true,
			ui: 'activitypanel',
			collapsible : true
		},
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
					hidden:true,
                    itemId: 'savepropmasterbtn',
                    text: 'Save'
                }
            ]
        },
        {
            xtype: 'textfield',
            anchor: '100%',
            hidden: true,
            fieldLabel: 'DealId',
            name: 'dealid'
        }
    ]
});