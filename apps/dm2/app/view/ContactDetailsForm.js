Ext.define('DM2.view.ContactDetailsForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.contactdetailsform',

    requires: [
        'DM2.view.ContactPhonesGridPanel',
		'DM2.view.ContactFaxGridPanel',
        'DM2.view.ContactEmailsGridPanel',
        'DM2.view.ContactAddressGridPanel',
        'DM2.view.ContactMiscGridPanel',
        'Ext.form.FieldSet',
        'Ext.form.field.Number',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'Ext.grid.Panel'
    ],

    scrollable: true,
    width: 400,
    bodyPadding: 5,
    collapsible: true,
    header: false,

    items: [
        {
            xtype: 'panel',
			//closeAction: 'demo',
			action:"toppanel",
            collapsible: true,
            title: 'Contact Info',
			closable:true,
			ui: 'activitypanel',
			frame:true,
			bodyPadding: 10,
			layout:{
				type:'vbox',
				align:'stretch'
			},
			margin:'0 0 10 0',
			header: {
				titlePosition: 0,
				items: [
					{
						xtype: 'button',
						itemId: 'contactdetailformaddbtn',
						margin: '0 10 0 0',
						text: 'Save'
					},
					{
						xtype: 'button',
						hidden: true,
						itemId: 'contactdetailformresetbtn',
						margin: '0 10 0 0',
						text: 'Reset'
					}
				]
			},
            items: [
                {
                    xtype: 'numberfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'idContact',
                    name: 'idContact'
                },
				{
                    xtype: 'numberfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'idUserxCont',
                    name: 'idUserxCont'
                },
                {
                    xtype: 'panel',
                    margin: '0 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            flex: 1,                            
                            fieldLabel: 'Contact Type',
                            labelAlign: 'right',
                            labelWidth: 70,
                            name: 'contactType',
                            displayField: 'type',
                            queryMode: 'local',
                            store: 'ContactTypes',
                            valueField: 'type'
                        }
                    ]
                },
				{
					layout:{
						type:'hbox',
						align:'stretch'
					},
					margin:'0 0 5 0',
					items:[{
						xtype: 'textfield',
						flex:1,
						anchor: '100%',                    
						fieldLabel: 'Title',
						labelAlign: 'right',
						labelWidth: 70,
						name: 'title'
					},
	                {
						xtype: 'checkboxfield',
						flex:1,
						anchor: '100%',                    
						fieldLabel: 'Add To My Contact',
						labelAlign: 'right',
						labelWidth: 130,
						name: 'isMyContact'
					},
					{
						xtype: 'checkboxfield',
						hidden:true,
						//flex:1,
						//anchor: '100%',                    
						//fieldLabel: 'Add To My Contact',
						//labelAlign: 'right',
						//labelWidth: 130,
						name: 'isMyContactOriginal'
					}]
				},
				{
					layout:{
						type:'hbox',
						align:'stretch'
					},
					margin:'0 0 5 0',
					items:[{
						xtype: 'textfield',
						flex:1,
						anchor: '100%',                    
						fieldLabel: 'First Name',
						labelAlign: 'right',
						labelWidth: 70,
						name: 'firstName'
					},
					{
						xtype: 'textfield',
						flex:1,
						anchor: '100%',                    
						fieldLabel: 'Last Name',
						labelAlign: 'right',
						labelWidth: 85,
						name: 'lastName'
					}]
				},
				{
					xtype: 'numberfield',
					hidden:true,
					anchor: '100%',
					fieldLabel: 'idCompany',
					name: 'idCompany'
				},
                {
					layout:{
						type:'hbox',
						align:'stretch'
					},
					margin:'0 0 5 0',
					items:[{
						xtype: 'textfield',
						readOnly:true,
						flex:1,
						anchor: '100%',                    
						fieldLabel: 'Company',
						labelAlign: 'right',
						labelWidth: 70,
						name: 'companyName'/*,
						triggers: {
							clear: {
								cls: 'x-form-arrow-trigger',
								handler: function() {
									this.fireEvent("ontriggerclick", this, 'company');
								}
							}
						}*/
					},
					{
						xtype: 'button',
						iconCls:'dots-three-horizontal',
						margin:'0 0 0 5',
						action:'companypickbtn'
					}]                 
                },
                {
					layout:{
						type:'hbox',
						align:'stretch'
					},
					margin:'0 0 5 0',
					items:[{
						xtype: 'textfield',
						flex:1,
						anchor: '100%',                    
						fieldLabel: 'Position',
						labelAlign: 'right',
						labelWidth: 70,
						name: 'position'
					},
					/*{
						xtype: 'component',
						flex:0.5
					},*/
					{
						xtype: 'checkboxfield',
						flex:1,
						align:'right',
						anchor: '100%',                    
						fieldLabel: 'Show Company',
						labelAlign: 'right',
						labelWidth: 120,
						name: 'showCompany'
					}]                
                }
            ]
        },
        {
            xtype: 'contactphonesgridpanel'
        },
		{
            xtype: 'contactfaxgridpanel',
			margin: '10 0 10 0'
        },
        {
            xtype: 'contactemailsgridpanel',
            margin: '10 0 10 0'
        },
        {
            xtype: 'contactaddressgridpanel',
            margin: '0 0 10 0'
        },
        {
            xtype: 'contactmiscgridpanel'
        }
    ]

});