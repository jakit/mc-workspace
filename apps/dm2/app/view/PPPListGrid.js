Ext.define('DM2.view.PPPListGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ppplistgrid',

    requires: [
        'Ext.grid.Panel'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.margin = 3;
		me.ui = 'activitypanel';
		me.closable = true;
		me.title = 'PPP List';
		me.itemId = 'ppplistgrid';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action:'savepppbtn',
					margin: '0 10 0 0',
					text: 'Save'
				}
			]
		};
		me.sortableColumns = false;
		me.store = 'PPPList';
		me.scrollable = true;
		me.columns = [
			{
				xtype: 'gridcolumn',
				flex:1,
				hidden:true,
				dataIndex: 'idPPP',
				text: 'idPPP'
			},
			{
				xtype: 'gridcolumn',
				flex:1,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'pppValue',
				text: 'PPP Value'
			}
		];
		me.callParent(arguments);
    }
});