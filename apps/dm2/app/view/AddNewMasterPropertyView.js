Ext.define('DM2.view.AddNewMasterPropertyView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addnewmasterpropertyview',

    requires: [
        'Ext.form.field.Number',
        'Ext.panel.Panel',
        'Ext.button.Button'
    ],
	margin: 3,
    closable:true,
    frame: true,
    scrollable: true,
    ui: 'activitypanel',
    defaults: {
        labelWidth: 120
    },
    bodyPadding: 5,
	header:{
		titlePosition: 0,
		items: [
			{
				xtype: 'button',
				action: 'createnewpropertybtn',
				text: 'Create'
			}
		]
	},
	layout: {
		type:'vbox',
		align: 'stretch'
	},
    items: [
		{
			layout:{
				type:'hbox',
				align:'stretch'
			},
			items:[{
				flex:1,
			    layout:{
					type:'vbox',
					align:'stretch'
				},
				items:[{
					xtype: 'textfield',
					hidden:true,
					readOnly:false,		
					fieldLabel: 'idProperty',
					name: 'idProperty'
				},{
					xtype: 'textfield',
					hidden:true,
					readOnly:false,
					fieldLabel: 'Property No.',
					name: 'idPropertyMaster'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'State',
					name: 'State'   
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Block',
					name: 'Block'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'StreetNumber',
					name: 'StreetNumber'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Street',
					name: 'StreetName'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Owner',
					name: 'Owner'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Township',
					name: 'Township'
				},
				{
					xtype: 'textfield',				
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'BuildingName',
					name: 'BuildingName'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'CurrentAVTAssess',
					name: 'CurrentAVTAssess'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Zoning',
					name: 'Zoning'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'LotDepth',
					name: 'LotDepth'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'BuildingFront',
					name: 'BuildingFront'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'LandArea',
					name: 'LandArea'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Stories',
					name: 'Stories'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'ResidentialUnits',
					name: 'ResidentialUnits'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'No.OfBedrooms',
					name: 'No.OfBedrooms'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'SaleDate',
					name: 'SaleDate'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'LastKnownLender',
					name: 'LastKnownLender'
				},
				{
					xtype: 'datefield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'LastKnown MortgageDate',
					name: 'LastKnownMortgageDate'
				},
				{
					xtype: 'checkboxfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'ManualBankUpate',
					name: 'ManualBankUpate'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'idCondo',
					name: 'idCondo'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'EstimatedYearBuilt',
					name: 'EstimatedYearBuilt'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'StandardStreet',
					name: 'StandardStreet'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'StandardStreetName',
					name: 'StandardStreetName'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'AddStandardStatus',
					name: 'AddStandardStatus'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'lot2',
					name: 'lot2'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'lastDealID',
					name: 'lastDealID'
				},
				{
					xtype: 'textfield',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'LastPrimary CSRInitials',
					name: 'lastPrimaryCSRInitials'
				}]
			},
			{
				flex:1,
			    layout:{
					type:'vbox',
					align:'stretch'
				},
				items:[{
					xtype: 'textfield',
					readOnly:false,
					labelAlign:'right',				
					fieldLabel: 'PropertyMasterCodeSource',
					name: 'PropertyMasterCodeSource'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Boro',
					name: 'Boro'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Lot',
					name: 'Lot'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'HighStreetNumber',
					name: 'HighStreetNumber'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'ZipCode',
					name: 'ZipCode'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'APN',
					name: 'APN'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Municipality',
					name: 'Municipality'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Curr.AddedValue',
					name: 'CurrentAddedValue'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'FullValue',
					name: 'FullValue'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'FrontLot',
					name: 'FrontLot'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'PropertyTax',
					name: 'PropertyTax'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'BuildingDept',
					name: 'BuildingDepartment'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Buildings',
					name: 'Buildings'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'CommUnits',
					name: 'CommUnits'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'SquareFootage',
					name: 'SquareFootage'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'TotalAccumulatedLoanAmount',
					name: 'TotalAccumulatedLoanAmount'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'SalePrice',
					name: 'SalePrice'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'Bank',
					name: 'Bank'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'LastKnown Lendee',
					name: 'LastKnownLendee'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'UnitNumber',
					name: 'UnitNumber'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'YearBuilt',
					name: 'YearBuilt'
				},
				{
					xtype: 'datefield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'UpdateDate',
					name: 'UpdateDate'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'StandardPrefixStreet',
					name: 'StandardPrefixStreet'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'StandardCity',
					name: 'StandardCity'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'AKAList',
					name: 'AKAList'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'CSRName',
					name: 'CSRName'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'LastPropertyID',
					name: 'lastPropertyID'
				},
				{
					xtype: 'textfield',
					labelAlign:'right',
					anchor: '100%',
					readOnly:false,
					fieldLabel: 'NumberOf Brothers',
					name: 'numberOfBrothers'
				}]
			}]
		},		
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
					hidden:true,
                    itemId: 'savepropmasterbtn',
                    text: 'Save'
                }
            ]
        },
        {
            xtype: 'textfield',
            anchor: '100%',
            hidden: true,
            fieldLabel: 'DealId',
            name: 'dealid'
        }
    ]
});