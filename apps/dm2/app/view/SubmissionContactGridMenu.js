Ext.define('DM2.view.SubmissionContactGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.submissioncontactgridmenu',

    requires: [
        'Ext.menu.Item'
    ],
	
    items: [
        {
            xtype: 'menuitem',
            itemevent: 'addcontact',
            text: 'Add Contact',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemevent: 'removecontact',
            text: 'Remove Contact',
            focusable: true
        }
    ]

});