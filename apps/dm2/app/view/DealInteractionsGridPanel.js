Ext.define('DM2.view.DealInteractionsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealinteractionsgridpanel',

    requires: [
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.grid.column.Boolean',
        'Ext.view.Table'
    ],

    cls: 'gridcommoncls',
    height: 250,
    width: 400,
    title: 'Interactions',

    columns: [
        {
            xtype: 'gridcolumn',
            dataIndex: 'string',
            text: 'String'
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'number',
            text: 'Number'
        },
        {
            xtype: 'datecolumn',
            dataIndex: 'date',
            text: 'Date'
        },
        {
            xtype: 'booleancolumn',
            dataIndex: 'bool',
            text: 'Boolean'
        }
    ]

});