Ext.define('DM2.view.AddSubmissionDocPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addsubmissiondocpanel',

    requires: [
        'Ext.grid.Panel',
        'Ext.view.Table',
        'Ext.grid.column.Widget',
        'Ext.ProgressBarWidget',
        'Ext.form.field.TextArea',
        'Ext.button.Button',
        'Ext.toolbar.Toolbar'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent : function() {
        var me = this;
		me.closable = true;
		me.title = 'Add Document';
		var docstemp = Ext.create('DM2.store.DocsTemp');
		
		me.items = [
			{
				xtype: 'gridpanel',
				cls: 'gridcommoncls',
				store: 'DocsTemp',
				columns: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'name',
						text: 'Name',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'size',
						text: 'Size',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'desc',
						text: 'Description',
						flex: 1
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'clsfctn',
						text: 'Classification',
						flex: 1
					}
				]
			},
			{
				xtype: 'panel',
				bodyPadding: 5,
				title: 'Details',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'panel',
						margin: '0 0 5 0',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',
								hidden:true,
								fieldLabel: 'Ext',
								labelWidth: 80,
								name: 'ext'
							},
							{
								xtype: 'textfield',
								flex: 1.2,
								fieldLabel: 'Name',
								labelWidth: 80,
								name: 'name',
								allowBlank: false,
								enableKeyEvents: true
							},
							{
								xtype: 'textfield',
								flex: 0.8,
								fieldLabel: 'Size (kb)',
								labelAlign: 'right',
								name: 'docfilesize',
								readOnly: true
							}
						]
					},
					{
						xtype: 'textareafield',
						labelWidth: 80,
						fieldLabel: 'Description',
						name: 'desc',
						enableKeyEvents: true
					},
					{
						xtype: 'textfield',
						labelWidth: 80,
						fieldLabel: 'Classification',
						name: 'clsfctn',
						enableKeyEvents: true,
						maxLength : 10,
						enforceMaxLength : true
					}
				]
			}
		];
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'bottom',
				ui: 'footer',
				defaults: {
					minWidth: 75
				},
				items: [
					{
						xtype: 'component',
						flex: 1
					},
					{
						xtype: 'button',
						itemId: 'docuploadallbtn',
						text: 'Upload'
					},
					{
						xtype: 'button',
						itemId: 'docclearallbtn',
						text: 'Clear'
					}
				]
			}
		]
		me.callParent(arguments);
    }
});