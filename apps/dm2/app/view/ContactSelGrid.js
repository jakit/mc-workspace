Ext.define('DM2.view.ContactSelGrid', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    xtype: 'contactselgrid',
    requires: ['DM2.view.component.grid.SelectionGridNew'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        //me.header = false;
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					margin: '0 10 0 0',
					action: 'filterbtn',
					actiontext: 'filter',
					text: 'Filter'
				}
			]
		};
        me.itemId = 'contactselgrid';
        me.store = Ext.create('DM2.store.ContactAllSelBuff');

		me.columns = [{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'firstName',
			text: 'FirstName',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'lastName',
			text: 'LastName',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'companyName',
			text: 'Company',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'contactType',
			text: 'Contact Type',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			hidden:true,
			dataIndex: 'title',
			text: 'Title',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			hidden: true,
			dataIndex: 'phone',
			text: 'Phone',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			//hidden: true,
			dataIndex: 'email',
			text: 'Email',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}];
		me.selModel = {
			selType: 'checkboxmodel'
		};
        me.callParent(arguments);
    }
});