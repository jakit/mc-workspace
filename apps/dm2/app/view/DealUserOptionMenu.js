Ext.define('DM2.view.DealUserOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.dealuseroptionmenu',

    requires: [
        'Ext.menu.Item'
    ],

    width: 120,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'edituser',
            text: 'Edit User',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'adduser',
            text: 'Add User',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
			hidden:true,
            itemId: 'removeuser',
            text: 'Remove User',
            focusable: true,
			modifyMenu : 'yes'
        }
    ]

});