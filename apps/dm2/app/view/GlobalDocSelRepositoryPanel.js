Ext.define('DM2.view.GlobalDocSelRepositoryPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.globaldocselrepositorypanel',

    requires: [
        'DM2.view.GlobalDocsSelRepositoryGridPanel',
        'Ext.form.Panel',
        'Ext.button.Button',
        'Ext.form.field.Number',
        'Ext.grid.Panel'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.frame = true;
		me.margin = 3;
		me.ui = 'activitypanel';
		me.layout = 'border';
		me.closable = true;
		me.title = 'Select Document';

		me.items = [
			{
				xtype: 'form',
				region: 'north',
				split: true,
				bodyPadding: 10,
				collapsible: true,
				header: false,
				title: 'Bank Info',
				items: [
					{
						xtype: 'panel',
						margin: '0 0 5 0',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'textfield',
								flex: 1,
								itemId: 'name',                            
								fieldLabel: 'Doc Name',
								labelWidth: 80,
								name: 'name'
							},
							{
								xtype: 'button',
								itemId: 'savedocbtn',
								margin: '0 0 0 10',
								text: 'Save'
							}
						]
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'Description',
						labelWidth: 80,
						name: 'description'
					},
					{
						xtype: 'panel',
						margin: '5 0 0 0',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [                        
							{
								xtype: 'combobox',
								flex: 1,
								itemId: 'classification',                           
								fieldLabel: 'Classification',
								//labelAlign: 'right',
								labelWidth: 80,
								name: 'classification',
								queryMode: 'local',
								store: 'Classifications',
								valueField: 'text',
								maxLength : 10,
								enforceMaxLength : true
							},
							{
								xtype: 'datefield',
								flex:1,
								readOnly:true,
								labelAlign: 'right',
								labelWidth: 55,
								fieldLabel: 'Date',
								name: 'modifyDateTime'
							},
							{
								xtype: 'numberfield',
								hidden: true,
								fieldLabel: 'documentid',
								name: 'idDocument'
							}
						]
					},
					{
						xtype: 'textfield',
						anchor: '100%',
						hidden: true,
						fieldLabel: 'Label',
						name: 'docid'
					}
				]
			},
			{
				xtype: 'globaldocsselrepositorygridpanel',
				region: 'center'
			}
		];
		me.callParent(arguments);
    }
});