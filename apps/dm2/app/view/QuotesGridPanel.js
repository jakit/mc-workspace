Ext.define('DM2.view.QuotesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.quotesgridpanel',

    requires: [
        'Ext.grid.column.Check',
        'Ext.grid.column.Number',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.menu.Menu'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
	    me.cls = 'gridcommoncls colorgridcls';
    	me.store = Ext.create('DM2.store.DealQuotesAll');//'DealQuotesAll';
		
	    me.columns = [
			/*{
				xtype: 'checkcolumn',
				hidden: true,
				dataIndex: 'selected',
				text: 'Selected',
				flex: 1
			},*/
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'bank',
				text: 'Bank',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'quotetype',
				text: 'Loan Type',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				width: 70,
				dataIndex: 'term',
				text: 'Term'
			},
			{
				xtype: 'numbercolumn',
				//dataIndex: 'receivedamount',
				dataIndex: 'amountRequested',
				text: 'Requested Amount',
				flex: 1,
				format: '0,000',
				align:'right',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					 //var formatVal = Ext.util.Format.number(value,'0,000');
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;
					}
				}
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'ratetype',
				text: 'Rate Type',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'spread',
				text: 'Spread',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'index',
				text: 'Index',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'indexvalue',
				text: 'Index Value',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'amortterm',
				text: 'Amort Term',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				width: 60,
				dataIndex: 'io',
				text: 'IO',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'ppp',
				text: 'PPP',
				flex: 1
			}/*,
			{
				xtype: 'gridcolumn',
				align:'center',
				//hidden: true,
				dataIndex: 'selected',
				text: 'OperatingAS',
				flex: 1,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					if(value=='S'){
						return '<span style="color:green;">'+value+'</span>';
					}else if(value=='Q'){
						return '<span style="color:blue;">'+value+'</span>';
					}else if(value=='P'){
						return '<span style="color:brown;">'+value+'</span>';
					}					
				}
			}*/
		];
		me.viewConfig = {
			getRowClass: function(record, rowIndex, rowParams, store) {
				if(record && record.get('selected') ===  "S"){
					return 'green-row-color-cls';
				}else if(record && record.get('selected') ===  "Q"){
					return 'blue-row-color-cls';
				}else if(record && record.get('selected') ===  "P"){
					return 'red-row-color-cls';
				}
			}
		};
    	me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				itemId: 'quotegridtoolbar',
				items: [
					{
						xtype: 'button',
						iconCls: 'add',
						text: 'Add Quote',
						menu: {
							xtype: 'menu',
							itemId: 'addquotetoolbarmenu'
						}
					},
					{
						xtype: 'button',
						hidden: true,
						itemId: 'receivequotedonebtn',
						text: 'Done'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});