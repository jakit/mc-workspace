Ext.define('DM2.view.FileSubmissionGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.filesubmissiongridmenu',

    requires: [
        'Ext.form.field.File',
        'Ext.form.field.FileButton',
        'Ext.menu.Item'
    ],

    items: [
        {
            xtype: 'filefield',
            itemevent: 'addfile',
            itemId: 'subaddfile',
            margin: '0 0 0 19px',
            ui: 'cstfilefieldui',
            buttonOnly: true,
            buttonText: 'Add File',
            buttonConfig: {
                xtype: 'filebutton',
                ui: 'filefieldbtnui-small',
                text: 'Add File'
            }
        },
        {
            xtype: 'menuitem',
            itemevent: 'filetemplate',
            text: 'Create file from Template',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemevent: 'filerepository',
            text: 'Import file from Repository',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemevent: 'removefile',
			itemId: 'removefilefromsubmission',
            text: 'Remove file from Submission',
            focusable: true
        }
    ]
});