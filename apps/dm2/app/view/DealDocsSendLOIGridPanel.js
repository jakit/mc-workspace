Ext.define('DM2.view.DealDocsSendLOIGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealdocssendloigridpanel',

    requires: [
        'DM2.view.override.DealDocsSendLOIGridPanel',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.grid.plugin.DragDrop',
        'Ext.util.Point',
        'Ext.selection.CheckboxModel',
        'Ext.toolbar.Toolbar',
        'Ext.button.Split',
        'Ext.menu.Menu',
        'Ext.menu.Item'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable : true,
    initComponent : function() {
        var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.itemId = 'dealdocssendloigridpanel';
		me.maxHeight = 250;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.title = 'Documents';
		//var senddocsperdealstore = Ext.create('DM2.store.SendDocsPerDeal');
		me.store = Ext.create('DM2.store.SendDocsPerDeal');//'SendDocsPerDeal';

		me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var docurlname = record.get('url');
					var ext = DM2.app.getController('MainController').getExtImg(docurlname);
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return '<a target="_blank" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
				},
				dataIndex: 'name',
				text: 'Name',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				width: 90,
				dataIndex: 'desc',
				text: 'Description'
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				width: 65,
				align: 'center',
				dataIndex: 'clsfctn',
				text: 'Classification'
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				width: 70,
				align: 'center',
				dataIndex: 'modifiedBy',
				text: 'By'
			}
		];
		me.viewConfig = {
			plugins: [
				{
					ptype: 'gridviewdragdrop',
					ddGroup: 'dropsubmissiondoc',
					enableDrag: false
				}
			]
		};
		me.selModel = {
			selType: 'checkboxmodel'
		};
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				itemId: 'senddoctoolbar',
				items: [
					{
						xtype: 'splitbutton',
						hidden: true,
						text: 'Select',
						menu: {
							xtype: 'menu',
							itemId: 'senddoctoolbarmenu',
							items: [
								{
									xtype: 'menuitem',
									itemevent: 'addfile',
									text: 'Add File',
									focusable: true
								},
								{
									xtype: 'menuitem',
									itemevent: 'filetemplate',
									text: 'Create file from Template',
									focusable: true
								},
								{
									xtype: 'menuitem',
									itemevent: 'filerepository',
									text: 'Import file from Repository',
									focusable: true
								}
							]
						}
					},
					{
						xtype: 'button',
						itemId: 'senduploaddocbtn',
						text: 'Upload New Document'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});