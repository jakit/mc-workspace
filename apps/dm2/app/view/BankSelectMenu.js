Ext.define('DM2.view.BankSelectMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.bankselectmenu',

    requires: [
        'Ext.menu.Item'
    ],

    width: 160,

    items: [
        {
            xtype: 'menuitem',
            text: 'Add Submission',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Remove Submission',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Global Submission',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Mark activity as done',
            focusable: true
        }
    ]

});