Ext.define('DM2.view.CreateNewDealFromPropertyPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.createnewdealfrompropertypanel',

    requires: [
		'DM2.view.CreateNewDealFromPropertyGrid',
        'DM2.view.CreateNewDealFromPropertyDocsGridPanel',
        'DM2.view.CreateNewDealUserGridPanel',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.view.Table',
        'Ext.selection.CheckboxModel',
        'Ext.grid.column.Check',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
		'DM2.store.DealStatus'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable : true,
    initComponent: function() {
		var me = this;
    	me.frame = true;
	    me.margin = 3;
	    //me.scrollable = true;
	    me.ui = 'activitypanel';
	    me.bodyPadding = 5;
	    me.closable = true;
		
		var officest = Ext.create('DM2.store.Office');
		var dealStatusSt = Ext.create('DM2.store.DealStatus');
		
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
	    me.items = [
        {
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textfield',
					hidden:true,
                    flex: 1,
                    itemId: 'filenumber',
                    fieldLabel: 'File #',
                    labelAlign: 'right',
                    labelWidth: 40,
                    name: 'filenumber'
                },
				{
                    xtype: 'textfield',
                    flex: 1,
                    itemId: 'name',
                    fieldLabel: 'Name',
                    labelAlign: 'right',
                    labelWidth: 60,
                    name: 'name'
                },
                {
                    xtype: 'combobox',
                    flex: 1,
					queryMode:'local',
                    itemId: 'status',
                    fieldLabel: 'Status',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'status',
                    displayField: 'statusFullName',
                    store: dealStatusSt,
                    valueField: 'status'
                }
            ]
        },
        {
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
					xtype: 'combobox',
					flex: 1,
					selectOnFocus: true,
					displayField: 'selectionDesc',
					queryMode: 'local',
					store: 'MortgageTypeList',//ratetypelist_st,
					typeAhead: true,
					valueField: 'selectionDesc',                 
					fieldLabel: 'Mortgage Type',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'mortgageType'
				},
                {
                    xtype: 'combobox',
                    flex: 1,
					queryMode:'local',
                    itemId: 'office',
                    fieldLabel: 'Office',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'office',
                    displayField: 'code',
                    store: officest,
                    valueField: 'code'
                }
            ]
        },
		{
            xtype: 'createnewdealfrompropertygrid'
        },
        {
            xtype: 'createnewdealfrompropertycontactgrid'
        },
        {
            xtype: 'createnewdealfrompropertydocsgridpanel'
        },
        {
            xtype: 'createnewdealusergridpanel'
        }
    ];
		
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'createnewdealbtn',
					itemId: 'createnewdealbtn',
					text: 'Create'
				}
			]
		};
		me.callParent(arguments);
    }
});