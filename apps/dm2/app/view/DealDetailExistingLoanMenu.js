Ext.define('DM2.view.DealDetailExistingLoanMenu', {
    extend: 'DM2.view.component.container.DealsDetailsContextMenu',
    alias: 'widget.dealdetailexistingloanmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
			action:'editloan',
            text: 'Edit Loan',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        }];
        me.callParent(arguments);
    }
});