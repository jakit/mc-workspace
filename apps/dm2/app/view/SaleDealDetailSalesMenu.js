Ext.define('DM2.view.SaleDealDetailSalesMenu', {
    extend: 'DM2.view.component.container.DealsDetailsContextMenu',
    alias: 'widget.saledealdetailsalesmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
			action: 'addbid',
            text: 'Add Bid',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			action: 'editsales',
            text: 'Edit Sales',
            focusable: true,
			modifyMenu : 'yes'
        }];
        me.callParent(arguments);
    }
});