Ext.define('DM2.view.StructuredGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.structuredgridpanel',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel',
		'DM2.store.LoanSchedule'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	initComponent: function() {
        var me = this;
		//var gridModel = me.generateGridModel();
		/*me.store = Ext.create('DM2.store.LoanSchedule', {
			extend: 'Ext.data.Store',
		
			requires: [
				//'DM2.model.ContactPhone',
				'Ext.data.proxy.Rest',
				'Ext.data.reader.Json',
				'Ext.data.writer.Json'
			],							          
            storeId: 'LoanSchedule',
            autoSync: true,
            model: gridModel,//'DM2.model.ContactEmail',
            proxy: {
                type: 'rest',
                api: {
                    create: DM2.view.AppConstants.apiurl+'mssql:LoanSchedule',
                    read: DM2.view.AppConstants.apiurl+'mssql:v_LoanSched',
                    update: DM2.view.AppConstants.apiurl+'mssql:LoanSchedule',
                    destroy: DM2.view.AppConstants.apiurl+'mssql:LoanSchedule'
                },
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_LoanSched',
                reader: {
                    type: 'json'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function(data, request) {
                            // do some manipulation of the unserialized data object
                            //console.log(data);
                            //console.log(request);
                            if(request._action==="create"){
                                Ext.Object.each(data, function(key, value, myself) {
                                    //console.log(key + ":" + value);
                    
                                    if (key === 'idLoanSched') {
                                        //console.log("Delete the key"+key);
                                        delete data[key];
                                    }
                                });
                            }
                            if(request._action==="update"){
                                data['@metadata'] = {'checksum' : 'override'};
                            }
							console.log(request);
							if(request._action==="destroy"){
								console.log("Calling destroy action");
                                //data['checksum'] = 'override';
								request._url = request._url+"?checksum=override";
                            }
                            //console.log(data);
                            return data;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                write: {
					fn: function(store, operation, eOpts) {
						if(operation.request._action==="create"){
							console.log("Create Write completes");
							//console.log(operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(false);
							store.getAt(store.getCount()-1).set('idLoanSched',operation._records[0].data.txsummary[0].idLoanSched);
							//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
							store.setAutoSync(true);
						}
					},
					scope: this
                }
            },
        

			onStoreWrite: function(store, operation, eOpts) {
				//console.log(store);
				//console.log(operation);
				if(operation.request._action==="create"){
					//console.log("Create Write completes");
					//console.log(operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(false);
					//store.getAt(0).set('idPhone',operation._records[0].data.txsummary[0].idPhone);
					//store.setAutoSync(true);
				}
			}        
        });*/
		me.store =  Ext.create('DM2.store.LoanSchedule');
		me.frame = true;
		me.margin = '0 0 0 0';
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.hidden =true;
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					itemId: 'addloansched',
					action:'addloansched',
					iconCls: 'add',
					text: 'Add'
				}
			]
		};
	    me.title = 'Structured Rate Schedule';
	    me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'startMonth',
				text: 'Start Month',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'endMonth',
				text: 'End Month',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'months',
				text: 'Duration',
				flex: 1/*,
				editor: {
					xtype: 'textfield'
				}*/
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'rate',
				text: 'Rate',
				flex: 1/*,
				editor: {
					xtype: 'textfield'
				}*/
			}
		];
		me.dockedItems = [{
			xtype: 'form',
			hidden:true,
			dock: 'bottom',			
			title:'Add Structured Rate Schedule',
			itemId:'structuredschedform',
			closable: true,
			closeAction : 'hide',
			//me.hidden = true;
			header : {
				titlePosition: 0,
				items: [
					{
						xtype: 'button',
						//disabled:true,
						action:'saveloanschedbtn',
						text: 'Save'
					}
				]
			},
			items: [{
				xtype:'panel',
				padding:10,
				layout:{
					type:'hbox',
					align:'center'
				},
				items:[{
					xtype: 'numberfield',
					hidden:true,
					fieldLabel: 'idLoanSched',
					labelAlign: 'left',
					labelWidth: 70,
					name: 'idLoanSched'
				},{
					xtype: 'numberfield',
					flex:1,
					hideTrigger:true,
					//keyNavEnabled:false,
					//mouseWheelEnabled:false,
					fieldLabel: 'Duration',
					//labelAlign: 'right',
					labelWidth: 60,
					name: 'months'
				},
				{
					xtype: 'numberfield',
					flex:1,
					hideTrigger:true,
					//keyNavEnabled:false,
					//mouseWheelEnabled:false,
					fieldLabel: 'Rate',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'rate'
				}]
			}]
		}];
		/*me.plugins = [
			{
				ptype: 'cellediting'
			}
		];
		me.selModel = {
			selType: 'cellmodel'
		};*/
		me.callParent(arguments);
	}
});