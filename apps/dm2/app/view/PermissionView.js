Ext.define('DM2.view.PermissionView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.permissionview',

    requires: [
        'DM2.view.PermissionSelectedGridPanel',
        'DM2.view.PermissionAllGridPanel',
        'Ext.grid.Panel',
        'Ext.button.Button'
    ],

    frame: true,
    itemId: 'permissionview',
    margin: 3,
    scrollable: true,
    ui: 'activitypanel',
    bodyPadding: 5,
    closable: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'permissionselectedgridpanel',
            margin: '0 0 5 0'
        },
        {
            xtype: 'permissionallgridpanel',
            margin: '5 0 0 0'
        }
    ],
    dockedItems: [
        {
            xtype: 'container',
            flex: 1,
            dock: 'bottom',
            layout: {
                type: 'hbox',
                align: 'stretch',
                padding: 5
            },
            items: [
                {
                    xtype: 'button',
                    flex: 1,
                    margin: '0 5 0 0',
                    text: 'Save'
                },
                {
                    xtype: 'button',
                    flex: 1,
                    margin: '0 0 0 5',
                    text: 'Restore'
                }
            ]
        }
    ]

});