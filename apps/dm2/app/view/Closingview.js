Ext.define('DM2.view.Closingview', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.closingview',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Date',
        'Ext.form.field.Checkbox',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Number',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.XTemplate'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.itemId = 'closingview';
		me.margin = 3;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;
		me.cls = ['removenumcls'];
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		var existingLoansSt = Ext.create('DM2.store.ExistingLoans');//'ExistingLoans';
		var dealpropertySt = Ext.create('DM2.store.Dealproperty');//'Dealproperty';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'doneActionBtn',
					margin: '0 10 0 0',
					text: 'Done'
				}
			]
		};
		me.items = [
			{
				xtype: 'form',
				//bodyPadding: 5,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					/*{
						xtype: 'panel',
						margin: '0 0 5 0',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [
							{
								xtype: 'datefield',
								flex: 1.2,
								labelWidth:120,
								fieldLabel: 'Scheduled Closing Date',
								name: 'trgtclosingdt'
							},
							{
								xtype: 'checkboxfield',
								flex: 0.8,
								fieldLabel: 'Closed',
								labelAlign: 'right'
							}
						]
					},*/
					{
						xtype: 'datefield',
						//flex: 1.2,
						labelWidth:120,
						fieldLabel: 'Scheduled Closing Date',
						name: 'trgtclosingdt'
					},
					{
						xtype: 'datefield',
						//flex: 1.2,
						labelWidth:120,
						fieldLabel: 'Close Date',
						name: 'closedt'
					}/*,
					{
						xtype: 'checkboxfield',
						labelWidth:120,
						itemId: 'sendinvoice',
						fieldLabel: 'Send Invoice'
					}*/
				]
			},
			{
				xtype: 'form',
				frame: true,
				itemId: 'closingloanfrm',
				//scrollable: true,
				ui: 'activitypanel',
				bodyPadding: 5,
				collapsible: true,
				title: 'Payoff',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'combobox',
						//flex: 1,
						itemId: 'loancombo',                    
						fieldLabel: 'Loans',
						labelWidth: 80,
						displayField: 'bank',
						queryMode: 'local',
						store: existingLoansSt,//'ExistingLoans',
						typeAhead: true,
						valueField: 'loanid'
					},
					{
						xtype: 'numberfield',
						labelWidth: 80,
						fieldLabel: 'Target Payoff Amount',
						name: 'trgtpayoutamt'
					}
					/*{
						xtype: 'panel',
						layout: 'column',
						items: [
							{
								xtype: 'panel',
								columnWidth: 0.5,
								margin: '0 5 0 0',
								layout: {
									type: 'vbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'textfield',                                    
										fieldLabel: 'Bank',
										labelWidth: 70,
										name: 'bank',
										readOnly: true
									},
									{
										xtype: 'numberfield',										
										fieldLabel: 'Requested Amount',
										labelWidth: 70,
										name: 'receivedamount',
										readOnly: true
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'Term',
										//labelAlign: 'right',
										labelWidth: 70,
										name: 'term',
										readOnly: true
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'Index',
										labelWidth: 70,
										name: 'index',
										readOnly: true
									},
									{
										xtype: 'numberfield',
										labelWidth: 80,
										fieldLabel: 'Target Payoff Amount',
										name: 'trgtpayoutamt'
									}
								]
							},
							{
								xtype: 'panel',
								columnWidth: 0.5,
								layout: {
									type: 'vbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'textfield',										
										fieldLabel: 'Loan Type',
										labelAlign: 'right',
										labelWidth: 70,
										name: 'loantype',
										readOnly: true
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'Index Value',
										labelAlign: 'right',
										labelWidth: 70,
										name: 'indexvalue',
										readOnly: true
									},
									{
										xtype: 'textfield',
										labelAlign: 'right',
										fieldLabel: 'Rate Type',
										labelWidth: 70,
										name: 'ratetype',
										readOnly: true
									},									
									{
										xtype: 'textfield',										
										fieldLabel: 'Spread',
										labelAlign: 'right',
										labelWidth: 70,
										name: 'spread',
										readOnly: true
									},									
									{
										xtype: 'datefield',
										labelWidth: 80,
										fieldLabel: 'Target Payoff Date',
										name: 'trgtpayoutdt'
									}
								]
							}
						]
					}*/
				],
				dockedItems: [
					{
						xtype: 'toolbar',
						dock: 'bottom',
						ui: 'footer',
						items: [
							{
								xtype: 'component',
								flex: 1
							},
							{
								xtype: 'button',
								text: 'Save'
							},
							{
								xtype: 'button',
								text: 'Reset'
							}
						]
					}
				]
			},
			{
				xtype: 'form',
				hidden:true,
				frame: true,
				itemId: 'propertyfrm',
				margin: '5 0 0 0',
				scrollable: 'true',
				ui: 'activitypanel',
				bodyPadding: 5,
				collapsible: true,
				title: 'Property',
				items: [
					{
						xtype: 'combobox',
						tpl: '<tpl for="."><div class="x-boundlist-item">{street_no} - {street}</div></tpl>',
						anchor: '100%',
						itemId: 'cvpropertycombo',						
						fieldLabel: 'Property',
						labelWidth: 60,
						displayField: 'street',
						displayTpl: [
							'<tpl for=".">',
							'{street_no} - {street}',
							'</tpl>'
						],
						queryMode: 'local',
						store: dealpropertySt,
						typeAhead: true,
						valueField: 'propertyid'
					},
					{
						xtype: 'panel',
						layout: 'column',
						items: [
							{
								xtype: 'panel',
								columnWidth: 0.5,
								margin: '0 5 0 0',
								layout: {
									type: 'vbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'checkboxfield',
										fieldLabel: 'Survey'
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'No',
										labelWidth: 60,
										name: 'street_no',
										readOnly: true
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'City',
										labelWidth: 60,
										name: 'city',
										readOnly: true
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'Property Type',
										labelWidth: 60,
										name: 'propertytype',
										readOnly: true
									}
								]
							},
							{
								xtype: 'panel',
								columnWidth: 0.5,
								layout: {
									type: 'vbox',
									align: 'stretch'
								},
								items: [
									{
										xtype: 'checkboxfield',
										fieldLabel: 'Title Insurance'
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'Street',
										labelAlign: 'right',
										labelWidth: 50,
										name: 'street',
										readOnly: true
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'State',
										labelAlign: 'right',
										labelWidth: 50,
										name: 'state'
									},
									{
										xtype: 'textfield',										
										fieldLabel: 'Zip',
										labelAlign: 'right',
										labelWidth: 50,
										name: 'zip'
									}
								]
							}
						]
					}
				]
			}
		];
		me.callParent(arguments);
    }
});