Ext.define('DM2.view.AddDealUserPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.adddealuserpanel',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Number',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
		'DM2.view.AddDealUserGrid'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);        
    },
    initComponent: function() {
        var me = this;
		me.frame = true;
		me.margin = 3;
		me.ui = 'activitypanel';
		me.closable = true;
		var rolesst = Ext.create('DM2.store.Roles');
		me.header  = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					//formBind: true,
					//disabled: true,
					itemId: 'adddealusersubmitbtn',
					text: 'Save'
				},
				{
					xtype: 'button',
					hidden:true,
					itemId: 'adddealuseresetbtn',
					text: 'Reset'
				}
			]
		};
		me.layout  = 'border';
		me.items = [{
				xtype: 'panel',
				region: 'north',
				split: true,
				collapsible: true,
				scrollable: true,
				bodyPadding: 5,
				header:false,
				layout: {
					type: 'column'
				},
				items: [{
					xtype: 'panel',
					columnWidth: 0.5,
					margin: '0 5 0 0',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						anchor: '100%',
						fieldLabel: 'First Name',
						name: 'firstName',
						readOnly: true
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Address',
						name: 'address',
						readOnly: true
					},
					{
						xtype: 'textfield',
						fieldLabel: 'State',
						name: 'state',
						readOnly: true
					},
					{
						xtype: 'textfield',
						fieldLabel: 'User Name',
						name: 'userName',
						readOnly: true
					},
					{
						xtype: 'combobox',
						anchor: '100%',
						fieldLabel: 'Role',
						name: 'role',
						allowBlank: false,
						displayField: 'description',
						queryMode: 'local',
						store: rolesst, //'Roles',
						valueField: 'idRole'
					},
					{
						xtype: 'numberfield',
						anchor: '100%',
						hidden: true,
						fieldLabel: 'idDealxUser',
						name: 'idDealxUser'
					},
					{
						xtype: 'numberfield',
						anchor: '100%',
						hidden: true,
						fieldLabel: 'idUser',
						name: 'idUser'
					}]
				},{
					xtype: 'panel',
					columnWidth: 0.5,
					margin: '0 5 0 0',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'textfield',
						labelAlign:'right',
						anchor: '100%',
						fieldLabel: 'Last Name',
						name: 'lastName',
						readOnly: true
					},
					{
						xtype: 'textfield',
						labelAlign:'right',
						fieldLabel: 'City',
						name: 'city',
						readOnly: true
					},
					{
						xtype: 'textfield',
						labelAlign:'right',
						fieldLabel: 'Zip',
						name: 'zip',
						readOnly: true
					},
					{
						xtype: 'textfield',
						labelAlign:'right',
						fieldLabel: 'Position',
						name: 'position',
						readOnly: true
					},
					{
						xtype: 'checkboxfield',
						hidden:true,
						labelAlign:'right',
						anchor: '100%',
						fieldLabel: 'Active',
						name: 'active',
						boxLabel: '',
						checked: true
					},
					{
						xtype: 'numberfield',
						enableKeyEvents : true,
						readOnly: true,
						hideTrigger:true,
						labelAlign:'right',
						fieldLabel: 'Commision (%)',
						name: 'commissionPercent',
						minValue : 0,
						maxValue : 100
					},
					{
						xtype: 'numberfield',
						hidden:true,
						labelAlign:'right',
						fieldLabel: 'Commision (%)',
						name: 'commissionPercentOriginal'
					}]
				}]
			},
			{
				xtype : 'adddealusergrid',
				region : 'center',
				bodyBorder : true,
				scrollable: true
			}
		];
		me.callParent(arguments);
	}
});