Ext.define('DM2.view.ClosingModGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.closingmodgridpanel',

    requires: [
        'Ext.grid.column.Date',
        'Ext.grid.column.Check'
    ],

    cls: 'gridcommoncls',
    frame: true,
    itemId: 'closingmodgridpanel',
    maxHeight: 200,
    ui: 'activitypanel',
    collapsible: true,
    title: 'Modifications',
    store: 'ApprovalDocs',

    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                var docurlname = record.get('url');
                var ext = /^.+\.([^.]+)$/.exec(docurlname);
                if(ext === null){
                    ext = "";
                }else {
                    ext = ext[1];
                }
                return '<a target="_blank" href="'+record.get('url')+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
            },
            dataIndex: 'name',
            text: 'Name',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            dataIndex: 'modifiedBy',
            text: 'User',
            flex: 1
        },
        {
            xtype: 'datecolumn',
            dataIndex: 'modifydate',
            text: 'Date',
            flex: 1
        },
        {
            xtype: 'checkcolumn',
            itemId: 'receivedmodback',
            dataIndex: 'receivedBack',
            text: 'ReceivedBack',
            flex: 1
        }
    ]

});