Ext.define('DM2.view.BankDocsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bankdocsgridpanel',

    requires: [
        'DM2.view.override.BankDocsGridPanel',
        'Ext.grid.column.Check',
        'Ext.grid.column.Action',
        'Ext.view.Table',
        'Ext.grid.plugin.DragDrop',
        'Ext.util.Point',
        'Ext.grid.plugin.CellEditing',
        'Ext.selection.CellModel',
        'Ext.panel.Panel'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.height = 200;
		me.itemId = 'bankdocsgridpanel';
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.scrollable = true;
		me.title = 'List Of Files';
		me.store = Ext.create('DM2.store.DocsPerSubmission');

    	me.columns = [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                var docurlname = record.get('url');
                var ext = DM2.app.getController('MainController').getExtImg(docurlname);
                return '<a target="_blank" href="'+docurlname+'"><img src="resources/images/icons/'+ext+'.png" />  '+value+'</a>';
            },
            dataIndex: 'name',
            text: 'Name',
            flex: 1
        },
        {
            xtype: 'checkcolumn',
            itemId: 'selectedclmn',
            width: 90,
            dataIndex: 'selected',
            text: 'Selected'
        },
        {
            xtype: 'actioncolumn',
            handler: function(view, rowIndex, colIndex, item, e, record, row) {
                this.fireEvent('itemClick', view, rowIndex, colIndex, item, e, record, row);
            },
            itemId: 'upcolumn',
            width: 65,
            align: 'center',
            dataIndex: 'bool',
            text: 'Up',
            tooltip: 'Up',
            icon: 'resources/images/up.png'
        },
        {
            xtype: 'actioncolumn',
            handler: function(view, rowIndex, colIndex, item, e, record, row) {
                this.fireEvent('itemClick', view, rowIndex, colIndex, item, e, record, row);
            },
            itemId: 'downcolumn',
            width: 70,
            align: 'center',
            dataIndex: 'bool',
            text: 'Down',
            tooltip: 'Down',
            icon: 'resources/images/down.png'
        }
    ];
    	me.viewConfig = {
        getRowClass: function(record, rowIndex, rowParams, store) {
            if(record && record.get('isglobal') ===  true){
                return 'global-doc-color-cls';
            }
        },
        plugins: [
            {
                ptype: 'gridviewdragdrop',
                ddGroup: 'dropsubmissiondoc',
                enableDrag: false
            }
        ]
    };
		me.plugins = [
			{
				ptype: 'cellediting'
			},
			Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
		];
		me.selModel = {
			selType: 'cellmodel'
		};
		me.callParent(arguments);
    }
});