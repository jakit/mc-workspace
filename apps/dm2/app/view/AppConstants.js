Ext.define('DM2.view.AppConstants', {
    extend: 'Ext.Base',

    singleton: true,
	
	//apiurl: 'http://devnyeapi01:8080/rest/default/hsjag/v1/'
	apiurl: '',
	ipaddress: '',
	projecturl: '',
	versionurl: '',
	group: '',
	environment: '',
	constructor: function(cfg) {
        var me = this;
		console.log("Initial Api Url Value : "+me.apiurl);
		var apiurlval = "";
		Ext.Ajax.request({
			 headers: {
				"Content-Type": "application/xml"
			 },
			 async : false, 
			 url: 'resources/liveapi.xml',
			 success: function (response) {
				var projecturlval="",versionurlval="",groupval="",environmentval="";
				var myxml = response.responseText;
				var xmlDoc = me.parseToXml(myxml);
				var itemcnt = xmlDoc.getElementsByTagName("item");
				if(itemcnt.length!=0){
					for(j=0;j<itemcnt.length;j++){
						apiurlval = itemcnt[j].getElementsByTagName("apiurl")[0].childNodes[0].nodeValue;
						if(itemcnt[j].getElementsByTagName("projecturl")[0]){
							projecturlval = itemcnt[j].getElementsByTagName("projecturl")[0].childNodes[0].nodeValue;
						}
						if(itemcnt[j].getElementsByTagName("versionurl")[0]){
							versionurlval = itemcnt[j].getElementsByTagName("versionurl")[0].childNodes[0].nodeValue;
						}
						if(itemcnt[j].getElementsByTagName("group")[0]){
							groupval = itemcnt[j].getElementsByTagName("group")[0].childNodes[0].nodeValue;
						}
						if(itemcnt[j].getElementsByTagName("environment")[0]){
							environmentval = itemcnt[j].getElementsByTagName("environment")[0].childNodes[0].nodeValue;
						}
					}
				}
				console.log("Apiurl Frm Xml : "+apiurlval);
				//DM2.view.AppConstants.apiurl = apiurlval;
				me.apiurl = apiurlval;
				me.projecturl = projecturlval;
				me.versionurl = versionurlval;
				me.group = groupval;
				if(environmentval!="" && environmentval!=null){
					me.environment = environmentval.toUpperCase();
				}
				console.log("New Api Url Value : "+me.apiurl);
			},
			failure: function (response) {
				console.log("Fail to load an XML.");
			}
		});
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	parseToXml: function(myxml){
		if (window.DOMParser){
			  var parser = new DOMParser();
			  xmlDoc = parser.parseFromString(myxml,"application/xml");
		} else { // Internet Explorer
			  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			  xmlDoc.async=false;
			  xmlDoc.loadXML(myxml); 
		}
		return xmlDoc;
	}
});