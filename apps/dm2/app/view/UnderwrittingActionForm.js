Ext.define('DM2.view.UnderwrittingActionForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.underwrittingactionform',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.field.Date',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Number'
    ],

    bodyPadding: 5,

    items: [
        {
            xtype: 'fieldset',
            collapsible: true,
            items: [
				{
					layout:'hbox',
					align:'stretch',
					margin:'0 0 5 0',
					items:[{
						xtype: 'textfield',
						anchor: '100%',
						flex:1,						
						fieldLabel: 'Executor Name',
						name: 'activityExecutorLastName'
					},
					{
						xtype:'button',
						margin:'0 0 0 10',
						iconCls: 'add',
	                    text: 'Add Contact',
						action:'addexecnamebtn'
					}]
                },
                {
                    xtype: 'datefield',
					format: 'm-d-Y',
                    anchor: '100%',                    
                    fieldLabel: 'Scheduled Date',
                    name: 'scheduledActivityDate'
                },
                {
                    xtype: 'datefield',
					format: 'm-d-Y',
                    anchor: '100%',                    
                    fieldLabel: 'Complete Date',
                    name: 'activityCompleteDate'
                },
                {
                    xtype: 'checkboxfield',
                    anchor: '100%',
                    fieldLabel: 'Completed',
                    name: 'activityComplete'
                },
				{
                    xtype: 'numberfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'idContact',
                    name: 'idContact'
                },
                {
                    xtype: 'numberfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'idUnderwriting',
                    name: 'idUnderwriting'
                },
                {
                    xtype: 'numberfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'idUnderwritingActions',
                    name: 'idUnderwritingActions'
                }
            ]
        }
    ]

});