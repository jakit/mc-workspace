Ext.define('DM2.view.DealDetailResearchPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.dealdetailresearchpanel',

    requires: [
        'Ext.panel.Panel',
        'Ext.button.Button'
    ],

	layout: 'border',
    title: 'Research',

    items: [
        {
            xtype: 'panel',
            region: 'center'
        },
        {
            xtype: 'panel',
            region: 'west',
            split: true,
            width: 150,
            collapsible: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'button',
                    itemId: 'gmapbtn',
                    margin: '10 5 5 5',
                    text: 'Gmap'
                },
                {
                    xtype: 'button',
                    itemId: 'zillowbtn',
                    margin: '5 5 10 5',
                    text: 'Zillow'
                }
            ]
        }
    ]

});