Ext.define('DM2.view.CommissionScheduleMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.commissionschedulemenu',
	requires: [
        'Ext.menu.Item'
    ],

    width: 200,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'editcommissionschedule',
			action:'editcommissionschedule',
            text: 'Edit Commission Schedule',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'deletecommissionschedule',
			action:'deletecommissionschedule',
            text: 'Delete Commission Schedule',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
            itemId: 'deleteallcommissionschedule',
			action: 'deleteallcommissionschedule',
            text: 'Delete All Commission Schedule',
            focusable: true,
			modifyMenu : 'yes'
        }
    ]
});