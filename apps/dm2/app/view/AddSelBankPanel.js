Ext.define('DM2.view.AddSelBankPanel', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    alias: 'widget.addselbankpanel',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.filters.filter.Number',
        'Ext.grid.filters.filter.String',
        'Ext.view.Table',
        'Ext.grid.filters.Filters',
        'Ext.tab.Panel',
        'Ext.tab.Tab',
        'Ext.tab.Bar',
		'DM2.view.component.grid.SelectionGridNew'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
		me.closable = true;
		me.title = 'Select Bank';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action:'savebankbtn',
					margin: '0 10 0 0',
					text: 'Save'
				}
			]
		};
		me.itemId = 'addselbankpanel';
		var bankallbuffSt = Ext.create('DM2.store.Bankallbuff');
		me.scrollable = true;
		//me.itemId = 'bcallbankgridpanel';
		me.store = bankallbuffSt;
		me.columns = [
			{
				xtype: 'gridcolumn',
				hidden: true,
				dataIndex: 'idBank',
				text: 'Bank Id',
				headerField: me.setFilterField("stringfield", {
					grid: me
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'fullName',
				text: 'Full Name',
				headerField: me.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'shortName',
				text: 'Short Name',
				headerField: me.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'address',
				text: 'Address',
				headerField: me.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				flex: .3,
				dataIndex: 'office',
				text: 'Office',
				headerField: me.setFilterField("genericlistfield", {
					grid:me,
					inlineData: [{
						'display': "CA",
						'val': "CA"
					}, {
						'display': "SD",
						'val': "SD"
					}, {
						'display': "FL",
						'val': "FL"
					}, {
						'display': "IL",
						'val': "IL"
					}, {
						'display': "NY",
						'val': "NY"
					}, {
						'display': "NJ",
						'val': "NJ"
					}, {
						'display': "MD",
						'val': "MD"
					}]
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				flex: .3,
				dataIndex: 'state',
				text: 'State',
				headerField: me.setFilterField("combocheckboxfield", {
					grid:me,
					inlineData: Ext.getStore('StatesList').getData(),
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'city',
				text: 'City',
				headerField: me.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				hidden: true,
				dataIndex: 'zip',
				text: 'Zip',
				headerField: me.setFilterField("stringfield", {
					grid: me,
					filterMode: "Contains",
					filterDataType: "string"
				})
			}
		];
		me.callParent(arguments);
    }
});