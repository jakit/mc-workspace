Ext.define('DM2.view.CreateNewDealContactGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.createnewdealcontactgridmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
			action:'addcontact',
            text: 'Add Contact',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        }];
        me.callParent(arguments);
    }
});