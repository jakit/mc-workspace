Ext.define('DM2.view.SelectBidPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.selectbidpanel',

    requires: [
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.view.Table',
		'DM2.store.SaleBids'
    ],
	
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.frame  = true;
		me.margin = 3;
		me.ui     = 'activitypanel';
		me.closable = true;
		me.title  = 'Activities > ';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'doneActionBtn',
					text: 'Done'
				}
			]
		};
		me.store = Ext.create('DM2.store.SaleBids');
		
		me.scrollable = true;
		me.cls = 'gridcommoncls';
		me.columnLines = true;
		me.multiColumnSort = true;
		me.sortableColumns = false;
		me.columns = [{
			xtype: 'gridcolumn',
			hidden:true,
			dataIndex: 'idSale',
			text: 'idSale',
			flex: 1
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'Buyer',
			text: 'Bidder',
			flex: 1
		},
		{
			xtype: 'datecolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				return Ext.util.Format.date(record.get('targetCloseDate'), "m/d/Y");
			},
			flex: 1,
			format:'m/d/Y',
			//width: 150,
			dataIndex: 'targetCloseDate',
			text: 'Target Close Date'
		},
		{
			xtype: 'gridcolumn',
			dataIndex: 'salePrice',
			text: 'Price',
			flex: 1,
			format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				//var formatVal = Ext.util.Format.number(value,'0,000');
				/*var readable = record.get('readable');
				if(readable==0){
					return null;
				}else{*/
					if(value==null || value==0){
						return null;  
					}else{
						var formatVal = Ext.util.Format.currency(value, '$',0);
						metaData.tdAttr = 'data-qtip="' + formatVal + '"';
						return formatVal;  
					}
				/*}*/
			}
		},
		{
			xtype: 'gridcolumn',
			//hidden:true,
			dataIndex: 'companyName',
			text: 'Company Name',
			flex: 1
		},
		{
			xtype: 'checkcolumn',
			dataIndex: 'selected',
			//processEvent: Ext.emptyFn,
			text: 'Selected',
			flex: 1
		}];
		/*me.plugins  = [
			{
				ptype: 'cellediting',
				clicksToEdit: 1
			}
		];*/
		me.callParent(arguments);
    }
});