Ext.define('DM2.view.BankContactsGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.bankcontactsgridpanel',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

    cls: 'gridcommoncls',
    itemId: 'bankcontactsgridpanel',
    title: 'Contacts',
    store: 'BankContacts',

    columns: [
        {
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'contactid',
            text: 'Contact Id'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'name',
            text: 'Name',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'company',
            text: 'Company',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'type',
            text: 'Type',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'phone',
            text: 'Phone',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'email',
            text: 'Email',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'button',
                    itemId: 'addbankcontactbtn',
                    iconCls: 'add',
                    text: 'Add'
                }
            ]
        }
    ]

});