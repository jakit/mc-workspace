Ext.define('DM2.view.AddGenSubmDocPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addgensubmdocpanel',

    requires: [
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

    scrollable: true,
    bodyPadding: 10,
    closable: true,
    title: 'Generate Document',

    items: [
        {
            xtype: 'textfield',
            anchor: '100%',
            fieldLabel: 'Name',
            name: 'name',
            allowBlank: false
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            defaults: {
                minWidth: 75
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
                    formBind: true,
                    disabled: true,
                    itemId: 'gendocbtn',
                    text: 'Generate'
                }
            ]
        }
    ]

});