Ext.define('DM2.view.AddDealUserGrid', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    xtype: 'adddealusergrid',
    requires: ['DM2.view.component.grid.SelectionGridNew'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
        me.itemId = 'csrgridpanel';
        me.store = Ext.create('DM2.store.CSR');

		me.columns = [{
			xtype : 'gridcolumn',
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'firstName',
			text : 'First Name',
			flex : 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}, {
			xtype : 'gridcolumn',
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'lastName',
			text : 'Last Name',
			flex : 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}, {
			xtype : 'gridcolumn',
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'userName',
			text : 'User Name',
			flex : 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}, {
			xtype : 'gridcolumn',
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'position',
			text : 'Position',
			flex : 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}, {
			xtype : 'gridcolumn',
			hidden:true,
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'state',
			text : 'State',
			flex : .35,
			headerField: me.setFilterField("genericlistfield", {
				grid:me,
				inlineData: Ext.getStore('StatesList').getData()
			})
		},  {
			xtype : 'gridcolumn',
			hidden:true,
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'address',
			text : 'Address',
			flex : 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},{
			xtype : 'gridcolumn',
			hidden:true,
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'city',
			text : 'City',
			flex : 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype : 'gridcolumn',
			renderer : function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex : 'office',
			text : 'Office',
			flex : 1,
			headerField: me.setFilterField("genericlistfield", {
				grid:me,
				inlineData: [{
					'display': "CA",
					'val': "CA"
				}, {
					'display': "SD",
					'val': "SD"
				}, {
					'display': "FL",
					'val': "FL"
				}, {
					'display': "IL",
					'val': "IL"
				}, {
					'display': "NY",
					'val': "NY"
				}, {
					'display': "NJ",
					'val': "NJ"
				}, {
					'display': "MD",
					'val': "MD"
				}]
			})
		}];       
        me.callParent(arguments);
    }
});