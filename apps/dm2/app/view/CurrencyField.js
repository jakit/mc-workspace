/**
 * ExtJS CurrencyField
 * Updated version of http://develtech.wordpress.com/2011/03/06/number-field-with-currency-symbol-thousand-separator-with-international-support/#respond
 * 
 * Settings configurable via default Ext.util.Format properties:
 * Ext.util.Format.currencyAtEnd      = false;
 * Ext.util.Format.currencyPrecision  = 2;
 * Ext.util.Format.currencySign       = '$ ';
 * Ext.util.Format.thousandSeparator  = ',';
 * Ext.util.Format.decimalSeparator   = '.';
 */

Ext.define('DM2.view.CurrencyField', {
  extend: 'Ext.form.field.Text',
  alias: ['widget.currencyfield','widget.currency'],
  maskRe: /[0-9 .,-]/,
  fieldStyle: 'text-align: right;',
  constructor: function(cfg) {
	var me = this;
	Ext.apply(me, cfg);
	me.callParent(arguments);
  },
  initComponent: function() {
	var me = this;
	Ext.util.Format.currencyPrecision  = 0;
	me.callParent(arguments);
  },
  onFocus: function(){
	  //console.log(v);
	   if (Ext.isEmpty(this.getRawValue()))  {
		   //console.log("Don't set value");
	   }else{
  			this.setRawValue(this.removeFormat(this.getRawValue()));
	   }
  },
  
  onBlur: function(){
	  if (Ext.isEmpty(this.getRawValue()))  {
		   //console.log("Don't set value");
	   }else{
			if (this.hasFormat()) {
			  this.setRawValue(Ext.util.Format.currency(this.getRawValue()));
			}
	   }
  },
  
  setValue: function(v){
	   //console.log(v);
	   if (Ext.isEmpty(v))  {
		   //console.log("Don't set value");
	   }else{
		    this.setRawValue(Ext.util.Format.currency(v));
	   }
  },
  
  removeFormat: function(v){
    if (Ext.isEmpty(v) || !this.hasFormat())  {
      return v;
    } else {
      v = v.replace(Ext.util.Format.currencySign, '');
      v = !Ext.isEmpty(Ext.util.Format.thousandSeparator) ? v.replace(new RegExp('[' + Ext.util.Format.thousandSeparator + ']', 'g'), '') : v;
      v = Ext.isEmpty(Ext.util.Format.decimalSeparator) ? v.replace(new RegExp('[' + Ext.util.Format.decimalSeparator + ']', 'g'), '') : v;
      return v;
    }
  },
  
  hasFormat: function(){
    return Ext.util.Format.thousandSeparator != '.' || !Ext.isEmpty(Ext.util.Format.currencySign) || Ext.util.Format.currencyPrecision > 0;  
  },
  
  processRawValue: function(val) {
    return this.removeFormat(val);
  }
});