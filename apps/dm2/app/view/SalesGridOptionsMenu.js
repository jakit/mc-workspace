Ext.define('DM2.view.SalesGridOptionsMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.salesgridoptionsmenu',

    requires: [
        'Ext.menu.Item'
    ],

    items: [
        {
            xtype: 'menuitem',
            action: 'opensale',
            text: 'Open',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            action: 'printsale',
            text: 'Print',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            action: 'createnewsale',
            text: 'Create New Sale',
            focusable: true,
			modifyMenu : 'yes'
        },
		{
            xtype: 'menuitem',
            action: 'refreshsales',
            text: 'Refresh Sales',
            focusable: true,
			modifyMenu : 'no'
        }
    ]
});