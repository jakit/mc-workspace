Ext.define('DM2.view.ActivityDetailPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.activitydetailpanel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.form.field.Display',
        'Ext.panel.Panel',
        'Ext.form.field.ComboBox',
        'Ext.button.Button',
        'Ext.form.Label',
        'Ext.form.RadioGroup',
        'Ext.form.field.Radio',
        'Ext.form.field.Number',
		'DM2.store.MainContactPhones'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        
        var me = this;
    	me.frame = true;
	    me.itemId = 'activitydetailpanel';
	    me.margin = 3;
    	me.ui = 'activitypanel';
	    me.bodyPadding = 5;
	    me.closable = true;
	    me.title =  'Activities > Call The Main Contact';
		me.closeAction = 'destroy';
	    me. layout =  {
			type: 'vbox',
			align: 'stretch'
		};
		
		var store = Ext.create('DM2.store.MainContactPhones');
		var prenotesstore = Ext.create('DM2.store.Prenotes');
		
	    me.items = [
        {
            xtype: 'fieldset',
            collapsible: true,
            items: [
                {
                    xtype: 'displayfield',
                    anchor: '100%',
                    cls: 'maincontactnamecls',
                    fieldLabel: 'Name',
                    hideLabel: true,
                    labelWidth: 60,
                    name: 'fullName'
                },
                {
                    xtype: 'panel',
                    margin: '3 0 3 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'panel',
                            flex: 1,
                            bodyPadding: '0 5 0 0',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'Company',
                                    hideLabel: true,
                                    labelWidth: 60,
                                    name: 'companyName'
                                },
                                {
                                    xtype: 'combobox',
                                    itemId: 'cntphone',
                                    margin: '15 0 0 0',
                                    fieldLabel: 'Label',
                                    hideLabel: true,
                                    name: 'cntphone',
                                    displayField: 'tag',
                                    queryMode: 'local',
                                    store: store,
                                    valueField: 'phoneNumber'
                                },
                                {
                                    xtype: 'button',
									hrefTarget : '_self',
                                    itemId: 'callbtn',
                                    margin: '5 0 5 0',
                                    width: '100%',
                                    iconCls: 'call'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            flex: 1,
                            style: 'border-left:1px solid #cecece;',
                            bodyPadding: '0 0 0 5',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'Position',
                                    hideLabel: true,
                                    labelWidth: 60,
                                    name: 'position'
                                },
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'Address',
                                    hideLabel: true,
                                    labelWidth: 60,
                                    name: 'address'
                                },
                                {
                                    xtype: 'displayfield',
                                    hideLabel: true,
                                    labelWidth: 60,
                                    name: 'citystatezip'
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'label',
            text: 'Enter Custom Note or Select Note'
        },
        {
            xtype: 'combobox',
            margin: '5 0 10 0',
            hideLabel: true,
            name: 'note',
            displayField: 'description',
            queryMode: 'local',
            store: prenotesstore,
            valueField: 'idstandardNotes'
        },
        {
            xtype: 'fieldset',
            margin: '0 0 10 0',
            padding: 10,
            title: 'Action',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'radiogroup',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'radiofield',
                            flex: 1,
                            //id: 'addreminder',
                            name: 'action',
                            boxLabel: 'Add reminder to call [n] days',
                            inputValue: 'addreminder'
                        },
                        {
                            xtype: 'radiofield',
                            flex: 1,
                            //id: 'closedealreason',
                            margin: '0 5 0 5',
                            name: 'action',
                            boxLabel: 'Close deal reason',
                            inputValue: 'closedealreason'
                        },
                        {
                            xtype: 'radiofield',
                            flex: 1,
                            //id: 'donothing',
                            name: 'action',
                            boxLabel: 'Do nothing',
                            inputValue: 'donothing'
                        }
                    ]
                },
                {
                    xtype: 'numberfield',
                    hidden: true,
                    fieldLabel: 'Days',
                    labelWidth: 60,
                    name: 'days'
                }
            ]
        },
        {
            xtype: 'panel',
            margin: '0 0 10 0',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
                    action: 'activitydonebtn',
                    itemId: 'activitydonebtn',
                    margin: '0 0 0 10',
                    text: 'Done'
                },
                {
                    xtype: 'numberfield',
                    flex: 1,
                    hidden: true,
                    itemId: 'activityid',
                    name: 'activityid'
                }
            ]
        }
    ];
		me.callParent(arguments);
    }
});