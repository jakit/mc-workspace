Ext.define('DM2.view.AddSaleDealBid', {
    extend: 'Ext.form.Panel',
    alias: 'widget.addsaledealbid',

    requires: [
        'Ext.panel.Panel',
        'Ext.form.field.ComboBox',
        'Ext.button.Button'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.frame = true;
		me.itemId = 'addsaledealbid';
		me.margin = 3;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;
		me.title = 'Add Bid';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					hidden:true,
					action: 'doneActionBtn',
					text: 'Done'
				}
			]
		};
    	me.items = [
			{
				xtype: 'textfield',
				hidden:true,
				anchor: '100%',
				fieldLabel: 'idBid',
				name: 'idBid'
			},
			{
				xtype: 'textfield',
				hidden:true,
				anchor: '100%',
				fieldLabel: 'idSale',
				name: 'idSale'
			},
			{
				xtype: 'textfield',
				hidden:true,
				anchor: '100%',
				fieldLabel: 'idBuyerName',
				name: 'idBuyerName'
			},
			{
				xtype: 'textfield',
				hidden:true,
				anchor: '100%',
				fieldLabel: 'contactType',
				name: 'contactType'
			},
			{
				layout:{
					type:'hbox',
					align:'stretch'
				},
				margin:'0 0 5 0',
				items:[{
					xtype: 'textfield',
					readOnly:true,
					flex:1,
					anchor: '100%',                    
					fieldLabel: 'Contact Name',
					name: 'fullName'
				},
				{
					xtype: 'button',
					iconCls:'dots-three-horizontal',
					margin:'0 0 0 5',
					action:'contactpickbtn'
				}]                 
			},
			{
				xtype: 'currencyfield',
				anchor: '100%',									
				fieldLabel: 'Price',
				name: 'price'
			},
			{
				xtype: 'datefield',
				anchor: '100%',
				fieldLabel: 'Good Until Date',
				name: 'targetCloseDate'
			},
			{
				xtype: 'panel',
				margin: '10 0 0 0',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
						xtype:'component',
						flex:1
					},
					{
						xtype: 'button',
						action: 'savesalebidbtn',
						margin: '0 0 0 10',
						width: 60,
						text: 'Save'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});