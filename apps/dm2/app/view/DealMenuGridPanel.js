Ext.define('DM2.view.DealMenuGridPanel', {
    extend: 'DM2.view.component.grid.DashboardNew',
    alias: 'widget.dealmenugridpanel',
    requires: [ 'Ext.grid.plugin.RowExpander' ],
    mixins: ['Filters2.InjectHeaderFields', 'Filters2.FiltersMixin'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
        me.store = 'Deals';
        me.viewConfig = {
            itemId: 'dealmenugridview',
            forceFit: true
        };
        me.plugins = [{
            ptype: 'rowexpander',
            expandOnDblClick: false,
            rowBodyTpl: [
                '<div id="propertygridrow-{dealid}"></div>'
            ]
        }];
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true,
                    tabIndex: 1
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .4,
            sortable: true,
            dataIndex: 'dealid',
            menuDisabled: true,
            text: 'DealNo.'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                if(value=='S'){
                    return '<img src="resources/images/simple.png" />';
                }
                else if(value=='P'){
                    return '<img src="resources/images/package.png" />';
                }
                else if(value=='B'){
                    return '<img src="resources/images/blanket.png" />';
                }
                else if(value=='C'){
                    return '<img src="resources/images/complex.png" />';
                }
                else
                {
                    return value;
                }
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'dealtype',
            menuDisabled: true,
            text: 'Deal Type'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true,
                    tabIndex: 2
                }
            ],
            cls: 'dealgridcolumncls',
            flex: 1.5,
            dataIndex: 'name',
            menuDisabled: true,
            text: 'Name'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'street_no',
            menuDisabled: true,
            text: 'Street No.'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            hidden: true,
            dataIndex: 'street',
            menuDisabled: true,
            text: 'Street'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true,
                    tabIndex: 3
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'city',
            menuDisabled: true,
            text: 'City'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true,
                    tabIndex: 4
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'state',
            menuDisabled: true,
            text: 'State'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true,
                    tabIndex: 5
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'maincontact',
            menuDisabled: true,
            text: 'Contact'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .3,
            dataIndex: 'status',
            menuDisabled: true,
            text: 'Status'
        }, {
            xtype: 'datecolumn',
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'startdate',
            menuDisabled: true,
            text: 'DealDate',
            format: 'm-d-Y'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'bank',
            menuDisabled: true,
            text: 'Bank'
        }, {
            xtype: 'gridcolumn',
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'loanamt',
            menuDisabled: true,
            text: 'Loan Amount',
            format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                 var formatVal = Ext.util.Format.number(val,'0,000');
				var formatVal = Ext.util.Format.currency(formatVal, '$');
				//console.log("Rend Value"+formatVal);
				meta.tdAttr = 'data-qtip="' + formatVal + '"';
				return formatVal;  
            }
        }, {
            xtype: 'datecolumn',
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            dataIndex: 'loandate',
            menuDisabled: true,
            flex: .5,
            text: 'LoanDate',
            format: 'm-d-Y'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            items: [
                {
                    xtype: 'searchtrigger',
                    autoSearch: true
                }
            ],
            cls: 'dealgridcolumncls',
            dataIndex: 'broker',
            menuDisabled: true,
            text: 'Broker',
            flex: .5
        }];
        
        me.callParent(arguments);  
   }
});