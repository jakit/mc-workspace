Ext.define('DM2.view.ContactTabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.contacttabpanel',

    requires: [
        'DM2.view.contact.ContactDocsGridPanel',
        'DM2.view.contact.ContactNotesGridPanel',
		'DM2.view.contact.ContactDealHistoryGrid',
        'Ext.grid.Panel',
        'Ext.tab.Tab',
        'Ext.tab.Bar'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
    	//height: 250,
    	//width: 400,
    	me.activeTab = 0;
		//me.ui = 'submenutabpanel';
    	me.items = [
			{
				xtype: 'contactdocsgridpanel',
				iconCls: 'docbtn'
			},
			{
				xtype: 'contactnotesgridpanel',
				iconCls: 'notebtn'
			}
		];
		me.callParent(arguments);        
    },
	tabBar: {
		xtype: 'tabbar',
		ui: 'submenutabpanel'
	}
});