Ext.define('DM2.view.CreateNewSalesDealFromPropertyPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.createnewsalesdealfrompropertypanel',

    requires: [
		'DM2.view.CreateNewDealFromPropertyGrid',
        'DM2.view.CreateNewDealFromPropertyDocsGridPanel',
        'DM2.view.CreateNewDealUserGridPanel',
		'DM2.view.CreateNewDealFromPropertyContactGrid',
        'Ext.form.field.ComboBox',
        'Ext.grid.Panel',
        'Ext.view.Table',
        'Ext.selection.CheckboxModel',
        'Ext.grid.column.Check',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
		'DM2.store.DealStatus'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable : true,
    initComponent: function() {
		var me = this;
    	me.frame = true;
	    me.margin = 3;
	    //me.scrollable = true;
	    me.ui = 'activitypanel';
	    me.bodyPadding = 5;
	    me.closable = true;
		
		var officest = Ext.create('DM2.store.Office');
		var dealStatusSt = Ext.create('DM2.store.SaleDealStatus');
		
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
	    me.items = [
        {
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textfield',
					hidden:true,
                    flex: 1,
                    itemId: 'filenumber',
                    fieldLabel: 'File #',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'filenumber'
                },
				{
                    xtype: 'textfield',
                    flex: 1,
                    itemId: 'name',
                    fieldLabel: 'Name',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'name'
                }
            ]
        },
        {
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'combobox',
                    flex: 1,
					queryMode:'local',
                    itemId: 'status',
                    fieldLabel: 'Status',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'status',
                    displayField: 'statusFullName',
                    store: dealStatusSt,
                    valueField: 'status'
                },
                {
                    xtype: 'combobox',
                    flex: 1,
					queryMode:'local',
                    itemId: 'office',
                    fieldLabel: 'Office',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'office',
                    displayField: 'code',
                    store: officest,
                    valueField: 'code'
                }
            ]
        },
		{
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'combobox',
                    flex: 1,
                    itemId: 'saleType',
                    fieldLabel: 'SaleType',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'saleType',
					queryMode: 'local',
					//displayField: 'saleName',
					//valueField: 'saleValue',
					store : [['S','Sell'],['B','Buy'],['C','Consulting'],['O','OffMarket']],
					listeners: {
						afterrender: function(combo) {
							var recordSelected = combo.getStore().getAt(0);                     
							combo.setValue(recordSelected.get('field1'));
						}
					}
                },
                {
                    xtype: 'currencyfield',
                    flex: 1,
                    itemId: 'price',
                    fieldLabel: 'Price',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'price'
                }
            ]
        },
		{
            xtype: 'panel',
            margin: '0px 0px 6px 0px',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
				{
					xtype: 'combobox',
					flex: 1,
					fieldLabel: 'Commission Type',
					labelAlign: 'right',
					labelWidth: 50,
					name: 'commissionType',
					queryMode: 'local',
					editable : false,
					//displayField: 'saleName',
					//valueField: 'saleValue',
					store : [['P','Percentage'],['F','Flat'],['S','Structure']],
					listeners: {
						afterrender: function(combo) {
							var recordSelected = combo.getStore().getAt(0);                     
							combo.setValue(recordSelected.get('field1'));
						},
						change: function(cmb , newValue , oldValue , eOpts){
							var percentCommission = cmb.up('createnewsalesdealfrompropertypanel').down('numberfield[name="percentCommission"]');
							var flatAmtCommission = cmb.up('createnewsalesdealfrompropertypanel').down('currencyfield[name="flatAmountCommission"]');
							if(newValue=="P"){
								percentCommission.show();
								flatAmtCommission.hide();
							} else if(newValue=="F"){
								percentCommission.hide();
								flatAmtCommission.show();
							} else if(newValue=="S"){
								percentCommission.hide();
								flatAmtCommission.hide();
							}
						}
					}
				},
                {
                    xtype: 'numberfield',
					hideTrigger:true,
                    flex: 1,
                    fieldLabel: 'Commission (%)',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'percentCommission'
                },
                {
                    xtype: 'currencyfield',
					hidden:true,
                    flex: 1,
                    fieldLabel: 'Commission Amt',
                    labelAlign: 'right',
                    labelWidth: 50,
                    name: 'flatAmountCommission'
                }
            ]
        }
    ];		
		
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'createnewdealbtn',
					itemId: 'createnewdealbtn',
					text: 'Create'
				}
			]
		};
		me.callParent(arguments);
    }
});