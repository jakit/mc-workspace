Ext.define('DM2.view.NotePanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.notepanel',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.form.field.Number'
    ],

    border: false,
    frame: true,
    margin: 3,
    ui: 'activitypanel',
    bodyPadding: 10,
    closable: true,

    items: [
        {
            xtype: 'textfield',
            anchor: '100%',
            fieldLabel: 'Content',
            name: 'content',
            allowBlank: false
        },
        {
            xtype: 'numberfield',
            anchor: '100%',
            hidden: true,
            fieldLabel: 'Label',
            name: 'noteid'
        },
		{
            xtype: 'panel',
            margin:'15 0 0 0',
            defaults: {
                minWidth: 75
            },
			layout:{
				type:'hbox',
				align:'stretch'
			},
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
					margin:'0 10 0 0',
                    formBind: true,
                    disabled: true,
                    itemId: 'notesubmitbtn',
                    text: 'Submit'
                },
                {
                    xtype: 'button',
                    itemId: 'noteresetbtn',
                    text: 'Reset'
                }
            ]
        }
    ]/*,
    dockedItems: [
        {
            xtype: 'toolbar',
            //dock: 'bottom',
            ui: 'footer',
            defaults: {
                minWidth: 75
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
                    formBind: true,
                    disabled: true,
                    itemId: 'notesubmitbtn',
                    text: 'Submit'
                },
                {
                    xtype: 'button',
                    itemId: 'noteresetbtn',
                    text: 'Reset'
                }
            ]
        }
    ]*/

});