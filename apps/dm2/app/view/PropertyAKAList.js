Ext.define('DM2.view.PropertyAKAList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.propertyakalist',

    requires: [
        'Ext.grid.column.Column',
        'Ext.view.Table'
    ],

    store: null,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'gridcommoncls';
        me.title = 'AKA';
        me.store = Ext.create('DM2.store.AKAList');
        me.columnLines = false;
		me.hideHeaders = true;
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'text',
            text: 'AKA',
            flex: 1
        }];        
        me.callParent(arguments);
    }    
});