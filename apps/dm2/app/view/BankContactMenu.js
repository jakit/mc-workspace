Ext.define('DM2.view.BankContactMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.bankcontactmenu',

    requires: [
        'Ext.menu.Item'
    ],

    width: 160,

    items: [
        {
            xtype: 'menuitem',
            text: 'Add Contact',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Remove Contact',
            focusable: true
        },
		{
            xtype: 'menuitem',
            itemId: 'callcontact',
            text: 'Call Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 250
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'emailcontact',
            text: 'Email Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'faxcontact',
            text: 'Fax Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 300
            }
        }
    ]

});