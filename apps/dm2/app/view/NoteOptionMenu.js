Ext.define('DM2.view.NoteOptionMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.noteoptionmenu',

    requires: [
        'Ext.menu.Item'
    ],

    width: 120,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'noteedit',
            text: 'Edit Note',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'noteadd',
            text: 'Add Note',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
			hidden: true,
            itemId: 'notehistory',
            text: 'Note History',
            focusable: true,
			modifyMenu : 'no'
        }
    ]

});