Ext.define('DM2.view.PropertyZillowWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.propertyzillowwindow',

    requires: [
        'Ext.container.Container',
        'Ext.view.View',
        'Ext.XTemplate'
    ],

    height: 500,
    width: 750,
    layout: 'fit',
    title: 'Property Zillow Location',

    items: [
        {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'container',
                    itemId: 'zillowContiner',
                    width: 500
                },
                {
                    xtype: 'dataview',
                    flex: 1,
                    itemSelector: 'div#locationitem',
                    itemTpl: [
                        '<div id="locationitem" class="itemHolder" style="padding: 10px;border-top: 1px solid black;">',
                        '    <div class="info">{street},{city},{state},{zipcode}</div>',
                        '        <div class="details">',
                        '           <a class="homelink" target="_blank" href="{homedetailslink}">HomeDetails</a>',
                        '         | <a class="maplink" target="_blank" href="{mapthishomelink}">Map It</a>',
                        '         | <a class="comparelink"target="_blank" href="{comparableslink}">Compare</a>',
                        '     </div>',
                        '</div>'
                    ],
                    store: 'SearchLocations'
                }
            ]
        }
    ]

});