Ext.define('DM2.view.DealModifyView', {
    extend: 'Ext.form.Panel',
    alias: 'widget.dealmodifyview',

    requires: [
        'Ext.panel.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Date',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent : function() {
        var me = this;
		me.frame = true;
		me.itemId = 'dealmodifyview';
		me.margin = 3;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;
		me.header = {
			titlePosition: 0,
			items: [{
				xtype: 'button',
				disabled: true,
				itemId: 'savebtn',
				margin: '0 10 0 0',
				minWidth: 65,
				text: 'Save'
			},
			{
				xtype: 'button',
				disabled: true,
				itemId: 'resetbtn',
				minWidth: 65,
				text: 'Restore'
			}]
		};
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		//var OfficeSt = Ext.create('DM2.store.Office');
		me.items = [
			{
				xtype: 'panel',
				margin: '0px 0px 6px 0px',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'textfield',
						flex: 1.1,						
						fieldLabel: 'Deal #',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'dealid',
						readOnly: true
					},
					{
						xtype: 'textfield',
						flex: 0.9,						
						fieldLabel: 'File #',
						labelAlign: 'right',
						labelWidth: 40,
						name: 'filenumber',
						readOnly: true
					}
				]
			},
			{
				xtype: 'textfield',
				fieldLabel: 'Name',
				labelAlign: 'right',
				labelWidth: 50,
				name: 'dealname'
			},
			{
				xtype: 'panel',
				margin: '0 0 5 0',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'combobox',
						flex: 1,
						queryMode:'local',
						fieldLabel: 'Status',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'status',
						displayField: 'statusFullName',
						store: 'DealStatus',
						valueField: 'status'
					},
					{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Set By',
						labelAlign: 'right',
						labelWidth: 55,
						name: 'statusSetBy',
						readOnly: true
					},
					{
						xtype: 'datefield',
						flex: 1,						
						fieldLabel: 'As-of',
						labelAlign: 'right',
						labelWidth: 55,
						name: 'statusDate',
						readOnly: true
					}
				]
			},
			{
				xtype: 'panel',
				margin: '0 0 5 0',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Amount',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'amount',
						readOnly: true
					},
					{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Prm. Broker',
						labelAlign: 'right',
						labelWidth: 75,
						name: 'primarybroker',
						readOnly: true
					}
				]
			},
			{
				xtype: 'panel',
				margin: '0 0 5 0',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'combobox',
						flex: 1,
						queryMode:'local',						
						itemId: 'office',
						fieldLabel: 'Office',
						labelAlign: 'right',
						labelWidth: 50,
						name: 'office',
						displayField: 'code',
						store: 'Office', //OfficeSt,
						valueField: 'code'/*,                
						readOnly: true*/
					},
					{
						xtype: 'textfield',
						flex: 1,						
						fieldLabel: 'Mortgage Type',
						labelAlign: 'right',
						labelWidth: 75,
						name: 'mtgtype',
						readOnly: true
					}
				]
			},
			{
				xtype: 'panel',
				layout:{
					type:'vbox',
					align:'stretch'
				},
				padding:'10 10 5 10',
				margin: '0 0 5 0',
				frame : true,
				ui: 'activitypanel',
				collapsible: true,
				title: 'Closing',
				items:[{
					xtype: 'datefield',
					anchor: '100%',
					fieldLabel: 'Scheduled Closing',
					name: 'scheduledCloseDate'
				},{
					xtype: 'datefield',
					anchor: '100%',
					fieldLabel: 'Closing Date',
					name: 'closeDate'
				},{
					xtype: 'datefield',
					anchor: '100%',
					fieldLabel: 'Bill Date',
					name: 'billDate'
				},{
					xtype: 'datefield',
					anchor: '100%',
					fieldLabel: 'Maturity Date',
					name: 'endDate'
				}/*,{
					xtype: 'panel',
					margin: '0 0 5 0',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
						{
							xtype: 'textfield',
							flex: 1.2,										
							fieldLabel: 'PPP Type',
							name: 'ppptype',
							readOnly: true
						},
						{
							xtype: 'textfield',
							flex: 0.8,										
							fieldLabel: 'Phone',
							labelWidth: 50,
							labelAlign: 'right',
							name: 'clientsattorneyphone',
							readOnly: true
						}
					]
				}*/]
			}
		];
		/*me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'bottom',
				ui: 'footer',
				items: [
					{
						xtype: 'component',
						flex: 1
					},
					{
						xtype: 'button',
						disabled: true,
						itemId: 'savebtn',
						margin: '0 10 0 0',
						minWidth: 65,
						text: 'Save'
					},
					{
						xtype: 'button',
						disabled: true,
						itemId: 'resetbtn',
						minWidth: 65,
						text: 'Restore'
					}
				]
			}
		];*/
		me.callParent(arguments);
    }
});