Ext.define('DM2.view.ChangeRoleContactForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.changerolecontactform',

    requires: [
        'Ext.panel.Panel',
        'Ext.form.field.ComboBox',
        'Ext.button.Button'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.frame = true;
		me.itemId = 'changerolecontactform';
		me.margin = 3;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;
		me.title = 'Change Role';
		
		var cntTypeSt = Ext.create('DM2.store.ContactTypes');/*'ContactTypes';*/
		
    	me.items = [
			{
				xtype: 'textfield',
				anchor: '100%',
				fieldLabel: 'Contact Name',
				name: 'fullName'
			},
			{
				xtype: 'panel',
				margin: '10 0 10 0',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [
					{
						xtype: 'combobox',
						flex: 1,
						fieldLabel: 'Role',
						labelWidth: 40,
						name: 'contactType',
						displayField: 'type',
						store: cntTypeSt,//'ContactTypes',
						valueField: 'type'
					},
					{
						xtype: 'button',
						itemId: 'savechangerolecontactbtn',
						margin: '0 0 0 10',
						width: 60,
						text: 'Save'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});