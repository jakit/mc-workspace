Ext.define('DM2.view.PermissionAllGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.permissionallgridpanel',

    requires: [
        'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.selection.CheckboxModel',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.height = 250;
		me.hidden = true;
		me.itemId = 'permissionallgridpanel';
		me.ui = 'activitypanel';
		me.closable = true;
		me.title = 'All Permission';
		me.store = Ext.create('DM2.store.AllPermission');
	
		me.columns = [
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'entityname',
				text: 'Entity Name',
				flex: 1
			},
			{
				xtype: 'checkcolumn',
				disabled: true,
				dataIndex: 'isgroup',
				text: 'Group',
				flex: 1
			},
			{
				xtype: 'gridcolumn',
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					metaData.tdAttr = 'data-qtip="' + value + '"';
					return value;
				},
				dataIndex: 'description',
				text: 'Description',
				flex: 1
			}
		];
		me.selModel = {
			selType: 'checkboxmodel',
			showHeaderCheckbox: false
		};
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						itemId: 'savepermission',
						text: 'Save'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});