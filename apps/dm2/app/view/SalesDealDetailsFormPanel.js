Ext.define('DM2.view.SalesDealDetailsFormPanel', {
    extend : 'Ext.form.Panel',
    alias : 'widget.salesdealdetailsformpanel',

    requires : ['DM2.view.DealNotesGridPanel',
				'DM2.view.DealDocsGridPanel',
				'DM2.view.DealDetailQuotesGridPanel',
				'DM2.view.DealDetailUserGridPanel',
				//'DM2.view.DealDetailResearchPanel',
				'DM2.view.DealDetailStatusHistoryPanel',
				'DM2.view.DealSalesGridPanel',
				'DM2.view.SalesDealDetailsCenterPanel',
				'DM2.view.DefaultWorkAreaPanel',
				'DM2.view.DealHistoryGrid',
				'Ext.tab.Panel',
				'Ext.grid.Panel',
				'Ext.tab.Tab',
				'Ext.tab.Bar'],

    constructor : function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent : function() {
        var me = this;
        me.title = 'Deal Details';
        me.frame = true;
     //   me.itemId = 'dealdetailsformpanel';
        me.margin = '2';
        me.defaults = {
            anchor : '100%'
        };
        me.layout = {
            type : 'border'
        };
        me.closable = true;
		me.header = false;
        /*
		me.header = {
            titlePosition : 1,
            items : [{
                xtype : 'image',
                margin : '0 10 0 0',
                width : '24px',
                height : '24px',
                src : 'resources/images/icons/deal_m.png'
            }]
        };
		*/

        me.items = [{
            xtype : 'panel',
            region : 'center',
            layout : 'border',
            items : [{
                xtype : 'tabpanel',
				action:'salesdealdetailtabpanel',
                region : 'south',
                split : true,
                height : '24%',
                collapsible : true,
                header : false,
                activeTab : 0,
                items : [],
                tabBar : {
                    xtype : 'tabbar',
                    ui : 'submenutabpanel'
                }
            }, {
                xtype : 'salesdealdetailscenterpanel',
                region : 'center'
            }]
        }, {
            xtype : 'container',
            region : 'east',
            split : true,
            style : 'background-color:#fff',
            width : '33%',
            layout : 'card',
            items : [{
                xtype : 'defaultworkareapanel'
            }]
        }];
        
        me.callParent(arguments);
    }
}); 