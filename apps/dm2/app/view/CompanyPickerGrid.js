Ext.define('DM2.view.CompanyPickerGrid', {
    extend: 'DM2.view.component.grid.SelectionGridNew',
    xtype: 'companypickergrid',
    requires: ['DM2.view.component.grid.SelectionGridNew'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
        me.store = Ext.create('DM2.store.Company');

		me.columns = [{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'companyName',
			text: 'Company',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'office',
			text: 'Office',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'address',
			text: 'Address',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			hidden:true,
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},                   
			dataIndex: 'state',
			text: 'State',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'city',
			text: 'City',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			hidden: true,
			dataIndex: 'phone1',
			text: 'Phone',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			hidden: true,
			dataIndex: 'email1',
			text: 'Email',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}];       
        me.callParent(arguments);
    }
});