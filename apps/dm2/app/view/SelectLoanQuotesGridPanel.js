Ext.define('DM2.view.SelectLoanQuotesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.selectloanquotesgridpanel',

    requires: [
        'Ext.grid.column.Check',
        'Ext.grid.column.Number',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

    cls: 'gridcommoncls',
    store: 'DealQuotesAll',

    columns: [
        {
            xtype: 'checkcolumn',
            hidden: true,
            dataIndex: 'selected',
            text: 'Selected',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'bank',
            text: 'Bank',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'loantype',
            text: 'Loan Type',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 70,
            dataIndex: 'term',
            text: 'Term'
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'receivedamount',
            text: 'Requested Amount',
            flex: 1,
			format: '0,000',
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                 //var formatVal = Ext.util.Format.number(value,'0,000');
				if(value==null || value==0){
					return null;  
				}else{			
					var formatVal = Ext.util.Format.currency(value, '$',0);
					metaData.tdAttr = 'data-qtip="' + formatVal + '"';
					return formatVal;
				}
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'ratetype',
            text: 'Rate Type',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'spread',
            text: 'Spread',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'index',
            text: 'Index',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'indexvalue',
            text: 'Index Value',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'amortterm',
            text: 'Amort Term',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            width: 60,
            dataIndex: 'io',
            text: 'IO',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'ppp',
            text: 'PPP',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            itemId: 'selectloangridtoolbar',
            items: [
                {
                    xtype: 'button',
                    itemId: 'selectloandonebtn',
                    text: 'Done'
                }
            ]
        }
    ]
});