Ext.define('DM2.view.GeneralDocSelRepositoryPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.generaldocselrepositorypanel',

    requires: [
        'DM2.view.GeneralDocsSelRepositoryGridPanel',
        'Ext.form.Panel',
        'Ext.button.Button',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Number',
        'Ext.grid.Panel'
    ],

    frame: true,
    margin: 3,
    ui: 'activitypanel',
    layout: 'border',
    closable: true,
    title: 'Select Document',
	
	items: [
        {
            xtype: 'form',
            region: 'north',
            split: true,
            bodyPadding: 10,
            collapsible: true,
            header: false,
            title: 'Bank Info',
            items: [
                {
                    xtype: 'panel',
					margin: '0 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            flex: 1,
                            itemId: 'name',                            
                            fieldLabel: 'Doc Name',
                            labelWidth: 80,
                            name: 'name'
                        },
                        {
                            xtype: 'button',
                            itemId: 'savedocbtn',
                            margin: '0 0 0 10',
                            text: 'Save'
                        }
                    ]
                },
				{
					xtype: 'textfield',
					anchor: '100%',
					fieldLabel: 'Description',
					labelWidth: 80,
					name: 'description'
				},
                {
                    xtype: 'panel',
                    margin: '5 0 0 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [                        
                        {
                            xtype: 'combobox',
                            flex: 1,
                            itemId: 'classification',                           
                            fieldLabel: 'Classification',
                            //labelAlign: 'right',
                            labelWidth: 80,
                            name: 'classification',
                            queryMode: 'local',
                            store: 'Classifications',
                            valueField: 'selectionDesc',
							displayField: 'selectionDesc'
                        },
						{
							xtype: 'datefield',
							flex:1,
							readOnly:true,
							labelAlign: 'right',
							labelWidth: 55,
							fieldLabel: 'Date',
							name: 'modifyDateTime'
						},
                        {
                            xtype: 'numberfield',
                            hidden: true,
                            fieldLabel: 'documentid',
                            name: 'idDocument'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    anchor: '100%',
                    hidden: true,
                    fieldLabel: 'Label',
                    name: 'docid'
                }
            ]
        },
        {
            xtype: 'generaldocsselrepositorygridpanel',
            region: 'center'
        }
    ]

});