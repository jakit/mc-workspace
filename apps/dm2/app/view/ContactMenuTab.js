Ext.define('DM2.view.ContactMenuTab', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.contactmenutab',

    requires: [
        'Ext.menu.Item',
        'Ext.menu.Menu'
    ],

    width: 160,

    items: [
        {
            xtype: 'menuitem',
			action:'addcontact',
            text: 'Add Contact',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'add'
        },
		{
            xtype: 'menuitem',
            text: 'Edit Contact',
			action: 'editcontact',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        },
        {
            xtype: 'menuitem',
			action:'removecontact',
            text: 'Remove Contact',
            focusable: true,
			modifyMenu : 'yes',
			menuType : 'edit'
        },
        {
            xtype: 'menuitem',
            itemId: 'callcontact',
            text: 'Call Contact',
            focusable: true,
			readMenu : 'yes',
            menu: {
                xtype: 'menu',
                width: 250
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'emailcontact',
            text: 'Email Contact',
            focusable: true,
			readMenu : 'yes',
            menu: {
                xtype: 'menu',
                width: 300
            }
        },
		{
            xtype: 'menuitem',
            itemId: 'faxcontact',
            text: 'Fax Contact',
            focusable: true,
			readMenu : 'yes',
            menu: {
                xtype: 'menu',
                width: 300
            }
        }
    ]
});