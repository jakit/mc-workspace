Ext.define('DM2.view.DealMenuLoanGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealmenuloangridpanel',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date'
    ],

    cls: 'dealmenuloangridcls',
    frame: true,
    ui: 'propertyloansubgrid',
    header: false,
    store: 'Loansdeal',
	margin:'0 0 0 2',
    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'bank',
            text: 'Bank',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'loanamt',
            text: 'Loan Amount',
            format: '0,000',
			flex: 1,
			align:'right',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                 //var formatVal = Ext.util.Format.number(value,'0,000');
				if(value==null || value==0){
					return null;  
				}else{
					var formatVal = Ext.util.Format.currency(value, '$',0);
					metaData.tdAttr = 'data-qtip="' + formatVal + '"';
					return formatVal;
				}			
            }
        },
        {
            xtype: 'datecolumn',
            dataIndex: 'loandate',
            text: 'Loan Date',
			flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'loantype',
            text: 'Loan Type',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'loanrate',
            text: 'Loan Rate',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'loanspread',
            text: 'Loan Spread',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'term',
            text: 'Term',
            flex: 1
        },
		{
            xtype: 'checkcolumn',
            dataIndex: 'existingLoan',
            text: 'ExistLoan',
			processEvent: Ext.emptyFn,
            flex: 1
        }
    ],

    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            store : me.buildStore()
        });

        me.callParent(arguments);
    },

    buildStore: function() {
        return Ext.create('DM2.store.Loansdeal', {});
    },
	listeners:{
		'itemmouseenter': function( view, record, item, index, e, eOpts ){
			//console.log("Hover on Loan Rec : "+record.get('bank'));
			
			var dealmenuprloanpanel = view.grid.ownerCt;
			var grids = dealmenuprloanpanel.query('grid');
			//console.log(grids);
			var prptygrid = grids[0];
			var loangrid  = grids[1];
			
			var loanid = record.get('loanid');

			var objArr = prptygrid.getStore().getProxy().getReader().rawData;
			Ext.Array.each(objArr, function(obj, index, countriesItSelf) {
				if(obj.loanid===loanid){
					var propertyid = obj.propertyid;
					var propertyrec = prptygrid.getStore().getById(propertyid);
					var rowIndex = prptygrid.store.indexOf(propertyrec);
					//console.log("rowIndex"+rowIndex);
					//Fine Row Index from grid using rec
					prptygrid.getView().addRowCls(rowIndex, 'hover-row-style');
				}
			});
		},
		'itemmouseleave': function( view, record, item, index, e, eOpts ){
			//console.log("Hover on Loan Rec : "+record.get('bank'));
			
			var dealmenuprloanpanel = view.grid.ownerCt;
			var grids = dealmenuprloanpanel.query('grid');
			//console.log(grids);
			var prptygrid = grids[0];
			var loangrid  = grids[1];
			
			var loanid = record.get('loanid');
			
			var objArr = prptygrid.getStore().getProxy().getReader().rawData;
			Ext.Array.each(objArr, function(obj, index, countriesItSelf) {
				if(obj.loanid===loanid){
					var propertyid = obj.propertyid;
					var propertyrec = prptygrid.getStore().getById(propertyid);
					var rowIndex = prptygrid.store.indexOf(propertyrec);
					//console.log("rowIndex"+rowIndex);
					//Fine Row Index from grid using rec
					prptygrid.getView().removeRowCls(rowIndex, 'hover-row-style');
				}
			});
		}
	}

});