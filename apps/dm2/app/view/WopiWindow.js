Ext.define('DM2.view.WopiWindow', {
    //extend: 'Ext.window.Window',
	extend: 'Ext.panel.Panel',
    alias: 'widget.wopiwindow',

    requires: [
        'Ext.window.Window'
    ],

    //height: 550,
    //width: 650,
    layout: 'fit',
	border: false,
    frame: true,
    margin: 3,
    ui: 'activitypanel',
    //bodyPadding: 10,
    closable: true,
	title: 'Docviewer'
});