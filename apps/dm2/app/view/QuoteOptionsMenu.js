Ext.define('DM2.view.QuoteOptionsMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.quoteoptionsmenu',
	requires: [
        'Ext.menu.Item'
    ],

    width: 120,

    items: [
        {
            xtype: 'menuitem',
            itemId: 'editoption',
            text: 'Edit Option',
            focusable: true,
			modifyMenu : 'yes'
        },
        {
            xtype: 'menuitem',
            itemId: 'deleteoption',
            text: 'Delete Option',
            focusable: true,
			modifyMenu : 'yes'
        }
    ]

});