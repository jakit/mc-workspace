Ext.define('DM2.view.AcrisNotesGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.acrisnotesgrid',

    requires: [
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'DM2.model.Note',
        'Ext.data.proxy.Rest'
    ],
    
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'gridcommoncls';
        me.title = 'ACRIS Notes Per Document';
        me.columnLines = false;
        me.ui = 'activitypanel';
		me.frame = true;
        me.store = Ext.create('DM2.store.AcrisNotes');
        me.scrollable = true;
		me.collapsible = true;
		me.style = "border-width:2px;";
        me.columns = [{
            xtype: 'gridcolumn',
            dataIndex: 'noteno',
            text: 'ID#',
            flex: 1
        },{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'notes',
            text: 'Notes',
            flex: 1
        }];

        me.callParent(arguments);
    }
});