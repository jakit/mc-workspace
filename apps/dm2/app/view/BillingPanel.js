Ext.define('DM2.view.BillingPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.billingpanel',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Date',
        'Ext.form.field.Checkbox',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Number',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.XTemplate'
    ],
	
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent: function() {
		var me = this;
		me.frame = true;
		me.itemId = 'billingpanel';
		me.margin = 3;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.bodyPadding = 5;
		me.closable = true;
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					action: 'sendbillbtn',
					itemId: 'sendbillbtn',
					text: 'Send Bill'
				}
			]
		};
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		var existingLoansSt = Ext.create('DM2.store.ExistingLoans');//'ExistingLoans';
		var dealpropertySt = Ext.create('DM2.store.Dealproperty');//'Dealproperty';		
		me.items = [
			{
				xtype: 'textfield',				
				fieldLabel: 'Fee Type',
				name: 'feetype'
			},
			{
				xtype: 'numberfield',				
				fieldLabel: 'Amount',
				name: 'amount'
			},
			{
				xtype: 'datefield',
				fieldLabel: 'Target Request Bill Date',
				name: 'trgtclosingdt'
			},
			{
				xtype: 'textareafield',
				itemId: 'accntnote',
				fieldLabel: 'Accounting Note'
			}/*,
			{
				xtype: 'textfield',				
				fieldLabel: 'Index',
				labelWidth: 60,
				name: 'index',
				readOnly: true
			},
			{
				xtype: 'numberfield',
				fieldLabel: 'Target Payoff Amount',
				name: 'trgtpayoutamt'
			}*/
		];
		me.callParent(arguments);
    }
});