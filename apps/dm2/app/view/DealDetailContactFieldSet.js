Ext.define('DM2.view.DealDetailContactFieldSet', {
    extend: 'DM2.view.component.container.GridToForm',
    alias: 'widget.dealdetailcontactfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool',
        'DM2.view.DealDetailContactMenu',
        'DM2.model.Contact',
        'Ext.data.proxy.Rest'        
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        
        var me = this;
        //me.frame = true;
        me.itemId = 'dealdetailcontactfieldset';
        //me.ui = 'contactdealpanel';      
        me.detailOnSingleRecord = true;
        me.contextMenu = Ext.create('DM2.view.DealDetailContactMenu');
        
        
        // *************
		var detailPanel,dealid;
        if(me.up('tab-deal-detail')){
        	detailPanel = me.up('tab-deal-detail');
        	dealid = detailPanel.dealid;
		}else if(me.up('tab-sales-detail')){
			detailPanel = me.up('tab-sales-detail');
        	dealid = detailPanel.dealid;
		}
        
        var store = Ext.create('Ext.data.Store', {
            storeId: 'store:' + dealid, 
            model: 'DM2.model.Contact',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_contactsPerDealOnly'
            },        
            autoLoad: false     
        });
        
        
        // ***************        
        
        me.listLayout = {
            icon: 'resources/images/icons/contact_m.png',
            title: "CONTACTS",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailcontactgrid'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            // store: 'DealDetailContacts',
            store: store,
			ui : 'contactdealpanel', 
			frame:true,
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                //dataIndex: 'fullName',
				dataIndex: 'displayName',
                text: 'Name',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'contactType',
                text: 'Role',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'officePhone_1',
                text: 'Phone',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'email_1',
                text: 'Email',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/contact_m.png',
            title: "CONTACTS",     
            scrollable: true,
			ui : 'contactdealpanel', 
			frame:true,
            items : [{
                xtype: 'container',
                layout: {
                    type: 'column'
                },
                items: [{
                    columnWidth: 0.5,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'displayfield',
                        fieldLabel: 'contactid',
                        labelAlign: 'right',
                        labelWidth: 65,
						hidden:true,
                        name: 'contactid'
                    },{
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'DisplayName',
                        labelAlign: 'right',
                        labelWidth: 65,
						name: 'displayName'
                    },{
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Name',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'fullName'
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Company',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'companyName',
                        readOnly: true
                    },
					{
						xtype: 'textfield',
						fieldLabel: 'Title',
						labelAlign: 'right',
						labelWidth: 65,
						name: 'title',
						readOnly: true
					},
                    /*{
						xtype: 'panel',
                        margin: '0px 0px 5px 0px',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [{
							xtype: 'checkboxfield',
							flex: 0.8,
							fieldLabel: 'Primary',
							labelAlign: 'right',
							labelWidth: 65,
							name: 'isprimary',
							itemId: 'isprimary',
							readOnly: true
						},{
                            xtype: 'textfield',
                            flex: 1.2,
                            fieldLabel: 'Title',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'title',
                            readOnly: true
                        }]
                    },*/
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'City',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'city',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Phone 1',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'officePhone_1',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Phone 2',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'officePhone_2',
                        readOnly: true
                    }]
                }, {
                    columnWidth: 0.5,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Role',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'contactType'
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Address',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'address',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'zipCode',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'zipCode',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'State',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'state',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Email 1',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'email_1',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',                        
                        fieldLabel: 'Email 2',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'email_2',
                        readOnly: true
                    }]
                }]
            }]
        };
        
        me.callParent(arguments);
    }

});