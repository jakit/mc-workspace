Ext.define('DM2.view.CreateNewDealFromPropertyGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.createnewdealfrompropertygrid',

    requires: [
        'Ext.view.Table',
        'Ext.grid.column.Date',
        'Ext.selection.CheckboxModel',
		'DM2.view.CreateNewDealPropertyGridMenu'
    ],

	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
		var me = this;
		me.cls = 'gridcommoncls';
		me.frame = true;
		me.height = 170;
		me.maxHeight = 175;
        me.minHeight = 100;
		me.itemId = 'createnewdealfrompropertygrid';
		me.margin = '5 0 0 0';
		me.scrollable = true;
		me.ui = 'activitypanel';
		me.collapsible = true;
		me.title = 'Properties';
		me.columnLines = true;
        me.multiColumnSort = true;
        me.sortableColumns = false;
		
		me.contextMenu = Ext.create('DM2.view.CreateNewDealPropertyGridMenu');
		
		me.store = Ext.create('DM2.store.Dealproperty');
		
		me.columns = [
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    width: 70,
                    dataIndex: 'street_no',
                    text: 'No.'
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'street',
                    text: 'Street',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'city',
                    text: 'City',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    width: 60,
                    dataIndex: 'state',
                    text: 'St.'
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    hidden: true,
                    dataIndex: 'buildingClass',
                    text: 'Bldg Cls'
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    hidden: true,
                    dataIndex: 'propertyType',
                    text: 'Property Type'
                },
                {
                    xtype: 'checkcolumn',
					itemId:'primaryProperty',
                    dataIndex: 'primaryProperty',
                    text: 'IsPrimary',
                    flex: 1
                }
            ];
		me.selModel = {
			selType: 'checkboxmodel'
		};
		me.plugins  = [
			{
				ptype: 'cellediting',
				clicksToEdit: 1/*,
				listeners: {
					beforeedit: 'onCellEditingBeforeEdit'
				}*/
			}
		];
		me.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						xtype: 'button',
						action: 'showaddnewdealpropertybtn',
						iconCls: 'add',
						text: 'Add Property'
					}
				]
			}
		];
		me.callParent(arguments);
    }
});