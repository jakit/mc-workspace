Ext.define('DM2.store.Notes', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Note',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.Note',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_notesPerDeal'
            }
        }, cfg)]);
    }
});