Ext.define('DM2.store.ContactFax', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.ContactFax',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'ContactFax',
            autoSync: true,
            model: 'DM2.model.ContactFax',
            proxy: {
                type: 'rest',
                api: {
                    read: DM2.view.AppConstants.apiurl+'mssql:Fax'
                },
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Fax',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});