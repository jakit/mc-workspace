Ext.define('DM2.store.Office', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Office',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'Office',
            model: 'DM2.model.Office',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Office'
            }
        }, cfg)]);
    }
});