Ext.define('DM2.store.Users', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.DealUser',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'DealUsers',
            model: 'DM2.model.DealUser',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Users'
            }
        }, cfg)]);
    }
});