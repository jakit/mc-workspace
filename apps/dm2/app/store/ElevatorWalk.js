Ext.define('DM2.store.ElevatorWalk', {
    extend: 'Ext.data.Store',

    requires: [
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'ElevatorWalk',
			fields: ['id', 'desc'],
            data: [
                {
                    id: 'e',
                    desc: 'e'
                },
                {
                    id: 'w',
                    desc: 'w'
                }
            ]
        }, cfg)]);
    }
});