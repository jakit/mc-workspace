Ext.define('DM2.store.Classifications', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Classification'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'Classifications',
            model: 'DM2.model.Classification'
        }, cfg)]);
    }
});