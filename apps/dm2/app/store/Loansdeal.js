Ext.define('DM2.store.Loansdeal', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Loandeal',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Loansdeal',
            model: 'DM2.model.Loandeal',
            proxy: {
                type: 'rest',
                noCache: false,
                //url: DM2.view.AppConstants.apiurl+'mssql:v_propertyAndLoanPerDeal',
				reader: {
                    type: 'json',
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
                                if(item.loanid!=null && item.loanid!=0){
                                    newdata.push(item);
                                }
                            });
                            return newdata;
                        },
                        scope: this
                    }
                }
            }
        }, cfg)]);
    }
});