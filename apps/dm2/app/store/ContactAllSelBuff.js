Ext.define('DM2.store.ContactAllSelBuff', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        'DM2.model.Dealhascontact',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            pageSize: 50,
            storeId: 'ContactAllSelBuff',
            autoLoad: false,
            model: 'DM2.model.Dealhascontact',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
				url: DM2.view.AppConstants.apiurl+'v_contacts',
				originalUrl: DM2.view.AppConstants.apiurl+'v_contacts',
                reader: {
                    type: 'json'
                }
            },
            listeners: {
                beforeload: {
                    fn: me.onBufferedStoreBeforeLoad,
                    scope: me
                }
            },
            sorters: {
                property: 'lastName'
            }
        }, cfg)]);
    },

    onBufferedStoreBeforeLoad: function(store, operation, eOpts) {
        var extraparams = {};
        if(store.sorters && store.sorters.getCount()){
            var sorter = store.sorters.getAt(0);
            console.log(sorter._direction);
            console.log(sorter.config.property);
            extraparams.order = sorter._property+' '+sorter._direction;
        }
        store.getProxy().extraParams = extraparams;
    }
});