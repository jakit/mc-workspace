Ext.define('DM2.store.AcrisNotes', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.AcrisNote',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'AcrisDocuments',
            autoLoad: false,
            model: 'DM2.model.AcrisNote',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'mssql:v_AcrisNote',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});