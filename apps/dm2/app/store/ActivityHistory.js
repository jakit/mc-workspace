Ext.define('DM2.store.ActivityHistory', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.ActivityHistory',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'ActivityHistory',
            model: 'DM2.model.ActivityHistory',
            proxy: {
                type: 'rest',
                url: DM2.view.AppConstants.apiurl+'mssql:ActivityHistory'
            }
        }, cfg)]);
    }
});