Ext.define('DM2.store.Allproperties', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        'DM2.model.PropertyMaster',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
			pageSize: 50,
            model: 'DM2.model.PropertyMaster',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'v_rPropertyMaster',
				originalUrl: DM2.view.AppConstants.apiurl+'v_rPropertyMaster', // as a backup
				reader: {
                    type: 'json',
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
                                if(item.idPropertyMaster){
                                    newdata.push(item);
                                }
                            });
                            return newdata;
                        },
                        scope: this
                    }
                }
            },
			listeners: {
                beforeload: {
                    fn: me.onBufferedStoreBeforeLoad,
                    scope: me
                }
            },
            sorters: {
                property: 'idPropertyMaster'
            }
        }, cfg)]);
    },

    onBufferedStoreBeforeLoad: function(store, operation, eOpts) {
        var extraparams = {};
        if(store.sorters && store.sorters.getCount()){
            var sorter = store.sorters.getAt(0);
            console.log(sorter._direction);
            console.log(sorter.config.property);
            extraparams.order = sorter._property+' '+sorter._direction;
        }
        if(store.filters && store.filters.getCount()){
            console.log(store.filters);
            var filters = store.filters;

            var filterstarr = [];

            for(var j=0;j<filters.getCount();j++){

                console.log(filters.getAt(j));
                var tempfilter = filters.getAt(j);

                if(tempfilter._operator==="like"){
                    var filterst = tempfilter._property + " " +tempfilter._operator + " " + "'%" +tempfilter._value+"%'";
                }else if(tempfilter._operator==="eq" || tempfilter._operator==="lt" || tempfilter._operator==="gt"){
                    if(tempfilter._operator==="eq"){
                        var opst = "=";
                    }
                    else if(tempfilter._operator==="lt"){
                        var opst = "<";
                    }
                    else if(tempfilter._operator==="gt"){
                        var opst = ">";
                    }
                    var filterst = tempfilter._property + " " +opst + " '"+tempfilter._value+"'";
                }
                filterstarr.push(filterst);
            }

            extraparams.filter = filterstarr;
        } else {
            extraparams.filter = [];
        }
        store.getProxy().extraParams = extraparams;
    }
});