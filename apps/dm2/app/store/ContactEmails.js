Ext.define('DM2.store.ContactEmails', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.ContactEmail',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'ContactEmails',
            autoSync: true,
            model: 'DM2.model.ContactEmail',
            proxy: {
                type: 'rest',
                api: {
                    create: DM2.view.AppConstants.apiurl+'mssql:Email',
                    read: DM2.view.AppConstants.apiurl+'mssql:Email',
                    update: DM2.view.AppConstants.apiurl+'mssql:Email',
                    destroy: DM2.view.AppConstants.apiurl+'mssql:Email'
                },
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Email',
                reader: {
                    type: 'json'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function(data, request) {
                            // do some manipulation of the unserialized data object
                            //console.log(data);
                            //console.log(request);
                            if(request._action==="create"){
                                Ext.Object.each(data, function(key, value, myself) {
                                    //console.log(key + ":" + value);
                    
                                    if (key === 'idEmail') {
                                        //console.log("Delete the key"+key);
                                        delete data[key];
                                    }
                                });
                            }
                            if(request._action==="update"){
                                data['@metadata'] = {'checksum' : 'override'};
                            }
                            //console.log(data);
                            return data;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                write: {
                    fn: me.onStoreWrite,
                    scope: me
                }
            }
        }, cfg)]);
    },

    onStoreWrite: function(store, operation, eOpts) {
        //console.log(store);
        //console.log(operation);
        if(operation.request._action==="create"){
            //console.log("Create Write completes");
            //console.log(operation._records[0].data.txsummary[0].idPhone);
            store.setAutoSync(false);
            store.getAt(0).set('idEmail',operation._records[0].data.txsummary[0].idEmail);
            store.setAutoSync(true);
        }
    }

});