Ext.define('DM2.store.Bankallbuff', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        'DM2.model.Bank',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            pageSize: 50,
			//numFromEdge : 10, 
			//trailingBufferZone : 25,
			//leadingBufferZone : 25,
			//purgePageCount : 1,
            storeId: 'Bankallbuff',
            autoLoad: false,
            model: 'DM2.model.Bank',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'v_banks',
				originalUrl: DM2.view.AppConstants.apiurl+'v_banks',
                reader: {
                    type: 'json'
                }
            },
            listeners: {
                beforeload: {
                    fn: me.onBufferedStoreBeforeLoad,
                    scope: me
                }
            },
            sorters: {
                direction: 'DESC',
                property: 'idBank'
            }
        }, cfg)]);
    },

    onBufferedStoreBeforeLoad: function(store, operation, eOpts) {
        var extraparams = {};
        if(store.sorters && store.sorters.getCount()) {
            var sorter = store.sorters.getAt(0);
            extraparams.order = sorter._property+' '+sorter._direction;
        }
        store.getProxy().extraParams = extraparams;
    }
});