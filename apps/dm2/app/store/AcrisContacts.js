Ext.define('DM2.store.AcrisContacts', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.AcrisContact',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'AcrisDocuments',
            autoLoad: false,
            model: 'DM2.model.AcrisContact',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'mssql:v_AcrisContact',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});