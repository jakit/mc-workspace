Ext.define('DM2.store.PPP', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.PPP'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'PPP',
            model: 'DM2.model.PPP'
        }, cfg)]);
    }
});