Ext.define('DM2.store.IndexSpread', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.IndexSpread',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'IndexSpread',
            model: 'DM2.model.IndexSpread',
            proxy: {
                type: 'rest',
                noCache: false,
				url: DM2.view.AppConstants.apiurl+'mssql:IndexSpread',
				reader: {
                    type: 'json'
                }
            }/*,
            sorters: {
                property: 'selName'
            }*/
        }, cfg)]);
    }
});