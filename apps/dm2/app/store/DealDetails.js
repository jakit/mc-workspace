Ext.define('DM2.store.DealDetails', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.DealDetail',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.DealDetail',
            proxy: {
                type: 'rest',
				pageParam : '',
				limitParam : '',
				startParam : '',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'RdealDetails',
				listeners: {
					exception: function(proxy, response, options){
						//Ext.MessageBox.alert('Error', response.status + ": " + response.statusText);
						//console.log("Exception is comming");
						/*
						console.log(response);
						var obj = Ext.decode(response.responseText);
						if(response.status==500 && obj.statusCode==500 && obj.errorCode==5004)
						{
							console.log("Exception is comming : "+obj.errorMessage);
							DM2.app.getController('DealController').dealPermissionNotAllowed();
						}*/
					}
				}
            }
        }, cfg)]);
    }
});