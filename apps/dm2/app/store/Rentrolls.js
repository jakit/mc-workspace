Ext.define('DM2.store.Rentrolls', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Rentroll',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Rentrolls',
            model: 'DM2.model.Rentroll',
            proxy: {
                type: 'rest',
                url: DM2.view.AppConstants.apiurl+'mssql:Rentroll'
            }
        }, cfg)]);
    }
});