Ext.define('DM2.store.Properties', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Property',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Properties',
            model: 'DM2.model.Property',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_propertyAndLoanPerDeal'
            }
        }, cfg)]);
    }
});