Ext.define('DM2.store.DealUsers', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.DealUser',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'DealUsers',
            model: 'DM2.model.DealUser',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_usersPerDeal'
            }
        }, cfg)]);
    }
});