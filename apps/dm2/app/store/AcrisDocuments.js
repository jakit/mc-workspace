Ext.define('DM2.store.AcrisDocuments', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Document',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'AcrisDocuments',
            autoLoad: false,
            model: 'DM2.model.AcrisDocument',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'mssql:v_AcrisDocGrid',
				reader: {
                    type: 'json',
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
                                if(item.idAcris){
                                    newdata.push(item);
                                }
                            });
                            return newdata;
                        },
                        scope: this
                    }
                }
            }
        }, cfg)]);
    }
});