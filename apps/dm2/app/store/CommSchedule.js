Ext.define('DM2.store.CommSchedule', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.CommSchedule',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'CommSchedule',
            //autoSync: true,
            model: 'DM2.model.CommSchedule',
            proxy: {
                type: 'rest',
                /*api: {
                    create: DM2.view.AppConstants.apiurl+'mssql:LoanSchedule',
                    read: DM2.view.AppConstants.apiurl+'mssql:v_LoanSched',
                    update: DM2.view.AppConstants.apiurl+'mssql:LoanSchedule',
                    destroy: DM2.view.AppConstants.apiurl+'mssql:LoanSchedule'
                },*/
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_CommissionSched',
                reader: {
                    type: 'json'
                }/*,
                writer: {
                    type: 'json',
                    transform: {
                        fn: function(data, request) {
                            // do some manipulation of the unserialized data object
                            //console.log(data);
                            //console.log(request);
                            if(request._action==="create"){
                                Ext.Object.each(data, function(key, value, myself) {
                                    //console.log(key + ":" + value);
                    
                                    if (key === 'idLoanSched') {
                                        //console.log("Delete the key"+key);
                                        delete data[key];
                                    }
                                });
                            }
                            if(request._action==="update"){
                                data['@metadata'] = {'checksum' : 'override'};
                            }
                            //console.log(data);
                            return data;
                        },
                        scope: this
                    }
                }*/
            }/*,
			sorters: {
                direction: 'ASC',
				property: 'orderID'
            }/*,
            listeners: {
                write: {
                    fn: me.onStoreWrite,
                    scope: me
                }
            }*/
        }, cfg)]);
    },

    onStoreWrite: function(store, operation, eOpts) {
        //console.log(store);
        //console.log(operation);
        if(operation.request._action==="create"){
            //console.log("Create Write completes");
            //console.log(operation._records[0].data.txsummary[0].idPhone);
            store.setAutoSync(false);
            store.getAt(0).set('idCommissionSched',operation._records[0].data.txsummary[0].idCommissionSched);
            store.setAutoSync(true);
        }
    }
});