Ext.define('DM2.store.Sales', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Sale',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.Sale',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_salesPerDeal'
            }
        }, cfg)]);
    }
});