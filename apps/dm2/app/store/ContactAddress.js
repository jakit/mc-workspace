Ext.define('DM2.store.ContactAddress', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.ContactAddress',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.data.writer.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'ContactAddress',
            autoSync: true,
            model: 'DM2.model.ContactAddress',
            proxy: {
                type: 'rest',
                api: {
                    create: DM2.view.AppConstants.apiurl+'mssql:Address',
                    read: DM2.view.AppConstants.apiurl+'mssql:Address',
                    update: DM2.view.AppConstants.apiurl+'mssql:Address',
                    destroy: DM2.view.AppConstants.apiurl+'mssql:Address'
                },
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Address',
                reader: {
                    type: 'json'
                },
                writer: {
                    type: 'json',
                    transform: {
                        fn: function(data, request) {
                            // do some manipulation of the unserialized data object
                            //console.log(data);
                            //console.log(request);
                            if(request._action==="create"){
                                Ext.Object.each(data, function(key, value, myself) {
                                    //console.log(key + ":" + value);
                    
                                    if (key === 'idAddress') {
                                        //console.log("Delete the key"+key);
                                        delete data[key];
                                    }
                                });
                            }
                            if(request._action==="update"){
                                data['@metadata'] = {'checksum' : 'override'};
                            }
                            //console.log(data);
                            return data;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                write: {
                    fn: me.onStoreWrite,
                    scope: me
                }
            }
        }, cfg)]);
    },

    onStoreWrite: function(store, operation, eOpts) {
        //console.log(store);
        //console.log(operation);
        if(operation.request._action==="create"){
            //console.log("Create Write completes");
            //console.log(operation._records[0].data.txsummary[0].idPhone);
            store.setAutoSync(false);
            store.getAt(0).set('idAddress',operation._records[0].data.txsummary[0].idAddress);
            store.setAutoSync(true);
        }
    }

});