Ext.define('DM2.store.StatesList', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.StatesList'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'StatesList',
            model: 'DM2.model.StatesList',
            data: [  
                {
					'display': "AZ",
					'val': "AZ"
				},
				{
					'display': "CA",
					'val': "CA"
				},
				{
					'display': "CO",
					'val': "CO"
				},
				{
					'display': "CT",
					'val': "CT"
				},
				{
					'display': "DC",
					'val': "DC"
				},
				{
					'display': "DE",
					'val': "DE"
				},
				{
					'display': "FL",
					'val': "FL"
				},
				{
					'display': "GA",
					'val': "GA"
				},
				{
					'display': "IL",
					'val': "IL"
				},
				{
					'display': "MA",
					'val': "MA"
				},
				{
					'display': "MD",
					'val': "MD"
				},
				{
					'display': "NC",
					'val': "NC"
				},
				{
					'display': "NJ",
					'val': "NJ"
				},
				{
					'display': "NV",
					'val': "NV"
				},
				{
					'display': "NY",
					'val': "NY"
				},
				{
					'display': "OR",
					'val': "OR"
				},
				{
					'display': "PA",
					'val': "PA"
				},
				{
					'display': "PR",
					'val': "PR"
				},
				{
					'display': "SC",
					'val': "SC"
				},
				{
					'display': "ST",
					'val': "ST"
				},
				{
					'display': "TN",
					'val': "TN"
				},
				{
					'display': "TX",
					'val': "TX"
				},
				{
					'display': "UT",
					'val': "UT"
				},
				{
					'display': "VA",
					'val': "VA"
				},
				{
					'display': "WI",
					'val': "WI"
				}
            ]
        }, cfg)]);
    }
});