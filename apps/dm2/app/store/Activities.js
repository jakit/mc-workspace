Ext.define('DM2.store.Activities', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Activity',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.Activity',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_ActivityTrack'
            }
        }, cfg)]);
    }
});