Ext.define('DM2.store.DealQuotes', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Quote',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'DealQuotes',
            model: 'DM2.model.Quote',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_quotesPerDeal'
            }
        }, cfg)]);
    }
});