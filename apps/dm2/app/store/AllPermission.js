Ext.define('DM2.store.AllPermission', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Permission',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'AllPermission',
            model: 'DM2.model.Permission',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_entity'
            }
        }, cfg)]);
    }
});