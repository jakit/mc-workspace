Ext.define('DM2.store.DealStatusHistory', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.DealStatusHistory',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'DealStatusHistory',
            model: 'DM2.model.DealStatusHistory',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_statusHistoryPerDeal'
            }
        }, cfg)]);
    }
});