Ext.define('DM2.store.Underwriting', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Underwriting',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'Underwriting',
            model: 'DM2.model.Underwriting',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'v_underwritingPerDealXProperty'
            }
        }, cfg)]);
    }
});