Ext.define('DM2.store.SaleBids', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.SaleBid',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'SaleBids',
            model: 'DM2.model.SaleBid',
            proxy: {
                type: 'rest',
                noCache: false,
				url: DM2.view.AppConstants.apiurl+'mssql:v_salesBid',
				reader: {
                    type: 'json'
                }
            }/*,
            sorters: {
                property: 'selName'
            }*/
        }, cfg)]);
    }
});