Ext.define('DM2.store.MasterLoans', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.MasterLoan',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'MasterLoans',
            model: 'DM2.model.MasterLoan',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_masterLoansPerDeal'
            }
        }, cfg)]);
    }
});