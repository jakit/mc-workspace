Ext.define('DM2.store.AKAList', {
    extend: 'Ext.data.Store',

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'AKAList',
			fields: [
				{
					name: 'text'
				}
			]
        }, cfg)]);
    }
});