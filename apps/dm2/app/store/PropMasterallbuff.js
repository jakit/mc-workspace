Ext.define('DM2.store.PropMasterallbuff', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        'DM2.model.PropertyMaster',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            pageSize: 50,
			//leadingBufferZone : 50,
			//trailingBufferZone : 50,
            autoLoad: false,
            model: 'DM2.model.PropertyMaster',
			remoteFilter: true,
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
				url: DM2.view.AppConstants.apiurl+'v_rPropertyMaster',
				originalUrl: DM2.view.AppConstants.apiurl+'v_rPropertyMaster',
                reader: {
                    type: 'json'
                }
            },
            listeners: {
                beforeload: {
                    fn: me.onBufferedStoreBeforeLoad,
                    scope: me
                }
            }
        }, cfg)]);
    },

    onBufferedStoreBeforeLoad: function(store, operation, eOpts) {
        var extraparams = {};
        if(store.sorters && store.sorters.getCount()) {
            var sorter = store.sorters.getAt(0);
            extraparams.order = sorter._property+' '+sorter._direction;
            //extraparams.sort = '';
        }
        /*if(store.filters && store.filters.getCount()) {
            console.log(store.filters);
            var filters = store.filters;

            var filterstarr = [];

            for(var j=0;j<filters.getCount();j++){

                console.log(filters.getAt(j));
                var tempfilter = filters.getAt(j);

                if(tempfilter._operator==="like"){
					var resval,searchVal;
					searchVal = tempfilter._value;
					resval = searchVal;
					var firstChar = searchVal.charAt(0);
					var lastChar  = searchVal.charAt(searchVal.length-1);
					console.log(firstChar);
					console.log(lastChar);
					if(searchVal=="^"){
						resval = "'%" +searchVal+"%'";
						return false;
					} else if(firstChar=="^" && lastChar=="^"){
						tempfilter._operator = "=";
						resval = searchVal.substring(1,searchVal.length-1);
						resval = "'"+resval+"'";
					} else if(firstChar=="^"){
						resval = searchVal.substring(1);
						resval = "'"+resval+"%'";
					} else if(lastChar=="^"){
						resval = searchVal.substring(0,searchVal.length-1);
						resval = "'%"+resval+"'";
					} else {
						resval = "'%" +searchVal+"%'";
					}
                    var filterst = tempfilter._property + " " +tempfilter._operator + " " + resval;
                    //var filterst = tempfilter._property + " " +tempfilter._operator + " " + "'%" +tempfilter._value+"%'";
                } else if(tempfilter._operator==="eq" || tempfilter._operator==="lt" || tempfilter._operator==="gt"){
                    if(tempfilter._operator==="eq"){
                        var opst = "=";
                    } else if(tempfilter._operator==="lt"){
                        var opst = "<";
                    } else if(tempfilter._operator==="gt"){
                        var opst = ">";
                    }
                    var filterst = tempfilter._property + " " +opst + " '"+tempfilter._value+"'";
                }
                filterstarr.push(filterst);
            }

            extraparams.filter = filterstarr;
        } else {
            extraparams.filter = [];
        }*/
        store.getProxy().extraParams = extraparams;
    }
});