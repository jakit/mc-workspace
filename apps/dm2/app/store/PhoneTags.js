Ext.define('DM2.store.PhoneTags', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Tag'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'PhoneTags',
            model: 'DM2.model.Tag',
            data: [
                {
                    idTag: '1',
                    tag: 'officePhone1'
                },
                {
                    idTag: '2',
                    tag: 'officePhone2'
                },
                {
                    idTag: '3',
                    tag: 'officePhone3'
                },
                {
                    idTag: '4',
                    tag: 'mobilePhone1'
                },
                {
                    idTag: '5',
                    tag: 'mobilePhone2'
                },
                {
                    idTag: '6',
                    tag: 'mobilePhone3'
                },
                {
                    idTag: '7',
                    tag: 'cell'
                }
            ]
        }, cfg)]);
    }
});