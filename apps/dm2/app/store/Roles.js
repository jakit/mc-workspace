Ext.define('DM2.store.Roles', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Role',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'Roles',
            model: 'DM2.model.Role',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Role'
            }
        }, cfg)]);
    }
});