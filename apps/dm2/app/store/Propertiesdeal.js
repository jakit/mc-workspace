Ext.define('DM2.store.Propertiesdeal', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Propertydeal',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Propertiesdeal',
            model: 'DM2.model.Propertydeal',
            proxy: {
                type: 'rest',
                noCache: false,
                //url: DM2.view.AppConstants.apiurl+'mssql:v_propertyAndLoanPerDeal',
				reader: {
                    type: 'json',
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
                                if(item.propertyid!=null && item.propertyid!=0){
                                    newdata.push(item);
                                }
                            });
                            return newdata;
                        },
                        scope: this
                    }
                }
            }
        }, cfg)]);
    }
});