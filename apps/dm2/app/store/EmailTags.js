Ext.define('DM2.store.EmailTags', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Tag'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'EmailTags',
            model: 'DM2.model.Tag',
            data: [
                {
                    idTag: '8',
                    tag: 'email1'
                },
                {
                    idTag: '9',
                    tag: 'email2'
                }
            ]
        }, cfg)]);
    }
});