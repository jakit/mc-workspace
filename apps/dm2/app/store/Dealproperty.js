Ext.define('DM2.store.Dealproperty', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Property',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Dealproperty',
            model: 'DM2.model.Property',
            proxy: {
                type: 'rest',
                url: DM2.view.AppConstants.apiurl+'mssql:v_PropertyPerDealOnly'
            }
        }, cfg)]);
    }
});