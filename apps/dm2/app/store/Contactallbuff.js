Ext.define('DM2.store.Contactallbuff', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        'DM2.model.Dealhascontact',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            pageSize: 50,
            storeId: 'Contactallbuff',
            autoLoad: false,
            model: 'DM2.model.Dealhascontact',
			remoteFilter: true,
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
				url: DM2.view.AppConstants.apiurl+'v_contacts',
				originalUrl: DM2.view.AppConstants.apiurl+'v_contacts', // as a backup
                reader: {
                    type: 'json'
                }
            },
            listeners: {
                beforeload: {
                    fn: me.onBufferedStoreBeforeLoad,
                    scope: me
                }
            },
            sorters: {
				direction: 'ASC',
				property: 'name'
            }
        }, cfg)]);
    },

    onBufferedStoreBeforeLoad: function(store, operation, eOpts) {
        var extraparams = {};
        if(store.sorters && store.sorters.getCount()) {
            var sorter = store.sorters.getAt(0);
            console.log(sorter._direction);
            console.log(sorter.config.property);
            extraparams.order = sorter._property+' '+sorter._direction;
        }
        if(store.filters && store.filters.getCount()){
            console.log(store.filters);
            var filters = store.filters;

            var filterstarr = [];

            for(var j=0;j<filters.getCount();j++){

                console.log(filters.getAt(j));
                var tempfilter = filters.getAt(j);
				
				//change filter param maincontact
                if(tempfilter._property==="phone"){
                    tempfilter._property = "phoneList";
                }
                if(tempfilter._property==="email"){
                    tempfilter._property = "emailList";
                }
				if(tempfilter._property==="fax"){
                    tempfilter._property = "faxList";
                }
				
                if(tempfilter._operator==="like"){
					var resval,searchVal;
					searchVal = tempfilter._value;
					resval = searchVal;
					var firstChar = searchVal.charAt(0);
					var lastChar  = searchVal.charAt(searchVal.length-1);
					console.log(firstChar);
					console.log(lastChar);
					if(searchVal=="^"){
						resval = "'%" +searchVal+"%'";
						return false;
					}else if(firstChar=="^" && lastChar=="^"){
						tempfilter._operator = "=";
						resval = searchVal.substring(1,searchVal.length-1);
						resval = "'"+resval+"'";
					}else if(firstChar=="^"){
						resval = searchVal.substring(1);
						resval = "'"+resval+"%'";
					}
					else if(lastChar=="^"){
						resval = searchVal.substring(0,searchVal.length-1);
						resval = "'%"+resval+"'";
					}else{
						resval = "'%" +searchVal+"%'";
					}
                    var filterst = tempfilter._property + " " +tempfilter._operator + " " + resval;
                    //var filterst = tempfilter._property + " " +tempfilter._operator + " " + "'%" +tempfilter._value+"%'";
                }else if(tempfilter._operator==="eq" || tempfilter._operator==="lt" || tempfilter._operator==="gt"){
                    if(tempfilter._operator==="eq"){
                        var opst = "=";
                    }
                    else if(tempfilter._operator==="lt"){
                        var opst = "<";
                    }
                    else if(tempfilter._operator==="gt"){
                        var opst = ">";
                    }
                    var filterst = tempfilter._property + " " +opst + " '"+tempfilter._value+"'";
                }
                filterstarr.push(filterst);
            }

            extraparams.filter = filterstarr;
        }else{
            extraparams.filter = [];
        }
        store.getProxy().extraParams = extraparams;
    }
});