Ext.define('DM2.store.BanksSelected', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Submission',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'BanksSelected',
            autoLoad: false,
            model: 'DM2.model.Submission',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'mssql:v_submissionsPerDeal_New',
                reader: {
                    type: 'json'
                }
            },
            sorters: {
                direction: 'DESC',
                property: 'idLoan'
            }
        }, cfg)]);
    }
});