Ext.define('DM2.store.SaleDealStatus', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.DealStatus'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'SaleDealStatus',
            model: 'DM2.model.DealStatus',
            data: [
                {
                    idStatus: '1',
                    status: 'PC',
					statusFullName:'Pre-contract'
                },
                {
                    idStatus: '2',
                    status: 'CO',
					statusFullName:'Contract Out'
                },
                {
                    idStatus: '3',
                    status: 'HC',
					statusFullName:'Hard Contract'
                },
                {
                    idStatus: '4',
                    status: 'CL',
					statusFullName:'Closed'
                }
            ]
        }, cfg)]);
    }
});