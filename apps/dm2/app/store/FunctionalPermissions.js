Ext.define('DM2.store.FunctionalPermissions', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.FunctionalPermission',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.FunctionalPermission',
            proxy: {
                type: 'rest',
				//pageParam : '',
				//limitParam : '',
				//startParam : '',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'FuncRights',
				listeners: {
					exception: function(proxy, response, options){
					}
				}
            }
        }, cfg)]);
    }
});