Ext.define('DM2.store.SearchLocations', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.SearchLocation'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'SearchLocations',
            model: 'DM2.model.SearchLocation'
        }, cfg)]);
    }
});