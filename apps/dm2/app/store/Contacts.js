Ext.define('DM2.store.Contacts', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Contact',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.Contact',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_contactsPerDealOnly'
            }
        }, cfg)]);
    }
});