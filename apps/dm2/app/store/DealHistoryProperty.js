Ext.define('DM2.store.DealHistoryProperty', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Deal',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'DealHistoryProperty',
            model: 'DM2.model.Deal',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:sp_dealsPerAddressUsingProperty',
				originalUrl: DM2.view.AppConstants.apiurl+'mssql:sp_dealsPerAddressUsingProperty',
				reader: {
					type: 'json',
					rootProperty: function(data){
						//console.log(data.result[0].rows);
						return data.result[0].rows;
					}
				}
            },
			sorters: {
                property: 'statusdate',
				direction: 'DESC'
            }
        }, cfg)]);
    }
});