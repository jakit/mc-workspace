Ext.define('DM2.store.Documents', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Document',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.Document',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal',
				originalUrl: DM2.view.AppConstants.apiurl+'mssql:v_docsPerDeal'
            }
        }, cfg)]);
    }
});