Ext.define('DM2.store.DocsPerSubmission', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Document',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'DocsPerSubmission',
            autoLoad: false,
            model: 'DM2.model.Document',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'mssql:v_docsPerSubmission',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});