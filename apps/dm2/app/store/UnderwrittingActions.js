Ext.define('DM2.store.UnderwrittingActions', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.UnderwrittingAction',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            //storeId: 'UnderwrittingActions',
            model: 'DM2.model.UnderwrittingAction',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:UnderwritingActions'
            }
        }, cfg)]);
    }
});