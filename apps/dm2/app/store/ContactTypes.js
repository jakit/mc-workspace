Ext.define('DM2.store.ContactTypes', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.ContactType',
        'Ext.data.proxy.Rest',
		'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'ContactTypes',
            model: 'DM2.model.ContactType',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:ContactType'
            },
			sorters: {
                direction: 'ASC',
                property: 'type'
            }
        }, cfg)]);
    }
});