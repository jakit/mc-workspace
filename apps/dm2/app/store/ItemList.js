Ext.define('DM2.store.ItemList', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        'DM2.model.ItemList',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            pageSize: 50,
            autoLoad: false,
            model: 'DM2.model.ItemList',
            remoteFilter: true,
            proxy: {
                type: 'rest',
				//sortParam : 'order',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'mssql:Members',
				originalUrl: DM2.view.AppConstants.apiurl+'mssql:Members',
                reader: {
                    type: 'json',
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
                                if(item.idMembers || item.idMembers==0){
                                    newdata.push(item);
                                } else if(item.idGroup || item.idGroup==0){
									newdata.push(item);
								}
                            });
                            return newdata;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                beforeload: {
                    fn: me.onBufferedStoreBeforeLoad,
                    scope: me
                }
            }/*,
            sorters: {
                direction: 'ASC',
				property: 'companyName'
            }*/
        }, cfg)]);
    },

    onBufferedStoreBeforeLoad: function(store, operation, eOpts) {
        var j;
        var extraparams = {};
        if(store.sorters && store.sorters.getCount()){
            var sorter = store.sorters.getAt(0);
            extraparams.order = sorter._property+' '+sorter._direction;
            //extraparams.sort = '';
        }
        
        if(store.filters && store.filters.getCount()) {
            var filters = store.filters;
            var filterstarr = [];

            for(j=0;j<filters.getCount();j++){

                var tempfilter = filters.getAt(j);

                if(tempfilter._operator==="like"){
					var resval,searchVal;
					searchVal = tempfilter._value;
					resval = searchVal;
					var firstChar = searchVal.charAt(0);
					var lastChar  = searchVal.charAt(searchVal.length-1);
					console.log(firstChar);
					console.log(lastChar);
					if(searchVal=="^"){
						resval = "'%" +searchVal+"%'";
						return false;
					}else if(firstChar=="^" && lastChar=="^"){
						tempfilter._operator = "=";
						resval = searchVal.substring(1,searchVal.length-1);
						resval = "'"+resval+"'";
					}else if(firstChar=="^"){
						resval = searchVal.substring(1);
						resval = "'"+resval+"%'";
					}
					else if(lastChar=="^"){
						resval = searchVal.substring(0,searchVal.length-1);
						resval = "'%"+resval+"'";
					}else{
						resval = "'%" +searchVal+"%'";
					}
                    var filterst = tempfilter._property + " " +tempfilter._operator + " " + resval;
                }else if(tempfilter._operator==="eq" || tempfilter._operator==="lt" || tempfilter._operator==="gt"){
                    if(tempfilter._operator==="eq"){
                        var opst = "=";
                    }
                    else if(tempfilter._operator==="lt"){
                        var opst = "<";
                    }
                    else if(tempfilter._operator==="gt"){
                        var opst = ">";
                    }
                    var filterst = tempfilter._property + " " +opst + " '"+tempfilter._value+"'";
                }
                filterstarr.push(filterst);
            }
            extraparams.filter = filterstarr;
        } else {
            extraparams.filter = [];
        }  
                  
        store.getProxy().extraParams = extraparams;
    }
});