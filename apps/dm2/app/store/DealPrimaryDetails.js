Ext.define('DM2.store.DealPrimaryDetails', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.DealPrimaryDetail',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DM2.model.DealPrimaryDetail',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_dealDetails'
            }
        }, cfg)]);
    }
});