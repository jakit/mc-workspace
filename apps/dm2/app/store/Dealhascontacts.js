Ext.define('DM2.store.Dealhascontacts', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Dealhascontact',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Dealhascontacts',
            model: 'DM2.model.Dealhascontact',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:Contact'
            },
            sorters: {
                property: 'lastName'
            }
        }, cfg)]);
    }
});