Ext.define('DM2.store.ExistingLoans', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Quote',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'ExistingLoans',
            model: 'DM2.model.Quote',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_existingLoansPerDeal'
            }
        }, cfg)]);
    }
});