Ext.define('DM2.store.DealQuotesAll', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Quote',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'DealQuotesAll',
            model: 'DM2.model.Quote',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:v_quotesPerDeal'
            },
            sorters: {
                direction: 'DESC',
                property: 'loanid'
            }
        }, cfg)]);
    }
});