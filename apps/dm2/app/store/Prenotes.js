Ext.define('DM2.store.Prenotes', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Prenote',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Prenotes',
            model: 'DM2.model.Prenote',
            proxy: {
                type: 'rest',
                url: DM2.view.AppConstants.apiurl+'mssql:standardNotes'
            }
        }, cfg)]);
    }
});