Ext.define('DM2.store.MortgageTypeList', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.SelectionList',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'MortgageTypeList',
            model: 'DM2.model.SelectionList',
            proxy: {
                type: 'rest',
                noCache: false,
                url: DM2.view.AppConstants.apiurl+'mssql:SelectionList'
            },
            sorters: {
                property: 'selName'
            }
        }, cfg)]);
    }
});