Ext.define('DM2.store.AddressTags', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Tag'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'AddressTags',
            model: 'DM2.model.Tag',
            data: [
                {
                    idTag: '10',
                    tag: 'address1'
                },
                {
                    idTag: '11',
                    tag: 'address2'
                }
            ]
        }, cfg)]);
    }
});