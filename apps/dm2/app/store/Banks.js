Ext.define('DM2.store.Banks', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Bank',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Banks',
            autoLoad: false,
            model: 'DM2.model.Bank',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: DM2.view.AppConstants.apiurl+'mssql:Bank',
                reader: {
                    type: 'json'
                }
            }
        }, cfg)]);
    }
});