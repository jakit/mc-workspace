Ext.define('DM2.store.PPPList', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.PPPList',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'PPPList',
            model: 'DM2.model.PPPList',
            proxy: {
                type: 'rest',
                noCache: false,
                //url: DM2.view.AppConstants.apiurl+'Rppp',
				url: DM2.view.AppConstants.apiurl+'mssql:PPP',
				reader: {
                    type: 'json'/*,
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
								var finaldesc = "";														  
                                for(var i=0; i < item.desc.length; i++){
									finaldesc = finaldesc+item.desc[i].pppdesc+"<br/>";
								}
								item.finaldesc = finaldesc;
								newdata.push(item);								
                            });
							console.log(newdata);
                            return newdata;
                        },
                        scope: this
                    }*/
                }
            }/*,
            sorters: {
                property: 'selName'
            }*/
        }, cfg)]);
    }
});