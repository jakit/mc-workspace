Ext.define('DM2.store.DocsTemp', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.Document'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'DocsTemp',
            model: 'DM2.model.Document'
        }, cfg)]);
    }
});