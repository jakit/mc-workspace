Ext.define('DM2.store.DealStatus', {
    extend: 'Ext.data.Store',

    requires: [
        'DM2.model.DealStatus'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'DealStatus',
            model: 'DM2.model.DealStatus',
            data: [
                {
                    idStatus: '1',
                    status: 'CL',
					statusFullName:'Closed'
                },
                {
                    idStatus: '2',
                    status: 'CX',
					statusFullName:'CX'
                },
                {
                    idStatus: '3',
                    status: 'PR',
					statusFullName:'PR'
                },
                {
                    idStatus: '4',
                    status: 'AC',
					statusFullName:'AC'
                },
                {
                    idStatus: '5',
                    status: 'IP',
					statusFullName:'IP'
                }
            ]
        }, cfg)]);
    }
});