Ext.define('DM2.model.LoanSchedule', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'idLoanSched',

    fields: [
        {
			type: 'int',
            name: 'idLoanSched'
        },
        {
            name: 'idLoan'
        },
        {
            name: 'orderID'
        },
        {
            name: 'months'
        },
        {
            name: 'rateType'
        },
		{
            name: 'rate'
        },
		{
            name: 'percentSpread'
        },
		{
            name: 'indexSpread'
        },
		{
            name: 'startMonth'
        },
		{
            name: 'endMonth'
        }
    ]
});