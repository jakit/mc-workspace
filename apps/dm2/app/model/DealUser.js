Ext.define('DM2.model.DealUser', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idDeal_has_User'
        },
		{
            name: 'idDealxUser'
        },
        {
            name: 'dealid'
        },
        {
            name: 'user'
        },
        {
            name: 'role'
        },
        {
            name: 'active'
        },
        {
            name: 'assigner'
        },
        {
            name: 'assigndate'
        },
        {
            name: 'Role_idRole'
        },
        {
            name: 'csrid'
        },
		{
            name: 'commissionPercent'
        }
    ]
});