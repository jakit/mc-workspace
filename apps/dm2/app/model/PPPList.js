Ext.define('DM2.model.PPPList', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idPPP'
        },
        {
            name: 'pppValue'
        },{
			name: 'finaldesc'
		}
    ]
});