Ext.define('DM2.model.PropertyMaster', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],
	
	idProperty: 'idPropertyMaster',
	
    fields: [
        {
            name: 'idPropertyMaster'
        },
        {
            name: 'PropertyMasterCodeSource'
        },
        {
            name: 'APN'
        },
        {
            name: 'State'
        },
        {
            name: 'Township'
        },
        {
            name: 'Municipality'
        },
        {
            name: 'Boro'
        },
        {
            name: 'Block'
        },
        {
            name: 'Lot'
        },
        {
            name: 'StreetNumber'
        },
        {
            name: 'HighStreetNumber'
        },
        {
            name: 'StreetName'
        },
        {
            name: 'ZipCode'
        },
        {
            name: 'BuildingName'
        },
        {
            name: 'CurrentAddedValue'
        },
        {
            name: 'CurrentAVTAssess'
        },
        {
            name: 'FullValue'
        },
        {
            name: 'BuildingClass'
        },
        {
            name: 'Zoning'
        },
        {
            name: 'Owner'
        },
        {
            name: 'FrontLot'
        },
        {
            name: 'LotDepth'
        },
        {
            name: 'BuildingFront'
        },
        {
            name: 'BuildingDepartment'
        },
        {
            name: 'LandArea'
        },
        {
            name: 'Buildings'
        },
        {
            name: 'Stories'
        },
        {
            name: 'ResidentialUnits'
        },
        {
            name: 'CommUnits'
        },
        {
            name: 'Units'
        },
        {
            name: 'SquareFootage'
        },
        {
            name: 'NumberOfBedrooms'
        },
        {
            name: 'TotalAccumulatedLoanAmount'
        },
        {
            name: 'PropertyTax'
        },
        {
            name: 'SalePrice'
        },
        {
            name: 'SaleDate'
        },
        {
            name: 'LastKnownLender'
        },
        {
            name: 'Bank'
        },
        {
            name: 'idBank'
        },
        {
            name: 'LastKnownMortgageDate'
        },
        {
            name: 'LastKnownLendee'
        },
        {
            name: 'ManualBankUpate'
        },
        {
            name: 'UnitNumber'
        },
        {
            name: 'idCondo'
        },
        {
            name: 'YearBuilt'
        },
        {
            name: 'EstimatedYearBuilt'
        },
        {
            name: 'UpdateDate'
        },
        {
            name: 'StandardStreet'
        },
        {
            name: 'StandardPrefixStreet'
        },
        {
            name: 'StandardStreetName'
        },
		{
            name: 'StandardCity'
        },
        {
            name: 'AddStandardStatus'
        },
        {
            name: 'AKAList'
        },
        {
            name: 'lot2'
        },
        {
            name: 'CSRName'
        },
		{
            name: 'lastDealID'
        },
        {
            name: 'lastPropertyID'
        },
        {
            name: 'lastPrimaryCSRInitials'
        },
        {
            name: 'numberOfBrothers'
        },
		{
            name: 'hasDeal'
        },
        {
            name: 'lastDealStatus'
        }
    ]
});