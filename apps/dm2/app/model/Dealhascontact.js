Ext.define('DM2.model.Dealhascontact', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idContact'
        },
		{
            name: 'contactid'
        },
		{
			name: 'idUserxCont'
		},
        {
            name: 'contactType'
        },
        {
            name: 'firstName'
        },
        {
            name: 'lastName'
        },
		{
            name: 'name'
        },
        {
            name: 'fullName'
        },
        {
            name: 'title'
        },
        {
            name: 'companyName',
			type: 'string'
        },
        {
            name: 'position'
        },
		{
            name: 'phone_1'
        },
		{
            name: 'phone'
        },
		{
            name: 'faxNumber'
        },
		{
            name: 'fax'
        },
        {
            name: 'officePhone_1'
        },
        {
            name: 'officePhone_2'
        },
        {
            name: 'homePhone_1'
        },
        {
            name: 'homePhone_2'
        },
        {
            name: 'mobilePhone_1'
        },
        {
            name: 'mobilePhone_2'
        },
        {
            name: 'officeFax'
        },
        {
            name: 'homeFax'
        },
        {
            name: 'pager'
        },
        {
            name: 'address'
        },
		{
            name: 'address1'
        },
		{
            name: 'email'
        },
        {
            name: 'email_1'
        },
        {
            name: 'email_2'
        },
        {
            name: 'city'
        },
        {
            name: 'state'
        },
        {
            name: 'zipCode'
        },
		{
            name: 'isMyContact'
        }
    ]
});