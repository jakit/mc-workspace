Ext.define('DM2.model.Deal', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'idDeal',

    fields: [{
        name: 'dealid'
    },
	{
        name: 'DM-dealNo'
    },
	{
        name: 'LT-dealNo'
    },
	{
        name: 'idDeal'/*,
		convert: function(v, rec) {
            //if(rec.get('loandate')){
                var magicDealID = rec.get('magicDealID');
                return magicDealID;
            //}
        }*/
    },
	{
        name: 'magicDealID'/*,
		convert: function(v, rec) {
            //if(rec.get('loandate')){
                var idDeal = rec.get('idDeal');
                return idDeal;
            //}
        }*/
    },
    {
        name: 'name'
    },
	{
        name: 'dealName'
    },
    {
        name: 'street_no'
    },
	{
        name: 'streetNumber'
    },
    {
        name: 'street'
    },
	{
        name: 'streetName'
    },
    {
        name: 'city'
    },
    {
        name: 'state'
    },
    {
        name: 'maincontact'
    },
	{
        name: 'mainContact'
    },
    {
        name: 'status'
    },
    {
        name: 'startdate'
    },
	{
        name: 'closedate'
    },
    {
        name: 'bank'
    },
	{
        name: 'bankName'
    },
    {
        name: 'loanamt'
    },
	{
        name: 'loanAmt'
    },
    {
        name: 'loandate'
    },
	{
        name: 'loanDate'
    },
    {
        convert: function(v, rec) {
            if(rec.get('loandate')){
                var loandtarr = rec.get('loandate').split("T");
                return loandtarr[0];
            }
        },
        name: 'loandisplaydate'
    },
    {
        name: 'broker'
    },
	{
        name: 'userList'
    },
    {
        name: 'dealtype'
    },
    {
        convert: function(v, rec) {
            if(rec.get('dealdate'))
            {
                var dealdtarr = rec.get('dealdate').split("T");
                return dealdtarr[0];
            }
        },
        name: 'dealdisplaydate'
    },
    {
        name: 'allcontacts'
    },
	{
        name: 'allContacts'
    },
    {
        name: 'allPropertyNames'
    },
	{
		name:'idPropertyMaster'
	},
	{
		name:'readable'
	},
	{
		name:'modifiable'
	},
	{
		name:'statusUserName'
	},{
		name:'statusdate'
	},{
		name:'buyer'
	},{
		name:'seller'
	},{
		name:'covered'
	},{
		name:'companyName'
	}]
});