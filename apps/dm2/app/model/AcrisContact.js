Ext.define('DM2.model.AcrisContact', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idAcris'
        },
		{
            name: 'partyType'
        },
		{
            name: 'partyName'
        },
        {
            name: 'addressLine1'
        },
        {
            name: 'addressLine2'
        },
        {
            name: 'addressCounty'
        },
        {
            name: 'addressCity'
        },
        {
            name: 'addressState'
        },
        {
            name: 'addressZip'
        }
    ]
});