Ext.define('DM2.model.DealHasProperty', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idDeal_has_Property'
        },
		{
            name: 'idDealxProp'
        },
        {
            name: 'Deal_idDeal'
        },
		{
            name: 'idDeal'
        },
        {
            name: 'Property_idProperty'
        },
		{
            name: 'idProperty'
        },
        {
            name: 'isPrimary'
        },
        {
            name: 'income'
        },
        {
            name: 'expenses'
        },
        {
            name: 'estimatedLoanSize'
        },
        {
            name: 'vacancyAmt'
        },
        {
            name: 'percentSold'
        },
        {
            name: 'percentRented'
        },
        {
            name: 'mortgageType'
        },
        {
            name: 'assessedValueYear'
        },
        {
            name: 'assessedValue'
        },
        {
            name: 'transactionValue'
        },
        {
            name: 'yearAcquired'
        },
        {
            name: 'propertyType'
        },
        {
            name: 'numberOfBuildings'
        },
        {
            name: 'numberOfStories'
        },
        {
            name: 'numberOfApartments'
        },
        {
            name: 'numberOfRooms'
        },
        {
            name: 'numberOfCommercialApartments'
        },
        {
            name: 'commercialSquareFeet'
        },
        {
            name: 'environmentalDate'
        },
        {
            name: 'appraisalDate'
        },
        {
            name: 'appraiser'
        },
        {
            name: 'commitExpirationDate'
        },
        {
            name: 'commitmentNumber'
        },
        {
            name: 'commitmentDate'
        },
        {
            name: 'commitmentCheck'
        },
        {
            name: 'commitmentCheckDate'
        },
        {
            name: 'commitmentCheckAmount'
        },
        {
            name: 'loanNumber'
        },
        {
            name: 'elevatorWalk'
        },
		{
			name: 'idSetup'
		}
    ]
});