Ext.define('DM2.model.Quote', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idQuote'
        },
        {
            name: 'quote'
        },
        {
            name: 'dealid'
        },
        {
            name: 'selected'
        },
		{
			convert: function(v, rec) {
				if(rec.get('selected')=="S"){
					return true;
				} else if(rec.get('selected')=="Q"){
					return false;
				}
			},
            name: 'selecteddis'
        },
        {
            name: 'bank'
        },
		{
            name: 'fullName'
        },
        {
            name: 'loantype'
        },
		{
            name: 'quotetype'
        },
        {
            name: 'term'
        },
        {
            name: 'receivedamount', type: 'int'
        },
		{
            name: 'amountRequested'
        },
		{
            name: 'amountQuoted'
        },
        {
            name: 'ratetype'
        },
        {
            name: 'spread'
        },
        {
            name: 'index'
        },
        {
            name: 'indexvalue'
        },
        {
            name: 'amortterm'
        },
        {
            name: 'io'
        },
        {
            name: 'ppp'
        },
		{
            name: 'pppWindow'
        },
        {
            name: 'loanid'
        },
        {
            name: 'bankid'
        },
        {
            name: 'hasCommit'
        },
        {
            name: 'sendmod'
        },
		{
			convert: function(v, rec) {
				if(rec.get('hasOption')=="N"){
					return false;
				}else if(rec.get('hasOption')=="Y"){
					return true;
				}
			},
            name: 'hasOption'
        }
    ]
});