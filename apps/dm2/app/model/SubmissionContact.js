Ext.define('DM2.model.SubmissionContact', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idSubmission_has_Contact'
        },
		{
            name: 'idSubmissionxCont'
        },
        {
            name: 'Submission_idSubmission'
        },
		{
            name: 'idSubmission'
        },
        {
            name: 'Contact_idContact'
        },
		{
            name: 'idContact'
        }
    ]
});