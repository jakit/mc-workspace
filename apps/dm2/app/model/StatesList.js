Ext.define('DM2.model.StatesList', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'display'
        },
        {
            name: 'val'
        }
    ]
});