Ext.define('DM2.model.ActivityHistory', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idActivityHistory'
        },
        {
            name: 'CSR_idCSR'
        },
        {
            name: 'idActivity'
        },
        {
            name: 'createDate'
        },
        {
            name: 'modifyTime'
        },
        {
            name: 'idDeal'
        }
    ]
});