Ext.define('DM2.model.Contact', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        { name: 'dealid' },
        { name: 'contactid' },
        { name: 'title' },
        { name: 'firstName' },
        { name: 'lastName' },
        { name: 'fullName' },
        { name: 'company' },
        { name: 'contactType' },
        { name: 'companyName' },
		{ name: 'displayName'},
        { name: 'position' },
        { name: 'address' },
        { name: 'city' },
        { name: 'state' },
        { name: 'zip' },
        { name: 'officePhone_1' },
        { name: 'officePhone_2' },
        { name: 'faxNumber'},
        { name: 'email_1'},
        { name: 'email_2'},
        { name: 'iddeal_has_contact' },
		{ name: 'idDealxCont' },
        { name: 'isprimary' },
        { name: '@metadata'},
		{ name: 'idproperty_has_contact' },
		{ name: 'idPropxCont' },
		{ name: 'idBankxCont' },
		{ name: 'idSubmissionxCont'},
		{ name: 'idSubxCont'},
		{ name: 'idLoan'},
		{ name: 'idContact'},
		{ name: 'Mast_MasterID'},
		{ name: 'name'},
		{ name: 'Mast_MasterID'},
		{ name: 'type'},
        { name: 'phone'},
        { name: 'email'},
		{ name: 'faxNumber'}
        /*{ name: 'assoctype'},
        { name: 'assocdesc' },
        { name: 'bankid' },
        { name: 'idBank_has_contact' },
        { name: 'submissionid'},
         */
    ]
});