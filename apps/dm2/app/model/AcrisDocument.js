Ext.define('DM2.model.AcrisDocument', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idAcris'
        },
		{
            name: 'APN'
        },
		{
            name: 'dateFileCreated'
        },
        {
            name: 'CRFN'
        },
        {
            name: 'recordBorough'
        },
        {
            name: 'docType'
        },
        {
            name: 'docDescription'
        },
        {
            name: 'docDate'
        },
        {
            name: 'docAmount'
        },
        {
            name: 'recordedDate'
        },
        {
            name: 'modifiedDate'
        },
        {
            name: 'realYear'
        },
        {
            name: 'RealNBR'
        },
        {
            name: 'realPG'
        }
    ]
});