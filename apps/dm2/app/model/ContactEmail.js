Ext.define('DM2.model.ContactEmail', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'idEmail',

    fields: [
        {
			type: 'int',
            name: 'idEmail'
        },
        {
            name: 'idContact'
        },
        {
            name: 'tag'
        },
        {
            name: 'email'
        },
        {
            name: 'isPrimary'
        }
    ]
});