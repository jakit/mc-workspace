Ext.define('DM2.model.MasterLoan', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'dealid'
        },
        {
            name: 'masterID'
        },
		{
            name: 'BankName',
			convert: function(v, rec) {
				var dealMakerBankName = rec.get('DealMakerBankName');
				if(dealMakerBankName==null || dealMakerBankName==""){
					var FeedBankName = rec.get('FeedBankName');
					return FeedBankName;
				} else {
					//console.log("Return DealMakerBankName"+v);
					return dealMakerBankName;
				}
			}
        },
        {
            name: 'DealMakerBankName'
        },
        {
            name: 'FeedBankName'
        },
        {
            name: 'MortgageAmount'
        },
        {
            name: 'PropertyTax'
        },
        {
            name: 'SaleDate'
        },
        {
            name: 'SalePrice'
        },
        {
            name: 'CurrentAVTAssess'
        },
        {
            name: 'FullValue'
        },
        {
            name: 'Owner'
        },
        {
            name: 'LastKnownMortgageDate'
        },
        {
            name: 'PropertyMasterCodeSource'
        },
		{
            name: 'Mast_SourceMasterCod'
        },
		{
            name: 'Mast_BldgClass'
        },
        {
            name: 'BuildingClassDescription'
        }
    ]
});