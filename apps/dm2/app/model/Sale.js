Ext.define('DM2.model.Sale', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'dealid'
        },
        {
            name: 'idSale'
        },
        {
            name: 'saleType'
        },
		{
			convert: function(v, rec) {
				var saleType = rec.get('saleType');
				var retVal=null;
				if(saleType=="S"){
					retVal = "Sell";
				} else if(saleType=="B"){
					retVal = "Buy";
				} else if(saleType=="C"){
					retVal = "Consulting";
				} else if(saleType=="O"){
					retVal = "OffMarket";
				}
				return retVal;
			},
			name: 'saleTypeDisplay'
		},
        {
            name: 'createdby'
        },
        {
            name: 'price'
        },
		{
            name: 'commissionPercentage'
        },
		{
            name: 'commissionNumber'
        },
        {
            name: 'createDateTime'
        },
        {
            name: 'modifiedby'
        },
        {
            name: 'modifyDateTime'
        },
		{
            name: 'modifiedDateTime',
			type:'date',
			convert: function(v, rec){
				if(v!=null){
					var dtarr = v.split('T');
					var timearr = dtarr[1].split('.');
					var st = dtarr[0]+" "+timearr[0];
					//console.log(st);
					var datearr = dtarr[0].split("-");
					var timeonlyarr = timearr[0].split(":");
					//console.log(new Date(datearr[0],datearr[1],datearr[2]-1,timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]));
					return new Date(datearr[0],datearr[1]-1,datearr[2],timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]);
				}else{
					return v;
				}
			}
        },
		{
            name: 'commissionType'
        },
		{
			convert: function(v, rec) {
				var commType = rec.get('commissionType');
				var retVal="";
				if(commType=="P"){
					retVal = "Percentage";
				} else if(commType=="F"){
					retVal = "Flat";
				} else if(commType=="S"){
					retVal = "Structure";
				}
				return retVal;
			},
			name: 'commissionTypeDisplay'
		},
		{
			convert: function(v, rec) {
				var commType = rec.get('commissionType');
				var retVal="";
				if(commType=="P"){
					retVal = rec.get('percentCommission');
				} else if(commType=="F"){
					retVal = rec.get('flatAmountCommission');
				} else if(commType=="S"){
					retVal = null;
				}
				return retVal;
			},
			name: 'commission'
		},
		{
			convert: function(v, rec) {
				var commType = rec.get('commissionType');
				var retVal="";
				if(commType=="P"){
					percentVal = rec.get('percentCommission');
					priceVal = rec.get('price');
					if(priceVal==null || priceVal==0){
						retVal = null;  
					} else {						
						retVal = (priceVal*percentVal)/100;
					}
				} else if(commType=="F"){
					retVal = null;
				} else if(commType=="S"){
					retVal = null;
				}
				return retVal;
			},
			name: 'commissionAmt'
		},
		{
			name: 'calcCommission'
		},
		{
			name: 'bid'
		}
    ]
});