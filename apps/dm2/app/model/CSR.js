Ext.define('DM2.model.CSR', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idCSR'
        },
        {
            name: 'lastName'
        },
        {
            name: 'firstName'
        },
        {
            name: 'userName'
        },
        {
            name: 'initials'
        },
        {
            name: 'position'
        },
        {
            name: 'address'
        },
        {
            name: 'city'
        },
        {
            name: 'state'
        },
        {
            name: 'active'
        },
        {
            name: 'email'
        }
    ]
});