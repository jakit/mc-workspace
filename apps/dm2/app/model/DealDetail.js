Ext.define('DM2.model.DealDetail', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'idDeal',

    fields: [{
        name: 'idDeal'
    },
    {
        name: 'magicDealID'
    },
    {
        name: 'dealName'
    },
    {
        name: 'status'
    },
    {
        name: 'statusUser'
    },
    {
        name: 'statusDate'
    },
    {
        name: 'startDate'
    },
    {
        name: 'endDate'
    },
    {
        name: 'billDate'
    },
    {
        name: 'closeDate'
    },
    {
        name: 'mainContactID'
    },
    {
        name: 'managerID'
    },
    {
        name: 'csrString'
    },
    {
        name: 'department'
    },
    {
        name: 'source'
    },
    {
        name: 'fileNumber'
    },
    {
        name: 'stage'
    },
    {
        name: 'step'
    },
	{
        name: 'createDate'
    },
	{
        name: 'modifyDate'
    },
	{
        name: 'modifyTime'
    },
	{
        name: 'createUser'
    },
	{
        name: 'modifyUser'
    },
	{
        name: 'dealType'
    },
	{
        name: 'lastDeal'
    },
	{
        name: 'deleted'
    },
	{
        name: 'office'
    },
	{
        name: 'existingNotes'
    },
	{
        name: 'fullName'
    },
	{
        name: 'aggregateDealType'
    },
	{
        name: 'statusSetMode'
    },
	{
        name: 'modifyDateTime'
    },
	{
        name: 'DealDetails'
    },
	{
        name: 'PropertiesPerDeal'
    },
	{
        name: 'ContactPerDeal'
    },
	{
        name: 'MasterLoansPerDeal'
    },
	{
        name: 'ExistingLoanPerDeal'
    },
	{
        name: 'QuotePerDeal'
    },
	{
        name: 'NotePerDeal'
    },
	{
        name: 'ActivityHistory'
    },
	{
        name: 'DocsPerDeal'
    },
	{
        name: 'UsersPerDeal'
    },
	{
        name: 'StatusHistoryPerDeal'
    }]
});