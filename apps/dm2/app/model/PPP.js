Ext.define('DM2.model.PPP', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'year'
        },
        {
            name: 'ppptype'
        },
        {
            name: 'rate'
        }
    ]
});