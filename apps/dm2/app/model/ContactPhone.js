Ext.define('DM2.model.ContactPhone', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Integer'
    ],

    idProperty: 'idPhone',

    fields: [
        {
            type: 'int',
            name: 'idPhone'
        },
        {
            name: 'idContact'
        },
        {
            name: 'tag'
        },
        /*{
            name: 'number'
        },*/
		{
            name: 'phoneNumber'
        },
        {
            name: 'isPrimary'
        }
    ]
});