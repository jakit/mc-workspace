Ext.define('DM2.model.DealPrimaryDetail', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'dealid',

    fields: [{
        name: 'dealid'
    },
    {
        name: 'asof'
    },
	{
        name: 'statusDate'
    },
    {
        name: 'dealname'
    },
    {
        name: 'amount'
    },
    {
        name: 'mtgtype'
    },
    {
        name: 'filenumber'
    },
    {
        name: 'statusSetMode'
    },
    {
        name: 'statusSetBy'
    },
    {
        name: 'status'
    },
    {
        name: 'office'
    },
    {
        name: 'PrimaryBroker'
    },
    {
        name: 'CSRList'
    },
	{
        name: 'userList'
    },
	{
        name: 'scheduledCloseDate'
    },
	{
        name: 'closeDate'
    },
	{
        name: 'billDate'
    },
	{
        name: 'endDate'
    },
	{
        name: 'saleType'
    }]
});