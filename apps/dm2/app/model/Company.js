Ext.define('DM2.model.Company', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        { name: 'idCompany' },
        { name: 'companyName' },
        { name: 'address' },
        { name: 'city' },
        { name: 'state' },
        { name: 'office' },
        { name: 'phone1' },
        { name: 'phone2' },
        { name: 'email1'},
        { name: 'email2'}
    ]
});