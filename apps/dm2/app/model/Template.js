Ext.define('DM2.model.Template', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idTemplate'
        },
        {
            name: 'description'
        },
		{
            name: 'name'
        },
        {
            name: 'url'
        },
		{
            name: 'uri'
        },
        {
            name: 'extension'
        }
    ]
});