Ext.define('DM2.model.SelectionList', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idSelList'
        },
        {
            name: 'selName'
        },
        {
            name: 'selType'
        }
    ]
});