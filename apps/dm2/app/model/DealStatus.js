Ext.define('DM2.model.DealStatus', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idStatus'
        },
        {
            name: 'status'
        },
		{
            name: 'statusFullName'
        }
    ]
});