Ext.define('DM2.model.ContactFax', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'idFax',

    fields: [
        {
            name: 'idFax'
        },
        {
            name: 'idContact'
        },
        {
            name: 'tag'
        },
        {
            name: 'faxNumber'
        },
        {
            name: 'isPrimary'
        }
    ]
});