Ext.define('DM2.model.Submission', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idLoan'
        },
        {
            name: 'dealid'
        },
        {
            name: 'bankid'
        },
        {
            name: 'contactid'
        },
        {
            name: 'submitdate'
        },
        {
            name: 'userName'
        },
        {
            name: 'submitmethod'
        },
        {
            name: 'submitnote'
        },
        {
            name: 'shortName'
        },
        {
            name: 'recvdate'
        },
        {
            name: 'contactname'
        },
        {
            name: 'fullName'
        },
		{
			name : 'amountRequested'
		}
    ]
});