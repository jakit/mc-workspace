Ext.define('DM2.model.Office', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idOffice'
        },
        {
            name: 'code'
        },
		{
            name: 'name'
        },
		{
            name: 'parentName'
        },
		{
            name: 'address'
        },
		{
            name: 'city'
        },
		{
            name: 'state'
        },
		{
            name: 'zip'
        },
		{
            name: 'phone'
        },
		{
            name: 'fax'
        },
		{
            name: 'url'
        },
		{
            name: 'email'
        },
		{
            name: 'display',
			convert: function(v, rec) {
				var display = rec.get('code');
				return display;
			}
        },
		{
            name: 'val',
			convert: function(v, rec) {
				var val = rec.get('code');
				return val;
			}
        }
    ]
});