Ext.define('DM2.model.Document', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'dealid'
        },
		{
            name: 'idDeal'
        },
		{
            name: 'idProperty'
        },
        {
            name: 'docid'
        },
        {
            name: 'name'
        },
        {
            name: 'size'
        },
        {
            name: 'desc'
        },
        {
            name: 'clsfctn'
        },
        {
            name: 'createdby'
        },
        {
            name: 'createddate'
        },
		{
            name: 'createDateTime'
        },
        {
            name: 'modifiedBy'
        },
		{
            name: 'modifyUserName'
        },
        {
            name: 'modifydate',
			convert: function(v, rec){
				//console.log(Ext.isString(v));
				//console.log(Ext.typeOf(v));
//								if (typeof myVar === 'string')
				if(v!=null && Ext.isString(v)){
					var dtarr = v.split('T');
					var timearr = dtarr[1].split('.');
					var st = dtarr[0]+" "+timearr[0];
					//console.log(st);
					var datearr = dtarr[0].split("-");
					var timeonlyarr = timearr[0].split(":");
					//console.log(new Date(datearr[0],datearr[1],datearr[2]-1,timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]));
					return new Date(datearr[0],datearr[1]-1,datearr[2],timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]);
				}else{
					return v;
				}
			}
        },
		{
            name: 'modifyDateTime',
			convert: function(v, rec){
				if(v!=null && Ext.isString(v)){
					var dtarr = v.split('T');
					var timearr = dtarr[1].split('.');
					var st = dtarr[0]+" "+timearr[0];
					//console.log(st);
					var datearr = dtarr[0].split("-");
					var timeonlyarr = timearr[0].split(":");
					//console.log(new Date(datearr[0],datearr[1],datearr[2]-1,timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]));
					return new Date(datearr[0],datearr[1]-1,datearr[2],timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]);
				}else{
					return v;
				}
			}
        },
        {
            name: 'assoctype'
        },
		{
			name: 'type'
		},
        {
            name: 'url'
        },
        {
            name: 'assocdesc'
        },
        {
            name: 'modifyUser'
        },
        {
            name: 'idContact'
        },
        {
            name: 'submissionid'
        },
        {
            name: 'selected'
        },
        {
            name: 'idDocument'
        },
        {
            name: 'description'
        },
        {
            name: 'ext'
        },
        {
            name: 'format'
        },
        {
            name: 'classification'
        },
        {
            name: 'dateModify'
        },
        {
            name: 'fileobj'
        },
        {
            name: 'uploadPercent'
        },
        {
            name: 'isglobal'
        },
        {
            name: 'uploadmode'
        },
        {
            name: 'filesize'
        },
        {
            name: 'bank'
        },
        {
            name: 'amount'
        },
        {
            name: 'receivedBack'
        },
		{
			name: 'idSubxDoc'
		},
		{
			name: 'username'
		},
		{
			name: 'Mast_MasterID'
		},
		{
			name: 'content'
		},
		{
			name: 'idGSubxDoc'
		}
    ]
});