Ext.define('DM2.model.Activity', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idActivity'
        },
        {
            name: 'description'
        },
        {
            name: 'status'
        },
        {
            name: 'mandatory'
        },
        {
            name: 'handler_fname'
        },
        {
            name: 'iconname'
        },
		{
            name: 'track'
        },
		{
            name: 'subTrack'
        }
    ]
});