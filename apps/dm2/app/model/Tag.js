Ext.define('DM2.model.Tag', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idTag'
        },
        {
            name: 'tag'
        },
		{
			name: 'label'
		}
    ]
});