Ext.define('DM2.model.IndexSpread', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idIndexSpread'
        },
        {
            name: 'indexSpreadValue'
        }
    ]
});