Ext.define('DM2.model.ItemList', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'itemId',
			mapping: function(data) {				
				if(data.idMembers){
					return data.idMembers;
				} else if(data.idGroup){
					return data.idGroup;
				}
			}
        },
        {
            name: 'itemValue',
			mapping: function(data) {
				if(data.userName){
					return data.userName;
				} else if(data.groupName){
					return data.groupName;
				}
			}
        }
    ]
});