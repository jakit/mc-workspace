Ext.define('DM2.model.QuoteOption', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
			name:'idLoanOption'
		},{
            name: 'idPPP'
        },{
            name: 'idLoan'
        },{
			name: 'optAmount'
		},{
			name: 'quoteRate'
		},{
			name: 'rateType'
		},{
			name: 'indexSpread'
		},{
			name: 'percentSpread'
		},{
			name: 'term'
		},{
			name: 'interestOnly'
		},{
			name: 'optionFloor'
		},{
			name: 'startDate'
		},{
			name: 'monthEnd'
		},{
			name: 'pppDescription'
		},{
			name: 'pppType'
		},{
			name: 'PPPOptionWindow'
		},{
			name: 'isSelected'
		},{
			name: 'amortization'
		},{
			name: 'optionFee'
		}
    ]
});