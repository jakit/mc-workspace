Ext.define('DM2.model.Allproperty', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idProperty'
        },
        {
            name: 'county'
        },
        {
            name: 'state'
        },
        {
            name: 'city'
        },
        {
            name: 'streetName'
        },
        {
            name: 'streetNumber'
        },
        {
            name: 'zipCode'
        },
        {
            name: 'address'
        },
        {
            name: 'type'
        },
        {
            name: 'yearBuilt'
        },
        {
            name: 'buildingClass'
        },
        {
            name: 'propertyType'
        },
        {
            name: 'stories'
        },
        {
            name: 'units'
        },
        {
            name: 'office'
        },
        {
            name: 'yearAcquired'
        },
        {
            name: 'yearLotAcquired'
        },
        {
            name: 'originalCost'
        },
        {
            name: 'originalLotCost'
        },
        {
            name: 'lienAmount'
        },
        {
            name: 'lotValue'
        },
        {
            name: 'improvementsCost'
        },
        {
            name: 'totalValueCosts'
        }
    ]
});