Ext.define('DM2.model.CommSchedule', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'idCommissionSched',

    fields: [
        {
			type: 'int',
            name: 'idCommissionSched'
        },
        {
            name: 'idSale'
        },
        {
            name: 'orderID'
        },
        {
            name: 'months'
        },
		{
            name: 'percentCommission'
        },
		{
            name: 'modifyDateTime'
        },
		{
            name: 'modifyUserName'
        },
		{
            name: 'createUserName'
        },
		{
            name: 'createDateTime'
        },
		{
            name: 'startAmount'
        },
		{
            name: 'endAmount'
        }
    ]
});