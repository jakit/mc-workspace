Ext.define('DM2.model.Classification', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'selectionDesc'
        },
		{
            name: 'idSelectionList'
        },
		{
			name:'text'
		}
    ]
});