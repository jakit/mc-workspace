Ext.define('DM2.model.SaleBid', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idBid'
        },
        {
            name: 'idSale'
        },
        {
            name: 'targetCloseDate'
        },
		{
            name: 'idBuyerName'
        },
		{
			name: 'idBuyer'
		},
		{
            name: 'Buyer'
        },
        {
            name: 'salePrice'
        },
		{
            name: 'companyName'
        },
        {
            name: 'createDateTime'
        },
        {
            name: 'modifyDateTime',
			type:'date',
			convert: function(v, rec){
				if(v!=null){
					var dtarr = v.split('T');
					var timearr = dtarr[1].split('.');
					var st = dtarr[0]+" "+timearr[0];
					//console.log(st);
					var datearr = dtarr[0].split("-");
					var timeonlyarr = timearr[0].split(":");
					//console.log(new Date(datearr[0],datearr[1],datearr[2]-1,timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]));
					return new Date(datearr[0],datearr[1]-1,datearr[2],timeonlyarr[0],timeonlyarr[1],timeonlyarr[2]);
				}else{
					return v;
				}
			}
        },
		{
			name: 'selected'
		}
    ]
});