Ext.define('DM2.model.Underwriting', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Date'
    ],

    fields: [
        {
            name: 'idUnderwriting'
        },
        {
            name: 'idDealxProp'
        },
        {
            name: 'idUnderwritingActions'
        },
		{
            name: 'Contact_idContact'
        },
		{
            name: 'idContact'
        },
		{
            name: 'firstName'
        },
		{
            name: 'lastName'
        },
        {
            type: 'date',
			dateWriteFormat : 'm-d-Y',
            name: 'scheduledActivityDate'
        },
        {
            type: 'date',
			dateWriteFormat : 'm-d-Y',
            name: 'activityCompleteDate'
        },
        {
            name: 'activityComplete'
        },
		{
            type: 'date',
            name: 'createDate'
        },
		{
            type: 'date',
            name: 'modifyDate'
        }
    ]
});