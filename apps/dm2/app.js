// START - place this block of code before Ext.application in app.js
var appMode;
        
try{
    if(appTimestamp) {
        appMode = "production";
        console.log("appTimestamp: " + appTimestamp);
        //console.log("majorVersion: " + majorVersion);
        //console.log("minorVersion: " + minorVersion);                
    }
} catch(e) {
    appMode = "development";
}
finally {
    console.log("appMode: " + appMode);
}
// END
Ext.BLANK_IMAGE_URL = "resources/images/icons/download.gif";
Ext.onReady(function(){
	console.log("On Ready");
	Ext.getBody().on("contextmenu", function(e, t, eOpts){console.log("On Body Right Click");e.preventDefault();});
});//this is valid 
Ext.application({
    name: 'DM2',
    requires: [
        'Ext.Loader',
        'Ext.grid.plugin.FileDrop'
    ],
    models: [
        'Deal',
        'Property',
        'Contact',
        'Note',
        'Document',
        'Dealhascontact',
        'Allproperty',
        'Activity',
        'ContactType',
        'Loan',
        'Prenote',
        'Rentroll',
        'ActivityHistory',
        'ContactPhone',
        'ContactEmail',
        'ContactMisc',
        'ContactAddress',
        'Quote',
        'DealUser',
        'Tag',
        'Bank',
        'SearchLocation',
        'Role',
        'PPP',
        'Submission',
        'DealHasProperty',
        'Template',
        'SubmissionContact',
        'MasterLoan',
        'PropertyMaster',
        'ContactFax',
        'UnderwrittingAction',
        'Underwriting',
        'Classification',
        'Permission',
        'DealStatus',
        'CSR',
        'DealStatusHistory',
        'Office',
        'Propertydeal',
        'Loandeal',
        'SelectionList',
		'DealDetail',
		'DealPrimaryDetail',
		'AcrisDocument',
		'AcrisContact',
		'AcrisNote',
		'PPPList',
		'QuoteOption',
		'StatesList',
		'LoanSchedule',
		'IndexSpread',
		'Sale',
		'FunctionalPermission',
		'SaleBid',
		'Company'
    ],
    stores: [
        'Deals',
        'Properties',
        'Contacts',
        'Notes',
        'Documents',
        'PropertyDocuments',
        'Dealhascontacts',
        'Allproperties',
        'Activities',
        'ContactTypes',
        'DealDetailContacts',
        'ExistingLoans',
        'Quotes',
        'Prenotes',
        'Propertyallbuff',
        'Contactallbuff',
        'Rentrolls',
        'Dealproperty',
        'ActivityHistory',
        'ContactPhones',
		'ContactFaxes',
        'ContactEmails',
        'ContactMiscs',
        'ContactAddress',
        'ContactDocuments',
        'ContactNotes',
        'DealQuotes',
        'DealUsers',
        'PhoneTags',
        'Bankallbuff',
        'SearchLocations',
        'MainContactPhones',
        'EmailTags',
        'AddressTags',
        'Roles',
        'Banks',
        'PPP',
        'BankContacts',
        'BanksSelected',
        'DocsPerSubmission',
        'GenFilesPerSubmission',
        'DealQuotesAll',
        'Templates',
        'Docsall',
        'DocsTemp',
        'GlobalDocsPerSubmission',
        'SubmissionContacts',
        'MasterLoans',
        'DealContactsSendLOI',
        'PropMasterallbuff',
        'ContactAllSelBuff',
        'ContactFax',
        'UnderwrittingActions',
        'Underwriting',
        'Classifications',
        'DealQuotesSelected',
        'SelectedPermission',
        'AllPermission',
        'ApprovalDocs',
        'SendDocsPerDeal',
        'DealActiveUsers',
        'DealStatus',
        'CSR',
        'DealStatusHistory',
        'Office',
        'Loansdeal',
        'Propertiesdeal',
        'DealHistory',
        'MortgageTypeList',
        'PropertyTypeList',
		'ElevatorWalk',
		'RateTypeList',
		'LoanTypeList',
		'RolesStatic',
		'DealDetails',
		'DealPrimaryDetails',
		'SelectionList',
		'PropertyContacts',
		'AcrisDocuments',
		'AcrisContacts',
		'AcrisNotes',
		'PPPList',
		'QuoteOptions',
		'Users',
		'StatesList',
		'LoanSchedule',
		'IndexSpread',
		'ContactAllMySel',
		'Salesallbuff',
		'SaleDealDetails',
		'Sales',
		'FunctionalPermissions',
		'SaleBids',
		'AKAList',
		'SaleDealStatus',
		'Company'
    ],
    views: [
        'MainViewport',
        'DealMenuPanel',
        'DealMenuGridPanel',
        'DealMenuTabPanel',
        'DealNotesGridPanel',
        'DealDocsGridPanel',
        'DealContactsGridPanel',
        'DealInteractionsGridPanel',
        'DealMenuPropertyGridPanel',
        'LoginContainer',
        'NotePanel',
        'NoteOptionMenu',
        'DealDetailsFormPanel',
        'DocOptionMenu',
        'DealGridOptionMenu',
        'DashboardContainer',
        'AddDealContactForm',
        'AddDealPropertyForm',
        'DealDetailsCenterPanel',
        'DealDetailContactMenu',
        'TaskGridPanel',
        'ChangeRoleContactForm',
        'DealDetailPropertiesMenu',
        'ResearchPanel',
        'DefaultWorkAreaPanel',
        'DealDetailResearchPanel',
        'ActivityDetailPanel',
        'PropertyGridPanel',
        'ContactDetailsForm',
        'ContactTabPanel',
        'ContactPhonesGridPanel',
        'ContactEmailsGridPanel',
        'ContactMiscGridPanel',
        'ContactAddressGridPanel',
        'DealDetailExistingLoanFieldset',
        'DealDetailPropertyFieldSet',
        'DealDetailNewLoanFieldset',
        'DealDetailQuotesGridPanel',
        'PropertyGmapWindow',
        'DealDetailActivityMenu',
        'DealDetailQuoteMenu',
        'DealDetailQuotePanel',
        'ContactMenu',
        'BankDetailsForm',
        'PropertyZillowWindow',
        'ContactMenuTab',
        'AddDealUserPanel',
        'DealUserOptionMenu',
        'LoanSizingPanel',
        'BankChooserPanel',
        'BankTabPanel',
        'BankContactsGridPanel',
        'BankContactMenu',
        'AddBankContactForm',
        'BankMenu',
        'LoanSizingDetailsForm',
        'AddSelBankPanel',
        'BankSelectMenu',
        'SubmissionContactsGridPanel',
        'BankDocsGridPanel',
        'GenFilesPerSubmissionGridPanel',
        'QuotesGridPanel',
        'QuoteMenu',
        'QuoteAddBankPanel',
        'SelectLoanPanel',
        'SelectLoanQuotesGridPanel',
        'SelectLoanMenu',
        'LoanSizeGridMenu',
        'FileSubmissionGridMenu',
        'TemplateSelGridPanel',
        'DocsSelGridPanel',
        'DocSelPanel',
        'AddSubmissionDocPanel',
        'EmailFaxSubmission',
        'ContactDocOptionMenu',
        'GlobalDocsSelGridPanel',
        'GlobalDocsGridMenu',
        'AddGlobalDocPanel',
        'GlobalTemplateSelGridPanel',
        'GlobalDocSelRepositoryPanel',
        'GlobalDocsSelRepositoryGridPanel',
        'DealDetailMasterLoanFieldset',
        'SendDocumentPanel',
        'DealDocsSendLOIGridPanel',
        'DealDocsSendLOIGridMenu',
        'CreateNewDealPanel',
        'PropMasterDetailsForm',
        'SubmissionContactGridMenu',
        'DealDetailContactFieldSet',
        'SearchTrigger',
        'Underwriting',
        'UnderwrittingActionForm',
        'AppConstants',
        'ApplicationGenFilesGridPanel',
        'PermissionView',
        'PermissionSelectedGridPanel',
        'PermissionAllGridPanel',
        'DealDetailOptionMenu',
        'Closingview',
        'ApprovalPanel',
        'ApprovalLoanGridPanel',
        'ApprovalGridPanel',
        'GeneralDocMenu',
        'GeneralTemplateSelGridPanel',
        'GeneralDocSelRepositoryPanel',
        'GeneralDocsSelRepositoryGridPanel',
        'GeneralAddDealDocPanel',
        'ClosingModGridPanel',
        'DealItemMenu',
        'CreateNewDealDocsGridPanel',
        'CreateNewDealUserGridPanel',
        'DealModifyView',
        'DealDetailStatusHistoryPanel',
		'ViewMasterDataForm',
		'ListACRISDocuments'
    ],
    controllers: [
        'NoteController',
        'MainController',
        'DealController',
        'DocController',
        'ContactController',
        'DashboardController',
        'TaskController',
        'DealDetailController',
        'ResearchController',
        'DealActivityController',
        'PropertyController',
        'QuoteController',
        'BankController',
        'ReportController',
        'DealUserController',
        'LoanController',
        'SendDocumentController',
        'UnderwritingController',
        'ApplicationController',
        'ReiterateController',
        'PermissionController',
        'ApprovalController',
        'ClosingController',
        'GeneralDocSelController',
        'RecvSignedLOIController',
        'RecvGoodFaithCheckController',
        'AddRentRollExpenseController',
        'SendLOIController',
        'CreateNewDealController',
        'DealModifyController',
        'DealStatusHistoryController',
        'DealHistoryController',
		'BillingController',
		'CreateNewDealFromPropertyController',
		'SalesController',
		'SalesDealDetailController',
		'ReceiveBidController',
		'SelectBidController',
		'SendInvoiceController'
    ],
    autoCreateViewport: 'DM2.view.MainViewport'
});