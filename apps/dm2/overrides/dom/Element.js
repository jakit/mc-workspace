Ext.define('overrides.dom.Element', {
    override: 'Ext.dom.Element',
	 
    removeCls: function(names, prefix, suffix) {
        var SEPARATOR = '-',	// <------New
            spacesRe = /\s+/;	             // <------ New

		var me = this,
		    elementData = me.getData(),
		    hasNewCls, dom, map, classList, i, ln, name;
    	
		if (!names) {
		    return me;
		}
		
		if (!elementData) {    // <---------- New
		    return me;	   // <-----------New
		} 		   // <-----------New
		
		if (!elementData.isSynchronized) {
		    me.synchronize();
		} 
		
		dom = me.dom;
		map = elementData.classMap;
		classList = elementData.classList;
		prefix = prefix ? prefix + SEPARATOR : '';
		suffix = suffix ? SEPARATOR + suffix : '';
		if (typeof names === 'string') {
		    names = names.split(spacesRe);		
		}
		for (i = 0 , ln = names.length; i < ln; i++) {
		    name = names[i];
		    if (name) {
		        name = prefix + name + suffix;
		        if (map[name]) {
		            delete map[name];
		            Ext.Array.remove(classList, name);
		            hasNewCls = true;
		        }
		    }
		}
		if (hasNewCls) {
		    dom.className = classList.join(' ');
		}
		return me;
	}
});
Ext.util.Observable.observe(Ext.data.Connection, {
    requestexception: function(conn, response, options) {
		console.log("requestexception");
		console.log(response.status);
		if (response.status === 401) {
			console.log("401 recieved");
			var obj = Ext.decode(response.responseText);
            Ext.Msg.alert('Error!', obj.errorMessage);
			setTimeout(function(){ 
				Ext.util.Cookies.clear("apikey-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("username-"+DM2.view.AppConstants.apiurl);
				//Ext.util.Cookies.clear("userid-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("keyexpiretime-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("apiserver-"+DM2.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("logintime-"+DM2.view.AppConstants.apiurl);
				window.location.reload();
			}, 4000);
		} else if(response.status === 0){
			var errorMessage = "Request Timed Out.";
			Ext.Msg.alert('Error!', errorMessage);
		} else if (response.status === 404) {
			console.log(response.status+" recieved");
			var obj = Ext.decode(response.responseText);
            Ext.Msg.alert('Error!', obj.errorMessage);			
		} else if(response.status === 500){
			console.log(response.status+" recieved");
			var obj = Ext.decode(response.responseText);
            Ext.Msg.alert('Error!', obj.errorMessage);
		} else {
			console.log(response.status+" recieved");
			var obj = Ext.decode(response.responseText);
            Ext.Msg.alert('Error!', obj.errorMessage);
		}
    }
});
Ext.event.Event.resolveTextNode = function(node) {
	try {
		return (node && node.nodeType === 3) ? node.parentNode : node;
	} catch (err) {
		// ignore any errors here
		return null;
	}
};