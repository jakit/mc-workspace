Ext.define('overrides.event.gesture.LongPress', {
    override: 'Ext.event.gesture.LongPress',
	 
    setLongPressTimer: function(e) {
        var me = this;
 		me.setMinDuration(1500);
        clearTimeout(me.timer);
        me.timer = Ext.defer(me.fireLongPress, me.getMinDuration(), me, [
            e
        ]);
    }
});