Ext.define('Overrides.data.BufferedStore', {
    override: 'Ext.data.BufferedStore',
	/**
     * Called after the configured proxy completes a prefetch operation.
     * @private
     * @param {Ext.data.operation.Operation} operation The operation that completed
     */
    onProxyPrefetch: function(operation) {
        if (this.destroying || this.destroyed) {
            return;
        }
        var me = this,
            resultSet = operation.getResultSet(),
            records = operation.getRecords(),
            successful = operation.wasSuccessful(),
            page = operation.getPage(),
            waitForReload = operation.waitForReload,
            oldTotal = me.totalCount,
            requests = me.pageRequests,
            key, op;
        // Only cache the data if the operation was invoked for the current pageMapGeneration.
        // If the pageMapGeneration has changed since the request was fired off, it will have been cancelled.
        if (operation.pageMapGeneration === me.getData().pageMapGeneration) {
            if (resultSet) {
            	console.log(resultSet.getTotal());
                //console.log(records.length);
                console.log(me.totalCount);
				//console.log(me.getData());
				console.log("Store Toltal Cnt : "+ me.getData().length*operation.config.limit);
                //console.log(operation.config.limit);
                var tempTotalCount;
                if(!me.totalCount){
                    tempTotalCount = operation.config.limit;
                } else {
                    tempTotalCount = me.totalCount;
                }
                if(records.length < operation.config.limit){
                    me.totalCount = ((tempTotalCount + records.length) - operation.config.limit);
                } else {
                    me.totalCount = tempTotalCount + (operation.config.limit);
                }
                //me.totalCount = resultSet.getTotal(); //Old Code Assignment
                if (me.totalCount !== oldTotal) {
                    me.fireEvent('totalcountchange', me.totalCount);
                }
            }
            // Remove the loaded page from the outstanding pages hash
            if (page !== undefined) {
                delete me.pageRequests[page];
            }
            // Prefetch is broadcast before the page is cached
            me.loading = false;
            me.fireEvent('prefetch', me, records, successful, operation);
            // Add the page into the page map.
            // pageadd event may trigger the onRangeAvailable
            if (successful) {
                if (me.totalCount === 0) {
                    if (waitForReload) {
                        for (key in requests) {
                            op = requests[key].getOperation();
                            // Created in the same batch, clear the waitForReload so this
                            // won't be run again
                            if (op.waitForReload === waitForReload) {
                                delete op.waitForReload;
                            }
                        }
                        me.getData().un('pageadd', waitForReload);
                        me.fireEvent('refresh', me);
                        me.fireEvent('load', me, [], true);
                    }
                } else {
                    me.cachePage(records, operation.getPage());
                }
            }
            //this is a callback that would have been passed to the 'read' function and is optional
            Ext.callback(operation.getCallback(), operation.getScope() || me, [
                records,
                operation,
                successful
            ]);
        }
    },
     /**
     * Get the Record with the specified id.
     *
     * This method is not affected by filtering, lookup will be performed from all records
     * inside the store, filtered or not.
     *
     * @param {Mixed} id The id of the Record to find.
     * @return {Ext.data.Model} The Record with the passed id. Returns null if not found.
     */
	getById: function(id) {
        var result = (this.snapshot || this.data).findBy(function(record) {
            return record.getId() === id;
        });
        if (this.buffered && !result) {
            return [];
        }
        return result;
    }
});