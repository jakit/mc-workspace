var viewer = null;
function fitToHeight() {
	viewer.zoom('page-fit');
}
function fitToWidth() {
	viewer.zoom('page-width');
}

function ZoomIn() {
	viewer.zoomIn();
}
function ZoomOut() {
	viewer.zoomOut();
}

function rotateClockButton() {
	viewer.rotate(90);
}
function rotateAntiClockButton() {
	viewer.rotate(-90);
}

function onOver(div) {
	//div.style.background = 'yellow';
}
function onOut(div) {

}
function print() {
	viewer.printDoc();
}