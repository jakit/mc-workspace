Ext.define('DealMaker.view.DocViewerModelWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.docviewermodelwindow',

    requires: [
        'DealMaker.view.DocViewerWindowViewModel'
    ],

    viewModel: {
        type: 'docviewermodelwindow'
    },
    //width: 250,
	width: 750,
	//height: '100%',
	height: 580,
    layout: 'fit',
	//padding:'36px 0 0 0',
    title: 'Doc Viewer',
	closeAction : 'hide',
    items: []

});