Ext.define('DealMaker.view.DealDetailExistingLoanFieldset', {
    extend: 'DealMaker.view.component.container.GridToForm',
    alias: 'widget.dealdetailexistingloanfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool'        
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.frame = true;
        me.itemId = 'dealdetailexistingloanfieldset';
        me.ui = 'existingloandealpanel';      
        me.detailOnSingleRecord = true;
        // me.contextMenu = Ext.create('DealMaker.view.DealDetailPropertiesMenu');
        
        me.listLayout= {
            icon: 'resources/images/icons/loan_m.png',
            title: "EXISTING LOAN",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailexistingloangridpanel'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: 'ExistingLoans',
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'bank',
                text: 'Bank',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                dataIndex: 'receivedamount',
                text: 'Amount',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ratetype',
                text: 'Rate',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'loantype',
                text: 'LoanType',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'term',
                text: 'Months',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'amortterm',
                text: 'Amort',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ppp',
                text: 'PPP',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/loan_m.png',
            title: "EXISTING LOAN",     
      //      detailOnSingleRecord: true,       
           // padding: '5 0 0 5',
            scrollable: true,
            layout: 'column',      
            
            items: [{
               xtype: 'container',
                layout: {
                    type: 'column'
                },
                items: [{
                    columnWidth: 0.4,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Bank',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'bank'
                    },
                    {
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Amount',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'receivedamount'
                    },
                    {
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'PPP',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'ppp'
                    },
                    {
                        xtype: 'container',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'textfield',
								readOnly: true,
                                flex: 1,
                                fieldLabel: 'Option',
                                labelAlign: 'right',
                                labelWidth: 50
                            },
                            {
                                xtype: 'textfield',
								readOnly: true,
                                flex: 1,
                                fieldLabel: 'Amort',
                                labelAlign: 'right',
                                labelWidth: 50,
                                name: 'amortterm'
                            }
                        ]
                    }]
                }, {
                    columnWidth: 0.3,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Rate',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'rateQuoted'
                    },
                    {
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Index',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'index'
                    },
                    {
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Spread',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'spread'
                    },
                    {
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Months',
                        labelAlign: 'right',
                        labelWidth: 50,
                        name: 'term'
                    }]
                }, {
                    columnWidth: 0.3,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'datefield',
						readOnly: true,
                        fieldLabel: 'Start',
                        labelAlign: 'right',
                        labelWidth: 65
                    },
                    {
                        xtype: 'datefield',
						readOnly: true,
                        fieldLabel: 'End',
                        labelAlign: 'right',
                        labelWidth: 65
                    },
                    {
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Execution',
                        labelAlign: 'right',
                        labelWidth: 65
                    },
                    {
                        xtype: 'textfield',
						readOnly: true,
                        fieldLabel: 'Mtg Type',
                        labelAlign: 'right',
                        labelWidth: 65
                    }]
                }] 
            }]
        };
        me.callParent(arguments);
    }

});