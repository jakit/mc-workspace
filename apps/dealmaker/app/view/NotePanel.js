Ext.define('DealMaker.view.NotePanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.notepanel',

    requires: [
        'DealMaker.view.NotePanelViewModel',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.form.field.Number'
    ],

    viewModel: {
        type: 'notepanel'
    },
    border: false,
    frame: true,
    margin: 3,
    ui: 'activitypanel',
    bodyPadding: 10,
    closable: true,

    items: [
        {
            xtype: 'textfield',
            anchor: '100%',
            fieldLabel: 'Content',
            name: 'content',
            allowBlank: false
        },
        {
            xtype: 'numberfield',
            anchor: '100%',
            hidden: true,
            fieldLabel: 'Label',
            name: 'noteid'
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            defaults: {
                minWidth: 75
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1
                },
                {
                    xtype: 'button',
                    formBind: true,
                    disabled: true,
                    itemId: 'notesubmitbtn',
                    text: 'Submit'
                },
                {
                    xtype: 'button',
                    itemId: 'noteresetbtn',
                    text: 'Reset'
                }
            ]
        }
    ]

});