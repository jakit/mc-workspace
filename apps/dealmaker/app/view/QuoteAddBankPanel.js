/*
 * File: app/view/QuoteAddBankPanel.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.view.QuoteAddBankPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.quoteaddbankpanel',

    requires: [
        'DealMaker.view.QuoteAddBankPanelViewModel',
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.button.Button',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.filters.filter.Number',
        'Ext.grid.filters.filter.String',
        'Ext.view.Table',
        'Ext.grid.filters.Filters'
    ],

    viewModel: {
        type: 'quoteaddbankpanel'
    },
    layout: 'border',
    closable: true,
    title: 'Select Bank',

    items: [
        {
            xtype: 'form',
            region: 'north',
            split: true,
            itemId: 'bcbankdetailform',
            bodyPadding: 5,
            collapsible: true,
            header: false,
            title: 'Bank Info',
            items: [
                {
                    xtype: 'panel',
                    margin: '0 0 5 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            flex: 1,
                            fieldLabel: 'Bank',
                            labelWidth: 50,
                            name: 'bank',
                            readOnly: true
                        },
                        {
                            xtype: 'button',
                            itemId: 'setbankbtn',
                            margin: '0 0 0 10',
                            text: 'Select'
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'cmp-grid-selectiongrid',
            flex: 1,
            region: 'center',
            itemId: 'bcallbankgridpanel',
            header: false,
            sortableColumns: false,
            store: 'Bankallbuff',
			scrollable:true,
            columns: [
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'idBank',
                    text: 'Bank Id',
                    filter: {
                        type: 'number'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'fullName',
                    text: 'Full Name',
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'shortName',
                    text: 'Short Name',
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'address',
                    text: 'Address',
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'office',
                    text: 'Office',
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'state',
                    text: 'State',
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'city',
                    text: 'City',
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'zip',
                    text: 'Zip',
                    filter: {
                        type: 'string'
                    }
                }
            ],
            plugins: [
                {
                    ptype: 'gridfilters'
                }
            ]
        }
    ]

});