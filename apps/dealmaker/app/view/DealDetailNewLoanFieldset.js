Ext.define('DealMaker.view.DealDetailNewLoanFieldset', {
    extend: 'DealMaker.view.component.container.GridToForm',
    alias: 'widget.dealdetailnewloanfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool'        
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.frame = true;
    //    me.itemId = 'dealdetailcontactfieldset';
        me.ui = 'newloandealpanel';      
        me.detailOnSingleRecord = true;
        // me.contextMenu = Ext.create('DealMaker.view.DealDetailPropertiesMenu');
        
        me.listLayout= {
            icon: 'resources/images/icons/loan_m.png',
            title: "NEW LOAN",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailnewloangridpanel'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: 'Quotes',
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'bank',
                text: 'Bank',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                dataIndex: 'receivedamount',
                text: 'RxAmt',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ratetype',
                text: 'Ratetype',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'loantype',
                text: 'LoanType',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'term',
                text: 'Term',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'amortterm',
                text: 'Amort',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'ppp',
                text: 'PPP',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/loan_m.png',
            title: "NEW LOAN",     
      		//detailOnSingleRecord: true,       
            //padding: '5 0 0 5',
            scrollable: true,
			items:[{
				xtype:'panel',
	            layout: 'column',
	            items: [{
                    xtype: 'panel',
                    columnWidth: 0.45,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Bank',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'bank'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'RxAmt',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'receivedamount'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'PPP',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'ppp'
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
									readOnly: true,
                                    flex: 1,
                                    fieldLabel: 'Option',
                                    labelAlign: 'right',
                                    labelWidth: 50
                                },
                                {
                                    xtype: 'textfield',
									readOnly: true,
                                    flex: 1,
                                    fieldLabel: 'Amort',
                                    labelAlign: 'right',
                                    labelWidth: 50,
                                    name: 'amortterm'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.25,
                    margin: '0px 5px 0px 0px',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'IO',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'io'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Index',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'index'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Spread',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'spread'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Term',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'term'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.3,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Ratetype',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'ratetype'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Loantype',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'loantype'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Indexval',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'indexvalue'
                        },
                        {
                            xtype: 'textfield',
							readOnly: true,
                            fieldLabel: 'Mtg Type',
                            labelAlign: 'right',
                            labelWidth: 65
                        }
                    ]
                }]
			}]
        };
        
        me.callParent(arguments);
    }

});