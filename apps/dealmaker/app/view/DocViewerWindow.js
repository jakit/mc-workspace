Ext.define('DealMaker.view.DocViewerWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.docviewerwindow',

    requires: [
        'DealMaker.view.DocViewerWindowViewModel'
    ],

    viewModel: {
        type: 'docviewerwindow'
    },
    //width: 250,
	width: '33%',
	height: '100%',
    layout: 'fit',
    title: 'Doc Viewer',

    items: []

});