Ext.define('DealMaker.view.component.grid.Dashboard', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cmp-grid-dashboard',
    requires: [
        'Ext.grid.column.Column',
        'Ext.grid.column.Date',
		'Ext.grid.column.Check',
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.XTemplate',
        'Filters.HeaderFields'        
    ],
    mixins: ['Filters.InjectHeaderFields', 'Filters.FiltersMixin'],
    scrollable: "y",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.header = false;
        me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
        var headerFieldsFeature = Ext.create('Filters.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];            
        me.callParent(arguments);  
   }    
});