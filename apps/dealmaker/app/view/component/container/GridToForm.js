/*
 * Based on ListToDetail.js
 * 
 * a list layout and a detail layout
 * me.listLayout ref as itemId: 'listLayout'
 * me.detailLayout ref as itemId: 'detailLayout'
 * layoutFocus can be set as 'listLayout or 0' or 'detailLayout or 1'
 * switchPanel(index) // 0 or 1
 * setData(data) // this will set the data property
 * getData() // this will retrieve the data property
 * 
 */
Ext.define('DealMaker.view.component.container.GridToForm', {
    extend: 'DealMaker.view.component.container.ListToDetail',
    alias: 'widget.cmp-container-gridtoform',
    requires: [
        'Ext.grid.Panel',
        'Ext.form.Panel',
        'Ext.selection.RowModel',
        'Ext.layout.container.Anchor',
        'Ext.form.field.Text',
        'Ext.tip.QuickTip'      
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    createModel: function(data) {
        var fields = [];
        var field;
        for(field in data) {
            fields.push({
                name: field
            });
        };
        var store = Ext.create('Ext.data.Store', {
           fields: fields,
           data: data 
        });
        return store.getAt(0);
        
    },
    loadForm: function(scope) {
        var detailLayout = scope.down('form[itemId="detailLayout"]'),
            form = detailLayout.getForm(),
            data = scope.getData();
        
        if(!(data.store && data.store.data)) {
            data = this.createModel(data);
        }  
        form.loadRecord(data);
        scope.switchPanel(1);
    },
    contextMenu: null,
    detailOnSingleRecord: true,
    initComponent: function() {
        var me = this,
            listLayout,
            detailLayout,
            x, c, l;        
        
        Ext.tip.QuickTipManager.init();

        listLayout = {
            xtype: 'grid',
            viewConfig: {
                forceFit: true  
            },
            selModel: {
                selType: 'rowmodel',
                mode: 'SINGLE'
            }
        };
        
        detailLayout = {
            xtype: 'form',
            layout: {
                type: 'anchor',
                anchor: '100%'
            },
            defaults: {
                margin: "5",
                readOnly: true,
                xtype: 'textfield',
                anchor: '100%'
            }
                        
        };

        listLayout.listeners = {
            'cellclick': {
                fn: function(view, td, cellIndex, rec, tr, rowIndex, e) {
					console.log("cellclick");
					console.log(e.button);
					if(e.button == 2) {      
						/*console.log("cancel rightclick");						
						  e.preventDefault();            
						  e.stopPropagation();            
						  e.stopEvent();            
						  console.log("Fire rightclick");
						  view.fireEventArgs('rowcontextmenu',arguments);
						  return false;        */
					}else{
						var me = view.up('cmp-container-listtodetail');
						me.setData(rec);   
						this.loadForm(me);                        
					}
                },
                scope: me
            }                
        };

   //     debugger;

        if(me.contextMenu) {
            if(!detailLayout.listeners) {
                detailLayout.listeners = {};
            }
            
            Ext.apply(detailLayout.listeners, {
                'afterrender': {
                    fn: function(cmp) {
                        
                        var me = this,
                            form, data,
                            grid = cmp.up().down('grid'),
                            c = grid.getStore().getCount(),
                            rec;
                        
                        // CG: Seems like useless because of how we load data in this app    
                        // // ** this was initially in 'beforerender'
                        // if(grid.up().detailOnSingleRecord && c == 1) {
                            // rec = grid.getStore().getAt(0);
                            // grid.up('cmp-container-listtodetail').setData(rec);
                        // } else {
                            // grid.up().detailOnSingleRecord = false;
                        // }                        
                        // // ** //
                        
                        if(me.contextMenu) {
                            cmp.getEl().on('contextmenu', function(e, el, obj) {
                                e.preventDefault();
                                this.contextMenu.showAt([e.pageX, e.pageY]);
								if(this.getItemId()=="dealdetailcontactfieldset"){
									var cntid = cmp.getForm().findField('contactid').getValue();
									DealMaker.app.getController('ContactController').loadPhoneRecords(cntid);
								}
                            }, me);                            
                        }

                        // needs to be determined at the controller level.
                        // if(me.detailOnSingleRecord == true) {
                            // form = cmp.getForm();
                            // data = cmp.up().getData();
                            // form.loadRecord(data);
                            // cmp.up().switchPanel(1);
                        // }
                    },
                    scope: me
                    
                } 
            });
            
            Ext.apply(listLayout.listeners, {
                'rowcontextmenu': {
                    fn: function(grid, rec, tr, rowIdx, e, opts) {
						console.log("rowcontextmenu");
                        e.stopEvent();
                        this.contextMenu.setRec(rec);
                        this.contextMenu.showAt(e.getXY());
                    },
                    scope: me
                },
                'containercontextmenu': {
                    fn: function(grid, e, opts) {
					   console.log("containercontextmenu");
                       e.preventDefault();
                       this.contextMenu.showAt(e.getXY());
                    },
                    scope: me
                }                
            });
        }
        
        Ext.apply(me.listLayout, listLayout);
        Ext.apply(me.detailLayout, detailLayout);

        // adding tooltips to the cells        
        x = 0;
        c = me.listLayout.columns;
        l = c.length;
        
        function defRenderer (val, meta, rec, rowIdx, colIdx, store, view) {
            meta.tdAttr = 'data-qtip="' + val + '"';
            return val;    
        };
        
        for (x = 0; x < l; x++) {
            c[x].renderer = defRenderer;
        }
        
        me.callParent(arguments);  
   }
    
});