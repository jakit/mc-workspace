Ext.define('DealMaker.view.DealDetailContactFieldSet', {
    extend: 'DealMaker.view.component.container.GridToForm',
    alias: 'widget.dealdetailcontactfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool',
        'DealMaker.view.DealDetailContactMenu'        
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        
        var me = this;
        me.frame = true;
        me.itemId = 'dealdetailcontactfieldset';
        me.ui = 'contactdealpanel';      
        me.detailOnSingleRecord = true;
        me.contextMenu = Ext.create('DealMaker.view.DealDetailContactMenu');
        
        me.listLayout = {
            icon: 'resources/images/icons/contact_m.png',
            title: "CONTACTS",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailcontactgrid'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: 'DealDetailContacts',
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'fullName',
                text: 'Name',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'contactType',
                text: 'Role',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'officePhone_1',
                text: 'Phone',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'email_1',
                text: 'Email',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/contact_m.png',
            title: "CONTACTS",     
            scrollable: true,
            items : [{
                xtype: 'container',
                layout: {
                    type: 'column'
                },
                items: [{
                    columnWidth: 0.5,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'displayfield',
                        fieldLabel: 'contactid',
                        labelAlign: 'right',
                        labelWidth: 65,
						hidden:true,
                        name: 'contactid'
                    },{
                        xtype: 'displayfield',
                        fieldLabel: 'Name',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'fullName'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Company',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'companyName',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        flex: 1,
                        fieldLabel: 'Title',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'title',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'City',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'city',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Phone 1',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'officePhone_1',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Phone 2',
                        labelAlign: 'right',
                        labelWidth: 65,
                        name: 'officePhone_2',
                        readOnly: true
                    }]
                }, {
                    columnWidth: 0.5,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'displayfield',
                        fieldLabel: 'Role',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'contactType'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Address',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'address',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'zipCode',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'zip',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'State',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'state',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email 1',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'email_1',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email 2',
                        labelAlign: 'right',
                        labelWidth: 70,
                        name: 'email_2',
                        readOnly: true
                    }]
                }]
            }]
        };
        
        me.callParent(arguments);
    }

});