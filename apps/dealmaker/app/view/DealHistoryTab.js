Ext.define('DealMaker.view.DealHistoryTab', {
    extend: 'Ext.button.Split',
    alias: 'widget.dealhistorytab',

    requires: [
        'DealMaker.view.DealHistoryTabViewModel',
        'Ext.button.Button',
		'Ext.button.Split'
    ],

    viewModel: {
        type: 'dealhistorytab'
    },
    iconCls: 'loanbtn',
	text:'Deal History',
	cls:'docbtncls',
	menu:{
		items:[{
			text:'Addressess'
		},
		{
			text:'Streets'
		},
		{
			text:'Contacts'
		}]
	}
});