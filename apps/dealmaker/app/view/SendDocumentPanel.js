/*
 * File: app/view/SendDocumentPanel.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.view.SendDocumentPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.senddocumentpanel',

    requires: [
        'DealMaker.view.SendDocumentPanelViewModel',
        'DealMaker.view.DealDocsSendLOIGridPanel',
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Number',
        'Ext.button.Button',
        'Ext.grid.Panel'
    ],

    viewModel: {
        type: 'senddocumentpanel'
    },
    frame: true,
    itemId: 'senddocumentpanel',
    margin: 3,
    scrollable: true,
    ui: 'activitypanel',
    bodyPadding: 5,
    closable: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'form',
            hidden: true,
            itemId: 'sendloiloanfrm',
            scrollable: 'true',
            header: false,
            items: [
                {
                    xtype: 'fieldset',
                    collapsible: true,
                    title: 'Loans',
                    items: [
                        {
                            xtype: 'combobox',
                            anchor: '100%',
                            itemId: 'loancombo',
                            
                            fieldLabel: 'Loans',
                            labelWidth: 60,
                            displayField: 'bank',
                            queryMode: 'local',
                            store: 'DealQuotesSelected',
                            typeAhead: true,
                            valueField: 'loanid'
                        },
                        {
                            xtype: 'panel',
                            layout: 'column',
                            items: [
                                {
                                    xtype: 'panel',
                                    columnWidth: 0.5,
                                    margin: '0 5 0 0',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Bank',
                                            labelWidth: 60,
                                            name: 'bank',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'numberfield',
                                            
                                            fieldLabel: 'RxAmt',
                                            labelWidth: 60,
                                            name: 'receivedamount',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Rate Type',
                                            labelWidth: 60,
                                            name: 'ratetype',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Index',
                                            labelWidth: 60,
                                            name: 'index',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'IO',
                                            labelWidth: 60,
                                            name: 'io',
                                            readOnly: true
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    columnWidth: 0.5,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Type',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'loantype',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Term',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'term'
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Spread',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'spread'
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Indexval',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'indexvalue'
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'ppp',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'ppp'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'form',
            itemId: 'sendloicontactfrm',
            scrollable: 'true',
            header: false,
            items: [
                {
                    xtype: 'fieldset',
                    collapsible: true,
                    title: 'Contacts',
                    items: [
                        {
                            xtype: 'combobox',
                            anchor: '100%',
                            itemId: 'contactcombo',
                            
                            fieldLabel: 'Contacts',
                            labelWidth: 60,
                            displayField: 'name',
                            queryMode: 'local',
                            store: 'DealContactsSendLOI',
                            typeAhead: true,
                            valueField: 'contactid'
                        },
                        {
                            xtype: 'panel',
                            layout: 'column',
                            items: [
                                {
                                    xtype: 'panel',
                                    columnWidth: 0.5,
                                    margin: '0 5 0 0',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Name',
                                            labelWidth: 60,
                                            name: 'name',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Company',
                                            labelWidth: 60,
                                            name: 'company',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Phone',
                                            labelWidth: 60,
                                            name: 'phone',
                                            readOnly: true
                                        }
                                    ]
                                },
                                {
                                    xtype: 'panel',
                                    columnWidth: 0.5,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            
                                            fieldLabel: 'Role',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'type',
                                            readOnly: true
                                        },
                                        {
                                            xtype: 'combobox',
                                            
                                            fieldLabel: 'Email',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'email',
                                            displayField: 'email',
                                            queryMode: 'local',
                                            store: 'ContactEmails',
                                            valueField: 'email'
                                        },
                                        {
                                            xtype: 'combobox',
                                            
                                            fieldLabel: 'Fax',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                                            name: 'faxNumber',
                                            displayField: 'faxNumber',
                                            queryMode: 'local',
                                            store: 'ContactFax',
                                            valueField: 'faxNumber'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'dealdocssendloigridpanel'
        }
    ],
    dockedItems: [
        {
            xtype: 'container',
            dock: 'bottom',
            margin: '5 0 5 0',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'button',
                    flex: 1,
                    disabled: true,
                    itemId: 'emailbtn',
                    margin: '0 5 0 5',
                    hrefTarget: '_self',
                    text: 'Email'
                },
                {
                    xtype: 'button',
                    flex: 1,
                    disabled: true,
                    itemId: 'faxbtn',
                    hrefTarget: '_self',
                    text: 'Fax'
                },
                {
                    xtype: 'button',
                    flex: 1,
                    disabled: true,
                    itemId: 'printbtn',
                    margin: '0 5 0 5',
                    text: 'Print'
                },
                {
                    xtype: 'button',
                    flex: 1,
                    disabled: true,
                    hidden: true,
                    itemId: 'printlabelbtn',
                    margin: '0 5 0 0',
                    text: 'Print Label'
                }
            ]
        }
    ]

});