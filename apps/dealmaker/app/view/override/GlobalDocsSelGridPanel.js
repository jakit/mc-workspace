Ext.define('DealMaker.view.override.GlobalDocsSelGridPanel', {
    override: 'DealMaker.view.GlobalDocsSelGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});