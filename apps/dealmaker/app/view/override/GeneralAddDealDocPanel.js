Ext.define('DealMaker.view.override.GeneralAddDealDocPanel', {
    override: 'DealMaker.view.GeneralAddDealDocPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});