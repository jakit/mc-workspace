Ext.define('DealMaker.view.override.PropertyDocsGridPanel', {
    override: 'DealMaker.view.PropertyDocsGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});