Ext.define('DealMaker.view.DealDetailMasterLoanFieldset', {
    extend: 'DealMaker.view.component.container.GridToForm',
    alias: 'widget.dealdetailmasterloanfieldset',
    requires: [
        'Ext.form.Label',
        'Ext.form.field.Text',
        'Ext.grid.column.Column',
        'Ext.layout.container.Column',
        'Ext.form.field.Date',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.panel.Tool'        
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.frame = true;
        me.itemId = 'dealdetailpropertyfieldset';
        me.ui = 'masterloandealpanel';      
        me.detailOnSingleRecord = true;
        // me.contextMenu = Ext.create('DealMaker.view.DealDetailPropertiesMenu');
        
        me.listLayout= {
            icon: 'resources/images/icons/loan_m.png',
            title: "MASTER LOAN",     
            cls: 'gridcommoncls',
            // itemId: 'dealdetailmasterloangridpanel'
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: 'MasterLoans',
            columns : [{
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                dataIndex: 'DealMakerBankName',
                text: 'MeridianBankName',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'FeedBankName',
                text: 'FeedBankName',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                dataIndex: 'MortgageAmount',
                text: 'MtgAmount',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                hidden: true,
                dataIndex: 'PropertyTax',
                text: 'PropTax',
                flex: 1
            },
            {
                xtype: 'datecolumn',
                hidden: true,
                dataIndex: 'SaleDate',
                text: 'SaleDate',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                hidden: true,
                dataIndex: 'SalePrice',
                text: 'SalePrice',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                dataIndex: 'CurrentAVTAssess',
                text: 'CurrAssessedVal',
                flex: 1
            },
            {
                xtype: 'numbercolumn',
                dataIndex: 'FullValue',
                text: 'PropFullValue',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'Owner',
                text: 'OwnerName',
                flex: 1
            },
            {
                xtype: 'datecolumn',
                hidden: true,
                dataIndex: 'LastKnownMortgageDate',
                text: 'LastKnownMtgDate',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                    metaData.tdAttr = 'data-qtip="' + value + '"';
                    return value;
                },
                hidden: true,
                dataIndex: 'PropertyMasterCodeSource',
                text: 'PropCodeSource',
                flex: 1
            }]
        };
        
        me.detailLayout = {
            icon: 'resources/images/icons/loan_m.png',
            title: "MASTER LOAN",     
            scrollable: true,
            items: [{
                xtype: 'container',
                layout: {
                    type: 'column'
                },
                items: [{
                    columnWidth: 0.5,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'MeridianBankName',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'DealMakerBankName',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'FeedBankName',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'FeedBankName',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'OwnerName',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'Owner',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'CurrAssessedVal',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'CurrentAVTAssess',
                        readOnly: true
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'LastKnownMtgDate',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'LastKnownMortgageDate',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'SalePrice',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'SalePrice',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'BuildingClassDesc',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'BuildingClassDescription',
                        readOnly: true
                    }]
                }, {
                    columnWidth: 0.5,
                    xtype: 'container',
                    layout: {
                        type: 'anchor',
                        anchor: '100%'
                    },
                    defaults: {
                        anchor: '100%',
                        margin: '2px'
                    },
                    items: [{
                        xtype: 'numberfield',
                        fieldLabel: 'MasterId',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'masterID',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'PropCodeSource',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'PropertyMasterCodeSource',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'PropFullValue',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'FullValue',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'PropTax',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'PropertyTax',
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'MtgAmount',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'MortgageAmount',
                        readOnly: true
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'SaleDate',
                        labelAlign: 'right',
                        labelWidth: 115,
                        name: 'SaleDate',
                        readOnly: true
                    }]
                }]
            }]
        };
        me.callParent(arguments);
    }

});