Ext.define('DealMaker.view.DealMenuPropertyLoanPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.dealmenupropertyloanpanel',

    requires: [
        'DealMaker.view.DealMenuPropertyLoanPanelViewModel',
        'DealMaker.view.DealMenuPropertyGridPanel',
        'DealMaker.view.DealMenuLoanGridPanel'
    ],

    viewModel: {
        type: 'dealmenupropertyloanpanel'
    },
    bodyPadding: 2,
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
    items: [
        {
            xtype: 'dealmenupropertygridpanel',
            title: 'Properties',
            flex: 1
        },
        {
            xtype: 'dealmenuloangridpanel',
            title: 'Loans',
            flex: 1
        }
    ]

});