Ext.define('DealMaker.view.ContactsGridPanel', {
    extend: 'DealMaker.view.component.grid.Dashboard',
    alias: 'widget.contactsgridpanel',
    requires: [ 'DealMaker.view.ContactsGridPanelViewModel' ],
    viewModel: {
        type: 'contactsgridpanel'
    },
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    scrollable: true,
    initComponent: function() {
        var me = this;
        
        me.itemId = 'contactsgridpanel';
        me.store = 'Contactallbuff';
        
        me.columns = [{
            xtype: 'gridcolumn',
            hidden: true,
            dataIndex: 'idContact',
            text: 'Contact No.',
            filter: {
                type: 'number'
            }
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'contactType',
            text: 'ContactType',
            filter: {
                type: 'string'
            }
        },{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'fullName',
            text: 'Full Name',
            filter: {
                type: 'string'
            }
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'firstName',
            text: 'First Name',
            filter: {
                type: 'string'
            }
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'lastName',
            text: 'Last Name',
            filter: {
                type: 'string'
            }
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 75,
			hidden: true,
            dataIndex: 'title',
            text: 'Title'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'companyName',
            text: 'Company',
            filter: {
                type: 'string'
            }
        }, 
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'address',
            text: 'Address'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'position',
            text: 'Position'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'mobilePhone_1',
            text: 'Mobile Ph 1'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'officePhone_1',
            text: 'Office Ph 1'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'email_1',
            text: 'Email1'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'city',
            text: 'City'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'state',
            text: 'State'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'zipCode',
            text: 'Zip Code'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'officePhone_2',
            text: 'Office Ph 2'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'homePhone_1',
            text: 'Home Ph 1'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'homePhone_2',
            text: 'Home Ph 2'
        }, 
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'mobilePhone_2',
            text: 'Mobile Ph 2'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'officeFax',
            text: 'Office Fax'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'homeFax',
            text: 'Home Fax'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'pager',
            text: 'Pager'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden: true,
            dataIndex: 'email_2',
            text: 'Email2'
        }];
        
        me.callParent(arguments);    
    }
});