Ext.define('DealMaker.view.DealDetailContactMenu', {
    extend: 'DealMaker.view.component.container.DealsDetailsContextMenu',
    alias: 'widget.dealdetailcontactmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
            text: 'Add Contact',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Remove Contact',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Change Role',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Edit Contact',
            focusable: true
        },
        {
            xtype: 'menuitem',
            itemId: 'callcontactdetail',
            text: 'Call Contact',
            focusable: true,
            menu: {
                xtype: 'menu',
                width: 250
            }
        }];    
        me.callParent(arguments);
    }
});