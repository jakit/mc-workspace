Ext.define('DealMaker.view.DealDetailPropertiesMenu', {
    extend: 'DealMaker.view.component.container.DealsDetailsContextMenu',
    alias: 'widget.dealdetailpropertiesmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
            text: 'Add Property',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Remove Property',
            focusable: true
        },
        {
            xtype: 'menuitem',
            text: 'Research',
            focusable: true,
            menu: {
                xtype: 'menu',
                itemId: 'researchmenu',
                width: 120,
                items: [
                    {
                        xtype: 'menuitem',
                        text: 'Gmap',
                        focusable: true
                    },
                    {
                        xtype: 'menuitem',
                        text: 'Zillow',
                        focusable: true
                    },
                    {
                        xtype: 'menuitem',
                        text: 'Trulia',
                        focusable: true
                    },
                    {
                        xtype: 'menuitem',
                        text: 'Reonomy',
                        focusable: true
                    }
                ]
            }
        }];    
        me.callParent(arguments);
    }
});