Ext.define('DealMaker.view.BankGridPanel', {
	extend: 'DealMaker.view.component.grid.Dashboard',
    alias: 'widget.bankgridpanel',
    requires: [
        'DealMaker.view.BankGridPanelViewModel'
    ],
    viewModel: {
        type: 'bankgridpanel'
    },
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	scrollable: true,
    initComponent: function() {
        var me = this;
        
        me.itemId = 'bankgridpanel';
        me.store = 'Bankallbuff';
        
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'fullName',
            text: 'Full Name',
			flex:1,
            filter: {
                type: 'string'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'shortName',
            text: 'Short Name',
            filter: {
                type: 'string'
            }
        },
		{
            xtype: 'gridcolumn',
            dataIndex: 'idBank',
            text: 'Bank Id',
            filter: {
                type: 'number'
            }
        },
        {
            xtype: 'checkcolumn',
            dataIndex: 'meridianBank',
            text: 'Meridian Bank'
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'office',
            text: 'Office',
            filter: {
                type: 'string'
            }
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'address',
            text: 'Address',
            filter: {
                type: 'string'
            }
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'state',
            text: 'State',
            filter: {
                type: 'string'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'city',
            text: 'City',
            filter: {
                type: 'string'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'zip',
            text: 'Zip',
            filter: {
                type: 'string'
            }
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'payoutNoticeDays',
            text: 'PayoutNoticeDays'
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'defaultExecution',
            text: 'DefaultExecution'
        },
        {
            xtype: 'gridcolumn',
			hidden:true,
            dataIndex: 'createUser',
            text: 'CreateUser'
        },
        {
            xtype: 'gridcolumn',
			hidden:true,
            dataIndex: 'modifyUser',
            text: 'ModifyUser'
        },
        {
            xtype: 'datecolumn',
			hidden:true,
            dataIndex: 'createDate',
            text: 'CreateDate',
            format: 'm-d-Y'
        },
        {
            xtype: 'datecolumn',
			hidden:true,
            dataIndex: 'modifyDate',
            text: 'ModifyDate',
            format: 'm-d-Y'
        },
        {
            xtype: 'gridcolumn',
			hidden:true,
            dataIndex: 'modifyTime',
            text: 'ModifyTime'
        }];
        
        me.callParent(arguments);    
    }
});