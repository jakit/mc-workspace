Ext.define('DealMaker.store.PropertyTypeList', {
    extend: 'Ext.data.Store',

    requires: [
        'DealMaker.model.SelectionList',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'PropertyTypeList',
            model: 'DealMaker.model.SelectionList',
            proxy: {
                type: 'rest',
                noCache: false,
                url: 'http://devnyespresso02/rest/default/hsjag/v1/mssql:SelectionList'
            },
            sorters: {
                property: 'selName'
            }
        }, cfg)]);
    }
});