Ext.define('DealMaker.store.Office', {
    extend: 'Ext.data.Store',

    requires: [
        'DealMaker.model.Office',
        'Ext.data.proxy.Rest',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Office',
            model: 'DealMaker.model.Office',
            proxy: {
                type: 'rest',
                noCache: false,
                url: 'http://devnyespresso02/rest/default/hsjag/v1/mssql:Office'
            }
        }, cfg)]);
    }
});