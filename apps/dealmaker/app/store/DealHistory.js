Ext.define('DealMaker.store.DealHistory', {
    extend: 'Ext.data.Store',

    requires: [
        'DealMaker.model.Deal',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'DealHistory',
            model: 'DealMaker.model.Deal',
            proxy: {
                type: 'rest',
                noCache: false,
                url: 'http://devnyespresso02/rest/default/hsjag/v1/mssql:v_dealPerAddress',
				reader: {
					type: 'json',
					rootProperty: function(data){
						console.log(data.result[0].rows);
						return data.result[0].rows;
					}
				}
            }
        }, cfg)]);
    }
});