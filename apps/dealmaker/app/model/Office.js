Ext.define('DealMaker.model.Office', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idOffice'
        },
        {
            name: 'code'
        },
		{
            name: 'name'
        },
		{
            name: 'parentName'
        },
		{
            name: 'address'
        },
		{
            name: 'city'
        },
		{
            name: 'state'
        },
		{
            name: 'zip'
        },
		{
            name: 'phone'
        },
		{
            name: 'fax'
        },
		{
            name: 'url'
        },
		{
            name: 'email'
        }
    ]
});