Ext.define('DealMaker.model.SelectionList', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idSelectionList'
        },
        {
            name: 'selectionDesc'
        },
        {
            name: 'selectionType'
        },
		{
            name: 'isDefault'
        }
    ]
});