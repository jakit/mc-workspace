Ext.define('DealMaker.controller.DealHistoryController', {
    extend: 'Ext.app.Controller',

    refs: {
		dealhistorytab: {
            //autoCreate: true,
            selector: 'dealhistorytab',
            xtype: 'dealhistorytab'
        }
    },

    init: function(application) {
        this.control({
        });
    },

    onCategoryChange: function(newValue) {
        var me = this;
        var dealhistory_st = Ext.getStore('DealHistory');
		var viewname;
		console.log(newValue);
		if(newValue=="Addressess"){
			viewname = "sp_dealsPerAddress";
		}else if(newValue=="Streets"){
			viewname = "sp_dealsPerStreet";
		}else if(newValue=="Contacts"){
			viewname = "sp_dealsPerContact";
		}
		console.log("viewname"+viewname);
		// Get The DealId
		var dashboardview = me.getController('DashboardController').getDashboardview();
		var currViewId = dashboardview.getLayout().getActiveItem().getItemId();
		if(currViewId==="dealmenupanel"){
			var dealgrid = me.getController('DealController').getDealmenugridpanel();
			if (dealgrid.getSelectionModel().hasSelection()) {
				var row = dealgrid.getSelectionModel().getSelection()[0];
				var dealid = row.get('dealid');
			}
		} else if(currViewId==="dealdetailsformpanel"){
			var dealdetailform = me.getController('DealDetailController').getDealdetailsformpanel();
	        var dealid = dealdetailform.dealrec.get('dealid');
		}
		//viewname = "v_deals";
		//var dealid = 1;
		var url = DealMaker.view.AppConstants.apiurl+'mssql:'+viewname;
		dealhistory_st.getProxy().setUrl(url);
        dealhistory_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        dealhistory_st.load({
            params:{
                //filter :'dealid = '+dealid
				'arg.dealid' : dealid
            },
            callback: function(records, operation, success){
            }
        });
    }

});
