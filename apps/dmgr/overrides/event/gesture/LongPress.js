Ext.define('overrides.event.gesture.LongPress', {
    override: 'Ext.event.gesture.LongPress',
	 
    setLongPressTimer: function(e) {
        var me = this;
 		me.setMinDuration(1500);
        me.timer = Ext.defer(function() {
            me.fireLongPress(e);
        }, me.getMinDuration());
    }
});