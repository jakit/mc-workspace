/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.BLANK_IMAGE_URL = "resources/images/icons/download.gif";
Ext.onReady(function(){
	console.log("On Ready");
	Ext.getBody().on("contextmenu", function(e, t, eOpts){console.log("On Body Right Click");e.preventDefault();});
});//this is valid 
Ext.define('DMGR.Application', {
    extend: 'Ext.app.Application',
    
    name: 'DMGR',
	
	controllers: [    
        'MainController',
		'DashboardController',
		'RightsController',
		'AclController',
		'FunctionRightController'
    ],
	
	models: [
		'FunctionalPermission'
	],
	
    stores: [
        // TODO: add global / shared stores here
		'FunctionalPermissions'
    ],
	
	views: [
		'AppConstants'
	],
    
    launch: function () {
        // TODO - Launch the application
    }
});