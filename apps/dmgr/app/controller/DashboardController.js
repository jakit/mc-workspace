Ext.define('DMGR.controller.DashboardController', {
    extend: 'Ext.app.Controller',
	requires:['DMGR.view.dashboard.TopMenuToolbar',
			  'DMGR.view.dashboard.DashboardCenterPanel',
			  'DMGR.view.about.AboutWindow',
			  'DMGR.view.rights.Layout',
			  'DMGR.view.acl.Layout',
			  'DMGR.view.functionright.Layout'],
    refs: {
        topmenutoolbar: {
            autoCreate: true,
            selector: 'topmenutoolbar',
            xtype: 'topmenutoolbar'
        },
        usernametext  : '#usernametext',
		msgtext       : '#msgtext',
        dashboardview : 'topmenutoolbar #dashboardview',
		aboutwindow   : 'aboutwindow',
		maintabpanel : 'dashboardcenterpanel'
    },

    init: function(application) {
         this.control({
			 'topmenutoolbar':{
				afterrender: this.afterTopMenuToolbarRender
			 },
			 'maintabpanel':{
				afterrender: this.afterMainTabPanelRender
			 },
             'topmenutoolbar #logoutbtn':{
                 click:this.logOutBtnClick
             },
			 'topmenutoolbar #aboutbtn':{
                 click:this.aboutBtnClick
             },
			 'topmenutoolbar button[action="acl"]':{
                 click:this.aclBtnClick
             },
			 'topmenutoolbar button[action="userDefACL"]':{
                 click:this.rightsBtnClick
             },
			 'topmenutoolbar button[action="funcRights"]':{
                 click:this.funcRightsBtnClick
             }
         });
    },
	
	afterTopMenuToolbarRender: function(toolbar){
		var me = this,toolbaritem;
		
		toolbaritem = Ext.create('Ext.Img',{
			autoEl : {
				tag : 'a',
				href : '/DealMaker/',
				target : '_self'
			},
			height : 40,
			width : 230,
			src : 'resources/images/logo.png'
		});
		toolbar.add(toolbaritem);
		
		var funpermdata = Ext.getStore('FunctionalPermissions').data.items;
		if(funpermdata){
			var btnCnt = 1;
			Ext.Array.each(funpermdata, function(arritem, index, countriesItSelf) {
				if (arritem.data.gui === "dmgr-menu") {
					if(arritem.data.allowed && arritem.data.allowed==1){
						var toolbaritem = Ext.create('Ext.button.Button',{
							ui : 'menubtnui-toolbar-medium',
							scale : 'medium',
							text : arritem.data.functionActivity,
							action : arritem.data.functionActivity,
							style:'text-transform: capitalize;'
						});
						toolbar.add(toolbaritem);
						if(btnCnt==1){
							me.setActiveBtn(toolbaritem);	
						}
						btnCnt++;
					}
				}
			});
		}		
		
		var toolbaritem = Ext.create('Ext.button.Button',{
			 itemId : 'aboutbtn',
            ui : 'menubtnui-toolbar-medium',
            scale : 'medium',
            text : 'About'
		});
		toolbar.add(toolbaritem);
		
		var toolbaritem = Ext.create('Ext.toolbar.TextItem',{
			itemId : 'msgtext'
		});
		toolbar.add(toolbaritem);
		
		var toolbaritem = Ext.create('Ext.toolbar.Fill');
		toolbar.add(toolbaritem);
		
		var toolbaritem = Ext.create('Ext.toolbar.TextItem',{
			itemId : 'usernametext',
            style : 'color:#fff;font-weight:bold;'
		});
		toolbar.add(toolbaritem);
		
		var toolbaritem = Ext.create('Ext.button.Button',{
			itemId : 'logoutbtn',
            ui : 'menubtnui-toolbar-medium',
            scale : 'medium',
            text : 'Log Out'
		});
		toolbar.add(toolbaritem);
	},
	
	afterMainTabPanelRender: function(tabpanel){
		var me = this,tabitem,j=0;
		console.log("After Main tab Panel Render");
		var funpermdata = Ext.getStore('FunctionalPermissions').data.items;
		if(funpermdata){
			Ext.Array.each(funpermdata, function(arritem, index, countriesItSelf) {
				if (arritem.data.gui === "dmgr-menu") {
					if(arritem.data.allowed && arritem.data.allowed==1 && arritem.data.functionActivity=="userDefACL"){
						tabitem = Ext.create('DMGR.view.rights.Layout');
					} else if(arritem.data.allowed && arritem.data.allowed==1 && arritem.data.functionActivity=="acl"){
						tabitem = Ext.create('DMGR.view.acl.Layout');
					}
					if(j==0){
						tabpanel.add(tabitem);
					}
					j++;
				}
			});
		}
	},

    setUserNameText: function(text) {
        this.getUsernametext().setText(text);
    },

    setTabPanel: function(btn, index) {
        var me = this;
        var viewport = btn.up('viewport');
        var tabPanel = viewport.down('dashboardcenterpanel');
        tabPanel.getLayout().setActiveItem(index);
        me.setActiveBtn(btn);        
    },

    showDashboardContainer: function() {
        var me = this;

        var mainviewport = me.getController('MainController').getMainviewport();
		mainviewport.removeAll();
		mainviewport.add([{
            xtype: 'topmenutoolbar',
            height: 60,
            region: 'north'
        }, {
            xtype: 'dashboardcenterpanel',
            region: 'center'
        }]);
        // mainviewport.getLayout().setActiveItem(me.getDashboardcontainer());
        // me.showDeals(me.getDashboardcontainer().down('#dealsbtn'));
    },

    logOutBtnClick: function(btn) {
        var me = this;
        Ext.util.Cookies.clear("dmgr-apikey-"+DMGR.view.AppConstants.apiurl);
        Ext.util.Cookies.clear("dmgr-username-"+DMGR.view.AppConstants.apiurl);
        Ext.util.Cookies.clear("dmgr-userid-"+DMGR.view.AppConstants.apiurl);
		Ext.util.Cookies.clear("dmgr-keyexpiretime-"+DMGR.view.AppConstants.apiurl);
		Ext.util.Cookies.clear("dmgr-apiserver-"+DMGR.view.AppConstants.apiurl);
		Ext.util.Cookies.clear("dmgr-logintime-"+DMGR.view.AppConstants.apiurl);
		
        me.getController('MainController').showLoginView();
    },

    setActiveBtn: function(btn) {
        //console.log("Set the Active Btn");
        var me = this;
        if(me.getTopmenutoolbar().currActiveBtn){
            me.getTopmenutoolbar().currActiveBtn.setUI('menubtnui-toolbar-medium');
        }
        //Set the Curr Active Btn as new one
        me.getTopmenutoolbar().currActiveBtn = btn;

        ///Add the Pressed active cls of that Button
        me.getTopmenutoolbar().currActiveBtn.setUI('activemenubtnui-toolbar-medium');
	},
	
	aboutBtnClick: function(){
		var me = this;
		//console.log(Ext.manifest);
		var aboutwindow = me.getAboutwindow();
		if(aboutwindow){
			aboutwindow.close();
		}
		var aboutwindow = Ext.create('DMGR.view.about.AboutWindow',{
			closeAction : 'destroy'
		});
		aboutwindow.show();
		var infopanel = aboutwindow.down('#infopanel');
		//var apiserver = Ext.util.Cookies.get("dmgr-apiserver-"+DMGR.view.AppConstants.apiurl);
		var apiserver = DMGR.view.AppConstants.apiurl;
		var logintime = Ext.util.Cookies.get("dmgr-logintime-"+DMGR.view.AppConstants.apiurl);
		var keyexpiretimeval = Ext.util.Cookies.get("dmgr-keyexpiretime-"+DMGR.view.AppConstants.apiurl);
		
		var expdate  = new Date(keyexpiretimeval);
		var currdate = new Date();
		var rmTime = expdate - currdate;
		var timeval = me.parseMillisecondsIntoReadableTime(rmTime);
		
		console.log(apiserver);
		console.log("Ext Js Version : "+Ext.getVersion().version);
		var st = '<br /><div style="font-weight:bold;">Apiserver : '+apiserver+'</div><br /><div style="font-weight:bold;">Sencha Ext Js Version : '+Ext.getVersion().version+'</div><br /><div style="font-weight:bold;">Dealmanager Version : 1.0.1</div><br /><div style="font-weight:bold;">Login Time : '+logintime+'</div><br /><div style="font-weight:bold;">KeyExpire Time : '+keyexpiretimeval+'</div><br /><div style="font-weight:bold;">Remaining Time : '+timeval+' H:mm:ss</div>';

		if(Ext.manifest.appBuildMode==="prod"){
			if(typeof appTimestamp !== 'undefined'){
				// the variable is defined
				console.log("appTimestamp"+appTimestamp);
				aboutwindow.setHeight(340);
				st = st+'<br /><div style="font-weight:bold;">Build Time : '+appTimestamp+'</div>';
			}
		}
		
		infopanel.setHtml(st);
	},
	
	parseMillisecondsIntoReadableTime: function(milliseconds){
	  //Get hours from milliseconds
	  var hours = milliseconds / (1000*60*60);
	  var absoluteHours = Math.floor(hours);
	  var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
	
	  //Get remainder from hours and convert to minutes
	  var minutes = (hours - absoluteHours) * 60;
	  var absoluteMinutes = Math.floor(minutes);
	  var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;
	
	  //Get remainder from minutes and convert to seconds
	  var seconds = (minutes - absoluteMinutes) * 60;
	  var absoluteSeconds = Math.floor(seconds);
	  var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
	
	  return h + ':' + m + ':' + s;
	},
	
	rightsBtnClick: function(btn){
		var me = this,tabitem;
		if(me.getMaintabpanel().down('rights-layout')){
			console.log("rights layout Exist & active it");
		}else{
			console.log("Create rights layout & active it");
			tabitem = Ext.create('DMGR.view.rights.Layout');
			me.getMaintabpanel().add(tabitem);
		}
		me.setTabPanel(btn, me.getMaintabpanel().down('rights-layout'));
	},
	
	aclBtnClick: function(btn){
		var me = this,tabitem;
		if(me.getMaintabpanel().down('acl-layout')){
			console.log("Acl layout Exist & active it");
		}else{
			console.log("Create Acl layout & active it");
			tabitem = Ext.create('DMGR.view.acl.Layout');
			me.getMaintabpanel().add(tabitem);
		}
		me.setTabPanel(btn, me.getMaintabpanel().down('acl-layout'));
		//me.getController('AclController').loadAcl(me.getMaintabpanel().down('acl-layout').down('acl-grid'));
	},
	
	funcRightsBtnClick: function(btn){
		var me = this,tabitem;
		if(me.getMaintabpanel().down('functionright-layout')){
			console.log("Functionright layout Exist & active it");
		}else{
			console.log("Create Functionright layout & active it");
			tabitem = Ext.create('DMGR.view.functionright.Layout');
			me.getMaintabpanel().add(tabitem);
		}
		me.setTabPanel(btn, me.getMaintabpanel().down('functionright-layout'));
	}
});