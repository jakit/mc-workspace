Ext.define('DMGR.controller.AclController', {
    extend: 'Ext.app.Controller',
	requires:['DMGR.view.acl.AclGridMenu',
			  'DMGR.view.acl.EntityPickerPanel'],
    refs: {
        aclgrid  : 'acl-grid',
		acllayout : 'acl-layout',
		acleastpanel : 'acl-layout #rights-east-panel',
		aclgridmenu : 'aclgridmenu',
		entitypickerpanel : 'entitypickerpanel'
    },

    init: function(application) {
		this.control({
			'aclgrid':{
				afterrender: this.afterAclGridRender,
				select : this.onAclGridItemSelect,
                rowcontextmenu: this.onAclGridRightClick
			},
			'aclgridmenu':{
				click:this.aclGridMenuClick
			},
			'#acl-east-panel button[action="save"]':{
				 click: this.onACLSaveClick
			},
			'#acl-east-panel button[action="reset"]':{
				 click: this.onACLResetClick
			},
			'acl-detail-form button[action="close"]':{
				 click: this.onACLCloseBtnClick
			},
			'#acl-east-panel button[action="entitypickbtn"]':{
				 click: this.onEntityPickBtnClick
			},
			'entitypickerpanel':{
				close: this.closeEntityPickerPanel
			},
			'entitypickerpanel button[action="selectEntityBtn"]': {
				click: 'selectEntityBtnClick'
			},
			'acl-layout acl-detail-form field':{
				 change: this.aclDetailFormFieldChange
			}
		});
    },
	
	afterAclGridRender: function(aclgrid){
		var me = this;	
		me.loadAcl(aclgrid);
	},
	
	loadAcl: function(aclgrid) {
        var me = this;		
		var acl_st = aclgrid.getStore();
        var aclproxy = acl_st.getProxy();
		var aclproxyCfg = aclproxy.config;
        aclproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		acl_st.setProxy(aclproxyCfg);
        acl_st.load({
            scope: this,
            callback: function(records, operation, success) {
                if(success && records.length > 0){            
  	        		aclgrid.getSelectionModel().select(0);
                }
            }
        });
    },
	
	onAclGridItemSelect: function(selModel, record, index, eOpts){
		console.log("Acl Grid Item is Selected.");
		var me = this;
		var acldetailform = selModel.view.ownerCt.up('acl-layout').down('acl-detail-form');
		me.selectACLRec(acldetailform,record);		
	},
	
	selectACLRec: function(acldetailform,record){
		var me = this;
		acldetailform.loadRecord(record);
		acldetailform.down('button[action="save"]').hide();
		acldetailform.down('button[action="save"]').disable();
		acldetailform.down('button[action="reset"]').hide();
		acldetailform.getForm().findField('idACL').show();
		acldetailform.setTitle("ACL Details");
		acldetailform.getForm().findField('entityName').setReadOnly(true);
		acldetailform.getForm().findField('isGroup').setReadOnly(true);
		acldetailform.getForm().findField('list').setReadOnly(true);
		acldetailform.getForm().findField('open').setReadOnly(true);
		acldetailform.getForm().findField('modify').setReadOnly(true);
		acldetailform.getForm().findField('delete').setReadOnly(true);
		acldetailform.getForm().findField('control').setReadOnly(true);
		acldetailform.down('button[action="entitypickbtn"]').disable();
		acldetailform.down('button[action="close"]').hide();
	},
	
	onAclGridRightClick:  function(view, record, tr, rowIndex, e, eOpts) {
        console.log("ACL Grid Right Click");
        var me = this;
        e.stopEvent();
        console.log("Show Menu for ACL Grid");
        if(view.up('acl-grid').contextMenu){
            view.up('acl-grid').contextMenu.showAt(e.getXY());
			var aclmenu = view.up('acl-grid').contextMenu;
			if(record.get('idACL')==null){
				aclmenu.down('menuitem[action="deleteacl"]').disable();
				aclmenu.down('menuitem[action="modifyacl"]').disable();
			} else {
				aclmenu.down('menuitem[action="deleteacl"]').enable();
				aclmenu.down('menuitem[action="modifyacl"]').enable();
			}
			view.up('acl-grid').contextMenu.ownerCmp = view.up('acl-grid');
        }
    },
	
	aclGridMenuClick: function(menu, item, e, eOpts){
		console.log("ACL Grid Sub Menu Clicked");
		var me = this;
		if(item){
			if(item.action === "deleteacl"){
				console.log("Delete ACL");
				me.deleteACLEntry(menu.ownerCmp);
			} else if(item.action === "modifyacl"){
				console.log("Modify ACL");
				me.modifyACLEntry(menu.ownerCmp);
			} else if(item.action === "addacl"){
				console.log("Add ACL");
				me.addACLEntry(menu.ownerCmp);
			}
		}
	},
	
	deleteACLEntry: function(aclgrid){
		var me = this,idACL;
        if (aclgrid.getSelectionModel().hasSelection()) {
            var record = aclgrid.getSelectionModel().getSelection()[0];
			idACL = record.get('idACL');
        }		     
                
        Ext.Msg.show({
            title:'Delete ACL?',
            message: 'Are you sure you want to delete the acl?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
					aclgrid.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DMGR.view.AppConstants.apiurl+'mssql:ACL/'+idACL+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            console.log("Delete ACL is done.");
							aclgrid.setLoading(false);
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200) {
								me.loadAcl(aclgrid);
                            } else {
                                console.log("Delete ACL Failure.");
                                Ext.Msg.alert('Delete ACL failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Delete ACL Failure.");
							aclgrid.setLoading(false);
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete ACL failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
	},
	
	addACLEntry: function(aclgrid){
		var me = this;
		var acllayout = aclgrid.up('acl-layout');
		var acleastpanel = acllayout.down('#acl-east-panel');
		var acldetailform = acllayout.down('acl-detail-form');
		
		acldetailform.getForm().reset();
		acldetailform.getForm().findField('idACL').hide();
		acldetailform.setTitle("Add ACL");
		if (aclgrid.getSelectionModel().hasSelection()) {
            var record = aclgrid.getSelectionModel().getSelection()[0];
			acldetailform.getForm().findField('objType').setValue(record.get('objType'));
			acldetailform.getForm().findField('objID').setValue(record.get('objID'));
			acldetailform.getForm().findField('objDesc').setValue(record.get('objDesc'));
        }
		me.doACLFormWritable(acldetailform);
	},
	
	doACLFormWritable: function(acldetailform){
		var me = this;
		acldetailform.getForm().findField('entityName').setReadOnly(false);
		acldetailform.getForm().findField('isGroup').setReadOnly(false);
		acldetailform.getForm().findField('list').setReadOnly(false);
		acldetailform.getForm().findField('open').setReadOnly(false);
		acldetailform.getForm().findField('modify').setReadOnly(false);
		acldetailform.getForm().findField('delete').setReadOnly(false);
		acldetailform.getForm().findField('control').setReadOnly(false);
		acldetailform.down('button[action="entitypickbtn"]').enable();
		
		acldetailform.down('button[action="save"]').show();
		acldetailform.down('button[action="reset"]').show();
		acldetailform.down('button[action="save"]').disable();
		acldetailform.down('button[action="reset"]').disable();
		acldetailform.down('button[action="close"]').show();
	},
	
	modifyACLEntry: function(aclgrid){
		var me = this;
		var acllayout = aclgrid.up('acl-layout');
		var acldetailform = acllayout.down('acl-detail-form');
		acldetailform.getForm().findField('idACL').show();

        if (aclgrid.getSelectionModel().hasSelection()) {
            var record = aclgrid.getSelectionModel().getSelection()[0];
			acldetailform.loadRecord(record);
        }
		acldetailform.setTitle("Modify ACL");
		me.doACLFormWritable(acldetailform);
	},
	
	onACLSaveClick: function(btn){
		var me = this,defaultACLRightTotal=0;
		var acllayout = btn.up('acl-layout');
		var acldetailform = acllayout.down('acl-detail-form');		
        var aclForm = acldetailform.getForm();
        var idACL = aclForm.findField('idACL').getValue();
		
		var list  = aclForm.findField('list').getValue();
		var openval  = aclForm.findField('open').getValue();
		var modify  = aclForm.findField('modify').getValue();
		var deleteval  = aclForm.findField('delete').getValue();
		var control = aclForm.findField('control').getValue();
		
		if(list==true){
			defaultACLRightTotal = defaultACLRightTotal + 1;
		}
		if(openval==true){
			defaultACLRightTotal = defaultACLRightTotal + 2;
		}
		if(modify==true){
			defaultACLRightTotal = defaultACLRightTotal + 4;
		}
		if(deleteval==true){
			defaultACLRightTotal = defaultACLRightTotal + 8;
		}
		if(control==true){
			defaultACLRightTotal = defaultACLRightTotal + 16;
		}
		
		var data = {
			'objectType' : aclForm.findField('objType').getValue(),
			'objectID'   : aclForm.findField('objID').getValue(),
			'entityName' : aclForm.findField('entityName').getValue(),
			'isGroup'    : aclForm.findField('isGroup').getValue(),
			'rights'     : defaultACLRightTotal
		};

        if(idACL===0 || idACL===null || idACL==="") {
            console.log("Add New ACL");
            var methodname = "POST";
        } else {
            console.log("Modify ACL");
            data['idACL'] = idACL;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";
        }

        acldetailform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DMGR.view.AppConstants.apiurl+'mssql:ACL',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                acldetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					me.loadAcl(acllayout.down('acl-grid'));
                } else {
                    console.log("Add/Edit ACL Failure.");
                    Ext.Msg.alert('ACL Save failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                acldetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save ACL', obj.errorMessage);
            }
        });
	},
	
	onACLResetClick: function(btn){
		var me = this;
		var acldetailform = btn.up('acl-detail-form');
		var aclgrid = btn.up('acl-layout').down('acl-grid');
        if (aclgrid.getSelectionModel().hasSelection()) {
            var record = aclgrid.getSelectionModel().getSelection()[0];
			acldetailform.loadRecord(record);
        }
		acldetailform.down('button[action="save"]').disable();
		acldetailform.down('button[action="reset"]').disable();
	},
	
	onEntityPickBtnClick: function(btn){
		var me = this,entitypickerpanel;
		console.log("Entity Picker button Click");
		me.showEntityPickPanel(btn);
	},
	
	showEntityPickPanel: function(btn){
		var me = this,detailform,eastregion;
		if(btn.up('acl-detail-form')){
			detailform = btn.up('acl-detail-form');
			eastregion = detailform.up('#acl-east-panel');
		} else if(btn.up('functionright-detail-form')){
			detailform = btn.up('functionright-detail-form');
			eastregion = detailform.up('#functionright-east-panel');
		}
		
		if(eastregion.down('entitypickerpanel')){
			entitypickerpanel = eastregion.down('entitypickerpanel');
		} else {
			entitypickerpanel = Ext.create('DMGR.view.acl.EntityPickerPanel');
		}
		eastregion.getLayout().setActiveItem(entitypickerpanel);
		entitypickerpanel.config.backview = detailform;
		me.loadEntity(entitypickerpanel);
	},
	
	loadEntity: function(entitypickerpanel){
		var me = this;		
		var entity_st = entitypickerpanel.getStore();
        var entityproxy = entity_st.getProxy();
		var entityproxyCfg = entityproxy.config;
        entityproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		entity_st.setProxy(entityproxyCfg);
        entity_st.load({
            scope: this,
            callback: function(records, operation, success) {
            }
        });
	},
	
	closeEntityPickerPanel: function(panel){
		var me = this,eastregion;
		console.log("Entity Picker Panel Close");
		var detailform = panel.config.backview;
		if(panel.up('#acl-east-panel')){
			eastregion = panel.up('#acl-east-panel');
		} else if(panel.up('#functionright-east-panel')){
			eastregion = panel.up('#functionright-east-panel');
		}
		eastregion.getLayout().setActiveItem(detailform);
		//eastregion.getHeader().show();
	},
	
	selectEntityBtnClick: function(btn){
		console.log("Save Entity To Entity Name.");
		var me = this;
		var entitypickerpanel = btn.up('entitypickerpanel');
        if (entitypickerpanel.getSelectionModel().hasSelection()) {
            var row = entitypickerpanel.getSelectionModel().getSelection()[0];
            var name = row.get('value');
			var acldetailform = entitypickerpanel.config.backview;	
			if(entitypickerpanel.up('#acl-east-panel')){
				acldetailform.getForm().findField('entityName').setValue(name);
				acldetailform.getForm().findField('isGroup').setValue(row.get('isGroup'));
			} else if(entitypickerpanel.up('#functionright-east-panel')){
				acldetailform.getForm().findField('entity').setValue(name);
				//acldetailform.getForm().findField('isGroup').setValue(row.get('isGroup'));
			}			
			me.closeEntityPickerPanel(entitypickerpanel);
        } else {
            Ext.Msg.alert('Failed', "Please select entity.");
            return false;
        }
	},
	
	aclDetailFormFieldChange: function(field , newValue , oldValue , eOpts){
		var me = this;
		var acldetailform = field.up('acl-detail-form');
		acldetailform.down('button[action="save"]').enable();
		acldetailform.down('button[action="reset"]').enable();
	},
	
	onACLCloseBtnClick: function(btn){
		var me = this;
		console.log("ACL btn Close click");
		var acldetailform = btn.up('acl-detail-form');
		var aclgrid = btn.up('acl-layout').down('acl-grid');
        if (aclgrid.getSelectionModel().hasSelection()) {
            var record = aclgrid.getSelectionModel().getSelection()[0];
			me.selectACLRec(acldetailform,record);
        }
	}
});