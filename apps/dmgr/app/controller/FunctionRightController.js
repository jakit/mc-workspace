Ext.define('DMGR.controller.FunctionRightController', {
    extend: 'Ext.app.Controller',
	requires:['DMGR.view.functionright.FunctionalRightGridMenu'/*,
			  'DMGR.view.acl.EntityPickerPanel'*/],
    refs: {
        functionrightgrid  : 'functionright-grid',
		functionrightlayout : 'functionright-layout',
		functionrighteastpanel : 'functionright-layout #functionright-east-panel',
		functionalrightgridmenu : 'functionalrightgridmenu'/*,
		entitypickerpanel : 'entitypickerpanel'*/
    },

    init: function(application) {
		this.control({
			'functionrightgrid':{
				afterrender: this.afterFunctionRightGridRender,
				select : this.onFunctionRightGridItemSelect,
                rowcontextmenu: this.onFunctionRightGridRightClick
			},
			'functionalrightgridmenu':{
				click:this.functionRightGridMenuClick
			},
			'#functionright-east-panel button[action="save"]':{
				 click: this.onFunctionRightSaveClick
			},
			'#functionright-east-panel button[action="reset"]':{
				 click: this.onFunctionRightResetClick
			},
			'#functionright-east-panel button[action="entitypickbtn"]':{
				 click: this.onEntityPickBtnClick
			},
			'functionright-layout functionright-detail-form field':{
				 change: this.functionRightDetailFormFieldChange
			}
		});
    },
	
	afterFunctionRightGridRender: function(functionrightgrid){
		var me = this;	
		me.loadFunctionRight(functionrightgrid);
	},
	
	loadFunctionRight: function(functionrightgrid) {
        var me = this;		
		var functionright_st = functionrightgrid.getStore();
        var functionrightproxy = functionright_st.getProxy();
		var functionrightproxyCfg = functionrightproxy.config;
        functionrightproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		functionright_st.setProxy(functionrightproxyCfg);
        functionright_st.load({
            scope: this,
            callback: function(records, operation, success) {
                if(success && records.length > 0){            
  	        		functionrightgrid.getSelectionModel().select(0);
                }
            }
        });
    },
	
	onFunctionRightGridItemSelect: function(selModel, record, index, eOpts){
		console.log("FunctionRight Grid Item is Selected.");
		var me = this;
		var functionrightdetailform = selModel.view.ownerCt.up('functionright-layout').down('functionright-detail-form');
		functionrightdetailform.loadRecord(record);
		me.loadFunctionActivity(functionrightdetailform,record);
		functionrightdetailform.setTitle("Function Right Details");
		functionrightdetailform.getForm().findField('idfunction').setReadOnly(true);
	},
		
	onFunctionRightGridRightClick:  function(view, record, tr, rowIndex, e, eOpts) {
        console.log("FunctionRight Grid Right Click");
        var me = this;
        e.stopEvent();
        console.log("Show Menu for Function Right Grid");
        if(view.up('functionright-grid').contextMenu){
            view.up('functionright-grid').contextMenu.showAt(e.getXY());
			var functionrightmenu = view.up('functionright-grid').contextMenu;
			view.up('functionright-grid').contextMenu.ownerCmp = view.up('functionright-grid');
        }
    },
	
	functionRightGridMenuClick: function(menu, item, e, eOpts){
		console.log("FunctionRight Grid Sub Menu Clicked");
		var me = this;
		if(item){
			if(item.action === "deletefunctionalright"){
				console.log("Delete Functional Right");
				me.deleteFunctionalRight(menu.ownerCmp);
			} else if(item.action === "modifyfunctionalright"){
				console.log("Modify Functional Right");
				me.modifyFunctionalRight(menu.ownerCmp);
			} else if(item.action === "addfunctionalright"){
				console.log("Add Functional Right");
				me.addFunctionalRight(menu.ownerCmp);
			}
		}
	},
	
	deleteFunctionalRight: function(functionalrightgrid){
		var me = this,idFunRight;
        if (functionalrightgrid.getSelectionModel().hasSelection()) {
            var record = functionalrightgrid.getSelectionModel().getSelection()[0];
			idFunRight = record.get('idFunRight');
        }		     
                
        Ext.Msg.show({
            title:'Delete Functional Right?',
            message: 'Are you sure you want to delete the Functional Right?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
					functionalrightgrid.setLoading(true);
                    Ext.Ajax.request({
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
                        },
                        method:'DELETE', //Delete Method with primary key
                        url:DMGR.view.AppConstants.apiurl+'mssql:FunRight/'+idFunRight+'?checksum=override',
                        scope:this,
                        success: function(response, opts) {
                            console.log("Delete Functional Right is done.");
							functionalrightgrid.setLoading(false);
                            var obj = Ext.decode(response.responseText);
                            if(obj.statusCode == 200) {
								me.loadFunctionRight(functionalrightgrid);
                            } else {
                                console.log("Delete Functional Right Failure.");
                                Ext.Msg.alert('Delete Functional Right failed', obj.errorMessage);
                            }
                        },
                        failure: function(response, opts) {
                            console.log("Delete Functional Right Failure.");
							functionalrightgrid.setLoading(false);
                            var obj = Ext.decode(response.responseText);
                            Ext.Msg.alert('Delete Functional Right failed', obj.errorMessage);
                        }
                    });
                }
            }
        });
	},
	
	doFunctionRightFormWritable: function(detailform){
		var me = this;		
		detailform.down('button[action="save"]').show();
		detailform.down('button[action="reset"]').show();
		detailform.down('button[action="save"]').disable();
		detailform.down('button[action="reset"]').disable();
	},
	
	onFunctionRightSaveClick: function(btn){
		var me = this,defaultACLRightTotal=0;
		var functionrightlayout = btn.up('functionright-layout');
		var functionrightdetailform = functionrightlayout.down('functionright-detail-form');		
        var functionrightForm = functionrightdetailform.getForm();
        var idFunRight = functionrightForm.findField('idFunRight').getValue();
		var assignedRightVal = functionrightForm.findField('assignedRight').getValue();
		if(assignedRightVal==true){
			assignedRightVal = 1;
		} else {
			assignedRightVal = 0;
		}
		var data = {
			'entity'   : functionrightForm.findField('entity').getValue(),
			'assignedRight' : assignedRightVal
		};

        if(idFunRight===0 || idFunRight===null || idFunRight==="") {
            console.log("Add New Function Right");
            var methodname = "POST";
			data['idFunction'] = functionrightForm.findField('idfunction').getValue();
        } else {
            console.log("Modify Function Right");
            data['idFunRight'] = idFunRight;
            data['@metadata'] = {'checksum' : 'override'};
            var methodname = "PUT";
        }

        functionrightdetailform.setLoading(true);
        Ext.Ajax.request({
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
            },
            url:DMGR.view.AppConstants.apiurl+'mssql:FunRight',
            params: Ext.util.JSON.encode(data),
            scope:this,
            method:methodname,
            success: function(response, opts) {
                functionrightdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                if(obj.statusCode == 200 || obj.statusCode == 201){
					me.loadFunctionRight(functionrightlayout.down('functionright-grid'));
                } else {
                    console.log("Function Right Failure.");
                    Ext.Msg.alert('Function Right Save failed', obj.errorMessage);
                }
            },
            failure: function(response, opts) {
                functionrightdetailform.setLoading(false);
                var obj = Ext.decode(response.responseText);
                Ext.Msg.alert('Save Function Right', obj.errorMessage);
            }
        });
	},
	
	onFunctionRightResetClick: function(btn){
		var me = this;
		var acldetailform = btn.up('acl-detail-form');
		var aclgrid = btn.up('acl-layout').down('acl-grid');
        if (aclgrid.getSelectionModel().hasSelection()) {
            var record = aclgrid.getSelectionModel().getSelection()[0];
			acldetailform.loadRecord(record);
        }
		acldetailform.down('button[action="save"]').disable();
		acldetailform.down('button[action="reset"]').disable();
	},
	
	onEntityPickBtnClick: function(btn){
		var me = this,entitypickerpanel;
		console.log("Entity Picker button Click");
		me.getController('AclController').showEntityPickPanel(btn);
	},
	
	functionRightDetailFormFieldChange: function(field , newValue , oldValue , eOpts){
		var me = this;
		var functionrightdetailform = field.up('functionright-detail-form');
		functionrightdetailform.down('button[action="save"]').enable();
		functionrightdetailform.down('button[action="reset"]').enable();
	},
	
	loadFunctionActivity: function(functionrightdetailform,record){
		var me = this;
		////Load Entity By User
		var idFunction = functionrightdetailform.getForm().findField('idfunction');
		var idFunctionSt = idFunction.getStore();		
        var idFunctionStproxy = idFunctionSt.getProxy();
        idFunctionStproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
		
        idFunctionSt.load({
			callback: function(){
				idFunction.setValue(record.get('idFunction'));
				me.doFunctionRightFormWritable(functionrightdetailform);
			}
		});
		///////////////////////
	},
	
	modifyFunctionalRight: function(functionalrightgrid){
		var me = this;
		var functionrightlayout = functionalrightgrid.up('functionright-layout');
		var functionrightdetailform = functionrightlayout.down('functionright-detail-form');

        if (functionalrightgrid.getSelectionModel().hasSelection()) {
            var record = functionalrightgrid.getSelectionModel().getSelection()[0];
			functionrightdetailform.loadRecord(record);
        }
		functionrightdetailform.setTitle("Modify Function Right");
		functionrightdetailform.getForm().findField('idfunction').setReadOnly(true);
		me.doFunctionRightFormWritable(functionrightdetailform);
	},
	
	addFunctionalRight: function(functionalrightgrid){
		var me = this;
		var functionrightlayout = functionalrightgrid.up('functionright-layout');
		var functionrighteastpanel = functionrightlayout.down('#functionright-east-panel');
		var functionrightdetailform = functionrightlayout.down('functionright-detail-form');
		
		functionrightdetailform.getForm().reset();
		functionrightdetailform.setTitle("Add Function Right");
		functionrightdetailform.getForm().findField('idfunction').setReadOnly(false);
		me.doFunctionRightFormWritable(functionrightdetailform);
	}
});