Ext.define('DMGR.controller.RightsController', {
    extend: 'Ext.app.Controller',
	requires:[],
    refs: {
        rightsgrid  : 'rights-grid',
		rightslayout : 'rights-layout',
		rightseastpanel : 'rights-layout #rights-east-panel'
    },

    init: function(application) {
		this.control({
			'rightsgrid':{
				afterrender: this.afterRightsGridRender,
				select : this.onRightsGridItemSelect
			},
			'#rights-east-panel button[action="save"]':{
				 click: this.onRightSaveClick
			},
			'#rights-east-panel button[action="reset"]':{
				 click: this.onRightResetClick
			},
			'rights-layout rights-detail-form field':{
				 change: this.rightsDetailFormFieldChange
			}
		});
    },
	
	afterRightsGridRender: function(rightgrid){
		var me = this;	
		me.loadRights(rightgrid);
	},
	
	loadRights: function(rightgrid) {
        var me = this;		
		var rights_st = rightgrid.getStore();
        var rightsproxy = rights_st.getProxy();
		var rightsproxyCfg = rightsproxy.config;
        rightsproxyCfg.headers = {
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        };
		rights_st.setProxy(rightsproxyCfg);
        rights_st.load({
            scope: this,
            callback: function(records, operation, success) {
                if(success && records.length > 0){
                	rightgrid.getSelectionModel().select(0);
                }
            }
        });
    },
	
	onRightsGridItemSelect: function(selModel, record, index, eOpts){
		console.log("Right Grid Item is Selected.");
		var me = this,filterstarr = [];
		var rightsdetailform = selModel.view.ownerCt.up('rights-layout').down('rights-detail-form');
		rightsdetailform.loadRecord(record);
		var rightseastpanel = rightsdetailform.up('#rights-east-panel');
		rightseastpanel.down('button[action="save"]').disable();
		////Load Entity By User
		var defaultACLEntity = rightsdetailform.getForm().findField('defaultACLEntity');
		var defaultACLEntitySt = defaultACLEntity.getStore();		
        var defaultACLEntityStproxy = defaultACLEntitySt.getProxy();
        defaultACLEntityStproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
		
        defaultACLEntitySt.load({
			params:{
				"arg.User" :record.get('userName')
			}
		});
		///////////////////////
	},
	
	onRightSaveClick: function(btn){
		var me = this,defaultACLRightTotal=0;
		console.log("Rights Save Button Click.");
		var rightseastpanel = btn.up('#rights-east-panel');
		var rightsdetailform = rightseastpanel.down('rights-detail-form');
		var rightslayout = btn.up('rights-layout');
		var rightsgrid = rightslayout.down('rights-grid');
		
		var defList  = rightsdetailform.getForm().findField('defList').getValue();
		var defOpen  = rightsdetailform.getForm().findField('defOpen').getValue();
		var defModify  = rightsdetailform.getForm().findField('defModify').getValue();
		var defDelete  = rightsdetailform.getForm().findField('defDelete').getValue();
		var defControl = rightsdetailform.getForm().findField('defControl').getValue();
		
		if(defList==true){
			defaultACLRightTotal = defaultACLRightTotal + 1;
		}
		if(defOpen==true){
			defaultACLRightTotal = defaultACLRightTotal + 2;
		}
		if(defModify==true){
			defaultACLRightTotal = defaultACLRightTotal + 4;
		}
		if(defDelete==true){
			defaultACLRightTotal = defaultACLRightTotal + 8;
		}
		if(defControl==true){
			defaultACLRightTotal = defaultACLRightTotal + 16;
		}
		var data = {
			'defaultACLEntity' : rightsdetailform.getForm().findField('defaultACLEntity').getValue(),
			'defaultACLRight' : defaultACLRightTotal,
			'idUser' : rightsdetailform.getForm().findField('idUser').getValue(),
			'@metadata': { 'checksum': 'override' }
		};
		rightsdetailform.setLoading(true);
		Ext.Ajax.request({
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
			},
			url:DMGR.view.AppConstants.apiurl+'mssql:Users',
			method:'PUT',
			params: Ext.util.JSON.encode(data),
			scope:this,
			success: function(response, opts) {
				console.log("Edit Rights is saved.");
				rightsdetailform.setLoading(false);
				var obj = Ext.decode(response.responseText);
				if(obj.statusCode == 200){
					//var dealdetailpropertyfieldset = detailpanel.down('dealdetailpropertyfieldset');
					//me.loadPropertyDetails(dealdetailpropertyfieldset);
					//me.editDealPropertyFormCloseBtnClick(editdealpropertyform);
					me.loadRights(rightsgrid);
				} else {
					console.log("Rights Save Failure.");
					Ext.Msg.alert('Right save failed', obj.errorMessage);
				}
			},
			failure: function(response, opts) {
				console.log("Save Rights Failure.");
				rightsdetailform.setLoading(false);
				var obj = Ext.decode(response.responseText);
				var errMsg = obj.errorMessage;
				Ext.Msg.alert('Right save failed', obj.errorMessage);
			}
		});
	},
	
	rightsDetailFormFieldChange: function(field , newValue , oldValue , eOpts){
		var me = this;
		var rightseastpanel = field.up('#rights-east-panel');
		rightseastpanel.down('button[action="save"]').enable();
	},
	
	onRightResetClick: function(btn){
		var me = this;
		console.log("On RIght Reset Clcik");
		var rightsdetailform = btn.up('rights-layout').down('rights-detail-form');
		var rightsgrid = btn.up('rights-layout').down('rights-grid');
        if (rightsgrid.getSelectionModel().hasSelection()) {
            var record = rightsgrid.getSelectionModel().getSelection()[0];
			rightsdetailform.loadRecord(record);
        }
		var rightseastpanel = btn.up('#rights-east-panel');
		rightseastpanel.down('button[action="save"]').disable();
	}
});