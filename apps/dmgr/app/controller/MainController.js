Ext.define('DMGR.controller.MainController', {
    extend: 'Ext.app.Controller',
	requires: [
		'DMGR.view.login.LoginContainer'
	],
    refs: {
        logincontainer: {
            autoCreate: true,
            selector: 'logincontainer',
            xtype: 'logincontainer'
        },
        mainviewport: 'mainviewport',
        topmenutoolbar: 'topmenutoolbar'
    },

    init: function(application) {
        this.control({
            'logincontainer field[name="username"]': {
                specialkey: this.enterBtnPressed
            },
            'logincontainer field[name="password"]': {
                specialkey: this.enterBtnPressed
            },
            'logincontainer #loginbtn': {
                click: this.loginBtnClick
            },
            'logincontainer #resetbtn': {
                click: this.resetBtnClick
            }
        });
    },
	
	onLaunch: function(){
		var me = this;
		console.log("Api Url : "+DMGR.view.AppConstants.apiurl);
		//console.log(DMGR.view.AppConstants.apiurl);
        var apiKeyVal = Ext.util.Cookies.get("dmgr-apikey-"+DMGR.view.AppConstants.apiurl);
		console.log("apiKeyVal"+apiKeyVal);
		//console.log(Ext.util.Cookies.clear("dmgr-apikey-"+DMGR.view.AppConstants.apiurl));
		if(apiKeyVal && apiKeyVal!=null){
            console.log("Api Key exist. So check whether it is valid or not?");
			var keyexpiretimeval = Ext.util.Cookies.get("dmgr-keyexpiretime-"+DMGR.view.AppConstants.apiurl);
			var expdate  = new Date(keyexpiretimeval);
			var currdate = new Date();
			console.log(expdate);
			console.log(currdate);
			
			if (expdate > currdate) {
				console.log("Expdate is greater than today date");
				me.reloadOnExpire(); /// Set the time interval for calling Reload on expire of token
				me.getMainviewport().config.apikey = apiKeyVal;
				
				me.loadFunctionalPermission();
			} else {
				Ext.util.Cookies.clear("dmgr-apikey-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-username-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-userid-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-keyexpiretime-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-apiserver-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-logintime-"+DMGR.view.AppConstants.apiurl);
				me.showLoginView();
			}			
        } else {
            me.showLoginView();
        }
	},
	
	reloadOnExpire: function(){
		var me = this;
		console.log("Reload Page on Expire");
		var keyexpiretimeval = Ext.util.Cookies.get("dmgr-keyexpiretime-"+DMGR.view.AppConstants.apiurl);
		var expdate  = new Date(keyexpiretimeval);
		var currdate = new Date();
		console.log(expdate);
		console.log(currdate);
		
		if (expdate > currdate) {
			console.log("expdate > currdate");
			var rmTime = expdate - currdate;
			console.log("Expire Function will be called after : "+rmTime);
			setTimeout(function(){ 
				//console.log("Expire Function will be called after : "+rmTime);				
				Ext.util.Cookies.clear("dmgr-apikey-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-username-"+DMGR.view.AppConstants.apiurl);
				//Ext.util.Cookies.clear("dmgr-userid-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-keyexpiretime-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-apiserver-"+DMGR.view.AppConstants.apiurl);
				Ext.util.Cookies.clear("dmgr-logintime-"+DMGR.view.AppConstants.apiurl);
				window.location.reload();
			}, rmTime);
			//}, 30000);
		}
	},
	
    showLoginView: function() {
        var me = this;
        var mainviewport = me.getMainviewport();
        var logincnt = me.getLogincontainer();

		mainviewport.removeAll();
		mainviewport.add(logincnt);
		
        //mainviewport.getLayout().setActiveItem(logincnt);
        logincnt.down('form').getForm().reset();
		
		var useridVal = Ext.util.Cookies.get("dmgr-userid-"+DMGR.view.AppConstants.apiurl);
		console.log("useridVal"+useridVal);
		//console.log(Ext.util.Cookies.clear("dmgr-apikey-"+DMGR.view.AppConstants.apiurl));
		if(useridVal && useridVal!=null){
			logincnt.down('form').getForm().findField('username').setValue(useridVal);
		}
		logincnt.down('form').getForm().findField('username').focus();
    },

    loginBtnClick: function(btn) {
        console.log("Login Button Clicked");
        var me = this;
        var form = this.getLogincontainer().down('form').getForm();
        if (form.isValid()) {
            console.log("Form Is Valid.");
            me.getLogincontainer().down('form').setLoading(true);
            Ext.Ajax.request({
                headers: { 'Content-Type': 'application/json' },
                url:DMGR.view.AppConstants.apiurl+'@authentication',
                params: Ext.util.JSON.encode(form.getValues()),
                scope:this,
                success: function(response, opts) {
                    console.log("Login Success.");
                    me.getLogincontainer().down('form').setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    //Ext.Msg.alert('Success', action.result.msg);
                    if(obj.apikey){
                        me.getMainviewport().config.apikey = obj.apikey;
                        //Set the Api Key as cookie var.
                        console.log(obj.expiration);
                        var expireDate = new Date(obj.expiration);
                        Ext.util.Cookies.set("dmgr-apikey-"+DMGR.view.AppConstants.apiurl,obj.apikey,expireDate);
                        Ext.util.Cookies.set("dmgr-username-"+DMGR.view.AppConstants.apiurl,obj.name,expireDate);
                        Ext.util.Cookies.set("dmgr-userid-"+DMGR.view.AppConstants.apiurl,form.findField('username').getValue(),expireDate);
						Ext.util.Cookies.set("dmgr-keyexpiretime-"+DMGR.view.AppConstants.apiurl,expireDate,expireDate);
						Ext.util.Cookies.set("dmgr-apiserver-"+DMGR.view.AppConstants.apiurl,obj.apiserver,expireDate);
						var d = new Date();
						Ext.util.Cookies.set("dmgr-logintime-"+DMGR.view.AppConstants.apiurl,d,expireDate);								
			
						me.loadFunctionalPermission();
						me.reloadOnExpire(); /// Set the time interval for calling Reload on expire of token						
                    } else {
                        Ext.Msg.alert('Failed', "Please enter correct username & password.");
                    }
                },
                failure: function(response, opts) {
                    console.log("Login Failure.");
                    me.getLogincontainer().down('form').setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    var errmsg = obj.errorMessage.replace("API key not found or not authorized: ", '');
                    Ext.Msg.alert('Login Failed', errmsg);
                }
            });
        }
    },

    enterBtnPressed: function(field, e) {
        // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
        // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
        if (e.getKey() == e.ENTER) {
            this.loginBtnClick();
        }
    },

    resetBtnClick: function(me) {
        this.getLogincontainer().down('form').getForm().reset();
		this.getLogincontainer().down('form').getForm().findField('username').focus();
    },
	
	loadFunctionalPermission: function(){
		var me = this;
		console.log("Load Functional Permission");
		
		var funPermStore = Ext.getStore('FunctionalPermissions');
		me.getMainviewport().config.funPermStore = funPermStore;
		
		funPermStore.getProxy().setHeaders({
			Accept: 'application/json',
			Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
		});
		me.getMainviewport().setLoading(true);
		funPermStore.load({
			scope: this,
			callback: function(records, operation, success) {
				me.getMainviewport().setLoading(false);
				if(success){
					me.createMainViewPortItems();
				}
			}
		});
	},
	
	createMainViewPortItems: function(){
		var me = this,btn,dashboardcontainer,activeItem;
		me.getController('DashboardController').showDashboardContainer();	
		// Set the Username Value
		var usernameval = "Hi, "+Ext.util.Cookies.get("dmgr-username-"+DMGR.view.AppConstants.apiurl);
		me.getController('DashboardController').setUserNameText(usernameval);
	}
});