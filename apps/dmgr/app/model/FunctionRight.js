Ext.define('DMGR.model.FunctionRight', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idFunRight'
        },
        {
            name: 'idFunction'
        },
        {
            name: 'guiComponent'
        },
        {
            name: 'col'
        },
        {
            name: 'functionActivity'
        },
        {
            name: 'description'
        },
		{
            name: 'defaultRight'
        },
		{
            name: 'entity'
        },
		{
            name: 'assignedRight'
        },
		{
            name: 'modifyUserName'
        },
		{
            name: 'modifyDateTime'
        }
    ]
});