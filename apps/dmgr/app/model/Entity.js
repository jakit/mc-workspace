Ext.define('DMGR.model.Entity', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'value'
        },
		{
            name: 'isGroup'
        }
    ]
});