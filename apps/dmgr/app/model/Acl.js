Ext.define('DMGR.model.Acl', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'objID'
        },
        {
            name: 'objType'
        },
        {
            name: 'objDesc'
        },
        {
            name: 'idACL'
        },
        {
            name: 'entityName'
        },
        {
            name: 'isGroup'
        },
		{
            name: 'rights'
        },
		{
            name: 'list'
        },
		{
            name: 'open'
        },
		{
            name: 'modify'
        },
		{
            name: 'delete'
        },
		{
            name: 'control'
        }
    ]
});