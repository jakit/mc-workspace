Ext.define('DMGR.store.FunctionalPermissions', {
    extend: 'Ext.data.Store',

    requires: [
        'DMGR.model.FunctionalPermission',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DMGR.model.FunctionalPermission',
            proxy: {
                type: 'rest',
				//pageParam : '',
				//limitParam : '',
				//startParam : '',
                noCache: false,
                url: DMGR.view.AppConstants.apiurl+'FuncRights',
				listeners: {
					exception: function(proxy, response, options){
					}
				}
            }
        }, cfg)]);
    }
});