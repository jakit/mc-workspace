Ext.define('DMGR.store.Functions', {
    extend: 'Ext.data.Store',

    requires: [
        'DMGR.model.Function',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DMGR.model.Function',
			autoLoad:false,
            proxy: {
                type: 'rest',
				//pageParam : '',
				//limitParam : '',
				//startParam : '',
                noCache: false,
                url: DMGR.view.AppConstants.apiurl+'mssql:Functions',
				reader: {
					type: 'json'
				}
            }
        }, cfg)]);
    }
});