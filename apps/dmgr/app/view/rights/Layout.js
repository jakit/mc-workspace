Ext.define('DMGR.view.rights.Layout', {
    extend: 'DMGR.view.component.BaseLayoutContainer',
    xtype: 'rights-layout',
    requires: [
        'DMGR.view.rights.Grid',
		'DMGR.view.rights.DetailForm'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'rights-grid' 
        };
        
        me.eastRegion = {
			xtype: 'panel',
			itemId:'rights-east-panel',
			layout:{
				type:'card'
			},
			title:'Right Details',
			header : {
				titlePosition: 0,
				items: [
					{
						xtype: 'button',
						disabled:true,
						margin: '0 10 0 0',
						action: 'save',
						text: 'Save'
					},
					{
						xtype: 'button',						
						action: 'reset',
						text: 'Reset'
					}					
				]
			},
			items:[{
				xtype: 'rights-detail-form',
				layout: {
					type: 'vbox',
					align:'stretch'
				}
		   }]
        };
       
        me.callParent(arguments);
    }
});