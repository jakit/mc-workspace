Ext.define('DMGR.view.component.SelectionGridNew', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cmp-grid-selectiongridnew',
    requires: [
        'Ext.grid.column.Column',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.XTemplate',
        'Filters2.HeaderFields'        
    ],
	mixins: ['Filters2.InjectHeaderFields', 'Filters2.FiltersMixin'],
    scrollable: "y",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        //me.header = false;
        me.cls = 'gridcommoncls';
		me.emptyText  = "No data is available";
        //me.frame = true;
        me.multiColumnSort = true;
        
        me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        var cfg = me.injectHeaderFields({
            useToolbar: false
            // hideable: true,
            // hidden: false,
            // position: "inner",
            // dockPosition: 'right',
            // buttons: me.filterButtons // comes from FiltersMixin.js
        });
        var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];            
        me.callParent(arguments);  
   }    
});