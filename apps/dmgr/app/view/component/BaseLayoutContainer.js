Ext.define('DMGR.view.component.BaseLayoutContainer', {
    extend: 'Ext.panel.Panel',
    xtype: 'tab-baselayoutcontainer',
    requires: [
        'Ext.layout.container.Border',
        'Ext.layout.container.Fit' 
    ],
    centerRegion: {},
    southRegion: {},
    eastRegion: {},
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this,
        centerRegion, southRegion, eastRegion;

        me.layout = {
            type: "border"
        };
        centerRegion = {
            region: 'center'
        };
        Ext.apply(centerRegion, me.centerRegion);
                
        eastRegion = {
            region: 'east',
            width: 400,
            collapsible: true,
            split: true,
            layout: {
                type: 'fit'
            }
        };
        Ext.apply(eastRegion, me.eastRegion);
        
        me.items = [{
            region: 'center',
            xtype: 'container',
            layout: {
                type: 'border'
            },
            items: [ centerRegion]
        }, eastRegion ];
        
        me.callParent(arguments);
    }
});            