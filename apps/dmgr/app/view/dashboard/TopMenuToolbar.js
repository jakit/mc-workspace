Ext.define('DMGR.view.dashboard.TopMenuToolbar', {
    extend : 'Ext.toolbar.Toolbar',
    alias : 'widget.topmenutoolbar',
    requires : [
        'Ext.Img', 
        'Ext.button.Split', 
        'Ext.menu.Menu', 
        'Ext.menu.Item', 
        'Ext.form.field.Text', 
        'Ext.toolbar.Fill', 
        'Ext.toolbar.TextItem'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.ui = 'menutoolbarui';
        me.items = [];        
        
        me.callParent(arguments);
    }
});