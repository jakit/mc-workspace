Ext.define('DMGR.view.functionright.Layout', {
    extend: 'DMGR.view.component.BaseLayoutContainer',
    xtype: 'functionright-layout',
    requires: [
        'DMGR.view.functionright.Grid',
		'DMGR.view.functionright.DetailForm'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'functionright-grid' 
        };
        
        me.eastRegion = {
			xtype: 'panel',
			itemId:'functionright-east-panel',
			layout:{
				type:'card'
			},
			title:'Function Right Details',
			header:false,
			items:[{
				xtype: 'functionright-detail-form',
				layout: {
					type: 'vbox',
					align:'stretch'
				}
		   }]
        };
       
        me.callParent(arguments);
    }
});