Ext.define('DMGR.view.functionright.DetailForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.functionright-detail-form',

    requires: [
        'Ext.form.field.Number',
        'Ext.panel.Panel',
        'Ext.button.Button',
		'DMGR.store.EntityByUsers',
		'DMGR.store.Functions'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
		var EntityByUsersSt = Ext.create('DMGR.store.EntityByUsers');
		var FunctionSt = Ext.create('DMGR.store.Functions');
		me.frame = true;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.defaults = {
			labelWidth: 100
		};
		me.bodyPadding = 5;
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					disabled:true,
					margin: '0 10 0 0',
					action: 'save',
					text: 'Save'
				},
				{
					xtype: 'button',						
					action: 'reset',
					text: 'Reset'
				}					
			]
		};
		me.title = "Function Right Details";
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		me.items = [
			{
				xtype: 'textfield',
				hidden:true,
				readOnly:true,		
				fieldLabel: 'FunRight ID',
				name: 'idFunRight'
			},
			{
				xtype: 'combobox',
				readOnly:true,
				store:FunctionSt,
				displayField:'functionActivity',
				valueField:'idfunction',
				queryMode:'local',
				anchor: '100%',
				fieldLabel: 'FunctionActivity',
				name: 'idfunction'
			},
			{
				layout:{
					type:'hbox',
					align:'stretch'
				},
				margin:'0 0 5 0',
				items:[{
					xtype: 'textfield',
					flex:1,
					anchor: '100%',
					labelWidth: 100,
					fieldLabel: 'Entity Name',
					name: 'entity'
				},
				{
					xtype: 'button',
					iconCls:'dots-three-horizontal',
					margin:'0 0 0 5',
					action:'entitypickbtn'
				}]                 
			},
			{
				xtype: 'checkboxfield',
				flex:1,
				fieldLabel: 'AssignedRight',
				//labelAlign: 'right',
				name: 'assignedRight'
			}
		];
		me.callParent(arguments);
    }
});