Ext.define('DMGR.view.acl.Layout', {
    extend: 'DMGR.view.component.BaseLayoutContainer',
    xtype: 'acl-layout',
    requires: [
        'DMGR.view.acl.Grid',
		'DMGR.view.acl.DetailForm'
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        
        me.style = {
            style: "border: 10px solid #e9f1fb"
        };
        
        me.centerRegion = {
           xtype: 'acl-grid' 
        };
        
        me.eastRegion = {
			xtype: 'panel',
			itemId:'acl-east-panel',
			layout:{
				type:'card'
			},
			header:false,
			items:[{
				xtype: 'acl-detail-form',
				layout: {
					type: 'vbox',
					align:'stretch'
				}
		   }]
        };
       
        me.callParent(arguments);
    }
});