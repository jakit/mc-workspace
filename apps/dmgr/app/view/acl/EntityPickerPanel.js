Ext.define('DMGR.view.acl.EntityPickerPanel', {
    extend: 'DMGR.view.component.SelectionGridNew',
    xtype: 'entitypickerpanel',
    requires: ['DMGR.view.component.SelectionGridNew',
			   'DMGR.store.Entity'],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
        me.store = Ext.create('DMGR.store.Entity');
		me.closable = true;
	    me.title = 'Select Entity';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					margin: '0 10 0 0',
					action: 'selectEntityBtn',
					text: 'Select'
				}
			]
		};
		me.columns = [{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'value',
			text: 'Name',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		},
		{
			xtype: 'gridcolumn',
			renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			},
			dataIndex: 'isGroup',
			text: 'Is Group',
			flex: 1,
			headerField: this.setFilterField("stringfield", {
				grid: me,
				filterMode: "Contains",
				filterDataType: "string"
			})
		}];       
        me.callParent(arguments);
    }
});