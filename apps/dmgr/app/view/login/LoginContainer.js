Ext.define('DMGR.view.login.LoginContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.logincontainer',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],

	region:'center',
    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
    },
    items: [
        {
            xtype: 'form',
            style: 'border: 1px solid #cecece;',
            width: 450,
            bodyBorder: true,
            bodyPadding: 10,
            title: 'DealManager - Login',
            standardSubmit: false,
			layout:{
				type:'hbox',
				align:'stretch'
			},
			items:[{
				flex:1,
				layout:{
					type:'vbox',
					align:'stretch'
				},
				items:[{
					xtype: 'textfield',
					labelWidth:70,
					anchor: '100%',
					fieldLabel: 'Username',
					name: 'username',
					allowBlank: false,
					enableKeyEvents: true
				},
				{
					xtype: 'textfield',
					labelWidth:70,
					anchor: '100%',
					fieldLabel: 'Password',
					name: 'password',
					inputType: 'password',
					allowBlank: false,
					enableKeyEvents: true
				}]
			},
			{
				xtype : 'image',
				margin:'5 0 0 15',
				//dock: 'right',
				autoEl : {
					tag : 'a',
					href : '/DealMaker/',
					target : '_self'
				},
				//height : 40,
				width : 133,
				src : 'resources/images/DealLogoSmall.png'
			}],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    defaults: {
                        minWidth: 75
                    },
                    items: [
                        {
                            xtype: 'component',
                            flex: 1
                        },
                        {
                            xtype: 'button',
                            formBind: true,
							disabled: true,
                            itemId: 'loginbtn',
                            text: 'Login'
                        },
                        {
                            xtype: 'button',
                            itemId: 'resetbtn',
                            text: 'Reset'
                        }
                    ]
                }
            ]
        }
    ]

});