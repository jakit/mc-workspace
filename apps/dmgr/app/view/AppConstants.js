Ext.define('DMGR.view.AppConstants', {
    extend: 'Ext.Base',

    singleton: true,

	apiurl: '',
	constructor: function(cfg) {
        var me = this;
		console.log("Initial Api Url Value : "+me.apiurl);
		var apiurlval = "";
		Ext.Ajax.request({
			 headers: {
				"Content-Type": "application/xml"
			 },
			 async : false, 
			 url: 'resources/liveapi.xml',
			 success: function (response) {
				var myxml = response.responseText;
				var xmlDoc = me.parseToXml(myxml);
				var itemcnt = xmlDoc.getElementsByTagName("item");
				if(itemcnt.length!=0){
					for(j=0;j<itemcnt.length;j++){
						apiurlval = itemcnt[j].getElementsByTagName("apiurl")[0].childNodes[0].nodeValue;
					}
				}
				console.log("Apiurl Frm Xml : "+apiurlval);
				me.apiurl = apiurlval;
				console.log("New Api Url Value : "+me.apiurl);
			},
			failure: function (response) {
				console.log("Fail to load an XML.");
			}
		});
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
	parseToXml: function(myxml){
		if (window.DOMParser){
			  var parser = new DOMParser();
			  xmlDoc = parser.parseFromString(myxml,"application/xml");
		} else { // Internet Explorer
			  xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
			  xmlDoc.async=false;
			  xmlDoc.loadXML(myxml); 
		}
		return xmlDoc;
	}
});