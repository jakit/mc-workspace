Ext.define('DMGR.view.main.MainViewport', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.mainviewport',
    requires: [
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'viewportcls';
        me.layout = {
            type: 'border'
        };
        me.items = [];
        me.callParent(arguments);
    }
});