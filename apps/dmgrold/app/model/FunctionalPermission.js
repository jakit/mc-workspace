Ext.define('DMGR.model.FunctionalPermission', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
		{
            name: 'idFunction'
        },
        {
            name: 'gui'
        },		
        {
            name: 'col'
        },
		{
            name: 'functionActivity'
        },
        {
            name: 'description'
        },
		{
            name: 'allowed'
        }
    ]
});