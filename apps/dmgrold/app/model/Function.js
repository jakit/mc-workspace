Ext.define('DMGR.model.Function', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
		{
            name: 'idfunction'
        },
        {
            name: 'guiComponent'
        },		
        {
            name: 'col'
        },
		{
            name: 'functionActivity'
        },
        {
            name: 'description'
        },
		{
            name: 'defaultRight'
        }
    ]
});