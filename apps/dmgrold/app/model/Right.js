Ext.define('DMGR.model.Right', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idUser'
        },
        {
            name: 'userName'
        },
        {
            name: 'defaultACLEntity'
        },
        {
            name: 'defaultACLRight'
        },
        {
            name: 'defList'
        },
        {
            name: 'defOpen'
        },
		{
            name: 'defModify'
        },
		{
            name: 'defDelete'
        },
		{
            name: 'defControl'
        }
    ]
});