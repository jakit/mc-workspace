Ext.define('DMGR.model.EntityByUser', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'value'
        }
    ]
});