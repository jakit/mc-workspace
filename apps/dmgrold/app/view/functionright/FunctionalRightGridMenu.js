Ext.define('DMGR.view.functionright.FunctionalRightGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.functionalrightgridmenu',
    initComponent: function() {
        var me = this;
		me.width = 170;
        me.items = [{
            xtype: 'menuitem',
            text: 'Delete Functional Right',
			action : 'deletefunctionalright'
        },
        {
            xtype: 'menuitem',
			action:'modifyfunctionalright',
            text: 'Modify Functional Right'
        },
        {
            xtype: 'menuitem',
            text: 'Add Functional Right',
            action: 'addfunctionalright'
        }];
        me.callParent(arguments);
    }
});