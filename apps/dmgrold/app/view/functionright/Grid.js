Ext.define('DMGR.view.functionright.Grid', {
    extend: 'DMGR.view.component.BaseGridPanelNew',
    xtype: 'functionright-grid',
    requires: ['DMGR.store.FunctionRights'],
    counter: 1,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
		me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
		me.store = Ext.create('DMGR.store.FunctionRights');
        me.viewConfig = {
            forceFit: true
        };
		
		me.contextMenu = Ext.create('DMGR.view.functionright.FunctionalRightGridMenu');
		
        me.columns = [{
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .6,
            sortable: true,
            dataIndex: 'idFunRight',
            text: 'FunRight ID',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },{
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .6,
            dataIndex: 'idFunction',
            text: 'Function ID',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'guiComponent',
            text: 'GuiComponent',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'col',
            text: 'Col',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'functionActivity',
            text: 'Function Activity',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .7,
            dataIndex: 'description',
            text: 'Description',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Equal",
                filterDataType: "string"
            })
        }, {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .6,
            dataIndex: 'defaultRight',
            text: 'Default Right',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },  {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
            },
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'entity',
            text: 'Entity',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'assignedRight',
            text: 'Assigned Right',
            flex: .5,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }];   

        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
		var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];
        me.callParent(arguments);
    }
});