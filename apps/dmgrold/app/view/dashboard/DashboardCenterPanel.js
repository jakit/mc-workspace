Ext.define('DMGR.view.dashboard.DashboardCenterPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'dashboardcenterpanel',
    requires: [
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.layout = {
            type: 'card'
        };        
        me.items = [];
        me.callParent(arguments);
    }
});