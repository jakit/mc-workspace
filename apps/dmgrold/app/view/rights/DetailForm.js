Ext.define('DMGR.view.rights.DetailForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.rights-detail-form',

    requires: [
        'Ext.form.field.Number',
        'Ext.panel.Panel',
        'Ext.button.Button',
		'DMGR.store.EntityByUsers'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
		var EntityByUsersSt = Ext.create('DMGR.store.EntityByUsers');
		me.frame = true;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.defaults = {
			labelWidth: 80
		};
		me.bodyPadding = 5;
		me.header = false;
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		me.items = [
			{
				xtype: 'textfield',
				//hidden:true,
				readOnly:true,		
				fieldLabel: 'User ID',
				name: 'idUser'
			},
			{
				xtype: 'textfield',
				anchor: '100%',
				readOnly:true,
				fieldLabel: 'User Name',
				name: 'userName'
			},
			{
				xtype: 'combobox',
				store:EntityByUsersSt,
				displayField:'value',
				valueField:'value',
				queryMode:'local',
				anchor: '100%',
				fieldLabel: 'defaultACLEntity',
				name: 'defaultACLEntity'
			},
			{
				xtype: 'textfield',
				hidden:true,
				anchor: '100%',
				readOnly:true,
				fieldLabel: 'defaultACLRight',
				name: 'defaultACLRight'
			},
			{
				//bodyPadding: 5,
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				margin:'0 0 5 0',
				items: [{
						xtype: 'checkboxfield',
						flex:1,
						fieldLabel: 'defList',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'defList'
					},{
						xtype: 'checkboxfield',
						flex:1,
						fieldLabel: 'defOpen',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'defOpen'
					},{
						xtype: 'checkboxfield',
						flex:1,
						fieldLabel: 'defModify',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'defModify'
					}]
			},
			{
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'checkboxfield',
					flex:1,
					fieldLabel: 'defDelete',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'defDelete'
				},
				{
					xtype: 'checkboxfield',
					flex:2,
					fieldLabel: 'defControl',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'defControl'
				}]
			}
		];
		me.callParent(arguments);
    }
});