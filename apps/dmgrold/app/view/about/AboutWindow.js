Ext.define('DMGR.view.about.AboutWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.aboutwindow',

    requires: [
        'Ext.window.Window'
    ],

    height: 340,
    width: 505,
    layout: {
		type:'vbox',
		align:'center'
	},
	title: 'About',
	padding:15,
    items: [{
            xtype : 'image',
			style:'background-color:#184f89',
            autoEl : {
                tag : 'a',
                href : '/DealMaker/',
                target : '_self'
            },
            height : 40,
            width : 230,
            src : 'resources/images/logo.png'
        },
        {
			itemId:'infopanel',
			style:'text-align:center'
        }
    ]

});