Ext.define('DMGR.view.acl.Grid', {
    extend: 'DMGR.view.component.BaseGridPanelNew',
    xtype: 'acl-grid',
    requires: ['DMGR.store.Acls',
			   'DMGR.view.acl.AclGridMenu'],
    counter: 1,
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this, resizeFeature, cfg, headerFieldsFeature;
        me.header = false;
		me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
		me.firstLoad = true;
        me.cls = 'deallistgridcls';
        me.ui = 'dealmenugridui';
		me.store = Ext.create('DMGR.store.Acls');
        me.viewConfig = {
            forceFit: true
        };
		
		me.contextMenu = Ext.create('DMGR.view.acl.AclGridMenu');
		
        me.columns = [{
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .6,
            sortable: true,
            dataIndex: 'idACL',
            text: 'ACL ID',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },{
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .9,
            dataIndex: 'objType',
            text: 'Obj Type',
			headerField: me.setFilterField("combocheckboxfield", {
            	grid:me,
                inlineData: [{
                    'display': "Contact",
                    'val': "Contact"
                }, {
                    'display': "Deal",
                    'val': "Deal"
                }, {
                    'display': "Doc",
                    'val': "Doc"
                }],
                filterDataType: "string"
            }) 
        }, {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'objID',
            text: 'Obj ID',
			headerField: me.setFilterField("stringfield", {
                grid: me
            })
        },
        {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'objDesc',
            text: 'Obj Desc',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'entityName',
            text: 'Entity Name',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }, {
			xtype:'checkcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'isGroup',
            text: 'Is Group',
			headerField: me.setFilterField("genericlistfield", {
                grid:me,
                inlineData: [{
                    'display': "true",
                    'val': "true"
                }, {
                    'display': "false",
                    'val': "false"
                }]
            })
        }, {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            cls: 'dealgridcolumncls',
            flex: .8,
            dataIndex: 'rights',
            text: 'Rights',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },  {
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
            },
            cls: 'dealgridcolumncls',
            flex: .5,
            dataIndex: 'list',
            text: 'List',
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'open',
            text: 'Open',
            flex: .5,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'modify',
            text: 'Modify',
            flex: .5,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'delete',
            text: 'Delete',
            flex: .5,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        },
		{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
            },
            cls: 'dealgridcolumncls',
            dataIndex: 'control',
            text: 'Control',
            flex: .5,
			headerField: me.setFilterField("stringfield", {
                grid: me,
                filterMode: "Contains",
                filterDataType: "string"
            })
        }];   

        var cfg = me.injectHeaderFields({
            hideable: true,
            hidden: false,
            position: "inner",
            dockPosition: 'right',
            buttons: me.filterButtons // comes from FiltersMixin.js
        });
		var headerFieldsFeature = Ext.create('Filters2.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];
        me.callParent(arguments);
    }
});