Ext.define('DMGR.view.acl.DetailForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.acl-detail-form',

    requires: [
        'Ext.form.field.Number',
        'Ext.panel.Panel',
        'Ext.button.Button'
    ],
	constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
		me.frame = true;
		//me.scrollable = true;
		me.ui = 'activitypanel';
		me.defaults = {
			labelWidth: 80
		};
		me.bodyPadding = 5;
		//me.header = false;
		me.layout = {
			type: 'vbox',
			align: 'stretch'
		};
		me.title = 'ACL Details';
		me.header = {
			titlePosition: 0,
			items: [
				{
					xtype: 'button',
					hidden:true,
					disabled:true,
					margin: '0 10 0 0',
					action: 'save',
					text: 'Save'
				},
				{
					xtype: 'button',
					hidden:true,
					action: 'reset',
					text: 'Reset'
				},
				{
					xtype: 'button',
					hidden:true,
					margin: '0 0 0 10',
					scale:'medium',
					ui:'closebtn',
					action: 'close',
					iconCls:'close'
				}
			]
		};
		me.items = [
			{
				xtype: 'textfield',
				//hidden:true,
				readOnly:true,		
				fieldLabel: 'ACL ID',
				name: 'idACL'
			},
			{
				xtype: 'textfield',
				anchor: '100%',
				readOnly:true,
				fieldLabel: 'Obj Type',
				name: 'objType'
			},
			{
				xtype: 'textfield',
				//hidden:true,
				readOnly:true,		
				fieldLabel: 'Obj ID',
				name: 'objID'
			},
			{
				xtype: 'textfield',
				anchor: '100%',
				readOnly:true,
				fieldLabel: 'Obj Desc',
				name: 'objDesc'
			},
			{
				layout:{
					type:'hbox',
					align:'stretch'
				},
				margin:'0 0 5 0',
				items:[{
					xtype: 'textfield',
					flex:1,
					anchor: '100%',
					labelWidth: 80,
					fieldLabel: 'Entity Name',
					name: 'entityName'
				},
				{
					xtype: 'button',
					iconCls:'dots-three-horizontal',
					margin:'0 0 0 5',
					action:'entitypickbtn'
				}]                 
			},
			{
				xtype: 'checkboxfield',
				anchor: '100%',
				fieldLabel: 'Is Group',
				//labelAlign: 'right',
				name: 'isGroup'
			},
			{
				xtype: 'textfield',
				hidden:true,
				anchor: '100%',
				readOnly:true,
				fieldLabel: 'Rights',
				name: 'rights'
			},
			{
				//bodyPadding: 5,
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				margin:'0 0 5 0',
				items: [{
						xtype: 'checkboxfield',
						flex:1,
						fieldLabel: 'List',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'list'
					},{
						xtype: 'checkboxfield',
						flex:1,
						fieldLabel: 'Open',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'open'
					},{
						xtype: 'checkboxfield',
						flex:1,
						fieldLabel: 'Modify',
						labelAlign: 'right',
						labelWidth: 60,
						name: 'modify'
					}]
			},
			{
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'checkboxfield',
					flex:1,
					fieldLabel: 'Delete',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'delete'
				},
				{
					xtype: 'checkboxfield',
					flex:2,
					fieldLabel: 'Control',
					labelAlign: 'right',
					labelWidth: 60,
					name: 'control'
				}]
			}
		];
		me.callParent(arguments);
    }
});