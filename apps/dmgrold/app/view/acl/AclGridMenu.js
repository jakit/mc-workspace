Ext.define('DMGR.view.acl.AclGridMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.aclgridmenu',
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'menuitem',
            text: 'Delete ACL Entry',
			action : 'deleteacl'
        },
        {
            xtype: 'menuitem',
			action:'modifyacl',
            text: 'Modify ACL Entry'
        },
        {
            xtype: 'menuitem',
            text: 'Add ACL Entry',
            action: 'addacl'
        }];
        me.callParent(arguments);
    }
});