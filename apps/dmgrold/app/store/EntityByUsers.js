Ext.define('DMGR.store.EntityByUsers', {
    extend: 'Ext.data.Store',

    requires: [
        'DMGR.model.EntityByUser',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            model: 'DMGR.model.EntityByUser',
			autoLoad:false,
            proxy: {
                type: 'rest',
				//pageParam : '',
				//limitParam : '',
				//startParam : '',
                noCache: false,
                url: DMGR.view.AppConstants.apiurl+'mssql:sp_entitySelectbyUser',
				reader: {
					type: 'json',
					rootProperty: function(data){
						//console.log(data.result[0].rows);
						return data.result[0].rows;
					}
				}
            }
        }, cfg)]);
    }
});