Ext.define('Overrides.data.BufferedStore', {
    override: 'Ext.data.BufferedStore',
	 
    onProxyPrefetch: function(operation) {
        var me = this,
            resultSet = operation.getResultSet(),
            records = operation.getRecords(),
            successful = operation.wasSuccessful(),
            page = operation.getPage(),
            oldTotal = me.totalCount;
			//console.log(me.getLeadingBufferZone());
			//console.log(me.getTrailingBufferZone());
		
        if (operation.pageMapGeneration === me.getData().pageMapGeneration) {

            if (resultSet) {
				console.log(resultSet.getTotal());
                //console.log(records.length);
                console.log(me.totalCount);
				//console.log(me.getData());
				console.log("Store Toltal Cnt : "+ me.getData().length*50);
                console.log(operation.config.limit);
                var tempTotalCount;
                if(!me.totalCount){
                    tempTotalCount = operation.config.limit;
                } else {
                    tempTotalCount = me.totalCount;
                }
                if(records.length < operation.config.limit){
                    me.totalCount = ((tempTotalCount + records.length) - operation.config.limit);
                } else {
                    me.totalCount = tempTotalCount + (operation.config.limit);
                }
                if (me.totalCount !== oldTotal) {
                    me.fireEvent('totalcountchange', me.totalCount);
                }
            }
            if (page !== undefined) {
                delete me.pageRequests[page];
            }
            me.loading = false;
            me.fireEvent('prefetch', me, records, successful, operation);
            if (successful) {
                me.cachePage(records, operation.getPage());
            }
            Ext.callback(operation.getCallback(), operation.getScope() || me, [records, operation, successful]);
        }
    },
	getById: function(id) {
        var result = (this.snapshot || this.data).findBy(function(record) {
            return record.getId() === id;
        });
        if (this.buffered && !result) {
            return [];
        }
        return result;
    }
});